#[cfg(test)]
mod tests {
	use assert;
	use assert::Assert;
	
	#[test]
	fn eqwd1() {
		assert!(Assert::differs_within_delta(1.1, 1.2, 0.2));
		assert_within_delta!(1.1, 1.2, 0.2);
	}
	
	use dfp::dfp_field::*;
	use dfp::dfp::*;
	use std::rc::*;
	
	static ATEST: i64 = (0x28be60db << 32) | 0x9391054a;

	static BTEST: f64 = 1.0 / 3.0;
	
	#[test]
	pub fn test_dfp() {
		let field = Rc::new(DfpField::new(100)).clone();
		let dfp = Dfp::new_i64(&field, 10010000020);
		println!("testing");
		assert!(dfp.exp == 3);
		assert!(dfp.sign == 1);
		let l = dfp.mant.len();
		
		assert!(l == (100 + 3) / 4);
		assert!(dfp.mant[l - 3] == 20);
		assert!(dfp.mant[l - 2] == 1000);
		assert!(dfp.mant[l - 1] == 100);
		
		
	}
	
	pub fn test() -> i32 {
		if BTEST == 1.0 {
			return 2;
		}
		if BTEST == 2.0 {
			return 3;
		}
		4
	}
	
	#[test]
	fn t2() {
		assert!(test() == 4);
	}
	
}