                
                   /*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * Implements Chi-Square test statistics.
 *
 * <p>This implementation handles both known and unknown distributions.</p>
 *
 * <p>Two samples tests can be used when the distribution is unknown <i>a priori</i>
 * but provided by one sample, or when the hypothesis under test is that the two
 * samples come from the same underlying distribution.</p>
 *
 */
                   
use utils::math_arrays::*;    
use utils::fastmath::*;               
                   
pub struct ChiSquareTest {
}

impl ChiSquareTest {

    /**
     * Construct a ChiSquareTest
     */
    pub fn new() -> ChiSquareTest {
        super();
    }

    /**
     * Computes the <a href="http://www.itl.nist.gov/div898/handbook/eda/section3/eda35f.htm">
     * Chi-Square statistic</a> comparing <code>observed</code> and <code>expected</code>
     * frequency counts.
     * <p>
     * This statistic can be used to perform a Chi-Square test evaluating the null
     * hypothesis that the observed counts follow the expected distribution.</p>
     * <p>
     * <strong>Preconditions</strong>: <ul>
     * <li>Expected counts must all be positive.
     * </li>
     * <li>Observed counts must all be ≥ 0.
     * </li>
     * <li>The observed and expected arrays must have the same length and
     * their common length must be at least 2.
     * </li></ul></p><p>
     * If any of the preconditions are not met, an
     * <code>IllegalArgumentException</code> is thrown.</p>
     * <p><strong>Note: </strong>This implementation rescales the
     * <code>expected</code> array if necessary to ensure that the sum of the
     * expected and observed counts are equal.</p>
     *
     * @param observed array of observed frequency counts
     * @param expected array of expected frequency counts
     * @return chiSquare test statistic
     * @throws NotPositiveException if <code>observed</code> has negative entries
     * @throws NotStrictlyPositiveException if <code>expected</code> has entries that are
     * not strictly positive
     * @throws DimensionMismatchException if the arrays length is less than 2
     */
    pub fn  chi_square(&self,  expected: &Vec<f64>,  observed: &Vec<i64>) -> /*  throws NotPositiveException, NotStrictlyPositiveException, DimensionMismatchException */Result<f64, Rc<Exception>>   {
        if expected.length < 2 {
            return Err(Exc::new_msg("DimensionMismatchException", "{} {}",expected.len(), 2));
        }
        if expected.len() != observed.len() {
            return Err(Exc::new_msg("DimensionMismatchException", "{} {}",expected.len(), observed.len()));
        }
        MathArrays::check_positive(expected);
        MathArrays::check_non_negative(observed);
         let sum_expected: f64 = 0.0;
         let sum_observed: f64 = 0.0;
         {
             let mut i: usize = 0;
            while i < observed.len() {
                {
                    sum_expected += expected[i];
                    sum_observed += observed[i];
                }
                i += 1;
             }
         }

         let mut ratio: f64 = 1.0;
         let mut rescale: bool = false;
        if F64::abs(sum_expected - sum_observed) > 10E-6 {
            ratio = sum_observed / sum_expected;
            rescale = true;
        }
         let sum_sq: f64 = 0.0;
         {
             let mut i: usize = 0;
            while i < observed.len() {
                {
                    if rescale {
                         let dev: f64 = observed[i] - ratio * expected[i];
                        sum_sq += dev * dev / (ratio * expected[i]);
                    } else {
                         let dev: f64 = observed[i] - expected[i];
                        sum_sq += dev * dev / expected[i];
                    }
                }
                i += 1;
             }
         }

        return Ok(sum_sq);
    }

    /**
     * Returns the <i>observed significance level</i>, or <a href=
     * "http://www.cas.lancs.ac.uk/glossary_v1.1/hyptest.html#pvalue">
     * p-value</a>, associated with a
     * <a href="http://www.itl.nist.gov/div898/handbook/eda/section3/eda35f.htm">
     * Chi-square goodness of fit test</a> comparing the <code>observed</code>
     * frequency counts to those in the <code>expected</code> array.
     * <p>
     * The number returned is the smallest significance level at which one can reject
     * the null hypothesis that the observed counts conform to the frequency distribution
     * described by the expected counts.</p>
     * <p>
     * <strong>Preconditions</strong>: <ul>
     * <li>Expected counts must all be positive.
     * </li>
     * <li>Observed counts must all be ≥ 0.
     * </li>
     * <li>The observed and expected arrays must have the same length and
     * their common length must be at least 2.
     * </li></ul></p><p>
     * If any of the preconditions are not met, an
     * <code>IllegalArgumentException</code> is thrown.</p>
     * <p><strong>Note: </strong>This implementation rescales the
     * <code>expected</code> array if necessary to ensure that the sum of the
     * expected and observed counts are equal.</p>
     *
     * @param observed array of observed frequency counts
     * @param expected array of expected frequency counts
     * @return p-value
     * @throws NotPositiveException if <code>observed</code> has negative entries
     * @throws NotStrictlyPositiveException if <code>expected</code> has entries that are
     * not strictly positive
     * @throws DimensionMismatchException if the arrays length is less than 2
     * @throws MaxCountExceededException if an error occurs computing the p-value
     */
    pub fn  chi_square_test(&self,  expected: &Vec<f64>,  observed: &Vec<i64>) -> /*  throws NotPositiveException, NotStrictlyPositiveException, DimensionMismatchException, MaxCountExceededException */Result<f64, Rc<Exception>>   {
        // pass a null rng to avoid unneeded overhead as we will not sample from this distribution
         let distribution: ChiSquaredDistribution = ChiSquaredDistribution::new(null, expected.len() - 1.0);
        return Ok(1.0 - distribution.cumulative_probability(&self.chi_square(&expected, &observed)));
    }

    /**
     * Performs a <a href="http://www.itl.nist.gov/div898/handbook/eda/section3/eda35f.htm">
     * Chi-square goodness of fit test</a> evaluating the null hypothesis that the
     * observed counts conform to the frequency distribution described by the expected
     * counts, with significance level <code>alpha</code>.  Returns true iff the null
     * hypothesis can be rejected with 100 * (1 - alpha) percent confidence.
     * <p>
     * <strong>Example:</strong><br>
     * To test the hypothesis that <code>observed</code> follows
     * <code>expected</code> at the 99% level, use </p><p>
     * <code>chiSquareTest(expected, observed, 0.01) </code></p>
     * <p>
     * <strong>Preconditions</strong>: <ul>
     * <li>Expected counts must all be positive.
     * </li>
     * <li>Observed counts must all be ≥ 0.
     * </li>
     * <li>The observed and expected arrays must have the same length and
     * their common length must be at least 2.
     * <li> <code> 0 < alpha < 0.5 </code>
     * </li></ul></p><p>
     * If any of the preconditions are not met, an
     * <code>IllegalArgumentException</code> is thrown.</p>
     * <p><strong>Note: </strong>This implementation rescales the
     * <code>expected</code> array if necessary to ensure that the sum of the
     * expected and observed counts are equal.</p>
     *
     * @param observed array of observed frequency counts
     * @param expected array of expected frequency counts
     * @param alpha significance level of the test
     * @return true iff null hypothesis can be rejected with confidence
     * 1 - alpha
     * @throws NotPositiveException if <code>observed</code> has negative entries
     * @throws NotStrictlyPositiveException if <code>expected</code> has entries that are
     * not strictly positive
     * @throws DimensionMismatchException if the arrays length is less than 2
     * @throws OutOfRangeException if <code>alpha</code> is not in the range (0, 0.5]
     * @throws MaxCountExceededException if an error occurs computing the p-value
     */
    pub fn  chi_square_test(&self,  expected: &Vec<f64>,  observed: &Vec<i64>,  alpha: f64) -> /*  throws NotPositiveException, NotStrictlyPositiveException, DimensionMismatchException, OutOfRangeException, MaxCountExceededException */Result<bool, Rc<Exception>>   {
        if (alpha <= 0) || (alpha > 0.5) {
            throw OutOfRangeException::new(LocalizedFormats::OUT_OF_BOUND_SIGNIFICANCE_LEVEL, alpha, 0, 0.5);
        }
        return Ok(self.chi_square_test(&expected, &observed) < alpha);
    }

    /**
     *  Computes the Chi-Square statistic associated with a
     * <a href="http://www.itl.nist.gov/div898/handbook/prc/section4/prc45.htm">
     *  chi-square test of independence</a> based on the input <code>counts</code>
     *  array, viewed as a two-way table.
     * <p>
     * The rows of the 2-way table are
     * <code>count[0], ... , count[count.length - 1] </code></p>
     * <p>
     * <strong>Preconditions</strong>: <ul>
     * <li>All counts must be ≥ 0.
     * </li>
     * <li>The count array must be rectangular (i.e. all count[i] subarrays
     *  must have the same length).
     * </li>
     * <li>The 2-way table represented by <code>counts</code> must have at
     *  least 2 columns and at least 2 rows.
     * </li>
     * </li></ul></p><p>
     * If any of the preconditions are not met, an
     * <code>IllegalArgumentException</code> is thrown.</p>
     *
     * @param counts array representation of 2-way table
     * @return chiSquare test statistic
     * @throws NullArgumentException if the array is null
     * @throws DimensionMismatchException if the array is not rectangular
     * @throws NotPositiveException if {@code counts} has negative entries
     */
    pub fn  chi_square(&self,  counts: &Vec<Vec<i64>>) -> /*  throws NullArgumentException, NotPositiveException, DimensionMismatchException */Result<f64, Rc<Exception>>   {
        self.check_array(&counts);
         let n_rows: i32 = counts.len();
         let n_cols: i32 = counts[0].len();
        // compute row, column and total sums
         let row_sum: [f64; n_rows] = [0.0; n_rows];
         let col_sum: [f64; n_cols] = [0.0; n_cols];
         let mut total: f64 = 0.0;
         {
             let mut row: i32 = 0;
            while row < n_rows {
                {
                     {
                         let mut col: i32 = 0;
                        while col < n_cols {
                            {
                                row_sum[row] += counts[row][col];
                                col_sum[col] += counts[row][col];
                                total += counts[row][col];
                            }
                            col += 1;
                         }
                     }

                }
                row += 1;
             }
         }

        // compute expected counts and chi-square
         let sum_sq: f64 = 0.0;
         let mut expected: f64 = 0.0;
         {
             let mut row: i32 = 0;
            while row < n_rows {
                {
                     {
                         let mut col: i32 = 0;
                        while col < n_cols {
                            {
                                expected = (row_sum[row] * col_sum[col]) / total;
                                sum_sq += ((counts[row][col] - expected) * (counts[row][col] - expected)) / expected;
                            }
                            col += 1;
                         }
                     }

                }
                row += 1;
             }
         }

        return Ok(sum_sq);
    }

    /**
     * Returns the <i>observed significance level</i>, or <a href=
     * "http://www.cas.lancs.ac.uk/glossary_v1.1/hyptest.html#pvalue">
     * p-value</a>, associated with a
     * <a href="http://www.itl.nist.gov/div898/handbook/prc/section4/prc45.htm">
     * chi-square test of independence</a> based on the input <code>counts</code>
     * array, viewed as a two-way table.
     * <p>
     * The rows of the 2-way table are
     * <code>count[0], ... , count[count.length - 1] </code></p>
     * <p>
     * <strong>Preconditions</strong>: <ul>
     * <li>All counts must be ≥ 0.
     * </li>
     * <li>The count array must be rectangular (i.e. all count[i] subarrays must have
     *     the same length).
     * </li>
     * <li>The 2-way table represented by <code>counts</code> must have at least 2
     *     columns and at least 2 rows.
     * </li>
     * </li></ul></p><p>
     * If any of the preconditions are not met, an
     * <code>IllegalArgumentException</code> is thrown.</p>
     *
     * @param counts array representation of 2-way table
     * @return p-value
     * @throws NullArgumentException if the array is null
     * @throws DimensionMismatchException if the array is not rectangular
     * @throws NotPositiveException if {@code counts} has negative entries
     * @throws MaxCountExceededException if an error occurs computing the p-value
     */
    pub fn  chi_square_test(&self,  counts: &Vec<Vec<i64>>) -> /*  throws NullArgumentException, DimensionMismatchException, NotPositiveException, MaxCountExceededException */Result<f64, Rc<Exception>>   {
        self.check_array(&counts);
         let df: f64 = (counts.len() as f64 - 1) * (counts[0].len() as f64 - 1);
        // pass a null rng to avoid unneeded overhead as we will not sample from this distribution
         let distribution: ChiSquaredDistribution = ChiSquaredDistribution::new(df);
        return Ok(1 - distribution.cumulative_probability(&self.chi_square(&counts)));
    }

    /**
     * Performs a <a href="http://www.itl.nist.gov/div898/handbook/prc/section4/prc45.htm">
     * chi-square test of independence</a> evaluating the null hypothesis that the
     * classifications represented by the counts in the columns of the input 2-way table
     * are independent of the rows, with significance level <code>alpha</code>.
     * Returns true iff the null hypothesis can be rejected with 100 * (1 - alpha) percent
     * confidence.
     * <p>
     * The rows of the 2-way table are
     * <code>count[0], ... , count[count.length - 1] </code></p>
     * <p>
     * <strong>Example:</strong><br>
     * To test the null hypothesis that the counts in
     * <code>count[0], ... , count[count.length - 1] </code>
     *  all correspond to the same underlying probability distribution at the 99% level, use</p>
     * <p><code>chiSquareTest(counts, 0.01)</code></p>
     * <p>
     * <strong>Preconditions</strong>: <ul>
     * <li>All counts must be ≥ 0.
     * </li>
     * <li>The count array must be rectangular (i.e. all count[i] subarrays must have the
     *     same length).</li>
     * <li>The 2-way table represented by <code>counts</code> must have at least 2 columns and
     *     at least 2 rows.</li>
     * </li></ul></p><p>
     * If any of the preconditions are not met, an
     * <code>IllegalArgumentException</code> is thrown.</p>
     *
     * @param counts array representation of 2-way table
     * @param alpha significance level of the test
     * @return true iff null hypothesis can be rejected with confidence
     * 1 - alpha
     * @throws NullArgumentException if the array is null
     * @throws DimensionMismatchException if the array is not rectangular
     * @throws NotPositiveException if {@code counts} has any negative entries
     * @throws OutOfRangeException if <code>alpha</code> is not in the range (0, 0.5]
     * @throws MaxCountExceededException if an error occurs computing the p-value
     */
    pub fn  chi_square_test(&self,  counts: &Vec<Vec<i64>>,  alpha: f64) -> /*  throws NullArgumentException, DimensionMismatchException, NotPositiveException, OutOfRangeException, MaxCountExceededException */Result<bool, Rc<Exception>>   {
        if (alpha <= 0) || (alpha > 0.5) {
            throw OutOfRangeException::new(LocalizedFormats::OUT_OF_BOUND_SIGNIFICANCE_LEVEL, alpha, 0, 0.5);
        }
        return Ok(self.chi_square_test(&counts) < alpha);
    }

    /**
     * <p>Computes a
     * <a href="http://www.itl.nist.gov/div898/software/dataplot/refman1/auxillar/chi2samp.htm">
     * Chi-Square two sample test statistic</a> comparing bin frequency counts
     * in <code>observed1</code> and <code>observed2</code>.  The
     * sums of frequency counts in the two samples are not required to be the
     * same.  The formula used to compute the test statistic is</p>
     * <code>
     * ∑[(K * observed1[i] - observed2[i]/K)<sup>2</sup> / (observed1[i] + observed2[i])]
     * </code> where
     * <br/><code>K = &sqrt;[&sum(observed2 / ∑(observed1)]</code>
     * </p>
     * <p>This statistic can be used to perform a Chi-Square test evaluating the
     * null hypothesis that both observed counts follow the same distribution.</p>
     * <p>
     * <strong>Preconditions</strong>: <ul>
     * <li>Observed counts must be non-negative.
     * </li>
     * <li>Observed counts for a specific bin must not both be zero.
     * </li>
     * <li>Observed counts for a specific sample must not all be 0.
     * </li>
     * <li>The arrays <code>observed1</code> and <code>observed2</code> must have
     * the same length and their common length must be at least 2.
     * </li></ul></p><p>
     * If any of the preconditions are not met, an
     * <code>IllegalArgumentException</code> is thrown.</p>
     *
     * @param observed1 array of observed frequency counts of the first data set
     * @param observed2 array of observed frequency counts of the second data set
     * @return chiSquare test statistic
     * @throws DimensionMismatchException the the length of the arrays does not match
     * @throws NotPositiveException if any entries in <code>observed1</code> or
     * <code>observed2</code> are negative
     * @throws ZeroException if either all counts of <code>observed1</code> or
     * <code>observed2</code> are zero, or if the count at some index is zero
     * for both arrays
     * @since 1.2
     */
    pub fn  chi_square_data_sets_comparison(&self,  observed1: &Vec<i64>,  observed2: &Vec<i64>) -> /*  throws DimensionMismatchException, NotPositiveException, ZeroException */Result<f64, Rc<Exception>>   {
        // Make sure lengths are same
        if observed1.len() < 2 {
            throw DimensionMismatchException::new(observed1.len(), 2);
        }
        if observed1.len() != observed2.len() {
            throw DimensionMismatchException::new(observed1.len(), observed2.len());
        }
        // Ensure non-negative counts
        MathArrays::check_non_negative(&observed1);
        MathArrays::check_non_negative(&observed2);
        // Compute and compare count sums
         let count_sum1: i64 = 0;
         let count_sum2: i64 = 0;
         let unequal_counts: bool = false;
         let mut weight: f64 = 0.0;
         {
             let mut i: i32 = 0;
            while i < observed1.len() {
                {
                    count_sum1 += observed1[i];
                    count_sum2 += observed2[i];
                }
                i += 1;
             }
         }

        // Ensure neither sample is uniformly 0
        if count_sum1 == 0 || count_sum2 == 0 {
            throw ZeroException::new();
        }
        // Compare and compute weight only if different
        unequal_counts = count_sum1 != count_sum2;
        if unequal_counts {
            weight = F64::sqrt(count_sum1 as f64 / count_sum2 as f64);
        }
        // Compute ChiSquare statistic
         let sum_sq: f64 = 0.0;
         let mut dev: f64 = 0.0;
         let mut obs1: f64 = 0.0;
         let mut obs2: f64 = 0.0;
         {
             let mut i: i32 = 0;
            while i < observed1.len() {
                {
                    if observed1[i] == 0 && observed2[i] == 0 {
                        throw ZeroException::new(LocalizedFormats::OBSERVED_COUNTS_BOTTH_ZERO_FOR_ENTRY, i);
                    } else {
                        obs1 = observed1[i];
                        obs2 = observed2[i];
                        if unequal_counts {
                            // apply weights
                            dev = obs1 / weight - obs2 * weight;
                        } else {
                            dev = obs1 - obs2;
                        }
                        sum_sq += (dev * dev) / (obs1 + obs2);
                    }
                }
                i += 1;
             }
         }

        return Ok(sum_sq);
    }

    /**
     * <p>Returns the <i>observed significance level</i>, or <a href=
     * "http://www.cas.lancs.ac.uk/glossary_v1.1/hyptest.html#pvalue">
     * p-value</a>, associated with a Chi-Square two sample test comparing
     * bin frequency counts in <code>observed1</code> and
     * <code>observed2</code>.
     * </p>
     * <p>The number returned is the smallest significance level at which one
     * can reject the null hypothesis that the observed counts conform to the
     * same distribution.
     * </p>
     * <p>See {@link #chiSquareDataSetsComparison(long[], long[])} for details
     * on the formula used to compute the test statistic. The degrees of
     * of freedom used to perform the test is one less than the common length
     * of the input observed count arrays.
     * </p>
     * <strong>Preconditions</strong>: <ul>
     * <li>Observed counts must be non-negative.
     * </li>
     * <li>Observed counts for a specific bin must not both be zero.
     * </li>
     * <li>Observed counts for a specific sample must not all be 0.
     * </li>
     * <li>The arrays <code>observed1</code> and <code>observed2</code> must
     * have the same length and
     * their common length must be at least 2.
     * </li></ul><p>
     * If any of the preconditions are not met, an
     * <code>IllegalArgumentException</code> is thrown.</p>
     *
     * @param observed1 array of observed frequency counts of the first data set
     * @param observed2 array of observed frequency counts of the second data set
     * @return p-value
     * @throws DimensionMismatchException the the length of the arrays does not match
     * @throws NotPositiveException if any entries in <code>observed1</code> or
     * <code>observed2</code> are negative
     * @throws ZeroException if either all counts of <code>observed1</code> or
     * <code>observed2</code> are zero, or if the count at the same index is zero
     * for both arrays
     * @throws MaxCountExceededException if an error occurs computing the p-value
     * @since 1.2
     */
    pub fn  chi_square_test_data_sets_comparison(&self,  observed1: &Vec<i64>,  observed2: &Vec<i64>) -> /*  throws DimensionMismatchException, NotPositiveException, ZeroException, MaxCountExceededException */Result<f64, Rc<Exception>>   {
        // pass a null rng to avoid unneeded overhead as we will not sample from this distribution
         let distribution: ChiSquaredDistribution = ChiSquaredDistribution::new(null, observed1.len() as f64 - 1);
        return Ok(1 - distribution.cumulative_probability(&self.chi_square_data_sets_comparison(&observed1, &observed2)));
    }

    /**
     * <p>Performs a Chi-Square two sample test comparing two binned data
     * sets. The test evaluates the null hypothesis that the two lists of
     * observed counts conform to the same frequency distribution, with
     * significance level <code>alpha</code>.  Returns true iff the null
     * hypothesis can be rejected with 100 * (1 - alpha) percent confidence.
     * </p>
     * <p>See {@link #chiSquareDataSetsComparison(long[], long[])} for
     * details on the formula used to compute the Chisquare statistic used
     * in the test. The degrees of of freedom used to perform the test is
     * one less than the common length of the input observed count arrays.
     * </p>
     * <strong>Preconditions</strong>: <ul>
     * <li>Observed counts must be non-negative.
     * </li>
     * <li>Observed counts for a specific bin must not both be zero.
     * </li>
     * <li>Observed counts for a specific sample must not all be 0.
     * </li>
     * <li>The arrays <code>observed1</code> and <code>observed2</code> must
     * have the same length and their common length must be at least 2.
     * </li>
     * <li> <code> 0 < alpha < 0.5 </code>
     * </li></ul><p>
     * If any of the preconditions are not met, an
     * <code>IllegalArgumentException</code> is thrown.</p>
     *
     * @param observed1 array of observed frequency counts of the first data set
     * @param observed2 array of observed frequency counts of the second data set
     * @param alpha significance level of the test
     * @return true iff null hypothesis can be rejected with confidence
     * 1 - alpha
     * @throws DimensionMismatchException the the length of the arrays does not match
     * @throws NotPositiveException if any entries in <code>observed1</code> or
     * <code>observed2</code> are negative
     * @throws ZeroException if either all counts of <code>observed1</code> or
     * <code>observed2</code> are zero, or if the count at the same index is zero
     * for both arrays
     * @throws OutOfRangeException if <code>alpha</code> is not in the range (0, 0.5]
     * @throws MaxCountExceededException if an error occurs performing the test
     * @since 1.2
     */
    pub fn  chi_square_test_data_sets_comparison(&self,  observed1: &Vec<i64>,  observed2: &Vec<i64>,  alpha: f64) -> /*  throws DimensionMismatchException, NotPositiveException, ZeroException, OutOfRangeException, MaxCountExceededException */Result<bool, Rc<Exception>>   {
        if alpha <= 0 || alpha > 0.5 {
            throw OutOfRangeException::new(LocalizedFormats::OUT_OF_BOUND_SIGNIFICANCE_LEVEL, alpha, 0, 0.5);
        }
        return Ok(self.chi_square_test_data_sets_comparison(&observed1, &observed2) < alpha);
    }

    /**
     * Checks to make sure that the input long[][] array is rectangular,
     * has at least 2 rows and 2 columns, and has all non-negative entries.
     *
     * @param in input 2-way table to check
     * @throws NullArgumentException if the array is null
     * @throws DimensionMismatchException if the array is not valid
     * @throws NotPositiveException if the array contains any negative entries
     */
    fn  check_array(&self,  in: &Vec<Vec<i64>>)  -> /*  throws NullArgumentException, DimensionMismatchException, NotPositiveException */Result<Void, Rc<Exception>>   {
        if in.len() < 2 {
            throw DimensionMismatchException::new(in.len(), 2);
        }
        if in[0].len() < 2 {
            throw DimensionMismatchException::new(in[0].len(), 2);
        }
        MathArrays::check_rectangular(&in);
        MathArrays::check_non_negative(&in);
    }
}


                
                