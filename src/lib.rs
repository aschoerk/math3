#![feature(plugin)]
#![cfg_attr(test, plugin(stainless))]

#![allow(dead_code)]
#[cfg(test)]
extern crate hamcrest;

#[macro_use]
extern crate lazy_static;
extern crate time;

#[macro_use]
pub mod assert;




pub mod base;
pub mod java;
pub mod complex;
pub mod distribution;
// pub mod tests;
pub mod util;
pub mod dfp;
pub mod random;
pub mod analysis;
pub mod base_tests;



static ATEST: i64 = (0x28be60db << 32) | 0x9391054a;

static BTEST: f64 = 1.0 / 3.0;

pub fn test() -> i32 {
    if BTEST == 1.0 {
        return 2;
    }
    if BTEST == 2.0 {
        return 3;
    }
    4
}


struct X<T: Copy> {
    pub a: Vec<i32>,
    pub i: i32,
    pub t: T,
}

struct Y<T: Copy> {
    pub a: Vec<T>,
    pub i: i32,
}

#[allow(unused_variables)]
impl<T: Copy> Y<T> {
    fn new(count: i32, val: i32) -> Y<i32> {
        let mut v: Vec<i32> = Vec::new();
        for _ in 0..count {
            v.push(10);
        }
        let res: Y<i32> = Y { a: v, i: 10 };
        res
    }

    fn new_s<S: Copy>(count: i32, val: S) -> Y<S> {
        let mut v: Vec<S> = Vec::new();
        for _ in 0..count {
            v.push(val);
        }
        let a = Y { a: v, i: 10 };
        // let res: X<i32> =  X { a: v, i: 10, t: 10 };
        a
    }
}

#[allow(unused_variables)]
impl<T: Copy> X<T> {
    fn new(count: i32, val: i32) -> X<i32> {
        let mut v: Vec<i32> = Vec::new();
        for _ in 0..count {
            v.push(10);
        }
        let res: X<i32> = X {
            a: v,
            i: 10,
            t: 10,
        };
        res
    }

    fn new_s<S: Copy>(count: i32, val: S) -> X<S> {
        let mut v: Vec<i32> = Vec::new();
        for _ in 0..count {
            v.push(10);
        }
        let a = X {
            a: v,
            i: 10,
            t: val,
        };
        // let res: X<i32> =  X { a: v, i: 10, t: 10 };
        a
    }
}
