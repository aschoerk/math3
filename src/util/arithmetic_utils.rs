
// Licensed to the Apache Software Foundation (ASF) under one or more
// contributor license agreements.  See the NOTICE file distributed with
// this work for additional information regarding copyright ownership.
// The ASF licenses this file to You under the Apache License, Version 2.0
// (the "License"); you may not use this file except in compliance with
// the License.  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

/**
 * Some useful, arithmetics related, additions to the built-in functions in
 * {@link Math}.
 *
 */

use std::i64;
use std::i32;
use util::combinatorics_utils::*;
use util::fastmath;
use java;
use std::rc::*;
use java::exc::*;

pub struct ArithmeticUtils {
}

pub struct I32 {
	
}

impl I32 {
    /**
     * Add two integers, checking for overflow.
     *
     * @param x an addend
     * @param y an addend
     * @return the sum {@code x+y}
     * @throws MathArithmeticException if the result can not be represented
     * as an {@code int}.
     * @since 1.1
     */
    pub fn add_and_check(x: i32, y: i32) -> Result<i32, Rc<Exception>> {
        let s: i64 = x as i64 + y as i64;
        if s < i32::MIN as i64 || s > i32::MAX as i64 {
            return Err(Exc::new_msg("MathArithmeticException",
                                    format!("overflow in addition: {0} + {1}", x, y)));
            // throw MathArithmeticException::new(LocalizedFormats::OVERFLOW_IN_ADDITION, x, y);
        }
        return Ok(s as i32);
    }
    /**
     * Returns an exact representation of the <a
     * href="http://mathworld.wolfram.com/BinomialCoefficient.html"> Binomial
     * Coefficient</a>, "{@code n choose k}", the number of
     * {@code k}-element subsets that can be selected from an
     * {@code n}-element set.
     * <p>
     * <Strong>Preconditions</strong>:
     * <ul>
     * <li> {@code 0 <= k <= n } (otherwise
     * {@code IllegalArgumentException} is thrown)</li>
     * <li> The result is small enough to fit into a {@code long}. The
     * largest value of {@code n} for which all coefficients are
     * {@code  < Long.MAX_VALUE} is 66. If the computed value exceeds
     * {@code Long.MAX_VALUE} an {@code ArithMeticException} is
     * thrown.</li>
     * </ul></p>
     *
     * @param n the size of the set
     * @param k the size of the subsets to be counted
     * @return {@code n choose k}
     * @throws NotPositiveException if {@code n < 0}.
     * @throws NumberIsTooLargeException if {@code k > n}.
     * @throws MathArithmeticException if the result is too large to be
     * represented by a long integer.
     * @deprecated use {@link CombinatoricsUtils#binomialCoefficient(int, int)}
     */
    pub fn binomial_coefficient(n: u32, k: u32) -> Result<i64, Rc<Exception>> {
        return CombinatoricsUtils::binomial_coefficient(n, k);
    }

    /**
     * Returns a {@code double} representation of the <a
     * href="http://mathworld.wolfram.com/BinomialCoefficient.html"> Binomial
     * Coefficient</a>, "{@code n choose k}", the number of
     * {@code k}-element subsets that can be selected from an
     * {@code n}-element set.
     * <p>
     * <Strong>Preconditions</strong>:
     * <ul>
     * <li> {@code 0 <= k <= n } (otherwise
     * {@code IllegalArgumentException} is thrown)</li>
     * <li> The result is small enough to fit into a {@code double}. The
     * largest value of {@code n} for which all coefficients are <
     * Double.MAX_VALUE is 1029. If the computed value exceeds Double.MAX_VALUE,
     * Double.POSITIVE_INFINITY is returned</li>
     * </ul></p>
     *
     * @param n the size of the set
     * @param k the size of the subsets to be counted
     * @return {@code n choose k}
     * @throws NotPositiveException if {@code n < 0}.
     * @throws NumberIsTooLargeException if {@code k > n}.
     * @throws MathArithmeticException if the result is too large to be
     * represented by a long integer.
     * @deprecated use {@link CombinatoricsUtils#binomialCoefficientDouble(int, int)}
     */
    pub fn binomial_coefficient_double(n: u32, k: u32) -> Result<f64, Rc<Exception>> {
        return CombinatoricsUtils::binomial_coefficient_double(n, k);
    }
    /**
     * Returns the natural {@code log} of the <a
     * href="http://mathworld.wolfram.com/BinomialCoefficient.html"> Binomial
     * Coefficient</a>, "{@code n choose k}", the number of
     * {@code k}-element subsets that can be selected from an
     * {@code n}-element set.
     * <p>
     * <Strong>Preconditions</strong>:
     * <ul>
     * <li> {@code 0 <= k <= n } (otherwise
     * {@code IllegalArgumentException} is thrown)</li>
     * </ul></p>
     *
     * @param n the size of the set
     * @param k the size of the subsets to be counted
     * @return {@code n choose k}
     * @throws NotPositiveException if {@code n < 0}.
     * @throws NumberIsTooLargeException if {@code k > n}.
     * @throws MathArithmeticException if the result is too large to be
     * represented by a long integer.
     * @deprecated use {@link CombinatoricsUtils#binomialCoefficientLog(int, int)}
     */
    pub fn binomial_coefficient_log(n: u32, k: u32) -> Result<f64, Rc<Exception>> {
        return CombinatoricsUtils::binomial_coefficient_log(n, k);
    }

    /**
     * Returns n!. Shorthand for {@code n} <a
     * href="http://mathworld.wolfram.com/Factorial.html"> Factorial</a>, the
     * product of the numbers {@code 1,...,n}.
     * <p>
     * <Strong>Preconditions</strong>:
     * <ul>
     * <li> {@code n >= 0} (otherwise
     * {@code IllegalArgumentException} is thrown)</li>
     * <li> The result is small enough to fit into a {@code long}. The
     * largest value of {@code n} for which {@code n!} <
     * Long.MAX_VALUE} is 20. If the computed value exceeds {@code Long.MAX_VALUE}
     * an {@code ArithMeticException } is thrown.</li>
     * </ul>
     * </p>
     *
     * @param n argument
     * @return {@code n!}
     * @throws MathArithmeticException if the result is too large to be represented
     * by a {@code long}.
     * @throws NotPositiveException if {@code n < 0}.
     * @throws MathArithmeticException if {@code n > 20}: The factorial value is too
     * large to fit in a {@code long}.
     * @deprecated use {@link CombinatoricsUtils#factorial(int)}
     */
    pub fn factorial(n: i32) -> Result<i64, Rc<Exception>> {
        return CombinatoricsUtils::factorial(n);
    }

    /**
     * Compute n!, the<a href="http://mathworld.wolfram.com/Factorial.html">
     * factorial</a> of {@code n} (the product of the numbers 1 to n), as a
     * {@code double}.
     * The result should be small enough to fit into a {@code double}: The
     * largest {@code n} for which {@code n! < Double.MAX_VALUE} is 170.
     * If the computed value exceeds {@code Double.MAX_VALUE},
     * {@code Double.POSITIVE_INFINITY} is returned.
     *
     * @param n Argument.
     * @return {@code n!}
     * @throws NotPositiveException if {@code n < 0}.
     * @deprecated use {@link CombinatoricsUtils#factorialDouble(int)}
     */
    pub fn factorial_double(n: i32) -> Result<f64, Rc<Exception>> {
        return CombinatoricsUtils::factorial_double(n);
    }

    /**
     * Compute the natural logarithm of the factorial of {@code n}.
     *
     * @param n Argument.
     * @return {@code n!}
     * @throws NotPositiveException if {@code n < 0}.
     * @deprecated use {@link CombinatoricsUtils#factorialLog(int)}
     */
    pub fn factorial_log(n: i32) -> Result<f64, Rc<Exception>> {
        return CombinatoricsUtils::factorial_log(n);
    }

    /**
     * Computes the greatest common divisor of the absolute value of two
     * numbers, using a modified version of the "binary gcd" method.
     * See Knuth 4.5.2 algorithm B.
     * The algorithm is due to Josef Stein (1961).
     * <br/>
     * Special cases:
     * <ul>
     *  <li>The invocations
     *   {@code gcd(Integer.MIN_VALUE, Integer.MIN_VALUE)},
     *   {@code gcd(Integer.MIN_VALUE, 0)} and
     *   {@code gcd(0, Integer.MIN_VALUE)} throw an
     *   {@code ArithmeticException}, because the result would be 2^31, which
     *   is too large for an int value.</li>
     *  <li>The result of {@code gcd(x, x)}, {@code gcd(0, x)} and
     *   {@code gcd(x, 0)} is the absolute value of {@code x}, except
     *   for the special cases above.</li>
     *  <li>The invocation {@code gcd(0, 0)} is the only one which returns
     *   {@code 0}.</li>
     * </ul>
     *
     * @param p Number.
     * @param q Number.
     * @return the greatest common divisor (never negative).
     * @throws MathArithmeticException if the result cannot be represented as
     * a non-negative {@code int} value.
     * @since 1.1
     */
    pub fn gcd(p: i32, q: i32) -> Result<i32, Rc<Exception>> {
        let mut a: i32 = p;
        let mut b: i32 = q;
        if a == 0 || b == 0 {
            if a == i32::MIN || b == i32::MIN {
                return Err(Exc::new_msg("MathArithmeticException",
                                        format!("overflow: gcd({0}, {1}) is 2^31", p ,q )));
                // throw MathArithmeticException::new(LocalizedFormats::GCD_OVERFLOW_32_BITS, p, q);
            }
            return Ok(fastmath::I32::abs(a + b));
        }
        let mut al: i64 = a as i64;
        let mut bl: i64 = b as i64;
        let mut use_long: bool = false;
        if a < 0 {
            if i32::MIN == a {
                use_long = true;
            } else {
                a = -a;
            }
            al = -al;
        }
        if b < 0 {
            if i32::MIN == b {
                use_long = true;
            } else {
                b = -b;
            }
            bl = -bl;
        }
        if use_long {
            if al == bl {
                return Err(Exc::new_msg("MathArithmeticException",
                                        format!("overflow: gcd({0}, {1}) is 2^31", p ,q )));
                // throw MathArithmeticException::new(LocalizedFormats::GCD_OVERFLOW_32_BITS, p, q);
            }
            let mut blbu: i64 = bl;
            bl = al;
            al = blbu % al;
            if al == 0 {
                if bl > i32::MAX as i64 {
                    return Err(Exc::new_msg("MathArithmeticException",
                                            format!("overflow: gcd({0}, {1}) is 2^31", p ,q )));
                    // throw MathArithmeticException::new(LocalizedFormats::GCD_OVERFLOW_32_BITS, p, q);
                }
                return Ok(bl as i32);
            }
            blbu = bl;
            // Now "al" and "bl" fit in an "int".
            b = al as i32;
            a = (blbu % al) as i32;
        }
        return Ok(I32::gcd_positive(a, b));
    }

    /**
     * Computes the greatest common divisor of two <em>positive</em> numbers
     * (this precondition is <em>not</em> checked and the result is undefined
     * if not fulfilled) using the "binary gcd" method which avoids division
     * and modulo operations.
     * See Knuth 4.5.2 algorithm B.
     * The algorithm is due to Josef Stein (1961).
     * <br/>
     * Special cases:
     * <ul>
     *  <li>The result of {@code gcd(x, x)}, {@code gcd(0, x)} and
     *   {@code gcd(x, 0)} is the value of {@code x}.</li>
     *  <li>The invocation {@code gcd(0, 0)} is the only one which returns
     *   {@code 0}.</li>
     * </ul>
     *
     * @param a Positive number.
     * @param b Positive number.
     * @return the greatest common divisor.
     */
    fn gcd_positive(a_p: i32, b_p: i32) -> i32 {
        let mut a = a_p;
        let mut b = b_p;
        if a == 0 {
            return b;
        } else if b == 0 {
            return a;
        }
        // Make "a" and "b" odd, keeping track of common power of 2.
        let a_twos: i32 = java::utils::number_of_trailing_zeros(a as u32);
        a >>= a_twos;
        let b_twos: i32 = java::utils::number_of_trailing_zeros(b as u32);
        b >>= b_twos;
        let shift: i32 = fastmath::I32::min(a_twos, b_twos);
        //  "b" becomes the minimum of the current values.
        while a != b {
            let delta: i32 = a - b;
            b = fastmath::I32::min(a, b);
            a = fastmath::I32::abs(delta);
            // Remove any power of 2 in "a" ("b" is guaranteed to be odd).
            a >>= java::utils::number_of_trailing_zeros(a as u32);
        }
        // Recover the common power of 2.
        return a << shift;
    }

    /**
     * <p>
     * Returns the least common multiple of the absolute value of two numbers,
     * using the formula {@code lcm(a,b) = (a / gcd(a,b)) * b}.
     * </p>
     * Special cases:
     * <ul>
     * <li>The invocations {@code lcm(Integer.MIN_VALUE, n)} and
     * {@code lcm(n, Integer.MIN_VALUE)}, where {@code abs(n)} is a
     * power of 2, throw an {@code ArithmeticException}, because the result
     * would be 2^31, which is too large for an int value.</li>
     * <li>The result of {@code lcm(0, x)} and {@code lcm(x, 0)} is
     * {@code 0} for any {@code x}.
     * </ul>
     *
     * @param a Number.
     * @param b Number.
     * @return the least common multiple, never negative.
     * @throws MathArithmeticException if the result cannot be represented as
     * a non-negative {@code int} value.
     * @since 1.1
     */
    pub fn lcm(a: i32, b: i32) -> Result<i32, Rc<Exception>> {
        if a == 0 || b == 0 {
            return Ok(0);
        }
        let tmp = try!(I32::gcd(a, b));
        let tmp2 = try!(I32::mul_and_check(a / tmp, b));
        let lcm: i32 = fastmath::I32::abs(tmp2);
        if lcm == i32::MIN {
            return Err(Exc::new_msg("MathArithmeticException",
                                    format!("overflow: lcm({0}, {1}) is 2^31", a ,b )));
        }
        return Ok(lcm);
    }


    /**
     * Multiply two integers, checking for overflow.
     *
     * @param x Factor.
     * @param y Factor.
     * @return the product {@code x * y}.
     * @throws MathArithmeticException if the result can not be
     * represented as an {@code int}.
     * @since 1.1
     */
    pub fn mul_and_check(x: i32, y: i32) -> Result<i32, Rc<Exception>> {
        let m: i64 = (x as i64) * (y as i64);
        if m < i32::MIN as i64 || m > i32::MAX as i64 {
            return Err(Exc::new("MathArithmeticException"));
        }
        return Ok(m as i32);
    }


    /**
     * Subtract two integers, checking for overflow.
     *
     * @param x Minuend.
     * @param y Subtrahend.
     * @return the difference {@code x - y}.
     * @throws MathArithmeticException if the result can not be represented
     * as an {@code int}.
     * @since 1.1
     */
    pub fn sub_and_check(x: i32, y: i32) -> Result<i32, Rc<Exception>> {
        let s: i64 = x as i64 - y as i64;
        if s < i32::MIN as i64 || s > i32::MAX as i64 {
            return Err(Exc::new_msg("MathArithmeticException",
                                    format!("overflow in subtraction: {0} - {1}", x ,y )));
        }
        return Ok(s as i32);
    }




    /**
     * Raise an int to an int power.
     *
     * @param k Number to raise.
     * @param e Exponent (must be positive or zero).
     * @return \( k^e \)
     * @throws NotPositiveException if {@code e < 0}.
     * @throws MathArithmeticException if the result would overflow.
     */
    pub fn pow(k: i32, e: i32) -> Result<i32, Rc<Exception>> {
        if e < 0 {
            return Err(Exc::new_msg("NotPositiveException", format!("exponent ({0})",e)));
            // throw NotPositiveException::new(LocalizedFormats::EXPONENT, e);
        }
        'try: loop {
            let mut exp: i32 = e;
            let mut result: i32 = 1;
            let mut k2p: i32 = k;
            loop {
                if (exp & 0x1) != 0 {
                    match I32::mul_and_check(result, k2p) {
                        Ok(res) => result = res,
                        _ => break 'try,
                    }
                }
                exp >>= 1;
                if exp == 0 {
                    break;
                }
                match I32::mul_and_check(k2p, k2p) {
                    Ok(k2p_res) => k2p = k2p_res,
                    _ => break 'try,
                }
            }
            return Ok(result);
        }
        {
            // mae.get_context().add_message(LocalizedFormats::OVERFLOW);
            // mae.get_context().add_message(LocalizedFormats::BASE, k);
            // mae.get_context().add_message(LocalizedFormats::EXPONENT, e);
            Err(Exc::new("MathArithmeticExpression"))
        }
    }
    /**
     * Raise an int to a long power.
     *
     * @param k Number to raise.
     * @param e Exponent (must be positive or zero).
     * @return k<sup>e</sup>
     * @throws NotPositiveException if {@code e < 0}.
     * @deprecated As of 3.3. Please use {@link #pow(int,int)} instead.
     */
    pub fn pow_i64(k: i32, e_p: i64) -> Result<i32, Rc<Exception>> {
        let mut e = e_p;
        if e < 0 {
            return Err(Exc::new_msg("NotPositiveException", format!("exponent ({0})",e)));
            // throw NotPositiveException::new(LocalizedFormats::EXPONENT, e);
        }
        let mut result: i32 = 1;
        let mut k2p: i32 = k;
        while e != 0 {
            if (e & 0x1) != 0 {
                result *= k2p;
            }
            k2p *= k2p;
            e >>= 1;
        }
        return Ok(result);
    }
}


pub struct I64 {
	
}

impl I64 {
    /**
     * Add two long integers, checking for overflow.
     *
     * @param a an addend
     * @param b an addend
     * @return the sum {@code a+b}
     * @throws MathArithmeticException if the result can not be represented as an long
     * @since 1.2
     */
    pub fn add_and_check(a: i64, b: i64) -> Result<i64, Rc<Exception>> {
        return I64::add_and_check_pattern(a, b); // , LocalizedFormats::OVERFLOW_IN_ADDITION);
    }





    /**
     * <p>
     * Gets the greatest common divisor of the absolute value of two numbers,
     * using the "binary gcd" method which avoids division and modulo
     * operations. See Knuth 4.5.2 algorithm B. This algorithm is due to Josef
     * Stein (1961).
     * </p>
     * Special cases:
     * <ul>
     * <li>The invocations
     * {@code gcd(Long.MIN_VALUE, Long.MIN_VALUE)},
     * {@code gcd(Long.MIN_VALUE, 0L)} and
     * {@code gcd(0L, Long.MIN_VALUE)} throw an
     * {@code ArithmeticException}, because the result would be 2^63, which
     * is too large for a long value.</li>
     * <li>The result of {@code gcd(x, x)}, {@code gcd(0L, x)} and
     * {@code gcd(x, 0L)} is the absolute value of {@code x}, except
     * for the special cases above.
     * <li>The invocation {@code gcd(0L, 0L)} is the only one which returns
     * {@code 0L}.</li>
     * </ul>
     *
     * @param p Number.
     * @param q Number.
     * @return the greatest common divisor, never negative.
     * @throws MathArithmeticException if the result cannot be represented as
     * a non-negative {@code long} value.
     * @since 2.1
     */
    pub fn gcd(p: i64, q: i64) -> Result<i64, Rc<Exception>> {
        let mut u: i64 = p;
        let mut v: i64 = q;
        if (u == 0) || (v == 0) {
            if (u == i64::MIN) || (v == i64::MIN) {
                return Err(Exc::new_msg("MathArithmeticException",
                                        format!("overflow: gcd({0}, {1}) is 2^63", p, q)));
                // throw MathArithmeticException::new(LocalizedFormats::GCD_OVERFLOW_64_BITS, p, q);
            }
            return Ok(fastmath::I64::abs(u) + fastmath::I64::abs(v));
        }
        // assert u!=0 && v!=0;
        if u > 0 {
            u = -u;
        }
        // make u negative
        if v > 0 {
            v = -v;
        }
        // make v negative
        // B1. [Find power of 2]
        let mut k: i32 = 0;
        while (u & 1) == 0 && (v & 1) == 0 && k < 63 {
            // while u and v are
            // both even...
            u /= 2;
            v /= 2;
            // cast out twos.
            k += 1;
        }
        if k == 63 {
            return Err(Exc::new_msg("MathArithmeticException",
                                    format!("overflow: gcd({0}, {1}) is 2^63", p, q)));
        }
        // B2. Initialize: u and v have been divided by 2^k and at least
        // one is odd.
        let mut t: i64 = if (u & 1) == 1 { v } else { -(u / 2) };
        // t positive: u was even, v is odd (t replaces u)
        loop {
            {
                // B4/B3: cast out twos from t.
                while (t & 1) == 0 {
                    // while t is even..
                    // cast out twos
                    t /= 2;
                }
                // B5 [reset max(u,v)]
                if t > 0 {
                    u = -t;
                } else {
                    v = t;
                }
                // B6/B3. at this point both u and v should be odd.
                t = (v - u) / 2;
                // |u| larger: t positive (replace u)
                // |v| larger: t negative (replace v)
            }
            if t == 0 {
                break;
            }
        }
        // gcd is u*2^k
        return Ok(-u * (1 << k));
    }
    /**
     * <p>
     * Returns the least common multiple of the absolute value of two numbers,
     * using the formula {@code lcm(a,b) = (a / gcd(a,b)) * b}.
     * </p>
     * Special cases:
     * <ul>
     * <li>The invocations {@code lcm(Long.MIN_VALUE, n)} and
     * {@code lcm(n, Long.MIN_VALUE)}, where {@code abs(n)} is a
     * power of 2, throw an {@code ArithmeticException}, because the result
     * would be 2^63, which is too large for an int value.</li>
     * <li>The result of {@code lcm(0L, x)} and {@code lcm(x, 0L)} is
     * {@code 0L} for any {@code x}.
     * </ul>
     *
     * @param a Number.
     * @param b Number.
     * @return the least common multiple, never negative.
     * @throws MathArithmeticException if the result cannot be represented
     * as a non-negative {@code long} value.
     * @since 2.1
     */
    pub fn lcm(a: i64, b: i64) -> Result<i64, Rc<Exception>> {
        if a == 0 || b == 0 {
            return Ok(0);
        }
        let tmp = try!(I64::gcd(a, b));
        let tmp2 = try!(I64::mul_and_check(a / tmp, b));
        let lcm: i64 = fastmath::I64::abs(tmp2);
        if lcm == i64::MIN {
            return Err(Exc::new_msg("MathArithmeticException",
                                    format!("overflow: lcm({0}, {1}) is 2^63", a ,b )));
            //          throw MathArithmeticException::new(LocalizedFormats::LCM_OVERFLOW_64_BITS, a, b);
        }
        return Ok(lcm);
    }
    /**
     * Multiply two long integers, checking for overflow.
     *
     * @param a Factor.
     * @param b Factor.
     * @return the product {@code a * b}.
     * @throws MathArithmeticException if the result can not be represented
     * as a {@code long}.
     * @since 1.2
     */
    pub fn mul_and_check(a: i64, b: i64) -> Result<i64, Rc<Exception>> {
        let ret: i64;
        if a > b {
            // use symmetry to reduce boundary cases
            ret = try!(I64::mul_and_check(b, a));
        } else {
            if a < 0 {
                if b < 0 {
                    // check for positive overflow with negative a, negative b
                    if a >= i64::MAX / b {
                        ret = a * b;
                    } else {
                        return Err(Exc::new("MathArithmeticException"));
                        //                        throw MathArithmeticException::new();
                    }
                } else if b > 0 {
                    // check for negative overflow with negative a, positive b
                    if i64::MIN / b <= a {
                        ret = a * b;
                    } else {
                        return Err(Exc::new("MathArithmeticException"));
                        //                        throw MathArithmeticException::new();
                    }
                } else {
                    // assert b == 0
                    ret = 0;
                }
            } else if a > 0 {
                // check for positive overflow with positive a, positive b
                if a <= i64::MAX / b {
                    ret = a * b;
                } else {
                    return Err(Exc::new("MathArithmeticException"));
                    //                    throw MathArithmeticException::new();
                }
            } else {
                // assert a == 0
                ret = 0;
            }
        }
        return Ok(ret);
    }
    /**
     * Subtract two long integers, checking for overflow.
     *
     * @param a Value.
     * @param b Value.
     * @return the difference {@code a - b}.
     * @throws MathArithmeticException if the result can not be represented as a
     * {@code long}.
     * @since 1.2
     */
    pub fn sub_and_check(a: i64, b: i64) -> Result<i64, Rc<Exception>> {
        let ret: i64;
        if b == i64::MIN {
            if a < 0 {
                ret = a - b;
            } else {
                return Err(Exc::new_msg("MathArithmeticException",
                                        format!("overflow in subtraction: {0} - {1}", a ,-b )));
                //                throw MathArithmeticException::new(LocalizedFormats::OVERFLOW_IN_ADDITION, a, -b);
            }
        } else {
            // use additive inverse
            ret = try!(I64::add_and_check_pattern(a, -b)); // , LocalizedFormats::OVERFLOW_IN_ADDITION);
        }
        return Ok(ret);
    }

    /** 
     * Add two long integers, checking for overflow.
     * 
     * @param a Addend.
     * @param b Addend.
     * @param pattern Pattern to use for any thrown exception.
     * @return the sum {@code a + b}.
     * @throws MathArithmeticException if the result cannot be represented
     * as a {@code long}.
     * @since 1.2
     */
    fn add_and_check_pattern(a: i64,
                             b: i64 /* ,  pattern: &Localizable */)
                             -> Result<i64, Rc<Exception>> {

        let result: i64 = a.wrapping_add(b);
        if !(((a ^ b) < 0) || ((a ^ result) >= 0)) {
            return Err(Exc::new_msg("MathArithmeticException",
                                    format!("overflow in addition: {0} + {1}", a, b)));
        }
        return Ok(result);

    }
    /**
     * Raise a long to an int power.
     *
     * @param k Number to raise.
     * @param e Exponent (must be positive or zero).
     * @return \( k^e \)
     * @throws NotPositiveException if {@code e < 0}.
     * @throws MathArithmeticException if the result would overflow.
     */
    pub fn pow_i32(k: i64, e: i32) -> Result<i64, Rc<Exception>> {
        if e < 0 {
            return Err(Exc::new_msg("NotPositiveException", format!("exponent ({0})",e)));
            // throw NotPositiveException::new(LocalizedFormats::EXPONENT, e);
        }
        'try: loop {
            let mut exp: i32 = e;
            let mut result = Ok(1i64);
            let mut k2p = k;
            loop {
                if (exp & 0x1) != 0 {
                    result = I64::mul_and_check(result.unwrap(), k2p);
                    if result.is_err() {
                        return result;
                    }
                }
                exp >>= 1;
                if exp == 0 {
                    break;
                }
                k2p = try!(I64::mul_and_check(k2p, k2p));
            }
            return result;
        }
        // catch ( mae: &MathArithmeticException) {
        // mae.get_context().add_message(LocalizedFormats::OVERFLOW);
        // mae.get_context().add_message(LocalizedFormats::BASE, k);
        // mae.get_context().add_message(LocalizedFormats::EXPONENT, e);
        // throw mae;
        // }
        //
    }

    /**
     * Raise a long to a long power.
     *
     * @param k Number to raise.
     * @param e Exponent (must be positive or zero).
     * @return k<sup>e</sup>
     * @throws NotPositiveException if {@code e < 0}.
     * @deprecated As of 3.3. Please use {@link #pow(long,int)} instead.
     */
    pub fn pow(k: i64, e_p: i64) -> Result<i64, Rc<Exception>> {
        let mut e = e_p;
        if e < 0 {
            return Err(Exc::new_msg("NotPositiveException", format!("exponent ({0})",e)));
            // throw NotPositiveException::new(LocalizedFormats::EXPONENT, e);
        }
        let mut result: i64 = 1;
        let mut k2p: i64 = k;
        while e != 0 {
            if (e & 0x1) != 0 {
                result *= k2p;
            }
            k2p *= k2p;
            e >>= 1;
        }
        return Ok(result);
    }


    /**
     * Returns true if the argument is a power of two.
     *
     * @param n the number to test
     * @return true if the argument is a power of two
     */
    pub fn is_power_of_two(n: i64) -> bool {
        return (n > 0) && ((n & (n - 1)) == 0);
    }
}
impl ArithmeticUtils {
    // Raise a BigInteger to an int power.
    //
    // @param k Number to raise.
    // @param e Exponent (must be positive or zero).
    // @return k<sup>e</sup>
    // @throws NotPositiveException if {@code e < 0}.
    // /
    // pub fn  pow( k: &BigInteger,  e: i32) -> /*  throws NotPositiveException */Result<BigInteger>   {
    // if e < 0 {
    // return Err("NotPositiveException");
    // throw NotPositiveException::new(LocalizedFormats::EXPONENT, e);
    // }
    // return k.pow(e);
    // }
    //
    //
    // Raise a BigInteger to a long power.
    //
    // @param k Number to raise.
    // @param e Exponent (must be positive or zero).
    // @return k<sup>e</sup>
    // @throws NotPositiveException if {@code e < 0}.
    // /
    // pub fn  pow( k: &BigInteger,  e: i64) -> /*  throws NotPositiveException */Result<BigInteger>   {
    // if e < 0 {
    // return Err("NotPositiveException");
    // throw NotPositiveException::new(LocalizedFormats::EXPONENT, e);
    // }
    // let mut result: BigInteger = BigInteger::ONE;
    // let mut k2p: BigInteger = k;
    // while e != 0 {
    // if (e & 0x1) != 0 {
    // result = result.multiply(k2p);
    // }
    // k2p = k2p.multiply(k2p);
    // e >>= 1;
    // }
    // return result;
    // }
    //
    //
    // Raise a BigInteger to a BigInteger power.
    //
    // @param k Number to raise.
    // @param e Exponent (must be positive or zero).
    // @return k<sup>e</sup>
    // @throws NotPositiveException if {@code e < 0}.
    // /
    // pub fn  pow( k: &BigInteger,  e: &BigInteger) -> /*  throws NotPositiveException */Result<BigInteger>   {
    // if e.compare_to(BigInteger::ZERO) < 0 {
    // return Err("NotPositiveException");
    // throw NotPositiveException::new(LocalizedFormats::EXPONENT, &e);
    // }
    // let mut result: BigInteger = BigInteger::ONE;
    // let mut k2p: BigInteger = k;
    // while !BigInteger::ZERO::equals(&e) {
    // if e.test_bit(0) {
    // result = result.multiply(k2p);
    // }
    // k2p = k2p.multiply(k2p);
    // e = e.shift_right(1);
    // }
    // return result;
    // }
    //
    //
    // Returns the <a
    // href="http://mathworld.wolfram.com/StirlingNumberoftheSecondKind.html">
    // Stirling number of the second kind</a>, "{@code S(n,k)}", the number of
    // ways of partitioning an {@code n}-element set into {@code k} non-empty
    // subsets.
    // <p>
    // The preconditions are {@code 0 <= k <= n } (otherwise
    // {@code NotPositiveException} is thrown)
    // </p>
    // @param n the size of the set
    // @param k the number of non-empty subsets
    // @return {@code S(n,k)}
    // @throws NotPositiveException if {@code k < 0}.
    // @throws NumberIsTooLargeException if {@code k > n}.
    // @throws MathArithmeticException if some overflow happens, typically for n exceeding 25 and
    // k between 20 and n-2 (S(n,n-1) is handled specifically and does not overflow)
    // @since 3.1
    // @deprecated use {@link CombinatoricsUtils#stirlingS2(int, int)}
    // /
    // pub fn  stirling_s2( n: i32,  k: i32) -> /*  throws NotPositiveException, NumberIsTooLargeException, MathArithmeticException */Result<i64, Rc<Exception>>    {
    // return CombinatoricsUtils::stirling_s2(n, k);
    // }
    //
}
