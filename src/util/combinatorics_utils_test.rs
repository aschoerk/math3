                  /*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// #[cfg(test)] 
// mod tests {
#![allow(unused_imports)]
	use util::fastmath;
	use std::f64;
	use std::i32;
	use std::rc::*;
	use std::cell::*;
	use java::exc::*;
	use assert;
	use util::precision::*;
	use random::bits_stream_generator::*;
	use random::well_generator::*;
	use util::test_utils::TestUtils;
	use util::combinatorics_utils::*;
	use std::collections::HashMap;
	use util::arithmetic_utils::*;
	                
 
/**
 * Test cases for the {@link CombinatoricsUtils} class.
 *
 */


    /** Verify that b(0,0) = 1 */
    #[test]
    pub fn  test0_choose0()   {
        assert_within_delta!(CombinatoricsUtils::binomial_coefficient_double(0, 0).unwrap(), 1.0, 0.0);
        assert_within_delta!(CombinatoricsUtils::binomial_coefficient_log(0, 0).unwrap(), 0.0, 0.0);
        assert_eq!(CombinatoricsUtils::binomial_coefficient(0, 0).unwrap(), 1);
    }
    
    fn test() {
    	let mut bc = Vec::new();
    	bc.push(HashMap::new());
    	bc[0].insert(1i32,2i64);
    	let cell = RefCell::new(bc);
    	cell.borrow_mut()[0].insert(2i32,3i64);
    }
    
    

    
     #[test]
    pub fn  test_binomial_coefficient()   {
         let bcoef5 = vec![1i64, 5, 10, 10, 5, 1, ]
        ;
         let bcoef6 = vec![1i64, 6, 15, 20, 15, 6, 1, ]
        ;
         {
             let mut i: usize = 0;
            while i < 6 {
                {
                    assert!(bcoef5[i] == CombinatoricsUtils::binomial_coefficient(5, i as u32).unwrap(),format!("5 choose {}", i));
                }
                i += 1;
             }
         }
    
    
         {
             let mut i: usize = 0;
            while i < 7 {
                {
                    assert!(bcoef6[i] == CombinatoricsUtils::binomial_coefficient(6, i as u32).unwrap(), format!("6 choose {}", i) );
                }
                i += 1;
             }
         }
    	let cache = &RefCell::new(Vec::<HashMap<i32,i64>>::new());

         {
             let mut n: u32 = 1;
            while n < 10 {
                {
                     {
                         let mut k: u32 = 0;
                        while k <= n {
                            {
                                assert!(binomial_coefficient(n, k, cache).unwrap() as i64 == CombinatoricsUtils::binomial_coefficient(n, k).unwrap(), format!("{} choose {}", n, k) );
                                assert_within_delta!(binomial_coefficient(n, k, cache).unwrap() as f64, CombinatoricsUtils::binomial_coefficient_double(n, k).unwrap(), f64::MIN,"{} choose {}", n, k);
                                assert_within_delta!(fastmath::F64::log(binomial_coefficient(n, k,cache).unwrap() as f64), CombinatoricsUtils::binomial_coefficient_log(n, k).unwrap() as f64, 10E-12, "{} choose {}", n, k);
                            }
                            k += 1;
                         }
                     }

                }
                n += 1;
             }
         }


         let n = vec![34u32, 66, 100, 1500, 1500, ]
        ;
         let k = vec![17u32, 33, 10, 1500 - 4, 4, ]
        ;
         {
             let mut i: usize = 0;
            while i < n.len() {
                {
                     let expected: i64 = binomial_coefficient(n[i], k[i], cache).unwrap();
                    assert!(expected == CombinatoricsUtils::binomial_coefficient(n[i], k[i]).unwrap() as i64,format!("{} choose {}", n[i], k[i]));
                    assert_within_delta!(expected as f64, CombinatoricsUtils::binomial_coefficient_double(n[i], k[i]).unwrap() as f64, 0.0, "{} choose {}", n[i], k[i]);
                    assert_within_delta!(fastmath::F64::log(expected as f64), CombinatoricsUtils::binomial_coefficient_log(n[i], k[i]).unwrap() as f64, 0.0,"log({} choose {})", n[i], k[i]);
                }
                i += 1;
             }
         }

    }
    
    
    
     #[test]
    pub fn  test_binomial_coefficient_fail()   {
       
            assert!(CombinatoricsUtils::binomial_coefficient(4, 5).is_err());
            assert!(CombinatoricsUtils::binomial_coefficient_double(4, 5).is_err());
          
            assert!(CombinatoricsUtils::binomial_coefficient_log(4, 5).is_err());
            
            assert!(CombinatoricsUtils::binomial_coefficient(67, 30).is_err());
            assert!(CombinatoricsUtils::binomial_coefficient(67, 34).is_err());
           
	         let x: f64 = CombinatoricsUtils::binomial_coefficient_double(1030, 515).unwrap();
	        assert!(f64::is_infinite(x), "expecting infinite binomial coefficient");
    }
    
    /**
     * Tests correctness for large n and sharpness of upper bound in API doc
     * JIRA: MATH-241
     */
    #[test]
    pub fn  test_binomial_coefficient_large()  {
         	let cache = &RefCell::new(Vec::<HashMap<i32,i64>>::new());
        // This tests all legal and illegal values for n <= 200.
         {
             let mut n: u32 = 0;
            while n <= 200 {
                {
                     {
                         let mut k: u32 = 0;
                        while k <= n {
                            {
                                 let our_result = CombinatoricsUtils::binomial_coefficient(n, k);
                                 let exact_result = binomial_coefficient(n, k, cache);
                                 let our: i64;
                                 let exact: i64;
                                                                
                                if our_result.is_err() {
                                	assert!(exact_result.is_err());
                                	assert!(n > 66);
                                } else {
                                	exact = exact_result.unwrap();
                                	our = our_result.unwrap();
	                                assert!(exact ==  our, format!("{} choose {}", n, k));
	                                if exact > 1 {
	                                    assert_within_delta!(1.0, CombinatoricsUtils::binomial_coefficient_double(n, k).unwrap() / exact as f64, 1e-10, "{} choose {}", n, k);
	                                    assert_within_delta!(1.0, CombinatoricsUtils::binomial_coefficient_log(n, k).unwrap() / fastmath::F64::log(exact as f64), 1e-10,"{} choose {}", n, k);
	                                }
                                }
                            }
                            k += 1;
                         }
                     }

                }
                n += 1;
             }
         }

         let mut our_result = CombinatoricsUtils::binomial_coefficient(300, 3).unwrap();
         let mut exact_result  = binomial_coefficient(300, 3, cache).unwrap();
        assert_eq!(exact_result, our_result);
        our_result = CombinatoricsUtils::binomial_coefficient(700, 697).unwrap();
        exact_result = binomial_coefficient(700, 697, cache).unwrap();
        assert_eq!(exact_result, our_result);
        // This one should throw
        assert!(CombinatoricsUtils::binomial_coefficient(700, 300).is_err());

        let n: i32 = 10000;
        our_result = CombinatoricsUtils::binomial_coefficient(n as u32, 3).unwrap();
        exact_result = binomial_coefficient(n as u32, 3, cache).unwrap();
        assert_eq!(exact_result, our_result);
        assert_within_delta!(1.0, CombinatoricsUtils::binomial_coefficient_double(n as u32, 3).unwrap() / (exact_result as f64), 1e-10);
        assert_within_delta!(1.0, CombinatoricsUtils::binomial_coefficient_log(n as u32, 3).unwrap() / fastmath::F64::log(exact_result as f64), 1e-10);
    }
    
    #[test]
    pub fn  test_factorial()   {
         {
             let mut i: i32 = 1;
            while i < 21 {
                {
                    assert!(factorial(i) == CombinatoricsUtils::factorial(i).unwrap(), format!("{}! ", i));
                    assert_within_delta!(factorial(i) as f64, CombinatoricsUtils::factorial_double(i).unwrap(), f64::MIN, "{}! ", i);
                    assert_within_delta!(fastmath::F64::log(factorial(i) as f64), CombinatoricsUtils::factorial_log(i).unwrap(), 10E-12, "{}! ", i);
                }
                i += 1;
             }
         }

        assert!(1 == CombinatoricsUtils::factorial(0).unwrap(),"0");
        assert_within_delta!(1.0, CombinatoricsUtils::factorial_double(0).unwrap(), 1E-14,"0");
        assert_within_delta!(0.0, CombinatoricsUtils::factorial_log(0).unwrap(), 1E-14,"0");
    }
    
     #[test]
    pub fn  test_factorial_fail()   {
        assert!(CombinatoricsUtils::factorial(-1).is_err());
            assert!(CombinatoricsUtils::factorial_double(-1).is_err());
            assert!(CombinatoricsUtils::factorial_log(-1).is_err());
            assert!(CombinatoricsUtils::factorial(21).is_err());
        assert!(f64::is_infinite(CombinatoricsUtils::factorial_double(171).unwrap()),"expecting infinite factorial value");
    }
    
    #[test]
    pub fn  test_stirling_s2()   {
        assert_eq!(1, CombinatoricsUtils::stirling_s2(0, 0).unwrap());
         {
             let mut n: u32 = 1;
            while n < 30 {
                {
                    assert_eq!(0, CombinatoricsUtils::stirling_s2(n, 0).unwrap());
                    assert_eq!(1, CombinatoricsUtils::stirling_s2(n, 1).unwrap());
                    if n > 2 {
                        assert_eq!((1 << (n - 1)) - 1, CombinatoricsUtils::stirling_s2(n, 2).unwrap());
                        assert_eq!(CombinatoricsUtils::binomial_coefficient(n, 2).unwrap(), CombinatoricsUtils::stirling_s2(n, n - 1).unwrap());
                    }
                    assert_eq!(1, CombinatoricsUtils::stirling_s2(n, n).unwrap());
                }
                n += 1;
             }
         }

        assert_eq!(536870911, CombinatoricsUtils::stirling_s2(30, 2).unwrap());
        assert_eq!(576460752303423487, CombinatoricsUtils::stirling_s2(60, 2).unwrap());
        assert_eq!(25, CombinatoricsUtils::stirling_s2(5, 3).unwrap());
        assert_eq!(90, CombinatoricsUtils::stirling_s2(6, 3).unwrap());
        assert_eq!(65, CombinatoricsUtils::stirling_s2(6, 4).unwrap());
        assert_eq!(301, CombinatoricsUtils::stirling_s2(7, 3).unwrap());
        assert_eq!(350, CombinatoricsUtils::stirling_s2(7, 4).unwrap());
        assert_eq!(140, CombinatoricsUtils::stirling_s2(7, 5).unwrap());
        assert_eq!(966, CombinatoricsUtils::stirling_s2(8, 3).unwrap());
        assert_eq!(1701, CombinatoricsUtils::stirling_s2(8, 4).unwrap());
        assert_eq!(1050, CombinatoricsUtils::stirling_s2(8, 5).unwrap());
        assert_eq!(266, CombinatoricsUtils::stirling_s2(8, 6).unwrap());
        assert_eq!(3025, CombinatoricsUtils::stirling_s2(9, 3).unwrap());
        assert_eq!(7770, CombinatoricsUtils::stirling_s2(9, 4).unwrap());
        assert_eq!(6951, CombinatoricsUtils::stirling_s2(9, 5).unwrap());
        assert_eq!(2646, CombinatoricsUtils::stirling_s2(9, 6).unwrap());
        assert_eq!(462, CombinatoricsUtils::stirling_s2(9, 7).unwrap());
        assert_eq!(9330, CombinatoricsUtils::stirling_s2(10, 3).unwrap());
        assert_eq!(34105, CombinatoricsUtils::stirling_s2(10, 4).unwrap());
        assert_eq!(42525, CombinatoricsUtils::stirling_s2(10, 5).unwrap());
        assert_eq!(22827, CombinatoricsUtils::stirling_s2(10, 6).unwrap());
        assert_eq!(5880, CombinatoricsUtils::stirling_s2(10, 7).unwrap());
        assert_eq!(750, CombinatoricsUtils::stirling_s2(10, 8).unwrap());
    }
    

    #[test]
    pub fn  test_stirling_s2_large_k()   {
        assert!(CombinatoricsUtils::stirling_s2(3, 4).is_err());
    }

    #[test]
    pub fn  test_stirling_s2_overflow()   {
        assert!(CombinatoricsUtils::stirling_s2(26, 9).is_err());
    }
    
    

    #[test]
    pub fn  test_check_binomial2()   {
        // k > n
        assert!(CombinatoricsUtils::check_binomial(4, 5).is_err());
    }

    #[test]
    pub fn  test_check_binomial3()   {
        // OK (no exception thrown)
        assert!(CombinatoricsUtils::check_binomial(5, 4).is_ok());
    }
    
            	#[allow(unused_must_use)]
        fn  binomial_coefficient( n: u32,  k: u32, binomial_cache: &RefCell<Vec<HashMap<i32,i64>>>) -> /*  throws MathArithmeticException */Result<i64, Rc<Exception>>   {
    	{
    		let cache: Ref<Vec<HashMap<i32,i64>>> = binomial_cache.borrow(); 
	        if cache.len() > n as usize {
	        	// let map_option_ref = Ref::map(cache_ref, |cache| &(cache[n as usize].get(&(k as i32))));
	        	// let map_ref = Ref::map(map_option_ref, |map_option| &(map_option.unwrap().clone()));
	        	// .unwrap_or(HashMap::new()));
	        	let y = *cache[n as usize].get(&(k as i32)).unwrap_or(&-1i64);
	        	 
	            if y > 0 {
	                return Ok(y);
	            }
	        }
    	}
         let result: i64;
        if (n == k) || (k == 0) {
            result = 1;
        } else if (k == 1) || (k == n - 1) {
            result = n as i64;
        } else {
        	let ni32 = n as i32;
            // Reduce stack depth for larger values of n
            if (k as i32) < (ni32 - 100) {
                binomial_coefficient(n - 100, k, binomial_cache);
            }
            if k > 100 {
                binomial_coefficient(n - 100, k - 100, binomial_cache);
            }
            result = try!(I64::add_and_check(try!(binomial_coefficient(n - 1, k - 1, binomial_cache)), try!(binomial_coefficient(n - 1, k, binomial_cache))));
        }
        if result == -1 {
            return Err(Exc::new("MathArithmeticException"));
        };
         {
         	let mut cache: RefMut<Vec<HashMap<i32,i64>>> = binomial_cache.borrow_mut(); 
            let mut i: usize = cache.len();
            while i < n as usize + 1 {
                {
                	let newmap: HashMap<i32,i64> = HashMap::new();
                    cache.push(newmap);
                }
                i += 1;
             }
            
            cache[n as usize].insert(k as i32, result);
            
         }
	       return Ok(result);

    }

    /**
     * Exact direct multiplication implementation to test against
     */
    fn  factorial( n: i32) -> i64  {
         let mut result: i64 = 1;
         {
             let mut i: i32 = 2;
            while i <= n {
                {
                    result *= i as i64;
                }
                i += 1;
             }
         }

        return result;
    }
// }


                
                
