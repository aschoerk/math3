
// Licensed to the Apache Software Foundation (ASF) under one or more
// contributor license agreements.  See the NOTICE file distributed with
// this work for additional information regarding copyright ownership.
// The ASF licenses this file to You under the Apache License, Version 2.0
// (the "License"); you may not use this file except in compliance with
// the License.  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//


/**
 * Utility to create <a href="http://en.wikipedia.org/wiki/Combination">
 * combinations</a> {@code (n, k)} of {@code k} elements in a set of
 * {@code n} elements.
 *
 * @since 3.3
 */

use std::iter::*;
use std::rc::*;
use java::exc::*;
use util::combinatorics_utils::*;
use util::arithmetic_utils;

enum IterationOrder {
    /** Lexicographic order. */
    LEXICOGRAPHIC,
}

// #[derive(Iterable<Vec<i32>>)]
pub struct Combinations {
    n: u32,

    k: u32,

    /** Iteration order. */
    iteration_order: IterationOrder,
}

impl Combinations {
    /**
     * Describes the type of iteration performed by the
     * {@link #iterator() iterator}.
     */
    /**
     * Creates an instance whose range is the k-element subsets of
     * {0, ..., n - 1} represented as {@code int[]} arrays.
     * <p>
     * The iteration order is lexicographic: the arrays returned by the
     * {@link #iterator() iterator} are sorted in descending order and
     * they are visited in lexicographic order with significance from
     * right to left.
     * For example, {@code new Combinations(4, 2).iterator()} returns
     * an iterator that will generate the following sequence of arrays
     * on successive calls to
     * {@code next()}:<br/>
     * {@code [0, 1], [0, 2], [1, 2], [0, 3], [1, 3], [2, 3]}
     * </p>
     * If {@code k == 0} an iterator containing an empty array is returned;
     * if {@code k == n} an iterator containing [0, ..., n - 1] is returned.
     *
     * @param n Size of the set from which subsets are selected.
     * @param k Size of the subsets to be enumerated.
     * @throws org.apache.commons.math3.exception.NotPositiveException if {@code n < 0}.
     * @throws org.apache.commons.math3.exception.NumberIsTooLargeException if {@code k > n}.
     */
    pub fn new(n: u32, k: u32) -> Result<Combinations, Rc<Exception>> {
        Combinations::new_order(n, k, IterationOrder::LEXICOGRAPHIC)
    }

    /**
     * Creates an instance whose range is the k-element subsets of
     * {0, ..., n - 1} represented as {@code int[]} arrays.
     * <p>
     * If the {@code iterationOrder} argument is set to
     * {@link IterationOrder#LEXICOGRAPHIC}, the arrays returned by the
     * {@link #iterator() iterator} are sorted in descending order and
     * they are visited in lexicographic order with significance from
     * right to left.
     * For example, {@code new Combinations(4, 2).iterator()} returns
     * an iterator that will generate the following sequence of arrays
     * on successive calls to
     * {@code next()}:<br/>
     * {@code [0, 1], [0, 2], [1, 2], [0, 3], [1, 3], [2, 3]}
     * </p>
     * If {@code k == 0} an iterator containing an empty array is returned;
     * if {@code k == n} an iterator containing [0, ..., n - 1] is returned.
     *
     * @param n Size of the set from which subsets are selected.
     * @param k Size of the subsets to be enumerated.
     * @param iterationOrder Specifies the {@link #iterator() iteration order}.
     * @throws org.apache.commons.math3.exception.NotPositiveException if {@code n < 0}.
     * @throws org.apache.commons.math3.exception.NumberIsTooLargeException if {@code k > n}.
     */
    fn new_order(n: u32,
                 k: u32,
                 iteration_order: IterationOrder)
                 -> Result<Combinations, Rc<Exception>> {
        try!(CombinatoricsUtils::check_binomial(n, k));
        Ok(Combinations {
            n: n,
            k: k,
            iteration_order: iteration_order,
        })

    }

    /**
     * Gets the size of the set from which combinations are drawn.
     *
     * @return the size of the universe.
     */
    pub fn get_n(&self) -> u32 {
        return self.n;
    }

    /**
     * Gets the number of elements in each combination.
     *
     * @return the size of the subsets to be enumerated.
     */
    pub fn get_k(&self) -> u32 {
        return self.k;
    }

    /** {@inheritDoc} */
    pub fn iter(&self) -> Result<LexicographicIterator, Rc<Exception>> {
        if self.k == 0 || self.k == self.n {

            return Ok(LexicographicIterator::new_singleton({
                let mut v = Vec::new();
                for tmp in 0u32..self.k {
                    v.push(tmp);
                }
                v
            }));
        }
        return Ok(LexicographicIterator::new(self.n, self.k));
    }

    // Defines a lexicographic ordering of combinations.
    // The returned comparator allows to compare any two combinations
    // that can be produced by this instance's {@link #iterator() iterator}.
    // Its {@code compare(int[],int[])} method will throw exceptions if
    // passed combinations that are inconsistent with this instance:
    // <ul>
    //  <li>{@code DimensionMismatchException} if the array lengths are not
    //      equal to {@code k},</li>
    //  <li>{@code OutOfRangeException} if an element of the array is not
    //      within the interval [0, {@code n}).</li>
    // </ul>
    // @return a lexicographic comparator.
    // /
    // pub fn  comparator(&self) -> Comparator<Vec<i32>>  {
    // return LexicographicComparator::new(self.n, self.k);
    // }
    //
}


/**
 * Lexicographic combinations iterator.
 * <p>
 * Implementation follows Algorithm T in <i>The Art of Computer Programming</i>
 * Internet Draft (PRE-FASCICLE 3A), "A Draft of Section 7.2.1.3 Generating All
 * Combinations</a>, D. Knuth, 2004.</p>
 * <p>
 * The degenerate cases {@code k == 0} and {@code k == n} are NOT handled by this
 * implementation.  If constructor arguments satisfy {@code k == 0}
 * or {@code k >= n}, no exception is generated, but the iterator is empty.
 * </p>
 *
 */

pub struct LexicographicIterator {
    /** Size of subsets returned by the iterator */
    k: u32,

    /**
     * c[1], ..., c[k] stores the next combination; c[k + 1], c[k + 2] are
     * sentinels.
     * <p>
     * Note that c[0] is "wasted" but this makes it a little easier to
     * follow the code.
     * </p>
     */
    c: Vec<u32>,

    /** Return value for {@link #hasNext()} */
    more: bool,

    is_singleton: bool,

    /** Marker: smallest index such that c[j + 1] > j */
    j: i32,
}

impl Iterator for LexicographicIterator {
    type Item = Vec<u32>;

    fn next(&mut self) -> Option<Vec<u32>> {
        if !self.more {
            return None;
            // throw NoSuchElementException::new();
        }
        if self.is_singleton {
            self.more = false;
            return Some(self.c.clone());
        }
        // Copy return value (prepared by last activation)
        let mut ret = Vec::new();
        for i in 1..(1 + self.k) {
            ret.push(self.c[i as usize]);
        }
        // Prepare next iteration
        // T2 and T6 loop
        let mut x: u32 = 0;
        if self.j > 0 {
            x = self.j as u32;
            self.c[self.j as usize] = x;
            self.j -= 1;
            return Some(ret);
        }
        // T3
        if self.c[1] + 1 < self.c[2] {
            self.c[1] += 1;
            return Some(ret);
        } else {
            self.j = 2;
        }
        // T4
        let mut step_done: bool = false;
        while !step_done {
            self.c[self.j as usize - 1] = (self.j - 2) as u32;
            x = self.c[self.j as usize] + 1;
            if x == self.c[self.j as usize + 1] {
                self.j += 1;
            } else {
                step_done = true;
            }
        }
        // T5
        if self.j > self.k as i32 {
            self.more = false;
            return Some(ret);
        }
        // T6
        self.c[self.j as usize] = x;
        self.j -= 1;
        return Some(ret);
    }
}

impl LexicographicIterator {
    fn new_singleton(c: Vec<u32>) -> LexicographicIterator {
        LexicographicIterator {
            k: c.len() as u32,
            c: c,
            j: -1,
            is_singleton: true,
            more: false,
        }

    }

    /**
     * Construct a CombinationIterator to enumerate k-sets from n.
     * <p>
     * NOTE: If {@code k === 0} or {@code k >= n}, the Iterator will be empty
     * (that is, {@link #hasNext()} will return {@code false} immediately.
     * </p>
     *
     * @param n size of the set from which subsets are enumerated
     * @param k size of the subsets to enumerate
     */
    fn new(n: u32, k: u32) -> LexicographicIterator {

        let mut c = vec![0u32; k as usize + 3];
        if k == 0 || k >= n {
            LexicographicIterator {
                k: k,
                c: c,
                j: k as i32,
                is_singleton: false,
                more: false,
            }
        } else {
            // Initialize c to start with lexicographically first k-set

            let mut i: usize = 1;
            while i <= k as usize {
                {
                    c[i] = i as u32 - 1;
                }
                i += 1;
            }


            // Initialize sentinels
            c[k as usize + 1] = n;
            c[k as usize + 2] = 0;


            LexicographicIterator {
                k: k,
                c: c,
                j: k as i32, // Set up invariant: j is smallest index such that c[j + 1] > j
                is_singleton: false,
                more: true,
            }
        }
    }
}

/**
     * Defines the lexicographic ordering of combinations, using
     * the {@link #lexNorm(int[])} method.
     */
/** Serializable version identifier. */
// #[derive(Comparator<Vec<i32>>, Serializable)]
struct LexicographicComparator {
    /** Size of the set from which combinations are drawn. */
    n: i32,

    /** Number of elements in each combination. */
    k: i32,
}


impl LexicographicComparator {
    /**
     * @param n Size of the set from which subsets are selected.
     * @param k Size of the subsets to be enumerated.
     */
    fn new(n: i32, k: i32) -> LexicographicComparator {
        LexicographicComparator { n: n, k: k }
    }

    /**
     * {@inheritDoc}
     *
     * @throws DimensionMismatchException if the array lengths are not
     * equal to {@code k}.
     * @throws OutOfRangeException if an element of the array is not
     * within the interval [0, {@code n}).
     */
    pub fn compare(&self, c1: &Vec<i32>, c2: &Vec<i32>) -> Result<i32, Rc<Exception>> {
        if c1.len() != self.k as usize {
            return Err(Exc::new_msg("DimensionMismatchException",
                                    format!("{} {}",c1.len(), self.k)));
        }
        if c2.len() != self.k as usize {
            return Err(Exc::new_msg("DimensionMismatchException",
                                    format!("{} {}",c2.len(), self.k)));
        }
        // Method "lexNorm" works with ordered arrays.
        let mut c1s = c1.clone();
        c1s.sort();
        let mut c2s = c2.clone();
        c2s.sort();
        let v1: i64 = try!(self.lex_norm(&c1s));
        let v2: i64 = try!(self.lex_norm(&c2s));
        if v1 < v2 {
            return Ok(-1);
        } else if v1 > v2 {
            return Ok(1);
        } else {
            return Ok(0);
        }
    }

    /**
     * Computes the value (in base 10) represented by the digit
     * (interpreted in base {@code n}) in the input array in reverse
     * order.
     * For example if {@code c} is {@code {3, 2, 1}}, and {@code n}
     * is 3, the method will return 18.
     *
     * @param c Input array.
     * @return the lexicographic norm.
     * @throws OutOfRangeException if an element of the array is not
     * within the interval [0, {@code n}).
     */
    fn lex_norm(&self, c: &Vec<i32>) -> Result<i64, Rc<Exception>> {
        let mut ret: i64 = 0;
        {
            let mut i: usize = 0;
            while i < c.len() {
                {
                    let digit: i32 = c[i];
                    if digit < 0 || digit >= self.n {
                        return Err(Exc::new_msg("OutOfRangeException",
                                                format!("{} {} {}",digit, 0, self.n - 1)));
                    }
                    let tmp = try!(arithmetic_utils::I64::pow(self.n as i64, i as i64));
                    ret += c[i] as i64 * tmp;
                }
                i += 1;
            }
        }

        return Ok(ret);
    }
}
