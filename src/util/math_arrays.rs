
// Licensed to the Apache Software Foundation (ASF) under one or more
// contributor license agreements.  See the NOTICE file distributed with
// this work for additional information regarding copyright ownership.
// The ASF licenses this file to You under the Apache License, Version 2.0
// (the "License"); you may not use this file except in compliance with
// the License.  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
use std::rc::*;
use std::cell::*;
use std::cmp::*;
use std::f64;
use base::*;
use java::exc::*;
use util::fastmath::F64;
use util::fastmath::I32;
use util::rsutils::*;
use util::precision;
use util::wrappers::*;
use random::bits_stream_generator::*;
use random::well_generator::*;
use distribution::integer_distribution::*;
use distribution::uniform_integer_distribution::*;
use std::collections::BTreeSet;
/**
 * Arrays utilities.
 *
 * @since 3.0
 */
pub struct MathArrays {
}

/**
 * Real-valued function that operate on an array or a part of it.
 * @since 3.1
 */
pub trait Function {
    /**
     * Operates on an entire array.
     *
     * @param array Array to operate on.
     * @return the result of the operation.
     */
    fn evaluate(&self, array: &Vec<f64>) -> f64;

    /**
     * @param array Array to operate on.
     * @param startIndex Index of the first element to take into account.
     * @param numElements Number of elements to take into account.
     * @return the result of the operation.
     */
    fn evaluate_part(&self, array: &Vec<f64>, start_index: i32, num_elements: i32) -> f64;
}


/**
     * Specification of ordering direction.
     */
#[derive(Debug)]
pub enum OrderDirection {
    /** Constant for increasing direction. */
    INCREASING,
    /** Constant for decreasing direction. */
    DECREASING,
}

/**
     * Specification for indicating that some operation applies
     * before or after a given index.
     */
#[derive(Debug)]
pub enum Position {
    /** Designates the beginning of the array (near index 0). */
    HEAD,
    /** Designates the end of the array. */
    TAIL,
}

/**
     * A helper data structure holding a double and an integer value.
     */
struct PairDoubleInteger {
    /** Key */
    key: f64,

    /** Value */
    value: i32,
}

impl PairDoubleInteger {
    /**
         * @param key Key.
         * @param value Value.
         */
    fn new(key: f64, value: i32) -> PairDoubleInteger {
        PairDoubleInteger {
            key: key,
            value: value,
        }
    }

    /** @return the key. */
    pub fn get_key(&self) -> f64 {
        return self.key;
    }

    /** @return the value. */
    pub fn get_value(&self) -> i32 {
        return self.value;
    }
}


impl MathArrays {
    /**
     * Create a copy of an array scaled by a value.
     *
     * @param arr Array to scale.
     * @param val Scalar.
     * @return scaled copy of array with each entry multiplied by val.
     * @since 3.2
     */
    pub fn scale(val: f64, arr: &Vec<f64>) -> Vec<f64> {
        let mut new_arr = Vec::new();
        {
            let mut i: usize = 0;
            while i < arr.len() {
                {
                    new_arr.push(arr[i] * val);
                }
                i += 1;
            }
        }

        return new_arr;
    }

    /**
     * <p>Multiply each element of an array by a value.</p>
     *
     * <p>The array is modified in place (no copy is created).</p>
     *
     * @param arr Array to scale
     * @param val Scalar
     * @since 3.2
     */
    pub fn scale_in_place(val: f64, arr: &mut Vec<f64>) {
        {
            let mut i: usize = 0;
            while i < arr.len() {
                {
                    arr[i] *= val;
                }
                i += 1;
            }
        }

    }

    /**
     * Creates an array whose contents will be the element-by-element
     * addition of the arguments.
     *
     * @param a First term of the addition.
     * @param b Second term of the addition.
     * @return a new array {@code r} where {@code r[i] = a[i] + b[i]}.
     * @throws DimensionMismatchException if the array lengths differ.
     * @since 3.1
     */
    pub fn ebe_add(a: &Vec<f64>, b: &Vec<f64>) -> Result<Vec<f64>, Rc<Exception>> {
        let res = MathArrays::check_equal_length(&a, &b);
        try!(res);
        let mut result: Vec<f64> = a.clone();
        {
            let mut i: usize = 0;
            while i < a.len() {
                {
                    result[i] += b[i];
                }
                i += 1;
            }
        }

        return Ok(result);
    }

    /**
     * Creates an array whose contents will be the element-by-element
     * subtraction of the second argument from the first.
     *
     * @param a First term.
     * @param b Element to be subtracted.
     * @return a new array {@code r} where {@code r[i] = a[i] - b[i]}.
     * @throws DimensionMismatchException if the array lengths differ.
     * @since 3.1
     */
    pub fn ebe_subtract(a: &Vec<f64>, b: &Vec<f64>) -> Result<Vec<f64>, Rc<Exception>> {
        try!(MathArrays::check_equal_length(&a, &b));
        let mut result: Vec<f64> = a.clone();
        {
            let mut i: usize = 0;
            while i < a.len() {
                {
                    result[i] -= b[i];
                }
                i += 1;
            }
        }

        return Ok(result);
    }

    /**
     * Creates an array whose contents will be the element-by-element
     * multiplication of the arguments.
     *
     * @param a First factor of the multiplication.
     * @param b Second factor of the multiplication.
     * @return a new array {@code r} where {@code r[i] = a[i] * b[i]}.
     * @throws DimensionMismatchException if the array lengths differ.
     * @since 3.1
     */
    pub fn ebe_multiply(a: &Vec<f64>, b: &Vec<f64>) -> Result<Vec<f64>, Rc<Exception>> {
        try!(MathArrays::check_equal_length(&a, &b));
        let mut result: Vec<f64> = a.clone();
        {
            let mut i: usize = 0;
            while i < a.len() {
                {
                    result[i] *= b[i];
                }
                i += 1;
            }
        }

        return Ok(result);
    }

    /**
     * Creates an array whose contents will be the element-by-element
     * division of the first argument by the second.
     *
     * @param a Numerator of the division.
     * @param b Denominator of the division.
     * @return a new array {@code r} where {@code r[i] = a[i] / b[i]}.
     * @throws DimensionMismatchException if the array lengths differ.
     * @since 3.1
     */
    pub fn ebe_divide(a: &Vec<f64>, b: &Vec<f64>) -> Result<Vec<f64>, Rc<Exception>> {
        try!(MathArrays::check_equal_length(&a, &b));
        let mut result: Vec<f64> = a.clone();
        {
            let mut i: usize = 0;
            while i < a.len() {
                {
                    result[i] /= b[i];
                }
                i += 1;
            }
        }

        return Ok(result);
    }

    /**
     * Calculates the L<sub>1</sub> (sum of abs) distance between two points.
     *
     * @param p1 the first point
     * @param p2 the second point
     * @return the L<sub>1</sub> distance between the two points
     * @throws DimensionMismatchException if the array lengths differ.
     */
    pub fn distance1(p1: &Vec<f64>, p2: &Vec<f64>) -> Result<f64, Rc<Exception>> {
        try!(MathArrays::check_equal_length(&p1, &p2));
        let mut sum: f64 = 0.0;
        {
            let mut i: usize = 0;
            while i < p1.len() {
                {
                    sum += F64::abs(p1[i] - p2[i]);
                }
                i += 1;
            }
        }

        return Ok(sum);
    }

    /**
     * Calculates the L<sub>1</sub> (sum of abs) distance between two points.
     *
     * @param p1 the first point
     * @param p2 the second point
     * @return the L<sub>1</sub> distance between the two points
     * @throws DimensionMismatchException if the array lengths differ.
     */
    pub fn distance1_i32(p1: &Vec<i32>, p2: &Vec<i32>) -> Result<i32, Rc<Exception>> {
        try!(MathArrays::check_equal_length(&p1, &p2));
        let mut sum: i32 = 0;
        {
            let mut i: usize = 0;
            while i < p1.len() {
                {
                    sum += I32::abs(p1[i] - p2[i]);
                }
                i += 1;
            }
        }

        return Ok(sum);
    }

    /**
     * Calculates the L<sub>2</sub> (Euclidean) distance between two points.
     *
     * @param p1 the first point
     * @param p2 the second point
     * @return the L<sub>2</sub> distance between the two points
     * @throws DimensionMismatchException if the array lengths differ.
     */
    pub fn distance(p1: &Vec<f64>, p2: &Vec<f64>) -> Result<f64, Rc<Exception>> {
        try!(MathArrays::check_equal_length(&p1, &p2));
        let mut sum: f64 = 0.0;
        {
            let mut i: usize = 0;
            while i < p1.len() {
                {
                    let dp: f64 = p1[i] - p2[i];
                    sum += dp * dp;
                }
                i += 1;
            }
        }

        return Ok(F64::sqrt(sum));
    }

    /**
     * Calculates the L<sub>2</sub> (Euclidean) distance between two points.
     *
     * @param p1 the first point
     * @param p2 the second point
     * @return the L<sub>2</sub> distance between the two points
     * @throws DimensionMismatchException if the array lengths differ.
     */
    pub fn distance_i32(p1: &Vec<i32>, p2: &Vec<i32>) -> Result<f64, Rc<Exception>> {
        try!(MathArrays::check_equal_length(&p1, &p2));
        let mut sum: f64 = 0.0;
        {
            let mut i: usize = 0;
            while i < p1.len() {
                {
                    let dp: f64 = (p1[i] - p2[i]) as f64;
                    sum += dp * dp;
                }
                i += 1;
            }
        }

        return Ok(F64::sqrt(sum));
    }

    /**
     * Calculates the L<sub>∞</sub> (max of abs) distance between two points.
     *
     * @param p1 the first point
     * @param p2 the second point
     * @return the L<sub>∞</sub> distance between the two points
     * @throws DimensionMismatchException if the array lengths differ.
     */
    pub fn distance_inf(p1: &Vec<f64>, p2: &Vec<f64>) -> Result<f64, Rc<Exception>> {
        try!(MathArrays::check_equal_length(&p1, &p2));
        let mut max: f64 = 0.0;
        {
            let mut i: usize = 0;
            while i < p1.len() {
                {
                    max = F64::max(max, F64::abs(p1[i] - p2[i]));
                }
                i += 1;
            }
        }

        return Ok(max);
    }

    /**
     * Calculates the L<sub>∞</sub> (max of abs) distance between two points.
     *
     * @param p1 the first point
     * @param p2 the second point
     * @return the L<sub>∞</sub> distance between the two points
     * @throws DimensionMismatchException if the array lengths differ.
     */
    pub fn distance_inf_i32(p1: &Vec<i32>, p2: &Vec<i32>) -> Result<i32, Rc<Exception>> {
        try!(MathArrays::check_equal_length(&p1, &p2));
        let mut max: i32 = 0;
        {
            let mut i: usize = 0;
            while i < p1.len() {
                {
                    max = I32::max(max, I32::abs(p1[i] - p2[i]));
                }
                i += 1;
            }
        }

        return Ok(max);
    }


    /**
     * Check that an array is monotonically increasing or decreasing.
     *
     * @param <T> the type of the elements in the specified array
     * @param val Values.
     * @param dir Ordering direction.
     * @param strict Whether the order should be strict.
     * @return {@code true} if sorted, {@code false} otherwise.
     */
    pub fn is_monotonic<T>(val: &Vec<T>, dir: OrderDirection, strict: bool) -> Option<bool>
        where T: PartialOrd
    {
        let mut previous: &T = &val[0];
        let max: usize = val.len();
        {
            let mut i: usize = 1;
            while i < max {
                {
                    match dir {
                        OrderDirection::INCREASING => {
                            let comp_option = previous.partial_cmp(&val[i]);
                            match comp_option {
                                Some(comp) => {
                                    if strict {
                                        if comp == Ordering::Equal || comp == Ordering::Greater {
                                            return Some(false);
                                        }
                                    } else {
                                        if comp == Ordering::Greater {
                                            return Some(false);
                                        }
                                    }
                                }
                                None => return None,
                            }
                        }
                        OrderDirection::DECREASING => {
                            let comp_option = val[i].partial_cmp(&previous);
                            match comp_option {
                                Some(comp) => {
                                    if strict {
                                        if comp == Ordering::Equal || comp == Ordering::Greater {
                                            return Some(false);
                                        }
                                    } else {
                                        if comp == Ordering::Greater {
                                            return Some(false);
                                        }
                                    }
                                }
                                None => return None,
                            }
                        }

                    }
                    previous = &val[i];
                }
                i += 1;
            }
        }

        return Some(true);
    }

    /**
     * Check that an array is monotonically increasing or decreasing.
     *
     * @param val Values.
     * @param dir Ordering direction.
     * @param strict Whether the order should be strict.
     * @return {@code true} if sorted, {@code false} otherwise.
     */
    pub fn is_monotonic_dir_strict(val: &Vec<f64>,
                                   dir: OrderDirection,
                                   strict: bool)
                                   -> Result<bool, Rc<Exception>> {
        return MathArrays::check_order_abort(&val, dir, strict, false);
    }

    pub fn check_equal_length<T>(a: &Vec<T>, b: &Vec<T>) -> Result<bool, Rc<Exception>> {
        if a.len() == b.len() {
            return Ok(true);
        } else {
            Err(Exc::new_msg("DimensionMismatchException",
                             format!("{} {}", a.len(), b.len())))
            // throw DimensionMismatchException::new(a.len(), b.len());
        }
    }



    /**
     * Check that the given array is sorted.
     *
     * @param val Values.
     * @param dir Ordering direction.
     * @param strict Whether the order should be strict.
     * @param abort Whether to throw an exception if the check fails.
     * @return {@code true} if the array is sorted.
     * @throws NonMonotonicSequenceException if the array is not sorted
     * and {@code abort} is {@code true}.
     */
    pub fn check_order_abort(val: &Vec<f64>,
                             dir: OrderDirection,
                             strict: bool,
                             abort: bool)
                             -> Result<bool, Rc<Exception>> {
        let mut previous: f64 = val[0];
        let max: usize = val.len();
        let mut index: usize = 1;
        'ITEM: while index < max {
            {
                match dir {
                    OrderDirection::INCREASING => {
                        if strict {
                            if val[index] <= previous {
                                break 'ITEM;
                            }
                        } else {
                            if val[index] < previous {
                                break 'ITEM;
                            }
                        }
                    }
                    OrderDirection::DECREASING => {
                        if strict {
                            if val[index] >= previous {
                                break 'ITEM;
                            }
                        } else {
                            if val[index] > previous {
                                break 'ITEM;
                            }
                        }
                    }

                }
                previous = val[index];
            }
            index += 1;
        }


        if index == max {
            // Loop completed.
            return Ok(true);
        }
        // Loop early exit means wrong ordering.
        if abort {
            return Err(Exc::new_msg("NonMonotonicSequenceException",
                                    format!("value: {}, previous: {}, index:{}, dir: {:?}, \
                                             strict: {}",
                                            val[index],
                                            previous,
                                            index,
                                            dir,
                                            strict)));
            // throw NonMonotonicSequenceException::new(val[index], previous, index, dir, strict);
        } else {
            return Ok(false);
        }
    }

    /**
     * Check that the given array is sorted.
     *
     * @param val Values.
     * @param dir Ordering direction.
     * @param strict Whether the order should be strict.
     * @throws NonMonotonicSequenceException if the array is not sorted.
     * @since 2.2
     */
    pub fn check_order(val: &Vec<f64>,
                       dir: OrderDirection,
                       strict: bool)
                       -> Result<bool, Rc<Exception>> {
        MathArrays::check_order_abort(&val, dir, strict, true)
    }

    /**
     * Check that the given array is sorted in strictly increasing order.
     *
     * @param val Values.
     * @throws NonMonotonicSequenceException if the array is not sorted.
     * @since 2.2
     */
    pub fn check_order_inc_strict(val: &Vec<f64>) -> Result<bool, Rc<Exception>> {
        MathArrays::check_order(&val, OrderDirection::INCREASING, true)
    }


    /**
     * Throws DimensionMismatchException if the input array is not rectangular.
     *
     * @param in array to be tested
     * @throws NullArgumentException if input array is null
     * @throws DimensionMismatchException if input array is not rectangular
     * @since 3.1
     */
    pub fn check_rectangular(input: &Vec<Vec<i64>>) -> Result<Void, Rc<Exception>> {

        let mut i: usize = 1;
        while i < input.len() {

            if input[i].len() != input[0].len() {
                return Err(Exc::new_msg("DimensionMismatchException",
                                        format!("check_rectangular: different row length {} {}",
                                                input[i].len(),
                                                input[0].len())));
                // throw DimensionMismatchException::new(LocalizedFormats::DIFFERENT_ROWS_LENGTHS, in[i].len(), in[0].len());
            }

            i += 1;
        }
        Ok(Void::new())



    }


    /**
     * Check that all entries of the input array are strictly positive.
     *
     * @param in Array to be tested
     * @throws NotStrictlyPositiveException if any entries of the array are not
     * strictly positive.
     * @since 3.1
     */
    pub fn check_positive(input: &Vec<f64>) -> Result<Void, Rc<Exception>> {
        {
            let mut i: usize = 0;
            while i < input.len() {
                {
                    if input[i] <= 0.0 {
                        return Err(Exc::new_msg("NotStrictlyPositiveException",
                                                format!("check_positive: {}", input[i])));
                        // throw NotStrictlyPositiveException::new(input[i]);
                    }
                }
                i += 1;
            }
        }
        Ok(Void::new())

    }



    /**
     * Check that no entry of the input array is {@code NaN}.
     *
     * @param in Array to be tested.
     * @throws NotANumberException if an entry is {@code NaN}.
     * @since 3.4
     */
    pub fn check_not_nan(input: &Vec<f64>) -> Result<Void, Rc<Exception>> {
        {
            let mut i: usize = 0;
            while i < input.len() {
                {
                    if f64::is_nan(input[i]) {
                        return Err(Exc::new("NotANumberException"));
                        // throw NotANumberException::new();
                    }
                }
                i += 1;
            }
        }
        Ok(Void::new())

    }

    /**
     * Check that all entries of the input array are >= 0.
     *
     * @param in Array to be tested
     * @throws NotPositiveException if any array entries are less than 0.
     * @since 3.1
     */
    pub fn check_non_negative(input: &Vec<i64>) -> Result<Void, Rc<Exception>> {
        {
            let mut i: usize = 0;
            while i < input.len() {
                {
                    if input[i] < 0 {
                        return Err(Exc::new_msg("NotPositiveException",
                                                format!("check_non_negative: {}", input[i])));
                        // throw NotPositiveException::new(input[i]);
                    }
                }
                i += 1;
            }
        }
        Ok(Void::new())
    }

    /**
     * Check all entries of the input array are >= 0.
     *
     * @param in Array to be tested
     * @throws NotPositiveException if any array entries are less than 0.
     * @since 3.1
     */
    pub fn check_non_negative_2d(input: &Vec<Vec<i64>>) -> Result<Void, Rc<Exception>> {
        {
            let mut i: usize = 0;
            while i < input.len() {
                {
                    {
                        let mut j: usize = 0;
                        while j < input[i].len() {
                            {
                                if input[i][j] < 0 {
                                    return Err(Exc::new_msg("NotPositiveException",
                                                            format!("check_non_negative_square: \
                                                                     {}",
                                                                    input[i][j])));
                                    // throw NotPositiveException::new(input[i][j]);
                                }
                            }
                            j += 1;
                        }
                    }

                }
                i += 1;
            }
        }
        Ok(Void::new())

    }


    /**
     * Returns the Cartesian norm (2-norm), handling both overflow and underflow.
     * Translation of the minpack enorm subroutine.
     *
     * The redistribution policy for MINPACK is available
     * <a href="http://www.netlib.org/minpack/disclaimer">here</a>, for
     * convenience, it is reproduced below.</p>
     *
     * <table border="0" width="80%" cellpadding="10" align="center" bgcolor="#E0E0E0">
     * <tr><td>
     *    Minpack Copyright Notice (1999) University of Chicago.
     *    All rights reserved
     * </td></tr>
     * <tr><td>
     * Redistribution and use in source and binary forms, with or without
     * modification, are permitted provided that the following conditions
     * are met:
     * <ol>
     *  <li>Redistributions of source code must retain the above copyright
     *      notice, this list of conditions and the following disclaimer.</li>
     * <li>Redistributions in binary form must reproduce the above
     *     copyright notice, this list of conditions and the following
     *     disclaimer in the documentation and/or other materials provided
     *     with the distribution.</li>
     * <li>The end-user documentation included with the redistribution, if any,
     *     must include the following acknowledgment:
     *     {@code This product includes software developed by the University of
     *           Chicago, as Operator of Argonne National Laboratory.}
     *     Alternately, this acknowledgment may appear in the software itself,
     *     if and wherever such third-party acknowledgments normally appear.</li>
     * <li><strong>WARRANTY DISCLAIMER. THE SOFTWARE IS SUPPLIED "AS IS"
     *     WITHOUT WARRANTY OF ANY KIND. THE COPYRIGHT HOLDER, THE
     *     UNITED STATES, THE UNITED STATES DEPARTMENT OF ENERGY, AND
     *     THEIR EMPLOYEES: (1) DISCLAIM ANY WARRANTIES, EXPRESS OR
     *     IMPLIED, INCLUDING BUT NOT LIMITED TO ANY IMPLIED WARRANTIES
     *     OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE
     *     OR NON-INFRINGEMENT, (2) DO NOT ASSUME ANY LEGAL LIABILITY
     *     OR RESPONSIBILITY FOR THE ACCURACY, COMPLETENESS, OR
     *     USEFULNESS OF THE SOFTWARE, (3) DO NOT REPRESENT THAT USE OF
     *     THE SOFTWARE WOULD NOT INFRINGE PRIVATELY OWNED RIGHTS, (4)
     *     DO NOT WARRANT THAT THE SOFTWARE WILL FUNCTION
     *     UNINTERRUPTED, THAT IT IS ERROR-FREE OR THAT ANY ERRORS WILL
     *     BE CORRECTED.</strong></li>
     * <li><strong>LIMITATION OF LIABILITY. IN NO EVENT WILL THE COPYRIGHT
     *     HOLDER, THE UNITED STATES, THE UNITED STATES DEPARTMENT OF
     *     ENERGY, OR THEIR EMPLOYEES: BE LIABLE FOR ANY INDIRECT,
     *     INCIDENTAL, CONSEQUENTIAL, SPECIAL OR PUNITIVE DAMAGES OF
     *     ANY KIND OR NATURE, INCLUDING BUT NOT LIMITED TO LOSS OF
     *     PROFITS OR LOSS OF DATA, FOR ANY REASON WHATSOEVER, WHETHER
     *     SUCH LIABILITY IS ASSERTED ON THE BASIS OF CONTRACT, TORT
     *     (INCLUDING NEGLIGENCE OR STRICT LIABILITY), OR OTHERWISE,
     *     EVEN IF ANY OF SAID PARTIES HAS BEEN WARNED OF THE
     *     POSSIBILITY OF SUCH LOSS OR DAMAGES.</strong></li>
     * <ol></td></tr>
     * </table>
     *
     * @param v Vector of doubles.
     * @return the 2-norm of the vector.
     * @since 2.2
     */
    pub fn safe_norm(v: &Vec<f64>) -> f64 {
        let rdwarf: f64 = 3.834e-20;
        let rgiant: f64 = 1.304e+19;
        let mut s1: f64 = 0.0;
        let mut s2: f64 = 0.0;
        let mut s3: f64 = 0.0;
        let mut x1max: f64 = 0.0;
        let mut x3max: f64 = 0.0;
        let floatn: f64 = v.len() as f64;
        let agiant: f64 = rgiant / floatn;
        {
            let mut i: usize = 0;
            while i < v.len() {
                {
                    let xabs: f64 = F64::abs(v[i] as f64);
                    if xabs < rdwarf || xabs > agiant {
                        if xabs > rdwarf {
                            if xabs > x1max {
                                let r: f64 = x1max / xabs;
                                s1 = 1.0 + s1 * r * r;
                                x1max = xabs;
                            } else {
                                let r: f64 = xabs / x1max;
                                s1 += r * r;
                            }
                        } else {
                            if xabs > x3max {
                                let r: f64 = x3max / xabs;
                                s3 = 1.0 + s3 * r * r;
                                x3max = xabs;
                            } else {
                                if xabs != 0.0 {
                                    let r: f64 = xabs / x3max;
                                    s3 += r * r;
                                }
                            }
                        }
                    } else {
                        s2 += xabs * xabs;
                    }
                }
                i += 1;
            }
        }

        let norm: f64;
        if s1 != 0.0 {
            norm = x1max * f64::sqrt(s1 + (s2 / x1max) / x1max);
        } else {
            if s2 == 0.0 {
                norm = x3max * f64::sqrt(s3);
            } else {
                if s2 >= x3max {
                    norm = f64::sqrt(s2 * (1.0 + (x3max / s2) * (x3max * s3)));
                } else {
                    norm = f64::sqrt(x3max * ((s2 / x3max) + (x3max * s3)));
                }
            }
        }
        return norm;
    }



    /**
     * Sort an array in ascending order in place and perform the same reordering
     * of entries on other arrays. For example, if
     * {@code x = [3, 1, 2], y = [1, 2, 3]} and {@code z = [0, 5, 7]}, then
     * {@code sortInPlace(x, y, z)} will update {@code x} to {@code [1, 2, 3]},
     * {@code y} to {@code [2, 3, 1]} and {@code z} to {@code [5, 7, 0]}.
     *
     * @param x Array to be sorted and used as a pattern for permutation
     * of the other arrays.
     * @param yList Set of arrays whose permutations of entries will follow
     * those performed on {@code x}.
     * @throws DimensionMismatchException if any {@code y} is not the same
     * size as {@code x}.
     * @throws NullArgumentException if {@code x} or any {@code y} is null.
     * @since 3.0
     */
    pub fn sort_in_place(x: &mut Vec<f64>,
                         y_list: &mut Vec<&mut Vec<f64>>)
                         -> Result<Void, Rc<Exception>> {
        MathArrays::sort_in_place_dir(x, OrderDirection::INCREASING, y_list)
    }

    fn cmp_f64(a: f64, b: f64) -> Ordering {
        if a.is_nan() {
            return Ordering::Greater;
        }
        if b.is_nan() || a < b {
            return Ordering::Less;
        }
        if a == b {
            return Ordering::Equal;
        }
        return Ordering::Greater;
    }

    /**
     * Sort an array in place and perform the same reordering of entries on
     * other arrays.  This method works the same as the other
     * {@link #sortInPlace(double[], double[][]) sortInPlace} method, but
     * allows the order of the sort to be provided in the {@code dir}
     * parameter.
     *
     * @param x Array to be sorted and used as a pattern for permutation
     * of the other arrays.
     * @param dir Order direction.
     * @param yList Set of arrays whose permutations of entries will follow
     * those performed on {@code x}.
     * @throws DimensionMismatchException if any {@code y} is not the same
     * size as {@code x}.
     * @throws NullArgumentException if {@code x} or any {@code y} is null
     * @since 3.0
     */
    pub fn sort_in_place_dir(x: &mut Vec<f64>,
                             dir: OrderDirection,
                             y_list: &mut Vec<&mut Vec<f64>>)
                             -> Result<Void, Rc<Exception>> {
        // Consistency checks.
        let y_list_len: usize = y_list.len();
        let len: usize = x.len();
        {
            let mut j: usize = 0;
            while j < y_list_len {
                {
                    let y: &Vec<f64> = &y_list[j];

                    if y.len() != len {
                        return Err(Exc::new_msg("DimensionMismatchException",
                                                format!("{} {}", y.len(), len)));
                        // throw DimensionMismatchException::new(y.len(), len);
                    }
                }
                j += 1;
            }
        }

        // Associate each abscissa "x[i]" with its index "i".
        let mut list = Vec::new();
        {
            let mut i: usize = 0;
            while i < len {
                {
                    list.push(PairDoubleInteger::new(x[i], i as i32));
                }
                i += 1;
            }
        }

        // Create comparators for increasing and decreasing orders.
        // let comp: Comparator<PairDoubleInteger> =  if dir == MathArrays::OrderDirection::INCREASING { Comparator<PairDoubleInteger>::new() {
        //
        // {@inheritDoc} */
        // pub fn  compare(&self,  o1: &PairDoubleInteger,  o2: &PairDoubleInteger) -> i32  {
        // return Double::compare(&o1.get_key(), &o2.get_key());
        // }
        // } } else { Comparator<PairDoubleInteger>::new() {
        //
        // pub fn  compare(&self,  o1: &PairDoubleInteger,  o2: &PairDoubleInteger) -> i32  {
        // return Double::compare(&o2.get_key(), &o1.get_key());
        // }
        // } };
        //
        // Sort.
        list.sort_by(|a, b| match dir {
            OrderDirection::INCREASING => MathArrays::cmp_f64(a.get_key(), b.get_key()),
            OrderDirection::DECREASING => MathArrays::cmp_f64(b.get_key(), a.get_key()),
        });
        // Modify the original array so that its elements are in
        // the prescribed order.
        // Retrieve indices of original locations.
        let mut indices = vec![0usize; len];
        {
            let mut i: usize = 0;
            while i < len {
                {
                    let e: &PairDoubleInteger = &list[i];
                    x[i] = e.get_key();
                    indices[i] = e.get_value() as usize;
                }
                i += 1;
            }
        }

        // elements to their new location.
        {
            let mut j: usize = 0;
            while j < y_list_len {
                {
                    // Input array will be modified in place.
                    let ref mut y_in_place = y_list[j];
                    let y_orig: Vec<f64> = y_in_place.clone();
                    {
                        let mut i: usize = 0;
                        while i < len {
                            {
                                y_in_place[i] = y_orig[indices[i]];
                            }
                            i += 1;
                        }
                    }

                }
                j += 1;
            }
        }
        Ok(Void::new())

    }

    /**
     * Creates a copy of the {@code source} array.
     *
     * @param source Array to be copied.
     * @return the copied array.
     */
    pub fn copy_of<T: Clone>(source: &Vec<T>) -> Vec<T> {
        return source.clone();
    }

    /**
     * Creates a copy of the {@code source} array.
     *
     * @param source Array to be copied.
     * @param len Number of entries to copy. If smaller then the source
     * length, the copy will be truncated, if larger it will padded with
     * zeroes.
     * @return the copied array.
     */
    pub fn copy_of_head<T: Clone>(source: &Vec<T>, len: usize, zero: T) -> Vec<T> {

        let mut output = Vec::new();
        for i in 0usize..I32::min(len as i32, source.len() as i32) as usize {
            output.push(source[i].clone());
        }
        while output.len() < len {
            output.push(zero.clone());
        }

        return output;
    }

    /**
     * Creates a copy of the {@code source} array.
     *
     * @param source Array to be copied.
     * @param from Initial index of the range to be copied, inclusive.
     * @param to Final index of the range to be copied, exclusive. (This index may lie outside the array.)
     * @return the copied array.
     */
    pub fn copy_of_range<T: Clone>(source: &Vec<T>, from: usize, to: usize, zero: T) -> Vec<T> {
        let len: usize = to - from;
        let mut output = Vec::new();
        for i in from..I32::min(to as i32, source.len() as i32) as usize {
            output.push(source[i].clone());
        }
        while output.len() < len {
            output.push(zero.clone());
        }
        return output;
    }






    /**
     * Compute a linear combination accurately.
     * This method computes the sum of the products
     * <code>a<sub>i</sub> b<sub>i</sub></code> to high accuracy.
     * It does so by using specific multiplication and addition algorithms to
     * preserve accuracy and reduce cancellation effects.
     * <br/>
     * It is based on the 2005 paper
     * <a href="http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.2.1547">
     * Accurate Sum and Dot Product</a> by Takeshi Ogita, Siegfried M. Rump,
     * and Shin'ichi Oishi published in SIAM J. Sci. Comput.
     *
     * @param a Factors.
     * @param b Factors.
     * @return <code>Σ<sub>i</sub> a<sub>i</sub> b<sub>i</sub></code>.
     * @throws DimensionMismatchException if arrays dimensions don't match
     */
    pub fn linear_combination_vec(a: &Vec<f64>, b: &Vec<f64>) -> Result<f64, Rc<Exception>> {
        try!(MathArrays::check_equal_length(&a, &b));
        let len: usize = a.len();
        if len == 1 {
            // Revert to scalar multiplication.
            return Ok(a[0] * b[0]);
        }
        let mut prod_high = vec![0.0; len];
        let mut prod_low_sum: f64 = 0.0;
        {
            let mut i: usize = 0;
            while i < len {
                {
                    let ai: f64 = a[i];
                    let a_high: f64 = long_bits_to_double(&(double_to_raw_long_bits(&ai) &
                                                            ((0xFFFFFFFFFFFFFFFF) << 27)));
                    let a_low: f64 = ai - a_high;
                    let bi: f64 = b[i];
                    let b_high: f64 = long_bits_to_double(&(double_to_raw_long_bits(&bi) &
                                                            ((0xFFFFFFFFFFFFFFFF) << 27)));
                    let b_low: f64 = bi - b_high;
                    prod_high[i] = ai * bi;
                    let prod_low: f64 = a_low * b_low -
                                        (((prod_high[i] - a_high * b_high) - a_low * b_high) -
                                         a_high * b_low);
                    prod_low_sum += prod_low;
                }
                i += 1;
            }
        }

        let prod_high_cur: f64 = prod_high[0];
        let mut prod_high_next: f64 = prod_high[1];
        let mut s_high_prev: f64 = prod_high_cur + prod_high_next;
        let mut s_prime: f64 = s_high_prev - prod_high_next;
        let mut s_low_sum: f64 = (prod_high_next - (s_high_prev - s_prime)) +
                                 (prod_high_cur - s_prime);
        let len_minus_one: usize = len - 1;
        {
            let mut i: usize = 1;
            while i < len_minus_one {
                {
                    prod_high_next = prod_high[i + 1];
                    let s_high_cur: f64 = s_high_prev + prod_high_next;
                    s_prime = s_high_cur - prod_high_next;
                    s_low_sum += (prod_high_next - (s_high_cur - s_prime)) +
                                 (s_high_prev - s_prime);
                    s_high_prev = s_high_cur;
                }
                i += 1;
            }
        }

        let mut result: f64 = s_high_prev + (prod_low_sum + s_low_sum);
        if f64::is_nan(result) {
            // either we have split infinite numbers or some coefficients were NaNs,
            // just rely on the naive implementation and let IEEE754 handle this
            result = 0.0;
            {
                let mut i: usize = 0;
                while i < len {
                    {
                        result += a[i] * b[i];
                    }
                    i += 1;
                }
            }

        }
        return Ok(result);
    }



    /**
     * Compute a linear combination accurately.
     * <p>
     * This method computes a<sub>1</sub>×b<sub>1</sub> +
     * a<sub>2</sub>×b<sub>2</sub> to high accuracy. It does
     * so by using specific multiplication and addition algorithms to
     * preserve accuracy and reduce cancellation effects. It is based
     * on the 2005 paper <a
     * href="http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.2.1547">
     * Accurate Sum and Dot Product</a> by Takeshi Ogita,
     * Siegfried M. Rump, and Shin'ichi Oishi published in SIAM J. Sci. Comput.
     * </p>
     * @param a1 first factor of the first term
     * @param b1 second factor of the first term
     * @param a2 first factor of the second term
     * @param b2 second factor of the second term
     * @return a<sub>1</sub>×b<sub>1</sub> +
     * a<sub>2</sub>×b<sub>2</sub>
     * @see #linearCombination(double, double, double, double, double, double)
     * @see #linearCombination(double, double, double, double, double, double, double, double)
     */
    pub fn linear_combination_2d(a1: f64, b1: f64, a2: f64, b2: f64) -> f64 {
        // the code below is split in many additions/subtractions that may
        // appear redundant. However, they should NOT be simplified, as they
        // use IEEE754 floating point arithmetic rounding properties.
        // The variable naming conventions are that xyzHigh contains the most significant
        // bits of xyz and xyzLow contains its least significant bits. So theoretically
        // xyz is the sum xyzHigh + xyzLow, but in many cases below, this sum cannot
        // be represented in only one double precision number so we preserve two numbers
        // to hold it as long as we can, combining the high and low order bits together
        // only at the end, after cancellation may have occurred on high order bits
        // split a1 and b1 as one 26 bits number and one 27 bits number
        let a1_high: f64 = long_bits_to_double(&(double_to_raw_long_bits(&a1) &
                                                 ((-1i64 as u64) << 27)));
        let a1_low: f64 = a1 - a1_high;
        let b1_high: f64 = long_bits_to_double(&(double_to_raw_long_bits(&b1) &
                                                 ((-1i64 as u64) << 27)));
        let b1_low: f64 = b1 - b1_high;
        // accurate multiplication a1 * b1
        let prod1_high: f64 = a1 * b1;
        let prod1_low: f64 = a1_low * b1_low -
                             (((prod1_high - a1_high * b1_high) - a1_low * b1_high) -
                              a1_high * b1_low);
        // split a2 and b2 as one 26 bits number and one 27 bits number
        let a2_high: f64 = long_bits_to_double(&(double_to_raw_long_bits(&a2) &
                                                 ((-1i64 as u64) << 27)));
        let a2_low: f64 = a2 - a2_high;
        let b2_high: f64 = long_bits_to_double(&(double_to_raw_long_bits(&b2) &
                                                 ((-1i64 as u64) << 27)));
        let b2_low: f64 = b2 - b2_high;
        // accurate multiplication a2 * b2
        let prod2_high: f64 = a2 * b2;
        let prod2_low: f64 = a2_low * b2_low -
                             (((prod2_high - a2_high * b2_high) - a2_low * b2_high) -
                              a2_high * b2_low);
        // accurate addition a1 * b1 + a2 * b2
        let s12_high: f64 = prod1_high + prod2_high;
        let s12_prime: f64 = s12_high - prod2_high;
        let s12_low: f64 = (prod2_high - (s12_high - s12_prime)) + (prod1_high - s12_prime);
        // final rounding, s12 may have suffered many cancellations, we try
        // to recover some bits from the extra words we have saved up to now
        let mut result: f64 = s12_high + (prod1_low + prod2_low + s12_low);
        if f64::is_nan(result) {
            // either we have split infinite numbers or some coefficients were NaNs,
            // just rely on the naive implementation and let IEEE754 handle this
            result = a1 * b1 + a2 * b2;
        }
        return result;
    }

    /**
     * Compute a linear combination accurately.
     * <p>
     * This method computes a<sub>1</sub>×b<sub>1</sub> +
     * a<sub>2</sub>×b<sub>2</sub> + a<sub>3</sub>×b<sub>3</sub>
     * to high accuracy. It does so by using specific multiplication and
     * addition algorithms to preserve accuracy and reduce cancellation effects.
     * It is based on the 2005 paper <a
     * href="http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.2.1547">
     * Accurate Sum and Dot Product</a> by Takeshi Ogita,
     * Siegfried M. Rump, and Shin'ichi Oishi published in SIAM J. Sci. Comput.
     * </p>
     * @param a1 first factor of the first term
     * @param b1 second factor of the first term
     * @param a2 first factor of the second term
     * @param b2 second factor of the second term
     * @param a3 first factor of the third term
     * @param b3 second factor of the third term
     * @return a<sub>1</sub>×b<sub>1</sub> +
     * a<sub>2</sub>×b<sub>2</sub> + a<sub>3</sub>×b<sub>3</sub>
     * @see #linearCombination(double, double, double, double)
     * @see #linearCombination(double, double, double, double, double, double, double, double)
     */
    pub fn linear_combination_3d(a1: f64, b1: f64, a2: f64, b2: f64, a3: f64, b3: f64) -> f64 {
        // the code below is split in many additions/subtractions that may
        // appear redundant. However, they should NOT be simplified, as they
        // do use IEEE754 floating point arithmetic rounding properties.
        // The variables naming conventions are that xyzHigh contains the most significant
        // bits of xyz and xyzLow contains its least significant bits. So theoretically
        // xyz is the sum xyzHigh + xyzLow, but in many cases below, this sum cannot
        // be represented in only one double precision number so we preserve two numbers
        // to hold it as long as we can, combining the high and low order bits together
        // only at the end, after cancellation may have occurred on high order bits
        // split a1 and b1 as one 26 bits number and one 27 bits number
        let a1_high: f64 = long_bits_to_double(&(double_to_raw_long_bits(&a1) &
                                                 ((-1i64 as u64) << 27)));
        let a1_low: f64 = a1 - a1_high;
        let b1_high: f64 = long_bits_to_double(&(double_to_raw_long_bits(&b1) &
                                                 ((-1i64 as u64) << 27)));
        let b1_low: f64 = b1 - b1_high;
        // accurate multiplication a1 * b1
        let prod1_high: f64 = a1 * b1;
        let prod1_low: f64 = a1_low * b1_low -
                             (((prod1_high - a1_high * b1_high) - a1_low * b1_high) -
                              a1_high * b1_low);
        // split a2 and b2 as one 26 bits number and one 27 bits number
        let a2_high: f64 = long_bits_to_double(&(double_to_raw_long_bits(&a2) &
                                                 ((-1i64 as u64) << 27)));
        let a2_low: f64 = a2 - a2_high;
        let b2_high: f64 = long_bits_to_double(&(double_to_raw_long_bits(&b2) &
                                                 ((-1i64 as u64) << 27)));
        let b2_low: f64 = b2 - b2_high;
        // accurate multiplication a2 * b2
        let prod2_high: f64 = a2 * b2;
        let prod2_low: f64 = a2_low * b2_low -
                             (((prod2_high - a2_high * b2_high) - a2_low * b2_high) -
                              a2_high * b2_low);
        // split a3 and b3 as one 26 bits number and one 27 bits number
        let a3_high: f64 = long_bits_to_double(&(double_to_raw_long_bits(&a3) &
                                                 ((-1i64 as u64) << 27)));
        let a3_low: f64 = a3 - a3_high;
        let b3_high: f64 = long_bits_to_double(&(double_to_raw_long_bits(&b3) &
                                                 ((-1i64 as u64) << 27)));
        let b3_low: f64 = b3 - b3_high;
        // accurate multiplication a3 * b3
        let prod3_high: f64 = a3 * b3;
        let prod3_low: f64 = a3_low * b3_low -
                             (((prod3_high - a3_high * b3_high) - a3_low * b3_high) -
                              a3_high * b3_low);
        // accurate addition a1 * b1 + a2 * b2
        let s12_high: f64 = prod1_high + prod2_high;
        let s12_prime: f64 = s12_high - prod2_high;
        let s12_low: f64 = (prod2_high - (s12_high - s12_prime)) + (prod1_high - s12_prime);
        // accurate addition a1 * b1 + a2 * b2 + a3 * b3
        let s123_high: f64 = s12_high + prod3_high;
        let s123_prime: f64 = s123_high - prod3_high;
        let s123_low: f64 = (prod3_high - (s123_high - s123_prime)) + (s12_high - s123_prime);
        // final rounding, s123 may have suffered many cancellations, we try
        // to recover some bits from the extra words we have saved up to now
        let mut result: f64 = s123_high + (prod1_low + prod2_low + prod3_low + s12_low + s123_low);
        if f64::is_nan(result) {
            // either we have split infinite numbers or some coefficients were NaNs,
            // just rely on the naive implementation and let IEEE754 handle this
            result = a1 * b1 + a2 * b2 + a3 * b3;
        }
        return result;
    }

    /**
     * Compute a linear combination accurately.
     * <p>
     * This method computes a<sub>1</sub>×b<sub>1</sub> +
     * a<sub>2</sub>×b<sub>2</sub> + a<sub>3</sub>×b<sub>3</sub> +
     * a<sub>4</sub>×b<sub>4</sub>
     * to high accuracy. It does so by using specific multiplication and
     * addition algorithms to preserve accuracy and reduce cancellation effects.
     * It is based on the 2005 paper <a
     * href="http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.2.1547">
     * Accurate Sum and Dot Product</a> by Takeshi Ogita,
     * Siegfried M. Rump, and Shin'ichi Oishi published in SIAM J. Sci. Comput.
     * </p>
     * @param a1 first factor of the first term
     * @param b1 second factor of the first term
     * @param a2 first factor of the second term
     * @param b2 second factor of the second term
     * @param a3 first factor of the third term
     * @param b3 second factor of the third term
     * @param a4 first factor of the third term
     * @param b4 second factor of the third term
     * @return a<sub>1</sub>×b<sub>1</sub> +
     * a<sub>2</sub>×b<sub>2</sub> + a<sub>3</sub>×b<sub>3</sub> +
     * a<sub>4</sub>×b<sub>4</sub>
     * @see #linearCombination(double, double, double, double)
     * @see #linearCombination(double, double, double, double, double, double)
     */
    pub fn linear_combination_4d(a1: f64,
                                 b1: f64,
                                 a2: f64,
                                 b2: f64,
                                 a3: f64,
                                 b3: f64,
                                 a4: f64,
                                 b4: f64)
                                 -> f64 {
        // the code below is split in many additions/subtractions that may
        // appear redundant. However, they should NOT be simplified, as they
        // do use IEEE754 floating point arithmetic rounding properties.
        // The variables naming conventions are that xyzHigh contains the most significant
        // bits of xyz and xyzLow contains its least significant bits. So theoretically
        // xyz is the sum xyzHigh + xyzLow, but in many cases below, this sum cannot
        // be represented in only one double precision number so we preserve two numbers
        // to hold it as long as we can, combining the high and low order bits together
        // only at the end, after cancellation may have occurred on high order bits
        // split a1 and b1 as one 26 bits number and one 27 bits number
        let a1_high: f64 = long_bits_to_double(&(double_to_raw_long_bits(&a1) &
                                                 ((-1i64 as u64) << 27)));
        let a1_low: f64 = a1 - a1_high;
        let b1_high: f64 = long_bits_to_double(&(double_to_raw_long_bits(&b1) &
                                                 ((-1i64 as u64) << 27)));
        let b1_low: f64 = b1 - b1_high;
        // accurate multiplication a1 * b1
        let prod1_high: f64 = a1 * b1;
        let prod1_low: f64 = a1_low * b1_low -
                             (((prod1_high - a1_high * b1_high) - a1_low * b1_high) -
                              a1_high * b1_low);
        // split a2 and b2 as one 26 bits number and one 27 bits number
        let a2_high: f64 = long_bits_to_double(&(double_to_raw_long_bits(&a2) &
                                                 ((-1i64 as u64) << 27)));
        let a2_low: f64 = a2 - a2_high;
        let b2_high: f64 = long_bits_to_double(&(double_to_raw_long_bits(&b2) &
                                                 ((-1i64 as u64) << 27)));
        let b2_low: f64 = b2 - b2_high;
        // accurate multiplication a2 * b2
        let prod2_high: f64 = a2 * b2;
        let prod2_low: f64 = a2_low * b2_low -
                             (((prod2_high - a2_high * b2_high) - a2_low * b2_high) -
                              a2_high * b2_low);
        // split a3 and b3 as one 26 bits number and one 27 bits number
        let a3_high: f64 = long_bits_to_double(&(double_to_raw_long_bits(&a3) &
                                                 ((-1i64 as u64) << 27)));
        let a3_low: f64 = a3 - a3_high;
        let b3_high: f64 = long_bits_to_double(&(double_to_raw_long_bits(&b3) &
                                                 ((-1i64 as u64) << 27)));
        let b3_low: f64 = b3 - b3_high;
        // accurate multiplication a3 * b3
        let prod3_high: f64 = a3 * b3;
        let prod3_low: f64 = a3_low * b3_low -
                             (((prod3_high - a3_high * b3_high) - a3_low * b3_high) -
                              a3_high * b3_low);
        // split a4 and b4 as one 26 bits number and one 27 bits number
        let a4_high: f64 = long_bits_to_double(&(double_to_raw_long_bits(&a4) &
                                                 ((-1i64 as u64) << 27)));
        let a4_low: f64 = a4 - a4_high;
        let b4_high: f64 = long_bits_to_double(&(double_to_raw_long_bits(&b4) &
                                                 ((-1i64 as u64) << 27)));
        let b4_low: f64 = b4 - b4_high;
        // accurate multiplication a4 * b4
        let prod4_high: f64 = a4 * b4;
        let prod4_low: f64 = a4_low * b4_low -
                             (((prod4_high - a4_high * b4_high) - a4_low * b4_high) -
                              a4_high * b4_low);
        // accurate addition a1 * b1 + a2 * b2
        let s12_high: f64 = prod1_high + prod2_high;
        let s12_prime: f64 = s12_high - prod2_high;
        let s12_low: f64 = (prod2_high - (s12_high - s12_prime)) + (prod1_high - s12_prime);
        // accurate addition a1 * b1 + a2 * b2 + a3 * b3
        let s123_high: f64 = s12_high + prod3_high;
        let s123_prime: f64 = s123_high - prod3_high;
        let s123_low: f64 = (prod3_high - (s123_high - s123_prime)) + (s12_high - s123_prime);
        // accurate addition a1 * b1 + a2 * b2 + a3 * b3 + a4 * b4
        let s1234_high: f64 = s123_high + prod4_high;
        let s1234_prime: f64 = s1234_high - prod4_high;
        let s1234_low: f64 = (prod4_high - (s1234_high - s1234_prime)) + (s123_high - s1234_prime);
        // final rounding, s1234 may have suffered many cancellations, we try
        // to recover some bits from the extra words we have saved up to now
        let mut result: f64 = s1234_high +
                              (prod1_low + prod2_low + prod3_low + prod4_low + s12_low +
                               s123_low + s1234_low);
        if f64::is_nan(result) {
            // either we have split infinite numbers or some coefficients were NaNs,
            // just rely on the naive implementation and let IEEE754 handle this
            result = a1 * b1 + a2 * b2 + a3 * b3 + a4 * b4;
        }
        return result;
    }



    /**
     * Returns true iff both arguments are null or have same dimensions and all
     * their elements are equal as defined by
     * {@link Precision#equals(float,float)}.
     *
     * @param x first array
     * @param y second array
     * @return true if the values are both null or have same dimension
     * and equal elements.
     */
    pub fn equals_f32(x: &Vec<f32>, y: &Vec<f32>) -> bool {

        if x.len() != y.len() {
            return false;
        }
        {
            let mut i: usize = 0;
            while i < x.len() {
                {
                    if !precision::F32::equals(x[i], y[i]) {
                        return false;
                    }
                }
                i += 1;
            }
        }

        return true;
    }



    /**
     * Returns true iff both arguments are null or have same dimensions and all
     * their elements are equal as defined by
     * {@link Precision#equalsIncludingNaN(double,double) this method}.
     *
     * @param x first array
     * @param y second array
     * @return true if the values are both null or have same dimension and
     * equal elements
     * @since 2.2
     */
    pub fn equals_including_nan_f32(x: &Vec<f32>, y: &Vec<f32>) -> bool {
        if x.len() != y.len() {
            return false;
        }
        {
            let mut i: usize = 0;
            while i < x.len() {
                {
                    if !precision::F32::equals_including_nan(x[i], y[i]) {
                        return false;
                    }
                }
                i += 1;
            }
        }

        return true;
    }

    /**
     * Returns {@code true} iff both arguments are {@code null} or have same
     * dimensions and all their elements are equal as defined by
     * {@link Precision#equals(double,double)}.
     *
     * @param x First array.
     * @param y Second array.
     * @return {@code true} if the values are both {@code null} or have same
     * dimension and equal elements.
     */
    pub fn equals(x: &Vec<f64>, y: &Vec<f64>) -> bool {
        if x.len() != y.len() {
            return false;
        }
        {
            let mut i: usize = 0;
            while i < x.len() {
                {
                    if !precision::F64::equals(x[i], y[i]) {
                        return false;
                    }
                }
                i += 1;
            }
        }

        return true;
    }

    /**
     * Returns {@code true} iff both arguments are {@code null} or have same
     * dimensions and all their elements are equal as defined by
     * {@link Precision#equalsIncludingNaN(double,double) this method}.
     *
     * @param x First array.
     * @param y Second array.
     * @return {@code true} if the values are both {@code null} or have same
     * dimension and equal elements.
     * @since 2.2
     */
    pub fn equals_including_nan(x: &Vec<f64>, y: &Vec<f64>) -> bool {
        if x.len() != y.len() {
            return false;
        }
        {
            let mut i: usize = 0;
            while i < x.len() {
                {
                    if !precision::F64::equals_including_nan(x[i], y[i]) {
                        return false;
                    }
                }
                i += 1;
            }
        }

        return true;
    }

    /**
     * Normalizes an array to make it sum to a specified value.
     * Returns the result of the transformation
     * <pre>
     *    x |-> x * normalizedSum / sum
     * </pre>
     * applied to each non-NaN element x of the input array, where sum is the
     * sum of the non-NaN entries in the input array.
     * <p>
     * Throws IllegalArgumentException if {@code normalizedSum} is infinite
     * or NaN and ArithmeticException if the input array contains any infinite elements
     * or sums to 0.
     * <p>
     * Ignores (i.e., copies unchanged to the output array) NaNs in the input array.
     *
     * @param values Input array to be normalized
     * @param normalizedSum Target sum for the normalized array
     * @return the normalized array.
     * @throws MathArithmeticException if the input array contains infinite
     * elements or sums to zero.
     * @throws MathIllegalArgumentException if the target sum is infinite or {@code NaN}.
     * @since 2.1
     */
    pub fn normalize_array(values: &Vec<f64>,
                           normalized_sum: f64)
                           -> Result<Vec<f64>, Rc<Exception>> {
        if f64::is_infinite(normalized_sum) {
            return Err(Exc::new("MathIllegalArgumentException"));
            // throw MathIllegalArgumentException::new(LocalizedFormats::MathIllegalArgumentException);
        }
        if f64::is_nan(normalized_sum) {
            return Err(Exc::new_msg("MathIllegalArgumentException",
                                    format!("Cannot normalize to NaN")));
            // throw MathIllegalArgumentException::new(LocalizedFormats::NORMALIZE_NAN);
        }
        let mut sum: f64 = 0.0;
        let len: usize = values.len();
        {
            let mut i: usize = 0;
            while i < len {
                {
                    if f64::is_infinite(values[i]) {
                        return Err(Exc::new_msg("MathIllegalArgumentException",
                                                format!("INFINITE_ARRAY_ELEMENT {} {}",
                                                        values[i],
                                                        i)));
                        // throw MathIllegalArgumentException::new(LocalizedFormats::INFINITE_ARRAY_ELEMENT, values[i], i);
                    }
                    if !f64::is_nan(values[i]) {
                        sum += values[i];
                    }
                }
                i += 1;
            }
        }

        if sum == 0.0 {
            return Err(Exc::new_msg("MathArithmeticException", format!("array sums to zero")));
            // throw MathArithmeticException::new(LocalizedFormats::ARRAY_SUMS_TO_ZERO);
        }
        let mut out = Vec::new();
        {
            let mut i: usize = 0;
            while i < len {
                {
                    if f64::is_nan(values[i]) {
                        out.push(f64::NAN);
                    } else {
                        out.push(values[i] * normalized_sum / sum);
                    }
                }
                i += 1;
            }
        }

        return Ok(out);
    }

    /** Build an array of elements.
     * <p>
     * Arrays are filled with field.getZero()
     *
     * @param <T> the type of the field elements
     * @param field field to which array elements belong
     * @param length of the array
     * @return a new array
     * @since 3.2
     */
    pub fn build_array<T>(field: &Field<T>, length: usize) -> Vec<T> {
        let mut res = Vec::new();
        for _ in 0..length {
            res.push(field.get_zero());
        }
        res
    }

    /** Build a double dimension  array of elements.
     * <p>
     * Arrays are filled with field.getZero()
     *
     * @param <T> the type of the field elements
     * @param field field to which array elements belong
     * @param rows number of rows in the array
     * @param columns number of columns (may be negative to build partial
     * arrays in the same way <code>new Field[rows][]</code> works)
     * @return a new array
     * @since 3.2
     */
    pub fn build_array_2dim<T>(field: &Field<T>, rows: usize, columns: i32) -> Vec<Vec<T>> {
        let mut res = Vec::new();
        if columns < 0 {
            for _ in 0..rows {
                res.push(Vec::new());
            }
        } else {
            for _ in 0..rows {
                let row = MathArrays::build_array(field, columns as usize);
                res.push(row);
            }
        };
        res
    }

    /**
     * Calculates the <a href="http://en.wikipedia.org/wiki/Convolution">
     * convolution</a> between two sequences.
     * <p>
     * The solution is obtained via straightforward computation of the
     * convolution sum (and not via FFT). Whenever the computation needs
     * an element that would be located at an index outside the input arrays,
     * the value is assumed to be zero.
     *
     * @param x First sequence.
     * Typically, this sequence will represent an input signal to a system.
     * @param h Second sequence.
     * Typically, this sequence will represent the impulse response of the system.
     * @return the convolution of {@code x} and {@code h}.
     * This array's length will be {@code x.len() + h.len() - 1}.
     * @throws NullArgumentException if either {@code x} or {@code h} is {@code null}.
     * @throws NoDataException if either {@code x} or {@code h} is empty.
     *
     * @since 3.3
     */
    pub fn convolve(x: &Vec<f64>, h: &Vec<f64>) -> Result<Vec<f64>, Rc<Exception>> {
        let x_len: usize = x.len();
        let h_len: usize = h.len();
        if x_len == 0 || h_len == 0 {
            return Err(Exc::new("NoDataException"));
            // throw NoDataException::new();
        }
        // initialize the output array
        let total_length: usize = x_len + h_len - 1;
        let mut y = vec![0.0f64; total_length];
        // straightforward implementation of the convolution sum
        {
            let mut n: usize = 0;
            while n < total_length {
                {
                    let mut yn: f64 = 0.0;
                    let mut k: usize = I32::max(0, n as i32 + 1 - x_len as i32) as usize;
                    let mut j: usize = n - k;
                    while k < h_len {
                        yn += x[j] * h[k];
                        if j == 0 {
                            break;
                        }
                        j -= 1;
                        k += 1;
                    }
                    y[n] = yn;
                }
                n += 1;
            }
        }

        return Ok(y);
    }



    /**
     * Shuffle the entries of the given array.
     * The {@code start} and {@code pos} parameters select which portion
     * of the array is randomized and which is left untouched.
     *
     * @see #shuffle(int[],int,Position,RandomGenerator)
     *
     * @param list Array whose entries will be shuffled (in-place).
     * @param start Index at which shuffling begins.
     * @param pos Shuffling is performed for index positions between
     * {@code start} and either the end (if {@link Position#TAIL})
     * or the beginning (if {@link Position#HEAD}) of the array.
     */
    pub fn shuffle_start_pos(list: &mut Vec<i32>, start: usize, pos: Position) {
        MathArrays::shuffle_start_pos_gen(list, start, pos, Rc::new(RefCell::new(WellGeneratorFactory::new_well_19937c())));
    }

    /**
     * Shuffle the entries of the given array, using the
     * <a href="http://en.wikipedia.org/wiki/Fisher–Yates_shuffle#The_modern_algorithm">
     * Fisher–Yates</a> algorithm.
     * The {@code start} and {@code pos} parameters select which portion
     * of the array is randomized and which is left untouched.
     *
     * @param list Array whose entries will be shuffled (in-place).
     * @param start Index at which shuffling begins.
     * @param pos Shuffling is performed for index positions between
     * {@code start} and either the end (if {@link Position#TAIL})
     * or the beginning (if {@link Position#HEAD}) of the array.
     * @param rng Random number generator.
     */
    pub fn shuffle_start_pos_gen(list: &mut Vec<i32>,
                                 start: usize,
                                 pos: Position,
                                 rng: Rc<RefCell<BitsStreamGenerator>>) {
        if list.len() == 0 {
            return;
        }
        match pos {
            Position::TAIL => {
                {
                    {
                        let mut i: usize = list.len() - 1;
                        while i >= start {
                            {
                                let target: usize;
                                if i == start {
                                    target = start;
                                } else {
                                    // NumberIsTooLargeException cannot occur.
                                    let mut distribution =
                                        UniformIntegerDistribution::new_gen(rng.clone(),
                                                                            start as i32,
                                                                            i as i32)
                                            .unwrap();
                                    target = distribution.sample().unwrap() as usize;
                                }
                                let temp: i32 = list[target];
                                list[target] = list[i];
                                list[i] = temp;
                            }
                            if i == start {
                                break;
                            }
                            i -= 1;
                        }
                    }

                }
            }
            Position::HEAD => {
                {
                    {
                        let mut i: usize = 0;
                        while i <= start {
                            {
                                let target: usize;
                                if i == start {
                                    target = start;
                                } else {
                                    // NumberIsTooLargeException cannot occur.
                                    let mut distribution =
                                        UniformIntegerDistribution::new_gen(rng.clone(),
                                                                            i as i32,
                                                                            start as i32)
                                            .unwrap();
                                    target = distribution.sample().unwrap() as usize;
                                }
                                let temp: i32 = list[target];
                                list[target] = list[i];
                                list[i] = temp;
                            }
                            i += 1;
                        }
                    }

                }

            }
        }
    }

    /**
     * Shuffle the entries of the given array.
     *
     * @see #shuffle(int[],int,Position,RandomGenerator)
     *
     * @param list Array whose entries will be shuffled (in-place).
     * @param rng Random number generator.
     */
    pub fn shuffle_gen(list: &mut Vec<i32>, rng: Rc<RefCell<BitsStreamGenerator>>) {
        MathArrays::shuffle_start_pos_gen(list, 0, Position::TAIL, rng);
    }

    /**
     * Shuffle the entries of the given array.
     *
     * @see #shuffle(int[],int,Position,RandomGenerator)
     *
     * @param list Array whose entries will be shuffled (in-place).
     */
    pub fn shuffle(list: &mut Vec<i32>) {
        MathArrays::shuffle_gen(list,
                                Rc::new(RefCell::new(WellGeneratorFactory::new_well_19937c())));
    }

    /**
     * Returns an array representing the natural number {@code n}.
     *
     * @param n Natural number.
     * @return an array whose entries are the numbers 0, 1, ..., {@code n}-1.
     * If {@code n == 0}, the returned array is empty.
     */
    pub fn natural(n: usize) -> Vec<i32> {
        return MathArrays::sequence(n, 0, 1);
    }

    /**
     * Returns an array of {@code size} integers starting at {@code start},
     * skipping {@code stride} numbers.
     *
     * @param size Natural number.
     * @param start Natural number.
     * @param stride Natural number.
     * @return an array whose entries are the numbers
     * {@code start, start + stride, ..., start + (size - 1) * stride}.
     * If {@code size == 0}, the returned array is empty.
     *
     * @since 3.4
     */
    pub fn sequence(size: usize, start: usize, stride: usize) -> Vec<i32> {
        let mut a = Vec::new();
        {
            let mut i: usize = 0;
            while i < size {
                {
                    a.push((start + i * stride) as i32);
                }
                i += 1;
            }
        }

        return a;
    }



    /**
     * This method is used
     * to verify that the input parameters designate a subarray of positive length.
     * <p>
     * <ul>
     * <li>returns <code>true</code> iff the parameters designate a subarray of
     * positive length</li>
     * <li>throws <code>MathIllegalArgumentException</code> if the array is null or
     * or the indices are invalid</li>
     * <li>returns <code>false</li> if the array is non-null, but
     * <code>length</code> is 0.
     * </ul></p>
     *
     * @param values the input array
     * @param begin index of the first array element to include
     * @param length the number of elements to include
     * @return true if the parameters are valid and designate a subarray of positive length
     * @throws MathIllegalArgumentException if the indices are invalid or the array is null
     * @since 3.3
     */
    pub fn verify_values_begin_length(values: &Vec<f64>,
                                      begin: i32,
                                      length: i32)
                                      -> Result<bool, Rc<Exception>> {
        return MathArrays::verify_values_begin_length_allow_empty(&values, begin, length, false);
    }

    /**
     * This method is used
     * to verify that the input parameters designate a subarray of positive length.
     * <p>
     * <ul>
     * <li>returns <code>true</code> iff the parameters designate a subarray of
     * non-negative length</li>
     * <li>throws <code>IllegalArgumentException</code> if the array is null or
     * or the indices are invalid</li>
     * <li>returns <code>false</li> if the array is non-null, but
     * <code>length</code> is 0 unless <code>allowEmpty</code> is <code>true</code>
     * </ul></p>
     *
     * @param values the input array
     * @param begin index of the first array element to include
     * @param length the number of elements to include
     * @param allowEmpty if <code>true</code> then zero length arrays are allowed
     * @return true if the parameters are valid
     * @throws MathIllegalArgumentException if the indices are invalid or the array is null
     * @since 3.3
     */
    pub fn verify_values_begin_length_allow_empty(values: &Vec<f64>,
                                                  begin: i32,
                                                  length: i32,
                                                  allow_empty: bool)
                                                  -> Result<bool, Rc<Exception>> {

        if begin < 0 {
            return Err(Exc::new_msg("NotPositiveException",
                                    format!("start position ({0})", begin)));
        }
        if length < 0 {
            return Err(Exc::new_msg("NotPositiveException",
                                    format!("length must be positive ({0})", length)));
        }
        if (begin + length) as usize > values.len() {
            return Err(Exc::new_msg("NumberIsTooLargeException",
                                    format!("subarray ends after array end {} {}",
                                            begin + length,
                                            values.len())));
        }
        if length == 0 && !allow_empty {
            return Ok(false);
        }
        return Ok(true);
    }


    /**
     * This method is used
     * to verify that the begin and length parameters designate a subarray of positive length
     * and the weights are all non-negative, non-NaN, finite, and not all zero.
     * <p>
     * <ul>
     * <li>returns <code>true</code> iff the parameters designate a subarray of
     * positive length and the weights array contains legitimate values.</li>
     * <li>throws <code>IllegalArgumentException</code> if any of the following are true:
     * <ul><li>the values array is null</li>
     *     <li>the weights array is null</li>
     *     <li>the weights array does not have the same length as the values array</li>
     *     <li>the weights array contains one or more infinite values</li>
     *     <li>the weights array contains one or more NaN values</li>
     *     <li>the weights array contains negative values</li>
     *     <li>the start and length arguments do not determine a valid array</li></ul>
     * </li>
     * <li>returns <code>false</li> if the array is non-null, but
     * <code>length</code> is 0.
     * </ul></p>
     *
     * @param values the input array
     * @param weights the weights array
     * @param begin index of the first array element to include
     * @param length the number of elements to include
     * @return true if the parameters are valid and designate a subarray of positive length
     * @throws MathIllegalArgumentException if the indices are invalid or the array is null
     * @since 3.3
     */
    pub fn verify_values_weigths_begin_length(values: &Vec<f64>,
                                              weights: &Vec<f64>,
                                              begin: i32,
                                              length: i32)
                                              -> Result<bool, Rc<Exception>> {
        return MathArrays::verify_values_weigths_begin_length_allow_empty(&values,
                                                                          &weights,
                                                                          begin,
                                                                          length,
                                                                          false);
    }

    /**
     * This method is used
     * to verify that the begin and length parameters designate a subarray of positive length
     * and the weights are all non-negative, non-NaN, finite, and not all zero.
     * <p>
     * <ul>
     * <li>returns <code>true</code> iff the parameters designate a subarray of
     * non-negative length and the weights array contains legitimate values.</li>
     * <li>throws <code>MathIllegalArgumentException</code> if any of the following are true:
     * <ul><li>the values array is null</li>
     *     <li>the weights array is null</li>
     *     <li>the weights array does not have the same length as the values array</li>
     *     <li>the weights array contains one or more infinite values</li>
     *     <li>the weights array contains one or more NaN values</li>
     *     <li>the weights array contains negative values</li>
     *     <li>the start and length arguments do not determine a valid array</li></ul>
     * </li>
     * <li>returns <code>false</li> if the array is non-null, but
     * <code>length</code> is 0 unless <code>allowEmpty</code> is <code>true</code>.
     * </ul></p>
     *
     * @param values the input array.
     * @param weights the weights array.
     * @param begin index of the first array element to include.
     * @param length the number of elements to include.
     * @param allowEmpty if {@code true} than allow zero length arrays to pass.
     * @return {@code true} if the parameters are valid.
     * @throws NullArgumentException if either of the arrays are null
     * @throws MathIllegalArgumentException if the array indices are not valid,
     * the weights array contains NaN, infinite or negative elements, or there
     * are no positive weights.
     * @since 3.3
     */
    pub fn verify_values_weigths_begin_length_allow_empty(values: &Vec<f64>,
                                                          weights: &Vec<f64>,
                                                          begin: i32,
                                                          length: i32,
                                                          allow_empty: bool)
                                                          -> Result<bool, Rc<Exception>> {

        try!(MathArrays::check_equal_length(&weights, &values));
        let mut contains_positive_weight: bool = false;
        {
            let mut i: usize = begin as usize;
            while i < (begin + length) as usize {
                {
                    let weight: f64 = weights[i];
                    if f64::is_nan(weight) {
                        return Err(Exc::new_msg("MathIllegalArgumentException",
                                                format!("element {0} is NaN", i)));
                    }
                    if f64::is_infinite(weight) {
                        return Err(Exc::new_msg("MathIllegalArgumentException",
                                                format!("Array contains an infinite element, \
                                                         {0} at index {1}",
                                                        weight,
                                                        i)));
                    }
                    if weight < 0.0 {
                        return Err(Exc::new_msg("MathIllegalArgumentException",
                                                format!("element {0} is negative: {1}",
                                                        i,
                                                        weight)));
                    }
                    if !contains_positive_weight && weight > 0.0 {
                        contains_positive_weight = true;
                    }
                }
                i += 1;
            }
        }

        if !contains_positive_weight {
            return Err(Exc::new_msg("MathIllegalArgumentException",
                                    format!("weigth array must contain at least one non-zero \
                                             value")));
        }
        return MathArrays::verify_values_begin_length_allow_empty(&values,
                                                                  begin,
                                                                  length,
                                                                  allow_empty);
    }


    /**
     * Concatenates a sequence of arrays. The return array consists of the
     * entries of the input arrays concatenated in the order they appear in
     * the argument list.  Null arrays cause NullPointerExceptions; zero
     * length arrays are allowed (contributing nothing to the output array).
     *
     * @param x list of double[] arrays to concatenate
     * @return a new array consisting of the entries of the argument arrays
     * @throws NullPointerException if any of the arrays are null
     * @since 3.6
     */
    pub fn concatenate(x: &Vec<&Vec<f64>>) -> Vec<f64> {
        let mut combined = Vec::new();
        for a in x.iter() {
            for r in (*a).iter() {
                combined.push(*r);
            }
        }
        return combined;
    }


    /**
     * Returns an array consisting of the unique values in {@code data}.
     * The return array is sorted in descending order.  Empty arrays
     * are allowed, but null arrays result in NullPointerException.
     * Infinities are allowed.  NaN values are allowed with maximum
     * sort order - i.e., if there are NaN values in {@code data},
     * {@code Double.NaN} will be the first element of the output array,
     * even if the array also contains {@code Double.POSITIVE_INFINITY}.
     *
     * @param data array to scan
     * @return descending list of values included in the input array
     * @throws NullPointerException if data is null
     * @since 3.6
     */
    pub fn unique(data: &Vec<f64>) -> Vec<f64> {
        let mut values = BTreeSet::new();

        for element in data.iter() {
            values.insert(F64Wrapper::new(*element));
        }

        let mut out = Vec::new();
        for value in values.iter().rev() {
            out.push(value.value);
        }
        return out;
    }

    /**
     * Calculates the cosine of the angle between two vectors.
     *
     * @param v1 Cartesian coordinates of the first vector.
     * @param v2 Cartesian coordinates of the second vector.
     * @return the cosine of the angle between the vectors.
     * @since 3.6
     */
    pub fn cos_angle(v1: &Vec<f64>, v2: &Vec<f64>) -> Result<f64, Rc<Exception>> {
        let lin_comb = try!(MathArrays::linear_combination_vec(v1, v2));
        Ok(lin_comb / (MathArrays::safe_norm(v1) * MathArrays::safe_norm(v2)))
    }
}
