pub use complex::complex::Complex;

pub use assert;

pub struct TestUtils {
}

impl TestUtils {
    /**
     * Verifies that expected and actual are within delta, or are both NaN or
     * infinities of the same sign.
     */
    pub fn assert_equals_f64(expected: f64, actual: f64, delta: f64) {
        assert_within_delta!(expected, actual, delta);
    }

    /**
     * Verifies that expected and actual are within delta, or are both NaN or
     * infinities of the same sign.
     */
    pub fn assert_equals_f64_msg(msg: &'static str, expected: f64, actual: f64, delta: f64) {
        // check for NaN
        if f64::is_nan(expected) {
            assert!(f64::is_nan(actual), "{} is not NaN.", actual);
        } else {
            assert_within_delta!(expected, actual, delta, msg);
        }
    }

    /**
     * Verifies that the two arguments are exactly the same, either
     * both NaN or infinities of same sign, or identical floating point values.
     */
    pub fn assert_same_f64(expected: f64, actual: f64) {
        if !(expected.is_nan() && actual.is_nan()) {
            assert_within_delta!(expected, actual, 0.0)
        }
    }

    /**
     * Verifies that real and imaginary parts of the two complex arguments
     * are exactly the same.  Also ensures that NaN / infinite components match.
     */
    pub fn assert_same_complex(expected: &Complex, actual: &Complex) {
        TestUtils::assert_same_f64(expected.get_real(), actual.get_real());
        TestUtils::assert_same_f64(expected.get_imaginary(), actual.get_imaginary());
    }

    /**
     * Verifies that real and imaginary parts of the two complex arguments
     * differ by at most delta.  Also ensures that NaN / infinite components match.
     */
    pub fn assert_equals_complex(expected: &Complex, actual: &Complex, delta: f64) {
        assert_within_delta!(expected.get_real(), actual.get_real(), delta);
        assert_within_delta!(expected.get_imaginary(), actual.get_imaginary(), delta);
    }

    /**
     * Verifies that two double arrays have equal entries, up to tolerance
     */
    pub fn assert_equals_f64_arrays(expected: &Vec<f64>, observed: &Vec<f64>, tolerance: f64) {
        if expected.len() == observed.len() {
            for i in 0..expected.len() {
                TestUtils::assert_equals_f64_msg("Array comparison failure",
                                                 expected[i],
                                                 observed[i],
                                                 tolerance);
            }
        } else {
            assert!(false, "Array comparison failure")
        }
    }

    /**
     * Eliminates points with zero mass from densityPoints and densityValues parallel
     * arrays.  Returns the number of positive mass points and collapses the arrays so
     * that the first <returned value> elements of the input arrays represent the positive
     * mass points.
     */
    pub fn eliminate_zero_mass_points(density_points: &mut Vec<i32>,
                                      density_values: &mut Vec<f64>)
                                      -> usize {
        let mut positive_mass_count: usize = 0;
        {
            let mut i: usize = 0;
            while i < density_values.len() {
                {
                    if density_values[i] > 0.0 {
                        positive_mass_count += 1;
                    }
                }
                i += 1;
            }
        }

        if positive_mass_count < density_values.len() {
            let mut new_points = vec![0i32; positive_mass_count];
            let mut new_values = vec![0.0f64; positive_mass_count];
            let mut j: usize = 0;
            {
                let mut i: usize = 0;
                while i < density_values.len() {
                    {
                        if density_values[i] > 0.0 {
                            new_points[j] = density_points[i];
                            new_values[j] = density_values[i];
                            j += 1;
                        }
                    }
                    i += 1;
                }
            }
            for i in 0..positive_mass_count {
                density_points[i] = new_points[i];
                density_values[i] = new_values[i];
            }
        }
        return positive_mass_count;
    }
}
