// Licensed to the Apache Software Foundation (ASF) under one or more
// contributor license agreements.  See the NOTICE file distributed with
// this work for additional information regarding copyright ownership.
// The ASF licenses this file to You under the Apache License, Version 2.0
// (the "License"); you may not use this file except in compliance with
// the License.  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
use std::rc::*;
use std::cell::*;
use java::exc::*;
use util::arithmetic_utils::*;
use util::fastmath;

/**
 * Combinatorial utilities.
 *
 * @since 3.3
 */
/** All long-representable factorials */
const FACTORIALS: [i64; 21] = [1,
                               1,
                               2,
                               6,
                               24,
                               120,
                               720,
                               5040,
                               40320,
                               362880,
                               3628800,
                               39916800,
                               479001600,
                               6227020800,
                               87178291200,
                               1307674368000,
                               20922789888000,
                               355687428096000,
                               6402373705728000,
                               121645100408832000,
                               2432902008176640000];

thread_local!(static STIRLING_S2: RefCell<Vec<Vec<i64>>> = RefCell::new(Vec::new()));


/** Stirling numbers of the second kind. */
//  const STIRLING_S2: AtomicReference<Vec<Vec<i64>>> = AtomicReference<Vec<Vec<i64>>>::new(null);
pub struct CombinatoricsUtils {}

impl CombinatoricsUtils {
    /**
     * Returns an exact representation of the <a
     * href="http://mathworld.wolfram.com/BinomialCoefficient.html"> Binomial
     * Coefficient</a>, "{@code n choose k}", the number of
     * {@code k}-element subsets that can be selected from an
     * {@code n}-element set.
     * <p>
     * <Strong>Preconditions</strong>:
     * <ul>
     * <li> {@code 0 <= k <= n } (otherwise
     * {@code MathIllegalArgumentException} is thrown)</li>
     * <li> The result is small enough to fit into a {@code long}. The
     * largest value of {@code n} for which all coefficients are
     * {@code  < Long.MAX_VALUE} is 66. If the computed value exceeds
     * {@code Long.MAX_VALUE} a {@code MathArithMeticException} is
     * thrown.</li>
     * </ul></p>
     *
     * @param n the size of the set
     * @param k the size of the subsets to be counted
     * @return {@code n choose k}
     * @throws NotPositiveException if {@code n < 0}.
     * @throws NumberIsTooLargeException if {@code k > n}.
     * @throws MathArithmeticException if the result is too large to be
     * represented by a long integer.
     */
    pub fn binomial_coefficient(n: u32, k: u32) -> Result<i64, Rc<Exception>> {
        try!(CombinatoricsUtils::check_binomial(n, k));
        if (n == k) || (k == 0) {
            return Ok(1);
        }
        if (k == 1) || (k == n - 1) {
            return Ok(n as i64);
        }
        // Use symmetry for large k
        if k > n / 2 {
            return CombinatoricsUtils::binomial_coefficient(n, n - k);
        }
        // We use the formula
        // (n choose k) = n! / (n-k)! / k!
        // (n choose k) == ((n-k+1)*...*n) / (1*...*k)
        // which could be written
        // (n choose k) == (n-1 choose k-1) * n / k
        let mut result: i64 = 1;
        if n <= 61 {
            // For n <= 61, the naive implementation cannot overflow.
            let mut i: u32 = n - k + 1;
            {
                let mut j: u32 = 1;
                while j <= k {
                    result = result * i as i64 / j as i64;
                    i += 1;
                    j += 1;
                }
            }
        } else if n <= 66 {
            // For n > 61 but n <= 66, the result cannot overflow,
            // but we must take care not to overflow intermediate values.
            let mut i: u32 = n - k + 1;
            {
                let mut j: u32 = 1;
                while j <= k {
                    {
                        // We know that (result * i) is divisible by j,
                        // but (result * i) may overflow, so we split j:
                        // Filter out the gcd, d, so j/d and i/d are integer.
                        // result is divisible by (j/d) because (j/d)
                        // is relative prime to (i/d) and is a divisor of
                        // result * (i/d).
                        let d: i64 = try!(I64::gcd(i as i64, j as i64));
                        result = (result / (j as i64 / d)) * (i as i64 / d);
                        i += 1;
                    }
                    j += 1;
                }
            }
        } else {
            // For n > 66, a result overflow might occur, so we check
            // the multiplication, taking care to not overflow
            // unnecessary.
            let mut i: u32 = n - k + 1;
            {
                let mut j: u32 = 1;
                while j <= k {
                    {
                        let d: i64 = try!(I64::gcd(i as i64, j as i64));
                        result = try!(I64::mul_and_check(result / (j as i64 / d), i as i64 / d));
                        i += 1;
                    }
                    j += 1;
                }
            }
        }
        return Ok(result);
    }

    /**
     * Returns a {@code double} representation of the <a
     * href="http://mathworld.wolfram.com/BinomialCoefficient.html"> Binomial
     * Coefficient</a>, "{@code n choose k}", the number of
     * {@code k}-element subsets that can be selected from an
     * {@code n}-element set.
     * <p>
     * <Strong>Preconditions</strong>:
     * <ul>
     * <li> {@code 0 <= k <= n } (otherwise
     * {@code IllegalArgumentException} is thrown)</li>
     * <li> The result is small enough to fit into a {@code double}. The
     * largest value of {@code n} for which all coefficients are less than
     * Double.MAX_VALUE is 1029. If the computed value exceeds Double.MAX_VALUE,
     * Double.POSITIVE_INFINITY is returned</li>
     * </ul></p>
     *
     * @param n the size of the set
     * @param k the size of the subsets to be counted
     * @return {@code n choose k}
     * @throws NotPositiveException if {@code n < 0}.
     * @throws NumberIsTooLargeException if {@code k > n}.
     * @throws MathArithmeticException if the result is too large to be
     * represented by a long integer.
     */
    pub fn binomial_coefficient_double(n: u32, k: u32) -> Result<f64, Rc<Exception>> {
        try!(CombinatoricsUtils::check_binomial(n, k));
        if (n == k) || (k == 0) {
            return Ok(1.0);
        }
        if (k == 1) || (k == n - 1) {
            return Ok(n as f64);
        }
        if k > n / 2 {
            return CombinatoricsUtils::binomial_coefficient_double(n, n - k);
        }
        if n < 67 {
            return Ok(try!(CombinatoricsUtils::binomial_coefficient(n, k)) as f64);
        }
        let mut result: f64 = 1.0;
        {
            let mut i: u32 = 1;
            while i <= k {
                {
                    result *= (n - k + i) as f64 / i as f64;
                }
                i += 1;
            }
        }

        return Ok(fastmath::F64::floor(result + 0.5));
    }

    /**
     * Returns the natural {@code log} of the <a
     * href="http://mathworld.wolfram.com/BinomialCoefficient.html"> Binomial
     * Coefficient</a>, "{@code n choose k}", the number of
     * {@code k}-element subsets that can be selected from an
     * {@code n}-element set.
     * <p>
     * <Strong>Preconditions</strong>:
     * <ul>
     * <li> {@code 0 <= k <= n } (otherwise
     * {@code MathIllegalArgumentException} is thrown)</li>
     * </ul></p>
     *
     * @param n the size of the set
     * @param k the size of the subsets to be counted
     * @return {@code n choose k}
     * @throws NotPositiveException if {@code n < 0}.
     * @throws NumberIsTooLargeException if {@code k > n}.
     * @throws MathArithmeticException if the result is too large to be
     * represented by a long integer.
     */
    pub fn binomial_coefficient_log(n: u32, k: u32) -> Result<f64, Rc<Exception>> {
        try!(CombinatoricsUtils::check_binomial(n, k));
        if (n == k) || (k == 0) {
            return Ok(0.0);
        }
        if (k == 1) || (k == n - 1) {
            return Ok(fastmath::F64::log(n as f64));
        }
        // For values small enough to do exact integer computation,
        // return the log of the exact value
        //
        if n < 67 {
            let tmp = try!(CombinatoricsUtils::binomial_coefficient(n, k));
            return Ok(fastmath::F64::log(tmp as f64));
        }
        // Return the log of binomialCoefficientDouble for values that will not
        // overflow binomialCoefficientDouble
        //
        if n < 1030 {
            let tmp = try!(CombinatoricsUtils::binomial_coefficient_double(n, k));
            return Ok(fastmath::F64::log(tmp));
        }
        if k > n / 2 {
            return CombinatoricsUtils::binomial_coefficient_log(n, n - k);
        }
        // Sum logs for values that could overflow
        //
        let mut log_sum: f64 = 0.0;
        // n!/(n-k)!
        {
            let mut i: u32 = n - k + 1;
            while i <= n {
                {
                    log_sum += fastmath::F64::log(i as f64);
                }
                i += 1;
            }
        }

        // divide by k!
        {
            let mut i: u32 = 2;
            while i <= k {
                {
                    log_sum -= fastmath::F64::log(i as f64);
                }
                i += 1;
            }
        }

        return Ok(log_sum);
    }

    /**
     * Returns n!. Shorthand for {@code n} <a
     * href="http://mathworld.wolfram.com/Factorial.html"> Factorial</a>, the
     * product of the numbers {@code 1,...,n}.
     * <p>
     * <Strong>Preconditions</strong>:
     * <ul>
     * <li> {@code n >= 0} (otherwise
     * {@code MathIllegalArgumentException} is thrown)</li>
     * <li> The result is small enough to fit into a {@code long}. The
     * largest value of {@code n} for which {@code n!} does not exceed
     * Long.MAX_VALUE} is 20. If the computed value exceeds {@code Long.MAX_VALUE}
     * an {@code MathArithMeticException } is thrown.</li>
     * </ul>
     * </p>
     *
     * @param n argument
     * @return {@code n!}
     * @throws MathArithmeticException if the result is too large to be represented
     * by a {@code long}.
     * @throws NotPositiveException if {@code n < 0}.
     * @throws MathArithmeticException if {@code n > 20}: The factorial value is too
     * large to fit in a {@code long}.
     */
    pub fn factorial(n: i32) -> Result<i64, Rc<Exception>> {
        if n < 0 {
            return Err(Exc::new_msg("NotPositiveException",
                                    format!("must have n >= 0 for n!, got n = {0}", n)));
            // throw NotPositiveException::new(LocalizedFormats::FACTORIAL_NEGATIVE_PARAMETER, n);
        }
        if n > 20 {
            return Err(Exc::new("MathArithmeticException")); // throw MathArithmeticException::new();
        }
        return Ok(FACTORIALS[n as usize]);
    }

    /**
     * Compute n!, the<a href="http://mathworld.wolfram.com/Factorial.html">
     * factorial</a> of {@code n} (the product of the numbers 1 to n), as a
     * {@code double}.
     * The result should be small enough to fit into a {@code double}: The
     * largest {@code n} for which {@code n!} does not exceed
     * {@code Double.MAX_VALUE} is 170. If the computed value exceeds
     * {@code Double.MAX_VALUE}, {@code Double.POSITIVE_INFINITY} is returned.
     *
     * @param n Argument.
     * @return {@code n!}
     * @throws NotPositiveException if {@code n < 0}.
     */
    pub fn factorial_double(n: i32) -> Result<f64, Rc<Exception>> {
        if n < 0 {
            return Err(Exc::new_msg("NotPositiveException",
                                    format!("must have n >= 0 for n!, got n = {0}", n)));
            // throw NotPositiveException::new(LocalizedFormats::FACTORIAL_NEGATIVE_PARAMETER, n);
        }
        if n < 21 {
            return Ok(FACTORIALS[n as usize] as f64);
        }
        let logresres = CombinatoricsUtils::factorial_log(n);
        match logresres {
            Ok(logres) => Ok(fastmath::F64::floor(fastmath::F64::exp(logres)) + 0.5),
            _ => logresres,
        }
    }

    /**
     * Compute the natural logarithm of the factorial of {@code n}.
     *
     * @param n Argument.
     * @return {@code n!}
     * @throws NotPositiveException if {@code n < 0}.
     */
    pub fn factorial_log(n: i32) -> Result<f64, Rc<Exception>> {
        if n < 0 {
            return Err(Exc::new_msg("NotPositiveException",
                                    format!("must have n >= 0 for n!, got n = {0}", n)));
            // throw NotPositiveException::new(LocalizedFormats::FACTORIAL_NEGATIVE_PARAMETER, n);
        }
        if n < 21 {
            return Ok(fastmath::F64::log(FACTORIALS[n as usize] as f64));
        }
        let mut log_sum: f64 = 0.0;
        {
            let mut i: i32 = 2;
            while i <= n {
                {
                    log_sum += fastmath::F64::log(i as f64);
                }
                i += 1;
            }
        }

        return Ok(log_sum);
    }


    /**
        * Returns the <a
        * href="http://mathworld.wolfram.com/StirlingNumberoftheSecondKind.html">
        * Stirling number of the second kind</a>, "{@code S(n,k)}", the number of
        * ways of partitioning an {@code n}-element set into {@code k} non-empty
        * subsets.
        * <p>
        * The preconditions are {@code 0 <= k <= n } (otherwise
        * {@code NotPositiveException} is thrown)
        * </p>
        * @param n the size of the set
        * @param k the number of non-empty subsets
        * @return {@code S(n,k)}
        * @throws NotPositiveException if {@code k < 0}.
        * @throws NumberIsTooLargeException if {@code k > n}.
        * @throws MathArithmeticException if some overflow happens, typically for n exceeding 25 and
        * k between 20 and n-2 (S(n,n-1) is handled specifically and does not overflow)
        * @since 3.1
        */
    pub fn stirling_s2(n: u32, k: u32) -> Result<i64, Rc<Exception>> {
        if k > n {
            return Err(Exc::new_msg("NumberIsTooLargeException", format!("k: {} n: {}", k, n)));
        }
        STIRLING_S2.with(|vec_ref_cell| {
            {
                let mut vec_ref = vec_ref_cell.borrow_mut();
                if vec_ref.len() == 0 {
                    vec_ref.push(vec![1i64]);
                    for i in 1usize..26 {
                        let mut veci = vec![0i64; i + 1];
                        veci[0] = 0;
                        veci[1] = 1;
                        veci[i] = 1;
                        let mut j = 2usize;
                        while j < i {
                            veci[j] = (j as i64) * vec_ref[i - 1][j] + vec_ref[i - 1][j - 1];
                            j += 1;
                        }
                        vec_ref.push(veci);
                    }
                }
            }
        });
        return STIRLING_S2.with(|vec_ref_cell| {
            {
                let vec_ref = vec_ref_cell.borrow();
                if (n as usize) < vec_ref.len() {
                    // the number is in the small cache
                    return Ok(vec_ref[n as usize][k as usize]);
                } else {
                    // use explicit formula to compute the number without caching it
                    if k == 0 {
                        return Ok(0);
                    } else if k == 1 || k == n {
                        return Ok(1);
                    } else if k == 2 {
                        return Ok((1 << (n - 1)) - 1);
                    } else if k == n - 1 {
                        let tmp = try!(CombinatoricsUtils::binomial_coefficient(n, 2));
                        return Ok(tmp);
                    } else {
                        // definition formula: note that this may trigger some overflow
                        let mut sum: i64 = 0;
                        let mut sign: i64 = if (k & 0x1) == 0 { 1 } else { -1 };
                        {
                            let mut j: u32 = 1;
                            while j <= k {
                                {
                                    sign = -sign;
                                    let comb_tmp = try!(CombinatoricsUtils::binomial_coefficient(k, j));
                                    let pow_tmp = try!(I64::pow_i32(j as i64, n as i32));
                                    sum += sign * comb_tmp * pow_tmp;
                                    if sum < 0 {
                                        // there was an overflow somewhere
                                        return Err(Exc::new_msg("MathArithmeticException", format!("Argument {0} outside domain [{1} ; {2}]", n, 0, vec_ref.len() - 1)));
                                    }
                                }
                                j += 1;
                            }
                        }
                        let tmp_fact = try!(CombinatoricsUtils::factorial(k as i32));

                        return Ok(sum / tmp_fact);
                    }
                }
            }
        });
    }

    // Returns an iterator whose range is the k-element subsets of {0, ..., n - 1}
    // represented as {@code int[]} arrays.
    // <p>
    // The arrays returned by the iterator are sorted in descending order and
    // they are visited in lexicographic order with significance from right to
    // left. For example, combinationsIterator(4, 2) returns an Iterator that
    // will generate the following sequence of arrays on successive calls to
    // {@code next()}:</p><p>
    // {@code [0, 1], [0, 2], [1, 2], [0, 3], [1, 3], [2, 3]}
    // </p><p>
    // If {@code k == 0} an Iterator containing an empty array is returned and
    // if {@code k == n} an Iterator containing [0, ..., n -1] is returned.</p>
    //
    // @param n Size of the set from which subsets are selected.
    // @param k Size of the subsets to be enumerated.
    // @return an {@link Iterator iterator} over the k-sets in n.
    // @throws NotPositiveException if {@code n < 0}.
    // @throws NumberIsTooLargeException if {@code k > n}.
    // /
    // pub fn  combinations_iterator( n: i32,  k: i32) -> Result<LexicographicIterator,Rc<Exception>>  {
    // return Combinations::new(n, k).iter();
    // }
    //

    /**
     * Check binomial preconditions.
     *
     * @param n Size of the set.
     * @param k Size of the subsets to be counted.
     * @throws NotPositiveException if {@code n < 0}.
     * @throws NumberIsTooLargeException if {@code k > n}.
     */
    pub fn check_binomial(n: u32, k: u32) -> Result<bool, Rc<Exception>> {
        if n < k {
            return Err(Exc::new_msg("NumberIsTooLargeException",
                                    format!("must have n >= k for binomial coefficient (n, k), got k = {0}, n = {1}", k, n)));
        }

        Ok(true)
    }
}
