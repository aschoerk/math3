#![allow(dead_code)]
#![allow(unused_parens)]
#![allow(non_upper_case_globals)]
use std::f64;
use std::f64::consts;
use std::f32;
use std::i64;
use std::i32;
use util::rsutils::*;
use util::fastmathcalc;
use util::fast_math_literal_arrays::*;
use util::precision;

const Double_POSITIVE_INFINITY: f64 = f64::INFINITY;
const Double_NEGATIVE_INFINITY: f64 = f64::NEG_INFINITY;
const Double_NaN: f64 = f64::NAN;

/** Archimede's constant PI, ratio of circle circumference to diameter. */
pub const PI: f64 = 105414357.0 / 33554432.0 + 1.984187159361080883e-9;
/** Napier's constant e, base of the natural logarithm. */
pub const E: f64 = 2850325.0 / 1048576.0 + 8.254840070411028747e-8;

/** Index of exp(0) in the array of integer exponentials. */
const EXP_INT_TABLE_MAX_INDEX: i16 = 750;
/** Length of the array of integer exponentials. */
const EXP_INT_TABLE_LEN: usize = (EXP_INT_TABLE_MAX_INDEX * 2) as usize;
/** Logarithm table length. */
const LN_MANT_LEN: usize = 1024;
/** Exponential fractions table length. */
const EXP_FRAC_TABLE_LEN: usize = 1025; // 0, 1/1024, ... 1024/1024

/** StrictMath.log(Double.MAX_VALUE): {@value} */
// const LOG_MAX_VALUE: f64 = f64::MAX.ln();
/** Indicator for tables initialization.
 * <p>
 * This compile-time constant should be set to true only if one explicitly
 * wants to compute the tables at class loading time instead of using the
 * already computed ones provided as literal arrays below.
 * </p>
 */
const RECOMPUTE_TABLES_AT_RUNTIME: bool = false;

/** log(2) (high bits). */
const LN_2_A: f64 = 0.693147063255310059;

/** log(2) (low bits). */
const LN_2_B: f64 = 1.17304635250823482e-7;

/** Coefficients for log, when input 0.99 < x < 1.01. */
const LN_QUICK_COEF: [[f64; 2]; 9] = [[1.0, 5.669184079525E-24],
                                      [-0.25, -0.25],
                                      [0.3333333134651184, 1.986821492305628E-8],
                                      [-0.25, -6.663542893624021E-14],
                                      [0.19999998807907104, 1.1921056801463227E-8],
                                      [-0.1666666567325592, -7.800414592973399E-9],
                                      [0.1428571343421936, 5.650007086920087E-9],
                                      [-0.12502530217170715, -7.44321345601866E-11],
                                      [0.11113807559013367, 9.219544613762692E-9]];

/** Coefficients for log in the range of 1.0 < x < 1.0 + 2^-10. */
const LN_HI_PREC_COEF: [[f64; 2]; 6] = [[1.0, -6.032174644509064E-23],
                                        [-0.25, -0.25],
                                        [0.3333333134651184, 1.9868161777724352E-8],
                                        [-0.2499999701976776, -2.957007209750105E-8],
                                        [0.19999954104423523, 1.5830993332061267E-10],
                                        [-0.16624879837036133, -2.6033824355191673E-8]];

/** Sine, Cosine, Tangent tables are for 0, 1/8, 2/8, ... 13/8 = PI/2 approx. */
const SINE_TABLE_LEN: u16 = 14;

/** Sine table (high bits). */
const SINE_TABLE_A: [f64; 14] = [0.0,
                                 0.1246747374534607,
                                 0.24740394949913025,
                                 0.366272509098053,
                                 0.4794255495071411,
                                 0.5850973129272461,
                                 0.6816387176513672,
                                 0.7675435543060303,
                                 0.8414709568023682,
                                 0.902267575263977,
                                 0.9489846229553223,
                                 0.9808930158615112,
                                 0.9974949359893799,
                                 0.9985313415527344];

/** Sine table (low bits). */
const SINE_TABLE_B: [f64; 14] = [0.0,
                                 -4.068233003401932E-9,
                                 9.755392680573412E-9,
                                 1.9987994582857286E-8,
                                 -1.0902938113007961E-8,
                                 -3.9986783938944604E-8,
                                 4.23719669792332E-8,
                                 -5.207000323380292E-8,
                                 2.800552834259E-8,
                                 1.883511811213715E-8,
                                 -3.5997360512765566E-9,
                                 4.116164446561962E-8,
                                 5.0614674548127384E-8,
                                 -1.0129027912496858E-9];

/** Cosine table (high bits). */
const COSINE_TABLE_A: [f64; 14] = [1.0,
                                   0.9921976327896118,
                                   0.9689123630523682,
                                   0.9305076599121094,
                                   0.8775825500488281,
                                   0.8109631538391113,
                                   0.7316888570785522,
                                   0.6409968137741089,
                                   0.5403022766113281,
                                   0.4311765432357788,
                                   0.3153223395347595,
                                   0.19454771280288696,
                                   0.07073719799518585,
                                   -0.05417713522911072];

/** Cosine table (low bits). */
const COSINE_TABLE_B: [f64; 14] = [0.0,
                                   3.4439717236742845E-8,
                                   5.865827662008209E-8,
                                   -3.7999795083850525E-8,
                                   1.184154459111628E-8,
                                   -3.43338934259355E-8,
                                   1.1795268640216787E-8,
                                   4.438921624363781E-8,
                                   2.925681159240093E-8,
                                   -2.6437112632041807E-8,
                                   2.2860509143963117E-8,
                                   -4.813899778443457E-9,
                                   3.6725170580355583E-9,
                                   2.0217439756338078E-10];


/** Tangent table, used by atan() (high bits). */
const TANGENT_TABLE_A: [f64; 14] = [0.0,
                                    0.1256551444530487,
                                    0.25534194707870483,
                                    0.3936265707015991,
                                    0.5463024377822876,
                                    0.7214844226837158,
                                    0.9315965175628662,
                                    1.1974215507507324,
                                    1.5574076175689697,
                                    2.092571258544922,
                                    3.0095696449279785,
                                    5.041914939880371,
                                    14.101419448852539,
                                    -18.430862426757812];

/** Tangent table, used by atan() (low bits). */
const TANGENT_TABLE_B: [f64; 14] = [0.0,
                                    -7.877917738262007E-9,
                                    -2.5857668567479893E-8,
                                    5.2240336371356666E-9,
                                    5.206150291559893E-8,
                                    1.8307188599677033E-8,
                                    -5.7618793749770706E-8,
                                    7.848361555046424E-8,
                                    1.0708593250394448E-7,
                                    1.7827257129423813E-8,
                                    2.893485277253286E-8,
                                    3.1660099222737955E-7,
                                    4.983191803254889E-7,
                                    -3.356118100840571E-7];

/** Bits of 1/(2*pi), need for reducePayneHanek(). */
const RECIP_2PI: [u64; 18] = [(0x28be60db << 32) | 0x9391054a,
                              (0x7f09d5f4 << 32) | 0x7d4d3770,
                              (0x36d8a566 << 32) | 0x4f10e410,
                              (0x7f9458ea << 32) | 0xf7aef158,
                              (0x6dc91b8e << 32) | 0x909374b8,
                              (0x01924bba << 32) | 0x82746487,
                              (0x3f877ac7 << 32) | 0x2c4a69cf,
                              (0xba208d7d << 32) | 0x4baed121,
                              (0x3a671c09 << 32) | 0xad17df90,
                              (0x4e64758e << 32) | 0x60d4ce7d,
                              (0x272117e2 << 32) | 0xef7e4a0e,
                              (0xc7fe25ff << 32) | 0xf7816603,
                              (0xfbcbc462 << 32) | 0xd6829b47,
                              (0xdb4d9fb3 << 32) | 0xc9f2c26d,
                              (0xd3d18fd9 << 32) | 0xa797fa8b,
                              (0x5d49eeb1 << 32) | 0xfaf97c5e,
                              (0xcf41ce7d << 32) | 0xe294a4ba,
                              0x9afed7ec << 32];

/** Bits of pi/4, need for reducePayneHanek(). */
const PI_O_4_BITS: [u64; 2] = [(0xc90fdaa2 << 32) | 0x2168c234, (0xc4c6628b << 32) | 0x80dc1cd1];

/** Eighths.
     * This is used by sinQ, because its faster to do a table lookup than
     * a multiply in this time-critical routine
     */
const EIGHTHS: [f64; 14] = [0.0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1.0, 1.125, 1.25,
                            1.375, 1.5, 1.625];

/** Table of 2^((n+2)/3) */
const CBRTTWO: [f64; 5] =
    [0.6299605249474366, 0.7937005259840998, 1.0, 1.2599210498948732, 1.5874010519681994];

//  There are 52 bits in the mantissa of a double.
//  For additional precision, the code splits double numbers into two parts,
//  by clearing the low order 30 bits if possible, and then performs the arithmetic
//  on each half separately.
//

/**
     * 0x40000000 - used to split a double into two parts, both with the low order bits cleared.
     * Equivalent to 2^30.
     */
const HEX_40000000: u64 = 0x40000000; // 1073741824L

/** Mask used to clear low order 30 bits */
const MASK_30BITS: u64 = 0xFFFFFFFFFFFFFFFF - (HEX_40000000 - 1); // 0xFFFFFFFFC0000000L;

/** Mask used to clear the non-sign part of an int. */
const MASK_NON_SIGN_INT: u32 = 0x7fffffff;

/** Mask used to clear the non-sign part of a long. */
const MASK_NON_SIGN_LONG: u64 = 0x7fffffffffffffff;

/** Mask used to extract exponent from double bits. */
const MASK_DOUBLE_EXPONENT: u64 = 0x7ff0000000000000;

/** Mask used to extract mantissa from double bits. */
const MASK_DOUBLE_MANTISSA: u64 = 0x000fffffffffffff;

/** Mask used to add implicit high order bit for normalized double. */
const IMPLICIT_HIGH_BIT: u64 = 0x0010000000000000;

/** 2^52 - double numbers this large must be integral (no fraction) or NaN or Infinite */
const TWO_POWER_52: f64 = 4503599627370496.0;

/** Constant: {@value}. */
const F_1_3: f64 = 1.0 / 3.0;
/** Constant: {@value}. */
const F_1_5: f64 = 1.0 / 5.0;
/** Constant: {@value}. */
const F_1_7: f64 = 1.0 / 7.0;
/** Constant: {@value}. */
const F_1_9: f64 = 1.0 / 9.0;
/** Constant: {@value}. */
const F_1_11: f64 = 1.0 / 11.0;
/** Constant: {@value}. */
const F_1_13: f64 = 1.0 / 13.0;
/** Constant: {@value}. */
const F_1_15: f64 = 1.0 / 15.0;
/** Constant: {@value}. */
const F_1_17: f64 = 1.0 / 17.0;
/** Constant: {@value}. */
const F_3_4: f64 = 3.0 / 4.0;
/** Constant: {@value}. */
const F_15_16: f64 = 15.0 / 16.0;
/** Constant: {@value}. */
const F_13_14: f64 = 13.0 / 14.0;
/** Constant: {@value}. */
const F_11_12: f64 = 11.0 / 12.0;
/** Constant: {@value}. */
const F_9_10: f64 = 9.0 / 10.0;
/** Constant: {@value}. */
const F_7_8: f64 = 7.0 / 8.0;
/** Constant: {@value}. */
const F_5_6: f64 = 5.0 / 6.0;
/** Constant: {@value}. */
const F_1_2: f64 = 1.0 / 2.0;
/** Constant: {@value}. */
const F_1_4: f64 = 1.0 / 4.0;

fn calc_exp_int_table(i: i32) -> [f64; EXP_INT_TABLE_LEN] {
    let mut exp_int_table_a: [f64; EXP_INT_TABLE_LEN] = [0.0; EXP_INT_TABLE_LEN];
    let mut exp_int_table_b: [f64; EXP_INT_TABLE_LEN] = [0.0; EXP_INT_TABLE_LEN];
    for i in 0..EXP_INT_TABLE_MAX_INDEX {
        let (_, tmp) = fastmathcalc::expint(i as i32);
        exp_int_table_a[(i + EXP_INT_TABLE_MAX_INDEX) as usize] = tmp[0];
        exp_int_table_b[(i + EXP_INT_TABLE_MAX_INDEX) as usize] = tmp[1];
        if i != 0 {
            let recip = fastmathcalc::split_reciprocal(tmp);
            exp_int_table_a[(EXP_INT_TABLE_MAX_INDEX - i) as usize] = recip[0];
            exp_int_table_b[(EXP_INT_TABLE_MAX_INDEX - i) as usize] = recip[1];
        }

    }
    if i == 1 {
        exp_int_table_a
    } else {
        exp_int_table_b
    }
}
fn calc_exp_frac_table(i: i32) -> [f64; EXP_FRAC_TABLE_LEN] {
    let mut exp_frac_table_a: [f64; EXP_FRAC_TABLE_LEN] = [0.0; EXP_FRAC_TABLE_LEN];
    let mut exp_frac_table_b: [f64; EXP_FRAC_TABLE_LEN] = [0.0; EXP_FRAC_TABLE_LEN];

    // Populate expFracTable
    let factor: f64 = 1.0 / (EXP_FRAC_TABLE_LEN as f64 - 1.0);
    for i in 0..EXP_FRAC_TABLE_LEN {
        let (_, tmp) = fastmathcalc::slowexp(i as f64 * factor);
        exp_frac_table_a[i] = tmp[0];
        exp_frac_table_b[i] = tmp[1];
    }
    if i == 1 {
        exp_frac_table_a
    } else {
        exp_frac_table_b
    }
}

fn calc_ln_mant() -> [[f64; 2]; LN_MANT_LEN] {
    let mut res: [[f64; 2]; LN_MANT_LEN] = [[0.0; 2]; LN_MANT_LEN];
    for i in 0..LN_MANT_LEN {
        let d: f64 = long_bits_to_double(&(((i as u64) << 42) | 0x3ff0000000000000));
        res[i] = fastmathcalc::slow_log(d);
    }
    res
}
lazy_static! {
    	pub static ref LOG_MAX_VALUE: f64 = f64::ln(f64::MAX);
    }
// lazy_static! {
// pub static ref EXP_INT_TABLE_A: [f64;EXP_INT_TABLE_LEN] = calc_exp_int_table(1); // TODO
// pub static ref EXP_INT_TABLE_B: [f64;EXP_INT_TABLE_LEN] = calc_exp_int_table(2); // TODO
// pub static ref EXP_FRAC_TABLE_A: [f64;EXP_FRAC_TABLE_LEN] = calc_exp_frac_table(1); // TODO
// pub static ref EXP_FRAC_TABLE_B: [f64;EXP_FRAC_TABLE_LEN] = calc_exp_frac_table(2); // TODO
// pub static ref LOG_MAX_VALUE: f64 = f64::ln(f64::MAX);
// pub static ref LN_MANT: [[f64; 2]; LN_MANT_LEN] = calc_ln_mant();
// }

pub struct F64 {
	
}
impl F64 {
    /**
     * Get the high order bits from the mantissa.
     * Equivalent to adding and subtracting HEX_40000 but also works for very large numbers
     *
     * @param d the value to split
     * @return the high order part of the mantissa
     */
    fn double_high_part(d: f64) -> f64 {
        if (d > -*precision::SAFE_MIN && d < *precision::SAFE_MIN) {
            return d; // These are un-normalised - don't try to convert
        }
        let mut xl = double_to_raw_long_bits(&d); // can take raw bits because just gonna convert it back
        xl &= MASK_30BITS; // Drop low order bits
        long_bits_to_double(&xl)
    }

    pub fn ieee_remainder(dividend: f64, divisor: f64) -> f64 {
        dividend - (divisor * f64::round(dividend / divisor))
    }

    /** Compute the square root of a number.
     * <p><b>Note:</b> this implementation currently delegates to {@link Math#sqrt}
     * @param a number on which evaluation is done
     * @return square root of a
     */
    pub fn sqrt(a: f64) -> f64 {
        f64::sqrt(a)
    }

    /**
     * Internal helper method for exponential function.
     * @param x original argument of the exponential function
     * @param extra extra bits of precision on input (To Be Confirmed)
     * @param hiPrec extra bits of precision on output (To Be Confirmed)
     * @return exp(x)
     */
    fn exp_prec(x: f64, extra: f64, hi_prec_o: bool) -> (f64, [f64; 2]) {
        let mut hi_prec: [f64; 2] = [0.0; 2];
        let int_part_a: f64;
        let int_part_b: f64;
        if f64::is_nan(x) {
            return (f64::NAN, hi_prec);
        }
        if f64::INFINITY == x {
            return (f64::INFINITY, hi_prec);
        }
        if f64::NEG_INFINITY == x {
            return (0.0, hi_prec);
        }
        let mut int_val = x as i32;

        // Lookup exp(floor(x)).
        // int_part_a will have the upper 22 bits, int_part_b will have the lower
        // 52 bits.
        //
        if x < 0.0 {

            // We don't check against int_val here as conversion of large negative double values
            // may be affected by a JIT bug. Subsequent comparisons can safely use int_val
            if x < -746.0 {
                hi_prec[0] = 0.0;
                hi_prec[1] = 0.0;

                return (0.0, hi_prec);
            }

            if (int_val < -709) {
                // This will produce a subnormal output
                let tmp_result = F64::exp_prec(x + 40.19140625, extra, hi_prec_o);
                let result = tmp_result.0 / 285040095144011776.0;
                if hi_prec_o {

                    hi_prec[0] = tmp_result.1[0] / 285040095144011776.0;
                    hi_prec[1] = tmp_result.1[1] / 285040095144011776.0;
                }
                return (result, hi_prec);
            }

            if (int_val == -709) {
                // exp(1.494140625) is nearly a machine number...
                let tmp_result = F64::exp_prec(x + 1.494140625, extra, hi_prec_o);
                let result = tmp_result.0 / 4.455505956692756620;
                if hi_prec_o {

                    hi_prec[0] = tmp_result.1[0] / 4.455505956692756620;
                    hi_prec[1] = tmp_result.1[1] / 4.455505956692756620;
                }
                return (result, hi_prec);
            }

            int_val -= 1;

        } else {
            if (int_val > 709) {
                if hi_prec_o {

                    hi_prec[0] = f64::INFINITY;
                    hi_prec[1] = 0.0;
                }
                return (f64::INFINITY, hi_prec);
            }

        }

        int_part_a = EXP_INT_A[(EXP_INT_TABLE_MAX_INDEX as i32 + int_val) as usize];
        int_part_b = EXP_INT_B[(EXP_INT_TABLE_MAX_INDEX as i32 + int_val) as usize];

        // Get the fractional part of x, find the greatest multiple of 2^-10 less than
        // x and look up the exp function of it.
        // frac_part_a will have the upper 22 bits, frac_part_b the lower 52 bits.
        //
        let int_frac = ((x - int_val as f64) * 1024.0) as usize;
        let frac_part_a: f64 = EXP_FRAC_A[int_frac];
        let frac_part_b: f64 = EXP_FRAC_B[int_frac];

        // epsilon is the difference in x from the nearest multiple of 2^-10.  It
        // has a value in the range 0 <= epsilon < 2^-10.
        // Do the subtraction from x as the last step to avoid possible loss of precision.
        //
        let epsilon: f64 = x - (int_val as f64 + int_frac as f64 / 1024.0);

        // Compute z = exp(epsilon) - 1.0 via a minimax polynomial.  z has
        // full double precision (52 bits).  Since z < 2^-10, we will have
        // 62 bits of precision when combined with the constant 1.  This will be
        // used in the last addition below to get proper rounding.

        // Remez generated polynomial.  Converges on the interval [0, 2^-10], error
        // is less than 0.5 ULP
        let mut z: f64 = 0.04168701738764507;
        z = z * epsilon + 0.1666666505023083;
        z = z * epsilon + 0.5000000000042687;
        z = z * epsilon + 1.0;
        z = z * epsilon + -3.940510424527919E-20;

        // Compute (int_part_a+int_part_b) * (frac_part_a+frac_part_b) by binomial
        // expansion.
        // tempA is exact since int_part_a and int_part_b only have 22 bits each.
        // temp_b will have 52 bits of precision.
        //
        let temp_a: f64 = int_part_a * frac_part_a;
        let temp_b: f64 = int_part_a * frac_part_b + int_part_b * frac_part_a +
                          int_part_b * frac_part_b;

        // Compute the result.  (1+z)(temp_a+temp_b).  Order of operations is
        // important.  For accuracy add by increasing size.  temp_a is exact and
        // much larger than the others.  If there are extra bits specified from the
        // pow() function, use them.
        let temp_c: f64 = temp_b + temp_a;

        // If temp_c is positive infinite, the evaluation below could result in NaN,
        // because z could be negative at the same time.
        if temp_c == f64::INFINITY {
            return (f64::INFINITY, hi_prec);
        }

        let result: f64 = if extra != 0.0 {
            temp_c * extra * z + temp_c * extra + temp_c * z + temp_b + temp_a
        } else {
            temp_c * z + temp_b + temp_a
        };

        if hi_prec_o {

            hi_prec[0] = temp_a;
            hi_prec[1] = temp_c * extra * z + temp_c * extra + temp_c * z + temp_b;

        };

        return (result, hi_prec);
    }

    /**
     * Exponential function.
     *
     * Computes exp(x), function result is nearly rounded.   It will be correctly
     * rounded to the theoretical value for 99.9% of input values, otherwise it will
     * have a 1 ULP error.
     *
     * Method:
     *    Lookup intVal = exp(int(x))
     *    Lookup fracVal = exp(int(x-int(x) / 1024.0) * 1024.0 );
     *    Compute z as the exponential of the remaining bits by a polynomial minus one
     *    exp(x) = intVal * fracVal * (1 + z)
     *
     * Accuracy:
     *    Calculation is done with 63 bits of precision, so result should be correctly
     *    rounded for 99.9% of input values, with less than 1 ULP error otherwise.
     *
     * @param x   a double
     * @return double e<sup>x</sup>
     */
    pub fn exp(x: f64) -> f64 {
        return F64::exp_prec(x, 0.0, false).0;
    }



    /** Internal helper method for expm1
     * @param x number to compute shifted exponential
     * @param hiPrecOut receive high precision result for -1.0 < x < 1.0
     * @return exp(x) - 1
     */
    fn expm1_prec(x_p: f64, hi_prec_o: bool) -> (f64, [f64; 2]) {
        let mut hi_prec: [f64; 2] = [0.0; 2];
        let mut x = x_p;
        if x != x || x == 0.0 {
            // NaN or zero
            return (x, hi_prec);
        }
        if x <= -1.0 || x >= 1.0 {
            // If not between +/- 1.0
            // return exp(x) - 1.0;
            let result = F64::exp_prec(x, 0.0, true);
            if x > 0.0 {
                return (-1.0 + result.1[0] + result.1[1], result.1);
            } else {
                let ra: f64 = -1.0 + result.1[0];
                let mut rb: f64 = -(ra + 1.0 - result.1[0]);
                rb += result.1[1];
                return (ra + rb, result.1);
            }
        }
        let base_a: f64;
        let base_b: f64;
        let epsilon: f64;
        let mut negative: bool = false;
        if x < 0.0 {
            x = -x;
            negative = true;
        }
        {
            let int_frac: i32 = (x * 1024.0) as i32;
            let mut temp_a: f64 = EXP_FRAC_A[int_frac as usize] - 1.0;
            let mut temp_b: f64 = EXP_FRAC_B[int_frac as usize];
            let mut temp: f64 = temp_a + temp_b;
            temp_b = -(temp - temp_a - temp_b);
            temp_a = temp;
            temp = temp_a * HEX_40000000 as f64;
            base_a = temp_a + temp - temp;
            base_b = temp_b + (temp_a - base_a);
            epsilon = x - int_frac as f64 / 1024.0;
        }
        // Compute expm1(epsilon)
        let mut zb: f64 = 0.008336750013465571;
        zb = zb * epsilon + 0.041666663879186654;
        zb = zb * epsilon + 0.16666666666745392;
        zb = zb * epsilon + 0.49999999999999994;
        zb *= epsilon;
        zb *= epsilon;
        let mut za: f64 = epsilon;
        let mut temp: f64 = za + zb;
        zb = -(temp - za - zb);
        za = temp;
        temp = za * HEX_40000000 as f64;
        temp = za + temp - temp;
        zb += za - temp;
        za = temp;
        // Combine the parts.   expm1(a+b) = expm1(a) + expm1(b) + expm1(a)*expm1(b)
        let mut ya: f64 = za * base_a;
        // double yb = za*baseB + zb*baseA + zb*baseB;
        temp = ya + za * base_b;
        let mut yb: f64 = -(temp - ya - za * base_b);
        ya = temp;
        temp = ya + zb * base_a;
        yb += -(temp - ya - zb * base_a);
        ya = temp;
        temp = ya + zb * base_b;
        yb += -(temp - ya - zb * base_b);
        ya = temp;
        // ya = ya + za + baseA;
        // yb = yb + zb + baseB;
        temp = ya + base_a;
        yb += -(temp - base_a - ya);
        ya = temp;
        temp = ya + za;
        // yb += (ya > za) ? -(temp - ya - za) : -(temp - za - ya);
        yb += -(temp - ya - za);
        ya = temp;
        temp = ya + base_b;
        // yb += (ya > baseB) ? -(temp - ya - baseB) : -(temp - baseB - ya);
        yb += -(temp - ya - base_b);
        ya = temp;
        temp = ya + zb;
        // yb += (ya > zb) ? -(temp - ya - zb) : -(temp - zb - ya);
        yb += -(temp - ya - zb);
        ya = temp;
        if negative {
            // Compute expm1(-x) = -expm1(x) / (expm1(x) + 1)
            let denom: f64 = 1.0 + ya;
            let denomr: f64 = 1.0 / denom;
            let denomb: f64 = -(denom - 1.0 - ya) + yb;
            let ratio: f64 = ya * denomr;
            temp = ratio * HEX_40000000 as f64;
            let ra: f64 = ratio + temp - temp;
            let mut rb: f64 = ratio - ra;
            temp = denom * HEX_40000000 as f64;
            za = denom + temp - temp;
            zb = denom - za;
            rb += (ya - za * ra - za * rb - zb * ra - zb * rb) * denomr;
            // f(x) = x/1+x
            // Compute f'(x)
            // Product rule:  d(uv) = du*v + u*dv
            // Chain rule:  d(f(g(x)) = f'(g(x))*f(g'(x))
            // d(1/x) = -1/(x*x)
            // d(1/1+x) = -1/( (1+x)^2) *  1 =  -1/((1+x)*(1+x))
            // d(x/1+x) = -x/((1+x)(1+x)) + 1/1+x = 1 / ((1+x)(1+x))
            // Adjust for yb
            // numerator
            rb += yb * denomr;
            // denominator
            rb += -ya * denomb * denomr * denomr;
            // negate
            ya = -ra;
            yb = -rb;
        }
        if hi_prec_o {
            hi_prec[0] = ya;
            hi_prec[1] = yb;
        }

        return (ya + yb, hi_prec);
    }


    /** Compute exp(x) - 1
     * @param x number to compute shifted exponential
     * @return exp(x) - 1
     */
    pub fn expm1(x: f64) -> f64 {
        F64::expm1_prec(x, false).0
    }



    /** Compute the hyperbolic cosine of a number.
     * @param x number on which evaluation is done
     * @return hyperbolic cosine of x
     */
    pub fn cosh(x_p: f64) -> f64 {
        let mut x = x_p;

        if x != x {
            return x;
        }

        // cosh[z] = (exp(z) + exp(-z))/2

        // for numbers with magnitude 20 or so,
        // exp(-z) can be ignored in comparison with exp(z)

        if x > 20.0 {
            if x >= *LOG_MAX_VALUE {
                // Avoid overflow (MATH-905).
                let t = F64::exp(0.5 * x);
                return (0.5 * t) * t;
            } else {
                return 0.5 * F64::exp(x);
            }
        } else if x < -20.0 {
            if x <= -*LOG_MAX_VALUE {
                // Avoid overflow (MATH-905).
                let t = F64::exp(-0.5 * x);
                return (0.5 * t) * t;
            } else {
                return 0.5 * F64::exp(-x);
            }
        }

        if (x < 0.0) {
            x = -x;
        }
        let res = F64::exp_prec(x, 0.0, true);
        let hi_prec: [f64; 2] = res.1;

        let mut ya = hi_prec[0] + hi_prec[1];
        let mut yb = -(ya - hi_prec[0] - hi_prec[1]);

        let temp = ya * HEX_40000000 as f64;
        let yaa = ya + temp - temp;
        let yab = ya - yaa;

        // recip = 1/y
        let recip = 1.0 / ya;
        let mut temp = recip * HEX_40000000 as f64;
        let recipa = recip + temp - temp;
        let mut recipb = recip - recipa;

        // Correct for rounding in division
        recipb += (1.0 - yaa * recipa - yaa * recipb - yab * recipa - yab * recipb) * recip;
        // Account for yb
        recipb += -yb * recip * recip;

        // y = y + 1/y
        temp = ya + recipa;
        yb += -(temp - ya - recipa);
        ya = temp;
        temp = ya + recipb;
        yb += -(temp - ya - recipb);
        ya = temp;

        (ya + yb) * 0.5
    }

    /** Compute the hyperbolic sine of a number.
     * @param x number on which evaluation is done
     * @return hyperbolic sine of x
     */
    pub fn sinh(x_p: f64) -> f64 {
        let mut x = x_p;
        let mut negate: bool = false;
        if x != x {
            return x;
        }
        if x > 20.0 {
            if x >= *LOG_MAX_VALUE {
                // Avoid overflow (MATH-905).
                let t: f64 = F64::exp(0.5 * x);
                return (0.5 * t) * t;
            } else {
                return 0.5 * F64::exp(x);
            }
        } else if x < -20.0 {
            if x <= -*LOG_MAX_VALUE {
                // Avoid overflow (MATH-905).
                let t: f64 = F64::exp(-0.5 * x);
                return (-0.5 * t) * t;
            } else {
                return -0.5 * F64::exp(-x);
            }
        }
        if x == 0.0 {
            return x;
        }
        if x < 0.0 {
            x = -x;
            negate = true;
        }
        let mut result: f64;
        if x > 0.25 {
            let res = F64::exp_prec(x, 0.0, true);
            let hi_prec: [f64; 2] = res.1;
            let mut ya: f64 = hi_prec[0] + hi_prec[1];
            let mut yb: f64 = -(ya - hi_prec[0] - hi_prec[1]);
            let mut temp: f64 = ya * HEX_40000000 as f64;
            let yaa: f64 = ya + temp - temp;
            let yab: f64 = ya - yaa;
            // recip = 1/y
            let recip: f64 = 1.0 / ya;
            temp = recip * HEX_40000000 as f64;
            let mut recipa: f64 = recip + temp - temp;
            let mut recipb: f64 = recip - recipa;
            // Correct for rounding in division
            recipb += (1.0 - yaa * recipa - yaa * recipb - yab * recipa - yab * recipb) * recip;
            // Account for yb
            recipb += -yb * recip * recip;
            recipa = -recipa;
            recipb = -recipb;
            // y = y + 1/y
            temp = ya + recipa;
            yb += -(temp - ya - recipa);
            ya = temp;
            temp = ya + recipb;
            yb += -(temp - ya - recipb);
            ya = temp;
            result = ya + yb;
            result *= 0.5;
        } else {
            let res = F64::expm1_prec(x, true);
            let hi_prec: [f64; 2] = res.1;
            let mut ya: f64 = hi_prec[0] + hi_prec[1];
            let mut yb: f64 = -(ya - hi_prec[0] - hi_prec[1]);
            // Compute expm1(-x) = -expm1(x) / (expm1(x) + 1)
            let denom: f64 = 1.0 + ya;
            let denomr: f64 = 1.0 / denom;
            let denomb: f64 = -(denom - 1.0 - ya) + yb;
            let ratio: f64 = ya * denomr;
            let mut temp: f64 = ratio * HEX_40000000 as f64;
            let ra: f64 = ratio + temp - temp;
            let mut rb: f64 = ratio - ra;
            temp = denom * HEX_40000000 as f64;
            let za: f64 = denom + temp - temp;
            let zb: f64 = denom - za;
            rb += (ya - za * ra - za * rb - zb * ra - zb * rb) * denomr;
            // Adjust for yb
            // numerator
            rb += yb * denomr;
            // denominator
            rb += -ya * denomb * denomr * denomr;
            // y = y - 1/y
            temp = ya + ra;
            yb += -(temp - ya - ra);
            ya = temp;
            temp = ya + rb;
            yb += -(temp - ya - rb);
            ya = temp;
            result = ya + yb;
            result *= 0.5;
        }
        if negate {
            result = -result;
        }
        return result;
    }

    /** Compute the hyperbolic tangent of a number.
     * @param x number on which evaluation is done
     * @return hyperbolic tangent of x
     */
    pub fn tanh(x_p: f64) -> f64 {
        let mut x = x_p;
        let mut negate: bool = false;
        if x != x {
            return x;
        }
        if x > 20.0 {
            return 1.0;
        }
        if x < -20.0 {
            return -1.0;
        }
        if x == 0.0 {
            return x;
        }
        if x < 0.0 {
            x = -x;
            negate = true;
        }
        let mut result: f64;
        if x >= 0.5 {
            // tanh(x) = (exp(2x) - 1) / (exp(2x) + 1)
            let res = F64::exp_prec(x * 2.0, 0.0, true);
            let hi_prec: [f64; 2] = res.1;
            let ya: f64 = hi_prec[0] + hi_prec[1];
            let yb: f64 = -(ya - hi_prec[0] - hi_prec[1]);
            // Numerator
            let mut na: f64 = -1.0 + ya;
            let mut nb: f64 = -(na + 1.0 - ya);
            let mut temp: f64 = na + yb;
            nb += -(temp - na - yb);
            na = temp;
            // Denominator
            let mut da: f64 = 1.0 + ya;
            let mut db: f64 = -(da - 1.0 - ya);
            temp = da + yb;
            db += -(temp - da - yb);
            da = temp;
            temp = da * HEX_40000000 as f64;
            let daa: f64 = da + temp - temp;
            let dab: f64 = da - daa;
            // ratio = na/da
            let ratio: f64 = na / da;
            temp = ratio * HEX_40000000 as f64;
            let ratioa: f64 = ratio + temp - temp;
            let mut ratiob: f64 = ratio - ratioa;
            // Correct for rounding in division
            ratiob += (na - daa * ratioa - daa * ratiob - dab * ratioa - dab * ratiob) / da;
            // Account for nb
            ratiob += nb / da;
            // Account for db
            ratiob += -db * na / da / da;
            result = ratioa + ratiob;
        } else {
            // tanh(x) = expm1(2x) / (expm1(2x) + 2)
            let res = F64::expm1_prec(x * 2.0, true);
            let hi_prec: [f64; 2] = res.1;
            let ya: f64 = hi_prec[0] + hi_prec[1];
            let yb: f64 = -(ya - hi_prec[0] - hi_prec[1]);
            // Numerator
            let na: f64 = ya;
            let nb: f64 = yb;
            // Denominator
            let mut da: f64 = 2.0 + ya;
            let mut db: f64 = -(da - 2.0 - ya);
            let mut temp: f64 = da + yb;
            db += -(temp - da - yb);
            da = temp;
            temp = da * HEX_40000000 as f64;
            let daa: f64 = da + temp - temp;
            let dab: f64 = da - daa;
            // ratio = na/da
            let ratio: f64 = na / da;
            temp = ratio * HEX_40000000 as f64;
            let ratioa: f64 = ratio + temp - temp;
            let mut ratiob: f64 = ratio - ratioa;
            // Correct for rounding in division
            ratiob += (na - daa * ratioa - daa * ratiob - dab * ratioa - dab * ratiob) / da;
            // Account for nb
            ratiob += nb / da;
            // Account for db
            ratiob += -db * na / da / da;
            result = ratioa + ratiob;
        }
        if negate {
            result = -result;
        }
        return result;
    }

    /** Compute the inverse hyperbolic cosine of a number.
     * @param a number on which evaluation is done
     * @return inverse hyperbolic cosine of a
     */
    pub fn acosh(a: f64) -> f64 {
        return F64::log(a + F64::sqrt(a * a - 1.0));
    }

    /** Compute the inverse hyperbolic sine of a number.
     * @param a number on which evaluation is done
     * @return inverse hyperbolic sine of a
     */
    pub fn asinh(a_p: f64) -> f64 {
        let mut a = a_p;
        let mut negative: bool = false;
        if a < 0.0 {
            negative = true;
            a = -a;
        }
        let abs_asinh: f64;
        if a > 0.167 {
            abs_asinh = F64::log(F64::sqrt(a * a + 1.0) + a);
        } else {
            let a2: f64 = a * a;
            if a > 0.097 {
                abs_asinh = a *
                            (1.0 -
                             a2 *
                             (F_1_3 -
                              a2 *
                              (F_1_5 -
                               a2 *
                               (F_1_7 -
                                a2 *
                                (F_1_9 -
                                 a2 *
                                 (F_1_11 -
                                  a2 * (F_1_13 - a2 * (F_1_15 - a2 * F_1_17 * F_15_16) * F_13_14) *
                                  F_11_12) * F_9_10) *
                                F_7_8) * F_5_6) * F_3_4) * F_1_2);
            } else if a > 0.036 {
                abs_asinh = a *
                            (1.0 -
                             a2 *
                             (F_1_3 -
                              a2 *
                              (F_1_5 -
                               a2 *
                               (F_1_7 -
                                a2 * (F_1_9 - a2 * (F_1_11 - a2 * F_1_13 * F_11_12) * F_9_10) *
                                F_7_8) * F_5_6) * F_3_4) * F_1_2);
            } else if a > 0.0036 {
                abs_asinh = a *
                            (1.0 -
                             a2 *
                             (F_1_3 -
                              a2 * (F_1_5 - a2 * (F_1_7 - a2 * F_1_9 * F_7_8) * F_5_6) * F_3_4) *
                             F_1_2);
            } else {
                abs_asinh = a * (1.0 - a2 * (F_1_3 - a2 * F_1_5 * F_3_4) * F_1_2);
            }
        }
        return if negative { -abs_asinh } else { abs_asinh };
    }

    /** Compute the inverse hyperbolic tangent of a number.
     * @param a number on which evaluation is done
     * @return inverse hyperbolic tangent of a
     */
    pub fn atanh(a_p: f64) -> f64 {
        let mut a = a_p;
        let mut negative: bool = false;
        if a < 0.0 {
            negative = true;
            a = -a;
        }
        let abs_atanh: f64;
        if a > 0.15 {
            abs_atanh = 0.5 * F64::log((1.0 + a) / (1.0 - a));
        } else {
            let a2: f64 = a * a;
            if a > 0.087 {
                abs_atanh = a *
                            (1.0 +
                             a2 *
                             (F_1_3 +
                              a2 *
                              (F_1_5 +
                               a2 *
                               (F_1_7 +
                                a2 *
                                (F_1_9 +
                                 a2 * (F_1_11 + a2 * (F_1_13 + a2 * (F_1_15 + a2 * F_1_17))))))));
            } else if a > 0.031 {
                abs_atanh = a *
                            (1.0 +
                             a2 *
                             (F_1_3 +
                              a2 *
                              (F_1_5 + a2 * (F_1_7 + a2 * (F_1_9 + a2 * (F_1_11 + a2 * F_1_13))))));
            } else if a > 0.003 {
                abs_atanh = a * (1.0 + a2 * (F_1_3 + a2 * (F_1_5 + a2 * (F_1_7 + a2 * F_1_9))));
            } else {
                abs_atanh = a * (1.0 + a2 * (F_1_3 + a2 * F_1_5));
            }
        }
        return if negative { -abs_atanh } else { abs_atanh };
    }



    /** Compute the signum of a number.
     * The signum is -1 for negative numbers, +1 for positive numbers and 0 otherwise
     * @param a number on which evaluation is done
     * @return -1.0, -0.0, +0.0, +1.0 or NaN depending on sign of a
     */
    pub fn signum(a: f64) -> f64 {
        // return +0.0/-0.0/NaN depending on a
        return if (a < 0.0) {
            -1.0
        } else {
            (if (a > 0.0) { 1.0 } else { a })
        };
    }



    /** Compute next number towards positive infinity.
     * @param a number to which neighbor should be computed
     * @return neighbor of a towards positive infinity
     */
    pub fn next_up(a: f64) -> f64 {
        return F64::next_after(a, f64::INFINITY);
    }

    /** Compute next number towards negative infinity.
     * @param a number to which neighbor should be computed
     * @return neighbor of a towards negative infinity
     * @since 3.4
     */
    pub fn next_down(a: f64) -> f64 {
        return F64::next_after(a, f64::NEG_INFINITY);
    }


    /**
     * Natural logarithm.
     *
     * @param x   a double
     * @return log(x)
     */
    pub fn log(x: f64) -> f64 {
        return F64::log_prec(x, false).0;
    }

    /**
     * Internal helper method for natural logarithm function.
     * @param x original argument of the natural logarithm function
     * @param hiPrec extra bits of precision on output (To Be Confirmed)
     * @return log(x)
     */
    fn log_prec(x: f64, want_hi_prec: bool) -> (f64, [f64; 2]) {
        let mut hi_prec_out: [f64; 2] = [0.0; 2];
        if x == 0.0 {
            // Handle special case of +0/-0
            return (f64::NEG_INFINITY, hi_prec_out);
        }
        let mut bits: u64 = double_to_raw_long_bits(&x);
        // Handle special cases of negative input, and NaN
        if ((bits & 0x8000000000000000) != 0 || x != x) && x != 0.0 {
            hi_prec_out[0] = f64::NAN;
            return (f64::NAN, hi_prec_out);
        }
        // Handle special cases of Positive infinity.
        if x == f64::INFINITY {
            hi_prec_out[0] = f64::INFINITY;
            return (f64::INFINITY, hi_prec_out);
        }
        // Extract the exponent
        let mut exp: i32 = (bits >> 52) as i32 - 1023;
        if (bits & 0x7ff0000000000000) == 0 {
            // Subnormal!
            if x == 0.0 {
                // Zero
                hi_prec_out[0] = f64::NEG_INFINITY;
                return (f64::NEG_INFINITY, hi_prec_out);
            }
            // Normalize the subnormal number.
            bits <<= 1;
            while (bits & 0x0010000000000000) == 0 {
                exp -= 1;
                bits <<= 1;
            }
        }
        if (exp == -1 || exp == 0) && x < 1.01 && x > 0.99 && !want_hi_prec {
            // The normal method doesn't work well in the range [0.99, 1.01], so call do a straight
            // polynomial expansion in higer precision.
            // Compute x - 1.0 and split it
            let mut xa: f64 = x - 1.0;
            let xb: f64; //  = xa - x + 1.0;
            let mut tmp: f64 = xa * HEX_40000000 as f64;
            let mut aa: f64 = xa + tmp - tmp;
            let mut ab: f64 = xa - aa;
            xa = aa;
            xb = ab;
            let ln_coef_last: [f64; 2] = LN_QUICK_COEF[8];
            let mut ya: f64 = ln_coef_last[0];
            let mut yb: f64 = ln_coef_last[1];
            for i in (0..8).rev() {
                // Multiply a = y * x
                aa = ya * xa;
                ab = ya * xb + yb * xa + yb * xb;
                // split, so now y = a
                tmp = aa * HEX_40000000 as f64;
                ya = aa + tmp - tmp;
                yb = aa - ya + ab;
                // Add  a = y + lnQuickCoef
                let ln_coef_i: [f64; 2] = LN_QUICK_COEF[i];
                aa = ya + ln_coef_i[0];
                ab = yb + ln_coef_i[1];
                // Split y = a
                tmp = aa * HEX_40000000 as f64;
                ya = aa + tmp - tmp;
                yb = aa - ya + ab;
            }
            // Multiply a = y * x
            aa = ya * xa;
            ab = ya * xb + yb * xa + yb * xb;
            // split, so now y = a
            tmp = aa * HEX_40000000 as f64;
            ya = aa + tmp - tmp;
            yb = aa - ya + ab;
            return (ya + yb, hi_prec_out);
        }
        // lnm is a log of a number in the range of 1.0 - 2.0, so 0 <= lnm < ln(2)
        let lnm: [f64; 2] = LN_MANT[((bits & 0x000ffc0000000000) >> 42) as usize];
        // double epsilon = x / Double.longBitsToDouble(bits & 0xfffffc0000000000L);
        //
        // epsilon -= 1.0;
        //
        // y is the most significant 10 bits of the mantissa
        // double y = Double.longBitsToDouble(bits & 0xfffffc0000000000L);
        // double epsilon = (x - y) / y;
        let epsilon: f64 = (bits & 0x3ffffffffff) as f64 /
                           (TWO_POWER_52 + (bits & 0x000ffc0000000000) as f64);
        let mut lnza: f64;
        let mut lnzb: f64 = 0.0;
        if want_hi_prec {
            /* split epsilon -> x */
             let mut tmp: f64 = epsilon * HEX_40000000 as f64	;
             let mut aa: f64 = epsilon + tmp - tmp;
             let mut ab: f64 = epsilon - aa;
             let xa: f64 = aa;
             let mut xb: f64 = ab;
            /* Need a more accurate epsilon, so adjust the division. */
             let numer: f64 = (bits & 0x3ffffffffff) as f64;
             let denom: f64 = TWO_POWER_52 + (bits & 0x000ffc0000000000) as f64;
            aa = numer - xa * denom - xb * denom;
            xb += aa / denom;
            /* Remez polynomial evaluation */
             let ln_coef_last = LN_HI_PREC_COEF[LN_HI_PREC_COEF.len() - 1];
             let mut ya: f64 = ln_coef_last[0];
             let mut yb: f64 = ln_coef_last[1];
            for i in (0..(LN_HI_PREC_COEF.len() - 1)).rev() {
                /* Multiply a = y * x */
                aa = ya * xa;
                ab = ya * xb + yb * xa + yb * xb;
                
                /* split, so now y = a */
                tmp = aa * HEX_40000000 as f64;

                ya = aa + tmp - tmp;
                yb = aa - ya + ab;
                /* Add  a = y + lnHiPrecCoef */
                 let ln_coef_i = LN_HI_PREC_COEF[i];
                aa = ya + ln_coef_i[0];
                ab = yb + ln_coef_i[1];
                /* Split y = a */
                tmp = aa * HEX_40000000 as f64;
                ya = aa + tmp - tmp;
                yb = aa - ya + ab;
            }
            /* Multiply a = y * x */
            aa = ya * xa;
            ab = ya * xb + yb * xa + yb * xb;
            /* split, so now lnz = a */
            /*
      tmp = aa * 1073741824.0;
      lnza = aa + tmp - tmp;
      lnzb = aa - lnza + ab;
             */
            lnza = aa + ab;
            lnzb = -(lnza - aa - ab);            
        } else {
            /* High precision not required.  Eval Remez polynomial
         using standard double precision */
            lnza = -0.16624882440418567;
            lnza = lnza * epsilon + 0.19999954120254515;
            lnza = lnza * epsilon + -0.2499999997677497;
            lnza = lnza * epsilon + 0.3333333333332802;
            lnza = lnza * epsilon + -0.5;
            lnza = lnza * epsilon + 1.0;
            lnza *= epsilon;
        }
        // Relative sizes:
        // lnzb     [0, 2.33E-10]
        // lnm[1]   [0, 1.17E-7]
        // ln2B*exp [0, 1.12E-4]
        // lnza      [0, 9.7E-4]
        // lnm[0]   [0, 0.692]
        // ln2A*exp [0, 709]
        //
        // Compute the following sum:
        // lnzb + lnm[1] + ln2B*exp + lnza + lnm[0] + ln2A*exp;
        //
        // return lnzb + lnm[1] + ln2B*exp + lnza + lnm[0] + ln2A*exp;
        let mut a: f64 = LN_2_A * exp as f64;
        let mut b: f64 = 0.0;
        let mut c: f64 = a + lnm[0];
        let mut d: f64 = -(c - a - lnm[0]);
        a = c;
        b += d;
        c = a + lnza;
        d = -(c - a - lnza);
        a = c;
        b += d;
        c = a + LN_2_B * exp as f64;
        d = -(c - a - LN_2_B * exp as f64);
        a = c;
        b += d;
        c = a + lnm[1];
        d = -(c - a - lnm[1]);
        a = c;
        b += d;
        c = a + lnzb;
        d = -(c - a - lnzb);
        a = c;
        b += d;
        hi_prec_out[0] = a;
        hi_prec_out[1] = b;
        return (a + b, hi_prec_out);
    }

    /**
     * Computes log(1 + x).
     *
     * @param x Number.
     * @return {@code log(1 + x)}.
     */
    pub fn log1p(x: f64) -> f64 {
        if x == -1.0 {
            return f64::NEG_INFINITY;
        }
        if x == f64::INFINITY {
            return f64::INFINITY;
        }
        if x > 1e-6 || x < -1e-6 {
            let xpa: f64 = 1.0 + x;
            let xpb: f64 = -(xpa - 1.0 - x);

            let lores = F64::log_prec(xpa, true);
            if lores.0.is_infinite() {
                // Don't allow this to be converted to NaN
                return lores.0;
            }
            // Do a taylor series expansion around xpa:
            //   f(x+y) = f(x) + f'(x) y + f''(x)/2 y^2
            let fx1: f64 = xpb / xpa;
            let epsilon: f64 = 0.5 * fx1 + 1.0;
            return epsilon * fx1 + lores.1[1] + lores.1[0];
        } else {
            // Value is small |x| < 1e6, do a Taylor series centered on 1.
            let y: f64 = (x * F_1_3 - F_1_2) * x + 1.0;
            return y * x;
        }
    }

    /** Compute the base 10 logarithm.
     * @param x a number
     * @return log10(x)
     */
    pub fn log10(x: f64) -> f64 {
        let lores = F64::log_prec(x, true);
        if lores.0.is_infinite() {
            // don't allow this to be converted to NaN
            return lores.0;
        }
        let tmp: f64 = lores.1[0] * HEX_40000000 as f64;
        let lna: f64 = lores.1[0] + tmp - tmp;
        let lnb: f64 = lores.1[0] - lna + lores.1[1];
        let rln10a: f64 = 0.4342944622039795;
        let rln10b: f64 = 1.9699272335463627E-8;
        return rln10b * lnb + rln10b * lna + rln10a * lnb + rln10a * lna;
    }


    /**
     * Computes the <a href="http://mathworld.wolfram.com/Logarithm.html">
     * logarithm</a> in a given base.
     *
     * Returns {@code NaN} if either argument is negative.
     * If {@code base} is 0 and {@code x} is positive, 0 is returned.
     * If {@code base} is positive and {@code x} is 0,
     * {@code Double.NEG_INFINITY} is returned.
     * If both arguments are 0, the result is {@code NaN}.
     *
     * @param base Base of the logarithm, must be greater than 0.
     * @param x Argument, must be greater than 0.
     * @return the value of the logarithm, i.e. the number {@code y} such that
     * <code>base<sup>y</sup> = x</code>.
     * @since 1.2 (previously in {@code MathUtils}, moved as of version 3.0)
     */
    pub fn log_base(base: f64, x: f64) -> f64 {
        return F64::log(x) / F64::log(base);
    }

    /**
     * Power function.  Compute x^y.
     *
     * @param x   a double
     * @param y   a double
     * @return double
     */
    pub fn pow(x: f64, y: f64) -> f64 {
        if y == 0.0 {
            // y = -0 or y = +0
            return 1.0;
        } else {
            let y_bits: u64 = double_to_raw_long_bits(&y);
            let y_raw_exp: i32 = ((y_bits & MASK_DOUBLE_EXPONENT) >> 52) as i32;
            let y_raw_mantissa: u64 = y_bits & MASK_DOUBLE_MANTISSA;
            let x_bits: u64 = double_to_raw_long_bits(&x);
            let x_raw_exp: i32 = ((x_bits & MASK_DOUBLE_EXPONENT) >> 52) as i32;
            let x_raw_mantissa: u64 = x_bits & MASK_DOUBLE_MANTISSA;
            if y_raw_exp > 1085 {
                if (y_raw_exp == 2047 && y_raw_mantissa != 0) ||
                   (x_raw_exp == 2047 && x_raw_mantissa != 0) {
                    // NaN
                    return f64::NAN;
                } else if x_raw_exp == 1023 && x_raw_mantissa == 0 {
                    // x = -1.0 or x = +1.0
                    if y_raw_exp == 2047 {
                        // y is infinite
                        return f64::NAN;
                    } else {
                        // y is a large even integer
                        return 1.0;
                    }
                } else {
                    // accuracy, at this magnitude it behaves just like infinity with regards to x
                    if (y > 0.0) ^ (x_raw_exp < 1023) {
                        // or     y = -infinity (or large engouh) and abs(x) < 1.0
                        return f64::INFINITY;
                    } else {
                        // or     y = -infinity (or large engouh) and abs(x) > 1.0
                        return 0.0;
                    }
                }
            } else {
                if y_raw_exp >= 1023 {
                    // y may be an integral value, which should be handled specifically
                    let y_full_mantissa: u64 = IMPLICIT_HIGH_BIT | y_raw_mantissa;
                    if y_raw_exp < 1075 {
                        // normal number with negative shift that may have a fractional part
                        let integral_mask: u64 = 0xFFFFFFFFFFFFFFFF << (1075 - y_raw_exp);
                        if (y_full_mantissa & integral_mask) == y_full_mantissa {
                            // all fractional bits are 0, the number is really integral
                            let l: u64 = y_full_mantissa >> (1075 - y_raw_exp);
                            return F64::pow_i64(x, if y < 0.0 { -(l as i64) } else { l as i64 });
                        }
                    } else {
                        // normal number with positive shift, always an integral value
                        // we know it fits in a primitive long because yRawExp > 1085 has been handled above
                        let l: u64 = y_full_mantissa << (y_raw_exp - 1075);
                        return F64::pow_i64(x, if (y < 0.0) { -(l as i64) } else { l as i64 });
                    }
                }
                if x == 0.0 {
                    // the integer powers have already been handled above
                    return if y < 0.0 { f64::INFINITY } else { 0.0 };
                } else if x_raw_exp == 2047 {
                    if x_raw_mantissa == 0 {
                        // x = -infinity or x = +infinity
                        return if (y < 0.0) { 0.0 } else { f64::INFINITY };
                    } else {
                        // NaN
                        return f64::NAN;
                    }
                } else if x < 0.0 {
                    // the integer powers have already been handled above
                    return f64::NAN;
                } else {
                    // this is the general case, for regular fractional numbers x and y
                    // Split y into ya and yb such that y = ya+yb
                    let tmp: f64 = y * HEX_40000000 as f64;
                    let ya: f64 = (y + tmp) - tmp;
                    let yb: f64 = y - ya;
                    // Compute ln(x)
                    let lores = F64::log_prec(x, true);
                    if lores.0.is_infinite() {
                        // don't allow this to be converted to NaN
                        return lores.0;
                    }
                    let mut lna: f64 = lores.1[0];
                    let mut lnb: f64 = lores.1[1];
                    // resplit lns
                    let tmp1: f64 = lna * HEX_40000000 as f64;
                    let tmp2: f64 = (lna + tmp1) - tmp1;
                    lnb += lna - tmp2;
                    lna = tmp2;
                    // y*ln(x) = (aa+ab)
                    let aa: f64 = lna * ya;
                    let ab: f64 = lna * yb + lnb * ya + lnb * yb;
                    lna = aa + ab;
                    lnb = -(lna - aa - ab);
                    let mut z: f64 = 1.0 / 120.0;
                    z = z * lnb + (1.0 / 24.0);
                    z = z * lnb + (1.0 / 6.0);
                    z = z * lnb + 0.5;
                    z = z * lnb + 1.0;
                    z *= lnb;
                    let result = F64::exp_prec(lna, z, false);
                    // result = result + result * z;
                    return result.0;
                }
            }
        }
    }




    /**
     * Raise a double to a long power.
     *
     * @param d Number to raise.
     * @param e Exponent.
     * @return d<sup>e</sup>
     * @since 3.6
     */
    pub fn pow_i64(d: f64, e: i64) -> f64 {
        if e == 0 {
            return 1.0;
        } else if e > 0 {
            return Split::new(d).pow(e).full;
        } else {
            if e == i64::MIN {
                return Split::new(d).reciprocal().pow(-(i64::MIN + 2)).full;
            }
            return Split::new(d).reciprocal().pow(-e).full;
        }
    }


    /**
     * Raise a double to an int power.
     *
     * @param d Number to raise.
     * @param e Exponent.
     * @return d<sup>e</sup>
     * @since 3.1
     */
    pub fn pow_i32(d: f64, e: i32) -> f64 {
        return F64::pow_i64(d, e as i64);
    }




    pub fn abs(x: f64) -> f64 {
        long_bits_to_double(&(MASK_NON_SIGN_LONG & double_to_raw_long_bits(&x)))
    }
    /**
     * Compute least significant bit (Unit in Last Position) for a number.
     * @param x number from which ulp is requested
     * @return ulp(x)
     */
    pub fn ulp(x: f64) -> f64 {
        if x.is_infinite() {
            f64::INFINITY
        } else {
            F64::abs(x - long_bits_to_double(&(double_to_raw_long_bits(&x) ^ 1)))
        }
    }

    /**
     * Multiply a double number by a power of 2.
     * @param d number to multiply
     * @param n power of 2
     * @return d &times; 2<sup>n</sup>
     */
    pub fn scalb(d: f64, n: i32) -> f64 {

        // first simple and fast handling when 2^n can be represented using normal numbers
        if (n > -1023) && (n < 1024) {
            return d * long_bits_to_double(&(((n + 0x3ff) as u64) << 52));
        }

        // handle special cases
        if d.is_nan() || d.is_infinite() || (d == 0.0) {
            return d;
        }

        if n < -2098 {
            return if d > 0.0 { 0.0 } else { -0.0 };
        }
        if n > 2097 {
            return if d > 0.0 {
                f64::INFINITY
            } else {
                f64::NEG_INFINITY
            };
        }

        // decompose d
        let bits = double_to_raw_long_bits(&d);
        let sign = bits & 0x8000000000000000u64;
        let exponent = ((bits as i64 >> 52) as i32) & 0x7ff;
        let mut mantissa = bits & 0x000fffffffffffff;

        // compute scaled exponent
        let mut scaled_exponent = exponent + n;

        if n < 0 {
            // we are really in the case n <= -1023
            if scaled_exponent > 0 {
                // both the input and the result are normal numbers, we only adjust the exponent
                long_bits_to_double(&(sign | ((scaled_exponent as u64) << 52) | mantissa))
            } else if scaled_exponent > -53 {
                // the input is a normal number and the result is a subnormal number

                // recover the hidden mantissa bit
                mantissa = mantissa | (1u64 << 52);

                // scales down complete mantissa, hence losing least significant bits
                let most_significant_lost_bit = mantissa & (1u64 << (-scaled_exponent));
                mantissa >>= 1 - scaled_exponent;
                if most_significant_lost_bit != 0 {
                    // we need to add 1 bit to round up the result
                    mantissa += 1;
                }
                long_bits_to_double(&(sign | mantissa))

            } else {
                // no need to compute the mantissa, the number scales down to 0
                if sign == 0 { 0.0 } else { -0.0 }
            }
        } else {
            // we are really in the case n >= 1024
            if exponent == 0 {

                // the input number is subnormal, normalize it
                while (mantissa >> 52) != 1 {
                    mantissa <<= 1;
                    scaled_exponent -= 1;
                }
                scaled_exponent += 1;

                mantissa = mantissa & 0x000fffffffffffff;

                if scaled_exponent < 2047 {
                    long_bits_to_double(&(sign | ((scaled_exponent as u64) << 52) | mantissa))
                } else {
                    if sign == 0 {
                        f64::INFINITY
                    } else {
                        f64::NEG_INFINITY
                    }
                }

            } else if scaled_exponent < 2047 {
                long_bits_to_double(&(sign | ((scaled_exponent as u64) << 52) | mantissa))
            } else {
                if sign == 0 {
                    f64::INFINITY
                } else {
                    f64::NEG_INFINITY
                }
            }
        }
    }  // scalb


    /**
     * Get the next machine representable number after a number, moving
     * in the direction of another number.
     * <p>
     * The ordering is as follows (increasing):
     * <ul>
     * <li>-INFINITY</li>
     * <li>-MAX_VALUE</li>
     * <li>-MIN_VALUE</li>
     * <li>-0.0</li>
     * <li>+0.0</li>
     * <li>+MIN_VALUE</li>
     * <li>+MAX_VALUE</li>
     * <li>+INFINITY</li>
     * <li></li>
     * <p>
     * If arguments compare equal, then the second argument is returned.
     * <p>
     * If {@code direction} is greater than {@code d},
     * the smallest machine representable number strictly greater than
     * {@code d} is returned; if less, then the largest representable number
     * strictly less than {@code d} is returned.</p>
     * <p>
     * If {@code d} is infinite and direction does not
     * bring it back to finite numbers, it is returned unchanged.</p>
     *
     * @param d base number
     * @param direction (the only important thing is whether
     * {@code direction} is greater or smaller than {@code d})
     * @return the next machine representable number in the specified direction
     */
    pub fn next_after(d: f64, direction: f64) -> f64 {
        // handling of some important special cases
        if (d.is_nan() || direction.is_nan()) {
            return f64::NAN;
        } else if (d == direction) {
            return direction;
        } else if (d.is_infinite()) {
            return if d < 0.0 { -f64::MAX } else { f64::MAX };
        } else if (d == 0.0) {
            return if direction < 0.0 { -f64::MIN } else { f64::MIN };
        }
        // special cases MAX_VALUE to infinity and  MIN_VALUE to 0
        // are handled just as normal numbers
        // can use raw bits since already dealt with infinity and NaN
        let bits = double_to_raw_long_bits(&d);
        let sign = bits & 0x8000000000000000u64;
        if ((direction < d) ^ (sign == 0)) {
            return long_bits_to_double(&(sign | ((bits & 0x7fffffffffffffff) + 1)));
        } else {
            return long_bits_to_double(&(sign | ((bits & 0x7fffffffffffffff) - 1)));
        }
    }

    /** Get the largest whole number smaller than x.
     * @param x number from which floor is requested
     * @return a double number f such that f is an integer f <= x < f + 1.0
     */
    pub fn floor(x: f64) -> f64 {
        let mut y: i64;
        if x != x {
            // NaN
            return x;
        }
        if x >= TWO_POWER_52 || x <= -TWO_POWER_52 {
            return x;
        }
        y = x as i64;
        if x < 0.0 && y as f64 != x {
            y -= 1;
        }
        if y == 0 {
            return x * y as f64;
        }
        return y as f64;
    }

    /** Get the smallest whole number larger than x.
     * @param x number from which ceil is requested
     * @return a double number c such that c is an integer c - 1.0 < x <= c
     */
    pub fn ceil(x: f64) -> f64 {
        let mut y: f64;
        if x != x {
            // NaN
            return x;
        }
        y = F64::floor(x);
        if y == x {
            return y;
        }
        y += 1.0;
        if y == 0.0 {
            return x * y;
        }
        return y;
    }

    /** Get the whole number that is the nearest to x, or the even one if x is exactly half way between two integers.
     * @param x number from which nearest whole number is requested
     * @return a double number r such that r is an integer r - 0.5 <= x <= r + 0.5
     */
    pub fn rint(x: f64) -> f64 {
        let y: f64 = F64::floor(x);
        let d: f64 = x - y;
        if d > 0.5 {
            if y == -1.0 {
                // Preserve sign of operand
                return -0.0;
            }
            return y + 1.0;
        }
        if d < 0.5 {
            return y;
        }
        // half way, round to even
        let z: i64 = y as i64;
        return if (z & 1) == 0 { y } else { y + 1.0 };
    }

    /** Get the closest long to x.
     * @param x number from which closest long is requested
     * @return closest long to x
     */
    pub fn round(x: f64) -> i64 {
        return F64::floor(x + 0.5) as i64;
    }


    /** Compute the minimum of two values
     * @param a first value
     * @param b second value
     * @return a if a is lesser or equal to b, b otherwise
     */
    pub fn min(a: f64, b: f64) -> f64 {
        if a > b {
            return b;
        }
        if a < b {
            return a;
        }
        // if either arg is NaN, return the other
        if a != a {
            return b;
        }
        if b != b {
            return a;
        }
        // min(+0.0,-0.0) == -0.0
        // 0x8000000000000000L == Double.doubleToRawLongBits(-0.0d)
        let bits: u64 = double_to_raw_long_bits(&a);
        if bits == 0x8000000000000000 {
            return a;
        }
        return b;
    }


    /** Compute the maximum of two values
     * @param a first value
     * @param b second value
     * @return b if a is lesser or equal to b, a otherwise
     */
    pub fn max(a: f64, b: f64) -> f64 {
        if a > b {
            return a;
        }
        if a < b {
            return b;
        }
        // if either arg is NaN, return the other
        if a != a {
            return b;
        }
        if b != b {
            return a;
        }
        // min(+0.0,-0.0) == -0.0
        // 0x8000000000000000L == Double.doubleToRawLongBits(-0.0d)
        let bits: u64 = double_to_raw_long_bits(&a);
        if bits == 0x8000000000000000 {
            return b;
        }
        return a;
    }

    /**
     * Returns the first argument with the sign of the second argument.
     * A NaN {@code sign} argument is treated as positive.
     *
     * @param magnitude the value to return
     * @param sign the sign for the returned value
     * @return the magnitude with the same sign as the {@code sign} argument
     */
    pub fn copy_sign(magnitude: f64, sign: f64) -> f64 {
        // The highest order bit is going to be zero if the
        // highest order bit of m and s is the same and one otherwise.
        // So (m^s) will be positive if both m and s have the same sign
        // and negative otherwise.
        // don't care about NaN
        let m: u64 = double_to_raw_long_bits(&magnitude);
        let s: u64 = double_to_raw_long_bits(&sign);
        if (m as i64 ^ s as i64) >= 0 {
            return magnitude;
        }
        // flip sign
        return -magnitude;
    }

    /**
     *  Computes sin(x) - x, where |x| < 1/16.
     *  Use a Remez polynomial approximation.
     *  @param x a number smaller than 1/16
     *  @return sin(x) - x
     */
    fn poly_sine(x: f64) -> f64 {
        let x2: f64 = x * x;
        let mut p: f64 = 2.7553817452272217E-6;
        p = p * x2 + -1.9841269659586505E-4;
        p = p * x2 + 0.008333333333329196;
        p = p * x2 + -0.16666666666666666;
        // p *= x2;
        // p *= x;
        p = p * x2 * x;
        return p;
    }

    /**
     *  Computes cos(x) - 1, where |x| < 1/16.
     *  Use a Remez polynomial approximation.
     *  @param x a number smaller than 1/16
     *  @return cos(x) - 1
     */
    fn poly_cosine(x: f64) -> f64 {
        let x2: f64 = x * x;
        let mut p: f64 = 2.479773539153719E-5;
        p = p * x2 + -0.0013888888689039883;
        p = p * x2 + 0.041666666666621166;
        p = p * x2 + -0.49999999999999994;
        p *= x2;
        return p;
    }

    /**
     *  Compute sine over the first quadrant (0 < x < pi/2).
     *  Use combination of table lookup and rational polynomial expansion.
     *  @param xa number from which sine is requested
     *  @param xb extra bits for x (may be 0.0)
     *  @return sin(xa + xb)
     */
    fn sin_q(xa: f64, xb: f64) -> f64 {
        let idx: usize = ((xa * 8.0) + 0.5) as usize;
        // idx*0.125;
        let epsilon: f64 = xa - EIGHTHS[idx];
        // Table lookups
        let sint_a: f64 = SINE_TABLE_A[idx];
        let sint_b: f64 = SINE_TABLE_B[idx];
        let cost_a: f64 = COSINE_TABLE_A[idx];
        let cost_b: f64 = COSINE_TABLE_B[idx];
        // Polynomial eval of sin(epsilon), cos(epsilon)
        let mut sin_eps_a: f64 = epsilon;
        let mut sin_eps_b: f64 = F64::poly_sine(epsilon);
        let cos_eps_a: f64 = 1.0;
        let cos_eps_b: f64 = F64::poly_cosine(epsilon);
        // Split epsilon   xa + xb = x
        let temp: f64 = sin_eps_a * HEX_40000000 as f64;
        let temp2: f64 = (sin_eps_a + temp) - temp;
        sin_eps_b += sin_eps_a - temp2;
        sin_eps_a = temp2;
        // Compute sin(x) by angle addition formula
        let result: f64;
        // Compute the following sum:
        //
        // result = sintA + costA*sinEpsA + sintA*cosEpsB + costA*sinEpsB +
        //          sintB + costB*sinEpsA + sintB*cosEpsB + costB*sinEpsB;
        //
        // Ranges of elements
        //
        // xxxtA   0            PI/2
        // xxxtB   -1.5e-9      1.5e-9
        // sinEpsA -0.0625      0.0625
        // sinEpsB -6e-11       6e-11
        // cosEpsA  1.0
        // cosEpsB  0           -0.0625
        //
        //
        // result = sintA + costA*sinEpsA + sintA*cosEpsB + costA*sinEpsB +
        //          sintB + costB*sinEpsA + sintB*cosEpsB + costB*sinEpsB;
        // result = sintA + sintA*cosEpsB + sintB + sintB * cosEpsB;
        // result += costA*sinEpsA + costA*sinEpsB + costB*sinEpsA + costB * sinEpsB;
        let mut a: f64 = 0.0;
        let mut b: f64 = 0.0;
        let mut t: f64 = sint_a;
        let mut c: f64 = a + t;
        let mut d: f64 = -(c - a - t);
        a = c;
        b += d;
        t = cost_a * sin_eps_a;
        c = a + t;
        d = -(c - a - t);
        a = c;
        b += d;
        b = b + sint_a * cos_eps_b + cost_a * sin_eps_b;
        // t = sintA*cosEpsB;
        // c = a + t;
        // d = -(c - a - t);
        // a = c;
        // b = b + d;
        //
        // t = costA*sinEpsB;
        // c = a + t;
        // d = -(c - a - t);
        // a = c;
        // b = b + d;
        //
        b = b + sint_b + cost_b * sin_eps_a + sint_b * cos_eps_b + cost_b * sin_eps_b;
        if xb != 0.0 {
            t = ((cost_a + cost_b) * (cos_eps_a + cos_eps_b) - (sint_a + sint_b) * (sin_eps_a + sin_eps_b)) * // approximate cosine*xb
            xb;
            c = a + t;
            d = -(c - a - t);
            a = c;
            b += d;
        }
        result = a + b;
        return result;
    }


    /**
     * Compute cosine in the first quadrant by subtracting input from PI/2 and
     * then calling sinQ.  This is more accurate as the input approaches PI/2.
     *  @param xa number from which cosine is requested
     *  @param xb extra bits for x (may be 0.0)
     *  @return cos(xa + xb)
     */
    fn cos_q(xa: f64, xb: f64) -> f64 {
        let pi2a: f64 = 1.5707963267948966;
        let pi2b: f64 = 6.123233995736766E-17;
        let a: f64 = pi2a - xa;
        let mut b: f64 = -(a - pi2a + xa);
        b += pi2b - xb;
        return F64::sin_q(a, b);
    }

    /**
     *  Compute tangent (or cotangent) over the first quadrant.   0 < x < pi/2
     *  Use combination of table lookup and rational polynomial expansion.
     *  @param xa number from which sine is requested
     *  @param xb extra bits for x (may be 0.0)
     *  @param cotanFlag if true, compute the cotangent instead of the tangent
     *  @return tan(xa+xb) (or cotangent, depending on cotanFlag)
     */
    fn tan_q(xa: f64, xb: f64, cotan_flag: bool) -> f64 {
        let idx: usize = ((xa * 8.0) + 0.5) as usize;
        // idx*0.125;
        let epsilon: f64 = xa - EIGHTHS[idx];
        // Table lookups
        let sint_a: f64 = SINE_TABLE_A[idx];
        let sint_b: f64 = SINE_TABLE_B[idx];
        let cost_a: f64 = COSINE_TABLE_A[idx];
        let cost_b: f64 = COSINE_TABLE_B[idx];
        // Polynomial eval of sin(epsilon), cos(epsilon)
        let mut sin_eps_a: f64 = epsilon;
        let mut sin_eps_b: f64 = F64::poly_sine(epsilon);
        let cos_eps_a: f64 = 1.0;
        let cos_eps_b: f64 = F64::poly_cosine(epsilon);
        // Split epsilon   xa + xb = x
        let mut temp: f64 = sin_eps_a * HEX_40000000 as f64;
        let temp2: f64 = (sin_eps_a + temp) - temp;
        sin_eps_b += sin_eps_a - temp2;
        sin_eps_a = temp2;
        // Compute sin(x) by angle addition formula
        // Compute the following sum:
        //
        // result = sintA + costA*sinEpsA + sintA*cosEpsB + costA*sinEpsB +
        //          sintB + costB*sinEpsA + sintB*cosEpsB + costB*sinEpsB;
        //
        // Ranges of elements
        //
        // xxxtA   0            PI/2
        // xxxtB   -1.5e-9      1.5e-9
        // sinEpsA -0.0625      0.0625
        // sinEpsB -6e-11       6e-11
        // cosEpsA  1.0
        // cosEpsB  0           -0.0625
        //
        //
        // result = sintA + costA*sinEpsA + sintA*cosEpsB + costA*sinEpsB +
        //          sintB + costB*sinEpsA + sintB*cosEpsB + costB*sinEpsB;
        // result = sintA + sintA*cosEpsB + sintB + sintB * cosEpsB;
        // result += costA*sinEpsA + costA*sinEpsB + costB*sinEpsA + costB * sinEpsB;
        let mut a: f64 = 0.0;
        let mut b: f64 = 0.0;
        // Compute sine
        let mut t: f64 = sint_a;
        let mut c: f64 = a + t;
        let mut d: f64 = -(c - a - t);
        a = c;
        b += d;
        t = cost_a * sin_eps_a;
        c = a + t;
        d = -(c - a - t);
        a = c;
        b += d;
        b += sint_a * cos_eps_b + cost_a * sin_eps_b;
        b += sint_b + cost_b * sin_eps_a + sint_b * cos_eps_b + cost_b * sin_eps_b;
        let mut sina: f64 = a + b;
        let mut sinb: f64 = -(sina - a - b);
        // Compute cosine
        a = 0.0;
        b = 0.0;
        t = cost_a * cos_eps_a;
        c = a + t;
        d = -(c - a - t);
        a = c;
        b += d;
        t = -sint_a * sin_eps_a;
        c = a + t;
        d = -(c - a - t);
        a = c;
        b += d;
        b += cost_b * cos_eps_a + cost_a * cos_eps_b + cost_b * cos_eps_b;
        b -= sint_b * sin_eps_a + sint_a * sin_eps_b + sint_b * sin_eps_b;
        let mut cosa: f64 = a + b;
        let mut cosb: f64 = -(cosa - a - b);
        if cotan_flag {
            let mut tmp: f64;
            tmp = cosa;
            cosa = sina;
            sina = tmp;
            tmp = cosb;
            cosb = sinb;
            sinb = tmp;
        }
        // estimate and correct, compute 1.0/(cosa+cosb)
        // double est = (sina+sinb)/(cosa+cosb);
        // double err = (sina - cosa*est) + (sinb - cosb*est);
        // est += err/(cosa+cosb);
        // err = (sina - cosa*est) + (sinb - cosb*est);
        //
        // f(x) = 1/x,   f'(x) = -1/x^2
        let est: f64 = sina / cosa;
        // Split the estimate to get more accurate read on division rounding
        temp = est * HEX_40000000 as f64;
        let esta: f64 = (est + temp) - temp;
        let estb: f64 = est - esta;
        temp = cosa * HEX_40000000 as f64;
        let cosaa: f64 = (cosa + temp) - temp;
        let cosab: f64 = cosa - cosaa;
        // double err = (sina - est*cosa)/cosa;  // Correction for division rounding
        // Correction for division rounding
        let mut err: f64 = (sina - esta * cosaa - esta * cosab - estb * cosaa - estb * cosab) /
                           cosa;
        // Change in est due to sinb
        err += sinb / cosa;
        // Change in est due to cosb
        err += -sina * cosb / cosa / cosa;
        if xb != 0.0 {
            // tan' = 1 + tan^2      cot' = -(1 + cot^2)
            // Approximate impact of xb
            let mut xbadj: f64 = xb + est * est * xb;
            if cotan_flag {
                xbadj = -xbadj;
            }
            err += xbadj;
        }
        return est + err;
    }



    /** Reduce the input argument using the Payne and Hanek method.
     *  This is good for all inputs 0.0 < x < inf
     *  Output is remainder after dividing by PI/2
     *  The result array should contain 3 numbers.
     *  result[0] is the integer portion, so mod 4 this gives the quadrant.
     *  result[1] is the upper bits of the remainder
     *  result[2] is the lower bits of the remainder
     *
     * @param x number to reduce
     * @param result placeholder where to put the result
     */
    fn reduce_payne_hanek(x: f64) -> [f64; 3] {
        // Convert input double to bits
        let mut inbits: u64 = double_to_raw_long_bits(&x);
        let mut exponent: i32 = ((inbits >> 52) & 0x7ff) as i32 - 1023;
        // Convert to fixed point representation
        inbits &= 0x000fffffffffffff;
        inbits |= 0x0010000000000000;
        // Normalize input to be between 0.5 and 1.0
        exponent += 1;
        inbits <<= 11;
        // Based on the exponent, get a shifted copy of recip2pi
        let mut shpi0: u64;
        let shpi_a: u64;
        let shpi_b: u64;
        let idx: usize = exponent as usize >> 6;
        let shift: usize = exponent as usize - (idx << 6);
        if shift != 0 {
            shpi0 = if (idx == 0) {
                0
            } else {
                (RECIP_2PI[idx - 1] << shift)
            };
            shpi0 |= RECIP_2PI[idx] >> /* >>> */ (64 - shift);
            shpi_a = (RECIP_2PI[idx] << shift) | (RECIP_2PI[idx + 1] >> /* >>> */ (64 - shift));
            shpi_b = (RECIP_2PI[idx + 1] << shift) | (RECIP_2PI[idx + 2] >> /* >>> */ (64 - shift));
        } else {
            shpi0 = if (idx == 0) { 0 } else { RECIP_2PI[idx - 1] };
            shpi_a = RECIP_2PI[idx];
            shpi_b = RECIP_2PI[idx + 1];
        }
        // Multiply input by shpiA
        let mut a: u64 = inbits >> /* >>> */ 32;
        let mut b: u64 = inbits & 0xffffffff;
        let mut c: u64 = shpi_a >> /* >>> */ 32;
        let mut d: u64 = shpi_a & 0xffffffff;
        let mut ac: u64 = a * c;
        let mut bd: u64 = b * d;
        let mut bc: u64 = b * c;
        let mut ad: u64 = a * d;
        let mut prod_b: u64 = bd + (ad << 32);
        let mut prod_a: u64 = ac + (ad >> /* >>> */ 32);
        let mut bita: bool = (bd & 0x8000000000000000) != 0;
        let mut bitb: bool = (ad & 0x80000000) != 0;
        let mut bitsum: bool = (prod_b & 0x8000000000000000) != 0;
        // Carry
        if (bita && bitb) || ((bita || bitb) && !bitsum) {
            prod_a += 1;
        }
        bita = (prod_b & 0x8000000000000000) != 0;
        bitb = (bc & 0x80000000) != 0;
        prod_b += bc << 32;
        prod_a += bc >> /* >>> */ 32;
        bitsum = (prod_b & 0x8000000000000000) != 0;
        // Carry
        if (bita && bitb) || ((bita || bitb) && !bitsum) {
            prod_a += 1;
        }
        // Multiply input by shpiB
        c = shpi_b >> /* >>> */ 32;
        d = shpi_b & 0xffffffff;
        ac = a * c;
        bc = b * c;
        ad = a * d;
        // Collect terms
        ac += (bc + ad) >> /* >>> */ 32;
        bita = (prod_b & 0x8000000000000000) != 0;
        bitb = (ac & 0x8000000000000000) != 0;
        prod_b += ac;
        bitsum = (prod_b & 0x8000000000000000) != 0;
        // Carry
        if (bita && bitb) || ((bita || bitb) && !bitsum) {
            prod_a += 1;
        }
        // Multiply by shpi0
        c = shpi0 >> /* >>> */ 32;
        d = shpi0 & 0xffffffff;
        bd = b * d;
        bc = b * c;
        ad = a * d;
        prod_a += bd + ((bc + ad) << 32);
        // prodA, prodB now contain the remainder as a fraction of PI.  We want this as a fraction of
        // PI/2, so use the following steps:
        // 1.) multiply by 4.
        // 2.) do a fixed point muliply by PI/4.
        // 3.) Convert to floating point.
        // 4.) Multiply by 2
        //
        // This identifies the quadrant
        let int_part: i32 = (prod_a >> /* >>> */ 62) as i32;
        // Multiply by 4
        prod_a <<= 2;
        prod_a |= prod_b >> /* >>> */ 62;
        prod_b <<= 2;
        // Multiply by PI/4
        a = prod_a >> /* >>> */ 32;
        b = prod_a & 0xffffffff;
        c = PI_O_4_BITS[0] >> /* >>> */ 32;
        d = PI_O_4_BITS[0] & 0xffffffff;
        ac = a * c;
        bd = b * d;
        bc = b * c;
        ad = a * d;
        let mut prod2_b: u64 = bd + (ad << 32);
        let mut prod2_a: u64 = ac + (ad >> /* >>> */ 32);
        bita = (bd & 0x8000000000000000) != 0;
        bitb = (ad & 0x80000000) != 0;
        bitsum = (prod2_b & 0x8000000000000000) != 0;
        // Carry
        if (bita && bitb) || ((bita || bitb) && !bitsum) {
            prod2_a += 1;
        }
        bita = (prod2_b & 0x8000000000000000) != 0;
        bitb = (bc & 0x80000000) != 0;
        prod2_b += bc << 32;
        prod2_a += bc >> /* >>> */ 32;
        bitsum = (prod2_b & 0x8000000000000000) != 0;
        // Carry
        if (bita && bitb) || ((bita || bitb) && !bitsum) {
            prod2_a += 1;
        }
        // Multiply input by pio4bits[1]
        c = PI_O_4_BITS[1] >> /* >>> */ 32;
        d = PI_O_4_BITS[1] & 0xffffffff;
        ac = a * c;
        bc = b * c;
        ad = a * d;
        // Collect terms
        ac += (bc + ad) >> /* >>> */ 32;
        bita = (prod2_b & 0x8000000000000000) != 0;
        bitb = (ac & 0x8000000000000000) != 0;
        prod2_b += ac;
        bitsum = (prod2_b & 0x8000000000000000) != 0;
        // Carry
        if (bita && bitb) || ((bita || bitb) && !bitsum) {
            prod2_a += 1;
        }
        // Multiply inputB by pio4bits[0]
        a = prod_b >> /* >>> */ 32;
        b = prod_b & 0xffffffff;
        c = PI_O_4_BITS[0] >> /* >>> */ 32;
        d = PI_O_4_BITS[0] & 0xffffffff;
        ac = a * c;
        bc = b * c;
        ad = a * d;
        // Collect terms
        ac += (bc + ad) >> /* >>> */ 32;
        bita = (prod2_b & 0x8000000000000000) != 0;
        bitb = (ac & 0x8000000000000000) != 0;
        prod2_b += ac;
        bitsum = (prod2_b & 0x8000000000000000) != 0;
        // Carry
        if (bita && bitb) || ((bita || bitb) && !bitsum) {
            prod2_a += 1;
        }
        // Convert to double
        // High order 52 bits
        let tmp_a: f64 = (prod2_a >> /* >>> */ 12) as f64 / TWO_POWER_52;
        // Low bits
        let tmp_b: f64 = (((prod2_a & 0xfff) << 40) + (prod2_b >> /* >>> */ 24)) as f64 /
                         TWO_POWER_52 / TWO_POWER_52;
        let sum_a: f64 = tmp_a + tmp_b;
        let sum_b: f64 = -(sum_a - tmp_a - tmp_b);
        // Multiply by PI/2 and return
        [int_part as f64, sum_a * 2.0, sum_b * 2.0]
    }

    /**
     * Sine function.
     *
     * @param x Argument.
     * @return sin(x)
     */
    pub fn sin(x: f64) -> f64 {
        let mut negative: bool = false;
        let mut quadrant: i32 = 0;
        let mut xa: f64;
        let mut xb: f64 = 0.0;
        // Take absolute value of the input
        xa = x;
        if x < 0.0 {
            negative = true;
            xa = -xa;
        }
        // Check for zero and negative zero
        if xa == 0.0 {
            let bits: u64 = double_to_raw_long_bits(&x);
            if bits & 0x8000000000000000 != 0 {
                return -0.0;
            }
            return 0.0;
        }
        if xa != xa || xa == f64::INFINITY {
            return f64::NAN;
        }
        // Perform any argument reduction
        if xa > 3294198.0 {
            // PI * (2**20)
            // Argument too big for CodyWaite reduction.  Must use
            // PayneHanek.
            let reduce_results = F64::reduce_payne_hanek(xa);
            quadrant = (reduce_results[0] as i32) & 3;
            xa = reduce_results[1];
            xb = reduce_results[2];
        } else if xa > 1.5707963267948966 {
            let cw: CodyWaite = CodyWaite::new(xa);
            quadrant = cw.get_k() & 3;
            xa = cw.get_rem_a();
            xb = cw.get_rem_b();
        }
        if negative {
            // Flip bit 1
            quadrant ^= 2;
        }
        match quadrant {
            0 => {
                return F64::sin_q(xa, xb);
            }
            1 => {
                return F64::cos_q(xa, xb);
            }
            2 => {
                return -F64::sin_q(xa, xb);
            }
            3 => {
                return -F64::cos_q(xa, xb);
            }
            _ => {
                return f64::NAN;
            }
        }
    }

    /**
     * Cosine function.
     *
     * @param x Argument.
     * @return cos(x)
     */
    pub fn cos(x: f64) -> f64 {
        let mut quadrant: i32 = 0;
        // Take absolute value of the input
        let mut xa: f64 = x;
        if x < 0.0 {
            xa = -xa;
        }
        if xa != xa || xa == f64::INFINITY {
            return f64::NAN;
        }
        // Perform any argument reduction
        let mut xb: f64 = 0.0;
        if xa > 3294198.0 {
            // PI * (2**20)
            // Argument too big for CodyWaite reduction.  Must use
            // PayneHanek.
            let reduce_results = F64::reduce_payne_hanek(xa);
            quadrant = (reduce_results[0] as i32) & 3;
            xa = reduce_results[1];
            xb = reduce_results[2];
        } else if xa > 1.5707963267948966 {
            let cw: CodyWaite = CodyWaite::new(xa);
            quadrant = cw.get_k() & 3;
            xa = cw.get_rem_a();
            xb = cw.get_rem_b();
        }
        match quadrant {
            0 => {
                return F64::cos_q(xa, xb);
            }
            1 => {
                return -F64::sin_q(xa, xb);
            }
            2 => {
                return -F64::cos_q(xa, xb);
            }
            3 => {
                return F64::sin_q(xa, xb);
            }
            _ => {
                return f64::NAN;
            }
        }
    }

    /**
     * Tangent function.
     *
     * @param x Argument.
     * @return tan(x)
     */
    pub fn tan(x: f64) -> f64 {
        let mut negative: bool = false;
        let mut quadrant: i32 = 0;
        // Take absolute value of the input
        let mut xa: f64 = x;
        if x < 0.0 {
            negative = true;
            xa = -xa;
        }
        // Check for zero and negative zero
        if xa == 0.0 {
            let bits: u64 = double_to_raw_long_bits(&x);
            if bits & 0x8000000000000000 != 0 {
                return -0.0;
            }
            return 0.0;
        }
        if xa != xa || xa == f64::INFINITY {
            return f64::NAN;
        }
        // Perform any argument reduction
        let mut xb: f64 = 0.0;
        if xa > 3294198.0 {
            // PI * (2**20)
            // Argument too big for CodyWaite reduction.  Must use
            // PayneHanek.
            let reduce_results = F64::reduce_payne_hanek(xa);
            quadrant = (reduce_results[0] as i32) & 3;
            xa = reduce_results[1];
            xb = reduce_results[2];
        } else if xa > 1.5707963267948966 {
            let cw: CodyWaite = CodyWaite::new(xa);
            quadrant = cw.get_k() & 3;
            xa = cw.get_rem_a();
            xb = cw.get_rem_b();
        }
        if xa > 1.5 {
            // Accuracy suffers between 1.5 and PI/2
            let pi2a: f64 = 1.5707963267948966;
            let pi2b: f64 = 6.123233995736766E-17;
            let a: f64 = pi2a - xa;
            let mut b: f64 = -(a - pi2a + xa);
            b += pi2b - xb;
            xa = a + b;
            xb = -(xa - a - b);
            quadrant ^= 1;
            negative ^= true;
        }
        let mut result: f64;
        if (quadrant & 1) == 0 {
            result = F64::tan_q(xa, xb, false);
        } else {
            result = -F64::tan_q(xa, xb, true);
        }
        if negative {
            result = -result;
        }
        return result;
    }


    /**
     * Arctangent function
     *  @param x a number
     *  @return atan(x)
     */
    pub fn atan(x: f64) -> f64 {
        return F64::atan_xa_xb_left_plane(x, 0.0, false);
    }

    /** Internal helper function to compute arctangent.
     * @param xa number from which arctangent is requested
     * @param xb extra bits for x (may be 0.0)
     * @param leftPlane if true, result angle must be put in the left half plane
     * @return atan(xa + xb) (or angle shifted by {@code PI} if leftPlane is true)
     */
    fn atan_xa_xb_left_plane(xa_p: f64, xb_p: f64, left_plane: bool) -> f64 {
        let mut xa = xa_p;
        let mut xb = xb_p;
        if xa == 0.0 {
            // Matches +/- 0.0; return correct sign
            return if left_plane {
                F64::copy_sign(consts::PI, xa)
            } else {
                xa
            };
        }
        let negate: bool;
        if xa < 0.0 {
            // negative
            xa = -xa;
            xb = -xb;
            negate = true;
        } else {
            negate = false;
        }
        if xa > 1.633123935319537E16 {
            // Very large input
            return if (negate ^ left_plane) {
                (-consts::PI * F_1_2)
            } else {
                (consts::PI * F_1_2)
            };
        }
        // Estimate the closest tabulated arctan value, compute eps = xa-tangentTable
        let idx: usize;
        if xa < 1.0 {
            idx = (((-1.7168146928204136 * xa * xa + 8.0) * xa) + 0.5) as usize;
        } else {
            let one_over_xa: f64 = 1.0 / xa;
            let tmp = (-((-1.7168146928204136 * one_over_xa * one_over_xa + 8.0) * one_over_xa) +
                       13.07);
            idx = if tmp.is_nan() { 0 } else { tmp as usize } // java handling of conversion of NaN to int
        }
        let tt_a: f64 = TANGENT_TABLE_A[idx];
        let tt_b: f64 = TANGENT_TABLE_B[idx];
        let mut eps_a: f64 = xa - tt_a;
        let mut eps_b: f64 = -(eps_a - xa + tt_a);
        eps_b += xb - tt_b;
        let mut temp: f64 = eps_a + eps_b;
        eps_b = -(temp - eps_a - eps_b);
        eps_a = temp;
        // Compute eps = eps / (1.0 + xa*tangent)
        temp = xa * HEX_40000000 as f64;
        let mut ya: f64 = xa + temp - temp;
        let mut yb: f64 = xb + xa - ya;
        xa = ya;
        xb += yb;
        // if (idx > 8 || idx == 0)
        if idx == 0 {
            // If the slope of the arctan is gentle enough (< 0.45), this approximation will suffice
            // double denom = 1.0 / (1.0 + xa*tangentTableA[idx] + xb*tangentTableA[idx] + xa*tangentTableB[idx] + xb*tangentTableB[idx]);
            let denom: f64 = 1.0 / (1.0 + (xa + xb) * (tt_a + tt_b));
            // double denom = 1.0 / (1.0 + xa*tangentTableA[idx]);
            ya = eps_a * denom;
            yb = eps_b * denom;
        } else {
            let mut temp2: f64 = xa * tt_a;
            let mut za: f64 = 1.0 + temp2;
            let mut zb: f64 = -(za - 1.0 - temp2);
            temp2 = xb * tt_a + xa * tt_b;
            temp = za + temp2;
            zb += -(temp - za - temp2);
            za = temp;
            zb += xb * tt_b;
            ya = eps_a / za;
            temp = ya * HEX_40000000 as f64;
            let yaa: f64 = (ya + temp) - temp;
            let yab: f64 = ya - yaa;
            temp = za * HEX_40000000 as f64;
            let zaa: f64 = (za + temp) - temp;
            let zab: f64 = za - zaa;
            // Correct for rounding in division
            yb = (eps_a - yaa * zaa - yaa * zab - yab * zaa - yab * zab) / za;
            yb += -eps_a * zb / za / za;
            yb += eps_b / za;
        }
        eps_a = ya;
        eps_b = yb;
        // Evaluate polynomial
        let eps_a2: f64 = eps_a * eps_a;
        // yb = -0.09001346640161823;
        // yb = yb * epsA2 + 0.11110718400605211;
        // yb = yb * epsA2 + -0.1428571349122913;
        // yb = yb * epsA2 + 0.19999999999273194;
        // yb = yb * epsA2 + -0.33333333333333093;
        // yb = yb * epsA2 * epsA;
        //
        yb = 0.07490822288864472;
        yb = yb * eps_a2 - 0.09088450866185192;
        yb = yb * eps_a2 + 0.11111095942313305;
        yb = yb * eps_a2 - 0.1428571423679182;
        yb = yb * eps_a2 + 0.19999999999923582;
        yb = yb * eps_a2 - 0.33333333333333287;
        yb = yb * eps_a2 * eps_a;
        ya = eps_a;
        temp = ya + yb;
        yb = -(temp - ya - yb);
        ya = temp;
        // Add in effect of epsB.   atan'(x) = 1/(1+x^2)
        yb += eps_b / (1.0 + eps_a * eps_a);
        let eighths: f64 = EIGHTHS[idx];
        // result = yb + eighths[idx] + ya;
        let mut za: f64 = eighths + ya;
        let mut zb: f64 = -(za - eighths - ya);
        temp = za + yb;
        zb += -(temp - za - yb);
        za = temp;
        let mut result: f64 = za + zb;
        if left_plane {
            // Result is in the left plane
            let resultb: f64 = -(result - za - zb);
            let pia: f64 = 1.5707963267948966 * 2.0;
            let pib: f64 = 6.123233995736766E-17 * 2.0;
            za = pia - result;
            zb = -(za - pia + result);
            zb += pib - resultb;
            result = za + zb;
        }
        if negate ^ left_plane {
            result = -result;
        }
        return result;
    }

    /**
     * Two arguments arctangent function
     * @param y ordinate
     * @param x abscissa
     * @return phase angle of point (x,y) between {@code -PI} and {@code PI}
     */
    pub fn atan2(y: f64, x: f64) -> f64 {
        if x != x || y != y {
            return f64::NAN;
        }
        if y == 0.0 {
            let result: f64 = x * y;
            let invx: f64 = 1.0 / x;
            let invy: f64 = 1.0 / y;
            if invx == 0.0 {
                // X is infinite
                if x > 0.0 {
                    // return +/- 0.0
                    return y;
                } else {
                    return F64::copy_sign(consts::PI, y);
                }
            }
            if x < 0.0 || invx < 0.0 {
                if y < 0.0 || invy < 0.0 {
                    return -consts::PI;
                } else {
                    return consts::PI;
                }
            } else {
                return result;
            }
        }
        if y == f64::INFINITY {
            if x == f64::INFINITY {
                return consts::PI * F_1_4;
            }
            if x == f64::NEG_INFINITY {
                return consts::PI * F_3_4;
            }
            return consts::PI * F_1_2;
        }
        if y == f64::NEG_INFINITY {
            if x == f64::INFINITY {
                return -consts::PI * F_1_4;
            }
            if x == f64::NEG_INFINITY {
                return -consts::PI * F_3_4;
            }
            return -consts::PI * F_1_2;
        }
        if x == f64::INFINITY {
            if y > 0.0 || 1.0 / y > 0.0 {
                return 0.0;
            }
            if y < 0.0 || 1.0 / y < 0.0 {
                return -0.0;
            }
        }
        if x == f64::NEG_INFINITY {
            if y > 0.0 || 1.0 / y > 0.0 {
                return consts::PI;
            }
            if y < 0.0 || 1.0 / y < 0.0 {
                return -consts::PI;
            }
        }
        if x == 0.0 {
            if y > 0.0 || 1.0 / y > 0.0 {
                return consts::PI * F_1_2;
            }
            if y < 0.0 || 1.0 / y < 0.0 {
                return -consts::PI * F_1_2;
            }
        }
        // Compute ratio r = y/x
        let r: f64 = y / x;
        if r.is_infinite() {
            // bypass calculations that can create NaN
            return F64::atan_xa_xb_left_plane(r, 0.0, x < 0.0);
        }
        let mut ra: f64 = F64::double_high_part(r);
        let mut rb: f64 = r - ra;
        // Split x
        let xa: f64 = F64::double_high_part(x);
        let xb: f64 = x - xa;
        rb += (y - ra * xa - ra * xb - rb * xa - rb * xb) / x;
        let temp: f64 = ra + rb;
        rb = -(temp - ra - rb);
        ra = temp;
        if ra == 0.0 {
            // Fix up the sign so atan works correctly
            ra = F64::copy_sign(0.0, y);
        }
        // Call atan
        let result: f64 = F64::atan_xa_xb_left_plane(ra, rb, x < 0.0);
        return result;
    }

    /** Compute the arc sine of a number.
     * @param x number on which evaluation is done
     * @return arc sine of x
     */
    pub fn asin(x: f64) -> f64 {
        if x != x {
            return f64::NAN;
        }
        if x > 1.0 || x < -1.0 {
            return f64::NAN;
        }
        if x == 1.0 {
            return consts::PI / 2.0;
        }
        if x == -1.0 {
            return -consts::PI / 2.0;
        }
        if x == 0.0 {
            // Matches +/- 0.0; return correct sign
            return x;
        }
        // Compute asin(x) = atan(x/sqrt(1-x*x))
        // Split x
        let mut temp: f64 = x * HEX_40000000 as f64;
        let xa: f64 = x + temp - temp;
        let xb: f64 = x - xa;
        // Square it
        let mut ya: f64 = xa * xa;
        let mut yb: f64 = xa * xb * 2.0 + xb * xb;
        // Subtract from 1
        ya = -ya;
        yb = -yb;
        let mut za: f64 = 1.0 + ya;
        let mut zb: f64 = -(za - 1.0 - ya);
        temp = za + yb;
        zb += -(temp - za - yb);
        za = temp;
        // Square root
        let y: f64;
        y = F64::sqrt(za);
        temp = y * HEX_40000000 as f64;
        ya = y + temp - temp;
        yb = y - ya;
        // Extend precision of sqrt

        yb += (za - ya * ya - 2.0 * ya * yb - yb * yb) / (2.0 * y);
        // Contribution of zb to sqrt
        let dx: f64 = zb / (2.0 * y);
        // Compute ratio r = x/y
        let r: f64 = x / y;
        temp = r * HEX_40000000 as f64;
        let mut ra: f64 = r + temp - temp;
        let mut rb: f64 = r - ra;
        // Correct for rounding in division
        rb += (x - ra * ya - ra * yb - rb * ya - rb * yb) / y;
        // Add in effect additional bits of sqrt.
        rb += -x * dx / y / y;
        temp = ra + rb;
        rb = -(temp - ra - rb);
        ra = temp;
        return F64::atan_xa_xb_left_plane(ra, rb, false);
    }

    /** Compute the arc cosine of a number.
     * @param x number on which evaluation is done
     * @return arc cosine of x
     */
    pub fn acos(x: f64) -> f64 {
        if x != x {
            return f64::NAN;
        }
        if x > 1.0 || x < -1.0 {
            return f64::NAN;
        }
        if x == -1.0 {
            return consts::PI;
        }
        if x == 1.0 {
            return 0.0;
        }
        if x == 0.0 {
            return consts::PI / 2.0;
        }
        // Compute acos(x) = atan(sqrt(1-x*x)/x)
        // Split x
        let mut temp: f64 = x * HEX_40000000 as f64;
        let xa: f64 = x + temp - temp;
        let xb: f64 = x - xa;
        // Square it
        let mut ya: f64 = xa * xa;
        let mut yb: f64 = xa * xb * 2.0 + xb * xb;
        // Subtract from 1
        ya = -ya;
        yb = -yb;
        let mut za: f64 = 1.0 + ya;
        let mut zb: f64 = -(za - 1.0 - ya);
        temp = za + yb;
        zb += -(temp - za - yb);
        za = temp;
        // Square root
        let mut y: f64 = F64::sqrt(za);
        temp = y * HEX_40000000 as f64;
        ya = y + temp - temp;
        yb = y - ya;
        // Extend precision of sqrt
        yb += (za - ya * ya - 2.0 * ya * yb - yb * yb) / (2.0 * y);
        // Contribution of zb to sqrt
        yb += zb / (2.0 * y);
        y = ya + yb;
        yb = -(y - ya - yb);
        // Compute ratio r = y/x
        let r: f64 = y / x;
        // Did r overflow?
        if r.is_infinite() {
            // so return the appropriate value
            return consts::PI / 2.0;
        }
        let mut ra: f64 = F64::double_high_part(r);
        let mut rb: f64 = r - ra;
        // Correct for rounding in division
        rb += (y - ra * xa - ra * xb - rb * xa - rb * xb) / x;
        // Add in effect additional bits of sqrt.
        rb += yb / x;
        temp = ra + rb;
        rb = -(temp - ra - rb);
        ra = temp;
        return F64::atan_xa_xb_left_plane(ra, rb, x < 0.0);
    }

    /** Compute the cubic root of a number.
     * @param x number on which evaluation is done
     * @return cubic root of x
     */
    pub fn cbrt(x_p: f64) -> f64 {
        let mut x = x_p;
        // Convert input double to bits
        let mut inbits: u64 = double_to_raw_long_bits(&x);
        let mut exponent: i32 = ((inbits >> 52) & 0x7ff) as i32 - 1023;
        let mut subnormal: bool = false;
        if exponent == -1023 {
            if x == 0.0 {
                return x;
            }
            // Subnormal, so normalize
            subnormal = true;
            // 2^54
            x *= 1.8014398509481984E16;
            inbits = double_to_raw_long_bits(&x);
            exponent = ((inbits >> 52) & 0x7ff) as i32 - 1023;
        }
        if exponent == 1024 {
            // Nan or infinity.  Don't care which.
            return x;
        }
        // Divide the exponent by 3
        let exp3: i32 = exponent / 3;
        // p2 will be the nearest power of 2 to x with its exponent divided by 3
        // let a = exp3 + 1023;
        // let b = a & 0x7fff;
        // let c = (b as i64) << 52;
        // let d = (inbits & 0x8000000000000000) | c as u64;
        // let p2: f64 = long_bits_to_double(&d);
        //
        let p2: f64 = long_bits_to_double(&((inbits & 0x8000000000000000) |
                                            (((exp3 + 1023) & 0x7ff) as u64) << 52));
        // This will be a number between 1 and 2
        let mant: f64 = long_bits_to_double(&((inbits & 0x000fffffffffffff) | 0x3ff0000000000000));
        // Estimate the cube root of mant by polynomial
        let mut est: f64 = -0.010714690733195933;
        est = est * mant + 0.0875862700108075;
        est = est * mant + -0.3058015757857271;
        est = est * mant + 0.7249995199969751;
        est = est * mant + 0.5039018405998233;
        est *= CBRTTWO[(exponent % 3 + 2) as usize];
        // est should now be good to about 15 bits of precision.   Do 2 rounds of
        // Newton's method to get closer,  this should get us full double precision
        // Scale down x for the purpose of doing newtons method.  This avoids over/under flows.
        let xs: f64 = x / (p2 * p2 * p2);
        est += (xs - est * est * est) / (3.0 * est * est);
        est += (xs - est * est * est) / (3.0 * est * est);
        // Do one round of Newton's method in extended precision to get the last bit right.
        let mut temp: f64 = est * HEX_40000000 as f64;
        let ya: f64 = est + temp - temp;
        let yb: f64 = est - ya;
        let mut za: f64 = ya * ya;
        let mut zb: f64 = ya * yb * 2.0 + yb * yb;
        temp = za * HEX_40000000 as f64;
        let temp2: f64 = za + temp - temp;
        zb += za - temp2;
        za = temp2;
        zb = za * yb + ya * zb + zb * yb;
        za *= ya;
        let na: f64 = xs - za;
        let mut nb: f64 = -(na - xs + za);
        nb -= zb;
        est += (na + nb) / (3.0 * est * est);
        // Scale by a power of two, so this is exact.
        est *= p2;
        if subnormal {
            // 2^-18
            est *= 3.814697265625E-6;
        }
        return est;
    }


    /**
     *  Convert degrees to radians, with error of less than 0.5 ULP
     *  @param x angle in degrees
     *  @return x converted into radians
     */
    pub fn to_radians(x: f64) -> f64 {
        if x.is_infinite() || x == 0.0 {
            // Matches +/- 0.0; return correct sign
            return x;
        }
        // These are PI/180 split into high and low order bits
        let facta: f64 = 0.01745329052209854;
        let factb: f64 = 1.997844754509471E-9;
        let xa: f64 = F64::double_high_part(x);
        let xb: f64 = x - xa;
        let mut result: f64 = xb * factb + xb * facta + xa * factb + xa * facta;
        if result == 0.0 {
            // ensure correct sign if calculation underflows
            result *= x;
        }
        return result;
    }

    /**
     *  Convert radians to degrees, with error of less than 0.5 ULP
     *  @param x angle in radians
     *  @return x converted into degrees
     */
    pub fn to_degrees(x: f64) -> f64 {
        if x.is_infinite() || x == 0.0 {
            // Matches +/- 0.0; return correct sign
            return x;
        }
        // These are 180/PI split into high and low order bits
        let facta: f64 = 57.2957763671875;
        let factb: f64 = 3.145894820876798E-6;
        let xa: f64 = F64::double_high_part(x);
        let xb: f64 = x - xa;
        return xb * factb + xb * facta + xa * factb + xa * facta;
    }

    /**
     * Returns the hypotenuse of a triangle with sides {@code x} and {@code y}
     * - sqrt(<i>x</i><sup>2</sup> +<i>y</i><sup>2</sup>)<br/>
     * avoiding intermediate overflow or underflow.
     *
     * <ul>
     * <li> If either argument is infinite, then the result is positive infinity.</li>
     * <li> else, if either argument is NaN then the result is NaN.</li>
     * </ul>
     *
     * @param x a value
     * @param y a value
     * @return sqrt(<i>x</i><sup>2</sup> +<i>y</i><sup>2</sup>)
     */
    pub fn hypot(x: f64, y: f64) -> f64 {
        if x.is_infinite() || y.is_infinite() {
            return f64::INFINITY;
        } else if f64::is_nan(x) || f64::is_nan(y) {
            return f64::NAN;
        } else {
            let exp_x: i32 = F64::get_exponent(x);
            let exp_y: i32 = F64::get_exponent(y);
            if exp_x > exp_y + 27 {
                // y is neglectible with respect to x
                return F64::abs(x);
            } else if exp_y > exp_x + 27 {
                // x is neglectible with respect to y
                return F64::abs(y);
            } else {
                // find an intermediate scale to avoid both overflow and underflow
                let middle_exp: i32 = (exp_x + exp_y) / 2;
                // scale parameters without losing precision
                let scaled_x: f64 = F64::scalb(x, -middle_exp);
                let scaled_y: f64 = F64::scalb(y, -middle_exp);
                // compute scaled hypotenuse
                let scaled_h: f64 = F64::sqrt(scaled_x * scaled_x + scaled_y * scaled_y);
                // remove scaling
                return F64::scalb(scaled_h, middle_exp);
            }
        }
    }

    /**
     * Return the exponent of a double number, removing the bias.
     * <p>
     * For double numbers of the form 2<sup>x</sup>, the unbiased
     * exponent is exactly x.
     * </p>
     * @param d number from which exponent is requested
     * @return exponent for d in IEEE754 representation, without bias
     */
    pub fn get_exponent(d: f64) -> i32 {
        // NaN and Infinite will return 1024 anywho so can use raw bits
        return ((double_to_raw_long_bits(&d) >> /* >>> */ 52) & 0x7ff) as i32 - 1023;
    }
}

pub struct F32 {
	
	
}
impl F32 {
    pub fn abs(x: f32) -> f32 {
        int_bits_to_float(&(MASK_NON_SIGN_INT & float_to_raw_int_bits(&x)))
    }
    /**
     * Compute least significant bit (Unit in Last Position) for a number.
     * @param x number from which ulp is requested
     * @return ulp(x)
     */
    pub fn ulp(x: f32) -> f32 {
        if x.is_infinite() {
            f32::INFINITY
        } else {
            F32::abs(x - int_bits_to_float(&(float_to_raw_int_bits(&x) ^ 1)))
        }
    }

    /**
     * Multiply a double number by a power of 2.
     * @param d number to multiply
     * @param n power of 2
     * @return d &times; 2<sup>n</sup>
     */
    pub fn scalb(d: f32, n: i32) -> f32 {

        // first simple and fast handling when 2^n can be represented using normal numbers
        if (n > -127) && (n < 128) {
            return d * int_bits_to_float(&(((n + 127) as u32) << 23));
        }

        // handle special cases
        if d.is_nan() || d.is_infinite() || (d == 0.0) {
            return d;
        }

        if n < -277 {
            return if d > 0.0 { 0.0 } else { -0.0 };
        }
        if n > 276 {
            return if d > 0.0 {
                f32::INFINITY
            } else {
                f32::NEG_INFINITY
            };
        }

        // decompose d
        let bits = float_to_raw_int_bits(&d);
        let sign = bits & 0x80000000u32;
        let exponent = ((bits as i32 >> 23) as i32) & 0xff;
        let mut mantissa = bits & 0x007fffff;

        // compute scaled exponent
        let mut scaled_exponent = exponent + n;

        if n < 0 {
            // we are really in the case n <= -127
            if scaled_exponent > 0 {
                // both the input and the result are normal numbers, we only adjust the exponent
                int_bits_to_float(&(sign | ((scaled_exponent as u32) << 23) | mantissa))
            } else if scaled_exponent > -24 {
                // the input is a normal number and the result is a subnormal number

                // recover the hidden mantissa bit
                mantissa = mantissa | (1u32 << 23);

                // scales down complete mantissa, hence losing least significant bits
                let most_significant_lost_bit = mantissa & (1u32 << (-scaled_exponent));
                mantissa >>= 1 - scaled_exponent;
                if most_significant_lost_bit != 0 {
                    // we need to add 1 bit to round up the result
                    mantissa += 1;
                }
                int_bits_to_float(&(sign | mantissa))

            } else {
                // no need to compute the mantissa, the number scales down to 0
                if sign == 0 { 0.0 } else { -0.0 }
            }
        } else {
            // we are really in the case n >= 128
            if exponent == 0 {

                // the input number is subnormal, normalize it
                while (mantissa >> 23) != 1 {
                    mantissa <<= 1;
                    scaled_exponent -= 1;
                }
                scaled_exponent += 1;

                mantissa = mantissa & 0x007fffff;

                if scaled_exponent < 255 {
                    int_bits_to_float(&(sign | ((scaled_exponent as u32) << 23) | mantissa))
                } else {
                    if sign == 0 {
                        f32::INFINITY
                    } else {
                        f32::NEG_INFINITY
                    }
                }

            } else if scaled_exponent < 255 {
                int_bits_to_float(&(sign | ((scaled_exponent as u32) << 23) | mantissa))
            } else {
                if sign == 0 {
                    f32::INFINITY
                } else {
                    f32::NEG_INFINITY
                }
            }
        }
    }  // scalb

    /**
     * Get the next machine representable number after a number, moving
     * in the direction of another number.
     * <p>
     * The ordering is as follows (increasing):
     * <ul>
     * <li>-INFINITY</li>
     * <li>-MAX_VALUE</li>
     * <li>-MIN_VALUE</li>
     * <li>-0.0</li>
     * <li>+0.0</li>
     * <li>+MIN_VALUE</li>
     * <li>+MAX_VALUE</li>
     * <li>+INFINITY</li>
     * <li></li>
     * <p>
     * If arguments compare equal, then the second argument is returned.
     * <p>
     * If {@code direction} is greater than {@code f},
     * the smallest machine representable number strictly greater than
     * {@code f} is returned; if less, then the largest representable number
     * strictly less than {@code f} is returned.</p>
     * <p>
     * If {@code f} is infinite and direction does not
     * bring it back to finite numbers, it is returned unchanged.</p>
     *
     * @param f base number
     * @param direction (the only important thing is whether
     * {@code direction} is greater or smaller than {@code f})
     * @return the next machine representable number in the specified direction
     */
    pub fn next_after(d: f32, direction: f32) -> f32 {
        // handling of some important special cases
        if (d.is_nan() || direction.is_nan()) {
            return f32::NAN;
        } else if (d == direction) {
            return direction;
        } else if (d.is_infinite()) {
            return if d < 0.0 { -f32::MAX } else { f32::MAX };
        } else if (d == 0.0) {
            return if direction < 0.0 { -f32::MIN } else { f32::MIN };
        }
        // special cases MAX_VALUE to infinity and  MIN_VALUE to 0
        // are handled just as normal numbers
        // can use raw bits since already dealt with infinity and NaN
        let bits = float_to_raw_int_bits(&d);
        let sign = bits & 0x80000000u32;
        if ((direction < d) ^ (sign == 0)) {
            return int_bits_to_float(&(sign | ((bits & 0x7fffffff) + 1)));
        } else {
            return int_bits_to_float(&(sign | ((bits & 0x7fffffff) - 1)));
        }

    }
    /** Get the closest int to x.
     * @param x number from which closest int is requested
     * @return closest int to x
     */
    pub fn round(x: f32) -> i32 {
        return F64::floor(x as f64 + 0.5) as i32;
    }


    /** Compute the minimum of two values
     * @param a first value
     * @param b second value
     * @return a if a is lesser or equal to b, b otherwise
     */
    pub fn min(a: f32, b: f32) -> f32 {
        if a > b {
            return b;
        }
        if a < b {
            return a;
        }
        // if either arg is NaN, return the other
        if a != a {
            return b;
        }
        if b != b {
            return a;
        }
        // min(+0.0,-0.0) == -0.0
        // 0x80000000 == Float.floatToRawIntBits(-0.0d)
        let bits: u32 = float_to_raw_int_bits(&a);
        if bits == 0x80000000 {
            return a;
        }
        return b;
    }

    /** Compute the maximum of two values
     * @param a first value
     * @param b second value
     * @return b if a is lesser or equal to b, a otherwise
     */
    pub fn max(a: f32, b: f32) -> f32 {
        if a > b {
            return a;
        }
        if a < b {
            return b;
        }
        // if either arg is NaN, return the other
        if a != a {
            return b;
        }
        if b != b {
            return a;
        }
        // min(+0.0,-0.0) == -0.0
        // 0x80000000 == Float.floatToRawIntBits(-0.0d)
        let bits: u32 = float_to_raw_int_bits(&a);
        if bits == 0x80000000 {
            return b;
        }
        return a;
    }

    /** Compute the signum of a number.
     * The signum is -1 for negative numbers, +1 for positive numbers and 0 otherwise
     * @param a number on which evaluation is done
     * @return -1.0, -0.0, +0.0, +1.0 or NaN depending on sign of a
     */
    pub fn signum(a: f32) -> f32 {
        // return +0.0/-0.0/NaN depending on a
        return if (a < 0.0) {
            -1.0
        } else {
            (if (a > 0.0) { 1.0 } else { a })
        };
    }

    /**
     * Returns the first argument with the sign of the second argument.
     * A NaN {@code sign} argument is treated as positive.
     *
     * @param magnitude the value to return
     * @param sign the sign for the returned value
     * @return the magnitude with the same sign as the {@code sign} argument
     */
    pub fn copy_sign(magnitude: f32, sign: f32) -> f32 {
        // The highest order bit is going to be zero if the
        // highest order bit of m and s is the same and one otherwise.
        // So (m^s) will be positive if both m and s have the same sign
        // and negative otherwise.
        let m: u32 = float_to_raw_int_bits(&magnitude);
        let s: u32 = float_to_raw_int_bits(&sign);
        if (m as i32 ^ s as i32) >= 0 {
            return magnitude;
        }
        // flip sign
        return -magnitude;
    }

    /** Compute next number towards positive infinity.
     * @param a number to which neighbor should be computed
     * @return neighbor of a towards positive infinity
     */
    pub fn next_up(a: f32) -> f32 {
        return F32::next_after(a, f32::INFINITY);
    }

    /** Compute next number towards negative infinity.
     * @param a number to which neighbor should be computed
     * @return neighbor of a towards negative infinity
     * @since 3.4  
     */
    pub fn next_down(a: f32) -> f32 {
        return F32::next_after(a, f32::NEG_INFINITY);
    }


    /**
     * Return the exponent of a float number, removing the bias.
     * <p>
     * For float numbers of the form 2<sup>x</sup>, the unbiased
     * exponent is exactly x.
     * </p>
     * @param f number from which exponent is requested
     * @return exponent for d in IEEE754 representation, without bias
     */
    pub fn get_exponent(f: f32) -> i32 {
        // NaN and Infinite will return the same exponent anywho so can use raw bits
        return ((float_to_raw_int_bits(&f) >> /* >>> */ 23) & 0xff) as i32 - 127;
    }
}


/** Class operator on double numbers split into one 26 bits number and one 27 bits number. */
/** Split version of NaN. */
pub const SPLIT_NAN: Split = Split {
    full: f64::NAN,
    high: f64::NAN,
    low: 0.0,
};

/** Split version of positive infinity. */
pub const SPLIT_INFINITY: Split = Split {
    full: f64::INFINITY,
    high: f64::INFINITY,
    low: 0.0,
};

/** Split version of negative infinity. */
pub const SPLIT_NEG_INFINITY: Split = Split {
    full: f64::NEG_INFINITY,
    high: f64::NEG_INFINITY,
    low: 0.0,
};

pub struct Split {
    /** Full number. */
    pub full: f64,

    /** High order bits. */
    pub high: f64,

    /** Low order bits. */
    pub low: f64,
}

impl Split {
    /** Simple constructor.
         * @param x number to split
         */
    fn new(x: f64) -> Split {
        let high = long_bits_to_double(&(double_to_raw_long_bits(&x) & (0xFFFFFFFFFFFFFFFF << 27)));

        Split {
            full: x,
            high: high,
            low: x - high,
        }
    }

    /** Simple constructor.
         * @param high high order bits
         * @param low low order bits
         */
    fn new_high_low(high: f64, low: f64) -> Split {
        Split::new_full_high_low(if high == 0.0 {
                                     (if low == 0.0 &&
                                         double_to_raw_long_bits(&high) as i64 == i64::MIN {
                                         // negative zero
                                         -0.0
                                     } else {
                                         low
                                     })
                                 } else {
                                     high + low
                                 },
                                 high,
                                 low)
    }

    /** Simple constructor.
         * @param full full number
         * @param high high order bits
         * @param low low order bits
         */
    fn new_full_high_low(full: f64, high: f64, low: f64) -> Split {
        Split {
            full: full,
            high: high,
            low: low,
        }
    }

    /** Multiply the instance by another one.
         * @param b other instance to multiply by
         * @return product
         */
    pub fn multiply(&self, b: &Split) -> Split {
        // beware the following expressions must NOT be simplified, they rely on floating point arithmetic properties
        let mul_basic: Split = Split::new(self.full * b.full);
        let mul_error: f64 = self.low * b.low -
                             (((mul_basic.full - self.high * b.high) - self.low * b.high) -
                              self.high * b.low);
        return Split::new_high_low(mul_basic.high, mul_basic.low + mul_error);
    }

    /** Compute the reciprocal of the instance.
         * @return reciprocal of the instance
         */
    pub fn reciprocal(&self) -> Split {
        let approximate_inv: f64 = 1.0 / self.full;
        let split_inv: Split = Split::new(approximate_inv);
        // if 1.0/d were computed perfectly, remultiplying it by d should give 1.0
        // we want to estimate the error so we can fix the low order bits of approximateInvLow
        // beware the following expressions must NOT be simplified, they rely on floating point arithmetic properties
        let product: Split = self.multiply(&split_inv);
        let error: f64 = (product.high - 1.0) + product.low;
        // better accuracy estimate of reciprocal
        return if f64::is_nan(error) {
            split_inv
        } else {
            Split::new_high_low(split_inv.high, split_inv.low - error / self.full)
        };
    }

    /** Computes this^e.
         * @param e exponent (beware, here it MUST be > 0; the only exclusion is Long.MIN_VALUE)
         * @return d^e, split in high and low bits
         * @since 3.6
         */
    fn pow(&self, e: i64) -> Split {
        // prepare result
        let mut result: Split = Split::new(1.0);
        // d^(2p)
        let mut d2p: Split = Split::new_full_high_low(self.full, self.high, self.low);
        {
            let mut p: i64 = e;
            while p != 0 {
                {
                    if (p & 0x1) != 0 {
                        // accurate multiplication result = result * d^(2p) using Veltkamp TwoProduct algorithm
                        result = result.multiply(&d2p);
                    }
                    // accurate squaring d^(2(p+1)) = d^(2p) * d^(2p) using Veltkamp TwoProduct algorithm
                    d2p = d2p.multiply(&d2p);
                }
                p = (p >> /* >>>= */ 1) & 0x7FFFFFFF_FFFFFFFF;
            }
        }

        if f64::is_nan(result.full) {
            if f64::is_nan(self.full) {
                return SPLIT_NAN;
            } else {
                // and the low order bits became NaN (because infinity - infinity = NaN)
                if F64::abs(self.full) < 1.0 {
                    return Split::new_high_low(F64::copy_sign(0.0, self.full), 0.0);
                } else if self.full < 0.0 && (e & 0x1) == 1 {
                    return SPLIT_NEG_INFINITY;
                } else {
                    return SPLIT_INFINITY;
                }
            }
        } else {
            return result;
        }
    }
}


struct CodyWaite {
    /** k */
    pub final_k: i32,

    /** remA */
    pub final_rem_a: f64,

    /** remB */
    pub final_rem_b: f64,
}

impl CodyWaite {
    /**
         * @param xa Argument.
         */
    fn new(xa: f64) -> CodyWaite {
        // Estimate k.
        // k = (int)(xa / 1.5707963267948966);
        let mut k: i32 = (xa * 0.6366197723675814) as i32;
        // Compute remainder.
        let mut rem_a: f64;
        let mut rem_b: f64;
        loop {
            let mut a: f64 = -k as f64 * 1.570796251296997;
            rem_a = xa + a;
            rem_b = -(rem_a - xa - a);
            a = -k as f64 * 7.549789948768648E-8;
            let mut b: f64 = rem_a;
            rem_a = a + b;
            rem_b += -(rem_a - b - a);
            a = -k as f64 * 6.123233995736766E-17;
            b = rem_a;
            rem_a = a + b;
            rem_b += -(rem_a - b - a);
            if rem_a > 0.0 {
                break;
            }
            // Remainder is negative, so decrement k and try again.
            // This should only happen if the input is very close
            // to an even multiple of pi/2.
            k -= 1;
        }
        CodyWaite {
            final_k: k,
            final_rem_a: rem_a,
            final_rem_b: rem_b,
        }
    }

    /**
         * @return k
         */
    fn get_k(&self) -> i32 {
        return self.final_k;
    }

    /**
         * @return remA
         */
    fn get_rem_a(&self) -> f64 {
        return self.final_rem_a;
    }

    /**
         * @return remB
         */
    fn get_rem_b(&self) -> f64 {
        return self.final_rem_b;
    }
}



pub struct I32 {
}
impl I32 {
    pub fn abs(x: i32) -> i32 {
        let i = x >> 31;
        (x ^ ((i as u32 ^ 0xFFFFFFFF) as i32 + 1)) + i
    }
    /** Compute the maximum of two values
     * @param a first value
     * @param b second value
     * @return b if a is lesser or equal to b, a otherwise
     */
    pub fn max(a: i32, b: i32) -> i32 {
        return if (a <= b) { b } else { a };
    }

    /** Compute the minimum of two values
     * @param a first value
     * @param b second value
     * @return a if a is lesser or equal to b, b otherwise
     */
    pub fn min(a: i32, b: i32) -> i32 {
        return if (a <= b) { a } else { b };
    }
}

pub struct I64 {
}

impl I64 {
    pub fn abs(x: i64) -> i64 {
        let l = x as u64 >> 63;
        (x ^ ((l ^ 0xFFFFFFFFFFFFFFFF) as i64 + 1)) + l as i64
    }
}
