#![allow(dead_code)]
#![allow(non_snake_case)]
#![allow(unused_variables)]
use std::f64;
use util::rsutils::*;
use util::fastmath;

// Exponent offset in IEEE754 representation.
const EXPONENT_OFFSET: u64 = 1023;
// Offset to order signed double numbers lexicographically.
const SGN_MASK: u64 = 0x8000000000000000;
// Offset to order signed double numbers lexicographically.
const SGN_MASK_FLOAT: u32 = 0x80000000;
// Positive zero.
const POSITIVE_ZERO: f64 = 0.0;

lazy_static! {
	pub static ref POSITIVE_ZERO_DOUBLE_BITS: i64 = double_to_raw_long_bits(&0.0) as i64;
	pub static ref NEGATIVE_ZERO_DOUBLE_BITS: i64 = double_to_raw_long_bits(&-0.0) as i64;
	pub static ref POSITIVE_ZERO_FLOAT_BITS:  u32 = float_to_raw_int_bits(&0.0);
	pub static ref NEGATIVE_ZERO_FLOAT_BITS:  u32 = float_to_raw_int_bits(&-0.0);
	pub static ref EPSILON: f64 = long_bits_to_double(&((EXPONENT_OFFSET - 53) << 52));
	pub static ref SAFE_MIN: f64 = long_bits_to_double(&((EXPONENT_OFFSET - 1022) << 52));
}


pub struct F64 {
	
}

impl F64 {
    fn positive_zero_double_bits() -> u64 {
        double_to_raw_long_bits(&0.0)
    }
    fn negative_zero_double_bits() -> u64 {
        double_to_raw_long_bits(&-0.0)
    }
    /**
     * Returns true iff they are equal as defined by
     * {@link #equals(float,float,int) equals(x, y, 1)}.
     *
     * @param x first value
     * @param y second value
     * @return {@code true} if the values are equal.
     */
    pub fn equals(x: f64, y: f64) -> bool {
        return F64::equals_max_ulps(x, y, 1);
    }


    pub fn equals_max_ulps(x: f64, y: f64, max_ulps: u64) -> bool {
        let xInt = double_to_raw_long_bits(&x) as i64;
        let yInt = double_to_raw_long_bits(&y) as i64;

        let isEqual = if ((xInt as u64 ^ yInt as u64) & SGN_MASK) == 0 {
            fastmath::I64::abs(xInt as i64 - yInt as i64) as u64 <= max_ulps
        } else {
            let (deltaPlus, deltaMinus) = if xInt < yInt {
                (yInt as i64 - F64::positive_zero_double_bits() as i64,
                 xInt as i64 - F64::negative_zero_double_bits() as i64)
            } else {
                (xInt as i64 - F64::positive_zero_double_bits() as i64,
                 yInt as i64 - F64::negative_zero_double_bits() as i64)
            };
            if deltaPlus > max_ulps as i64 {
                false
            } else {
                deltaMinus <= (max_ulps as i64 - deltaPlus)
            }
        };
        isEqual && !f64::is_nan(x) && !f64::is_nan(y)
    }

    /**
     * Returns {@code true} if there is no double value strictly between the
     * arguments or the difference between them is within the range of allowed
     * error (inclusive). Returns {@code false} if either of the arguments
     * is NaN.
     *
     * @param x First value.
     * @param y Second value.
     * @param eps Amount of allowed absolute error.
     * @return {@code true} if the values are two adjacent floating point
     * numbers or they are within range of each other.
     */
    pub fn equals_eps(x: f64, y: f64, eps: f64) -> bool {
        return F64::equals_i32(x, y, 1) || fastmath::F64::abs(y - x) <= eps;
    }


    /**
     * Returns true if the arguments are equal or within the range of allowed
     * error (inclusive).
     * <p>
     * Two float numbers are considered equal if there are {@code (maxUlps - 1)}
     * (or fewer) floating point numbers between them, i.e. two adjacent
     * floating point numbers are considered equal.
     * </p>
     * <p>
     * Adapted from <a
     * href="http://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/">
     * Bruce Dawson</a>. Returns {@code false} if either of the arguments is NaN.
     * </p>
     *
     * @param x first value
     * @param y second value
     * @param maxUlps {@code (maxUlps - 1)} is the number of floating point
     * values between {@code x} and {@code y}.
     * @return {@code true} if there are fewer than {@code maxUlps} floating
     * point values between {@code x} and {@code y}.
     */
    pub fn equals_i32(x: f64, y: f64, max_ulps: u32) -> bool {
        let x_int: i64 = double_to_raw_long_bits(&x) as i64;
        let y_int: i64 = double_to_raw_long_bits(&y) as i64;
        let is_equal: bool;
        if ((x_int ^ y_int) & SGN_MASK as i64) == 0 {
            // number have same sign, there is no risk of overflow
            is_equal = fastmath::I64::abs(x_int - y_int) <= max_ulps as i64;
        } else {
            // number have opposite signs, take care of overflow
            let delta_plus: i64;
            let delta_minus: i64;
            if x_int < y_int {
                delta_plus = y_int - *POSITIVE_ZERO_DOUBLE_BITS;
                delta_minus = x_int - *NEGATIVE_ZERO_DOUBLE_BITS;
            } else {
                delta_plus = x_int - *POSITIVE_ZERO_DOUBLE_BITS;
                delta_minus = y_int - *NEGATIVE_ZERO_DOUBLE_BITS;
            }
            if delta_plus > max_ulps as i64 {
                is_equal = false;
            } else {
                is_equal = delta_minus <= (max_ulps as i64 - delta_plus);
            }
        }
        return is_equal && !f64::is_nan(x) && !f64::is_nan(y);
    }


    pub fn equals_in_1(x: f64, y: f64) -> bool {
        return F64::equals_i32(x, y, 1);
    }

    /**
     * Returns true if both arguments are NaN or they are
     * equal as defined by {@link #equals(float,float) equals(x, y, 1)}.
     *
     * @param x first value
     * @param y second value
     * @return {@code true} if the values are equal or both are NaN.
     * @since 2.2
     */
    pub fn equals_including_nan(x: f64, y: f64) -> bool {
        return if x != x || y != y {
            (x != x) == (y != y)
        } else {
            F64::equals_max_ulps(x, y, 1)
        };
    }

    /**
     * Returns {@code true} if there is no double value strictly between the
     * arguments or the relative difference between them is less than or equal
     * to the given tolerance. Returns {@code false} if either of the arguments
     * is NaN.
     *
     * @param x First value.
     * @param y Second value.
     * @param eps Amount of allowed relative error.
     * @return {@code true} if the values are two adjacent floating point
     * numbers or they are within range of each other.
     * @since 3.1
     */
    pub fn equals_with_relative_tolerance(x: f64, y: f64, eps: f64) -> bool {
        if F64::equals_i32(x, y, 1) {
            return true;
        }
        let absolute_max: f64 = fastmath::F64::max(fastmath::F64::abs(x), fastmath::F64::abs(y));
        let relative_difference: f64 = fastmath::F64::abs((x - y) / absolute_max);
        return relative_difference <= eps;
    }
}

pub struct F32 {
	
}

impl F32 {
    /**
     * Returns true iff they are equal as defined by
     * {@link #equals(float,float,int) equals(x, y, 1)}.
     *
     * @param x first value
     * @param y second value
     * @return {@code true} if the values are equal.
     */
    pub fn equals(x: f32, y: f32) -> bool {
        return F32::equals_max_ulps(x, y, 1);
    }

    /**
     * Returns true if both arguments are NaN or they are
     * equal as defined by {@link #equals(float,float) equals(x, y, 1)}.
     *
     * @param x first value
     * @param y second value
     * @return {@code true} if the values are equal or both are NaN.
     * @since 2.2
     */
    pub fn equals_including_nan(x: f32, y: f32) -> bool {
        return if x != x || y != y {
            !((x != x) ^ (y != y))
        } else {
            F32::equals_max_ulps(x, y, 1)
        };
    }

    /**
     * Returns true if the arguments are equal or within the range of allowed
     * error (inclusive).  Returns {@code false} if either of the arguments
     * is NaN.
     *
     * @param x first value
     * @param y second value
     * @param eps the amount of absolute error to allow.
     * @return {@code true} if the values are equal or within range of each other.
     * @since 2.2
     */
    pub fn equals_eps(x: f32, y: f32, eps: f32) -> bool {
        return F32::equals_max_ulps(x, y, 1) || fastmath::F32::abs(y - x) <= eps;
    }

    /**
     * Returns true if the arguments are both NaN, are equal, or are within the range
     * of allowed error (inclusive).
     *
     * @param x first value
     * @param y second value
     * @param eps the amount of absolute error to allow.
     * @return {@code true} if the values are equal or within range of each other,
     * or both are NaN.
     * @since 2.2
     */
    pub fn equals_including_nan_eps(x: f32, y: f32, eps: f32) -> bool {
        return F32::equals_including_nan(x, y) || (fastmath::F32::abs(y - x) <= eps);
    }

    /**
     * Returns true if the arguments are equal or within the range of allowed
     * error (inclusive).
     * Two float numbers are considered equal if there are {@code (maxUlps - 1)}
     * (or fewer) floating point numbers between them, i.e. two adjacent floating
     * point numbers are considered equal.
     * Adapted from <a
     * href="http://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/">
     * Bruce Dawson</a>.  Returns {@code false} if either of the arguments is NaN.
     *
     * @param x first value
     * @param y second value
     * @param maxUlps {@code (maxUlps - 1)} is the number of floating point
     * values between {@code x} and {@code y}.
     * @return {@code true} if there are fewer than {@code maxUlps} floating
     * point values between {@code x} and {@code y}.
     * @since 2.2
     */
    pub fn equals_max_ulps(x: f32, y: f32, max_ulps: u32) -> bool {
        let x_int: u32 = float_to_raw_int_bits(&x);
        let y_int: u32 = float_to_raw_int_bits(&y);
        let is_equal: bool;
        if ((x_int ^ y_int) & SGN_MASK_FLOAT) == 0 {
            // number have same sign, there is no risk of overflow
            is_equal = fastmath::I32::abs(x_int as i32 - y_int as i32) as u32 <= max_ulps;
        } else {
            // number have opposite signs, take care of overflow
            let delta_plus: u32;
            let delta_minus: u32;
            if x_int < y_int {
                delta_plus = y_int - *POSITIVE_ZERO_FLOAT_BITS;
                delta_minus = x_int - *NEGATIVE_ZERO_FLOAT_BITS;
            } else {
                delta_plus = x_int - *POSITIVE_ZERO_FLOAT_BITS;
                delta_minus = y_int - *NEGATIVE_ZERO_FLOAT_BITS;
            }
            if delta_plus > max_ulps {
                is_equal = false;
            } else {
                is_equal = delta_minus <= (max_ulps - delta_plus);
            }
        }
        return is_equal && !f32::is_nan(x) && !f32::is_nan(y);
    }

    /**
     * Returns true if the arguments are both NaN or if they are equal as defined
     * by {@link #equals(float,float,int) equals(x, y, maxUlps)}.
     *
     * @param x first value
     * @param y second value
     * @param maxUlps {@code (maxUlps - 1)} is the number of floating point
     * values between {@code x} and {@code y}.
     * @return {@code true} if both arguments are NaN or if there are less than
     * {@code maxUlps} floating point values between {@code x} and {@code y}.
     * @since 2.2
     */
    pub fn equals_including_nan_max_ulps(x: f32, y: f32, max_ulps: u32) -> bool {
        return if x != x || y != y {
            !((x != x) ^ (y != y))
        } else {
            F32::equals_max_ulps(x, y, max_ulps)
        };
    }
}

#[cfg(test)]
mod tests {
    use util::precision::F64;
    use std::f64;

    #[test]
    pub fn test_equals() {
        F64::equals(1.0, 1.0);
        F64::equals(-0.0, 0.0);
        F64::equals(f64::NEG_INFINITY, f64::INFINITY);
    }


}
