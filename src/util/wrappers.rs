use std::cmp::Ordering;
use std::cmp::PartialOrd;
use std::cmp::PartialEq;


pub struct F64Wrapper {
    pub value: f64,
}

impl PartialEq for F64Wrapper {
    fn eq(&self, other: &F64Wrapper) -> bool {
        self.value.eq(&other.value)
    }
}

impl PartialOrd for F64Wrapper {
    fn partial_cmp(&self, other: &F64Wrapper) -> Option<Ordering> {
        self.value.partial_cmp(&other.value)
    }
}

impl Eq for F64Wrapper {}
impl Ord for F64Wrapper {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.value.is_nan() {
            if other.value.is_nan() {
                return Ordering::Equal;
            } else {
                return Ordering::Greater;
            }
        } else {
            if other.value.is_nan() {
                return Ordering::Less;
            }
            if self.value < other.value {
                return Ordering::Less;
            }
            if self.value > other.value {
                return Ordering::Greater;
            }
            Ordering::Equal
        }
    }
}

impl F64Wrapper {
    pub fn new(value: f64) -> F64Wrapper {
        F64Wrapper { value: value }
    }
}
