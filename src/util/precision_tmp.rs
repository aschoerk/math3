                
                   /*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org::apache::commons::math3::util;

use java::math::BigDecimal;
use org::apache::commons::math3::exception::MathArithmeticException;
use org::apache::commons::math3::exception::MathIllegalArgumentException;
use org::apache::commons::math3::exception::util::LocalizedFormats;

/**
 * Utilities for comparing numbers.
 *
 * @since 3.0
 */

/**
     * <p>
     * Largest double-precision floating-point number such that
     * {@code 1 + EPSILON} is numerically equal to 1. This value is an upper
     * bound on the relative error due to rounding real numbers to double
     * precision floating-point numbers.
     * </p>
     * <p>
     * In IEEE 754 arithmetic, this is 2<sup>-53</sup>.
     * </p>
     *
     * @see <a href="http://en.wikipedia.org/wiki/Machine_epsilon">Machine epsilon</a>
     */
 const EPSILON: f64;

/**
     * Safe minimum, such that {@code 1 / SAFE_MIN} does not overflow.
     * <br/>
     * In IEEE 754 arithmetic, this is also the smallest normalized
     * number 2<sup>-1022</sup>.
     */
 const SAFE_MIN: f64;

/** Exponent offset in IEEE754 representation. */
 const EXPONENT_OFFSET: i64 = 1023;

/** Offset to order signed double numbers lexicographically. */
 const SGN_MASK: i64 = 0x8000000000000000;

/** Offset to order signed double numbers lexicographically. */
 const SGN_MASK_FLOAT: i32 = 0x80000000;

/** Positive zero. */
 const POSITIVE_ZERO: f64 = 0d.0;

/** Positive zero bits. */
 const POSITIVE_ZERO_DOUBLE_BITS: i64 = Double.double_to_raw_long_bits(0.0);

/** Negative zero bits. */
 const NEGATIVE_ZERO_DOUBLE_BITS: i64 = Double.double_to_raw_long_bits(-0.0);

/** Positive zero bits. */
 const POSITIVE_ZERO_FLOAT_BITS: i32 = Float.float_to_raw_int_bits(0.0f);

/** Negative zero bits. */
 const NEGATIVE_ZERO_FLOAT_BITS: i32 = Float.float_to_raw_int_bits(-0.0f);
pub struct Precision {
}

impl Precision {

    static {
        /*
         *  This was previously expressed as = 0x1.0p-53;
         *  However, OpenJDK (Sparc Solaris) cannot handle such small
         *  constants: MATH-721
         */
        EPSILON = Double.long_bits_to_double((EXPONENT_OFFSET - 53) << 52);
        /*
         * This was previously expressed as = 0x1.0p-1022;
         * However, OpenJDK (Sparc Solaris) cannot handle such small
         * constants: MATH-721
         */
        SAFE_MIN = Double.long_bits_to_double((EXPONENT_OFFSET - 1022) << 52);
    }

    /**
     * Private constructor.
     */
    fn new() -> Precision {
    }

    /**
     * Compares two numbers given some amount of allowed error.
     *
     * @param x the first number
     * @param y the second number
     * @param eps the amount of error to allow when checking for equality
     * @return <ul><li>0 if  {@link #equals(double, double, double) equals(x, y, eps)}</li>
     *       <li>< 0 if !{@link #equals(double, double, double) equals(x, y, eps)} && x < y</li>
     *       <li>> 0 if !{@link #equals(double, double, double) equals(x, y, eps)} && x > y or
     *       either argument is NaN</li></ul>
     */
    pub fn  compare_to( x: f64,  y: f64,  eps: f64) -> i32  {
        if ::equals(&x, &y, &eps) {
            return 0;
        } else if x < y {
            return -1;
        }
        return 1;
    }

    /**
     * Compares two numbers given some amount of allowed error.
     * Two float numbers are considered equal if there are {@code (maxUlps - 1)}
     * (or fewer) floating point numbers between them, i.e. two adjacent floating
     * point numbers are considered equal.
     * Adapted from <a
     * href="http://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/">
     * Bruce Dawson</a>. Returns {@code false} if either of the arguments is NaN.
     *
     * @param x first value
     * @param y second value
     * @param maxUlps {@code (maxUlps - 1)} is the number of floating point
     * values between {@code x} and {@code y}.
     * @return <ul><li>0 if  {@link #equals(double, double, int) equals(x, y, maxUlps)}</li>
     *       <li>< 0 if !{@link #equals(double, double, int) equals(x, y, maxUlps)} && x < y</li>
     *       <li>> 0 if !{@link #equals(double, double, int) equals(x, y, maxUlps)} && x > y
     *       or either argument is NaN</li></ul>
     */
    pub fn  compare_to( x: f64,  y: f64,  max_ulps: i32) -> i32  {
        if ::equals(&x, &y, &max_ulps) {
            return 0;
        } else if x < y {
            return -1;
        }
        return 1;
    }

   

   
    /**
     * Returns true iff they are equal as defined by
     * {@link #equals(double,double,int) equals(x, y, 1)}.
     *
     * @param x first value
     * @param y second value
     * @return {@code true} if the values are equal.
     */
    pub fn  equals( x: f64,  y: f64) -> bool  {
        return ::equals(&x, &y, 1);
    }

    /**
     * Returns true if the arguments are both NaN or they are
     * equal as defined by {@link #equals(double,double) equals(x, y, 1)}.
     *
     * @param x first value
     * @param y second value
     * @return {@code true} if the values are equal or both are NaN.
     * @since 2.2
     */
    pub fn  equals_including_na_n( x: f64,  y: f64) -> bool  {
        return  if (x != x || y != y) { !(x != x ^ y != y) } else { ::equals(&x, &y, 1) };
    }

    /**
     * Returns {@code true} if there is no double value strictly between the
     * arguments or the difference between them is within the range of allowed
     * error (inclusive). Returns {@code false} if either of the arguments
     * is NaN.
     *
     * @param x First value.
     * @param y Second value.
     * @param eps Amount of allowed absolute error.
     * @return {@code true} if the values are two adjacent floating point
     * numbers or they are within range of each other.
     */
    pub fn  equals( x: f64,  y: f64,  eps: f64) -> bool  {
        return ::equals(&x, &y, 1) || FastMath.abs(y - x) <= eps;
    }

    /**
     * Returns {@code true} if there is no double value strictly between the
     * arguments or the relative difference between them is less than or equal
     * to the given tolerance. Returns {@code false} if either of the arguments
     * is NaN.
     *
     * @param x First value.
     * @param y Second value.
     * @param eps Amount of allowed relative error.
     * @return {@code true} if the values are two adjacent floating point
     * numbers or they are within range of each other.
     * @since 3.1
     */
    pub fn  equals_with_relative_tolerance( x: f64,  y: f64,  eps: f64) -> bool  {
        if ::equals(&x, &y, 1) {
            return true;
        }
         let absolute_max: f64 = FastMath.max(&FastMath.abs(&x), &FastMath.abs(&y));
         let relative_difference: f64 = FastMath.abs((x - y) / absolute_max);
        return relative_difference <= eps;
    }

    /**
     * Returns true if the arguments are both NaN, are equal or are within the range
     * of allowed error (inclusive).
     *
     * @param x first value
     * @param y second value
     * @param eps the amount of absolute error to allow.
     * @return {@code true} if the values are equal or within range of each other,
     * or both are NaN.
     * @since 2.2
     */
    pub fn  equals_including_na_n( x: f64,  y: f64,  eps: f64) -> bool  {
        return ::equals_including_na_n(&x, &y) || (FastMath.abs(y - x) <= eps);
    }

    /**
     * Returns true if the arguments are equal or within the range of allowed
     * error (inclusive).
     * <p>
     * Two float numbers are considered equal if there are {@code (maxUlps - 1)}
     * (or fewer) floating point numbers between them, i.e. two adjacent
     * floating point numbers are considered equal.
     * </p>
     * <p>
     * Adapted from <a
     * href="http://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/">
     * Bruce Dawson</a>. Returns {@code false} if either of the arguments is NaN.
     * </p>
     *
     * @param x first value
     * @param y second value
     * @param maxUlps {@code (maxUlps - 1)} is the number of floating point
     * values between {@code x} and {@code y}.
     * @return {@code true} if there are fewer than {@code maxUlps} floating
     * point values between {@code x} and {@code y}.
     */
    pub fn  equals( x: f64,  y: f64,  max_ulps: i32) -> bool  {
         let x_int: i64 = Double.double_to_raw_long_bits(&x);
         let y_int: i64 = Double.double_to_raw_long_bits(&y);
         let is_equal: bool;
        if ((x_int ^ y_int) & SGN_MASK) == 0 {
            // number have same sign, there is no risk of overflow
            is_equal = FastMath.abs(x_int - y_int) <= max_ulps;
        } else {
            // number have opposite signs, take care of overflow
             let delta_plus: i64;
             let delta_minus: i64;
            if x_int < y_int {
                delta_plus = y_int - POSITIVE_ZERO_DOUBLE_BITS;
                delta_minus = x_int - NEGATIVE_ZERO_DOUBLE_BITS;
            } else {
                delta_plus = x_int - POSITIVE_ZERO_DOUBLE_BITS;
                delta_minus = y_int - NEGATIVE_ZERO_DOUBLE_BITS;
            }
            if delta_plus > max_ulps {
                is_equal = false;
            } else {
                is_equal = delta_minus <= (max_ulps - delta_plus);
            }
        }
        return is_equal && !Double.is_na_n(&x) && !Double.is_na_n(&y);
    }

    /**
     * Returns true if both arguments are NaN or if they are equal as defined
     * by {@link #equals(double,double,int) equals(x, y, maxUlps)}.
     *
     * @param x first value
     * @param y second value
     * @param maxUlps {@code (maxUlps - 1)} is the number of floating point
     * values between {@code x} and {@code y}.
     * @return {@code true} if both arguments are NaN or if there are less than
     * {@code maxUlps} floating point values between {@code x} and {@code y}.
     * @since 2.2
     */
    pub fn  equals_including_na_n( x: f64,  y: f64,  max_ulps: i32) -> bool  {
        return  if (x != x || y != y) { !(x != x ^ y != y) } else { ::equals(&x, &y, &max_ulps) };
    }

    /**
     * Rounds the given value to the specified number of decimal places.
     * The value is rounded using the {@link BigDecimal#ROUND_HALF_UP} method.
     *
     * @param x Value to round.
     * @param scale Number of digits to the right of the decimal point.
     * @return the rounded value.
     * @since 1.1 (previously in {@code MathUtils}, moved as of version 3.0)
     */
    pub fn  round( x: f64,  scale: i32) -> f64  {
        return ::round(&x, &scale, BigDecimal::ROUND_HALF_UP);
    }

    /**
     * Rounds the given value to the specified number of decimal places.
     * The value is rounded using the given method which is any method defined
     * in {@link BigDecimal}.
     * If {@code x} is infinite or {@code NaN}, then the value of {@code x} is
     * returned unchanged, regardless of the other parameters.
     *
     * @param x Value to round.
     * @param scale Number of digits to the right of the decimal point.
     * @param roundingMethod Rounding method as defined in {@link BigDecimal}.
     * @return the rounded value.
     * @throws ArithmeticException if {@code roundingMethod == ROUND_UNNECESSARY}
     * and the specified scaling operation would require rounding.
     * @throws IllegalArgumentException if {@code roundingMethod} does not
     * represent a valid rounding mode.
     * @since 1.1 (previously in {@code MathUtils}, moved as of version 3.0)
     */
    pub fn  round( x: f64,  scale: i32,  rounding_method: i32) -> f64  {
        try {
             let rounded: f64 = (BigDecimal::new(&Double.to_string(&x)).set_scale(&scale, &rounding_method)).double_value();
            // MATH-1089: negative values rounded to zero should result in negative zero
            return  if rounded == POSITIVE_ZERO { POSITIVE_ZERO * x } else { rounded };
        } catch ( ex: &NumberFormatException) {
            if Double.is_infinite(&x) {
                return x;
            } else {
                return Double::NaN;
            }
        }
    }

    /**
     * Rounds the given value to the specified number of decimal places.
     * The value is rounded using the {@link BigDecimal#ROUND_HALF_UP} method.
     *
     * @param x Value to round.
     * @param scale Number of digits to the right of the decimal point.
     * @return the rounded value.
     * @since 1.1 (previously in {@code MathUtils}, moved as of version 3.0)
     */
    pub fn  round( x: f32,  scale: i32) -> f32  {
        return ::round(&x, &scale, BigDecimal::ROUND_HALF_UP);
    }

    /**
     * Rounds the given value to the specified number of decimal places.
     * The value is rounded using the given method which is any method defined
     * in {@link BigDecimal}.
     *
     * @param x Value to round.
     * @param scale Number of digits to the right of the decimal point.
     * @param roundingMethod Rounding method as defined in {@link BigDecimal}.
     * @return the rounded value.
     * @since 1.1 (previously in {@code MathUtils}, moved as of version 3.0)
     * @throws MathArithmeticException if an exact operation is required but result is not exact
     * @throws MathIllegalArgumentException if {@code roundingMethod} is not a valid rounding method.
     */
    pub fn  round( x: f32,  scale: i32,  rounding_method: i32) -> /*  throws MathArithmeticException, MathIllegalArgumentException */Result<f32>   {
         let sign: f32 = FastMath.copy_sign(1f.0, &x);
         let factor: f32 = FastMath.pow(10.0f, &scale) as f32 * sign;
        return ::round_unscaled(x * factor, &sign, &rounding_method) as f32 / factor;
    }

    /**
     * Rounds the given non-negative value to the "nearest" integer. Nearest is
     * determined by the rounding method specified. Rounding methods are defined
     * in {@link BigDecimal}.
     *
     * @param unscaled Value to round.
     * @param sign Sign of the original, scaled value.
     * @param roundingMethod Rounding method, as defined in {@link BigDecimal}.
     * @return the rounded value.
     * @throws MathArithmeticException if an exact operation is required but result is not exact
     * @throws MathIllegalArgumentException if {@code roundingMethod} is not a valid rounding method.
     * @since 1.1 (previously in {@code MathUtils}, moved as of version 3.0)
     */
    fn  round_unscaled( unscaled: f64,  sign: f64,  rounding_method: i32) -> /*  throws MathArithmeticException, MathIllegalArgumentException */Result<f64>   {
        match rounding_method {
              BigDecimal::ROUND_CEILING => 
                 {
                    if sign == -1 {
                        unscaled = FastMath.floor(&FastMath.next_after(&unscaled, Double::NEGATIVE_INFINITY));
                    } else {
                        unscaled = FastMath.ceil(&FastMath.next_after(&unscaled, Double::POSITIVE_INFINITY));
                    }
                    break;
                }
              BigDecimal::ROUND_DOWN => 
                 {
                    unscaled = FastMath.floor(&FastMath.next_after(&unscaled, Double::NEGATIVE_INFINITY));
                    break;
                }
              BigDecimal::ROUND_FLOOR => 
                 {
                    if sign == -1 {
                        unscaled = FastMath.ceil(&FastMath.next_after(&unscaled, Double::POSITIVE_INFINITY));
                    } else {
                        unscaled = FastMath.floor(&FastMath.next_after(&unscaled, Double::NEGATIVE_INFINITY));
                    }
                    break;
                }
              BigDecimal::ROUND_HALF_DOWN => 
                 {
                    {
                        unscaled = FastMath.next_after(&unscaled, Double::NEGATIVE_INFINITY);
                         let fraction: f64 = unscaled - FastMath.floor(&unscaled);
                        if fraction > 0.5 {
                            unscaled = FastMath.ceil(&unscaled);
                        } else {
                            unscaled = FastMath.floor(&unscaled);
                        }
                        break;
                    }
                }
              BigDecimal::ROUND_HALF_EVEN => 
                 {
                    {
                         let fraction: f64 = unscaled - FastMath.floor(&unscaled);
                        if fraction > 0.5 {
                            unscaled = FastMath.ceil(&unscaled);
                        } else if fraction < 0.5 {
                            unscaled = FastMath.floor(&unscaled);
                        } else {
                            // The following equality test is intentional and needed for rounding purposes
                            if FastMath.floor(&unscaled) / 2.0 == FastMath.floor(FastMath.floor(&unscaled) / 2.0) {
                                // even
                                unscaled = FastMath.floor(&unscaled);
                            } else {
                                // odd
                                unscaled = FastMath.ceil(&unscaled);
                            }
                        }
                        break;
                    }
                }
              BigDecimal::ROUND_HALF_UP => 
                 {
                    {
                        unscaled = FastMath.next_after(&unscaled, Double::POSITIVE_INFINITY);
                         let fraction: f64 = unscaled - FastMath.floor(&unscaled);
                        if fraction >= 0.5 {
                            unscaled = FastMath.ceil(&unscaled);
                        } else {
                            unscaled = FastMath.floor(&unscaled);
                        }
                        break;
                    }
                }
              BigDecimal::ROUND_UNNECESSARY => 
                 {
                    if unscaled != FastMath.floor(&unscaled) {
                        throw MathArithmeticException::new();
                    }
                    break;
                }
              BigDecimal::ROUND_UP => 
                 {
                    // do not round if the discarded fraction is equal to zero
                    if unscaled != FastMath.floor(&unscaled) {
                        unscaled = FastMath.ceil(&FastMath.next_after(&unscaled, Double::POSITIVE_INFINITY));
                    }
                    break;
                }
            default:
                 {
                    throw MathIllegalArgumentException::new(LocalizedFormats::INVALID_ROUNDING_METHOD, &rounding_method, "ROUND_CEILING", BigDecimal::ROUND_CEILING, "ROUND_DOWN", BigDecimal::ROUND_DOWN, "ROUND_FLOOR", BigDecimal::ROUND_FLOOR, "ROUND_HALF_DOWN", BigDecimal::ROUND_HALF_DOWN, "ROUND_HALF_EVEN", BigDecimal::ROUND_HALF_EVEN, "ROUND_HALF_UP", BigDecimal::ROUND_HALF_UP, "ROUND_UNNECESSARY", BigDecimal::ROUND_UNNECESSARY, "ROUND_UP", BigDecimal::ROUND_UP);
                }
        }
        return unscaled;
    }

    /**
     * Computes a number {@code delta} close to {@code originalDelta} with
     * the property that <pre><code>
     *   x + delta - x
     * </code></pre>
     * is exactly machine-representable.
     * This is useful when computing numerical derivatives, in order to reduce
     * roundoff errors.
     *
     * @param x Value.
     * @param originalDelta Offset value.
     * @return a number {@code delta} so that {@code x + delta} and {@code x}
     * differ by a representable floating number.
     */
    pub fn  representable_delta( x: f64,  original_delta: f64) -> f64  {
        return x + original_delta - x;
    }
}


                
                