#[cfg(test)] 
mod tests {
	use util::math_arrays::*;
	use util::fastmath;
	use std::f64;
	use std::i32;
	use assert;
	use std::rc::*;
	use std::cell::*;
	use util::precision::*;
	use random::bits_stream_generator::*;
	use random::well_generator::*;
	use util::test_utils::TestUtils;

	const test_array: [f64;6] = [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, ]
    ;

     const test_weights_array: [f64;6] = [0.3, 0.2, 1.3, 1.1, 1.0, 1.8, ]
    ;

     const test_negative_weights_array: [f64;6] = [-0.3, 0.2, -1.3, 1.1, 1.0, 1.8, ]
    ;


     const singleton_array: [f64;1] = [0.0, ];

	
	#[test]
    pub fn  test_scale()   {
         let test = vec![-2.5, -1.0, 0.0, 1.0, 2.5, ]
        ;
         let correct_test: Vec<f64> = MathArrays::copy_of(&test);
         let correct_scaled = vec![5.25, 2.1, 0.0, -2.1, -5.25, ]
        ;
         let scaled: Vec<f64> = MathArrays::scale(-2.1, &test);
        // Make sure test has not changed
         {
             let mut i: usize = 0;
            while i < test.len() {
                {
                    assert_within_delta!(correct_test[i], test[i], 0.0); 
                }
                i += 1;
             }
         }

        // Test scaled values
         {
             let mut i: usize = 0;
            while i < scaled.len() {
                {
                    assert_within_delta!(correct_scaled[i], scaled[i], 0.0);
                }
                i += 1;
             }
         }

    }
    
   
	 #[test]
    pub fn  test_scale_in_place()   {
         let mut test = vec![-2.5, -1.0, 0.0, 1.0, 2.5, ]
        ;
         let correct_scaled = vec![5.25, 2.1, 0.0, -2.1, -5.25, ]
        ;
        MathArrays::scale_in_place(-2.1, &mut test);
        // Make sure test has changed
         {
             let mut i: usize = 0;
            while i < test.len() {
                {
                    assert_within_delta!(correct_scaled[i], test[i], 0.0);
                }
                i += 1;
             }
         }

    }

    #[test]
    pub fn  test_ebe_add_precondition()   {
        assert!(MathArrays::ebe_add(&vec![0.0; 3], &vec![0.0; 4]).is_err());
    }

    #[test]
    pub fn  test_ebe_subtract_precondition()   {
        assert!(MathArrays::ebe_subtract(&vec![0.0; 3], &vec![0.0; 4]).is_err());
    }

    #[test]
    pub fn  test_ebe_multiply_precondition()   {
        assert!(MathArrays::ebe_multiply(&vec![0.0; 3], &vec![0.0; 4]).is_err());
    }

    #[test]
    pub fn  test_ebe_divide_precondition()   {
        assert!(MathArrays::ebe_divide(&vec![0.0; 3], &vec![0.0; 4]).is_err());
    }
    
    
    #[test]
    pub fn  test_ebe_add()   {
         let a= vec![0.0, 1.0, 2.0, ]
        ;
         let b= vec![3.0, 5.0, 7.0, ]
        ;
         let r: Vec<f64> = MathArrays::ebe_add(&a, &b).unwrap();
         {
             let mut i: usize = 0;
            while i < a.len() {
                {
                    assert_within_delta!(a[i] + b[i], r[i], 0.0);
                }
                i += 1;
             }
         }

    }

    #[test]
    pub fn  test_ebe_subtract()   {
         let a= vec![0.0, 1.0, 2.0, ]
        ;
         let b= vec![3.0, 5.0, 7.0, ]
        ;
         let r: Vec<f64> = MathArrays::ebe_subtract(&a, &b).unwrap();
         {
             let mut i: usize = 0;
            while i < a.len() {
                {
                    assert_within_delta!(a[i] - b[i], r[i], 0.0);
                }
                i += 1;
             }
         }

    }

    #[test]
    pub fn  test_ebe_multiply()   {
         let a = vec![0.0, 1.0, 2.0, ]
        ;
         let b = vec![3.0, 5.0, 7.0, ]
        ;
         let r: Vec<f64> = MathArrays::ebe_multiply(&a, &b).unwrap();
         {
             let mut i: usize = 0;
            while i < a.len() {
                {
                    assert_within_delta!(a[i] * b[i], r[i], 0.0);
                }
                i += 1;
             }
         }

    }

    #[test]
    pub fn  test_ebe_divide()   {
         let a = vec![0.0, 1.0, 2.0, ]
        ;
         let b = vec![3.0, 5.0, 7.0, ]
        ;
         let r: Vec<f64> = MathArrays::ebe_divide(&a, &b).unwrap();
         {
             let mut i: usize = 0;
            while i < a.len() {
                {
                    assert_within_delta!(a[i] / b[i], r[i], 0.0);
                }
                i += 1;
             }
         }

    }

    #[test]
    pub fn  test_l1_distance_double()   {
         let p1 = vec![2.5, 0.0, ]
        ;
         let p2 = vec![-0.5, 4.0, ]
        ;
        assert!(F64::equals_eps(7.0, MathArrays::distance1(&p1, &p2).unwrap(), 1.0));
    }

    #[test]
    pub fn  test_l1_distance_int()   {
         let p1 = vec![3.0, 0.0, ]
        ;
         let p2 = vec![0.0, 4.0, ]
        ;
        assert_eq!(7.0, MathArrays::distance1(&p1, &p2).unwrap());
    }

    #[test]
    pub fn  test_l2_distance_double()   {
         let p1 = vec![2.5, 0.0, ]
        ;
         let p2 = vec![-0.5, 4.0, ]
        ;
        assert!(F64::equals_eps(5.0, MathArrays::distance(&p1, &p2).unwrap(), 1.0));
    }

    #[test]
    pub fn  test_l2_distance_int()   {
         let p1 = vec![3.0, 0.0, ]
        ;
         let p2 = vec![0.0, 4.0, ]
        ;
        assert!(F64::equals_eps(5.0, MathArrays::distance(&p1, &p2).unwrap(), 1.0));
    }

    #[test]
    pub fn  test_l_inf_distance_double()   {
         let p1 = vec![2.5, 0.0, ]
        ;
         let p2 = vec![-0.5, 4.0, ]
        ;
        assert!(F64::equals_eps(4.0, MathArrays::distance_inf(&p1, &p2).unwrap(), 1.0));
    }

    #[test]
    pub fn  test_l_inf_distance_int()   {
         let p1 = vec![3.0, 0.0, ]
        ;
         let p2= vec![0.0, 4.0, ]
        ;
        assert_eq!(4.0, MathArrays::distance_inf(&p1, &p2).unwrap());
    }

    #[test]
    pub fn  test_cos_angle2_d()   {
         let mut expected: f64;
         let v1= vec![1.0, 0.0, ]
        ;
        expected = 1.0;
        assert_within_delta!(expected, MathArrays::cos_angle(&v1, &v1).unwrap(), 0.0);
         let v2= vec![0.0, 1.0, ]
        ;
        expected = 0.0;
        assert_within_delta!(expected, MathArrays::cos_angle(&v1, &v2).unwrap(), 0.0);
         let v3= vec![7.0, 7.0, ]
        ;
        expected = f64::sqrt(2.0) / 2.0;
        assert_within_delta!(expected, MathArrays::cos_angle(&v1, &v3).unwrap(), 1e-15);
        assert_within_delta!(expected, MathArrays::cos_angle(&v3, &v2).unwrap(), 1e-15);
         let v4= vec![-5.0, 0.0, ]
        ;
        expected = -1.0;
        assert_within_delta!(expected, MathArrays::cos_angle(&v1, &v4).unwrap(), 0.0);
         let v5= vec![-100.0, 100.0, ]
        ;
        expected = 0.0;
        assert_within_delta!(expected, MathArrays::cos_angle(&v3, &v5).unwrap(),0.0);
    }

    #[test]
    pub fn  test_cos_angle3_d()   {
         let mut expected: f64;
         let v1= vec![1.0, 1.0, 0.0, ]
        ;
        expected = 1.0;
        assert_within_delta!(expected, MathArrays::cos_angle(&v1, &v1).unwrap(), 1e-15);
         let v2= vec![1.0, 1.0, 1.0, ]
        ;
        expected = f64::sqrt(2.0) / f64::sqrt(3.0);
        assert_within_delta!(expected, MathArrays::cos_angle(&v1, &v2).unwrap(), 1e-15);
    }

    #[test]
    pub fn  test_cos_angle_extreme()   {
         let mut expected: f64;
         let tiny: f64 = 1e-200;
         let v1= vec![tiny, tiny, ]
        ;
         let big: f64 = 1e200;
         let v2= vec![-big, -big, ]
        ;
        expected = -1.0;
        assert_within_delta!(expected, MathArrays::cos_angle(&v1, &v2).unwrap(), 1e-15);
         let v3= vec![big, -big, ]
        ;
        expected = 0.0;
        assert_within_delta!(expected, MathArrays::cos_angle(&v1, &v3).unwrap(), 1e-15);
    }
    
     #[test]
    pub fn  test_check_order()   {
        assert!(MathArrays::check_order(&vec![-15.0, -5.5, -1.0, 2.0, 15.0, ]
        , OrderDirection::INCREASING, true).is_ok());
        assert!(MathArrays::check_order(&vec![-15.0, -5.5, -1.0, 2.0, 2.0, ]
        , OrderDirection::INCREASING, false).is_ok());
        assert!(MathArrays::check_order(&vec![3.0, -5.5, -11.0, -27.5, ]
        , OrderDirection::DECREASING, true).is_ok());
        assert!(MathArrays::check_order(&vec![3.0, 0.0, 0.0, -5.5, -11.0, -27.5, ]
        , OrderDirection::DECREASING, false).is_ok());
        
           assert!( MathArrays::check_order(&vec![-15.0, -5.5, -1.0, -1.0, 2.0, 15.0, ]
            , OrderDirection::INCREASING, true).is_err());
           
           assert!( MathArrays::check_order(&vec![-15.0, -5.5, -1.0, -2.0, 2.0, ]
            , OrderDirection::INCREASING, false).is_err());
            
           assert!( MathArrays::check_order(&vec![3.0, 3.0, -5.5, -11.0, -27.5, ]
            , OrderDirection::DECREASING, true).is_err());
           assert!( MathArrays::check_order(&vec![3.0, -1.0, 0.0, -5.5, -11.0, -27.5, ]
            , OrderDirection::DECREASING, false).is_err());
           assert!( MathArrays::check_order(&vec![3.0, 0.0, -5.5, -11.0, -10.0, ]
            , OrderDirection::DECREASING, false).is_err());
            

    }
    
    #[test]
    pub fn  test_is_monotonic()   {
        assert!(!MathArrays::is_monotonic(&vec![-15.0, -5.5, -1.0, -1.0, 2.0, 15.0, ]
        , OrderDirection::INCREASING, true).unwrap());
        assert!(MathArrays::is_monotonic(&vec![-15.0, -5.5, -1.0, 0.0, 2.0, 15.0, ]
        , OrderDirection::INCREASING, true).unwrap());
        assert!(!MathArrays::is_monotonic(&vec![-15.0, -5.5, -1.0, -2.0, 2.0, ]
        , OrderDirection::INCREASING, false).unwrap());
        assert!(MathArrays::is_monotonic(&vec![-15.0, -5.5, -1.0, -1.0, 2.0, ]
        , OrderDirection::INCREASING, false).unwrap());
        assert!(!MathArrays::is_monotonic(&vec![3.0, 3.0, -5.5, -11.0, -27.5, ]
        , OrderDirection::DECREASING, true).unwrap());
        assert!(MathArrays::is_monotonic(&vec![3.0, 2.0, -5.5, -11.0, -27.5, ]
        , OrderDirection::DECREASING, true).unwrap());
        assert!(!MathArrays::is_monotonic(&vec![3.0, -1.0, 0.0, -5.5, -11.0, -27.5, ]
        , OrderDirection::DECREASING, false).unwrap());
        assert!(MathArrays::is_monotonic(&vec![3.0, 0.0, 0.0, -5.5, -11.0, -27.5, ]
        , OrderDirection::DECREASING, false).unwrap());
    }
    
     #[test]
    pub fn  test_is_monotonic_comparable()   {
        assert!(!&MathArrays::is_monotonic(&vec![-15.0, -5.5, -1.0, -1.0, 2.0, 15.0, ]
        , OrderDirection::INCREASING, true).unwrap());
        assert!(&MathArrays::is_monotonic(&vec![-15.0, -5.5, -1.0, 0.0, 2.0, 15.0, ]
        , OrderDirection::INCREASING, true).unwrap());
        assert!(!&MathArrays::is_monotonic(&vec![-15.0, -5.5, -1.0, -2.0, 2.0, ]
        , OrderDirection::INCREASING, false).unwrap());
        assert!(&MathArrays::is_monotonic(&vec![-15.0, -5.5, -1.0, -1.0, 2.0, ]
        , OrderDirection::INCREASING, false).unwrap());
        assert!(!&MathArrays::is_monotonic(&vec![3.0, 3.0, -5.5, -11.0, -27.5, ]
        , OrderDirection::DECREASING, true).unwrap());
        assert!(&MathArrays::is_monotonic(&vec![3.0, 2.0, -5.5, -11.0, -27.5, ]
        , OrderDirection::DECREASING, true).unwrap());
        assert!(!&MathArrays::is_monotonic(&vec![3.0, -1.0, 0.0, -5.5, -11.0, -27.5, ]
        , OrderDirection::DECREASING, false).unwrap());
        assert!(&MathArrays::is_monotonic(&vec![3.0, 0.0, 0.0, -5.5, -11.0, -27.5, ]
        , OrderDirection::DECREASING, false).unwrap());
    }
    
        #[test]
    pub fn  test_check_rectangular()   {
         let rect  = vec![vec![0, 1, ]
        , vec![2, 3, ]
        , ]
        ;
         let ragged  = vec![vec![0, 1, ]
        , vec![2, ]
        , ]
        ;
         
         let empty = vec![Vec::<i64>::new()];
        assert!(MathArrays::check_rectangular(&rect).is_ok());
        assert!(MathArrays::check_rectangular(&empty).is_ok());
        assert!(MathArrays::check_rectangular(&ragged).is_err());        
    }
    
     #[test]
    pub fn  test_check_positive()   {
         let positive  = vec![1.0, 2.0, 3.0, ]
        ;
         let non_negative  = vec![0.0, 1.0, 2.0, ]
        ;
         let empty = Vec::<f64>::new();
        assert!(MathArrays::check_positive(&positive).is_ok());
        assert!(MathArrays::check_positive(&empty).is_ok());
       
            assert!(MathArrays::check_positive(&non_negative).is_err());
          

    }
 #[test]
    pub fn  test_check_non_negative()   {
         let non_negative  = vec![0, 1, ]
        ;
         let has_negative  = vec![-1, ]
        ;
         let empty = Vec::<i64>::new();
        assert!(MathArrays::check_non_negative(&non_negative).is_ok());
        assert!(MathArrays::check_non_negative(&empty).is_ok());
        assert!(MathArrays::check_non_negative(&has_negative).is_err());

    }
    
    
    #[test]
    pub fn  test_check_non_negative_2d()   {
         let non_negative  = vec![vec![0, 1, ]
        , vec![1, 0, ]
        , ]
        ;
         let has_negative  = vec![vec![-1, ]
        , vec![0, ]
        , ]
        ;
               let empty = vec![Vec::<i64>::new()];
        assert!(MathArrays::check_non_negative_2d(&non_negative).is_ok());
        assert!(MathArrays::check_non_negative_2d(&empty).is_ok());
       
            assert!(MathArrays::check_non_negative_2d(&has_negative).is_err());

    }
    
        #[test]
    pub fn  test_check_not_na_n()   {
         let without_nan = vec![f64::NEG_INFINITY, -f64::MAX, -1.0, 0.0, f64::MIN, fastmath::F64::ulp(1.0), 1.0, 3.0, 113.0, 4769.0, f64::MAX, f64::INFINITY, ]
        ;
         let with_nan = vec![f64::NEG_INFINITY, -f64::MAX, -1.0, 0.0, f64::MIN, fastmath::F64::ulp(1.0), 1.0, 3.0, 113.0, 4769.0, f64::MAX, f64::INFINITY, f64::NAN, ]
        ;
         
         let empty = Vec::<f64>::new();
        assert!(MathArrays::check_not_nan(&without_nan).is_ok());
        assert!(MathArrays::check_not_nan(&empty).is_ok());
        assert!(MathArrays::check_not_nan(&with_nan).is_err());

    }
    
     #[test]
    pub fn  test_check_equal_length1()   {
        assert!(MathArrays::check_equal_length(&vec![1.0, 2.0, 3.0, ]
        ,&vec![1.0, 2.0, 3.0, 4.0, ]
        ).is_err());
    }

    #[test]
    pub fn  test_check_equal_length2()   {
         let a  = vec![-1.0, -12.0, -23.0, -34.0, ]
        ;
         let b  = vec![56.0, 67.0, 78.0, 89.0, 2.0]
        ;
        assert!(MathArrays::check_equal_length(&a, &b).is_err());
    }


    #[test]
    pub fn  test_sort_in_place()   {
         let mut x1 = vec![2.0, 5.0, -3.0, 1.0, 4.0, ]
        ;
         let mut x2 = vec![4.0, 25.0, 9.0, 1.0, 16.0, ]
        ;
         let mut x3 = vec![8.0, 125.0, -27.0, 1.0, 64.0, ]
        ;
        MathArrays::sort_in_place(&mut x1, &mut vec![&mut x2, &mut x3]);
        assert_within_delta!(-3.0, x1[0], fastmath::F64::ulp(1.0));
        assert_within_delta!(9.0, x2[0], fastmath::F64::ulp(1.0));
        assert_within_delta!(-27.0, x3[0], fastmath::F64::ulp(1.0));
        assert_within_delta!(1.0, x1[1], fastmath::F64::ulp(1.0));
        assert_within_delta!(1.0, x2[1], fastmath::F64::ulp(1.0));
        assert_within_delta!(1.0, x3[1], fastmath::F64::ulp(1.0));
        assert_within_delta!(2.0, x1[2], fastmath::F64::ulp(1.0));
        assert_within_delta!(4.0, x2[2], fastmath::F64::ulp(1.0));
        assert_within_delta!(8.0, x3[2], fastmath::F64::ulp(1.0));
        assert_within_delta!(4.0, x1[3], fastmath::F64::ulp(1.0));
        assert_within_delta!(16.0, x2[3], fastmath::F64::ulp(1.0));
        assert_within_delta!(64.0, x3[3], fastmath::F64::ulp(1.0));
        assert_within_delta!(5.0, x1[4], fastmath::F64::ulp(1.0));
        assert_within_delta!(25.0, x2[4], fastmath::F64::ulp(1.0));
        assert_within_delta!(125.0, x3[4], fastmath::F64::ulp(1.0));
    }

#[test]
    pub fn  test_sort_in_place_decresasing_order()   {
         let mut x1 = vec![2.0, 5.0, -3.0, 1.0, 4.0, ]
        ;
         let mut x2 = vec![4.0, 25.0, 9.0, 1.0, 16.0, ]
        ;
         let mut x3 = vec![8.0, 125.0, -27.0, 1.0, 64.0, ]
        ;
        MathArrays::sort_in_place_dir(&mut x1, OrderDirection::DECREASING, &mut vec![&mut x2, &mut x3]);
        assert_within_delta!(-3.0, x1[4], fastmath::F64::ulp(1.0));
        assert_within_delta!(9.0, x2[4], fastmath::F64::ulp(1.0));
        assert_within_delta!(-27.0, x3[4], fastmath::F64::ulp(1.0));
        assert_within_delta!(1.0, x1[3], fastmath::F64::ulp(1.0));
        assert_within_delta!(1.0, x2[3], fastmath::F64::ulp(1.0));
        assert_within_delta!(1.0, x3[3], fastmath::F64::ulp(1.0));
        assert_within_delta!(2.0, x1[2], fastmath::F64::ulp(1.0));
        assert_within_delta!(4.0, x2[2], fastmath::F64::ulp(1.0));
        assert_within_delta!(8.0, x3[2], fastmath::F64::ulp(1.0));
        assert_within_delta!(4.0, x1[1], fastmath::F64::ulp(1.0));
        assert_within_delta!(16.0, x2[1], fastmath::F64::ulp(1.0));
        assert_within_delta!(64.0, x3[1], fastmath::F64::ulp(1.0));
        assert_within_delta!(5.0, x1[0], fastmath::F64::ulp(1.0));
        assert_within_delta!(25.0, x2[0], fastmath::F64::ulp(1.0));
        assert_within_delta!(125.0, x3[0], fastmath::F64::ulp(1.0));
    }
    
    

    #[test]
    pub fn  test_sort_in_place_example() 
    {
         let mut x = vec![3.0, 1.0, 2.0, ]
        ;
         let mut y = vec![1.0, 2.0, 3.0, ]
        ;
         let mut z = vec![0.0, 5.0, 7.0, ]
        ;
        MathArrays::sort_in_place(&mut x, &mut vec![&mut y,&mut z]);
         let sx = vec![1.0, 2.0, 3.0, ]
        ;
         let sy = vec![2.0, 3.0, 1.0, ]
        ;
         let sz = vec![5.0, 7.0, 0.0, ]
        ;
        assert!(sx == x);
        assert!(sy == y);
        assert!(sz == z);
        
    }
    
    #[test]
    pub fn  test_sort_in_place_failures()   {         
         let mut one = vec![1.0, ]
        ;
         let mut two = vec![1.0, 2.0, ]
        ;
        assert!(MathArrays::sort_in_place(&mut one, &mut vec![&mut two]).is_err());
    }
    
    

    #[test]
    pub fn  test_copy_of_int()   {
         let source = vec![i32::MIN, -1, 0, 1, 3, 113, 4769, i32::MAX, ]
        ;
         let dest = MathArrays::copy_of(&source);
        assert_eq!(dest.len(), source.len());
         {
             let mut i: usize = 0;
            while i < source.len() {
                {
                    assert_eq!(source[i], dest[i]);
                }
                i += 1;
             }
         }

    }

    #[test]
    pub fn  test_copy_of_int2()   {
         let source = vec![i32::MIN, -1, 0, 1, 3, 113, 4769, i32::MAX, ]
        ;
         let offset: usize = 3;
         let dest: Vec<i32> = MathArrays::copy_of_head(&source, source.len() - offset,0);
        assert_eq!(dest.len(), source.len() - offset);
         {
             let mut i: usize = 0;
            while i < source.len() - offset {
                {
                    assert_eq!(source[i], dest[i]);
                }
                i += 1;
             }
         }

    }
    
    
    #[test]
    pub fn  test_copy_of_int3()   {
         let source = vec![i32::MIN, -1, 0, 1, 3, 113, 4769, i32::MAX, ]
        ;
         let offset: usize = 3;
         let dest: Vec<i32> = MathArrays::copy_of_head(&source, source.len() + offset,0);
        assert_eq!(dest.len(), source.len() + offset);
         {
             let mut i: usize = 0;
            while i < source.len() {
                {
                    assert_eq!(source[i], dest[i]);
                }
                i += 1;
             }
         }

         {
             let mut i: usize = source.len();
            while i < source.len() + offset {
                {
                    assert_eq!(dest[i],0);
                }
                i += 1;
             }
         }

    }
    
    
    #[test]
    pub fn  test_copy_of_double()   {
         let source = vec![f64::NEG_INFINITY, -f64::MAX, -1.0, 0.0, f64::MIN, fastmath::F64::ulp(1.0), 1.0, 3.0, 113.0, 4769.0, f64::MAX, f64::INFINITY, ]
        ;
         let dest: Vec<f64> = MathArrays::copy_of(&source);
        assert_eq!(dest.len(), source.len());
         {
             let mut i: usize = 0;
            while i < source.len() {
                {
                    assert_within_delta!(source[i], dest[i],0.0);
                }
                i += 1;
             }
         }

    }
    
    
    #[test]
    pub fn  test_copy_of_double2()   {
         let source = vec![f64::NEG_INFINITY, -f64::MAX, -1.0, 0.0, f64::MIN, fastmath::F64::ulp(1.0), 1.0, 3.0, 113.0, 4769.0, f64::MAX, f64::INFINITY, ]
        ;
         let offset: usize = 3;
         let dest: Vec<f64> = MathArrays::copy_of_head(&source, source.len() - offset,0.0);
        assert_eq!(dest.len(), source.len() - offset);
         {
             let mut i: usize = 0;
            while i < source.len() - offset {
                {
                    assert_within_delta!(source[i], dest[i],0.0);
                }
                i += 1;
             }
         }

    }


    #[test]
    pub fn  test_copy_of_double3()   {
         let source = vec![f64::NEG_INFINITY, -f64::MAX, -1.0, 0.0, f64::MIN, fastmath::F64::ulp(1.0), 1.0, 3.0, 113.0, 4769.0, f64::MAX, f64::INFINITY, ]
        ;
         let offset: usize = 3;
         let dest: Vec<f64> = MathArrays::copy_of_head(&source, source.len() + offset,0.0);
        assert_eq!(dest.len(), source.len() + offset);
         {
             let mut i: usize = 0;
            while i < source.len() {
                {
                    assert_within_delta!(source[i], dest[i], 0.0);
                }
                i += 1;
             }
         }

         {
             let mut i: usize = source.len();
            while i < source.len() + offset {
                {
                    assert_within_delta!(0.0, dest[i], 0.0);
                }
                i += 1;
             }
         }

    }


    #[test]
    pub fn  test_copy_of_range()   {
         let source = vec![f64::NEG_INFINITY, -f64::MAX, -1.0, 0.0, f64::MIN, fastmath::F64::ulp(1.0), 1.0, 3.0, 113.0, 4769.0, f64::MAX, f64::INFINITY, ]
        ;
         let from: usize = 3;
         let to: usize = source.len() + 14;
         let dest: Vec<f64> = MathArrays::copy_of_range(&source, from, to, 0.0);
        assert_eq!(dest.len(), to - from);
         {
             let mut i: usize = from;
            while i < source.len() {
                {
                    assert_within_delta!(source[i], dest[i - from], 0.0);
                }
                i += 1;
             }
         }

         {
             let mut i: usize = source.len();
            while i < dest.len() {
                {
                    assert_within_delta!(0.0, dest[i - from], 0.0);
                }
                i += 1;
             }
         }

    }

    // MATH-1005
    #[test]
    pub fn  test_linear_combination_with_single_element_array()   {
         let a = vec![1.23456789, ]
        ;
         let b = vec![98765432.1, ]
        ;
        assert_within_delta!(a[0] * b[0], MathArrays::linear_combination_vec(&a, &b).unwrap(), 0.0);
    }


    #[test]
    pub fn  test_linear_combination1()   {
         let a  = vec![-1321008684645961.0 / 268435456.0, -5774608829631843.0 / 268435456.0, -7645843051051357.0 / 8589934592.0, ]
        ;
         let b  = vec![-5712344449280879.0 / 2097152.0, -4550117129121957.0 / 2097152.0, 8846951984510141.0 / 131072.0, ]
        ;
         let ab_sum_inline: f64 = MathArrays::linear_combination_3d(a[0], b[0], a[1], b[1], a[2], b[2]);
         let ab_sum_array: f64 = MathArrays::linear_combination_vec(&a, &b).unwrap();
        assert_within_delta!(ab_sum_inline, ab_sum_array, 0.0);
        assert_within_delta!(-1.8551294182586248737720779899, ab_sum_inline, 1.0e-15);
         let naive: f64 = a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
        assert!(fastmath::F64::abs(naive - ab_sum_inline) > 1.5);
    }
    #[test]
    pub fn  test_linear_combination2()   {
        // we compare accurate versus naive dot product implementations
        // on regular vectors (i.e. not extreme cases like in the previous test)
         let mut random = WellGeneratorFactory::new_well_1024a();
         random.set_seed_i64(&553267312521321234);
         {
             let mut i: usize = 0;
            while i < 10000 {
                {
                     let ux: f64 = 1e17 * random.next_double();
                     let uy: f64 = 1e17 * random.next_double();
                     let uz: f64 = 1e17 * random.next_double();
                     let vx: f64 = 1e17 * random.next_double();
                     let vy: f64 = 1e17 * random.next_double();
                     let vz: f64 = 1e17 * random.next_double();
                     let s_inline: f64 = MathArrays::linear_combination_3d(ux, vx, uy, vy, uz, vz);
                     let s_array: f64 = MathArrays::linear_combination_vec(&vec![ux, uy, uz, ]
                    , &vec![vx, vy, vz, ]
                    ).unwrap();
                    assert_within_delta!(s_inline, s_array, 0.0);
                }
                i += 1;
             }
         }

    }
    
        #[test]
    pub fn  test_linear_combination_huge()   {
         let scale: i32 = 971;
         let a  = vec![-1321008684645961.0 / 268435456.0, -5774608829631843.0 / 268435456.0, -7645843051051357.0 / 8589934592.0, ]
        ;
         let b  = vec![-5712344449280879.0 / 2097152.0, -4550117129121957.0 / 2097152.0, 8846951984510141.0 / 131072.0, ]
        ;
         let mut scaled_a = vec![0.0; a.len()];
         let mut scaled_b = vec![0.0; b.len()];
         {
             let mut i: usize = 0;
            while i < scaled_a.len() {
                {
                    scaled_a[i] = fastmath::F64::scalb(a[i], -scale);
                    scaled_b[i] = fastmath::F64::scalb(b[i], scale);
                }
                i += 1;
             }
         }

         let ab_sum_inline: f64 = MathArrays::linear_combination_3d(scaled_a[0], scaled_b[0], scaled_a[1], scaled_b[1], scaled_a[2], scaled_b[2]);
         let ab_sum_array: f64 = MathArrays::linear_combination_vec(&scaled_a, &scaled_b).unwrap();
        assert_within_delta!(ab_sum_inline, ab_sum_array, 0.0);
        assert_within_delta!(-1.8551294182586248737720779899, ab_sum_inline, 1.0e-15);
         let naive: f64 = scaled_a[0] * scaled_b[0] + scaled_a[1] * scaled_b[1] + scaled_a[2] * scaled_b[2];
        assert!(fastmath::F64::abs(naive - ab_sum_inline) > 1.5);
    }


    #[test]
    pub fn  test_linear_combination_infinite()   {
         let a  = vec![vec![1.0, 2.0, 3.0, 4.0, ]
        , vec![1.0, f64::INFINITY, 3.0, 4.0, ]
        , vec![1.0, 2.0, f64::INFINITY, 4.0, ]
        , vec![1.0, f64::INFINITY, 3.0, f64::NEG_INFINITY, ]
        , vec![1.0, 2.0, 3.0, 4.0, ]
        , vec![1.0, 2.0, 3.0, 4.0, ]
        , vec![1.0, 2.0, 3.0, 4.0, ]
        , vec![1.0, 2.0, 3.0, 4.0, ]
        , ]
        ;
         let b  = vec![vec![1.0, -2.0, 3.0, 4.0, ]
        , vec![1.0, -2.0, 3.0, 4.0, ]
        , vec![1.0, -2.0, 3.0, 4.0, ]
        , vec![1.0, -2.0, 3.0, 4.0, ]
        , vec![1.0, f64::INFINITY, 3.0, 4.0, ]
        , vec![1.0, -2.0, f64::INFINITY, 4.0, ]
        , vec![1.0, f64::INFINITY, 3.0, f64::NEG_INFINITY, ]
        , vec![f64::NAN, -2.0, 3.0, 4.0, ]
        , ]
        ;
        assert_within_delta!(-3.0, MathArrays::linear_combination_2d(a[0][0], b[0][0], a[0][1], b[0][1]), 1.0e-10);
        assert_within_delta!(6.0, MathArrays::linear_combination_3d(a[0][0], b[0][0], a[0][1], b[0][1], a[0][2], b[0][2]), 1.0e-10);
        assert_within_delta!(22.0, MathArrays::linear_combination_4d(a[0][0], b[0][0], a[0][1], b[0][1], a[0][2], b[0][2], a[0][3], b[0][3]), 1.0e-10);
        assert_within_delta!(22.0, MathArrays::linear_combination_vec(&a[0], &b[0]).unwrap(), 1.0e-10);
        assert_within_delta!(f64::NEG_INFINITY, MathArrays::linear_combination_2d(a[1][0], b[1][0], a[1][1], b[1][1]), 1.0e-10);
        assert_within_delta!(f64::NEG_INFINITY, MathArrays::linear_combination_3d(a[1][0], b[1][0], a[1][1], b[1][1], a[1][2], b[1][2]), 1.0e-10);
        assert_within_delta!(f64::NEG_INFINITY, MathArrays::linear_combination_4d(a[1][0], b[1][0], a[1][1], b[1][1], a[1][2], b[1][2], a[1][3], b[1][3]), 1.0e-10);
        assert_within_delta!(f64::NEG_INFINITY, MathArrays::linear_combination_vec(&a[1], &b[1]).unwrap(), 1.0e-10);
        assert_within_delta!(-3.0, MathArrays::linear_combination_2d(a[2][0], b[2][0], a[2][1], b[2][1]), 1.0e-10);
        assert_within_delta!(f64::INFINITY, MathArrays::linear_combination_3d(a[2][0], b[2][0], a[2][1], b[2][1], a[2][2], b[2][2]), 1.0e-10);
        assert_within_delta!(f64::INFINITY, MathArrays::linear_combination_4d(a[2][0], b[2][0], a[2][1], b[2][1], a[2][2], b[2][2], a[2][3], b[2][3]), 1.0e-10);
        assert_within_delta!(f64::INFINITY, MathArrays::linear_combination_vec(&a[2], &b[2]).unwrap(), 1.0e-10);
        assert_within_delta!(f64::NEG_INFINITY, MathArrays::linear_combination_2d(a[3][0], b[3][0], a[3][1], b[3][1]), 1.0e-10);
        assert_within_delta!(f64::NEG_INFINITY, MathArrays::linear_combination_3d(a[3][0], b[3][0], a[3][1], b[3][1], a[3][2], b[3][2]), 1.0e-10);
        assert_within_delta!(f64::NEG_INFINITY, MathArrays::linear_combination_4d(a[3][0], b[3][0], a[3][1], b[3][1], a[3][2], b[3][2], a[3][3], b[3][3]), 1.0e-10);
        assert_within_delta!(f64::NEG_INFINITY, MathArrays::linear_combination_vec(&a[3], &b[3]).unwrap(), 1.0e-10);
        assert_within_delta!(f64::INFINITY, MathArrays::linear_combination_2d(a[4][0], b[4][0], a[4][1], b[4][1]), 1.0e-10);
        assert_within_delta!(f64::INFINITY, MathArrays::linear_combination_3d(a[4][0], b[4][0], a[4][1], b[4][1], a[4][2], b[4][2]), 1.0e-10);
        assert_within_delta!(f64::INFINITY, MathArrays::linear_combination_4d(a[4][0], b[4][0], a[4][1], b[4][1], a[4][2], b[4][2], a[4][3], b[4][3]), 1.0e-10);
        assert_within_delta!(f64::INFINITY, MathArrays::linear_combination_vec(&a[4], &b[4]).unwrap(), 1.0e-10);
        assert_within_delta!(-3.0, MathArrays::linear_combination_2d(a[5][0], b[5][0], a[5][1], b[5][1]), 1.0e-10);
        assert_within_delta!(f64::INFINITY, MathArrays::linear_combination_3d(a[5][0], b[5][0], a[5][1], b[5][1], a[5][2], b[5][2]), 1.0e-10);
        assert_within_delta!(f64::INFINITY, MathArrays::linear_combination_4d(a[5][0], b[5][0], a[5][1], b[5][1], a[5][2], b[5][2], a[5][3], b[5][3]), 1.0e-10);
        assert_within_delta!(f64::INFINITY, MathArrays::linear_combination_vec(&a[5], &b[5]).unwrap(), 1.0e-10);
        assert_within_delta!(f64::INFINITY, MathArrays::linear_combination_2d(a[6][0], b[6][0], a[6][1], b[6][1]), 1.0e-10);
        assert_within_delta!(f64::INFINITY, MathArrays::linear_combination_3d(a[6][0], b[6][0], a[6][1], b[6][1], a[6][2], b[6][2]), 1.0e-10);
        assert!(&f64::is_nan(MathArrays::linear_combination_4d(a[6][0], b[6][0], a[6][1], b[6][1], a[6][2], b[6][2], a[6][3], b[6][3])));
        assert!(&f64::is_nan(MathArrays::linear_combination_vec(&a[6], &b[6]).unwrap()));
        assert!(&f64::is_nan(MathArrays::linear_combination_2d(a[7][0], b[7][0], a[7][1], b[7][1])));
        assert!(&f64::is_nan(MathArrays::linear_combination_3d(a[7][0], b[7][0], a[7][1], b[7][1], a[7][2], b[7][2])));
        assert!(&f64::is_nan(MathArrays::linear_combination_4d(a[7][0], b[7][0], a[7][1], b[7][1], a[7][2], b[7][2], a[7][3], b[7][3])));
        assert!(&f64::is_nan(MathArrays::linear_combination_vec(&a[7], &b[7]).unwrap()));
    }

	#[test]
    pub fn  test_array_equals()   {
        assert!(!&MathArrays::equals(&vec![1.0, ]
        , &vec! [0.0; 0]));
        assert!(&MathArrays::equals(&vec![1.0, ]
        , &vec![1.0, ]
        ));
        assert!(&MathArrays::equals(&vec![f64::INFINITY, f64::NEG_INFINITY, 1.0, 0.0, ]
        , &vec![f64::INFINITY, f64::NEG_INFINITY, 1.0, 0.0, ]
        ));
        assert!(!&MathArrays::equals(&vec![f64::NAN, ]
        , &vec![f64::NAN, ]
        ));
        assert!(!&MathArrays::equals(&vec![f64::INFINITY, ]
        ,&vec![f64::NEG_INFINITY, ]
        ));
        assert!(!&MathArrays::equals(&vec![1.0, ]
        ,&vec![fastmath::F64::next_after(fastmath::F64::next_after(1.0, 2.0), 2.0), ]
        ));
    }

    #[test]
    pub fn  test_array_equals_including_nan()   {
        assert!(!&MathArrays::equals_including_nan(&vec![1.0, ]
        , &vec![0.0; 0]));
        assert!(&MathArrays::equals_including_nan(&vec![1.0, ]
        ,&vec![1.0, ]
        ));
        assert!(&MathArrays::equals_including_nan(&vec![f64::NAN, f64::INFINITY, f64::NEG_INFINITY, 1.0, 0.0, ]
        ,&vec![f64::NAN, f64::INFINITY, f64::NEG_INFINITY, 1.0, 0.0, ]
        ));
        assert!(!&MathArrays::equals_including_nan(&vec![f64::INFINITY, ]
        ,&vec![f64::NEG_INFINITY, ]
        ));
        assert!(!&MathArrays::equals_including_nan(&vec![1.0, ]
        ,&vec![fastmath::F64::next_after(fastmath::F64::next_after(1.0, 2.0), 2.0), ]
        ));
    }

 #[test]
    pub fn  test_normalize_array()   {
         let test_values1  = vec![1.0, 1.0, 2.0, ]
        ;
        TestUtils::assert_equals_f64_arrays(&vec![0.25, 0.25, 0.5, ]
        , &MathArrays::normalize_array(&test_values1, 1.0).unwrap(), f64::MIN);
         
         let test_values2  = vec![-1.0, -1.0, 1.0, ]
        ;
        TestUtils::assert_equals_f64_arrays(&vec![1.0, 1.0, -1.0, ]
        , &MathArrays::normalize_array(&test_values2, 1.0).unwrap(), f64::MIN);
        // Ignore NaNs
         let test_values3  = vec![-1.0, -1.0, f64::NAN, 1.0, f64::NAN, ]
        ;
        TestUtils::assert_equals_f64_arrays(&vec![1.0, 1.0, f64::NAN, -1.0, f64::NAN, ]
        , &MathArrays::normalize_array(&test_values3, 1.0).unwrap(), f64::MIN);
        // Zero sum -> MathArithmeticException
         let zero_sum  = vec![-1.0, 1.0, ];
        
            assert!(MathArrays::normalize_array(&zero_sum, 1.0).is_err());
        
        // Infinite elements -> MathArithmeticException
         let has_inf  = vec![1.0, 2.0, 1.0, f64::NEG_INFINITY, ];
         assert!(MathArrays::normalize_array(&has_inf, 1.0).is_err());
         assert!(MathArrays::normalize_array(&test_values1, f64::INFINITY).is_err());
         assert!(MathArrays::normalize_array(&test_values1, f64::NAN).is_err());
        

    }

#[test]
    pub fn  test_convolve()   {
        /* Test Case (obtained via SciPy)
         * x=[1.2,-1.8,1.4]
         * h=[1.0,0.8,0.5,0.3]
         * convolve(x,h) -> array([ 1.2 , -0.84,  0.56,  0.58,  0.16,  0.42])
         */
         let x1 = vec![1.2, -1.8, 1.4, ]
        ;
         let h1 = vec![1.0, 0.8, 0.5, 0.3, ]
        ;
         let y1 = vec![1.2, -0.84, 0.56, 0.58, 0.16, 0.42, ]
        ;
         let tolerance: f64 = 1e-13;
         let mut y_actual: Vec<f64> = MathArrays::convolve(&x1, &h1).unwrap();
        TestUtils::assert_equals_f64_arrays(&y1, &y_actual, tolerance);
         let x2 = vec![1.0, 2.0, 3.0, ]
        ;
         let h2 = vec![0.0, 1.0, 0.5, ]
        ;
         let y2 = vec![0.0, 1.0, 2.5, 4.0, 1.5, ]
        ;
        y_actual = MathArrays::convolve(&x2, &h2).unwrap();
        TestUtils::assert_equals_f64_arrays(&y2, &y_actual, tolerance);
        
           
            assert!(MathArrays::convolve(&vec![1.0, 2.0, ]
            ,  &Vec::new()).is_err());
            
            assert!(MathArrays::convolve(&Vec::new() ,&vec![1.0, 2.0, ]
            ).is_err());
            
            assert!(MathArrays::convolve( &Vec::new(), &Vec::new() ).is_err());
           

    }
    
     #[test]
    pub fn  test_shuffle_tail()   {
         let orig  = vec![0, 1, 2, 3, 4, 5, 6, 7, 8, 9, ]
        ;
         let mut list: Vec<i32> = orig.clone();
         let start: usize = 4;
         let mut gen = WellGeneratorFactory::new_well_1024a();
         gen.set_seed_i64(&7654321);
        MathArrays::shuffle_start_pos_gen(&mut list, start, Position::TAIL, Rc::new(RefCell::new(gen)));
        // Ensure that all entries below index "start" did not move.
         {
             let mut i: usize = 0;
            while i < start {
                {
                    assert_eq!(orig[i], list[i]);
                }
                i += 1;
             }
         }

        // Ensure that at least one entry has moved.
         let mut ok: bool = false;
         {
             let mut i: usize = start;
            while i < orig.len() - 1 {
                {
                    if orig[i] != list[i] {
                        ok = true;
                        break;
                    }
                }
                i += 1;
             }
         }

        assert!(ok);
    }

 #[test]
    pub fn  test_shuffle_head()   {
         let orig  = vec![0, 1, 2, 3, 4, 5, 6, 7, 8, 9, ]
        ;
         let mut list: Vec<i32> = orig.clone();
         let start: usize = 4;
         let mut gen = WellGeneratorFactory::new_well_1024a();
         gen.set_seed_i64(&1234567);
        MathArrays::shuffle_start_pos_gen(&mut list, start, Position::HEAD, Rc::new(RefCell::new(gen)));
        // Ensure that all entries above index "start" did not move.
         {
             let mut i: usize = start + 1;
            while i < orig.len() {
                {
                    assert_eq!(orig[i], list[i]);
                }
                i += 1;
             }
         }

        // Ensure that at least one entry has moved.
         let mut ok: bool = false;
         {
             let mut i: usize = 0;
            while i <= start {
                {
                    if orig[i] != list[i] {
                        ok = true;
                        break;
                    }
                }
                i += 1;
             }
         }

        assert!(ok);
    }

 #[test]
    pub fn  test_natural()   {
         let n: usize = 4;
         let expected = vec![0, 1, 2, 3, ]
        ;
         let natural: Vec<i32> = MathArrays::natural(n);
         {
             let mut i: usize = 0;
            while i < n {
                {
                    assert_eq!(expected[i], natural[i]);
                }
                i += 1;
             }
         }

    }

       #[test]
    pub fn  test_natural_zero()   {
         let natural: Vec<i32> = MathArrays::natural(0);
	    assert_eq!(0, natural.len());
    }

    #[test]
    pub fn  test_sequence()   {
         let size: usize = 4;
         let start: usize = 5;
         let stride: usize = 2;
         let expected = vec![5, 7, 9, 11, ]
        ;
         let seq: Vec<i32> = MathArrays::sequence(size, start, stride);
         {
             let mut i: usize = 0;
            while i < size {
                {
                    assert_eq!(expected[i], seq[i]);
                }
                i += 1;
             }
         }

    }

    #[test]
    pub fn  test_sequence_zero()   {
         let seq: Vec<i32> = MathArrays::sequence(0, 12345, 6789);
        assert_eq!(0, seq.len());
    }
    
    fn to_vec<T: Copy>(slice: &[T]) -> Vec<T> {
    	let mut res = Vec::new();
    	for i in slice {
    		res.push(*i);
    	}
    	res
    }
    
        #[test]
    pub fn  test_verify_values_positive()   {
         {
             let mut j: usize = 0;
            while j < 6 {
                {
                     {
                         let mut i: usize = 1;
                        while i < (7 - j) {
                            {
                                assert!(MathArrays::verify_values_begin_length(&to_vec(&test_array), 0, i as i32).is_ok());
                            }
                            i += 1;
                         }
                     }

                }
                j += 1;
             }
         }

        assert!(&MathArrays::verify_values_begin_length(&to_vec(&singleton_array), 0, 1).is_ok());
        assert!(&MathArrays::verify_values_begin_length_allow_empty(&to_vec(&singleton_array), 0, 0, true).is_ok());
    }

    #[test]
    pub fn  test_verify_values_negative()   {
        assert!(!&MathArrays::verify_values_begin_length(&to_vec(&singleton_array), 0, 0).unwrap());
        assert!(!&MathArrays::verify_values_begin_length(&to_vec(&test_array), 0, 0).unwrap());
        
            // start past end
            assert!(MathArrays::verify_values_begin_length(&to_vec(&singleton_array), 2, 1).is_err());
         
            // end past end
            assert!(MathArrays::verify_values_begin_length(&to_vec(&test_array), 0, 7).is_err());
           
            // start negative
            assert!(MathArrays::verify_values_begin_length(&to_vec(&test_array), -1, 1).is_err());
            
            // len() negative
           assert!( MathArrays::verify_values_begin_length(&to_vec(&test_array), 0, -1).is_err());
           
            // weights.len() != value.len()
            assert!(MathArrays::verify_values_weigths_begin_length(&to_vec(&singleton_array), &to_vec(&test_weights_array), 0, 1).is_err());
           
            // can't have negative weights
            assert!(MathArrays::verify_values_weigths_begin_length(&to_vec(&test_array), &to_vec(&test_negative_weights_array), 0, 6).is_err());
            
    }
    
        #[test]
    pub fn  test_concatenate()   {
         let u  = vec![0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, ]
        ;
         let x  = vec![0.0, 1.0, 2.0, ]
        ;
         let y  = vec![3.0, 4.0, 5.0, 6.0, 7.0, 8.0, ]
        ;
         let z  = vec![9.0, ]
        ;
        TestUtils::assert_equals_f64_arrays(&u, &MathArrays::concatenate(&vec![&x, &y, &z]), 0.0);
    }

    #[test]
    pub fn  test_concatentate_single()   {
         let x  = vec![0.0, 1.0, 2.0, ]
        ;
        TestUtils::assert_equals_f64_arrays(&x, &MathArrays::concatenate(&vec![&x]), 0.0);
    }

	#[test]
    pub fn  test_concatenate_empty_arguments()   {
         let x  = vec![0.0, 1.0, 2.0, ]
        ;
         let y  = vec![3.0, ]
        ;
         let z = Vec::new();
         let u  = vec![0.0, 1.0, 2.0, 3.0, ]
        ;
        TestUtils::assert_equals_f64_arrays(&u, &MathArrays::concatenate(&vec![&x, &z, &y]), 0.0);
        TestUtils::assert_equals_f64_arrays(&u, &MathArrays::concatenate(&vec![&x, &y, &z]), 0.0);
        TestUtils::assert_equals_f64_arrays(&u, &MathArrays::concatenate(&vec![&z, &x, &y]), 0.0);
        assert_eq!(0, MathArrays::concatenate(&vec![&z, &z, &z]).len());
    }
    
    
    #[test]
    pub fn  test_unique()   {
         let x = vec![0.0, 9.0, 3.0, 0.0, 11.0, 7.0, 3.0, 5.0, -1.0, -2.0, ]
        ;
         let values = vec![11.0, 9.0, 7.0, 5.0, 3.0, 0.0, -1.0, -2.0, ]
        ;
        TestUtils::assert_equals_f64_arrays(&values, &MathArrays::unique(&x), 0.0);
    }
 
    #[test]
    pub fn  test_unique_infinite_values()   {
         let x = vec![0.0, f64::NEG_INFINITY, 3.0, f64::NEG_INFINITY, 3.0, f64::INFINITY, f64::INFINITY, ]
        ;
         let u = vec![f64::INFINITY, 3.0, 0.0, f64::NEG_INFINITY, ]
        ;
        TestUtils::assert_equals_f64_arrays(&u, &MathArrays::unique(&x), 0.0);
    }
    
    
     #[test]
    pub fn  test_unique_nan_values()   {
         let x  = vec![10.0, 2.0, f64::NAN, f64::NAN, f64::NAN, f64::INFINITY, f64::INFINITY, f64::NEG_INFINITY, ]
        ;
         let u: Vec<f64> = MathArrays::unique(&x);
        assert_eq!(5, u.len());
        assert!(f64::is_nan(u[0]));
        assert_within_delta!(f64::INFINITY, u[1], 0.0);
        assert_within_delta!(10.0, u[2], 0.0);
        assert_within_delta!(2.0, u[3], 0.0);
        assert_within_delta!(f64::NEG_INFINITY, u[4], 0.0);
    }



}