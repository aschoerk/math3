              
                   /**
     * Utility that increments a counter until a maximum is reached, at
     * which point, the instance will by default throw a
     * {@link MaxCountExceededException}.
     * However, the user is able to override this behaviour by defining a
     * custom {@link MaxCountExceededCallback callback}, in order to e.g.
     * select which exception must be thrown.
     */

/*
/** Default callback. */
 const CALLBACK: MaxCountExceededCallback = MaxCountExceededCallback::new() {

    /** {@inheritDoc} */
    pub fn  trigger(&self,  max: i32)  -> /*  throws MaxCountExceededException */Result<Void, Rc<Exception>>   {
        throw MaxCountExceededException::new(max);
    }
};
 */

pub struct Incrementor {

    /** Initial value the counter. */
     pub init: i32;

    /** Upper limit for the counter. */
     pub maximal_count: i32;

    /** Increment. */
     pub increment: i32;

    /** Function called at counter exhaustion. */
     pub max_count_callback: MaxCountExceededCallback;

    /** Current count. */
     pub mut count: i32 = 0;
}

impl Incrementor {

/*
    /**
         * Defines a method to be called at counter exhaustion.
         * The {@link #trigger(int) trigger} method should usually throw an exception.
         */
    pub trait MaxCountExceededCallback {

        /**
             * Function called when the maximal count has been reached.
             *
             * @param maximalCount Maximal count.
             * @throws MaxCountExceededException at counter exhaustion
             */
        fn  trigger(&self,  maximal_count: i32)  -> /*  throws MaxCountExceededException */Result<Void, Rc<Exception>>  ;
    }
*/

    /**
         * Creates an incrementor.
         * The counter will be exhausted either when {@code max} is reached
         * or when {@code nTimes} increments have been performed.
         *
         * @param start Initial value.
         * @param max Maximal count.
         * @param step Increment.
         * @param cb Function to be called when the maximal count has been reached.
         * @throws NullArgumentException if {@code cb} is {@code null}.
         */
    fn new( start: i32,  max: i32,  step: i32,  cb: &MaxCountExceededCallback) -> Incrementor throws NullArgumentException {
        if cb == null {
            throw NullArgumentException::new();
        }
        let .init = start;
        let .maximalCount = max;
        let .increment = step;
        let .maxCountCallback = cb;
        let .count = start;
    }

    /**
         * Factory method that creates a default instance.
         * The initial and maximal values are set to 0.
         * For the new instance to be useful, the maximal count must be set
         * by calling {@link #withMaximalCount(int) withMaximalCount}.
         *
         * @return an new instance.
         */
    pub fn  create() -> Incrementor  {
        return Incrementor::new(0, 0, 1, CALLBACK);
    }

    /**
         * Creates a new instance with a given initial value.
         * The counter is reset to the initial value.
         *
         * @param start Initial value of the counter.
         * @return a new instance.
         */
    pub fn  with_start(&self,  start: i32) -> Incrementor  {
        return Incrementor::new(start, self.maximalCount, self.increment, self.maxCountCallback);
    }

    /**
         * Creates a new instance with a given maximal count.
         * The counter is reset to the initial value.
         *
         * @param max Maximal count.
         * @return a new instance.
         */
    pub fn  with_maximal_count(&self,  max: i32) -> Incrementor  {
        return Incrementor::new(self.init, max, self.increment, self.maxCountCallback);
    }

    /**
         * Creates a new instance with a given increment.
         * The counter is reset to the initial value.
         *
         * @param step Increment.
         * @return a new instance.
         */
    pub fn  with_increment(&self,  step: i32) -> Incrementor  {
        if step == 0 {
            throw ZeroException::new();
        }
        return Incrementor::new(self.init, self.maximalCount, step, self.maxCountCallback);
    }

    /**
         * Creates a new instance with a given callback.
         * The counter is reset to the initial value.
         *
         * @param cb Callback to be called at counter exhaustion.
         * @return a new instance.
         */
    pub fn  with_callback(&self,  cb: &MaxCountExceededCallback) -> Incrementor  {
        return Incrementor::new(self.init, self.maximalCount, self.increment, cb);
    }

    /**
         * Gets the upper limit of the counter.
         *
         * @return the counter upper limit.
         */
    pub fn  get_maximal_count(&self) -> i32  {
        return self.maximal_count;
    }

    /**
         * Gets the current count.
         *
         * @return the current count.
         */
    pub fn  get_count(&self) -> i32  {
        return self.count;
    }

    /**
         * Checks whether incrementing the counter {@code nTimes} is allowed.
         *
         * @return {@code false} if calling {@link #increment()}
         * will trigger a {@code MaxCountExceededException},
         * {@code true} otherwise.
         */
    pub fn  can_increment(&self) -> bool  {
        return self.can_increment(1);
    }

    /**
         * Checks whether incrementing the counter several times is allowed.
         *
         * @param nTimes Number of increments.
         * @return {@code false} if calling {@link #increment(int)
         * increment(nTimes)} would call the {@link MaxCountExceededCallback callback}
         * {@code true} otherwise.
         */
    pub fn  can_increment(&self,  n_times: i32) -> bool  {
         let final_count: i32 = self.count + n_times * self.increment;
        return  if self.increment < 0 { final_count > self.maximal_count } else { final_count < self.maximal_count };
    }

    /**
         * Performs multiple increments.
         *
         * @param nTimes Number of increments.
         * @throws MaxCountExceededException at counter exhaustion.
         * @throws NotStrictlyPositiveException if {@code nTimes <= 0}.
         *
         * @see #increment()
         */
    pub fn  increment(&self,  n_times: i32)  -> /*  throws MaxCountExceededException */Result<Void, Rc<Exception>>   {
        if n_times <= 0 {
            throw NotStrictlyPositiveException::new(n_times);
        }
        if !self.can_increment(0) {
            self.max_count_callback.trigger(self.maximal_count);
        }
        self.count += n_times * self.increment;
    }

    /**
         * Adds the increment value to the current iteration count.
         * At counter exhaustion, this method will call the
         * {@link MaxCountExceededCallback#trigger(int) trigger} method of the
         * callback object passed to the
         * {@link #withCallback(MaxCountExceededCallback)} method.
         * If not explicitly set, a default callback is used that will throw
         * a {@code MaxCountExceededException}.
         *
         * @throws MaxCountExceededException at counter exhaustion, unless a
         * custom {@link MaxCountExceededCallback callback} has been set.
         *
         * @see #increment(int)
         */
    pub fn  increment(&self)  -> /*  throws MaxCountExceededException */Result<Void, Rc<Exception>>   {
        self.increment(1);
    }

    pub fn  has_next(&self) -> bool  {
        return self.can_increment(0);
    }

    pub fn  next(&self) -> Integer  {
         let value: i32 = self.count;
        self.increment();
        return value;
    }

    /**
         * Not applicable.
         *
         * @throws MathUnsupportedOperationException
         */
    pub fn  remove(&self)   {
        throw MathUnsupportedOperationException::new();
    }
}