                
                   /*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org::apache::commons::math3::util;

use java::io::PrintStream;
use org::apache::commons::math3::exception::MathArithmeticException;
use org::apache::commons::math3::exception::util::LocalizedFormats;

/**
 * Faster, more accurate, portable alternative to {@link Math} and
 * {@link StrictMath} for large scale computation.
 * <p>
 * FastMath is a drop-in replacement for both Math and StrictMath. This
 * means that for any method in Math (say {@code Math.sin(x)} or
 * {@code Math.cbrt(y)}), user can directly change the class and use the
 * methods as is (using {@code FastMath.sin(x)} or {@code FastMath.cbrt(y)}
 * in the previous example).
 * </p>
 * <p>
 * FastMath speed is achieved by relying heavily on optimizing compilers
 * to native code present in many JVMs today and use of large tables.
 * The larger tables are lazily initialised on first use, so that the setup
 * time does not penalise methods that don't need them.
 * </p>
 * <p>
 * Note that FastMath is
 * extensively used inside Apache Commons Math, so by calling some algorithms,
 * the overhead when the the tables need to be intialised will occur
 * regardless of the end-user calling FastMath methods directly or not.
 * Performance figures for a specific JVM and hardware can be evaluated by
 * running the FastMathTestPerformance tests in the test directory of the source
 * distribution.
 * </p>
 * <p>
 * FastMath accuracy should be mostly independent of the JVM as it relies only
 * on IEEE-754 basic operations and on embedded tables. Almost all operations
 * are accurate to about 0.5 ulp throughout the domain range. This statement,
 * of course is only a rough global observed behavior, it is <em>not</em> a
 * guarantee for <em>every</em> double numbers input (see William Kahan's <a
 * href="http://en.wikipedia.org/wiki/Rounding#The_table-maker.27s_dilemma">Table
 * Maker's Dilemma</a>).
 * </p>
 * <p>
 * FastMath additionally implements the following methods not found in Math/StrictMath:
 * <ul>
 * <li>{@link #asinh(double)}</li>
 * <li>{@link #acosh(double)}</li>
 * <li>{@link #atanh(double)}</li>
 * </ul>
 * The following methods are found in Math/StrictMath since 1.6 only, they are provided
 * by FastMath even in 1.5 Java virtual machines
 * <ul>
 * <li>{@link #copySign(double, double)}</li>
 * <li>{@link #getExponent(double)}</li>
 * <li>{@link #nextAfter(double,double)}</li>
 * <li>{@link #nextUp(double)}</li>
 * <li>{@link #scalb(double, int)}</li>
 * <li>{@link #copySign(float, float)}</li>
 * <li>{@link #getExponent(float)}</li>
 * <li>{@link #nextAfter(float,double)}</li>
 * <li>{@link #nextUp(float)}</li>
 * <li>{@link #scalb(float, int)}</li>
 * </ul>
 * </p>
 * @since 2.2
 */

/** Archimede's constant PI, ratio of circle circumference to diameter. */
 const PI: f64 = 105414357.0 / 33554432.0 + 1.984187159361080883e-9;

/** Napier's constant e, base of the natural logarithm. */
 const E: f64 = 2850325.0 / 1048576.0 + 8.254840070411028747e-8;

/** Index of exp(0) in the array of integer exponentials. */
 const EXP_INT_TABLE_MAX_INDEX: i32 = 750;

/** Length of the array of integer exponentials. */
 const EXP_INT_TABLE_LEN: i32 = EXP_INT_TABLE_MAX_INDEX * 2;

/** Logarithm table length. */
 const LN_MANT_LEN: i32 = 1024;

/** Exponential fractions table length. */
// 0, 1/1024, ... 1024/1024
 const EXP_FRAC_TABLE_LEN: i32 = 1025;

/** StrictMath.log(Double.MAX_VALUE): {@value} */
 const LOG_MAX_VALUE: f64 = StrictMath.log(Double::MAX_VALUE);

/** Indicator for tables initialization.
     * <p>
     * This compile-time constant should be set to true only if one explicitly
     * wants to compute the tables at class loading time instead of using the
     * already computed ones provided as literal arrays below.
     * </p>
     */
 const RECOMPUTE_TABLES_AT_RUNTIME: bool = false;

/** log(2) (high bits). */
 const LN_2_A: f64 = 0.693147063255310059;

/** log(2) (low bits). */
 const LN_2_B: f64 = 1.17304635250823482e-7;

/** Coefficients for log, when input 0.99 < x < 1.01. */
 const LN_QUICK_COEF: [[f64; 2]; 9] = [[1.0, 5.669184079525E-24, ]
, [-0.25, -0.25, ]
, [0.3333333134651184, 1.986821492305628E-8, ]
, [-0.25, -6.663542893624021E-14, ]
, [0.19999998807907104, 1.1921056801463227E-8, ]
, [-0.1666666567325592, -7.800414592973399E-9, ]
, [0.1428571343421936, 5.650007086920087E-9, ]
, [-0.12502530217170715, -7.44321345601866E-11, ]
, [0.11113807559013367, 9.219544613762692E-9, ]
, ]
;

/** Coefficients for log in the range of 1.0 < x < 1.0 + 2^-10. */
 const LN_HI_PREC_COEF: [[f64; 2]; 6] = [[1.0, -6.032174644509064E-23, ]
, [-0.25, -0.25, ]
, [0.3333333134651184, 1.9868161777724352E-8, ]
, [-0.2499999701976776, -2.957007209750105E-8, ]
, [0.19999954104423523, 1.5830993332061267E-10, ]
, [-0.16624879837036133, -2.6033824355191673E-8, ]
, ]
;

/** Sine, Cosine, Tangent tables are for 0, 1/8, 2/8, ... 13/8 = PI/2 approx. */
 const SINE_TABLE_LEN: i32 = 14;

/** Sine table (high bits). */
 const SINE_TABLE_A: [f64; 14] = [0.0, 0.1246747374534607, 0.24740394949913025, 0.366272509098053, 0.4794255495071411, 0.5850973129272461, 0.6816387176513672, 0.7675435543060303, 0.8414709568023682, 0.902267575263977, 0.9489846229553223, 0.9808930158615112, 0.9974949359893799, 0.9985313415527344, ]
;

/** Sine table (low bits). */
 const SINE_TABLE_B: [f64; 14] = [0.0, -4.068233003401932E-9, 9.755392680573412E-9, 1.9987994582857286E-8, -1.0902938113007961E-8, -3.9986783938944604E-8, 4.23719669792332E-8, -5.207000323380292E-8, 2.800552834259E-8, 1.883511811213715E-8, -3.5997360512765566E-9, 4.116164446561962E-8, 5.0614674548127384E-8, -1.0129027912496858E-9, ]
;

/** Cosine table (high bits). */
 const COSINE_TABLE_A: [f64; 14] = [1.0, 0.9921976327896118, 0.9689123630523682, 0.9305076599121094, 0.8775825500488281, 0.8109631538391113, 0.7316888570785522, 0.6409968137741089, 0.5403022766113281, 0.4311765432357788, 0.3153223395347595, 0.19454771280288696, 0.07073719799518585, -0.05417713522911072, ]
;

/** Cosine table (low bits). */
 const COSINE_TABLE_B: [f64; 14] = [0.0, 3.4439717236742845E-8, 5.865827662008209E-8, -3.7999795083850525E-8, 1.184154459111628E-8, -3.43338934259355E-8, 1.1795268640216787E-8, 4.438921624363781E-8, 2.925681159240093E-8, -2.6437112632041807E-8, 2.2860509143963117E-8, -4.813899778443457E-9, 3.6725170580355583E-9, 2.0217439756338078E-10, ]
;

/** Tangent table, used by atan() (high bits). */
 const TANGENT_TABLE_A: [f64; 14] = [0.0, 0.1256551444530487, 0.25534194707870483, 0.3936265707015991, 0.5463024377822876, 0.7214844226837158, 0.9315965175628662, 1.1974215507507324, 1.5574076175689697, 2.092571258544922, 3.0095696449279785, 5.041914939880371, 14.101419448852539, -18.430862426757812, ]
;

/** Tangent table, used by atan() (low bits). */
 const TANGENT_TABLE_B: [f64; 14] = [0.0, -7.877917738262007E-9, -2.5857668567479893E-8, 5.2240336371356666E-9, 5.206150291559893E-8, 1.8307188599677033E-8, -5.7618793749770706E-8, 7.848361555046424E-8, 1.0708593250394448E-7, 1.7827257129423813E-8, 2.893485277253286E-8, 3.1660099222737955E-7, 4.983191803254889E-7, -3.356118100840571E-7, ]
;

/** Bits of 1/(2*pi), need for reducePayneHanek(). */
 const RECIP_2PI : [i64; 18] = [(0x28be60db << 32) | 0x9391054a, (0x7f09d5f4 << 32) | 0x7d4d3770, (0x36d8a566 << 32) | 0x4f10e410, (0x7f9458ea << 32) | 0xf7aef158, (0x6dc91b8e << 32) | 0x909374b8, (0x01924bba << 32) | 0x82746487, (0x3f877ac7 << 32) | 0x2c4a69cf, (0xba208d7d << 32) | 0x4baed121, (0x3a671c09 << 32) | 0xad17df90, (0x4e64758e << 32) | 0x60d4ce7d, (0x272117e2 << 32) | 0xef7e4a0e, (0xc7fe25ff << 32) | 0xf7816603, (0xfbcbc462 << 32) | 0xd6829b47, (0xdb4d9fb3 << 32) | 0xc9f2c26d, (0xd3d18fd9 << 32) | 0xa797fa8b, (0x5d49eeb1 << 32) | 0xfaf97c5e, (0xcf41ce7d << 32) | 0xe294a4ba, 0x9afed7ec << 32, ]
;

/** Bits of pi/4, need for reducePayneHanek(). */
 const PI_O_4_BITS : [i64; 2] = [(0xc90fdaa2 << 32) | 0x2168c234, (0xc4c6628b << 32) | 0x80dc1cd1, ]
;

/** Eighths.
     * This is used by sinQ, because its faster to do a table lookup than
     * a multiply in this time-critical routine
     */
 const EIGHTHS: [f64; 14] = [0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1.0, 1.125, 1.25, 1.375, 1.5, 1.625, ]
;

/** Table of 2^((n+2)/3) */
 const CBRTTWO: [f64; 5] = [0.6299605249474366, 0.7937005259840998, 1.0, 1.2599210498948732, 1.5874010519681994, ]
;

/*
     *  There are 52 bits in the mantissa of a double.
     *  For additional precision, the code splits double numbers into two parts,
     *  by clearing the low order 30 bits if possible, and then performs the arithmetic
     *  on each half separately.
     */
/**
     * 0x40000000 - used to split a double into two parts, both with the low order bits cleared.
     * Equivalent to 2^30.
     */
// 1073741824L
 const HEX_40000000: i64 = 0x40000000;

/** Mask used to clear low order 30 bits */
// 0xFFFFFFFFC0000000L;
 const MASK_30BITS: i64 = -1 - (HEX_40000000 - 1);

/** Mask used to clear the non-sign part of an int. */
 const MASK_NON_SIGN_INT: i32 = 0x7fffffff;

/** Mask used to clear the non-sign part of a long. */
 const MASK_NON_SIGN_LONG: i64 = 0x7fffffffffffffff;

/** Mask used to extract exponent from double bits. */
 const MASK_DOUBLE_EXPONENT: i64 = 0x7ff0000000000000;

/** Mask used to extract mantissa from double bits. */
 const MASK_DOUBLE_MANTISSA: i64 = 0x000fffffffffffff;

/** Mask used to add implicit high order bit for normalized double. */
 const IMPLICIT_HIGH_BIT: i64 = 0x0010000000000000;

/** 2^52 - double numbers this large must be integral (no fraction) or NaN or Infinite */
 const TWO_POWER_52: f64 = 4503599627370496.0;

/** Constant: {@value}. */
 const F_1_3: f64 = 1d.0 / 3d.0;

/** Constant: {@value}. */
 const F_1_5: f64 = 1d.0 / 5d.0;

/** Constant: {@value}. */
 const F_1_7: f64 = 1d.0 / 7d.0;

/** Constant: {@value}. */
 const F_1_9: f64 = 1d.0 / 9d.0;

/** Constant: {@value}. */
 const F_1_11: f64 = 1d.0 / 11d.0;

/** Constant: {@value}. */
 const F_1_13: f64 = 1d.0 / 13d.0;

/** Constant: {@value}. */
 const F_1_15: f64 = 1d.0 / 15d.0;

/** Constant: {@value}. */
 const F_1_17: f64 = 1d.0 / 17d.0;

/** Constant: {@value}. */
 const F_3_4: f64 = 3d.0 / 4d.0;

/** Constant: {@value}. */
 const F_15_16: f64 = 15d.0 / 16d.0;

/** Constant: {@value}. */
 const F_13_14: f64 = 13d.0 / 14d.0;

/** Constant: {@value}. */
 const F_11_12: f64 = 11d.0 / 12d.0;

/** Constant: {@value}. */
 const F_9_10: f64 = 9d.0 / 10d.0;

/** Constant: {@value}. */
 const F_7_8: f64 = 7d.0 / 8d.0;

/** Constant: {@value}. */
 const F_5_6: f64 = 5d.0 / 6d.0;

/** Constant: {@value}. */
 const F_1_2: f64 = 1d.0 / 2d.0;

/** Constant: {@value}. */
 const F_1_4: f64 = 1d.0 / 4d.0;
pub struct FastMath {
}

impl FastMath {

    /**
     * Private Constructor
     */
    fn new() -> FastMath {
    }

    // Generic helper methods
    /**
     * Get the high order bits from the mantissa.
     * Equivalent to adding and subtracting HEX_40000 but also works for very large numbers
     *
     * @param d the value to split
     * @return the high order part of the mantissa
     */
    fn  double_high_part( d: f64) -> f64  {
        if d > -Precision::SAFE_MIN && d < Precision::SAFE_MIN {
            // These are un-normalised - don't try to convert
            return d;
        }
        // can take raw bits because just gonna convert it back
         let mut xl: i64 = Double.double_to_raw_long_bits(d);
        // Drop low order bits
        xl &= MASK_30BITS;
        return Double.long_bits_to_double(xl);
    }

 
    
    

    

    /** Returns a pseudo-random number between 0.0 and 1.0.
     * <p><b>Note:</b> this implementation currently delegates to {@link Math#random}
     * @return a random number between 0.0 and 1.0
     */
    pub fn  random() -> f64  {
        return Math.random();
    }

   
    



    /** Class operator on double numbers split into one 26 bits number and one 27 bits number. */

    /** Split version of NaN. */
     const NAN: Split = Split::new(Double::NaN, 0);

    /** Split version of positive infinity. */
     const INFINITY: Split = Split::new(Double::POSITIVE_INFINITY, 0);

    /** Split version of negative infinity. */
     const NEG_INFINITY: Split = Split::new(Double::NEGATIVE_INFINITY, 0);
    struct Split {

        /** Full number. */
         let mut full: f64;

        /** High order bits. */
         let mut high: f64;

        /** Low order bits. */
         let mut low: f64;
    }
    
    impl Split {

        /** Simple constructor.
         * @param x number to split
         */
        fn new( x: f64) -> Split {
            full = x;
            high = Double.long_bits_to_double(Double.double_to_raw_long_bits(x) & ((-1) << 27));
            low = x - high;
        }

        /** Simple constructor.
         * @param high high order bits
         * @param low low order bits
         */
        fn new( high: f64,  low: f64) -> Split {
            this( if high == 0.0 { ( if low == 0.0 && Double.double_to_raw_long_bits(high) == Long::MIN_VALUE { /* negative zero */
            /* negative zero */
            -0.0 } else { low }) } else { high + low }, high, low);
        }

        /** Simple constructor.
         * @param full full number
         * @param high high order bits
         * @param low low order bits
         */
        fn new( full: f64,  high: f64,  low: f64) -> Split {
            let .full = full;
            let .high = high;
            let .low = low;
        }

        /** Multiply the instance by another one.
         * @param b other instance to multiply by
         * @return product
         */
        pub fn  multiply(&self,  b: &Split) -> Split  {
            // beware the following expressions must NOT be simplified, they rely on floating point arithmetic properties
             let mul_basic: Split = Split::new(self.full * b.full);
             let mul_error: f64 = self.low * b.low - (((mul_basic.full - self.high * b.high) - self.low * b.high) - self.high * b.low);
            return Split::new(mul_basic.high, mul_basic.low + mul_error);
        }

        /** Compute the reciprocal of the instance.
         * @return reciprocal of the instance
         */
        pub fn  reciprocal(&self) -> Split  {
             let approximate_inv: f64 = 1.0 / self.full;
             let split_inv: Split = Split::new(approximate_inv);
            // if 1.0/d were computed perfectly, remultiplying it by d should give 1.0
            // we want to estimate the error so we can fix the low order bits of approximateInvLow
            // beware the following expressions must NOT be simplified, they rely on floating point arithmetic properties
             let product: Split = self.multiply(split_inv);
             let error: f64 = (product.high - 1) + product.low;
            // better accuracy estimate of reciprocal
            return  if Double.is_na_n(error) { split_inv } else { Split::new(split_inv.high, split_inv.low - error / self.full) };
        }

        /** Computes this^e.
         * @param e exponent (beware, here it MUST be > 0; the only exclusion is Long.MIN_VALUE)
         * @return d^e, split in high and low bits
         * @since 3.6
         */
        fn  pow(&self,  e: i64) -> Split  {
            // prepare result
             let mut result: Split = Split::new(1);
            // d^(2p)
             let mut d2p: Split = Split::new(self.full, self.high, self.low);
             {
                 let mut p: i64 = e;
                while p != 0 {
                    {
                        if (p & 0x1) != 0 {
                            // accurate multiplication result = result * d^(2p) using Veltkamp TwoProduct algorithm
                            result = result.multiply(d2p);
                        }
                        // accurate squaring d^(2(p+1)) = d^(2p) * d^(2p) using Veltkamp TwoProduct algorithm
                        d2p = d2p.multiply(d2p);
                    }
                    p >>= /* >>>= */ 1;
                 }
             }

            if Double.is_na_n(result.full) {
                if Double.is_na_n(self.full) {
                    return Split::NAN;
                } else {
                    // and the low order bits became NaN (because infinity - infinity = NaN)
                    if FastMath.abs(self.full) < 1 {
                        return Split::new(FastMath.copy_sign(0.0, self.full), 0.0);
                    } else if self.full < 0 && (e & 0x1) == 1 {
                        return Split::NEGATIVE_INFINITY;
                    } else {
                        return Split::POSITIVE_INFINITY;
                    }
                }
            } else {
                return result;
            }
        }
    }


    


   

    

    

    

    

    

  

  

  
    /**
     * Computes the remainder as prescribed by the IEEE 754 standard.
     * The remainder value is mathematically equal to {@code x - y*n}
     * where {@code n} is the mathematical integer closest to the exact mathematical value
     * of the quotient {@code x/y}.
     * If two mathematical integers are equally close to {@code x/y} then
     * {@code n} is the integer that is even.
     * <p>
     * <ul>
     * <li>If either operand is NaN, the result is NaN.</li>
     * <li>If the result is not NaN, the sign of the result equals the sign of the dividend.</li>
     * <li>If the dividend is an infinity, or the divisor is a zero, or both, the result is NaN.</li>
     * <li>If the dividend is finite and the divisor is an infinity, the result equals the dividend.</li>
     * <li>If the dividend is a zero and the divisor is finite, the result equals the dividend.</li>
     * </ul>
     * <p><b>Note:</b> this implementation currently delegates to {@link StrictMath#IEEEremainder}
     * @param dividend the number to be divided
     * @param divisor the number by which to divide
     * @return the remainder, rounded
     */
    pub fn  IEEEremainder( dividend: f64,  divisor: f64) -> f64  {
        // TODO provide our own implementation
        return StrictMath.IEEEremainder(dividend, divisor);
    }

    /** Convert a long to interger, detecting overflows
     * @param n number to convert to int
     * @return integer with same valie as n if no overflows occur
     * @exception MathArithmeticException if n cannot fit into an int
     * @since 3.4
     */
    pub fn  to_int_exact( n: i64) -> /*  throws MathArithmeticException */Result<i32>   {
        if n < Integer::MIN_VALUE || n > Integer::MAX_VALUE {
            throw MathArithmeticException::new(LocalizedFormats::OVERFLOW);
        }
        return n as i32;
    }

    /** Increment a number, detecting overflows.
     * @param n number to increment
     * @return n+1 if no overflows occur
     * @exception MathArithmeticException if an overflow occurs
     * @since 3.4
     */
    pub fn  increment_exact( n: i32) -> /*  throws MathArithmeticException */Result<i32>   {
        if n == Integer::MAX_VALUE {
            throw MathArithmeticException::new(LocalizedFormats::OVERFLOW_IN_ADDITION, n, 1);
        }
        return n + 1;
    }

    /** Increment a number, detecting overflows.
     * @param n number to increment
     * @return n+1 if no overflows occur
     * @exception MathArithmeticException if an overflow occurs
     * @since 3.4
     */
    pub fn  increment_exact( n: i64) -> /*  throws MathArithmeticException */Result<i64>   {
        if n == Long::MAX_VALUE {
            throw MathArithmeticException::new(LocalizedFormats::OVERFLOW_IN_ADDITION, n, 1);
        }
        return n + 1;
    }

    /** Decrement a number, detecting overflows.
     * @param n number to decrement
     * @return n-1 if no overflows occur
     * @exception MathArithmeticException if an overflow occurs
     * @since 3.4
     */
    pub fn  decrement_exact( n: i32) -> /*  throws MathArithmeticException */Result<i32>   {
        if n == Integer::MIN_VALUE {
            throw MathArithmeticException::new(LocalizedFormats::OVERFLOW_IN_SUBTRACTION, n, 1);
        }
        return n - 1;
    }

    /** Decrement a number, detecting overflows.
     * @param n number to decrement
     * @return n-1 if no overflows occur
     * @exception MathArithmeticException if an overflow occurs
     * @since 3.4
     */
    pub fn  decrement_exact( n: i64) -> /*  throws MathArithmeticException */Result<i64>   {
        if n == Long::MIN_VALUE {
            throw MathArithmeticException::new(LocalizedFormats::OVERFLOW_IN_SUBTRACTION, n, 1);
        }
        return n - 1;
    }

    /** Add two numbers, detecting overflows.
     * @param a first number to add
     * @param b second number to add
     * @return a+b if no overflows occur
     * @exception MathArithmeticException if an overflow occurs
     * @since 3.4
     */
    pub fn  add_exact( a: i32,  b: i32) -> /*  throws MathArithmeticException */Result<i32>   {
        // compute sum
         let sum: i32 = a + b;
        // check for overflow
        if (a ^ b) >= 0 && (sum ^ b) < 0 {
            throw MathArithmeticException::new(LocalizedFormats::OVERFLOW_IN_ADDITION, a, b);
        }
        return sum;
    }

    /** Add two numbers, detecting overflows.
     * @param a first number to add
     * @param b second number to add
     * @return a+b if no overflows occur
     * @exception MathArithmeticException if an overflow occurs
     * @since 3.4
     */
    pub fn  add_exact( a: i64,  b: i64) -> /*  throws MathArithmeticException */Result<i64>   {
        // compute sum
         let sum: i64 = a + b;
        // check for overflow
        if (a ^ b) >= 0 && (sum ^ b) < 0 {
            throw MathArithmeticException::new(LocalizedFormats::OVERFLOW_IN_ADDITION, a, b);
        }
        return sum;
    }

    /** Subtract two numbers, detecting overflows.
     * @param a first number
     * @param b second number to subtract from a
     * @return a-b if no overflows occur
     * @exception MathArithmeticException if an overflow occurs
     * @since 3.4
     */
    pub fn  subtract_exact( a: i32,  b: i32) -> i32  {
        // compute subtraction
         let sub: i32 = a - b;
        // check for overflow
        if (a ^ b) < 0 && (sub ^ b) >= 0 {
            throw MathArithmeticException::new(LocalizedFormats::OVERFLOW_IN_SUBTRACTION, a, b);
        }
        return sub;
    }

    /** Subtract two numbers, detecting overflows.
     * @param a first number
     * @param b second number to subtract from a
     * @return a-b if no overflows occur
     * @exception MathArithmeticException if an overflow occurs
     * @since 3.4
     */
    pub fn  subtract_exact( a: i64,  b: i64) -> i64  {
        // compute subtraction
         let sub: i64 = a - b;
        // check for overflow
        if (a ^ b) < 0 && (sub ^ b) >= 0 {
            throw MathArithmeticException::new(LocalizedFormats::OVERFLOW_IN_SUBTRACTION, a, b);
        }
        return sub;
    }

    /** Multiply two numbers, detecting overflows.
     * @param a first number to multiply
     * @param b second number to multiply
     * @return a*b if no overflows occur
     * @exception MathArithmeticException if an overflow occurs
     * @since 3.4
     */
    pub fn  multiply_exact( a: i32,  b: i32) -> i32  {
        if ((b > 0) && (a > Integer::MAX_VALUE / b || a < Integer::MIN_VALUE / b)) || ((b < -1) && (a > Integer::MIN_VALUE / b || a < Integer::MAX_VALUE / b)) || ((b == -1) && (a == Integer::MIN_VALUE)) {
            throw MathArithmeticException::new(LocalizedFormats::OVERFLOW_IN_MULTIPLICATION, a, b);
        }
        return a * b;
    }

    /** Multiply two numbers, detecting overflows.
     * @param a first number to multiply
     * @param b second number to multiply
     * @return a*b if no overflows occur
     * @exception MathArithmeticException if an overflow occurs
     * @since 3.4
     */
    pub fn  multiply_exact( a: i64,  b: i64) -> i64  {
        if ((b > 0) && (a > Long::MAX_VALUE / b || a < Long::MIN_VALUE / b)) || ((b < -1) && (a > Long::MIN_VALUE / b || a < Long::MAX_VALUE / b)) || ((b == -1) && (a == Long::MIN_VALUE)) {
            throw MathArithmeticException::new(LocalizedFormats::OVERFLOW_IN_MULTIPLICATION, a, b);
        }
        return a * b;
    }

    /** Finds q such that a = q b + r with 0 <= r < b if b > 0 and b < r <= 0 if b < 0.
     * <p>
     * This methods returns the same value as integer division when
     * a and b are same signs, but returns a different value when
     * they are opposite (i.e. q is negative).
     * </p>
     * @param a dividend
     * @param b divisor
     * @return q such that a = q b + r with 0 <= r < b if b > 0 and b < r <= 0 if b < 0
     * @exception MathArithmeticException if b == 0
     * @see #floorMod(int, int)
     * @since 3.4
     */
    pub fn  floor_div( a: i32,  b: i32) -> /*  throws MathArithmeticException */Result<i32>   {
        if b == 0 {
            throw MathArithmeticException::new(LocalizedFormats::ZERO_DENOMINATOR);
        }
         let m: i32 = a % b;
        if (a ^ b) >= 0 || m == 0 {
            // a an b have same sign, or division is exact
            return a / b;
        } else {
            // a and b have opposite signs and division is not exact
            return (a / b) - 1;
        }
    }

    /** Finds q such that a = q b + r with 0 <= r < b if b > 0 and b < r <= 0 if b < 0.
     * <p>
     * This methods returns the same value as integer division when
     * a and b are same signs, but returns a different value when
     * they are opposite (i.e. q is negative).
     * </p>
     * @param a dividend
     * @param b divisor
     * @return q such that a = q b + r with 0 <= r < b if b > 0 and b < r <= 0 if b < 0
     * @exception MathArithmeticException if b == 0
     * @see #floorMod(long, long)
     * @since 3.4
     */
    pub fn  floor_div( a: i64,  b: i64) -> /*  throws MathArithmeticException */Result<i64>   {
        if b == 0 {
            throw MathArithmeticException::new(LocalizedFormats::ZERO_DENOMINATOR);
        }
         let m: i64 = a % b;
        if (a ^ b) >= 0 || m == 0 {
            // a an b have same sign, or division is exact
            return a / b;
        } else {
            // a and b have opposite signs and division is not exact
            return (a / b) - 1;
        }
    }

    /** Finds r such that a = q b + r with 0 <= r < b if b > 0 and b < r <= 0 if b < 0.
     * <p>
     * This methods returns the same value as integer modulo when
     * a and b are same signs, but returns a different value when
     * they are opposite (i.e. q is negative).
     * </p>
     * @param a dividend
     * @param b divisor
     * @return r such that a = q b + r with 0 <= r < b if b > 0 and b < r <= 0 if b < 0
     * @exception MathArithmeticException if b == 0
     * @see #floorDiv(int, int)
     * @since 3.4
     */
    pub fn  floor_mod( a: i32,  b: i32) -> /*  throws MathArithmeticException */Result<i32>   {
        if b == 0 {
            throw MathArithmeticException::new(LocalizedFormats::ZERO_DENOMINATOR);
        }
         let m: i32 = a % b;
        if (a ^ b) >= 0 || m == 0 {
            // a an b have same sign, or division is exact
            return m;
        } else {
            // a and b have opposite signs and division is not exact
            return b + m;
        }
    }

    /** Finds r such that a = q b + r with 0 <= r < b if b > 0 and b < r <= 0 if b < 0.
     * <p>
     * This methods returns the same value as integer modulo when
     * a and b are same signs, but returns a different value when
     * they are opposite (i.e. q is negative).
     * </p>
     * @param a dividend
     * @param b divisor
     * @return r such that a = q b + r with 0 <= r < b if b > 0 and b < r <= 0 if b < 0
     * @exception MathArithmeticException if b == 0
     * @see #floorDiv(long, long)
     * @since 3.4
     */
    pub fn  floor_mod( a: i64,  b: i64) -> i64  {
        if b == 0 {
            throw MathArithmeticException::new(LocalizedFormats::ZERO_DENOMINATOR);
        }
         let m: i64 = a % b;
        if (a ^ b) >= 0 || m == 0 {
            // a an b have same sign, or division is exact
            return m;
        } else {
            // a and b have opposite signs and division is not exact
            return b + m;
        }
    }



   
}


                
                