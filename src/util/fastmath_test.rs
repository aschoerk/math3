#[cfg(test)] 
mod tests {
	
	use util::fastmath::F64;
	use util::fastmath::F32;
	use util::fastmath::I64;
	use util::fastmath::I32;
	use util::fastmath;
	use util::precision;
	use util::rsutils::*;
	use std::f64;
	use std::f32;
	use std::i8;
	use std::i16;
	use std::i32;
	use std::i64;
	use random::bits_stream_generator::*;
	use random::mersenne_twister::*;
	use dfp::dfp_field::*;
	use dfp::dfp_math::*;
	use dfp::dfp::*;
	#[macro_use]
	use assert;
	
	 const MAX_ERROR_ULP: f64 = 0.51;

     const NUMBER_OF_TRIALS: i32 = 1000;

    fn create_generator() -> MersenneTwister {
    	// Box::new(MersenneTwister::new_i64(&6176597458463500194))
    	MersenneTwister::new_i64(&6176597458463500194)
    }
         	
	
	#[test]
	fn f64_abs() {
		assert!(F64::abs(-1.0) == 1.0);
		assert!(F64::abs(-1.0e7) == 1.0e7);
		assert!(F64::abs(1.0) == 1.0);
		assert!(F64::abs(1.0e7) == 1.0e7);
		assert!(F64::abs(-1.0e-7) == 1.0e-7);
		assert!(F64::abs(-0.0) == 0.0);
		assert!(F64::abs(-1.011) == 1.011);		
	}
	
	#[test]
	fn f32_abs() {
		assert!(F32::abs(-1.0) == 1.0);
		assert!(F32::abs(-1.0e7) == 1.0e7);
		assert!(F32::abs(1.0) == 1.0);
		assert!(F32::abs(1.0e7) == 1.0e7);
		assert!(F32::abs(-1.0e-7) == 1.0e-7);
		assert!(F32::abs(-0.0) == 0.0);
		assert!(F32::abs(-1.011) == 1.011);		
	}
	
	    #[test]
    pub fn  test_min_max_double()   {
         let pairs = [[-50.0, 50.0, ]
        , [f64::INFINITY, 1.0, ]
        , [f64::NEG_INFINITY, 1.0, ]
        , [f64::NAN, 1.0, ]
        , [f64::INFINITY, 0.0, ]
        , [f64::NEG_INFINITY, 0.0, ]
        , [f64::NAN, 0.0, ]
        , [f64::NAN, f64::NEG_INFINITY, ]
        , [f64::NAN, f64::INFINITY, ]
        , [*precision::SAFE_MIN, *precision::EPSILON, ]
        , ]
        ;
	    for  pair in pairs.iter() {
            assert_within_delta!(f64::min(pair[0], pair[1]), F64::min(pair[0], pair[1]), *precision::EPSILON);
            assert_within_delta!(f64::min(pair[1], pair[0]), F64::min(pair[1], pair[0]), *precision::EPSILON);
            assert_within_delta!(f64::max(pair[0], pair[1]), F64::max(pair[0], pair[1]), *precision::EPSILON);
            assert_within_delta!(f64::max(pair[1], pair[0]), F64::max(pair[1], pair[0]), *precision::EPSILON);
        }
    }
    
        #[test]
    pub fn  test_min_max_float()   {
         let pairs = [[-50.0, 50.0, ]
        , [f32::INFINITY, 1.0, ]
        , [f32::NEG_INFINITY, 1.0, ]
        , [f32::NAN, 1.0, ]
        , [f32::INFINITY, 0.0, ]
        , [f32::NEG_INFINITY, 0.0, ]
        , [f32::NAN, 0.0, ]
        , [f32::NAN, f32::NEG_INFINITY, ]
        , [f32::NAN, f32::INFINITY, ]
        , [*precision::SAFE_MIN as f32, *precision::EPSILON as f32, ]
        , ]
        ;
	    for  pair in pairs.iter() {
            assert_within_delta!(f32::min(pair[0], pair[1]), F32::min(pair[0], pair[1]), *precision::EPSILON as f32);
            assert_within_delta!(f32::min(pair[1], pair[0]), F32::min(pair[1], pair[0]), *precision::EPSILON as f32);
            assert_within_delta!(f32::max(pair[0], pair[1]), F32::max(pair[0], pair[1]), *precision::EPSILON as f32);
            assert_within_delta!(f32::max(pair[1], pair[0]), F32::max(pair[1], pair[0]), *precision::EPSILON as f32);
        }
    }

    #[test]
    pub fn  test_constants()   {
        assert_within_delta!(f64::consts::PI, fastmath::PI, 1.0e-20);
        assert_within_delta!(f64::consts::E, fastmath::E, 1.0e-20);
    }

    #[test]
    pub fn  test_atan2()   {
         let y1: f64 = 1.2713504628280707e10;
         let x1: f64 = -5.674940885228782e-10;
        assert_within_delta!(f64::atan2(y1, x1), F64::atan2(y1, x1), 2.0 * *precision::EPSILON);
         let y2: f64 = 0.0;
         let x2: f64 = f64::INFINITY;
        assert_within_delta!(f64::atan2(y2, x2), F64::atan2(y2, x2), *precision::SAFE_MIN);
    }
	
    #[test]
    pub fn  test_hyperbolic()   {
         let mut max_err: f64 = 0.0;
         {
             let mut x: f64 = -30.0;
            while x < 30.0 {
                {
                     let tst: f64 = F64::sinh(x);
                     let reference: f64 = f64::sinh(x);
                    max_err = F64::max(max_err, F64::abs(reference - tst) / F64::ulp(reference));
                }
                x += 0.001;
             }
         }

        assert_within_delta!(0.0, max_err, 2.0);
        max_err = 0.0;
         {
             let mut x: f64 = -30.0;
            while x < 30.0 {
                {
                     let tst: f64 = F64::cosh(x);
                     let reference: f64 = f64::cosh(x);
                    max_err = F64::max(max_err, F64::abs(reference - tst) / F64::ulp(reference));
                }
                x += 0.001;
             }
         }

        assert_within_delta!(0.0, max_err, 2.0);
        max_err = 0.0;
         {
             let mut x: f64 = -0.5;
            while x < 0.5 {
                {
                     let tst: f64 = F64::tanh(x);
                     let reference: f64 = f64::tanh(x);
                    max_err = F64::max(max_err, F64::abs(reference - tst) / F64::ulp(reference));
                }
                x += 0.001;
             }
         }

        assert_within_delta!(0.0, max_err, 4.0);
    }
    
    #[test]
    pub fn  test_math904()   {
         let x: f64 = -1.0;
         let y: f64 = (5.0 + 1.0e-15) * 1.0e15;
         let val = F64::pow(x, y);
         let valm = F64::pow(x, -y);
        assert_within_delta!(x.powf(y), val, 0.0);
        assert_within_delta!(x.powf(-y), valm, 0.0);
    }

    #[test]
    pub fn  test_math905_large_positive()   {
         let start: f64 = f64::MAX.ln();
         let end_t: f64 = f64::sqrt(2.0) * f64::sqrt(f64::MAX);
         let end: f64 = 2.0 * end_t.ln();
         let mut max_err: f64 = 0.0;
         {
             let mut x: f64 = start;
            while x < end {
                {
                     let tst: f64 = F64::cosh(x);
                     let reference: f64 = f64::cosh(x);
                    max_err = F64::max(max_err, F64::abs(reference - tst) / F64::ulp(reference));
                }
                x += 1.0e-3;
             }
         }

        assert_within_delta!(0.0, max_err, 3.0);
         {
             let mut x: f64 = start;
            while x < end {
                {
                     let tst: f64 = F64::sinh(x);
                     let reference: f64 = f64::sinh(x);
                    max_err = F64::max(max_err, F64::abs(reference - tst) / F64::ulp(reference));
                }
                x += 1.0e-3;
             }
         }

        assert_within_delta!(0.0, max_err, 3.0);
    }

    #[test]
    pub fn  test_math905_large_negative()   {
         let start: f64 = -f64::MAX.ln();
         let end_t: f64 = f64::sqrt(2.0) * f64::sqrt(f64::MAX);
         let end: f64 = -2.0 * end_t.ln();
         let mut max_err: f64 = 0.0;
         {
             let mut x: f64 = start;
            while x > end {
                {
                     let tst: f64 = F64::cosh(x);
                     let reference: f64 = f64::cosh(x);
                    max_err = F64::max(max_err, F64::abs(reference - tst) / F64::ulp(reference));
                }
                x -= 1.0e-3;
             }
         }

        assert_within_delta!(0.0, max_err, 3.0);
         {
             let mut x: f64 = start;
            while x > end {
                {
                     let tst: f64 = F64::sinh(x);
                     let reference: f64 = f64::sinh(x);
                    max_err = F64::max(max_err, F64::abs(reference - tst) / F64::ulp(reference));
                }
                x -= 1.0e-3;
             }
         }

        assert_within_delta!(0.0, max_err, 3.0);
    }

	
	 #[test]
    pub fn  test_math1269()   {
         let arg: f64 = 709.8125;
         let v_m: f64 = arg.exp();
         let v_f_m: f64 = F64::exp(arg);
        assert!(precision::F64::equals_including_nan(v_m, v_f_m),"exp( {}) is {} instead of {}",arg,v_f_m,v_m);
    }    
       
     #[test]
    pub fn  test_hyperbolic_inverses()   {
         let mut max_err: f64 = 0.0;
         {
             let mut x: f64 = -30.0;
            while x < 30.0 {
                {
                	let a = F64::sinh(F64::asinh(x));
                	let b = F64::ulp(x);
                    max_err = F64::max(max_err, F64::abs(x - F64::sinh(F64::asinh(x))) / (2.0 * F64::ulp(x)));
                    if max_err > 3.0 {
                    	println!("maxerr: {}, a: {}, b: {}",max_err, a, b);
                    }
                }
                x += 0.01;
             }
         }

        assert_within_delta!(0.0, max_err, 3.0);
        max_err = 0.0;
         {
             let mut x: f64 = 1.0;
            while x < 30.0 {
                {
                    max_err = F64::max(max_err, F64::abs(x - F64::cosh(F64::acosh(x))) / (2.0 * F64::ulp(x)));
                }
                x += 0.01;
             }
         }

        assert_within_delta!(0.0, max_err, 2.0);
        max_err = 0.0;
         {
             let mut x: f64 = -1.0 + *precision::EPSILON;
            while x < 1.0 - *precision::EPSILON {
                {
                    max_err = F64::max(max_err, F64::abs(x - F64::tanh(F64::atanh(x))) / (2.0 * F64::ulp(x)));
                }
                x += 0.0001;
             }
         }

        assert_within_delta!(0.0, max_err, 2.0);
    }

	
	 #[test]
    pub fn  test_log_accuracy()   {
    	let mut generator = create_generator(); // MersenneTwister::new_i64(&6176597458463500194);
    	let field = DfpField::new(40);
         let mut maxerrulp: f64 = 0.0;
         {
         	
             let mut i: i32 = 0;
            while i < NUMBER_OF_TRIALS {
                {
                     let x: f64 = f64::exp(generator.next_double() * 1416.0 - 708.0) * generator.next_double();
                    // double x = generator.nextDouble()*2.0;
                     let tst: f64 = F64::log(x);
                     let reference: f64 = DfpMath::log(&field.new_dfp_f64(x)).to_double();
                     let err: f64 = (tst - reference) / reference;
	                    if err != 0.0 {
                         let ulp: f64 = f64::abs(reference - long_bits_to_double(&(double_to_raw_long_bits(&reference) ^ 1)));
                         let errulp: f64 = field.new_dfp_f64(tst).subtract(&DfpMath::log(&field.new_dfp_f64(x))).divide(&field.new_dfp_f64(ulp)).unwrap().to_double();
                        //                System.out.println(x + "\t" + tst + "\t" + reference + "\t" + err + "\t" + errulp);
                        maxerrulp = f64::max(maxerrulp, f64::abs(errulp));
                    }
                }
                i += 1;
             }
         }
        assert!(maxerrulp < MAX_ERROR_ULP,"log() had {} errors in excess of {} ULP",maxerrulp, MAX_ERROR_ULP);
    }

	    
     #[test]
    pub fn  test_log10_accuracy()   {
         let mut generator = create_generator(); // MersenneTwister::new_i64(&6176597458463500194);
    	let field = DfpField::new(40);
        let mut maxerrulp: f64 = 0.0;
         {
             let mut i: i32 = 0;
            while i < NUMBER_OF_TRIALS {
                {
                     let x: f64 = f64::exp(generator.next_double() * 1416.0 - 708.0) * generator.next_double();
                    // double x = generator.nextDouble()*2.0;
                     let tst: f64 = F64::log10(x);
                     let reference: f64 = DfpMath::log(&field.new_dfp_f64(x)).divide(&DfpMath::log(&field.new_dfp_str("10").unwrap())).unwrap().to_double();
                     let err: f64 = (tst - reference) / reference;
                    if err != 0.0 {
                         let ulp: f64 = f64::abs(reference - long_bits_to_double(&(double_to_raw_long_bits(&reference) ^ 1)));
                         let errulp: f64 = field
	                         .new_dfp_f64(tst)
	                         .subtract(&DfpMath::log(
	                         		&field.new_dfp_f64(x)).divide(&DfpMath::log(&field.new_dfp_str("10").unwrap())).unwrap()
	                                  ).divide(&field.new_dfp_f64(ulp)).unwrap().to_double();
                        //                System.out.println(x + "\t" + tst + "\t" + reference + "\t" + err + "\t" + errulp);
                        maxerrulp = f64::max(maxerrulp, f64::abs(errulp));
                    }
                }
                i += 1;
             }
         }

         assert!(maxerrulp < MAX_ERROR_ULP,"log10() had {} errors in excess of {} ULP",maxerrulp, MAX_ERROR_ULP);
    }
   
     #[test]
    pub fn  test_log1p_accuracy()   {
         let mut generator = create_generator(); // MersenneTwister::new_i64(&6176597458463500194);
    	let field = DfpField::new(40);
        let mut maxerrulp: f64 = 0.0;
         {
             let mut i: i32 = 0;
            while i < NUMBER_OF_TRIALS {
                {
                     let x: f64 = f64::exp(generator.next_double() * 10.0 - 5.0) * generator.next_double();
                    // double x = generator.nextDouble()*2.0;
                     let xr = double_to_raw_long_bits(&x);
                     let tst: f64 = F64::log1p(x);
                     let tstr = double_to_raw_long_bits(&tst);
                     let reference: f64 = DfpMath::log(&field.new_dfp_f64(x).add(&field.get_one())).to_double();
                     let referencer = double_to_raw_long_bits(&reference);
                     let err: f64 = (tst - reference) / reference;
                    if err != 0.0 {
                         let ulp: f64 = f64::abs(reference - long_bits_to_double(&(double_to_raw_long_bits(&reference) ^ 1)));
                         let errulp: f64 = field.new_dfp_f64(tst)
                         .subtract(
                         	 &DfpMath::log(
                         		&field.new_dfp_f64(x).add(&field.get_one())
	                         )
                         )
                         .divide(&field.new_dfp_f64(ulp)).unwrap().to_double();
                        //                System.out.println(x + "\t" + tst + "\t" + reference + "\t" + err + "\t" + errulp);
                        maxerrulp = f64::max(maxerrulp, f64::abs(errulp));
                    }
                }
                i += 1;
             }
         }

        assert!(maxerrulp < MAX_ERROR_ULP,"log1p() had {} errors in excess of {} ULP",maxerrulp, MAX_ERROR_ULP);
    }

    #[test]
    pub fn  test_log1p_special_cases()   {
        assert!(f64::is_infinite(F64::log1p(-1.0)),"Logp of -1.0 should be -Inf");
    }
	
	
    #[test]
    pub fn  test_log_special_cases()   {
    	let my_min = long_bits_to_double(&1);
        assert_within_delta!( f64::NEG_INFINITY, F64::log(0.0), 1.0,"Log of zero should be -Inf");
        assert_within_delta!( f64::NEG_INFINITY, F64::log(-0.0), 1.0,"Log of -zero should be -Inf");
        assert!( f64::is_nan(F64::log(f64::NAN)),"Log of NaN should be NaN");
        assert!( f64::is_nan(F64::log(-1.0)),"Log of negative number should be NaN");
        assert_within_delta!( -744.4400719213812, F64::log(my_min), *precision::EPSILON,"Log of Double.MIN_VALUE should be -744.4400719213812");
        assert_within_delta!( f64::INFINITY, F64::log(f64::INFINITY), 1.0,"Log of infinity should be infinity");
    }
    

    #[test]
    pub fn  test_exp_special_cases()   {
    	let my_min = long_bits_to_double(&1);
        // Smallest value that will round up to Double.MIN_VALUE
        assert_within_delta!(my_min, F64::exp(-745.1332191019411), *precision::EPSILON);
        assert_within_delta!( 0.0, F64::exp(-745.1332191019412), *precision::EPSILON,"exp(-745.1332191019412) should be 0.0");
        assert!( f64::is_nan(F64::exp(f64::NAN)),"exp of NaN should be NaN");
        assert_within_delta!( f64::INFINITY, F64::exp(f64::INFINITY), 1.0,"exp of infinity should be infinity");
        assert_within_delta!( 0.0, F64::exp(f64::NEG_INFINITY), *precision::EPSILON,"exp of -infinity should be 0.0");
        assert_within_delta!( f64::consts::E, F64::exp(1.0), *precision::EPSILON,"exp(1) should be f64::consts::E");
    }


    #[test]
    pub fn  test_pow_special_cases()   {
         const EXACT: f64 = -1.0;
        assert_within_delta!( 1.0, F64::pow(-1.0, 0.0), *precision::EPSILON,"pow(-1, 0) should be 1.0");
        assert_within_delta!( 1.0, F64::pow(-1.0, -0.0), *precision::EPSILON,"pow(-1, -0) should be 1.0");
        assert_within_delta!( fastmath::PI, F64::pow(fastmath::PI, 1.0), *precision::EPSILON,"pow(PI, 1.0) should be PI");
        assert_within_delta!( -fastmath::PI, F64::pow(-fastmath::PI, 1.0), *precision::EPSILON,"pow(-PI, 1.0) should be -PI");
        assert!( f64::is_nan(F64::pow(f64::consts::PI, f64::NAN)),"pow(PI, NaN) should be NaN");
        assert!( f64::is_nan(F64::pow(f64::NAN, f64::consts::PI)),"pow(NaN, PI) should be NaN");
        assert_within_delta!( f64::INFINITY, F64::pow(2.0, f64::INFINITY), 1.0,"pow(2.0, Infinity) should be Infinity");
        assert_within_delta!( f64::INFINITY, F64::pow(0.5, f64::NEG_INFINITY), 1.0,"pow(0.5, -Infinity) should be Infinity");
        assert_within_delta!( 0.0, F64::pow(0.5, f64::INFINITY), *precision::EPSILON,"pow(0.5, Infinity) should be 0.0");
        assert_within_delta!( 0.0, F64::pow(2.0, f64::NEG_INFINITY), *precision::EPSILON,"pow(2.0, -Infinity) should be 0.0");
        assert_within_delta!( 0.0, F64::pow(0.0, 0.5), *precision::EPSILON,"pow(0.0, 0.5) should be 0.0");
        assert_within_delta!( 0.0, F64::pow(f64::INFINITY, -0.5), *precision::EPSILON,"pow(Infinity, -0.5) should be 0.0");
        assert_within_delta!( f64::INFINITY, F64::pow(0.0, -0.5), 1.0,"pow(0.0, -0.5) should be Inf");
        assert_within_delta!( f64::INFINITY, F64::pow(f64::INFINITY, 0.5), 1.0,"pow(Inf, 0.5) should be Inf");
        assert_within_delta!( f64::NEG_INFINITY, F64::pow(-0.0, -3.0), 1.0,"pow(-0.0, -3.0) should be -Inf");
        assert_within_delta!( 0.0, F64::pow(-0.0, f64::INFINITY), *precision::EPSILON,"pow(-0.0, Infinity) should be 0.0");
        assert!( f64::is_nan(F64::pow(-0.0, f64::NAN)),"pow(-0.0, NaN) should be NaN");
        let my_min = long_bits_to_double(&1);
        assert_within_delta!( f64::INFINITY, F64::pow(-0.0, -my_min), 1.0,"pow(-0.0, -tiny) should be Infinity");
        assert_within_delta!( f64::INFINITY, F64::pow(-0.0, -f64::MAX), 1.0,"pow(-0.0, -huge) should be Infinity");
        assert_within_delta!( f64::NEG_INFINITY, F64::pow(f64::NEG_INFINITY, 3.0), 1.0,"pow(-Inf, 3.0) should be -Inf");
        assert_within_delta!( -0.0, F64::pow(f64::NEG_INFINITY, -3.0), EXACT,"pow(-Inf, -3.0) should be -0.0");
        assert_within_delta!( f64::INFINITY, F64::pow(-0.0, -3.5), 1.0,"pow(-0.0, -3.5) should be Inf");
        assert_within_delta!( f64::INFINITY, F64::pow(f64::INFINITY, 3.5), 1.0,"pow(Inf, 3.5) should be Inf");
        assert_within_delta!( -8.0, F64::pow(-2.0, 3.0), *precision::EPSILON,"pow(-2.0, 3.0) should be -8.0");
        assert!( f64::is_nan(F64::pow(-2.0, 3.5)),"pow(-2.0, 3.5) should be NaN");
        assert!( f64::is_nan(F64::pow(f64::NAN, f64::NEG_INFINITY)),"pow(NaN, -Infinity) should be NaN");      
        assert_within_delta!( 1.0, F64::pow(f64::NAN, 0.0), *precision::EPSILON,"pow(NaN, 0.0) should be 1.0");
        assert_within_delta!( 0.0, F64::pow(f64::NEG_INFINITY, f64::NEG_INFINITY), *precision::EPSILON,"pow(-Infinity, -Infinity) should be 0.0");
        assert_within_delta!( 0.0, F64::pow(-f64::MAX, -f64::MAX), *precision::EPSILON,"pow(-huge, -huge) should be 0.0");
        assert!( f64::is_infinite(F64::pow(-f64::MAX, f64::MAX)),"pow(-huge,  huge) should be +Inf");
        assert!( f64::is_nan(F64::pow(f64::NAN, f64::NEG_INFINITY)),"pow(NaN, -Infinity) should be NaN");
        assert_within_delta!( 1.0, F64::pow(f64::NAN, -0.0), *precision::EPSILON,"pow(NaN, -0.0) should be 1.0");
        assert_within_delta!( 0.0, F64::pow(f64::NEG_INFINITY, f64::NEG_INFINITY), *precision::EPSILON,"pow(-Infinity, -Infinity) should be 0.0");
        assert_within_delta!( 0.0, F64::pow(-f64::MAX, -f64::MAX), *precision::EPSILON,"pow(-huge, -huge) should be 0.0");
        assert_within_delta!( f64::INFINITY, F64::pow(-f64::MAX, f64::MAX), 1.0,"pow(-huge,  huge) should be +Inf");
        // Added tests for a 100% coverage
        assert!( f64::is_nan(F64::pow(f64::INFINITY, f64::NAN)),"pow(+Inf, NaN) should be NaN");
        assert!( f64::is_nan(F64::pow(1.0, f64::INFINITY)),"pow(1.0, +Inf) should be NaN");
        assert!( f64::is_nan(F64::pow(f64::NEG_INFINITY, f64::NAN)),"pow(-Inf, NaN) should be NaN");
        assert_within_delta!( -0.0, F64::pow(f64::NEG_INFINITY, -1.0), EXACT,"pow(-Inf, -1.0) should be -0.0");
        assert_within_delta!( 0.0, F64::pow(f64::NEG_INFINITY, -2.0), EXACT,"pow(-Inf, -2.0) should be 0.0");
        assert_within_delta!( f64::NEG_INFINITY, F64::pow(f64::NEG_INFINITY, 1.0), 1.0,"pow(-Inf, 1.0) should be -Inf");
        assert_within_delta!( f64::INFINITY, F64::pow(f64::NEG_INFINITY, 2.0), 1.0,"pow(-Inf, 2.0) should be +Inf");
        assert!( f64::is_nan(F64::pow(1.0, f64::NEG_INFINITY)),"pow(1.0, -Inf) should be NaN");
        assert_within_delta!( -0.0, F64::pow(-0.0, 1.0), EXACT,"pow(-0.0, 1.0) should be -0.0");
        assert_within_delta!( 0.0, F64::pow(0.0, 1.0), EXACT,"pow(0.0, 1.0) should be 0.0");
        assert_within_delta!( 0.0, F64::pow(0.0, f64::INFINITY), EXACT,"pow(0.0, +Inf) should be 0.0");
        assert_within_delta!( 0.0, F64::pow(-0.0, 6.0), EXACT,"pow(-0.0, even) should be 0.0");
        assert_within_delta!( -0.0, F64::pow(-0.0, 13.0), EXACT,"pow(-0.0, odd) should be -0.0");
        assert_within_delta!( f64::INFINITY, F64::pow(-0.0, -6.0), EXACT,"pow(-0.0, -even) should be +Inf");
        assert_within_delta!( f64::NEG_INFINITY, F64::pow(-0.0, -13.0), EXACT,"pow(-0.0, -odd) should be -Inf");
        assert_within_delta!( 16.0, F64::pow(-2.0, 4.0), EXACT,"pow(-2.0, 4.0) should be 16.0");        
        assert!((-2.0 as f64).powf(4.5).is_nan(),"pow(-2.0, 4.5) should be NaN");
        assert!(F64::pow(-2.0, 4.5).is_nan(),"pow(-2.0, 4.5) should be NaN");
        assert_within_delta!( 1.0, F64::pow(-0.0, -0.0), EXACT,"pow(-0.0, -0.0) should be 1.0");
        assert_within_delta!( 1.0, F64::pow(-0.0, 0.0), EXACT,"pow(-0.0, 0.0) should be 1.0");
        assert_within_delta!( 1.0, F64::pow(0.0, -0.0), EXACT,"pow(0.0, -0.0) should be 1.0");
        assert_within_delta!( 1.0, F64::pow(0.0, 0.0), EXACT,"pow(0.0, 0.0) should be 1.0");
    }

	
	 #[test]
    pub fn  test_pow_all_special_cases()   {
         const EXACT: f64 = -1.0;
         const DOUBLES : [f64; 86] = 
         [f64::NEG_INFINITY, -0.0, 0.0, f64::INFINITY, 
         i64::MIN as f64, i32::MIN as f64, i16::MIN as f64, i8::MIN as f64, -(i64::MIN as f64) as f64, 
         -(i32::MIN as f64) as f64, -(i16::MIN as f64) as f64, -(i8::MIN as f64) as f64, i8::MAX as f64, 
         i16::MAX as f64, i32::MAX as f64, i64::MAX as f64, -i8::MAX as f64, -i16::MAX as f64, -i32::MAX as f64, 
         -i64::MAX as f64, f32::MAX as f64, f64::MAX as f64, f64::MIN as f64, f32::MIN as f64, -f32::MAX as f64, -f32::MAX as f64, 
         -f64::MIN as f64, -f32::MIN as f64, 
         0.5, 0.1, 0.2, 0.8, 1.1, 1.2, 1.5, 1.8, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 
         8.0, 9.0, 1.3, 2.2, 2.5, 2.8, 33.0, 33.1, 33.5, 33.8, 10.0, 300.0, 400.0, 500.0, 
         -0.5, -0.1, -0.2, -0.8, -1.1, -1.2, -1.5, -1.8, -1.0, -2.0, -3.0, -4.0, -5.0, -6.0, 
         -7.0, -8.0, -9.0, -1.3, -2.2, -2.5, -2.8, -33.0, -33.1, -33.5, -33.8, -10.0, -300.0, 
         -400.0, -500.0, ]
        ;
        assert!(F64::pow(f64::NAN,1.0).is_nan());
        // If the second argument is positive or negative zero, then the result is 1.0.
        for  d in DOUBLES.iter() {
            assert_within_delta!(1.0, F64::pow(*d, 0.0), EXACT);
        }
        for  d in DOUBLES.iter() {
            assert_within_delta!(1.0, F64::pow(*d, -0.0), EXACT);
        }
        // If the second argument is 1.0, then the result is the same as the first argument.
        for  d in DOUBLES.iter() {
            assert_within_delta!(*d, F64::pow(*d, 1.0), EXACT);
        }
        // If the second argument is NaN, then the result is NaN.
        for  d in DOUBLES.iter() {
            assert!(F64::pow(*d, f64::NAN).is_nan());
        }
        // If the first argument is NaN and the second argument is nonzero, then the result is NaN.
        for  i in DOUBLES.iter() {
            if *i != 0.0 {
                assert!(F64::pow(f64::NAN, *i).is_nan());
            }
        }
        // the absolute value of the first argument is less than 1 and the second argument is negative infinity, then the result is positive infinity.
        for  d in DOUBLES.iter() {
            if f64::abs(*d) > 1.0 {
                assert_within_delta!(f64::INFINITY, F64::pow(*d, f64::INFINITY), EXACT);
            }
        }
        for  d in DOUBLES.iter() {
            if f64::abs(*d) < 1.0 {
                assert_within_delta!(f64::INFINITY, F64::pow(*d, f64::NEG_INFINITY), EXACT);
            }
        }
        // the absolute value of the first argument is less than 1 and the second argument is positive infinity, then the result is positive zero.
        for  d in DOUBLES.iter() {
            if f64::abs(*d) > 1.0 {
                assert_within_delta!(0.0, F64::pow(*d, f64::NEG_INFINITY), EXACT);
            }
        }
        for  d in DOUBLES.iter() {
            if f64::abs(*d) < 1.0 {
                assert_within_delta!(0.0, F64::pow(*d, f64::INFINITY), EXACT);
            }
        }
        // If the absolute value of the first argument equals 1 and the second argument is infinite, then the result is NaN.
        assert!(F64::pow(1.0, f64::INFINITY).is_nan());
        assert!(F64::pow(1.0, f64::NEG_INFINITY).is_nan());
        assert!(F64::pow(-1.0, f64::INFINITY).is_nan());
        assert!(F64::pow(-1.0, f64::NEG_INFINITY).is_nan());
        // the first argument is positive infinity and the second argument is less than zero, then the result is positive zero.
        for  i in DOUBLES.iter() {
            if *i > 0.0 {
                assert_within_delta!(0.0, F64::pow(0.0, *i), EXACT);
            }
        }
        for  i in DOUBLES.iter() {
            if *i < 0.0 {
                assert_within_delta!(0.0, F64::pow(f64::INFINITY, *i), EXACT);
            }
        }
        // the first argument is positive infinity and the second argument is greater than zero, then the result is positive infinity.
        for  i in DOUBLES.iter() {
            if *i < 0.0 {
                assert_within_delta!(f64::INFINITY, F64::pow(0.0, *i), EXACT);
            }
        }
        for  i in DOUBLES.iter() {
            if *i > 0.0 {
                assert_within_delta!(f64::INFINITY, F64::pow(f64::INFINITY, *i), EXACT);
            }
        }
        // the first argument is negative infinity and the second argument is less than zero but not a finite odd integer, then the result is positive zero.
        for  i in DOUBLES.iter() {
            if *i > 0.0 && ((*i).is_infinite() || *i % 2.0 == 0.0) {
                assert_within_delta!(0.0, F64::pow(-0.0, *i), EXACT);
            }
        }
        for  i in DOUBLES.iter() {
            if *i < 0.0 && ((*i).is_infinite() || *i % 2.0 == 0.0) {
                assert_within_delta!(0.0, F64::pow(f64::NEG_INFINITY, *i), EXACT);
            }
        }
        // the first argument is negative infinity and the second argument is a negative finite odd integer, then the result is negative zero.
        for  i in DOUBLES.iter() {
            if *i > 0.0 && *i % 2.0 == 1.0 {
                assert_within_delta!(-0.0, F64::pow(-0.0, *i), EXACT);
            }
        }
        for  i in DOUBLES.iter() {
            if *i < 0.0 && *i % 2.0 == -1.0 {
                assert_within_delta!(-0.0, F64::pow(f64::NEG_INFINITY, *i), EXACT);
            }
        }
        // the first argument is negative infinity and the second argument is greater than zero but not a finite odd integer, then the result is positive infinity.
        for  i in DOUBLES.iter() {
            if *i > 0.0 && ((*i).is_infinite() || *i % 2.0 == 0.0) {
                assert_within_delta!(f64::INFINITY, F64::pow(f64::NEG_INFINITY, *i), EXACT);
            }
        }
        for  i in DOUBLES.iter() {
            if *i < 0.0 && ((*i).is_infinite() || *i % 2.0 == 0.0) {
                assert_within_delta!(f64::INFINITY, F64::pow(-0.0, *i), EXACT);
            }
        }
        // the first argument is negative infinity and the second argument is a positive finite odd integer, then the result is negative infinity.
        for  i in DOUBLES.iter() {
            if *i > 0.0 && *i % 2.0 == 1.0 {
                assert_within_delta!(f64::NEG_INFINITY, F64::pow(f64::NEG_INFINITY, *i), EXACT);
            }
        }
        for  i in DOUBLES.iter() {
            if *i < 0.0 && *i % 2.0 == -1.0 {
                assert_within_delta!(f64::NEG_INFINITY, F64::pow(-0.0, *i), EXACT);
            }
        }
        for  d in DOUBLES.iter() {
            // If the first argument is finite and less than zero
            if *d < 0.0 && f64::abs(*d) <= f64::MAX {
                for  i in DOUBLES.iter() {
                    if f64::abs(*i) <= f64::MAX {
                        // if the second argument is a finite even integer, the result is equal to the result of raising the absolute value of the first argument to the power of the second argument
                        if *i % 2.0 == 0.0 {
                            assert_within_delta!(F64::pow(-d, *i), F64::pow(*d, *i), EXACT);
                        }
                        else // if the second argument is a finite odd integer, the result is equal to the negative of the result of raising the absolute value of the first argument to the power of the second argument
                        if f64::abs(*i) % 2.0 == 1.0 {
                            assert_within_delta!(-F64::pow(-d, *i), F64::pow(*d, *i), EXACT);
                        }
                        else {// if the second argument is finite and not an integer, then the result is NaN.
                            assert!(F64::pow(*d, *i).is_nan());
                        }

                    }
                }
            }
        }
        /*
        // If both arguments are integers, then the result is exactly equal to the mathematical result of raising the first argument to the power
        // of the second argument if that result can in fact be represented exactly as a double value.
        // This value is empirical: 2^18 > 200.000 resulting bits after raising d to power i.
         const TOO_BIG_TO_CALCULATE: i32 = 18;
        for  d in DOUBLES.iter() {
            if *d % 1.0 == 0.0 {
                 let d_negative: bool = double_to_raw_long_bits(d) < 0;
                for  i in DOUBLES.iter() {
                    if *i % 1.0 == 0.0 {
                         let bd: BigInteger = BigDecimal.value_of(&d).to_big_integer().abs();
                         let bi: BigInteger = BigDecimal.value_of(&i).to_big_integer().abs();
                         let mut expected: f64;
                        if bd.bit_length() > 1 && bi.bit_length() > 1 && 32 - Integer.number_of_leading_zeros(&bd.bit_length()) + bi.bit_length() > TOO_BIG_TO_CALCULATE {
                            // Result would be too big.
                            expected =  if i < 0.0 { 0.0 } else { f64::INFINITY };
                        } else {
                             let res: BigInteger = ArithmeticUtils.pow(&bd, &bi);
                            if i >= 0.0 {
                                expected = res.double_value();
                            } else if res.signum() == 0 {
                                expected = f64::INFINITY;
                            } else {
                                expected = BigDecimal::ONE.divide(BigDecimal::new(&res), 1024, RoundingMode::HALF_UP).double_value();
                            }
                        }
                        if d_negative && bi.test_bit(0) {
                            expected = -expected;
                        }
                        assert_within_delta!(d + "^" + i + "=" + expected + ", f64::pow=" + f64::pow(*d, *i), &expected, F64::pow(*d, *i),  if expected == 0.0 || Double.is_infinite(&expected) || f64::is_nan(&expected) { EXACT } else { 2.0 * f64::ulp(&expected) });
                    }
                }
            }
        }
        */
        
    }

 #[test]
    pub fn  test_pow_large_integral_double()   {
         let y: f64 = F64::scalb(1.0, 65);
        assert_within_delta!(f64::INFINITY, F64::pow(F64::next_up(1.0), y), 1.0);
        assert_within_delta!(1.0, F64::pow(1.0, y), 1.0);
        assert_within_delta!(0.0, F64::pow(F64::next_down(1.0), y), 1.0);
        assert_within_delta!(0.0, F64::pow(F64::next_up(-1.0), y), 1.0);
        assert_within_delta!(1.0, F64::pow(-1.0, y), 1.0);
        assert_within_delta!(f64::INFINITY, F64::pow(F64::next_down(-1.0), y), 1.0);
    }


 #[test]
    pub fn  test_atan2_special_cases()   {
        assert!( f64::is_nan(F64::atan2(f64::NAN, 0.0)),"atan2(NaN, 0.0) should be NaN");
        assert!( f64::is_nan(F64::atan2(0.0, f64::NAN)),"atan2(0.0, NaN) should be NaN");
        assert_within_delta!( 0.0, F64::atan2(0.0, 0.0), *precision::EPSILON,"atan2(0.0, 0.0) should be 0.0");
        assert_within_delta!( 0.0, F64::atan2(0.0, 0.001), *precision::EPSILON,"atan2(0.0, 0.001) should be 0.0");
        assert_within_delta!( 0.0, F64::atan2(0.1, f64::INFINITY), *precision::EPSILON,"atan2(0.1, +Inf) should be 0.0");
        assert_within_delta!( -0.0, F64::atan2(-0.0, 0.0), *precision::EPSILON,"atan2(-0.0, 0.0) should be -0.0");
        assert_within_delta!( -0.0, F64::atan2(-0.0, 0.001), *precision::EPSILON,"atan2(-0.0, 0.001) should be -0.0");
        assert_within_delta!( -0.0, F64::atan2(-0.1, f64::INFINITY), *precision::EPSILON,"atan2(-0.0, +Inf) should be -0.0");
        assert_within_delta!( fastmath::PI, F64::atan2(0.0, -0.0), *precision::EPSILON,"atan2(0.0, -0.0) should be PI");
        assert_within_delta!( fastmath::PI, F64::atan2(0.1, f64::NEG_INFINITY), *precision::EPSILON,"atan2(0.1, -Inf) should be PI");
        assert_within_delta!( -fastmath::PI, F64::atan2(-0.0, -0.0), *precision::EPSILON,"atan2(-0.0, -0.0) should be -PI");
        assert_within_delta!( -fastmath::PI, F64::atan2(-0.1, f64::NEG_INFINITY), *precision::EPSILON,"atan2(0.1, -Inf) should be -PI");
        assert_within_delta!( fastmath::PI / 2.0, F64::atan2(0.1, 0.0), *precision::EPSILON,"atan2(0.1, 0.0) should be PI/2");
        assert_within_delta!( fastmath::PI / 2.0, F64::atan2(0.1, -0.0), *precision::EPSILON,"atan2(0.1, -0.0) should be PI/2");
        assert_within_delta!( fastmath::PI / 2.0, F64::atan2(f64::INFINITY, 0.1), *precision::EPSILON,"atan2(Inf, 0.1) should be PI/2");
        assert_within_delta!( fastmath::PI / 2.0, F64::atan2(f64::INFINITY, -0.1), *precision::EPSILON,"atan2(Inf, -0.1) should be PI/2");
        assert_within_delta!( -fastmath::PI / 2.0, F64::atan2(-0.1, 0.0), *precision::EPSILON,"atan2(-0.1, 0.0) should be -PI/2");
        assert_within_delta!( -fastmath::PI / 2.0, F64::atan2(-0.1, -0.0), *precision::EPSILON,"atan2(-0.1, -0.0) should be -PI/2");
        assert_within_delta!( -fastmath::PI / 2.0, F64::atan2(f64::NEG_INFINITY, 0.1), *precision::EPSILON,"atan2(-Inf, 0.1) should be -PI/2");
        assert_within_delta!( -fastmath::PI / 2.0, F64::atan2(f64::NEG_INFINITY, -0.1), *precision::EPSILON,"atan2(-Inf, -0.1) should be -PI/2");
        assert_within_delta!( fastmath::PI / 4.0, F64::atan2(f64::INFINITY, f64::INFINITY), *precision::EPSILON,"atan2(Inf, Inf) should be PI/4");
        assert_within_delta!( fastmath::PI * 3.0 / 4.0, F64::atan2(f64::INFINITY, f64::NEG_INFINITY), *precision::EPSILON,"atan2(Inf, -Inf) should be PI * 3/4");
        assert_within_delta!( -fastmath::PI / 4.0, F64::atan2(f64::NEG_INFINITY, f64::INFINITY), *precision::EPSILON,"atan2(-Inf, Inf) should be -PI/4");
        assert_within_delta!( -fastmath::PI * 3.0 / 4.0, F64::atan2(f64::NEG_INFINITY, f64::NEG_INFINITY), *precision::EPSILON,"atan2(-Inf, -Inf) should be -PI * 3/4");
    }
    

    #[test]
    pub fn  test_pow_accuracy()   {
    	let mut generator = create_generator();
    	let field = DfpField::new(40);
         let mut maxerrulp: f64 = 0.0;
         {
             let mut i: i32 = 0;
            while i < NUMBER_OF_TRIALS {
                {
                     let x: f64 = (generator.next_double() * 2.0 + 0.25);
                     let y: f64 = (generator.next_double() * 1200.0 - 600.0) * generator.next_double();
                    /*
             * double x = F64::floor(generator.nextDouble()*1024.0 - 512.0); double
             * y; if (x != 0) y = F64::floor(512.0 / F64::abs(x)); else
             * y = generator.nextDouble()*1200.0; y = y - y/2; x = F64::pow(2.0, x) *
             * generator.nextDouble(); y = y * generator.nextDouble();
             */
                    // double x = generator.nextDouble()*2.0;
                     let tst: f64 = F64::pow(x, y);
                     let reference: f64 = DfpMath::pow_dfp(&field.new_dfp_f64(x), &field.new_dfp_f64(y)).to_double();
                     let err: f64 = (tst - reference) / reference;
                    if err != 0.0 {
                         let ulp: f64 = f64::abs(reference - long_bits_to_double(&(double_to_raw_long_bits(&reference) ^ 1)));
                         let errulp: f64 = 
                         field.new_dfp_f64(tst)
                         .subtract(&DfpMath::pow_dfp(&field.new_dfp_f64(x), &field.new_dfp_f64(y)))
                         .divide(&field.new_dfp_f64(ulp)).unwrap().to_double();
                        //                System.out.println(x + "\t" + y + "\t" + tst + "\t" + reference + "\t" + err + "\t" + errulp);
                        maxerrulp = f64::max(maxerrulp, f64::abs(errulp));
                    }
                }
                i += 1;
             }
         }

        assert!(maxerrulp < MAX_ERROR_ULP, "pow() had errors {} in excess of {} ULP", maxerrulp, MAX_ERROR_ULP);
    }
    
       
    #[test]
    pub fn  test_exp_accuracy()   {
    	let mut generator = create_generator();
    	let field = DfpField::new(40);    	
         let mut maxerrulp: f64 = 0.0;
         {
             let mut i: i32 = 0;
            while i < NUMBER_OF_TRIALS {
                {
                    /* double x = 1.0 + i/1024.0/2.0; */
                     let x: f64 = ((generator.next_double() * 1416.0) - 708.0) * generator.next_double();
                    // double x = (generator.nextDouble() * 20.0) - 10.0;
                    // double x = ((generator.nextDouble() * 2.0) - 1.0) * generator.nextDouble();
                    /* double x = 3.0 / 512.0 * i - 3.0; */
                     let tst: f64 = F64::exp(x);
                     let reference: f64 = DfpMath::exp(&field.new_dfp_f64(x)).to_double();
                     let err: f64 = (tst - reference) / reference;
                    if err != 0.0 {
                         let ulp: f64 = f64::abs(reference - long_bits_to_double(&(double_to_raw_long_bits(&reference) ^ 1)));
                         let errulp: f64 = field.new_dfp_f64(tst)
                         .subtract(&DfpMath::exp(&field.new_dfp_f64(x)))
                         .divide(&field.new_dfp_f64(ulp)).unwrap().to_double();
                        //                System.out.println(x + "\t" + tst + "\t" + reference + "\t" + err + "\t" + errulp);
                        maxerrulp = f64::max(maxerrulp, f64::abs(errulp));
                    }
                }
                i += 1;
             }
         }

        assert!(maxerrulp < MAX_ERROR_ULP, "exp() had errors {} in excess of {} ULP", maxerrulp, MAX_ERROR_ULP);
    }

	 #[test]
    pub fn  test_sin_accuracy()   {
    	let mut generator = create_generator();
    	let field = DfpField::new(40);   
         let mut maxerrulp: f64 = 0.0;
         {
             let mut i: i32 = 0;
            while i < NUMBER_OF_TRIALS {
                {
                    /* double x = 1.0 + i/1024.0/2.0; */
                    // double x = ((generator.nextDouble() * 1416.0) - 708.0) * generator.nextDouble();
                     let x: f64 = ((generator.next_double() * f64::consts::PI) - f64::consts::PI / 2.0) * ((2.0 as f64).powf(21.0)) * generator.next_double();
                    // double x = (generator.nextDouble() * 20.0) - 10.0;
                    // double x = ((generator.nextDouble() * 2.0) - 1.0) * generator.nextDouble();
                    /* double x = 3.0 / 512.0 * i - 3.0; */
                     let tst: f64 = F64::sin(x);
                     let reference: f64 = DfpMath::sin(&field.new_dfp_f64(x)).to_double();
                     let err: f64 = (tst - reference) / reference;
                    if err != 0.0 {
                         let ulp: f64 = f64::abs(reference - long_bits_to_double(&(double_to_raw_long_bits(&reference) ^ 1)));
                         let errulp: f64 = field.new_dfp_f64(tst).subtract(&DfpMath::sin(&field.new_dfp_f64(x))).divide(&field.new_dfp_f64(ulp)).unwrap().to_double();
                        //                System.out.println(x + "\t" + tst + "\t" + reference + "\t" + err + "\t" + errulp);
                        maxerrulp = f64::max(maxerrulp, f64::abs(errulp));
                    }
                }
                i += 1;
             }
         }

        assert!(maxerrulp < MAX_ERROR_ULP, "sin() had errors {} in excess of {} ULP", maxerrulp, MAX_ERROR_ULP);
    }
    
    
 #[test]
    pub fn  test_cos_accuracy()   {
    	let mut generator = create_generator();
    	let field = DfpField::new(40);   
         let mut maxerrulp: f64 = 0.0;
         {
             let mut i: i32 = 0;
            while i < NUMBER_OF_TRIALS {
                {
                    /* double x = 1.0 + i/1024.0/2.0; */
                    // double x = ((generator.nextDouble() * 1416.0) - 708.0) * generator.nextDouble();
                     let x: f64 = ((generator.next_double() * f64::consts::PI) - f64::consts::PI / 2.0) * ((2.0 as f64).powf(21.0)) * generator.next_double();
                    // double x = (generator.nextDouble() * 20.0) - 10.0;
                    // double x = ((generator.nextDouble() * 2.0) - 1.0) * generator.nextDouble();
                    /* double x = 3.0 / 512.0 * i - 3.0; */
                     let tst: f64 = F64::cos(x);
                     let reference: f64 = DfpMath::cos(&field.new_dfp_f64(x)).to_double();
                     let err: f64 = (tst - reference) / reference;
                    if err != 0.0 {
                         let ulp: f64 = f64::abs(reference - long_bits_to_double(&(double_to_raw_long_bits(&reference) ^ 1)));
                         let errulp: f64 = field.new_dfp_f64(tst).subtract(&DfpMath::cos(&field.new_dfp_f64(x))).divide(&field.new_dfp_f64(ulp)).unwrap().to_double();
                        //                System.out.println(x + "\t" + tst + "\t" + reference + "\t" + err + "\t" + errulp);
                        maxerrulp = f64::max(maxerrulp, f64::abs(errulp));
                    }
                }
                i += 1;
             }
         }

          assert!(maxerrulp < MAX_ERROR_ULP, "cos() had errors {} in excess of {} ULP", maxerrulp, MAX_ERROR_ULP);
    }

    #[test]
    pub fn  test_tan_accuracy()   {
    	let mut generator = create_generator();
    	let field = DfpField::new(40);   
         let mut maxerrulp: f64 = 0.0;
         {
             let mut i: i32 = 0;
            while i < NUMBER_OF_TRIALS {
                {
                    /* double x = 1.0 + i/1024.0/2.0; */
                    // double x = ((generator.nextDouble() * 1416.0) - 708.0) * generator.nextDouble();
                     let x: f64 = ((generator.next_double() * f64::consts::PI) - f64::consts::PI / 2.0) * ((2.0 as f64).powf(12.0)) * generator.next_double();
                    // double x = (generator.nextDouble() * 20.0) - 10.0;
                    // double x = ((generator.nextDouble() * 2.0) - 1.0) * generator.nextDouble();
                    /* double x = 3.0 / 512.0 * i - 3.0; */
                     let tst: f64 = F64::tan(x);
                     let reference: f64 = DfpMath::tan(&field.new_dfp_f64(x)).to_double();
                     let err: f64 = (tst - reference) / reference;
                    if err != 0.0 {
                         let ulp: f64 = f64::abs(reference - long_bits_to_double(&(double_to_raw_long_bits(&reference) ^ 1)));
                         let errulp: f64 = field.new_dfp_f64(tst).subtract(&DfpMath::tan(&field.new_dfp_f64(x))).divide(&field.new_dfp_f64(ulp)).unwrap().to_double();
                        //                System.out.println(x + "\t" + tst + "\t" + reference + "\t" + err + "\t" + errulp);
                        maxerrulp = f64::max(maxerrulp, f64::abs(errulp));
                    }
                }
                i += 1;
             }
         }

          assert!(maxerrulp < MAX_ERROR_ULP, "tan() had errors {} in excess of {} ULP", maxerrulp, MAX_ERROR_ULP);
    }

    #[test]
    pub fn  test_atan_accuracy()   {
    	let mut generator = create_generator();
    	let field = DfpField::new(40);   
         let mut maxerrulp: f64 = 0.0;
         {
             let mut i: i32 = 0;
            while i < NUMBER_OF_TRIALS {
                {
                    /* double x = 1.0 + i/1024.0/2.0; */
                    // double x = ((generator.nextDouble() * 1416.0) - 708.0) * generator.nextDouble();
                    // double x = ((generator.nextDouble() * f64::consts::PI) - f64::consts::PI/2.0) *
                    // generator.nextDouble();
                     let x: f64 = ((generator.next_double() * 16.0) - 8.0) * generator.next_double();
                    // double x = (generator.nextDouble() * 20.0) - 10.0;
                    // double x = ((generator.nextDouble() * 2.0) - 1.0) * generator.nextDouble();
                    /* double x = 3.0 / 512.0 * i - 3.0; */
                     let tst: f64 = F64::atan(x);
                     let reference: f64 = DfpMath::atan(&field.new_dfp_f64(x)).to_double();
                     let err: f64 = (tst - reference) / reference;
                    if err != 0.0 {
                         let ulp: f64 = f64::abs(reference - long_bits_to_double(&(double_to_raw_long_bits(&reference) ^ 1)));
                         let errulp: f64 = field.new_dfp_f64(tst).subtract(&DfpMath::atan(&field.new_dfp_f64(x))).divide(&field.new_dfp_f64(ulp)).unwrap().to_double();
                        //                System.out.println(x + "\t" + tst + "\t" + reference + "\t" + err + "\t" + errulp);
                        maxerrulp = f64::max(maxerrulp, f64::abs(errulp));
                    }
                }
                i += 1;
             }
         }

          assert!(maxerrulp < MAX_ERROR_ULP, "atan() had errors {} in excess of {} ULP", maxerrulp, MAX_ERROR_ULP);
    }

    #[test]
    pub fn  test_atan2_accuracy()   {
    	let mut generator = create_generator();
    	let field = DfpField::new(40);   
         let mut maxerrulp: f64 = 0.0;
         {
             let mut i: i32 = 0;
            while i < NUMBER_OF_TRIALS {
                {
                    /* double x = 1.0 + i/1024.0/2.0; */
                    // double x = ((generator.nextDouble() * 1416.0) - 708.0) * generator.nextDouble();
                     let x: f64 = generator.next_double() - 0.5;
                     let y: f64 = generator.next_double() - 0.5;
                    // double x = (generator.nextDouble() * 20.0) - 10.0;
                    // double x = ((generator.nextDouble() * 2.0) - 1.0) * generator.nextDouble();
                    /* double x = 3.0 / 512.0 * i - 3.0; */
                     let tst: f64 = F64::atan2(y, x);
                     let mut refdfp: Dfp = DfpMath::atan(&field.new_dfp_f64(y).divide(&field.new_dfp_f64(x)).unwrap());
                    /* Make adjustments for sign */
                    if x < 0.0 {
                        if y > 0.0 {
                            refdfp = field.get_pi().add(&refdfp);
                        }
                        else {refdfp = refdfp.subtract(&field.get_pi());
                        }

                    }
                     let reference: f64 = refdfp.to_double();
                     let err: f64 = (tst - reference) / reference;
                    if err != 0.0 {
                         let ulp: f64 = f64::abs(reference - long_bits_to_double(&(double_to_raw_long_bits(&reference) ^ 1)));
                         let errulp: f64 = field.new_dfp_f64(tst).subtract(&refdfp).divide(&field.new_dfp_f64(ulp)).unwrap().to_double();
                        //                System.out.println(x + "\t" + y + "\t" + tst + "\t" + reference + "\t" + errulp);
                        maxerrulp = f64::max(maxerrulp, f64::abs(errulp));
                    }
                }
                i += 1;
             }
         }

          assert!(maxerrulp < MAX_ERROR_ULP, "atan2() had errors {} in excess of {} ULP", maxerrulp, MAX_ERROR_ULP);
    }

    #[test]
    pub fn  test_expm1_accuracy()   {
    	let mut generator = create_generator();
    	let field = DfpField::new(40);   
         let mut maxerrulp: f64 = 0.0;
         {
             let mut i: i32 = 0;
            while i < NUMBER_OF_TRIALS {
                {
                    /* double x = 1.0 + i/1024.0/2.0; */
                    // double x = (generator.nextDouble() * 20.0) - 10.0;
                     let x: f64 = ((generator.next_double() * 16.0) - 8.0) * generator.next_double();
                    /* double x = 3.0 / 512.0 * i - 3.0; */
                     let tst: f64 = F64::expm1(x);
                     let reference: f64 = DfpMath::exp(&field.new_dfp_f64(x)).subtract(&field.get_one()).to_double();
                     let err: f64 = (tst - reference) / reference;
                    if err != 0.0 {
                         let ulp: f64 = f64::abs(reference - long_bits_to_double(&(double_to_raw_long_bits(&reference) ^ 1)));
                         let errulp: f64 = field.new_dfp_f64(tst).subtract(&DfpMath::exp(&field.new_dfp_f64(x)).subtract(&field.get_one())).divide(&field.new_dfp_f64(ulp)).unwrap().to_double();
                        //                System.out.println(x + "\t" + tst + "\t" + reference + "\t" + err + "\t" + errulp);
                        maxerrulp = f64::max(maxerrulp, f64::abs(errulp));
                    }
                }
                i += 1;
             }
         }

          assert!(maxerrulp < MAX_ERROR_ULP, "expm1() had errors {} in excess of {} ULP", maxerrulp, MAX_ERROR_ULP);
    }

    #[test]
    pub fn  test_asin_accuracy()   {
    	let mut generator = create_generator();
    	let field = DfpField::new(40);   
         let mut maxerrulp: f64 = 0.0;
         {
             let mut i: i32 = 0;
            while i < 10000 {
                {
                     let x: f64 = ((generator.next_double() * 2.0) - 1.0) * generator.next_double();
                     let tst: f64 = F64::asin(x);
                     let reference: f64 = DfpMath::asin(&field.new_dfp_f64(x)).to_double();
                     let err: f64 = (tst - reference) / reference;
                    if err != 0.0 {
                         let ulp: f64 = f64::abs(reference - long_bits_to_double(&(double_to_raw_long_bits(&reference) ^ 1)));
                         let errulp: f64 = field.new_dfp_f64(tst).subtract(&DfpMath::asin(&field.new_dfp_f64(x))).divide(&field.new_dfp_f64(ulp)).unwrap().to_double();
                        //System.out.println(x+"\t"+tst+"\t"+reference+"\t"+err+"\t"+errulp);
                        maxerrulp = f64::max(maxerrulp, f64::abs(errulp));
                    }
                }
                i += 1;
             }
         }

          assert!(maxerrulp < MAX_ERROR_ULP, "asin() had errors {} in excess of {} ULP", maxerrulp, MAX_ERROR_ULP);
    }

    #[test]
    pub fn  test_acos_accuracy()   {
    	let mut generator = create_generator();
    	let field = DfpField::new(40);   
         let mut maxerrulp: f64 = 0.0;
         {
             let mut i: i32 = 0;
            while i < 10000 {
                {
                     let x: f64 = ((generator.next_double() * 2.0) - 1.0) * generator.next_double();
                     let tst: f64 = F64::acos(x);
                     let reference: f64 = DfpMath::acos(&field.new_dfp_f64(x)).to_double();
                     let err: f64 = (tst - reference) / reference;
                    if err != 0.0 {
                         let ulp: f64 = f64::abs(reference - long_bits_to_double(&(double_to_raw_long_bits(&reference) ^ 1)));
                         let errulp: f64 = field.new_dfp_f64(tst).subtract(&DfpMath::acos(&field.new_dfp_f64(x))).divide(&field.new_dfp_f64(ulp)).unwrap().to_double();
                        //System.out.println(x+"\t"+tst+"\t"+reference+"\t"+err+"\t"+errulp);
                        maxerrulp = f64::max(maxerrulp, f64::abs(errulp));
                    }
                }
                i += 1;
             }
         }

          assert!(maxerrulp < MAX_ERROR_ULP, "acos() had errors {} in excess of {} ULP", maxerrulp, MAX_ERROR_ULP);
    }
    
    
     /**
     * Added tests for a 100% coverage of acos().
     */
    #[test]
    pub fn  test_acos_special_cases()   {
        assert!( f64::is_nan(F64::acos(f64::NAN)),"acos(NaN) should be NaN");
        assert!( f64::is_nan(F64::acos(-1.1)),"acos(-1.1) should be NaN");
        assert!( f64::is_nan(F64::acos(1.1)),"acos(-1.1) should be NaN");
        assert_within_delta!( F64::acos(-1.0), fastmath::PI, *precision::EPSILON,"acos(-1.0) should be PI");
        assert_within_delta!( F64::acos(1.0), 0.0, *precision::EPSILON,"acos(1.0) should be 0.0");
        assert_within_delta!( F64::acos(0.0), fastmath::PI / 2.0, *precision::EPSILON,"acos(0.0) should be PI/2");
    }

    /**
     * Added tests for a 100% coverage of asin().
     */
    #[test]
    pub fn  test_asin_special_cases()   {
        assert!( f64::is_nan(F64::asin(f64::NAN)),"asin(NaN) should be NaN");
        assert!( f64::is_nan(F64::asin(1.1)),"asin(1.1) should be NaN");
        assert!( f64::is_nan(F64::asin(-1.1)),"asin(-1.1) should be NaN");
        assert_within_delta!( F64::asin(1.0), fastmath::PI / 2.0, *precision::EPSILON,"asin(1.0) should be PI/2");
        assert_within_delta!( F64::asin(-1.0), -fastmath::PI / 2.0, *precision::EPSILON,"asin(-1.0) should be -PI/2");
        assert_within_delta!( F64::asin(0.0), 0.0, *precision::EPSILON,"asin(0.0) should be 0.0");
    }

    fn  cosh(x: &Dfp) -> Dfp  {
        return DfpMath::exp(&x).add(&DfpMath::exp(&x.negate())).divide_i32(2);
    }

    fn  sinh(x: &Dfp) -> Dfp  {
        return DfpMath::exp(&x).subtract(&DfpMath::exp(&x.negate())).divide_i32(2)
    }

    fn  tanh(x: &Dfp) -> Dfp  {
        return sinh(&x).divide(&cosh(&x)).unwrap();
    }

    #[test]
    pub fn  test_sinh_accuracy()   {
    	let mut generator = create_generator();
    	let field = DfpField::new(40);   
         let mut maxerrulp: f64 = 0.0;
         {
             let mut i: i32 = 0;
            while i < 10000 {
                {
                     let x: f64 = ((generator.next_double() * 16.0) - 8.0) * generator.next_double();
                     let tst: f64 = F64::sinh(x);
                     let reference: f64 = sinh(&field.new_dfp_f64(x)).to_double();
                     let err: f64 = (tst - reference) / reference;
                    if err != 0.0 {
                         let ulp: f64 = f64::abs(reference - long_bits_to_double(&(double_to_raw_long_bits(&reference) ^ 1)));
                         let errulp: f64 = field.new_dfp_f64(tst).subtract(&sinh(&field.new_dfp_f64(x))).divide(&field.new_dfp_f64(ulp)).unwrap().to_double();
                        //System.out.println(x+"\t"+tst+"\t"+reference+"\t"+err+"\t"+errulp);
                        maxerrulp = f64::max(maxerrulp, f64::abs(errulp));
                    }
                }
                i += 1;
             }
         }

          assert!(maxerrulp < MAX_ERROR_ULP, "sinh() had errors {} in excess of {} ULP", maxerrulp, MAX_ERROR_ULP);
    }

    #[test]
    pub fn  test_cosh_accuracy()   {
    	let mut generator = create_generator();
    	let field = DfpField::new(40);   
         let mut maxerrulp: f64 = 0.0;
         {
             let mut i: i32 = 0;
            while i < 10000 {
                {
                     let x: f64 = ((generator.next_double() * 16.0) - 8.0) * generator.next_double();
                     let tst: f64 = F64::cosh(x);
                     let reference: f64 = cosh(&field.new_dfp_f64(x)).to_double();
                     let err: f64 = (tst - reference) / reference;
                    if err != 0.0 {
                         let ulp: f64 = f64::abs(reference - long_bits_to_double(&(double_to_raw_long_bits(&reference) ^ 1)));
                         let errulp: f64 = field.new_dfp_f64(tst).subtract(&cosh(&field.new_dfp_f64(x))).divide(&field.new_dfp_f64(ulp)).unwrap().to_double();
                        //System.out.println(x+"\t"+tst+"\t"+reference+"\t"+err+"\t"+errulp);
                        maxerrulp = f64::max(maxerrulp, f64::abs(errulp));
                    }
                }
                i += 1;
             }
         }

          assert!(maxerrulp < MAX_ERROR_ULP, "cosh() had errors {} in excess of {} ULP", maxerrulp, MAX_ERROR_ULP);
    }

    #[test]
    pub fn  test_tanh_accuracy()   {
    	let mut generator = create_generator();
    	let field = DfpField::new(40);   
         let mut maxerrulp: f64 = 0.0;
         {
             let mut i: i32 = 0;
            while i < 10000 {
                {
                     let x: f64 = ((generator.next_double() * 16.0) - 8.0) * generator.next_double();
                     let tst: f64 = F64::tanh(x);
                     let reference: f64 = tanh(&field.new_dfp_f64(x)).to_double();
                     let err: f64 = (tst - reference) / reference;
                    if err != 0.0 {
                         let ulp: f64 = f64::abs(reference - long_bits_to_double(&(double_to_raw_long_bits(&reference) ^ 1)));
                         let errulp: f64 = field.new_dfp_f64(tst).subtract(&tanh(&field.new_dfp_f64(x))).divide(&field.new_dfp_f64(ulp)).unwrap().to_double();
                        //System.out.println(x+"\t"+tst+"\t"+reference+"\t"+err+"\t"+errulp);
                        maxerrulp = f64::max(maxerrulp, f64::abs(errulp));
                    }
                }
                i += 1;
             }
         }

          assert!(maxerrulp < MAX_ERROR_ULP, "tanh() had errors {} in excess of {} ULP", maxerrulp, MAX_ERROR_ULP);
    }

    #[test]
    pub fn  test_cbrt_accuracy()   {
    	let mut generator = create_generator();
    	let field = DfpField::new(40);   
         let mut maxerrulp: f64 = 0.0;
         {
             let mut i: i32 = 0;
            while i < 10000 {
                {
                     let x: f64 = ((generator.next_double() * 200.0) - 100.0) * generator.next_double();
                     let tst: f64 = F64::cbrt(x);
                     let reference: f64 = cbrt(&field.new_dfp_f64(x)).to_double();
                     let err: f64 = (tst - reference) / reference;
                    if err != 0.0 {
                         let ulp: f64 = f64::abs(reference - long_bits_to_double(&(double_to_raw_long_bits(&reference) ^ 1)));
                         let errulp: f64 = field.new_dfp_f64(tst).subtract(&cbrt(&field.new_dfp_f64(x))).divide(&field.new_dfp_f64(ulp)).unwrap().to_double();
                        //System.out.println(x+"\t"+tst+"\t"+reference+"\t"+err+"\t"+errulp);
                        maxerrulp = f64::max(maxerrulp, f64::abs(errulp));
                    }
                }
                i += 1;
             }
         }

          assert!(maxerrulp < MAX_ERROR_ULP, "cbrt() had errors {} in excess of {} ULP", maxerrulp, MAX_ERROR_ULP);
    }

    fn  cbrt(x_p: &Dfp) -> Dfp  {
    	let mut x: Dfp = x_p.clone();
         let mut negative: bool = false;
        if x.less_than(&x.get_zero()) {
            negative = true;
            x = x.negate();
        }
         let mut y: Dfp = DfpMath::pow_dfp(&x, &x.get_one().divide_f64(3.0));
        if negative {
            y = y.negate();
        }
        return y;
    }
    
    
    #[test]
    pub fn  test_to_degrees()   {
    	let mut generator = create_generator();
    	let field = DfpField::new(40);   
         let mut maxerrulp: f64 = 0.0;
         {
             let mut i: i32 = 0;
            while i < NUMBER_OF_TRIALS {
                {
                     let x: f64 = generator.next_double();
                     let tst: f64 = field.new_dfp_f64(x).multiply_i32(180).divide(&field.get_pi()).unwrap().to_double();
                     let reference: f64 = F64::to_degrees(x);
                     let err: f64 = (tst - reference) / reference;
                    if err != 0.0 {
                         let ulp: f64 = f64::abs(reference - long_bits_to_double(&(double_to_raw_long_bits(&reference) ^ 1)));
                         let errulp: f64 = field.new_dfp_f64(tst).subtract(&DfpMath::exp(&field.new_dfp_f64(x)).subtract(&field.get_one())).divide(&field.new_dfp_f64(ulp)).unwrap().to_double();
                        //                System.out.println(x + "\t" + tst + "\t" + reference + "\t" + err + "\t" + errulp);
                        maxerrulp = f64::max(maxerrulp, f64::abs(errulp));
                    }
                }
                i += 1;
             }
         }

        
          assert!(maxerrulp < MAX_ERROR_ULP, "toDegrees() had errors {} in excess of {} ULP", maxerrulp, MAX_ERROR_ULP);
    }

    #[test]
    pub fn  test_to_radians()   {
    	let mut generator = create_generator();
    	let field = DfpField::new(40);   
         let mut maxerrulp: f64 = 0.0;
         {
             let mut i: i32 = 0;
            while i < NUMBER_OF_TRIALS {
                {
                     let x: f64 = generator.next_double();
                     let tst: f64 = field.new_dfp_f64(x).multiply(&field.get_pi()).divide_f64(180.0).to_double();
                     let reference: f64 = F64::to_radians(x);
                     let err: f64 = (tst - reference) / reference;
                    if err != 0.0 {
                         let ulp: f64 = f64::abs(reference - long_bits_to_double(&(double_to_raw_long_bits(&reference) ^ 1)));
                         let errulp: f64 = field.new_dfp_f64(tst).subtract(&DfpMath::exp(&field.new_dfp_f64(x)).subtract(&field.get_one())).divide(&field.new_dfp_f64(ulp)).unwrap().to_double();
                        //                System.out.println(x + "\t" + tst + "\t" + reference + "\t" + err + "\t" + errulp);
                        maxerrulp = f64::max(maxerrulp, f64::abs(errulp));
                    }
                }
                i += 1;
             }
         }
		 assert!(maxerrulp < MAX_ERROR_ULP, "toRadians() had errors {} in excess of {} ULP", maxerrulp, MAX_ERROR_ULP);
    }

    #[test]
    pub fn  test_next_after()   {
        // 0x402fffffffffffff 0x404123456789abcd -> 4030000000000000
        assert_within_delta!(16.0, F64::next_up(15.999999999999998), 0.0);
        // 0xc02fffffffffffff 0x404123456789abcd -> c02ffffffffffffe
        assert_within_delta!(-15.999999999999996, F64::next_after(-15.999999999999998, 34.27555555555555), 0.0);
        // 0x402fffffffffffff 0x400123456789abcd -> 402ffffffffffffe
        assert_within_delta!(15.999999999999996, F64::next_down(15.999999999999998), 0.0);
        // 0xc02fffffffffffff 0x400123456789abcd -> c02ffffffffffffe
        assert_within_delta!(-15.999999999999996, F64::next_after(-15.999999999999998, 2.142222222222222), 0.0);
        // 0x4020000000000000 0x404123456789abcd -> 4020000000000001
        assert_within_delta!(8.000000000000002, F64::next_after(8.0, 34.27555555555555), 0.0);
        // 0xc020000000000000 0x404123456789abcd -> c01fffffffffffff
        assert_within_delta!(-7.999999999999999, F64::next_after(-8.0, 34.27555555555555), 0.0);
        // 0x4020000000000000 0x400123456789abcd -> 401fffffffffffff
        assert_within_delta!(7.999999999999999, F64::next_after(8.0, 2.142222222222222), 0.0);
        // 0xc020000000000000 0x400123456789abcd -> c01fffffffffffff
        assert_within_delta!(-7.999999999999999, F64::next_after(-8.0, 2.142222222222222), 0.0);
        // 0x3f2e43753d36a223 0x3f2e43753d36a224 -> 3f2e43753d36a224
        assert_within_delta!(2.308922399667661E-4, F64::next_after(2.3089223996676606E-4, 2.308922399667661E-4), 0.0);
        // 0x3f2e43753d36a223 0x3f2e43753d36a223 -> 3f2e43753d36a223
        assert_within_delta!(2.3089223996676606E-4, F64::next_after(2.3089223996676606E-4, 2.3089223996676606E-4), 0.0);
        // 0x3f2e43753d36a223 0x3f2e43753d36a222 -> 3f2e43753d36a222
        assert_within_delta!(2.3089223996676603E-4, F64::next_after(2.3089223996676606E-4, 2.3089223996676603E-4), 0.0);
        // 0x3f2e43753d36a223 0xbf2e43753d36a224 -> 3f2e43753d36a222
        assert_within_delta!(2.3089223996676603E-4, F64::next_after(2.3089223996676606E-4, -2.308922399667661E-4), 0.0);
        // 0x3f2e43753d36a223 0xbf2e43753d36a223 -> 3f2e43753d36a222
        assert_within_delta!(2.3089223996676603E-4, F64::next_after(2.3089223996676606E-4, -2.3089223996676606E-4), 0.0);
        // 0x3f2e43753d36a223 0xbf2e43753d36a222 -> 3f2e43753d36a222
        assert_within_delta!(2.3089223996676603E-4, F64::next_after(2.3089223996676606E-4, -2.3089223996676603E-4), 0.0);
        // 0xbf2e43753d36a223 0x3f2e43753d36a224 -> bf2e43753d36a222
        assert_within_delta!(-2.3089223996676603E-4, F64::next_after(-2.3089223996676606E-4, 2.308922399667661E-4), 0.0);
        // 0xbf2e43753d36a223 0x3f2e43753d36a223 -> bf2e43753d36a222
        assert_within_delta!(-2.3089223996676603E-4, F64::next_after(-2.3089223996676606E-4, 2.3089223996676606E-4), 0.0);
        // 0xbf2e43753d36a223 0x3f2e43753d36a222 -> bf2e43753d36a222
        assert_within_delta!(-2.3089223996676603E-4, F64::next_after(-2.3089223996676606E-4, 2.3089223996676603E-4), 0.0);
        // 0xbf2e43753d36a223 0xbf2e43753d36a224 -> bf2e43753d36a224
        assert_within_delta!(-2.308922399667661E-4, F64::next_after(-2.3089223996676606E-4, -2.308922399667661E-4), 0.0);
        // 0xbf2e43753d36a223 0xbf2e43753d36a223 -> bf2e43753d36a223
        assert_within_delta!(-2.3089223996676606E-4, F64::next_after(-2.3089223996676606E-4, -2.3089223996676606E-4), 0.0);
        // 0xbf2e43753d36a223 0xbf2e43753d36a222 -> bf2e43753d36a222
        assert_within_delta!(-2.3089223996676603E-4, F64::next_after(-2.3089223996676606E-4, -2.3089223996676603E-4), 0.0);
    }
    

    #[test]
    pub fn  test_double_next_after_special_cases()   {    	
        assert_within_delta!(-f64::MAX, F64::next_after(f64::NEG_INFINITY, 0.0), 0.0);
        assert_within_delta!(f64::MAX, F64::next_after(f64::INFINITY, 0.0), 0.0);
        assert!(F64::next_after(f64::NAN, 0.0).is_nan());
        assert_within_delta!(f64::INFINITY, F64::next_after(f64::MAX, f64::INFINITY), 0.0);
        assert_within_delta!(f64::NEG_INFINITY, F64::next_after(-f64::MAX, f64::NEG_INFINITY), 0.0);        
        assert_within_delta!(f64::MIN, F64::next_after(0.0, 1.0), 0.0);
        assert_within_delta!(-f64::MIN, F64::next_after(0.0, -1.0), 0.0);       
    	let my_min = long_bits_to_double(&1);
        assert_within_delta!(0.0, F64::next_after(my_min, -1.0), 0.0);
        assert_within_delta!(0.0, F64::next_after(-my_min, 1.0), 0.0);
    }


    #[test]
    pub fn  test_float_next_after_special_cases()   {   	
        assert_within_delta!(-f32::MAX, F32::next_after(f32::NEG_INFINITY, 0.0), 0.0);
        assert_within_delta!(f32::MAX, F32::next_after(f32::INFINITY, 0.0), 0.0);
        assert!(F32::next_after(f32::NAN, 0.0).is_nan());
        assert_within_delta!(f32::INFINITY, F32::next_up(f32::MAX), 0.0);
        assert_within_delta!(f32::NEG_INFINITY, F32::next_down(-f32::MAX), 0.0);        
        assert_within_delta!(f32::MIN, F32::next_after(0.0, 1.0), 0.0);
        assert_within_delta!(-f32::MIN, F32::next_after(0.0, -1.0), 0.0);
		let my_min = int_bits_to_float(&1);
        assert_within_delta!(0.0, F32::next_after(my_min, -1.0), 0.0);
        assert_within_delta!(0.0, F32::next_after(-my_min, 1.0), 0.0);
    }


    #[test]
    pub fn  test_double_scalb_special_cases()   {
    	/*
        assert_within_delta!(2.5269841324701218E-175, F64::scalb(2.2250738585072014E-308, 442), 0.0);
        assert_within_delta!(1.307993905256674E297, F64::scalb(1.1102230246251565E-16, 1040), 0.0);
        */
    	let my_min = long_bits_to_double(&1);
        assert_within_delta!(7.2520887996488946E-217, F64::scalb(my_min, 356), 0.0);
        assert_within_delta!(8.98846567431158E307, F64::scalb(my_min, 2097), 0.0);
        assert_within_delta!(f64::INFINITY, F64::scalb(my_min, 2098), 0.0);
        assert_within_delta!(1.1125369292536007E-308, F64::scalb(2.225073858507201E-308, -1), 0.0);
        assert_within_delta!(1.0E-323, F64::scalb(f64::MAX, -2097), 0.0);
        let my_max = long_bits_to_double(&0x7FEFFFFFFFFFFFFF);
        assert_within_delta!(my_min, F64::scalb(my_max, -2098), 0.0);
        assert_within_delta!(0.0, F64::scalb(my_max, -2099), 0.0);
        assert_within_delta!(f64::INFINITY, F64::scalb(f64::INFINITY, -1000000), 0.0);
        assert_within_delta!(f64::NEG_INFINITY, F64::scalb(-1.1102230246251565E-16, 1078), 0.0);
        assert_within_delta!(f64::NEG_INFINITY, F64::scalb(-1.1102230246251565E-16, 1079), 0.0);
        assert_within_delta!(f64::NEG_INFINITY, F64::scalb(-2.2250738585072014E-308, 2047), 0.0);
        assert_within_delta!(f64::NEG_INFINITY, F64::scalb(-2.2250738585072014E-308, 2048), 0.0);
        assert_within_delta!(f64::NEG_INFINITY, F64::scalb(-1.7976931348623157E308, 2147483647), 0.0);
        assert_within_delta!(f64::INFINITY, F64::scalb(1.7976931348623157E308, 2147483647), 0.0);
        assert_within_delta!(f64::NEG_INFINITY, F64::scalb(-1.1102230246251565E-16, 2147483647), 0.0);
        assert_within_delta!(f64::INFINITY, F64::scalb(1.1102230246251565E-16, 2147483647), 0.0);
        assert_within_delta!(f64::NEG_INFINITY, F64::scalb(-2.2250738585072014E-308, 2147483647), 0.0);
        assert_within_delta!(f64::INFINITY, F64::scalb(2.2250738585072014E-308, 2147483647), 0.0);
    }

    #[test]
    pub fn  test_float_scalb_special_cases()   {
    	let my_min = int_bits_to_float(&1);
    	 let my_max = int_bits_to_float(&0x7F7FFFFF);
        assert_within_delta!(0.0, F32::scalb(my_min, -30), 0.0);
        assert_within_delta!(2.0 * f32::MIN, F32::scalb(f32::MIN, 1), 0.0);
        assert_within_delta!(7.555786e22, F32::scalb(f32::MAX, -52), 0.0);
        assert_within_delta!(1.7014118e38, F32::scalb(my_min, 276), 0.0);
        assert_within_delta!(f32::INFINITY, F32::scalb(my_min, 277), 0.0);
        assert_within_delta!(5.8774718e-39, F32::scalb(1.1754944e-38, -1), 0.0);
        assert_within_delta!(2.0 * my_min, F32::scalb(my_max, -276), 0.0);
        assert_within_delta!(my_min, F32::scalb(my_max, -277), 0.0);
        assert_within_delta!(0.0, F32::scalb(f32::MAX, -278), 0.0);
        assert_within_delta!(f32::INFINITY, F32::scalb(f32::INFINITY, -1000000), 0.0);
        assert_within_delta!(-3.13994498e38, F32::scalb(-1.1e-7, 151), 0.0);
        assert_within_delta!(f32::NEG_INFINITY, F32::scalb(-1.1e-7, 152), 0.0);
        assert_within_delta!(f32::INFINITY, F32::scalb(3.4028235E38, 2147483647), 0.0);
        assert_within_delta!(f32::NEG_INFINITY, F32::scalb(-3.4028235E38, 2147483647), 0.0);
    }

 #[test]
    pub fn  test_signum_double()   {
         let delta: f64 = 0.0;
        assert_within_delta!(1.0, F64::signum(2.0), delta);
        assert_within_delta!(0.0, F64::signum(0.0), delta);
        assert_within_delta!(-1.0, F64::signum(-2.0), delta);
         assert!(F64::signum(f64::NAN).is_nan());
    }


    #[test]
    pub fn  test_signum_float()   	{
         let delta: f32 = 0.0;
        assert_within_delta!(1.0, F32::signum(2.0), delta);
        assert_within_delta!(0.0, F32::signum(0.0), delta);
        assert_within_delta!(-1.0, F32::signum(-2.0), delta);
         assert!(F32::signum(f32::NAN).is_nan());
    }


    #[test]
    pub fn  test_log_with_base()   {
        assert_within_delta!(2.0, F64::log_base(2.0, 4.0), 0.0);
        assert_within_delta!(3.0, F64::log_base(2.0, 8.0), 0.0);
        assert!(f64::is_nan(F64::log_base(-1.0, 1.0)));
        assert!(f64::is_nan(F64::log_base(1.0, -1.0)));
        assert!(f64::is_nan(F64::log_base(0.0, 0.0)));
        assert_within_delta!(0.0, F64::log_base(0.0, 10.0), 0.0);
        assert_within_delta!(f64::NEG_INFINITY, F64::log_base(10.0, 0.0), 0.0);
    }


    #[test]
    pub fn  test_indicator_double()   {
         let delta: f64 = 0.0;
        assert_within_delta!(1.0, F64::copy_sign(1.0, 2.0), delta);
        assert_within_delta!(1.0, F64::copy_sign(1.0, 0.0), delta);
        assert_within_delta!(-1.0, F64::copy_sign(1.0, -0.0), delta);
        assert_within_delta!(1.0, F64::copy_sign(1.0, f64::INFINITY), delta);
        assert_within_delta!(-1.0, F64::copy_sign(1.0, f64::NEG_INFINITY), delta);
        assert_within_delta!(1.0, F64::copy_sign(1.0, f64::NAN), delta);
        assert_within_delta!(-1.0, F64::copy_sign(1.0, -2.0), delta);
    }

    #[test]
    pub fn  test_indicator_float()   {
         let delta: f32 = 0.0;
        assert_within_delta!(1.0, F32::copy_sign(1.0, 2.0), delta);
        assert_within_delta!(1.0, F32::copy_sign(1.0, 0.0), delta);
        assert_within_delta!(-1.0, F32::copy_sign(1.0, -0.0), delta);
        assert_within_delta!(1.0, F32::copy_sign(1.0, f32::INFINITY), delta);
        assert_within_delta!(-1.0, F32::copy_sign(1.0, f32::NEG_INFINITY), delta);
        assert_within_delta!(1.0, F32::copy_sign(1.0, f32::NAN), delta);
        assert_within_delta!(-1.0, F32::copy_sign(1.0, -2.0), delta);
    }

    #[test]
    pub fn  test_int_pow()   {
         let max_exp: i32 = 300;
         let field: DfpField = DfpField::new(40);
         let base: f64 = 1.23456789;
         let base_dfp: Dfp = field.new_dfp_f64(base);
         let mut dfp_power: Dfp = field.get_one();
         {
             let mut i: i32 = 0;
            while i < max_exp {
                {
                    assert_within_delta!(dfp_power.to_double(), F64::pow_i32(base, i), 0.6 * F64::ulp(dfp_power.to_double()),"exp={}",i);
                    dfp_power = dfp_power.multiply(&base_dfp);
                }
                i += 1;
             }
         }

    }
    
    
      #[test]
    pub fn  test_int_pow_huge()   {
        assert!(f64::is_infinite(F64::pow(F64::scalb(1.0, 500), 4.0)));
    }
    
    
    #[test]
    pub fn  test_int_pow_long_min_value()   {
        assert_within_delta!(1.0, F64::pow(1.0, i64::MIN as f64), -1.0);
    }
        
        #[test]
    pub fn  test_int_pow_special_cases()   {
         const EXACT: f64 = -1.0;
        const DOUBLES : [f64; 86] = 
         [f64::NEG_INFINITY, -0.0, 0.0, f64::INFINITY, 
         i64::MIN as f64, i32::MIN as f64, i16::MIN as f64, i8::MIN as f64, -(i64::MIN as f64) as f64, 
         -(i32::MIN as f64) as f64, -(i16::MIN as f64) as f64, -(i8::MIN as f64) as f64, i8::MAX as f64, 
         i16::MAX as f64, i32::MAX as f64, i64::MAX as f64, -i8::MAX as f64, -i16::MAX as f64, -i32::MAX as f64, 
         -i64::MAX as f64, f32::MAX as f64, f64::MAX as f64, f64::MIN as f64, f32::MIN as f64, -f32::MAX as f64, -f32::MAX as f64, 
         -f64::MIN as f64, -f32::MIN as f64, 
         0.5, 0.1, 0.2, 0.8, 1.1, 1.2, 1.5, 1.8, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 
         8.0, 9.0, 1.3, 2.2, 2.5, 2.8, 33.0, 33.1, 33.5, 33.8, 10.0, 300.0, 400.0, 500.0, 
         -0.5, -0.1, -0.2, -0.8, -1.1, -1.2, -1.5, -1.8, -1.0, -2.0, -3.0, -4.0, -5.0, -6.0, 
         -7.0, -8.0, -9.0, -1.3, -2.2, -2.5, -2.8, -33.0, -33.1, -33.5, -33.8, -10.0, -300.0, 
         -400.0, -500.0, ];
         assert!(F64::pow_i32(f64::NAN,1).is_nan());
         const INTS : [i64; 31] = [i64::MAX, i64::MAX - 1, i64::MIN, i64::MIN + 1, i64::MIN + 2,
          i32::MAX as i64, i32::MAX as i64 - 1, i32::MIN as i64, i32::MIN as i64 + 1, i32::MIN as i64 + 2,
           0, 1, 2, 3, 5, 8, 10, 20, 100, 300, 500, -1, -2, -3, -5, -8, -10, -20, -100, -300, -500, ]
        ;
        
        // If the second argument is positive or negative zero, then the result is 1.0.
        for  d in DOUBLES.iter() {
            assert_within_delta!(1.0, F64::pow_i64(*d, 0), EXACT);
        }
        // If the second argument is 1.0, then the result is the same as the first argument.
        for  d in DOUBLES.iter() {
            assert_within_delta!(*d, F64::pow_i64(*d, 1), EXACT);
        }
        // If the first argument is NaN and the second argument is nonzero, then the result is NaN.
        for  i in INTS.iter() {
            if *i != 0 {
                assert!(F64::pow_i64(f64::NAN, *i).is_nan());
            }
        }
        // the absolute value of the first argument is less than 1 and the second argument is negative infinity, then the result is positive infinity.
        for  d in DOUBLES.iter() {
            if f64::abs(*d) > 1.0 {
                assert_within_delta!(f64::INFINITY, F64::pow_i64(*d, i64::MAX - 1), EXACT);
            }
        }        
        for  d in DOUBLES.iter() {
            if f64::abs(*d) < 1.0 {
                assert_within_delta!(f64::INFINITY, F64::abs(F64::pow_i64(*d, i64::MIN)), EXACT);
            }
        }
        // Note: i64.MAX_VALUE isn't actually an infinity, so its parity affects the sign of resulting infinity.
        for  d in DOUBLES.iter() {
            if f64::abs(*d) > 1.0 {
                assert!(f64::is_infinite(F64::pow_i64(*d, i64::MAX)));
            }
        }
        for  d in DOUBLES.iter() {
            if f64::abs(*d) < 1.0 {
                assert!(f64::is_infinite(F64::pow_i64(*d, i64::MIN + 1)));
            }
        }
        // the absolute value of the first argument is less than 1 and the second argument is positive infinity, then the result is positive zero.
        for  d in DOUBLES.iter() {
            if f64::abs(*d) > 1.0 {
                assert_within_delta!(0.0, F64::pow_i64(*d, i64::MIN), EXACT);
            }
        }
        for  d in DOUBLES.iter() {
            if f64::abs(*d) < 1.0 {
                assert_within_delta!(0.0, F64::pow_i64(*d, i64::MAX - 1), EXACT);
            }
        }
        // Note: i64.MAX_VALUE isn't actually an infinity, so its parity affects the sign of resulting zero.
        for  d in DOUBLES.iter() {
            if f64::abs(*d) > 1.0 {
                assert!(F64::pow_i64(*d, i64::MIN + 1) == 0.0);
            }
        }
        for  d in DOUBLES.iter() {
            if f64::abs(*d) < 1.0 {
                assert!(F64::pow_i64(*d, i64::MAX) == 0.0);
            }
        }
        // the first argument is positive infinity and the second argument is less than zero, then the result is positive zero.
        for  i in INTS.iter() {
            if *i > 0 {
                assert_within_delta!(0.0, F64::pow_i64(0.0, *i), EXACT);
            }
        }
        for  i in INTS.iter() {
            if *i < 0 {
                assert_within_delta!(0.0, F64::pow_i64(f64::INFINITY, *i), EXACT);
            }
        }
        // the first argument is positive infinity and the second argument is greater than zero, then the result is positive infinity.
        for  i in INTS.iter() {
            if *i < 0 {
                assert_within_delta!(f64::INFINITY, F64::pow_i64(0.0, *i), EXACT);
            }
        }
        for  i in INTS.iter() {
            if *i > 0 {
                assert_within_delta!(f64::INFINITY, F64::pow_i64(f64::INFINITY, *i), EXACT);
            }
        }
        // the first argument is negative infinity and the second argument is less than zero but not a finite odd integer, then the result is positive zero.
        for  i in INTS.iter() {
            if *i > 0 && (*i & 1) == 0 {
                assert_within_delta!(0.0, F64::pow_i64(-0.0, *i), EXACT);
            }
        }
        for  i in INTS.iter() {
            if *i < 0 && (*i & 1) == 0 {
                assert_within_delta!(0.0, F64::pow_i64(f64::NEG_INFINITY, *i), EXACT);
            }
        }
        // the first argument is negative infinity and the second argument is a negative finite odd integer, then the result is negative zero.
        for  i in INTS.iter() {
            if *i > 0 && (*i & 1) == 1 {
                assert_within_delta!(-0.0, F64::pow_i64(-0.0, *i), EXACT);
            }
        }
        for  i in INTS.iter() {
            if *i < 0 && (*i & 1) == 1 {
                assert_within_delta!(-0.0, F64::pow_i64(f64::NEG_INFINITY, *i), EXACT);
            }
        }
        // the first argument is negative infinity and the second argument is greater than zero but not a finite odd integer, then the result is positive infinity.
        for  i in INTS.iter() {
            if *i > 0 && (*i & 1) == 0 {
                assert_within_delta!(f64::INFINITY, F64::pow_i64(f64::NEG_INFINITY, *i), EXACT);
            }
        }        
        for  i in INTS.iter() {
            if *i < 0 && (*i & 1) == 0 {
                assert_within_delta!(f64::INFINITY, F64::pow_i64(-0.0, *i), EXACT);
            }
        }
        // the first argument is negative infinity and the second argument is a positive finite odd integer, then the result is negative infinity.
        for  i in INTS.iter() {
            if *i > 0 && (*i & 1) == 1 {
                assert_within_delta!(f64::NEG_INFINITY, F64::pow_i64(f64::NEG_INFINITY, *i), EXACT);
            }
        }
        for  i in INTS.iter() {
            if *i < 0 && (*i & 1) == 1 {
                assert_within_delta!(f64::NEG_INFINITY, F64::pow_i64(-0.0, *i), EXACT);
            }
        }
        for  d in DOUBLES.iter() {
            // If the first argument is finite and less than zero
            if *d < 0.0 && f64::abs(*d) <= f64::MAX {
                for  i in INTS.iter() {
                    // if the second argument is a finite even integer, the result is equal to the result of raising the absolute value of the first argument to the power of the second argument
                    if (*i & 1) == 0 {
                        assert_within_delta!(F64::pow_i64(-*d, *i), F64::pow_i64(*d, *i), EXACT);
                    }
                    else {// if the second argument is a finite odd integer, the result is equal to the negative of the result of raising the absolute value of the first argument to the power of the second argument
                        assert_within_delta!(-F64::pow_i64(-*d, *i), F64::pow_i64(*d, *i), EXACT);
                    }

                // if the second argument is finite and not an integer, then the result is NaN. <- Impossible with int.
                }
            }
        }
    // If both arguments are integers, then the result is exactly equal to the mathematical result of raising the first argument to the power
    // of the second argument if that result can in fact be represented exactly as a double value. <- Other tests.
    }
    
}
