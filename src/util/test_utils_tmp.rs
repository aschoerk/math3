/**
 */
pub struct TestUtils {
}

impl TestUtils {

    /**
     * Collection of static methods used in math unit tests.
     */
    fn new() -> TestUtils {
        super();
    }

    /**
     * Verifies that expected and actual are within delta, or are both NaN or
     * infinities of the same sign.
     */
    pub fn  assert_equals( expected: f64,  actual: f64,  delta: f64)   {
        Assert.assert_equals(null, &expected, &actual, &delta);
    }

    /**
     * Verifies that expected and actual are within delta, or are both NaN or
     * infinities of the same sign.
     */
    pub fn  assert_equals( msg: &String,  expected: f64,  actual: f64,  delta: f64)   {
        // check for NaN
        if Double.is_na_n(&expected) {
            Assert.assert_true("" + actual + " is not NaN.", &Double.is_na_n(&actual));
        } else {
            Assert.assert_equals(&msg, &expected, &actual, &delta);
        }
    }

    /**
     * Verifies that the two arguments are exactly the same, either
     * both NaN or infinities of same sign, or identical floating point values.
     */
    pub fn  assert_same( expected: f64,  actual: f64)   {
        Assert.assert_equals(&expected, &actual, 0);
    }

    /**
     * Verifies that real and imaginary parts of the two complex arguments
     * are exactly the same.  Also ensures that NaN / infinite components match.
     */
    pub fn  assert_same( expected: &Complex,  actual: &Complex)   {
        ::assert_same(&expected.get_real(), &actual.get_real());
        ::assert_same(&expected.get_imaginary(), &actual.get_imaginary());
    }

    /**
     * Verifies that real and imaginary parts of the two complex arguments
     * differ by at most delta.  Also ensures that NaN / infinite components match.
     */
    pub fn  assert_equals( expected: &Complex,  actual: &Complex,  delta: f64)   {
        Assert.assert_equals(&expected.get_real(), &actual.get_real(), &delta);
        Assert.assert_equals(&expected.get_imaginary(), &actual.get_imaginary(), &delta);
    }

    /**
     * Verifies that two double arrays have equal entries, up to tolerance
     */
    pub fn  assert_equals( expected: Vec<f64>,  observed: Vec<f64>,  tolerance: f64	) {
        ::assert_equals("Array comparison failure", &expected, &observed, &tolerance);
    }

    /**
     * Serializes an object to a bytes array and then recovers the object from the bytes array.
     * Returns the deserialized object.
     *
     * @param o  object to serialize and recover
     * @return  the recovered, deserialized object
     */
    pub fn  serialize_and_recover( o: &Object) -> Object  {
        try {
            // serialize the Object
             let bos: ByteArrayOutputStream = ByteArrayOutputStream::new();
             let so: ObjectOutputStream = ObjectOutputStream::new(&bos);
            so.write_object(&o);
            // deserialize the Object
             let bis: ByteArrayInputStream = ByteArrayInputStream::new(&bos.to_byte_array());
             let si: ObjectInputStream = ObjectInputStream::new(&bis);
            return si.read_object();
        } catch ( ioe: &IOException) {
            return null;
        } catch ( cnfe: &ClassNotFoundException) {
            return null;
        }
    }

    /**
     * Verifies that serialization preserves equals and hashCode.
     * Serializes the object, then recovers it and checks equals and hash code.
     *
     * @param object  the object to serialize and recover
     */
    pub fn  check_serialized_equality( object: &Object)   {
         let object2: Object = ::serialize_and_recover(&object);
        Assert.assert_equals("Equals check", &object, &object2);
        Assert.assert_equals("HashCode check", &object.hash_code(), &object2.hash_code());
    }

    /**
     * Verifies that the relative error in actual vs. expected is less than or
     * equal to relativeError.  If expected is infinite or NaN, actual must be
     * the same (NaN or infinity of the same sign).
     *
     * @param expected expected value
     * @param actual  observed value
     * @param relativeError  maximum allowable relative error
     */
    pub fn  assert_relatively_equals( expected: f64,  actual: f64,  relative_error: f64)   {
        ::assert_relatively_equals(null, &expected, &actual, &relative_error);
    }

    /**
     * Verifies that the relative error in actual vs. expected is less than or
     * equal to relativeError.  If expected is infinite or NaN, actual must be
     * the same (NaN or infinity of the same sign).
     *
     * @param msg  message to return with failure
     * @param expected expected value
     * @param actual  observed value
     * @param relativeError  maximum allowable relative error
     */
    pub fn  assert_relatively_equals( msg: &String,  expected: f64,  actual: f64,  relative_error: f64)   {
        if Double.is_na_n(&expected) {
            Assert.assert_true(&msg, &Double.is_na_n(&actual));
        } else if Double.is_na_n(&actual) {
            Assert.assert_true(&msg, &Double.is_na_n(&expected));
        } else if Double.is_infinite(&actual) || Double.is_infinite(&expected) {
            Assert.assert_equals(&expected, &actual, &relative_error);
        } else if expected == 0.0 {
            Assert.assert_equals(&msg, &actual, &expected, &relative_error);
        } else {
             let abs_error: f64 = FastMath.abs(&expected) * relative_error;
            Assert.assert_equals(&msg, &expected, &actual, &abs_error);
        }
    }

    /**
     * Fails iff values does not contain a number within epsilon of z.
     *
     * @param msg  message to return with failure
     * @param values complex array to search
     * @param z  value sought
     * @param epsilon  tolerance
     */
    pub fn  assert_contains( msg: &String,  values: &Complex[],  z: &Complex,  epsilon: f64)   {
        for  let value: Complex in values {
            if Precision.equals(&value.get_real(), &z.get_real(), &epsilon) && Precision.equals(&value.get_imaginary(), &z.get_imaginary(), &epsilon) {
                return;
            }
        }
        Assert.fail(msg + " Unable to find " + (ComplexFormat::new()).format(&z));
    }

    /**
     * Fails iff values does not contain a number within epsilon of z.
     *
     * @param values complex array to search
     * @param z  value sought
     * @param epsilon  tolerance
     */
    pub fn  assert_contains( values: &Complex[],  z: &Complex,  epsilon: f64)   {
        ::assert_contains(null, &values, &z, &epsilon);
    }

    /**
     * Fails iff values does not contain a number within epsilon of x.
     *
     * @param msg  message to return with failure
     * @param values double array to search
     * @param x value sought
     * @param epsilon  tolerance
     */
    pub fn  assert_contains( msg: &String,  values: &f64[],  x: f64,  epsilon: f64)   {
        for  let value: f64 in values {
            if Precision.equals(&value, &x, &epsilon) {
                return;
            }
        }
        Assert.fail(msg + " Unable to find " + x);
    }

    /**
     * Fails iff values does not contain a number within epsilon of x.
     *
     * @param values double array to search
     * @param x value sought
     * @param epsilon  tolerance
     */
    pub fn  assert_contains( values: &f64[],  x: f64,  epsilon: f64)   {
        ::assert_contains(null, &values, &x, &epsilon);
    }

    /**
     * Asserts that all entries of the specified vectors are equal to within a
     * positive {@code delta}.
     *
     * @param message the identifying message for the assertion error (can be
     * {@code null})
     * @param expected expected value
     * @param actual actual value
     * @param delta the maximum difference between the entries of the expected
     * and actual vectors for which both entries are still considered equal
     */
    pub fn  assert_equals( message: &String,  expected: &f64[],  actual: &RealVector,  delta: f64)   {
         let msg_and_sep: String =  if message.equals("") { "" } else { message + ", " };
        Assert.assert_equals(msg_and_sep + "dimension", expected.length, &actual.get_dimension());
         {
             let mut i: i32 = 0;
            while i < expected.length {
                {
                    Assert.assert_equals(msg_and_sep + "entry #" + i, expected[i], &actual.get_entry(&i), &delta);
                }
                i += 1 !!!check!!! post increment;
             }
         }

    }

    /**
     * Asserts that all entries of the specified vectors are equal to within a
     * positive {@code delta}.
     *
     * @param message the identifying message for the assertion error (can be
     * {@code null})
     * @param expected expected value
     * @param actual actual value
     * @param delta the maximum difference between the entries of the expected
     * and actual vectors for which both entries are still considered equal
     */
    pub fn  assert_equals( message: &String,  expected: &RealVector,  actual: &RealVector,  delta: f64)   {
         let msg_and_sep: String =  if message.equals("") { "" } else { message + ", " };
        Assert.assert_equals(msg_and_sep + "dimension", &expected.get_dimension(), &actual.get_dimension());
         let dim: i32 = expected.get_dimension();
         {
             let mut i: i32 = 0;
            while i < dim {
                {
                    Assert.assert_equals(msg_and_sep + "entry #" + i, &expected.get_entry(&i), &actual.get_entry(&i), &delta);
                }
                i += 1 !!!check!!! post increment;
             }
         }

    }

    /** verifies that two matrices are close (1-norm) */
    pub fn  assert_equals( msg: &String,  expected: &RealMatrix,  observed: &RealMatrix,  tolerance: f64)   {
        Assert.assert_not_null(msg + "\nObserved should not be null", &observed);
        if expected.get_column_dimension() != observed.get_column_dimension() || expected.get_row_dimension() != observed.get_row_dimension() {
             let message_buffer: StringBuilder = StringBuilder::new(&msg);
            message_buffer.append("\nObserved has incorrect dimensions.");
            message_buffer.append("\nobserved is " + observed.get_row_dimension() + " x " + observed.get_column_dimension());
            message_buffer.append("\nexpected " + expected.get_row_dimension() + " x " + expected.get_column_dimension());
            Assert.fail(&message_buffer.to_string());
        }
         let delta: RealMatrix = expected.subtract(&observed);
        if delta.get_norm() >= tolerance {
             let message_buffer: StringBuilder = StringBuilder::new(&msg);
            message_buffer.append("\nExpected: " + expected);
            message_buffer.append("\nObserved: " + observed);
            message_buffer.append("\nexpected - observed: " + delta);
            Assert.fail(&message_buffer.to_string());
        }
    }

    /** verifies that two matrices are equal */
    pub fn  assert_equals( expected: &FieldMatrix<? extends FieldElement<?>>,  observed: &FieldMatrix<? extends FieldElement<?>>)   {
        Assert.assert_not_null("Observed should not be null", &observed);
        if expected.get_column_dimension() != observed.get_column_dimension() || expected.get_row_dimension() != observed.get_row_dimension() {
             let message_buffer: StringBuilder = StringBuilder::new();
            message_buffer.append("Observed has incorrect dimensions.");
            message_buffer.append("\nobserved is " + observed.get_row_dimension() + " x " + observed.get_column_dimension());
            message_buffer.append("\nexpected " + expected.get_row_dimension() + " x " + expected.get_column_dimension());
            Assert.fail(&message_buffer.to_string());
        }
         {
             let mut i: i32 = 0;
            while i < expected.get_row_dimension() {
                {
                     {
                         let mut j: i32 = 0;
                        while j < expected.get_column_dimension() {
                            {
                                 let eij: FieldElement<?> = expected.get_entry(&i, &j);
                                 let oij: FieldElement<?> = observed.get_entry(&i, &j);
                                Assert.assert_equals(&eij, &oij);
                            }
                            j += 1;
                         }
                     }

                }
                i += 1;
             }
         }

    }

    /** verifies that two arrays are close (sup norm) */
    pub fn  assert_equals( msg: &String,  expected: &f64[],  observed: &f64[],  tolerance: f64)   {
         let out: StringBuilder = StringBuilder::new(&msg);
        if expected.length != observed.length {
            out.append("\n Arrays not same length. \n");
            out.append("expected has length ");
            out.append(expected.length);
            out.append(" observed length = ");
            out.append(observed.length);
            Assert.fail(&out.to_string());
        }
         let mut failure: bool = false;
         {
             let mut i: i32 = 0;
            while i < expected.length {
                {
                    if !Precision.equals_including_na_n(expected[i], observed[i], &tolerance) {
                        failure = true;
                        out.append("\n Elements at index ");
                        out.append(&i);
                        out.append(" differ. ");
                        out.append(" expected = ");
                        out.append(expected[i]);
                        out.append(" observed = ");
                        out.append(observed[i]);
                    }
                }
                i += 1 !!!check!!! post increment;
             }
         }

        if failure {
            Assert.fail(&out.to_string());
        }
    }

    /** verifies that two arrays are equal */
    pub fn <T extends FieldElement<T>>  assert_equals( m: &T[],  n: &T[])   {
        if m.length != n.length {
            Assert.fail("vectors not same length");
        }
         {
             let mut i: i32 = 0;
            while i < m.length {
                {
                    Assert.assert_equals(m[i], n[i]);
                }
                i += 1 !!!check!!! post increment;
             }
         }

    }

    /**
     * Computes the sum of squared deviations of <values> from <target>
     * @param values array of deviates
     * @param target value to compute deviations from
     *
     * @return sum of squared deviations
     */
    pub fn  sum_square_dev( values: &f64[],  target: f64) -> f64  {
         let mut sumsq: f64 = 0d.0;
         {
             let mut i: i32 = 0;
            while i < values.length {
                {
                     let dev: f64 = values[i] - target;
                    sumsq += (dev * dev);
                }
                i += 1 !!!check!!! post increment;
             }
         }

        return sumsq;
    }

    /**
     * Asserts the null hypothesis for a ChiSquare test.  Fails and dumps arguments and test
     * statistics if the null hypothesis can be rejected with confidence 100 * (1 - alpha)%
     *
     * @param valueLabels labels for the values of the discrete distribution under test
     * @param expected expected counts
     * @param observed observed counts
     * @param alpha significance level of the test
     */
    pub fn  assert_chi_square_accept( value_labels: &String[],  expected: &f64[],  observed: &i64[],  alpha: f64)   {
         let chi_square_test: ChiSquareTest = ChiSquareTest::new();
        // Fail if we can reject null hypothesis that distributions are the same
        if chi_square_test.chi_square_test(&expected, &observed, &alpha) {
             let msg_buffer: StringBuilder = StringBuilder::new();
             let df: DecimalFormat = DecimalFormat::new("#.##");
            msg_buffer.append("Chisquare test failed");
            msg_buffer.append(" p-value = ");
            msg_buffer.append(&chi_square_test.chi_square_test(&expected, &observed));
            msg_buffer.append(" chisquare statistic = ");
            msg_buffer.append(&chi_square_test.chi_square(&expected, &observed));
            msg_buffer.append(". \n");
            msg_buffer.append("value\texpected\tobserved\n");
             {
                 let mut i: i32 = 0;
                while i < expected.length {
                    {
                        msg_buffer.append(value_labels[i]);
                        msg_buffer.append("\t");
                        msg_buffer.append(&df.format(expected[i]));
                        msg_buffer.append("\t\t");
                        msg_buffer.append(observed[i]);
                        msg_buffer.append("\n");
                    }
                    i += 1 !!!check!!! post increment;
                 }
             }

            msg_buffer.append("This test can fail randomly due to sampling error with probability ");
            msg_buffer.append(&alpha);
            msg_buffer.append(".");
            Assert.fail(&msg_buffer.to_string());
        }
    }

    /**
     * Asserts the null hypothesis for a ChiSquare test.  Fails and dumps arguments and test
     * statistics if the null hypothesis can be rejected with confidence 100 * (1 - alpha)%
     *
     * @param values integer values whose observed and expected counts are being compared
     * @param expected expected counts
     * @param observed observed counts
     * @param alpha significance level of the test
     */
    pub fn  assert_chi_square_accept( values: &i32[],  expected: &f64[],  observed: &i64[],  alpha: f64)   {
         let mut labels: [Option<String>; values.length] = [None; values.length];
         {
             let mut i: i32 = 0;
            while i < values.length {
                {
                    labels[i] = Integer.to_string(values[i]);
                }
                i += 1 !!!check!!! post increment;
             }
         }

        ::assert_chi_square_accept(&labels, &expected, &observed, &alpha);
    }

    /**
     * Asserts the null hypothesis for a ChiSquare test.  Fails and dumps arguments and test
     * statistics if the null hypothesis can be rejected with confidence 100 * (1 - alpha)%
     *
     * @param expected expected counts
     * @param observed observed counts
     * @param alpha significance level of the test
     */
    pub fn  assert_chi_square_accept( expected: &f64[],  observed: &i64[],  alpha: f64)   {
         let mut labels: [Option<String>; expected.length] = [None; expected.length];
         {
             let mut i: i32 = 0;
            while i < labels.length {
                {
                    labels[i] = Integer.to_string(i + 1);
                }
                i += 1 !!!check!!! post increment;
             }
         }

        ::assert_chi_square_accept(&labels, &expected, &observed, &alpha);
    }

    /**
     * Computes the 25th, 50th and 75th percentiles of the given distribution and returns
     * these values in an array.
     */
    pub fn  get_distribution_quartiles( distribution: &RealDistribution) -> f64[]  {
         let mut quantiles: [f64; 3] = [0.0; 3];
        quantiles[0] = distribution.inverse_cumulative_probability(0.25);
        quantiles[1] = distribution.inverse_cumulative_probability(0.5);
        quantiles[2] = distribution.inverse_cumulative_probability(0.75);
        return quantiles;
    }

    /**
     * Updates observed counts of values in quartiles.
     * counts[0] <-> 1st quartile ... counts[3] <-> top quartile
     */
    pub fn  update_counts( value: f64,  counts: &i64[],  quartiles: &f64[])   {
        if value < quartiles[0] {
            counts[0] += 1 !!!check!!! post increment;
        } else if value > quartiles[2] {
            counts[3] += 1 !!!check!!! post increment;
        } else if value > quartiles[1] {
            counts[2] += 1 !!!check!!! post increment;
        } else {
            counts[1] += 1 !!!check!!! post increment;
        }
    }

    /**
     * Eliminates points with zero mass from densityPoints and densityValues parallel
     * arrays.  Returns the number of positive mass points and collapses the arrays so
     * that the first <returned value> elements of the input arrays represent the positive
     * mass points.
     */
    pub fn  eliminate_zero_mass_points( density_points: &i32[],  density_values: &f64[]) -> i32  {
         let positive_mass_count: i32 = 0;
         {
             let mut i: i32 = 0;
            while i < density_values.length {
                {
                    if density_values[i] > 0 {
                        positive_mass_count += 1 !!!check!!! post increment;
                    }
                }
                i += 1 !!!check!!! post increment;
             }
         }

        if positive_mass_count < density_values.length {
             let new_points: [i32; positive_mass_count] = [0; positive_mass_count];
             let new_values: [f64; positive_mass_count] = [0.0; positive_mass_count];
             let mut j: i32 = 0;
             {
                 let mut i: i32 = 0;
                while i < density_values.length {
                    {
                        if density_values[i] > 0 {
                            new_points[j] = density_points[i];
                            new_values[j] = density_values[i];
                            j += 1 !!!check!!! post increment;
                        }
                    }
                    i += 1 !!!check!!! post increment;
                 }
             }

            System.arraycopy(&new_points, 0, &density_points, 0, &positive_mass_count);
            System.arraycopy(&new_values, 0, &density_values, 0, &positive_mass_count);
        }
        return positive_mass_count;
    }
}
