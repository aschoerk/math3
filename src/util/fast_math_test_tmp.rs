                
                   /*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org::apache::commons::math3::util;

use java::lang::reflect::Method;
use java::lang::reflect::Modifier;
use java::lang::reflect::Type;
use java::math::BigDecimal;
use java::math::BigInteger;
use java::math::RoundingMode;
use org::apache::commons::math3::TestUtils;
use org::apache::commons::math3::dfp::Dfp;
use org::apache::commons::math3::dfp::DfpField;
use org::apache::commons::math3::dfp::DfpMath;
use org::apache::commons::math3::exception::MathArithmeticException;
use org::apache::commons::math3::random::MersenneTwister;
use org::apache::commons::math3::random::RandomGenerator;
use org::apache::commons::math3::random::Well1024a;
use org::junit::Assert;
use org::junit::Before;
use org::junit::Ignore;
use org::junit::Test;


 const MAX_ERROR_ULP: f64 = 0.51;

 const NUMBER_OF_TRIALS: i32 = 1000;
pub struct FastMathTest {

     let mut field: DfpField;

     let mut generator: RandomGenerator;
}

impl FastMathTest {

    pub fn  set_up()   {
        field = DfpField::new(40);
        generator = MersenneTwister::new(6176597458463500194);
    }


    #[test]
    pub fn  test_increment_exact_int()   {
         let special_values : [i32; 33] = [i32::MIN, i32::MIN + 1, i32::MIN + 2, i32::MAX, i32::MAX - 1, i32::MAX - 2, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -1 - (i32::MIN / 2), 0 - (i32::MIN / 2), 1 - (i32::MIN / 2), -1 + (i32::MAX / 2), 0 + (i32::MAX / 2), 1 + (i32::MAX / 2), ]
        ;
        for  let a: i32 in special_values {
             let bd_a: BigInteger = BigInteger.value_of(&a);
             let bd_sum: BigInteger = bd_a.add(Bigi32::ONE);
            if bd_sum.compare_to(&BigInteger.value_of(i32::MIN)) < 0 || bd_sum.compare_to(&BigInteger.value_of(i32::MAX)) > 0 {
                try {
                    F64::increment_exact(&a);
                    Assert.fail("an exception should have been thrown");
                } catch ( mae: &MathArithmeticException) {
                }
            } else {
                assert_within_delta!(&bd_sum, &BigInteger.value_of(F64::increment_exact(&a)));
            }
        }
    }

    #[test]
    pub fn  test_decrement_exact_int()   {
         let special_values : [i32; 33] = [i32::MIN, i32::MIN + 1, i32::MIN + 2, i32::MAX, i32::MAX - 1, i32::MAX - 2, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -1 - (i32::MIN / 2), 0 - (i32::MIN / 2), 1 - (i32::MIN / 2), -1 + (i32::MAX / 2), 0 + (i32::MAX / 2), 1 + (i32::MAX / 2), ]
        ;
        for  let a: i32 in special_values {
             let bd_a: BigInteger = BigInteger.value_of(&a);
             let bd_sub: BigInteger = bd_a.subtract(Bigi32::ONE);
            if bd_sub.compare_to(&BigInteger.value_of(i32::MIN)) < 0 || bd_sub.compare_to(&BigInteger.value_of(i32::MAX)) > 0 {
                try {
                    F64::decrement_exact(&a);
                    Assert.fail("an exception should have been thrown");
                } catch ( mae: &MathArithmeticException) {
                }
            } else {
                assert_within_delta!(&bd_sub, &BigInteger.value_of(F64::decrement_exact(&a)));
            }
        }
    }

    #[test]
    pub fn  test_add_exact_int()   {
         let special_values : [i32; 33] = [i32::MIN, i32::MIN + 1, i32::MIN + 2, i32::MAX, i32::MAX - 1, i32::MAX - 2, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -1 - (i32::MIN / 2), 0 - (i32::MIN / 2), 1 - (i32::MIN / 2), -1 + (i32::MAX / 2), 0 + (i32::MAX / 2), 1 + (i32::MAX / 2), ]
        ;
        for  let a: i32 in special_values {
            for  let b: i32 in special_values {
                 let bd_a: BigInteger = BigInteger.value_of(&a);
                 let bd_b: BigInteger = BigInteger.value_of(&b);
                 let bd_sum: BigInteger = bd_a.add(&bd_b);
                if bd_sum.compare_to(&BigInteger.value_of(i32::MIN)) < 0 || bd_sum.compare_to(&BigInteger.value_of(i32::MAX)) > 0 {
                    try {
                        F64::add_exact(&a, &b);
                        Assert.fail("an exception should have been thrown");
                    } catch ( mae: &MathArithmeticException) {
                    }
                } else {
                    assert_within_delta!(&bd_sum, &BigInteger.value_of(F64::add_exact(&a, &b)));
                }
            }
        }
    }

    #[test]
    pub fn  test_add_exact_long()   {
         let special_values : [i64; 33] = [Long::MIN, Long::MIN + 1, Long::MIN + 2, Long::MAX, Long::MAX - 1, Long::MAX - 2, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -1 - (Long::MIN / 2), 0 - (Long::MIN / 2), 1 - (Long::MIN / 2), -1 + (Long::MAX / 2), 0 + (Long::MAX / 2), 1 + (Long::MAX / 2), ]
        ;
        for  let a: i64 in special_values {
            for  let b: i64 in special_values {
                 let bd_a: BigInteger = BigInteger.value_of(&a);
                 let bd_b: BigInteger = BigInteger.value_of(&b);
                 let bd_sum: BigInteger = bd_a.add(&bd_b);
                if bd_sum.compare_to(&BigInteger.value_of(Long::MIN)) < 0 || bd_sum.compare_to(&BigInteger.value_of(Long::MAX)) > 0 {
                    try {
                        F64::add_exact(&a, &b);
                        Assert.fail("an exception should have been thrown");
                    } catch ( mae: &MathArithmeticException) {
                    }
                } else {
                    assert_within_delta!(&bd_sum, &BigInteger.value_of(F64::add_exact(&a, &b)));
                }
            }
        }
    }

    #[test]
    pub fn  test_subtract_exact_int()   {
         let special_values : [i32; 33] = [i32::MIN, i32::MIN + 1, i32::MIN + 2, i32::MAX, i32::MAX - 1, i32::MAX - 2, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -1 - (i32::MIN / 2), 0 - (i32::MIN / 2), 1 - (i32::MIN / 2), -1 + (i32::MAX / 2), 0 + (i32::MAX / 2), 1 + (i32::MAX / 2), ]
        ;
        for  let a: i32 in special_values {
            for  let b: i32 in special_values {
                 let bd_a: BigInteger = BigInteger.value_of(&a);
                 let bd_b: BigInteger = BigInteger.value_of(&b);
                 let bd_sub: BigInteger = bd_a.subtract(&bd_b);
                if bd_sub.compare_to(&BigInteger.value_of(i32::MIN)) < 0 || bd_sub.compare_to(&BigInteger.value_of(i32::MAX)) > 0 {
                    try {
                        F64::subtract_exact(&a, &b);
                        Assert.fail("an exception should have been thrown");
                    } catch ( mae: &MathArithmeticException) {
                    }
                } else {
                    assert_within_delta!(&bd_sub, &BigInteger.value_of(F64::subtract_exact(&a, &b)));
                }
            }
        }
    }

    #[test]
    pub fn  test_subtract_exact_long()   {
         let special_values : [i64; 33] = [Long::MIN, Long::MIN + 1, Long::MIN + 2, Long::MAX, Long::MAX - 1, Long::MAX - 2, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -1 - (Long::MIN / 2), 0 - (Long::MIN / 2), 1 - (Long::MIN / 2), -1 + (Long::MAX / 2), 0 + (Long::MAX / 2), 1 + (Long::MAX / 2), ]
        ;
        for  let a: i64 in special_values {
            for  let b: i64 in special_values {
                 let bd_a: BigInteger = BigInteger.value_of(&a);
                 let bd_b: BigInteger = BigInteger.value_of(&b);
                 let bd_sub: BigInteger = bd_a.subtract(&bd_b);
                if bd_sub.compare_to(&BigInteger.value_of(Long::MIN)) < 0 || bd_sub.compare_to(&BigInteger.value_of(Long::MAX)) > 0 {
                    try {
                        F64::subtract_exact(&a, &b);
                        Assert.fail("an exception should have been thrown");
                    } catch ( mae: &MathArithmeticException) {
                    }
                } else {
                    assert_within_delta!(&bd_sub, &BigInteger.value_of(F64::subtract_exact(&a, &b)));
                }
            }
        }
    }

    #[test]
    pub fn  test_multiply_exact_int()   {
         let special_values : [i32; 33] = [i32::MIN, i32::MIN + 1, i32::MIN + 2, i32::MAX, i32::MAX - 1, i32::MAX - 2, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -1 - (i32::MIN / 2), 0 - (i32::MIN / 2), 1 - (i32::MIN / 2), -1 + (i32::MAX / 2), 0 + (i32::MAX / 2), 1 + (i32::MAX / 2), ]
        ;
        for  let a: i32 in special_values {
            for  let b: i32 in special_values {
                 let bd_a: BigInteger = BigInteger.value_of(&a);
                 let bd_b: BigInteger = BigInteger.value_of(&b);
                 let bd_mul: BigInteger = bd_a.multiply(&bd_b);
                if bd_mul.compare_to(&BigInteger.value_of(i32::MIN)) < 0 || bd_mul.compare_to(&BigInteger.value_of(i32::MAX)) > 0 {
                    try {
                        F64::multiply_exact(&a, &b);
                        Assert.fail("an exception should have been thrown " + a + b);
                    } catch ( mae: &MathArithmeticException) {
                    }
                } else {
                    assert_within_delta!(&bd_mul, &BigInteger.value_of(F64::multiply_exact(&a, &b)));
                }
            }
        }
    }

    #[test]
    pub fn  test_multiply_exact_long()   {
         let special_values : [i64; 33] = [Long::MIN, Long::MIN + 1, Long::MIN + 2, Long::MAX, Long::MAX - 1, Long::MAX - 2, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -1 - (Long::MIN / 2), 0 - (Long::MIN / 2), 1 - (Long::MIN / 2), -1 + (Long::MAX / 2), 0 + (Long::MAX / 2), 1 + (Long::MAX / 2), ]
        ;
        for  let a: i64 in special_values {
            for  let b: i64 in special_values {
                 let bd_a: BigInteger = BigInteger.value_of(&a);
                 let bd_b: BigInteger = BigInteger.value_of(&b);
                 let bd_mul: BigInteger = bd_a.multiply(&bd_b);
                if bd_mul.compare_to(&BigInteger.value_of(Long::MIN)) < 0 || bd_mul.compare_to(&BigInteger.value_of(Long::MAX)) > 0 {
                    try {
                        F64::multiply_exact(&a, &b);
                        Assert.fail("an exception should have been thrown " + a + b);
                    } catch ( mae: &MathArithmeticException) {
                    }
                } else {
                    assert_within_delta!(&bd_mul, &BigInteger.value_of(F64::multiply_exact(&a, &b)));
                }
            }
        }
    }

    #[test]
    pub fn  test_to_int_exact_too_low()   {
        F64::to_int_exact(-1 + i32::MIN);
    }

    #[test]
    pub fn  test_to_int_exact_too_high()   {
        F64::to_int_exact(1 + i32::MAX);
    }

    #[test]
    pub fn  test_to_int_exact()   {
         {
             let mut n: i32 = -1000;
            while n < 1000 {
                {
                    assert_within_delta!(&n, F64::to_int_exact(0 + n));
                }
                n += 1;
             }
         }

        assert_within_delta!(i32::MIN, F64::to_int_exact(0 + i32::MIN));
        assert_within_delta!(i32::MAX, F64::to_int_exact(0 + i32::MAX));
    }

    #[test]
    pub fn  test_floor_div_int()   {
        assert_within_delta!(1, F64::floor_div(4, 3));
        assert_within_delta!(-2, F64::floor_div(-4, 3));
        assert_within_delta!(-2, F64::floor_div(4, -3));
        assert_within_delta!(1, F64::floor_div(-4, -3));
        try {
            F64::floor_div(1, 0);
            Assert.fail("an exception should have been thrown");
        } catch ( mae: &MathArithmeticException) {
        }
         {
             let mut a: i32 = -100;
            while a <= 100 {
                {
                     {
                         let mut b: i32 = -100;
                        while b <= 100 {
                            {
                                if b != 0 {
                                    assert_within_delta!(&self.poor_man_floor_div(&a, &b), F64::floor_div(&a, &b));
                                }
                            }
                            b += 1;
                         }
                     }

                }
                a += 1;
             }
         }

    }

    #[test]
    pub fn  test_floor_mod_int()   {
        assert_within_delta!(1, F64::floor_mod(4, 3));
        assert_within_delta!(2, F64::floor_mod(-4, 3));
        assert_within_delta!(-2, F64::floor_mod(4, -3));
        assert_within_delta!(-1, F64::floor_mod(-4, -3));
        try {
            F64::floor_mod(1, 0);
            Assert.fail("an exception should have been thrown");
        } catch ( mae: &MathArithmeticException) {
        }
         {
             let mut a: i32 = -100;
            while a <= 100 {
                {
                     {
                         let mut b: i32 = -100;
                        while b <= 100 {
                            {
                                if b != 0 {
                                    assert_within_delta!(&self.poor_man_floor_mod(&a, &b), F64::floor_mod(&a, &b));
                                }
                            }
                            b += 1;
                         }
                     }

                }
                a += 1;
             }
         }

    }

    #[test]
    pub fn  test_floor_div_mod_int()   {
         let generator: RandomGenerator = Well1024a::new(0x7ccab45edeaab90a);
         {
             let mut i: i32 = 0;
            while i < 10000 {
                {
                     let a: i32 = generator.next_int();
                     let b: i32 = generator.next_int();
                    if b == 0 {
                        try {
                            F64::floor_div(&a, &b);
                            Assert.fail("an exception should have been thrown");
                        } catch ( mae: &MathArithmeticException) {
                        }
                    } else {
                         let d: i32 = F64::floor_div(&a, &b);
                         let m: i32 = F64::floor_mod(&a, &b);
                        assert_within_delta!(F64::to_int_exact(&self.poor_man_floor_div(&a, &b)), &d);
                        assert_within_delta!(F64::to_int_exact(&self.poor_man_floor_mod(&a, &b)), &m);
                        assert_within_delta!(&a, d * b + m);
                        if b < 0 {
                            assert!(m <= 0);
                            assert!(-m < -b);
                        } else {
                            assert!(m >= 0);
                            assert!(m < b);
                        }
                    }
                }
                i += 1;
             }
         }

    }

    #[test]
    pub fn  test_floor_div_long()   {
        assert_within_delta!(1, F64::floor_div(4, 3));
        assert_within_delta!(-2, F64::floor_div(-4, 3));
        assert_within_delta!(-2, F64::floor_div(4, -3));
        assert_within_delta!(1, F64::floor_div(-4, -3));
        try {
            F64::floor_div(1, 0);
            Assert.fail("an exception should have been thrown");
        } catch ( mae: &MathArithmeticException) {
        }
         {
             let mut a: i64 = -100;
            while a <= 100 {
                {
                     {
                         let mut b: i64 = -100;
                        while b <= 100 {
                            {
                                if b != 0 {
                                    assert_within_delta!(&self.poor_man_floor_div(&a, &b), F64::floor_div(&a, &b));
                                }
                            }
                            b += 1;
                         }
                     }

                }
                a += 1;
             }
         }

    }

    #[test]
    pub fn  test_floor_mod_long()   {
        assert_within_delta!(1, F64::floor_mod(4, 3));
        assert_within_delta!(2, F64::floor_mod(-4, 3));
        assert_within_delta!(-2, F64::floor_mod(4, -3));
        assert_within_delta!(-1, F64::floor_mod(-4, -3));
        try {
            F64::floor_mod(1, 0);
            Assert.fail("an exception should have been thrown");
        } catch ( mae: &MathArithmeticException) {
        }
         {
             let mut a: i64 = -100;
            while a <= 100 {
                {
                     {
                         let mut b: i64 = -100;
                        while b <= 100 {
                            {
                                if b != 0 {
                                    assert_within_delta!(&self.poor_man_floor_mod(&a, &b), F64::floor_mod(&a, &b));
                                }
                            }
                            b += 1;
                         }
                     }

                }
                a += 1;
             }
         }

    }

    #[test]
    pub fn  test_floor_div_mod_long()   {
         let generator: RandomGenerator = Well1024a::new(0xb87b9bc14c96ccd5);
         {
             let mut i: i32 = 0;
            while i < 10000 {
                {
                     let a: i64 = generator.next_long();
                     let b: i64 = generator.next_long();
                    if b == 0 {
                        try {
                            F64::floor_div(&a, &b);
                            Assert.fail("an exception should have been thrown");
                        } catch ( mae: &MathArithmeticException) {
                        }
                    } else {
                         let d: i64 = F64::floor_div(&a, &b);
                         let m: i64 = F64::floor_mod(&a, &b);
                        assert_within_delta!(&self.poor_man_floor_div(&a, &b), &d);
                        assert_within_delta!(&self.poor_man_floor_mod(&a, &b), &m);
                        assert_within_delta!(&a, d * b + m);
                        if b < 0 {
                            assert!(m <= 0);
                            assert!(-m < -b);
                        } else {
                            assert!(m >= 0);
                            assert!(m < b);
                        }
                    }
                }
                i += 1;
             }
         }

    }

    fn  poor_man_floor_div(a: i64,  b: i64) -> i64  {
        // find q0, r0 such that a = q0 b + r0
         let q0: BigInteger = BigInteger.value_of(a / b);
         let r0: BigInteger = BigInteger.value_of(a % b);
         let mut fd: BigInteger = BigInteger.value_of(i32::MIN);
         let big_b: BigInteger = BigInteger.value_of(&b);
         {
             let mut k: i32 = -2;
            while k < 2 {
                {
                    // find another pair q, r such that a = q b + r
                     let big_k: BigInteger = BigInteger.value_of(&k);
                     let q: BigInteger = q0.subtract(&big_k);
                     let r: BigInteger = r0.add(&big_k.multiply(&big_b));
                    if r.abs().compare_to(&big_b.abs()) < 0 && (r.long_value() == 0 || ((r.long_value() ^ b) & 0x8000000000000000) == 0) {
                        if fd.compare_to(&q) < 0 {
                            fd = q;
                        }
                    }
                }
                k += 1;
             }
         }

        return fd.long_value();
    }

    fn  poor_man_floor_mod(a: i64,  b: i64) -> i64  {
        return a - b * self.poor_man_floor_div(&a, &b);
    }
}


                
                