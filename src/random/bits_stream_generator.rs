
// Licensed to the Apache Software Foundation (ASF) under one or more
// contributor license agreements.  See the NOTICE file distributed with
// this work for additional information regarding copyright ownership.
// The ASF licenses this file to You under the Apache License, Version 2.0
// (the "License"); you may not use this file except in compliance with
// the License.  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

use std::f64;
use util::fastmath::*;
use util::rsutils::*;
use std::rc::*;
use java::exc::*;
/** Base class for random number generators that generates bits streams.
 *
 * @since 2.0
 */

pub trait BitsAdvancingGenerator {
    /** Generate next pseudorandom number.
     * <p>This method is the core generation algorithm. It is used by all the
     * public generation methods for the various primitive types {@link
     * #nextBoolean()}, {@link #nextBytes(byte[])}, {@link #nextDouble()},
     * {@link #nextFloat()}, {@link #nextGaussian()}, {@link #nextInt()},
     * {@link #next(int)} and {@link #nextLong()}.</p>
     * @param bits number of random bits to produce
     * @return random bits generated
     */
    fn next(&mut self, bits: i32) -> i32;
}


pub trait BitsStreamGenerator: BitsAdvancingGenerator + GaussianManager {
    fn set_seed_i32(&mut self, seed: &i32) {
        let mut param = Vec::new();
        param.push(*seed);
        self.set_seed_i32a(&param);
    }

    /** {@inheritDoc} */
    fn set_seed_i64(&mut self, seed: &i64) {
        let mut param = Vec::new();
        let mut seed_high = seed >> 32;
        seed_high &= 0xFFFFFFFF as i64;
        param.push(seed_high as i32);
        param.push((seed & 0xffffffff) as i32);
        self.set_seed_i32a(&param);
    }

    /** {@inheritDoc} */
    fn set_seed_i32a(&mut self, seed: &Vec<i32>);


    /** {@inheritDoc} */
    fn next_boolean(&mut self) -> bool {
        return self.next(1) != 0;
    }

    /** {@inheritDoc} */
    fn next_double(&mut self) -> f64 {
        let high: i64 = (self.next(26) as i64) << 26;
        let low: i32 = self.next(26);
        let factor = long_bits_to_double(&0x3cb0000000000000);
        return (high | low as i64) as f64 * factor;
    }

    /** {@inheritDoc} */
    fn next_float(&mut self) -> f32 {
        let factor = long_bits_to_double(&0x3e80000000000000);
        return self.next(23) as f32 * factor as f32;
    }

    /** {@inheritDoc} */
    fn next_gaussian(&mut self) -> f64 {
        let random: f64;
        if f64::is_nan(self.get_next_gaussian()) {
            // generate a new pair of gaussian numbers
            let x: f64 = self.next_double();
            let y: f64 = self.next_double();
            let alpha: f64 = 2.0 * PI * x;
            let r: f64 = F64::sqrt(-2.0 * F64::log(y));
            random = r * F64::cos(alpha);
            self.set_next_gaussian(r * F64::sin(alpha));
        } else {
            // use the second element of the pair already generated
            random = self.get_next_gaussian();
            self.set_next_gaussian(f64::NAN);
        }
        return random;
    }

    /** {@inheritDoc} */
    fn next_int(&mut self) -> i32 {
        return self.next(32);
    }

    /**
     * {@inheritDoc}
     * <p>This default implementation is copied from Apache Harmony
     * java.util.Random (r929253).</p>
     *
     * <p>Implementation notes: <ul>
     * <li>If n is a power of 2, this method returns
     * {@code (int) ((n * (long) next(31)) >> 31)}.</li>
     *
     * <li>If n is not a power of 2, what is returned is {@code next(31) % n}
     * with {@code next(31)} values rejected (i.e. regenerated) until a
     * value that is larger than the remainder of {@code Integer.MAX_VALUE / n}
     * is generated. Rejection of this initial segment is necessary to ensure
     * a uniform distribution.</li></ul></p>
     */
    fn next_int_i32(&mut self, n: i32) -> Result<i32, Rc<Exception>> {
        if n > 0 {
            if (n & -n) == n {
                return Ok(((n as i64 * self.next(31) as i64) >> 31) as i32);
            }
            let mut bits: i32;
            let mut val: i32;
            loop {
                {
                    bits = self.next(31);
                    val = bits % n;
                }
                if !(bits - val + (n - 1) < 0) {
                    break;
                }
            }
            return Ok(val);
        }
        Err(Exc::new("NotStrictlyPositive"))
    }

    /** {@inheritDoc} */
    fn next_long(&mut self) -> i64 {
        let high: i64 = (self.next(32) as i64) << 32;
        let low: i64 = (self.next(32) as i64) & 0xffffffff;
        return high | low;
    }

    /**
     * Returns a pseudorandom, uniformly distributed {@code long} value
     * between 0 (inclusive) and the specified value (exclusive), drawn from
     * this random number generator's sequence.
     *
     * @param n the bound on the random number to be returned.  Must be
     * positive.
     * @return  a pseudorandom, uniformly distributed {@code long}
     * value between 0 (inclusive) and n (exclusive).
     * @throws IllegalArgumentException  if n is not positive.
     */
    fn next_long_i64(&mut self, n: i64) -> Result<i64, &str> {
        if n > 0 {
            let mut bits: i64;
            let mut val: i64;
            loop {
                bits = (self.next(31) as i64) << 32;
                bits |= (self.next(32) as i64) & 0xffffffff;
                val = bits % n;
                if !(bits - val + (n - 1) < 0) {
                    break;
                }
            }
            return Ok(val);
        }
        Err("NotStrictlyPositive")
    }


    /**
     * Generates random bytes and places them into a user-supplied array.
     *
     * <p>
     * The array is filled with bytes extracted from random integers.
     * This implies that the number of random bytes generated may be larger than
     * the length of the byte array.
     * </p>
     *
     * @param bytes Array in which to put the generated bytes. Cannot be {@code null}.
     * @param start Index at which to start inserting the generated bytes.
     * @param len Number of bytes to insert.
     * @throws OutOfRangeException if {@code start < 0} or {@code start >= bytes.length}.
     * @throws OutOfRangeException if {@code len < 0} or {@code len > bytes.length - start}.
     */
    fn next_bytes(&mut self, len: i32) -> Vec<i8> {
        if len < 0 {
            return Vec::new();
        }
        self.next_bytes_fill(len)
    }

    /**
     * Generates random bytes and places them into a user-supplied array.
     *
     * <p>
     * The array is filled with bytes extracted from random integers.
     * This implies that the number of random bytes generated may be larger than
     * the length of the byte array.
     * </p>
     *
     * @param bytes Array in which to put the generated bytes. Cannot be {@code null}.
     * @param start Index at which to start inserting the generated bytes.
     * @param len Number of bytes to insert.
     */
    fn next_bytes_fill(&mut self, len: i32) -> Vec<i8> {
        let mut bytes = Vec::new();
        // Index of first insertion.
        let mut index: i32 = 0;
        // Index of first insertion plus multiple 4 part of length (i.e. length
        // with two least significant bits unset).
        let index_loop_limit: i32 = index + (len & 0x7ffffffc);
        // Start filling in the byte array, 4 bytes 	at a time.
        while index < index_loop_limit {
            let random: i32 = self.next(32);
            bytes.push(random as i8);
            bytes.push((random >> /* >>> */ 8) as i8);
            bytes.push((random >> /* >>> */ 16) as i8);
            bytes.push((random >> /* >>> */ 24) as i8);
            index += 4;
        }
        // Index of last insertion + 1.
        let index_limit: i32 = len;
        // Fill in the remaining bytes.
        if index < index_limit {
            let mut random: i32 = self.next(32);
            loop {
                bytes.push(random as i8);
                index += 1;
                if index < index_limit {
                    random >>= /* >>>= */ 8;
                } else {
                    break;
                }
            }
        }
        bytes
    }
}

#[derive(Clone)]
pub struct GaussianPart {
    next_gaussian: f64,
}

impl GaussianPart {
    pub fn new() -> GaussianPart {
        GaussianPart { next_gaussian: f64::NAN }
    }
    /**
	     * Clears the cache used by the default implementation of
	     * {@link #nextGaussian}.
	     */
    pub fn clear(&mut self) {
        self.set_next_gaussian(f64::NAN);
    }

    pub fn get_next_gaussian(&self) -> f64 {
        self.next_gaussian
    }

    pub fn set_next_gaussian(&mut self, next_gaussian: f64) {
        self.next_gaussian = next_gaussian;
    }
}

pub trait GaussianManager {
    fn get_gaussian_part(&mut self) -> &mut GaussianPart;

    /**
	     * Clears the cache used by the default implementation of
	     * {@link #nextGaussian}.
	     */
    fn clear(&mut self) {
        self.get_gaussian_part().set_next_gaussian(f64::NAN);
    }

    fn get_next_gaussian(&mut self) -> f64 {
        self.get_gaussian_part().get_next_gaussian()
    }

    fn set_next_gaussian(&mut self, next_gaussian: f64) {
        self.get_gaussian_part().set_next_gaussian(next_gaussian);
    }
}
