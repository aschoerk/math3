use random::bits_stream_generator::*;
use time;
use util::fastmath::*;

/** Size of the bytes pool. */
const N: usize = 624;

/** Period second parameter. */
const M: usize = 397;

/** X * MATRIX_A for X = {0, 1}. */
const MAG01: [u32; 2] = [0x0, 0x9908b0df as u32];

#[derive(Clone)]
pub struct MersenneTwister {
    /** Bytes pool. */
    mt: Vec<u32>,

    /** Current index in the bytes pool. */
    mti: usize,

    next_gaussian: GaussianPart,
}

impl GaussianManager for MersenneTwister {
    fn get_gaussian_part(&mut self) -> &mut GaussianPart {
        &mut self.next_gaussian
    }
}

impl BitsStreamGenerator for MersenneTwister {
    /** Reinitialize the generator as if just built with the given int seed.
     * <p>The state of the generator is exactly the same as a new
     * generator built with the same seed.</p>
     * @param seed the initial seed (32 bits integer)
     */
    fn set_seed_i32(&mut self, seed: &i32) {
        // we use a long masked by 0xffffffffL as a poor man unsigned int
        let mut long_m_t: i64 = *seed as i64;

        self.mt.clear();
        // NB: unlike original C code, we are working with java longs, the cast below makes masking unnecessary
        self.mt.push(long_m_t as u32);
        {
            self.mti = 1;
            while self.mti < N {
                {
                    // See Knuth TAOCP Vol2. 3rd Ed. P.106 for multiplier.
                    // initializer from the 2002-01-09 C version by Makoto Matsumoto
                    long_m_t = (1812433253 * (long_m_t ^ (long_m_t >> 30)) + self.mti as i64) &
                               0xffffffff;
                    self.mt.push(long_m_t as u32);
                }
                self.mti += 1;
            }
        }

        // Clear normal deviate cache
        self.clear();
    }

    /** Reinitialize the generator as if just built with the given int array seed.
     * <p>The state of the generator is exactly the same as a new
     * generator built with the same seed.</p>
     * @param seed the initial seed (32 bits integers array), if null
     * the seed of the generator will be the current system time plus the
     * system identity hash code of this instance
     */
    fn set_seed_i32a(&mut self, seed: &Vec<i32>) {
        self.set_seed_i32(&19650218);
        let mut i: usize = 1;
        let mut j: usize = 0;
        {
            let mut k: i32 = I32::max(N as i32, seed.len() as i32);
            while k != 0 {
                {
                    let l0: i64 = self.mt[i] as i64;
                    let l1: i64 = self.mt[i - 1] as i64;
                    // non linear
                    let l: i64 = (l0 ^ ((l1 ^ (l1 >> 30)) * 1664525)) + seed[j] as i64 + j as i64;
                    self.mt[i] = (l & 0xffffffff) as u32;
                    i += 1;
                    j += 1;
                    if i >= N {
                        self.mt[0] = self.mt[N - 1];
                        i = 1;
                    }
                    if j >= seed.len() {
                        j = 0;
                    }
                }
                k -= 1;
            }
        }

        {
            let mut k: usize = N - 1;
            while k != 0 {
                {
                    let l0: i64 = self.mt[i] as i64;  // & 0x7fffffff) | ( if self.mt[i] < 0 { 0x80000000 } else { 0x0 });
                    let l1: i64 = self.mt[i - 1] as i64; //  & 0x7fffffff) | ( if self.mt[i - 1] < 0 { 0x80000000 } else { 0x0 });
                    // non linear
                    let l: i64 = (l0 ^ ((l1 ^ (l1 >> 30)) * 1566083941)) - i as i64;
                    self.mt[i] = (l & 0xffffffff) as u32;
                    i += 1;
                    if i >= N {
                        self.mt[0] = self.mt[N - 1];
                        i = 1;
                    }
                }
                k -= 1;
            }
        }

        // MSB is 1; assuring non-zero initial array
        self.mt[0] = 0x80000000 as u32;
        // Clear normal deviate cache
        self.clear();
    }

    /** Reinitialize the generator as if just built with the given long seed.
     * <p>The state of the generator is exactly the same as a new
     * generator built with the same seed.</p>
     * @param seed the initial seed (64 bits integer)
     */
    fn set_seed_i64(&mut self, seed: &i64) {
        let mut param = Vec::new();
        param.push((seed >> /* >>> */ 32) as i32);
        param.push((seed & 0xffffffff) as i32);
        self.set_seed_i32a(&param);
    }
}

impl BitsAdvancingGenerator for MersenneTwister {
    /** Generate next pseudorandom number.
     * <p>This method is the core generation algorithm. It is used by all the
     * public generation methods for the various primitive types {@link
     * #nextBoolean()}, {@link #nextBytes(byte[])}, {@link #nextDouble()},
     * {@link #nextFloat()}, {@link #nextGaussian()}, {@link #nextInt()},
     * {@link #next(int)} and {@link #nextLong()}.</p>
     * @param bits number of random bits to produce
     * @return random bits generated
     */
    fn next(&mut self, bits: i32) -> i32 {
        let mut y: u32;
        if self.mti >= N {
            // generate N words at one time
            let mut mt_next: u32 = self.mt[0];
            {
                let mut k: usize = 0;
                while k < N - M {
                    {
                        let mt_curr: u32 = mt_next;
                        mt_next = self.mt[k + 1];
                        y = (mt_curr & 0x80000000) | (mt_next & 0x7fffffff);
                        self.mt[k] = self.mt[k + M] ^ (y >> /* >>> */ 1) ^
                                     MAG01[(y & 0x1) as usize];
                    }
                    k += 1;
                }
            }

            {
                let mut k: usize = N - M;
                while k < N - 1 {
                    {
                        let mt_curr: u32 = mt_next;
                        mt_next = self.mt[k + 1];
                        y = (mt_curr & 0x80000000) | (mt_next & 0x7fffffff);
                        self.mt[k] = self.mt[k - (N - M)] ^ (y >> /* >>> */ 1) ^
                                     MAG01[(y & 0x1) as usize];
                    }
                    k += 1;
                }
            }

            y = (mt_next & 0x80000000) | (self.mt[0] & 0x7fffffff);
            self.mt[N - 1] = self.mt[M - 1] ^ (y >> /* >>> */ 1) ^ MAG01[(y & 0x1) as usize];
            self.mti = 0;
        }
        y = self.mt[self.mti];
        self.mti += 1;
        // tempering
        y ^= y >> /* >>> */ 11;
        y ^= (y << 7) & 0x9d2c5680;
        y ^= (y << 15) & 0xefc60000;
        y ^= y >> /* >>> */ 18;
        return (y >> /* >>> */ (32 - bits)) as i32;
    }
}


impl MersenneTwister {
    fn new_struct() -> MersenneTwister {
        MersenneTwister {
            mt: Vec::new(),
            mti: 0,
            next_gaussian: GaussianPart::new(),
        }
    }

    /** Creates a new random number generator.
     * <p>The instance is initialized using the current time plus the
     * system identity hash code of this instance as the seed.</p>
     */
    pub fn new() -> MersenneTwister {
        let mut res = MersenneTwister::new_struct();
        let hash: *const usize = &res.mti;
        let timespec = time::get_time();
        res.set_seed_i64(&((timespec.sec ^ timespec.nsec as i64) + hash as i64));
        res
    }

    /** Creates a new random number generator using a single int seed.
     * @param seed the initial seed (32 bits integer)
     */
    pub fn new_i32(seed: &i32) -> MersenneTwister {
        let mut mt = MersenneTwister::new_struct();
        mt.set_seed_i32(seed);
        mt
    }

    /** Creates a new random number generator using an int array seed.
     * @param seed the initial seed (32 bits integers array), if null
     * the seed of the generator will be related to the current time
     */
    pub fn new_i32a(seed: &Vec<i32>) -> MersenneTwister {
        let mut mt = MersenneTwister::new_struct();
        mt.set_seed_i32a(&seed);
        mt
    }

    /** Creates a new random number generator using a single long seed.
     * @param seed the initial seed (64 bits integer)
     */
    pub fn new_i64(seed: &i64) -> MersenneTwister {
        let mut mt = MersenneTwister::new_struct();
        mt.set_seed_i64(seed);
        mt
    }
}


#[cfg(test)]
mod tests {
    use random::mersenne_twister::*;
    use random::bits_stream_generator::*;

    #[test]
    pub fn test1() {
        let mut mt = MersenneTwister::new_i32a(&vec![0x123, 0x234, 0x345, 0x456]);
        let mut res = Vec::new();
        res.push(mt.next_int());
        res.push(mt.next_int());
        res.push(mt.next_int());
        res.push(mt.next_int());
        res.push(mt.next_int());
        res.push(mt.next_int());
        res.push(mt.next_int());
        res.push(mt.next_int());

    }
}
