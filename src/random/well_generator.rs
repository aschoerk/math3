use random::bits_stream_generator::*;
use time;
use util::fastmath::*;
use std::rc::Rc;


#[derive(Clone)]
pub struct WellParams {
    /** Current index in the bytes pool. */
    index: usize,

    /** Bytes pool. */
    v: Vec<i32>,

    /** Index indirection table giving for each index its predecessor taking table size into account. */
    i_rm1: Vec<usize>,

    /** Index indirection table giving for each index its second predecessor taking table size into account. */
    i_rm2: Vec<usize>,

    /** Index indirection table giving for each index the value index + m1 taking table size into account. */
    i1: Vec<usize>,

    /** Index indirection table giving for each index the value index + m2 taking table size into account. */
    i2: Vec<usize>,

    /** Index indirection table giving for each index the value index + m3 taking table size into account. */
    i3: Vec<usize>,
}

#[derive(Clone)]
pub struct WellData {
    params: WellParams,

    next_gaussian: GaussianPart,

    next_bits: Rc<Box<Fn(&mut WellParams, i32) -> i32>>,
}

impl GaussianManager for WellData {
    fn get_gaussian_part(&mut self) -> &mut GaussianPart {
        &mut self.next_gaussian
    }
}

impl BitsAdvancingGenerator for WellData {
    fn next(&mut self, bits: i32) -> i32 {
        let mut p = self.params.clone();
        let func = Rc::get_mut(&mut self.next_bits).unwrap().as_mut();
        let res = func(&mut p, bits);
        self.params = p;
        res

    }
}


impl BitsStreamGenerator for WellData {
    /** Reinitialize the generator as if just built with the given int array seed.
     * <p>The state of the generator is exactly the same as a new
     * generator built with the same seed.</p>
     * @param seed the initial seed (32 bits integers array). If null
     * the seed of the generator will be the system time plus the system identity
     * hash code of the instance.
     */
    fn set_seed_i32a(&mut self, seed: &Vec<i32>) {

        let seed_len: usize = seed.len();
        if seed_len == 0 {
            let hash: *const usize = &self.params.index;
            let timespec = time::get_time();
            self.set_seed_i64(&((timespec.sec ^ timespec.nsec as i64) + hash as i64));
        } else {

            let v_len: usize = self.params.v.len();

            for i in 0..I32::min(seed_len as i32, v_len as i32) as usize {
                self.params.v[i] = seed[i] as i32;
            }

            if seed.len() < self.params.v.len() {
                {
                    let mut i: usize = seed.len();
                    while i < self.params.v.len() {
                        {
                            let l: i64 = self.params.v[i - seed.len()] as i64;
                            self.params.v[i] =
                                ((1812433253 * (l ^ (l >> 30)) + i as i64) & 0xffffffff) as i32;
                        }
                        i += 1;
                    }
                }

            }
            self.params.index = 0;
            // Clear normal deviate cache
            self.clear();
        }
    }
}


impl WellData {
    /** Creates a new random number generator.
     * <p>The instance is initialized using the current time plus the
     * system identity hash code of this instance as the seed.</p>
     * @param k number of bits in the pool (not necessarily a multiple of 32)
     * @param m1 first parameter of the algorithm
     * @param m2 second parameter of the algorithm
     * @param m3 third parameter of the algorithm
     */
    fn new_k_m1_m2_m3(k: i32,
                      m1: i32,
                      m2: i32,
                      m3: i32,
                      next_bits_p: Box<Fn(&mut WellParams, i32) -> i32>)
                      -> WellData {
        WellData::new_k_m1_m2_m3_seeda(k, m1, m2, m3, &Vec::new(), next_bits_p)
    }
    /** Creates a new random number generator using a single int seed.
     * @param k number of bits in the pool (not necessarily a multiple of 32)
     * @param m1 first parameter of the algorithm
     * @param m2 second parameter of the algorithm
     * @param m3 third parameter of the algorithm
     * @param seed the initial seed (32 bits integer)
     */
    fn new_k_m1_m2_m3_seed(k: i32,
                           m1: i32,
                           m2: i32,
                           m3: i32,
                           seed_p: i32,
                           next_bits_p: Box<Fn(&mut WellParams, i32) -> i32>)
                           -> WellData {
        let mut seed = Vec::new();
        seed.push(seed_p);
        WellData::new_k_m1_m2_m3_seeda(k, m1, m2, m3, &seed, next_bits_p)
    }
    /** Creates a new random number generator using an int array seed.
     * @param k number of bits in the pool (not necessarily a multiple of 32)
     * @param m1 first parameter of the algorithm
     * @param m2 second parameter of the algorithm
     * @param m3 third parameter of the algorithm
     * @param seed the initial seed (32 bits integers array), if null
     * the seed of the generator will be related to the current time
     */
    fn new_k_m1_m2_m3_seeda(k: i32,
                            m1: i32,
                            m2: i32,
                            m3: i32,
                            seed_p: &Vec<i32>,
                            next_bits_p: Box<Fn(&mut WellParams, i32) -> i32>)
                            -> WellData {
        // the bits pool contains k bits, k = r w - p where r is the number
        // of w bits blocks, w is the block size (always 32 in the original paper)
        // and p is the number of unused bits in the last block
        let w: i32 = 32;
        let r: usize = ((k + w - 1) / w) as usize;
        let params = WellParams {
            index: 0,
            v: vec![0;r],
            i_rm1: vec![0;r],
            i_rm2: vec![0;r],
            i1: vec![0;r],
            i2: vec![0;r],
            i3: vec![0;r],
        };
        let mut res = WellData {
            params: params,
            next_gaussian: GaussianPart::new(),
            next_bits: Rc::new(next_bits_p),
        };

        // precompute indirection index tables. These tables are used for optimizing access
        // they allow saving computations like "(j + r - 2) % r" with costly modulo operations

        {
            let mut j: usize = 0;
            while j < r {
                {
                    res.params.i_rm1[j] = (j + r - 1) % r;
                    res.params.i_rm2[j] = (j + r - 2) % r;
                    res.params.i1[j] = (j + m1 as usize) % r;
                    res.params.i2[j] = (j + m2 as usize) % r;
                    res.params.i3[j] = (j + m3 as usize) % r;
                }
                j += 1;
            }
        }

        // initialize the pool content
        res.set_seed_i32a(&seed_p);
        res
    }
}

pub struct WellGeneratorFactory {
	
}

impl WellGeneratorFactory {
    pub fn new_well_1024a() -> WellData {
        WellData::new_k_m1_m2_m3(1024,
                                 3,
                                 24,
                                 10,
                                 Box::new(move |well_params, bits| {

            let index_rm1: usize = well_params.i_rm1[well_params.index];
            let v0: i32 = well_params.v[well_params.index];
            let v_m1: i32 = well_params.v[well_params.i1[well_params.index]];
            let v_m2: i32 = well_params.v[well_params.i2[well_params.index]];
            let v_m3: i32 = well_params.v[well_params.i3[well_params.index]];
            let z0: i32 = well_params.v[index_rm1];
            let z1: i32 = v0 ^ (v_m1 ^ (v_m1 as u32 >> /* >>> */ 8) as i32);
            let z2: i32 = (v_m2 ^ (v_m2 << 19)) ^ (v_m3 ^ (v_m3 << 14));
            let z3: i32 = z1 ^ z2;
            let z4: i32 = (z0 ^ (z0 << 11)) ^ (z1 ^ (z1 << 7)) ^ (z2 ^ (z2 << 13));
            well_params.v[well_params.index] = z3;
            well_params.v[index_rm1] = z4;
            well_params.index = index_rm1;
            return (z4 as u32 >> /* >>> */ (32 - bits)) as i32;
        }))
    }

    pub fn new_well_512a() -> WellData {
        WellData::new_k_m1_m2_m3(512,
                                 13,
                                 9,
                                 5,
                                 Box::new(move |well_params, bits| {

            let index_rm1: usize = well_params.i_rm1[well_params.index];
            let v0: i32 = well_params.v[well_params.index];
            let v_m1: i32 = well_params.v[well_params.i1[well_params.index]];
            let v_m2: i32 = well_params.v[well_params.i2[well_params.index]];
            let z0: i32 = well_params.v[index_rm1];

            // the values below include the errata of the original article
            let z1: i32 = (v0 ^ (v0 << 16)) ^ (v_m1 ^ (v_m1 << 15));
            let z2: i32 = v_m2 ^ (v_m2 as u32 >> /* >>> */ 11) as i32;
            let z3: i32 = z1 ^ z2;
            let z4: i32 = (z0 ^ (z0 << 2)) ^ (z1 ^ (z1 << 18)) ^ (z2 << 28) ^
                          (z3 ^ ((z3 << 5) & 0xda442d24 as u32 as i32));

            well_params.v[well_params.index] = z3;
            well_params.v[index_rm1] = z4;
            well_params.index = index_rm1;
            return (z4 as u32 >> /* >>> */ (32 - bits)) as i32;
        }))
    }

    pub fn new_well_19937c() -> WellData {
        WellData::new_k_m1_m2_m3(19937,
                                 70,
                                 179,
                                 449,
                                 Box::new(move |well_params, bits| {
            let index_rm1: usize = well_params.i_rm1[well_params.index];
            let index_rm2: usize = well_params.i_rm2[well_params.index];
            let v0: i32 = well_params.v[well_params.index];
            let v_m1: i32 = well_params.v[well_params.i1[well_params.index]];
            let v_m2: i32 = well_params.v[well_params.i2[well_params.index]];
            let v_m3: i32 = well_params.v[well_params.i3[well_params.index]];

            let z0: i32 = (0x80000000 as u32 as i32 & well_params.v[index_rm1]) ^
                          (0x7FFFFFFF & well_params.v[index_rm2]);
            let z1: i32 = (v0 ^ (v0 << 25)) ^ (v_m1 ^ (v_m1 as u32 >> /* >>> */ 27) as i32);
            let z2: i32 = (v_m2 as u32 >> /* >>> */ 9) as i32 ^
                          (v_m3 ^ (v_m3 as u32 >> /* >>> */ 1) as i32);
            let z3: i32 = z1 ^ z2;
            let mut z4: i32 = z0 ^ (z1 ^ (z1 << 9)) ^ (z2 ^ (z2 << 21)) ^
                              (z3 ^ (z3 as u32 >> /* >>> */ 21) as i32);
            well_params.v[well_params.index] = z3;
            well_params.v[index_rm1] = z4;
            well_params.v[index_rm2] &= 0x80000000 as u32 as i32;
            well_params.index = index_rm1;
            // add Matsumoto-Kurita tempering
            // to get a maximally-equidistributed generator
            z4 ^= (z4 << 7) & 0xe46e1700 as u32 as i32;
            z4 ^= (z4 << 15) & 0x9b868000 as u32 as i32;


            return (z4 as u32 >> /* >>> */ (32 - bits)) as i32;
        }))
    }

    pub fn new_well_19937a() -> WellData {
        WellData::new_k_m1_m2_m3(19937,
                                 70,
                                 179,
                                 449,
                                 Box::new(move |well_params, bits| {
            let index_rm1: usize = well_params.i_rm1[well_params.index];
            let index_rm2: usize = well_params.i_rm2[well_params.index];
            let v0: i32 = well_params.v[well_params.index];
            let v_m1: i32 = well_params.v[well_params.i1[well_params.index]];
            let v_m2: i32 = well_params.v[well_params.i2[well_params.index]];
            let v_m3: i32 = well_params.v[well_params.i3[well_params.index]];

            let z1: i32 = (v0 ^ (v0 << 25)) ^ (v_m1 ^ (v_m1 as u32 >> /* >>> */ 27) as i32);
            let z0: i32 = (0x80000000 as u32 as i32 & well_params.v[index_rm1]) ^
                          (0x7FFFFFFF & well_params.v[index_rm2]);
            let z2: i32 = (v_m2 as u32 >> /* >>> */ 9) as i32 ^
                          (v_m3 ^ (v_m3 as u32 >> /* >>> */ 1) as i32);
            let z3: i32 = z1 ^ z2;
            let z4: i32 = z0 ^ (z1 ^ (z1 << 9)) ^ (z2 ^ (z2 << 21)) ^
                          (z3 ^ (z3 as u32 >> /* >>> */ 21) as i32);
            well_params.v[well_params.index] = z3;
            well_params.v[index_rm1] = z4;
            well_params.v[index_rm2] &= 0x80000000 as u32 as i32;
            well_params.index = index_rm1;


            return (z4 as u32 >> /* >>> */ (32 - bits)) as i32;
        }))
    }

    pub fn new_well_44497a() -> WellData {
        WellData::new_k_m1_m2_m3(44497,
                                 23,
                                 481,
                                 229,
                                 Box::new(move |well_params, bits| {
            let index_rm1: usize = well_params.i_rm1[well_params.index];
            let index_rm2: usize = well_params.i_rm2[well_params.index];
            let v0: i32 = well_params.v[well_params.index];
            let v_m1: i32 = well_params.v[well_params.i1[well_params.index]];
            let v_m2: i32 = well_params.v[well_params.i2[well_params.index]];
            let v_m3: i32 = well_params.v[well_params.i3[well_params.index]];

            // the values below include the errata of the original article
            // the values below include the errata of the original article
            let z0: i32 = (0xFFFF8000 as u32 as i32 & well_params.v[index_rm1]) ^
                          (0x00007FFF & well_params.v[index_rm2]);
            let z1: i32 = (v0 ^ (v0 << 24)) ^ (v_m1 ^ (v_m1 as u32 >> /* >>> */ 30) as i32);
            let z2: i32 = (v_m2 ^ (v_m2 << 10)) ^ (v_m3 << 26);
            let z3: i32 = z1 ^ z2;
            let z2_prime: i32 = ((z2 << 9) ^ (z2 as u32 >> /* >>> */ 23) as i32) &
                                0xfbffffff as u32 as i32;
            let z2_second: i32 = if (z2 & 0x00020000) != 0 {
                (z2_prime ^ 0xb729fcec as u32 as i32)
            } else {
                z2_prime
            };
            let z4: i32 = z0 ^ (z1 ^ (z1 as u32 >> /* >>> */ 20) as i32) ^ z2_second ^ z3;
            well_params.v[well_params.index] = z3;
            well_params.v[index_rm1] = z4;
            well_params.v[index_rm2] &= 0xFFFF8000 as u32 as i32;
            well_params.index = index_rm1;

            return (z4 as u32 >> /* >>> */ (32 - bits)) as i32;
        }))
    }


    pub fn new_well_44497b() -> WellData {
        WellData::new_k_m1_m2_m3(44497,
                                 23,
                                 481,
                                 229,
                                 Box::new(move |well_params, bits| {
            let index_rm1: usize = well_params.i_rm1[well_params.index];
            let index_rm2: usize = well_params.i_rm2[well_params.index];
            let v0: i32 = well_params.v[well_params.index];
            let v_m1: i32 = well_params.v[well_params.i1[well_params.index]];
            let v_m2: i32 = well_params.v[well_params.i2[well_params.index]];
            let v_m3: i32 = well_params.v[well_params.i3[well_params.index]];

            // the values below include the errata of the original article
            // the values below include the errata of the original article
            let z0: i32 = (0xFFFF8000 as u32 as i32 & well_params.v[index_rm1]) ^
                          (0x00007FFF & well_params.v[index_rm2]);
            let z1: i32 = (v0 ^ (v0 << 24)) ^ (v_m1 ^ (v_m1 as u32 >> /* >>> */ 30) as i32);
            let z2: i32 = (v_m2 ^ (v_m2 << 10)) ^ (v_m3 << 26);
            let z3: i32 = z1 ^ z2;
            let z2_prime: i32 = ((z2 << 9) ^ (z2 as u32 >> /* >>> */ 23) as i32) &
                                0xfbffffff as u32 as i32;
            let z2_second: i32 = if (z2 & 0x00020000) != 0 {
                (z2_prime ^ 0xb729fcec as u32 as i32)
            } else {
                z2_prime
            };
            let mut z4: i32 = z0 ^ (z1 ^ (z1 as u32 >> /* >>> */ 20) as i32) ^ z2_second ^ z3;
            well_params.v[well_params.index] = z3;
            well_params.v[index_rm1] = z4;
            well_params.v[index_rm2] &= 0xFFFF8000 as u32 as i32;
            well_params.index = index_rm1;
            // add Matsumoto-Kurita tempering
            // to get a maximally-equidistributed generator
            z4 ^= (z4 << 7) & 0x93dd1400 as u32 as i32;
            z4 ^= (z4 << 15) & 0xfa118000 as u32 as i32;
            return (z4 as u32 >> /* >>> */ (32 - bits)) as i32;
        }))
    }
}
