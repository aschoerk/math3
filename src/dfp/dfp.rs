
// Licensed to the Apache Software Foundation (ASF) under one or more
// contributor license agreements.  See the NOTICE file distributed with
// this work for additional information regarding copyright ownership.
// The ASF licenses this file to You under the Apache License, Version 2.0
// (the "License"); you may not use this file except in compliance with
// the License.  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//


use util::fastmath;
pub use std::fmt;
use util::rsutils::*;
use dfp::dfp_field::*;
use dfp::dfp_math::*;
use std::usize;
use std::f64;
use std::rc::*;
use std::char::*;
use base;

/**
 *  Decimal floating point library for Java
 *
 *  <p>Another floating point class.  This one is built using radix 10000
 *  which is 10<sup>4</sup>, so its almost decimal.</p>
 *
 *  <p>The design goals here are:
 *  <ol>
 *    <li>Decimal math, or close to it</li>
 *    <li>Settable precision (but no mix between numbers using different settings)</li>
 *    <li>Portability.  Code should be kept as portable as possible.</li>
 *    <li>Performance</li>
 *    <li>Accuracy  - Results should always be +/- 1 ULP for basic
 *         algebraic operation</li>
 *    <li>Comply with IEEE 854-1987 as much as possible.
 *         (See IEEE 854-1987 notes below)</li>
 *  </ol></p>
 *
 *  <p>Trade offs:
 *  <ol>
 *    <li>Memory foot print.  I'm using more memory than necessary to
 *         represent numbers to get better performance.</li>
 *    <li>Digits are bigger, so rounding is a greater loss.  So, if you
 *         really need 12 decimal digits, better use 4 base 10000 digits
 *         there can be one partially filled.</li>
 *  </ol></p>
 *
 *  <p>Numbers are represented  in the following form:
 *  <pre>
 *  n  =  sign × mant × (radix)<sup>exp</sup>;</p>
 *  </pre>
 *  where sign is ±1, mantissa represents a fractional number between
 *  zero and one.  mant[0] is the least significant digit.
 *  exp is in the range of -32767 to 32768</p>
 *
 *  <p>IEEE 854-1987  Notes and differences</p>
 *
 *  <p>IEEE 854 requires the radix to be either 2 or 10.  The radix here is
 *  10000, so that requirement is not met, but  it is possible that a
 *  subclassed can be made to make it behave as a radix 10
 *  number.  It is my opinion that if it looks and behaves as a radix
 *  10 number then it is one and that requirement would be met.</p>
 *
 *  <p>The radix of 10000 was chosen because it should be faster to operate
 *  on 4 decimal digits at once instead of one at a time.  Radix 10 behavior
 *  can be realized by adding an additional rounding step to ensure that
 *  the number of decimal digits represented is constant.</p>
 *
 *  <p>The IEEE standard specifically leaves out internal data encoding,
 *  so it is reasonable to conclude that such a subclass of this radix
 *  10000 system is merely an encoding of a radix 10 system.</p>
 *
 *  <p>IEEE 854 also specifies the existence of "sub-normal" numbers.  This
 *  class does not contain any such entities.  The most significant radix
 *  10000 digit is always non-zero.  Instead, we support "gradual underflow"
 *  by raising the underflow flag for numbers less with exponent less than
 *  expMin, but don't flush to zero until the exponent reaches MIN_EXP-digits.
 *  Thus the smallest number we can represent would be:
 *  1E(-(MIN_EXP-digits-1)*4),  eg, for digits=5, MIN_EXP=-32767, that would
 *  be 1e-131092.</p>
 *
 *  <p>IEEE 854 defines that the implied radix point lies just to the right
 *  of the most significant digit and to the left of the remaining digits.
 *  This implementation puts the implied radix point to the left of all
 *  digits including the most significant one.  The most significant digit
 *  here is the one just to the right of the radix point.  This is a fine
 *  detail and is really only a matter of definition.  Any side effects of
 *  this can be rendered invisible by a subclass.</p>
 * @see DfpField
 * @since 2.2
 */
/** The radix, or base of this system.  Set to 10000 */
pub const RADIX: i32 = 10000;
pub const RADIX_L: i64 = 10000;

/** The minimum exponent before underflow is signaled.  Flush to zero
     *  occurs at minExp-DIGITS */
pub const MIN_EXP: i32 = -32767;

/** The maximum exponent before overflow is signaled and results flushed
     *  to infinity */
pub const MAX_EXP: i32 = 32768;

/** The amount under/overflows are scaled by before going to trap handler */
pub const ERR_SCALE: i32 = 32760;

/** Indicator value for normal finite numbers. */
pub const FINITE: i8 = 0;

/** Indicator value for Infinity. */
pub const INFINITE: i8 = 1;

/** Indicator value for signaling NaN. */
pub const SNAN: i8 = 2;

/** Indicator value for quiet NaN. */
pub const QNAN: i8 = 3;

/** String for NaN representation. */
const NAN_STRING: &'static str = "NaN";

/** String for positive infinity representation. */
const POS_INFINITY_STRING: &'static str = "Infinity";

/** String for negative infinity representation. */
const NEG_INFINITY_STRING: &'static str = "-Infinity";

/** Name for traps triggered by addition. */
const ADD_TRAP: &'static str = "add";

/** Name for traps triggered by multiplication. */
const MULTIPLY_TRAP: &'static str = "multiply";

/** Name for traps triggered by division. */
const DIVIDE_TRAP: &'static str = "divide";

/** Name for traps triggered by square root. */
const SQRT_TRAP: &'static str = "sqrt";

/** Name for traps triggered by alignment. */
const ALIGN_TRAP: &'static str = "align";

/** Name for traps triggered by truncation. */
const TRUNC_TRAP: &'static str = "trunc";

/** Name for traps triggered by nextAfter. */
const NEXT_AFTER_TRAP: &'static str = "nextAfter";

/** Name for traps triggered by lessThan. */
const LESS_THAN_TRAP: &'static str = "lessThan";

/** Name for traps triggered by greaterThan. */
const GREATER_THAN_TRAP: &'static str = "greaterThan";

/** Name for traps triggered by newInstance. */
const NEW_INSTANCE_TRAP: &'static str = "newInstance";

#[derive(Clone,Hash)]
pub struct Dfp {
    /** Mantissa. */
    pub mant: Vec<i32>,

    /** Sign bit: 1 for positive, -1 for negative. */
    pub sign: i8,

    /** Exponent. */
    pub exp: i32,

    /** Indicator for non-finite / non-number values. */
    pub nans: i8,

    /** Factory building similar Dfp's. */
    pub field: Rc<DfpField>,
}

impl fmt::Debug for Dfp {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Dfp: {}", self.to_string())
    }
}

impl PartialEq for Dfp {
    fn eq(&self, other: &Dfp) -> bool {
        let x: &Dfp = other;
        if self.is_na_n() || x.is_na_n() ||
           self.field.get_radix_digits() != x.field.get_radix_digits() {
            return false;
        }
        return Dfp::compare(self, x) == 0;
    }

    fn ne(&self, other: &Dfp) -> bool {
        let x: &Dfp = other;

        if self.is_na_n() || x.is_na_n() ||
           self.field.get_radix_digits() != x.field.get_radix_digits() {
            return false;
        }
        return self.greater_than(x) || self.less_than(x);
    }
}

const MIN_I64: i64 = (-1 as i64) << (64 - 1);

impl base::Field<Dfp> for Dfp {
    fn get_zero(&self) -> Dfp {
        self.get_zero()
    }

    fn get_one(&self) -> Dfp {
        self.get_one()
    }
}

impl base::FieldElement<Dfp> for Dfp {
    /** '+' operator.
     * @param a right hand side parameter of the operator
     * @return this+a
     */
    fn add(&self, a: &Dfp) -> Dfp {
        self.add(a)
    }

    /** '-' operator.
     * @param a right hand side parameter of the operator
     * @return this-a
     */
    fn subtract(&self, a: &Dfp) -> Dfp {
        self.subtract(a)
    }

    fn negate(&self) -> Dfp {
        self.negate()
    }

    fn multiply_i32(&self, n: i32) -> Dfp {
        self.multiply_i32(n)
    }

    fn multiply(&self, a: &Dfp) -> Dfp {
        self.multiply(a)
    }

    fn divide(&self, a: &Dfp) -> Result<Dfp, String> {
        // MathArithmeticsException
        self.divide(a)
    }

    fn reciprocal(&self) -> Result<Dfp, String> {
        // MathArithmeticsException
        self.reciprocal()
    }
}

impl base::RealFieldElement<Dfp> for Dfp {
    /** Get the real value of the number.
     * @return real value
     */
    fn get_real(&self) -> f64 {
        self.get_real()
    }

    /** '+' operator.
     * @param a right hand side parameter of the operator
     * @return this+a
     */
    fn add_f64(&self, a: f64) -> Dfp {
        self.add_f64(a)
    }

    /** '-' operator.
     * @param a right hand side parameter of the operator
     * @return this-a
     */
    fn subtract_f64(&self, a: f64) -> Dfp {
        self.subtract_f64(a)
    }

    /** '×' operator.
     * @param a right hand side parameter of the operator
     * @return this×a
     */
    fn multiply_f64(&self, a: f64) -> Dfp {
        self.multiply_f64(a)
    }

    /** '÷' operator.
     * @param a right hand side parameter of the operator
     * @return this÷a
     */
    fn divide_f64(&self, a: f64) -> Dfp {
        self.divide_f64(a)
    }

    /** IEEE remainder operator.
     * @param a right hand side parameter of the operator
     * @return this - n × a where n is the closest integer to this/a
     * (the even integer is chosen for n if this/a is halfway between two integers)
     */
    fn remainder_f64(&self, a: f64) -> Dfp {
        self.remainder_f64(a)
    }

    /** IEEE remainder operator.
     * @param a right hand side parameter of the operator
     * @return this - n × a where n is the closest integer to this/a
     * (the even integer is chosen for n if this/a is halfway between two integers)
     * @exception DimensionMismatchException if number of free parameters or orders are inconsistent
     */
    fn remainder(&self, a: &Dfp) -> Result<Dfp, String> {
        Ok(self.remainder(a))
    }

    /** absolute value.
     * @return abs(this)
     */
    fn abs(&self) -> Dfp {
        self.abs()
    }

    /** Get the smallest whole number larger than instance.
     * @return ceil(this)
     */
    fn ceil(&self) -> Dfp {
        self.ceil()
    }

    /** Get the largest whole number smaller than instance.
     * @return floor(this)
     */
    fn floor(&self) -> Dfp {
        self.floor()
    }

    /** Get the whole number that is the nearest to the instance, or the even one if x is exactly half way between two integers.
     * @return a double number r such that r is an integer r - 0.5 ≤ this ≤ r + 0.5
     */
    fn rint(&self) -> Dfp {
        self.rint()
    }

    /** Get the closest long to instance value.
     * @return closest long to {@link #getReal()}
     */
    fn round(&self) -> i64 {
        fastmath::F64::round(self.to_double())
    }

    /** Compute the signum of the instance.
     * The signum is -1 for negative numbers, +1 for positive numbers and 0 otherwise
     * @return -1.0, -0.0, +0.0, +1.0 or NaN depending on sign of a
     */
    fn signum(&self) -> Dfp {
        self.signum()
    }

    /**
     * Returns the instance with the sign of the argument.
     * A NaN {@code sign} argument is treated as positive.
     *
     * @param sign the sign for the returned value
     * @return the instance with the same sign as the {@code sign} argument
     */
    fn copy_sign(&self, sign: &Dfp) -> Dfp {
        self.copy_sign(sign)
    }

    /**
     * Returns the instance with the sign of the argument.
     * A NaN {@code sign} argument is treated as positive.
     *
     * @param sign the sign for the returned value
     * @return the instance with the same sign as the {@code sign} argument
     */
    fn copy_sign_f64(&self, sign: f64) -> Dfp {
        self.copy_sign_f64(sign)
    }

    /**
     * Multiply the instance by a power of 2.
     * @param n power of 2
     * @return this × 2<sup>n</sup>
     */
    fn scalb(&self, n: i32) -> Dfp {
        self.scalb(n)
    }

    /**
     * Returns the hypotenuse of a triangle with sides {@code this} and {@code y}
     * - sqrt(<i>this</i><sup>2</sup> +<i>y</i><sup>2</sup>)
     * avoiding intermediate overflow or underflow.
     *
     * <ul>
     * <li> If either argument is infinite, then the result is positive infinity.</li>
     * <li> else, if either argument is NaN then the result is NaN.</li>
     * </ul>
     *
     * @param y a value
     * @return sqrt(<i>this</i><sup>2</sup> +<i>y</i><sup>2</sup>)
     * @exception DimensionMismatchException if number of free parameters or orders are inconsistent
     */
    fn hypot(&self, y: &Dfp) -> Result<Dfp, String> {
        Ok(self.hypot(y))
    }

    /** {@inheritDoc} */
    fn reciprocal(&self) -> Dfp {
        self.reciprocal().unwrap()
    }

    /** Square root.
     * @return square root of the instance
     */
    fn sqrt(&self) -> Dfp {
        self.sqrt()
    }

    /** Cubic root.
     * @return cubic root of the instance
     */
    fn cbrt(&self) -> Dfp {
        self.cbrt()
    }

    /** N<sup>th</sup> root.
     * @param n order of the root
     * @return n<sup>th</sup> root of the instance
     */
    fn root_n(&self, n: i32) -> Dfp {
        self.root_n(n)
    }

    /** Power operation.
     * @param p power to apply
     * @return this<sup>p</sup>
     */
    fn pow_f64(&self, p: f64) -> Dfp {
        self.pow_f64(p)
    }

    /** Integer power operation.
     * @param n power to apply
     * @return this<sup>n</sup>
     */
    fn pow_i32(&self, n: i32) -> Dfp {
        self.pow_i32(n)
    }

    /** Power operation.
     * @param e exponent
     * @return this<sup>e</sup>
     * @exception DimensionMismatchException if number of free parameters or orders are inconsistent
     */
    fn pow(&self, e: &Dfp) -> Result<Dfp, String> {
        Ok(self.pow(e))
    }

    /** Exponential.
     * @return exponential of the instance
     */
    fn exp(&self) -> Dfp {
        self.exp()
    }

    /** Exponential minus 1.
     * @return exponential minus one of the instance
     */
    fn expm1(&self) -> Dfp {
        self.expm1()
    }

    /** Natural logarithm.
     * @return logarithm of the instance
     */
    fn log(&self) -> Dfp {
        self.log()
    }

    /** Shifted natural logarithm.
     * @return logarithm of one plus the instance
     */
    fn log1p(&self) -> Dfp {
        self.log1p()
    }

    //    TODO: add this method in 4.0, as it is not possible to do it in 3.2
    //          due to incompatibility of the return type in the Dfp class
    //    /** Base 10 logarithm.
    //     * @return base 10 logarithm of the instance
    //     */
    //    Dfp log10();
    /** Cosine operation.
     * @return cos(this)
     */
    fn cos(&self) -> Dfp {
        self.cos()
    }

    /** Sine operation.
     * @return sin(this)
     */
    fn sin(&self) -> Dfp {
        self.sin()
    }

    /** Tangent operation.
     * @return tan(this)
     */
    fn tan(&self) -> Dfp {
        self.tan()
    }

    /** Arc cosine operation.
     * @return acos(this)
     */
    fn acos(&self) -> Dfp {
        self.acos()
    }

    /** Arc sine operation.
     * @return asin(this)
     */
    fn asin(&self) -> Dfp {
        self.asin()
    }

    /** Arc tangent operation.
     * @return atan(this)
     */
    fn atan(&self) -> Dfp {
        self.atan()
    }

    /** Two arguments arc tangent operation.
     * @param x second argument of the arc tangent
     * @return atan2(this, x)
     * @exception DimensionMismatchException if number of free parameters or orders are inconsistent
     */
    fn atan2(&self, x: &Dfp) -> Result<Dfp, String> {
        Ok(self.atan2(x))
    }

    /** Hyperbolic cosine operation.
     * @return cosh(this)
     */
    fn cosh(&self) -> Dfp {
        self.cosh()
    }

    /** Hyperbolic sine operation.
     * @return sinh(this)
     */
    fn sinh(&self) -> Dfp {
        self.sinh()
    }

    /** Hyperbolic tangent operation.
     * @return tanh(this)
     */
    fn tanh(&self) -> Dfp {
        self.tanh()
    }

    /** Inverse hyperbolic cosine operation.
     * @return acosh(this)
     */
    fn acosh(&self) -> Dfp {
        self.acosh()
    }

    /** Inverse hyperbolic sine operation.
     * @return asin(this)
     */
    fn asinh(&self) -> Dfp {
        self.asinh()
    }

    /** Inverse hyperbolic  tangent operation.
     * @return atanh(this)
     */
    fn atanh(&self) -> Dfp {
        self.atanh()
    }

    /**
     * Compute a linear combination.
     * @param a Factors.
     * @param b Factors.
     * @return <code>Σ<sub>i</sub> a<sub>i</sub> b<sub>i</sub></code>.
     * @throws DimensionMismatchException if arrays dimensions don't match
     * @since 3.2
     */
    fn linear_combination_vec(&self, a: &Vec<Dfp>, b: &Vec<Dfp>) -> Result<Dfp, String> {
        self.linear_combination_dfpn_dfpn(a, b)
    }

    /**
     * Compute a linear combination.
     * @param a Factors.
     * @param b Factors.
     * @return <code>Σ<sub>i</sub> a<sub>i</sub> b<sub>i</sub></code>.
     * @throws DimensionMismatchException if arrays dimensions don't match
     * @since 3.2
     */
    fn linear_combination_f64_vec(&self, a: &Vec<f64>, b: &Vec<Dfp>) -> Result<Dfp, String> {
        self.linear_combination_f64n_dfpn(a, b)
    }

    /**
     * Compute a linear combination.
     * @param a1 first factor of the first term
     * @param b1 second factor of the first term
     * @param a2 first factor of the second term
     * @param b2 second factor of the second term
     * @return a<sub>1</sub>×b<sub>1</sub> +
     * a<sub>2</sub>×b<sub>2</sub>
     * @see #linearCombination(Object, Object, Object, Object, Object, Object)
     * @see #linearCombination(Object, Object, Object, Object, Object, Object, Object, Object)
     * @since 3.2
     */
    fn linear_combination_2_factors(&self, a1: &Dfp, b1: &Dfp, a2: &Dfp, b2: &Dfp) -> Dfp {
        self.linear_combination_dfp_dfp_2(a1, b1, a2, b2)
    }

    /**
     * Compute a linear combination.
     * @param a1 first factor of the first term
     * @param b1 second factor of the first term
     * @param a2 first factor of the second term
     * @param b2 second factor of the second term
     * @return a<sub>1</sub>×b<sub>1</sub> +
     * a<sub>2</sub>×b<sub>2</sub>
     * @see #linearCombination(double, Object, double, Object, double, Object)
     * @see #linearCombination(double, Object, double, Object, double, Object, double, Object)
     * @since 3.2
     */
    fn linear_combination_f64_2_factors(&self, a1: f64, b1: &Dfp, a2: f64, b2: &Dfp) -> Dfp {
        self.linear_combination_f64_dfp_2(a1, b1, a2, b2)
    }

    /**
     * Compute a linear combination.
     * @param a1 first factor of the first term
     * @param b1 second factor of the first term
     * @param a2 first factor of the second term
     * @param b2 second factor of the second term
     * @param a3 first factor of the third term
     * @param b3 second factor of the third term
     * @return a<sub>1</sub>×b<sub>1</sub> +
     * a<sub>2</sub>×b<sub>2</sub> + a<sub>3</sub>×b<sub>3</sub>
     * @see #linearCombination(Object, Object, Object, Object)
     * @see #linearCombination(Object, Object, Object, Object, Object, Object, Object, Object)
     * @since 3.2
     */
    fn linear_combination_3_factors(&self,
                                    a1: &Dfp,
                                    b1: &Dfp,
                                    a2: &Dfp,
                                    b2: &Dfp,
                                    a3: &Dfp,
                                    b3: &Dfp)
                                    -> Dfp {
        self.linear_combination_dfp_dfp_3(a1, b1, a2, b2, a3, b3)
    }

    /**
     * Compute a linear combination.
     * @param a1 first factor of the first term
     * @param b1 second factor of the first term
     * @param a2 first factor of the second term
     * @param b2 second factor of the second term
     * @param a3 first factor of the third term
     * @param b3 second factor of the third term
     * @return a<sub>1</sub>×b<sub>1</sub> +
     * a<sub>2</sub>×b<sub>2</sub> + a<sub>3</sub>×b<sub>3</sub>
     * @see #linearCombination(double, Object, double, Object)
     * @see #linearCombination(double, Object, double, Object, double, Object, double, Object)
     * @since 3.2
     */
    fn linear_combination_f64_3_factors(&self,
                                        a1: f64,
                                        b1: &Dfp,
                                        a2: f64,
                                        b2: &Dfp,
                                        a3: f64,
                                        b3: &Dfp)
                                        -> Dfp {
        self.linear_combination_f64_dfp_3(a1, b1, a2, b2, a3, b3)
    }

    /**
     * Compute a linear combination.
     * @param a1 first factor of the first term
     * @param b1 second factor of the first term
     * @param a2 first factor of the second term
     * @param b2 second factor of the second term
     * @param a3 first factor of the third term
     * @param b3 second factor of the third term
     * @param a4 first factor of the third term
     * @param b4 second factor of the third term
     * @return a<sub>1</sub>×b<sub>1</sub> +
     * a<sub>2</sub>×b<sub>2</sub> + a<sub>3</sub>×b<sub>3</sub> +
     * a<sub>4</sub>×b<sub>4</sub>
     * @see #linearCombination(Object, Object, Object, Object)
     * @see #linearCombination(Object, Object, Object, Object, Object, Object)
     * @since 3.2
     */
    fn linear_combination_4_factors(&self,
                                    a1: &Dfp,
                                    b1: &Dfp,
                                    a2: &Dfp,
                                    b2: &Dfp,
                                    a3: &Dfp,
                                    b3: &Dfp,
                                    a4: &Dfp,
                                    b4: &Dfp)
                                    -> Dfp {
        self.linear_combination_dfp_dfp_4(a1, b1, a2, b2, a3, b3, a4, b4)
    }

    /**
     * Compute a linear combination.
     * @param a1 first factor of the first term
     * @param b1 second factor of the first term
     * @param a2 first factor of the second term
     * @param b2 second factor of the second term
     * @param a3 first factor of the third term
     * @param b3 second factor of the third term
     * @param a4 first factor of the third term
     * @param b4 second factor of the third term
     * @return a<sub>1</sub>×b<sub>1</sub> +
     * a<sub>2</sub>×b<sub>2</sub> + a<sub>3</sub>×b<sub>3</sub> +
     * a<sub>4</sub>×b<sub>4</sub>
     * @see #linearCombination(double, Object, double, Object)
     * @see #linearCombination(double, Object, double, Object, double, Object)
     * @since 3.2
     */
    fn linear_combination_f64_4_factors(&self,
                                        a1: f64,
                                        b1: &Dfp,
                                        a2: f64,
                                        b2: &Dfp,
                                        a3: f64,
                                        b3: &Dfp,
                                        a4: f64,
                                        b4: &Dfp)
                                        -> Dfp {
        self.linear_combination_f64_dfp_4(a1, b1, a2, b2, a3, b3, a4, b4)
    }
}

impl Dfp {
    /** Makes an instance with a value of zero.
     * @param field field to which this instance belongs
   */
    pub fn new(field: &Rc<DfpField>) -> Dfp {
        let mut res = Dfp {
            mant: Vec::with_capacity(field.as_ref().get_radix_digits()),
            sign: 1,
            exp: 0,
            nans: FINITE,
            field: field.clone(),
        };
        for _ in 0..res.mant.capacity() {
            res.mant.push(0)
        }
        res

    }

    /** Create an instance from a byte value.
     * @param field field to which this instance belongs
     * @param x value to convert to an instance
     */
    pub fn new_i8(field: &Rc<DfpField>, x: i8) -> Dfp {
        Dfp::new_i64(field, x as i64)
    }

    /** Create an instance from an int value.
     * @param field field to which this instance belongs
     * @param x value to convert to an instance
     */
    pub fn new_i32(field: &Rc<DfpField>, x: i32) -> Dfp {
        Dfp::new_i64(field, x as i64)
    }

    /** Creates an instance with a non-finite value.
     * @param field field to which this instance belongs
     * @param sign sign of the Dfp to create
     * @param nans code of the value, must be one of {@link #INFINITE},
     * {@link #SNAN},  {@link #QNAN}
     */
    pub fn new_sign_nans(field: &Rc<DfpField>, sign: i8, nans: i8) -> Dfp {
        let mut res = Dfp::new(field);
        res.sign = sign;
        res.nans = nans;
        res
    }

    /** Create an instance from a String representation.
     * @param field field to which this instance belongs
     * @param s string representation of the instance
     */
    pub fn new_str(field: &Rc<DfpField>, s: &str) -> Result<Dfp, String> {
        let mut res = Dfp::new(field);
        let mut decimal_found: bool = false;

        // Check some special cases
        if s == POS_INFINITY_STRING {
            res.sign = 1 as i8;
            res.nans = INFINITE;
            return Ok(res);
        }
        if s == NEG_INFINITY_STRING {
            res.sign = -1 as i8;
            res.nans = INFINITE;
            return Ok(res);
        }
        if s == NAN_STRING {
            res.sign = 1 as i8;
            res.nans = QNAN;
            return Ok(res);
        }
        // Check for scientific notation
        let mut p: usize = s.find("e").unwrap_or(usize::MAX);
        if p == usize::MAX {
            // try upper case?
            p = s.find("E").unwrap_or(usize::MAX);
        }

        let mut sciexp: i32 = 0;
        let fpdecimal = if p != usize::MAX {
            // scientific notation
            let tmp = s.split_at(p).0;
            let fpexp = s.split_at(p + 1).1;
            let mut negative: bool = false;
            let mut chariter = fpexp.chars();
            loop {
                match chariter.next() {
                    Some('-') => {
                        negative = true;
                    }
                    Some('+') => {
                        negative = false;
                    }
                    Some(c) => {
                        if c >= '0' && c <= '9' {
                            sciexp = sciexp * 10 + c.to_digit(10).unwrap() as i32;
                        } else {
                            return Err(format!("invalid exponent"));
                        }
                    }
                    None => break,
                }
            }
            if negative {
                sciexp = -sciexp;
            }
            tmp
        } else {
            // normal case
            s
        };
        // If there is a minus sign in the number then it is negative
        if fpdecimal.find("-").unwrap_or(usize::MAX) != usize::MAX {
            res.sign = -1;
        }
        // First off, find all of the leading zeros, trailing zeros, and significant digits
        p = 0;
        // Move p to first significant digit
        let mut decimal_pos: i32 = 0;
        loop {
            let c = fpdecimal.chars().nth(p).unwrap();
            if c >= '1' && c <= '9' {
                break;
            }
            if decimal_found && c == '0' {
                decimal_pos -= 1;
            }
            if c == '.' {
                decimal_found = true;
            }
            p += 1;
            if p == fpdecimal.len() {
                break;
            }
        }

        // size of radix in decimal digits
        let rsize: usize = 4;
        // Starting offset into Striped
        let offset: usize = 4;
        let mut striped = String::new(); //Vec::with_capacity(res.get_radix_digits() * rsize + offset * 2); // Copy the string onto Stripped
        let mut q: usize = offset;
        striped.push_str("0000");
        //        striped[0] = '0';
        //        striped[1] = '0';
        //        striped[2] = '0';
        //        striped[3] = '0';
        let mut significant_digits: usize = 0;
        loop {
            if p == fpdecimal.len() {
                break;
            }
            // Don't want to run pass the end of the array
            if q == res.mant.capacity() * rsize + offset + 1 {
                break;
            }
            let c = fpdecimal.chars().nth(p).unwrap();
            if c == '.' {
                decimal_found = true;
                decimal_pos = significant_digits as i32;
                p += 1;
                continue;
            }
            if c < '0' || c > '9' {
                p += 1;
                continue;
            }
            striped.push(c); // striped[q] = c;
            q += 1;
            p += 1;
            significant_digits += 1;
        }

        // If the decimal point has been found then get rid of trailing zeros.
        if decimal_found && q != offset {
            loop {
                q -= 1;
                if q == offset {
                    break;
                }
                if striped.chars().nth(q).unwrap() == '0' {
                    significant_digits -= 1;
                } else {
                    break;
                }
            }

        }
        // special case of numbers like "0.00000"
        if decimal_found && significant_digits == 0 {
            decimal_pos = 0;
        }
        // Implicit decimal point at end of number if not present
        if !decimal_found {
            decimal_pos = (q - offset) as i32;
        }
        // Find the number of significant trailing zeros
        // set q to point to first sig digit
        q = offset;
        p = offset + significant_digits - 1;
        while p > q {
            if striped.chars().nth(q).unwrap() != '0' {
                break;
            }
            p -= 1;
        }
        // Make sure the decimal is on a mod 10000 boundary
        let mut i: i32 = (((rsize * 100) as i32 - decimal_pos) as i32 - sciexp % rsize as i32) %
                         rsize as i32;
        q -= i as usize;
        decimal_pos += i;
        // Make the mantissa length right by adding zeros at the end if necessary
        while (p as i32 - q as i32) < (res.mant.capacity() * rsize) as i32 {
            {
                let mut i = 0usize;
                while i < rsize {
                    {
                        p += 1;
                        striped.insert(p, '0'); // [p] = '0';
                    }
                    i += 1;
                }
            }

        }
        // and where the least significant digit is
        {
            i = res.mant.capacity() as i32 - 1;
            res.mant.clear();
            while i >= 0 {
                {
                    res.mant.insert(0,
                                    (striped.chars().nth(q).unwrap().to_digit(10).unwrap() * 1000 +
                                     striped.chars().nth(q + 1).unwrap().to_digit(10).unwrap() *
                                     100 +
                                     striped.chars().nth(q + 2).unwrap().to_digit(10).unwrap() *
                                     10 +
                                     striped.chars()
                                        .nth(q + 3)
                                        .unwrap()
                                        .to_digit(10)
                                        .unwrap()) as i32);
                    q += 4;
                }
                i -= 1;
            }
        }

        res.exp = (decimal_pos + sciexp) / rsize as i32;
        if q < striped.len() {
            // Is there possible another digit?
            res.round(striped.chars().nth(q).unwrap().to_digit(10).unwrap() as i32 * 1000);
        }
        Ok(res)
    }


    /** Create an instance from a long value.
     * @param field field to which this instance belongs
     * @param x value to convert to an instance
     */
    pub fn new_i64(field: &Rc<DfpField>, x_p: i64) -> Dfp {

        let mut res = Dfp {
            mant: Vec::with_capacity(field.as_ref().get_radix_digits()),
            sign: 1,
            exp: 0,
            nans: FINITE,
            field: field.clone(),
        };

        // initialize as if 0
        let mut is_long_min: bool = false;
        let mut x = x_p;
        if x == MIN_I64 {
            // special case for Long.MIN_VALUE (-9223372036854775808)
            // we must shift it before taking its absolute value
            is_long_min = true;
            x += 1;
        }
        // set the sign
        if x < 0 {
            res.sign = -1;
            x = -x;
        } else {
            res.sign = 1;
        }
        res.exp = 0;
        let mut tmp_x = x;
        while tmp_x != 0 {
            res.exp += 1;
            tmp_x /= RADIX_L;
        }
        for _ in 0..(res.mant.capacity() as i32 - res.exp) {
            res.mant.push(0);
        }
        for _ in (0..res.exp).rev() {
            let nopart = x % RADIX_L;
            res.mant.push(nopart as i32);
            x /= RADIX_L;
        }
        if is_long_min {
            // we know in this case that fixing the last digit is sufficient
            let mut i: i32 = 0;
            while i < res.mant.capacity() as i32 - 1 {
                if res.mant[i as usize] != 0 {
                    res.mant[i as usize] += 1;
                    break;
                }
                i += 1;
            }
        }
        res
    }


    /** Create an instance from a double value.
     * @param field field to which this instance belongs
     * @param x value to convert to an instance
     */
    pub fn new_f64(field: &Rc<DfpField>, x: f64) -> Dfp {
        // initialize as if 0

        let mut res = Dfp {
            mant: Vec::with_capacity(field.as_ref().get_radix_digits()),
            sign: 1,
            exp: 0,
            nans: FINITE,
            field: field.clone(),
        };

        let bits: u64 = double_to_raw_long_bits(&x);
        let mut mantissa: i64 = (bits & 0x000fffffffffffff) as i64;
        let mut exponent: i32 = ((bits & 0x7ff0000000000000) >> 52) as i32 - 1023;
        if exponent == -1023 {
            // Zero or sub-normal
            if x == 0.0 {
                // make sure 0 has the right sign
                if (bits & 0x8000000000000000) != 0 {
                    res.sign = -1;
                }
                for _ in 0..res.mant.capacity() {
                    res.mant.push(0)
                }
                return res;
            }
            exponent += 1;
            // Normalize the subnormal number
            while (mantissa & 0x0010000000000000) == 0 {
                exponent -= 1;
                mantissa <<= 1;
            }
            mantissa &= 0x000fffffffffffff;
        }
        if exponent == 1024 {
            // infinity or NAN
            if x != x {
                res.sign = 1 as i8;
                res.nans = QNAN;
            } else if x < 0.0 {
                res.sign = -1 as i8;
                res.nans = INFINITE;
            } else {
                res.sign = 1 as i8;
                res.nans = INFINITE;
            }
            for _ in 0..res.mant.capacity() {
                res.mant.push(0)
            }
            return res;
        }
        let mut xdfp: Dfp = Dfp::new_i64(field, mantissa);
        let divisor = Dfp::new_i64(field, 4503599627370496i64);
        xdfp = xdfp.divide(&divisor).unwrap().add(&field.get_one());
        xdfp = xdfp.multiply(&DfpMath::pow_i32(&field.get_two(), exponent));
        if (bits & 0x8000000000000000) != 0 {
            xdfp = xdfp.negate();
        }
        Dfp {
            mant: xdfp.mant,
            sign: xdfp.sign,
            exp: xdfp.exp,
            nans: res.nans,
            field: field.clone(),
        }
        // Divide by 2^52, then add one
    }

    /** Copy constructor.
     * @param d instance to copy
     */
    pub fn clone(&self) -> Dfp {
        Dfp {
            mant: self.mant.clone(),
            sign: self.sign,
            exp: self.exp,
            nans: self.nans,
            field: self.field.clone(),
        }
    }





    /** Divide by a single digit less than radix.
     *  Special case, so there are speed advantages. 0 <= divisor < radix
     * @param divisor divisor
     * @return quotient of this by divisor
     */
    pub fn divide_i32(&self, divisor: i32) -> Dfp {
        // Handle special cases
        if self.nans != FINITE {
            if self.is_na_n() {
                return self.clone();
            }
            if self.nans == INFINITE {
                return self.clone();
            }
        }
        // Test for divide by zero
        if divisor == 0 {
            self.field.set_i_e_e_e_flags_bits(FLAG_DIV_ZERO);
            let mut result: Dfp = self.get_zero().clone();
            result.sign = self.sign;
            result.nans = INFINITE;
            result = self.dotrap(FLAG_DIV_ZERO, DIVIDE_TRAP, &self.get_zero(), &result);
            return result;
        }
        // range check divisor
        if divisor < 0 || divisor >= RADIX {
            self.field.set_i_e_e_e_flags_bits(FLAG_INVALID);
            let mut result: Dfp = self.get_zero();
            result.nans = QNAN;
            result = self.dotrap(FLAG_INVALID, DIVIDE_TRAP, &result, &result);
            return result;
        }
        let mut result: Dfp = self.clone();
        let mut rl: i32 = 0;
        {
            let mut i: i32 = self.mant.len() as i32 - 1;
            while i >= 0 {
                {
                    let r: i32 = rl * RADIX + result.mant[i as usize];
                    let rh: i32 = r / divisor;
                    rl = r - rh * divisor;
                    result.mant[i as usize] = rh;
                }
                i -= 1;
            }
        }

        if result.mant[self.mant.len() - 1] == 0 {
            // normalize
            result.shift_left();
            // compute the next digit and put it in
            let r: i32 = rl * RADIX;
            let rh: i32 = r / divisor;
            rl = r - rh * divisor;
            result.mant[0] = rh;
        }
        // do the rounding
        let excp: i32 = result.round(rl * RADIX / divisor);
        if excp != 0 {
            result = self.dotrap(excp, DIVIDE_TRAP, &result, &result);
        }
        return result;
    }

    /** Compute the square root.
     * @return square root of the instance
     * @since 3.2
     */
    pub fn sqrt(&self) -> Dfp {
        // check for unusual cases
        if self.nans == FINITE && self.mant[self.mant.len() - 1] == 0 {
            // if zero
            return self.clone();
        }
        if self.nans != FINITE {
            if self.nans == INFINITE && self.sign == 1 {
                // if positive infinity
                return self.clone();
            }
            if self.nans == QNAN {
                return self.clone();
            }
            if self.nans == SNAN {
                let mut result: Dfp;
                self.field.set_i_e_e_e_flags_bits(FLAG_INVALID);
                result = self.clone();
                result = self.dotrap(FLAG_INVALID, SQRT_TRAP, self, &result);
                return result;
            }
        }
        if self.sign == -1 {
            // if negative
            let mut result: Dfp;
            self.field.set_i_e_e_e_flags_bits(FLAG_INVALID);
            result = self.clone();
            result.nans = QNAN;
            result = self.dotrap(FLAG_INVALID, SQRT_TRAP, &result, &result);
            return result;
        }
        let mut x: Dfp = self.clone();
        // Lets make a reasonable guess as to the size of the square root
        if x.exp < -1 || x.exp > 1 {
            x.exp = self.exp / 2;
        }
        // Coarsely estimate the mantissa
        match x.mant[self.mant.len() - 1] / 2000 {
            0 => {
                x.mant[self.mant.len() - 1] = x.mant[self.mant.len() - 1] / 2 + 1;
            }
            2 => {
                x.mant[self.mant.len() - 1] = 1500;
            }
            3 => {
                x.mant[self.mant.len() - 1] = 2200;
            }
            _ => {
                x.mant[self.mant.len() - 1] = 3000;
            }
        }
        let mut dx: Dfp;
        // Now that we have the first pass estimate, compute the rest
        // by the formula dx = (y - x*x) / (2x);
        let mut px: Dfp = self.get_zero();
        let mut ppx: Dfp;
        while x != px {
            dx = self.new_instance_dfp(&x);
            dx.sign = -1;
            dx = dx.add(&self.clone().divide(&x).unwrap());
            dx = dx.divide_i32(2);
            ppx = px;
            px = x.clone();
            x = x.add(&dx);
            if x == ppx {
                // alternating between two values
                break;
            }
            // is a sufficient test since dx is normalized
            if dx.mant[self.mant.len() - 1] == 0 {
                break;
            }
        }
        return x;
    }

    /** Get a string representation of the instance.
     * @return string representation of the instance
     */
    pub fn to_string(&self) -> String {
        if self.nans != FINITE {
            // if non-finite exceptional cases
            return String::from(if self.nans == INFINITE {
                if self.sign < 0 {
                    NEG_INFINITY_STRING
                } else {
                    POS_INFINITY_STRING
                }
            } else {
                NAN_STRING
            });
        }
        if self.exp > self.mant.len() as i32 || self.exp < -1 {
            return self.dfp2sci();
        }
        return self.dfp2string();
    }

    /** Convert an instance to a string using scientific notation.
     * @return string representation of the instance in scientific notation
     */
    pub fn dfp2sci(&self) -> String {
        let mut rawdigits = String::new(); // self.mant.len() * 4] = [None; self.mant.len() * 4];
        let mut outputbuffer = String::new(); // [Option<char>; self.mant.len() * 4 + 20] = [None; self.mant.len() * 4 + 20];
        let mut p: usize;
        let e: i32;
        let mut ae: i32;
        let shf: usize;
        // Get all the digits
        {
            let mut i: usize = self.mant.len() - 1;
            loop {
                {
                    rawdigits.push(from_u32((self.mant[i] / 1000) as u32 + '0' as u32).unwrap());
                    rawdigits.push(from_u32(((self.mant[i] / 100) % 10) as u32 + '0' as u32)
                                       .unwrap());
                    rawdigits.push(from_u32(((self.mant[i] / 10) % 10) as u32 + '0' as u32)
                                       .unwrap());
                    rawdigits.push(from_u32(((self.mant[i]) % 10) as u32 + '0' as u32).unwrap());
                }
                if i == 0 {
                    break;
                }
                i -= 1;
            }
        }

        if self.sign == -1 {
            outputbuffer.push('-');
        }
        let mut rawdigits_iter = rawdigits.chars();
        let mut eoc: bool = false;
        // Find the first non-zero one
        {
            p = 0;
            while p < rawdigits.len() {
                {
                    match rawdigits_iter.next() {
                        Some('0') => {}
                        Some(c) => {
                            outputbuffer.push(c);
                            outputbuffer.push('.');
                            break;
                        }
                        _ => eoc = true,
                    }
                }
                p += 1;
            }
            shf = p;
            if eoc {
                outputbuffer.push('0');
                outputbuffer.push('.');
                outputbuffer.push('0');
                outputbuffer.push('e');
                outputbuffer.push('0');
                return outputbuffer; // , 0, 5);
            }
            while p < rawdigits.len() && !eoc {
                {
                    match rawdigits_iter.next() {
                        Some(c) => outputbuffer.push(c),
                        _ => eoc = true,
                    }
                }
                p += 1;
            }
        }


        // Now do the conversion
        outputbuffer.push('e');
        // Find the msd of the exponent
        e = self.exp * 4 - shf as i32 - 1;
        ae = e;
        if e < 0 {
            ae = -e;
        }
        // Find the largest p such that p < e
        {
            p = 1000000000;
            while p > ae as usize {
                {
                    // nothing to do
                }
                p /= 10;
            }
        }

        if e < 0 {
            outputbuffer.push('-');
        }
        while p > 0 {
            outputbuffer.push(from_u32((ae / p as i32) as u32 + '0' as u32).unwrap());
            ae %= p as i32;
            p /= 10;
        }
        return outputbuffer;
    }

    /** Convert an instance to a string using normal notation.
     * @return string representation of the instance in normal notation
     */
    pub fn dfp2string(&self) -> String {
        let mut buffer = String::new(); // [Option<char>; self.mant.len() * 4 + 20] = [None; self.mant.len() * 4 + 20];
        let mut e: i32 = self.exp;
        let mut point_inserted: bool = false;
        if e <= 0 {
            buffer.push('0');
            buffer.push('.');
            point_inserted = true;
        }
        while e < 0 {
            buffer.push('0');
            buffer.push('0');
            buffer.push('0');
            buffer.push('0');
            e += 1;
        }
        {
            let mut i: usize = self.mant.len() - 1;
            loop {
                {
                    buffer.push(from_u32((self.mant[i] / 1000) as u32 + '0' as u32).unwrap());
                    buffer.push(from_u32(((self.mant[i] / 100) % 10) as u32 + '0' as u32).unwrap());
                    buffer.push(from_u32(((self.mant[i] / 10) % 10) as u32 + '0' as u32).unwrap());
                    buffer.push(from_u32(((self.mant[i]) % 10) as u32 + '0' as u32).unwrap());

                    e -= 1;
                    if e == 0 {
                        buffer.push('.');
                        point_inserted = true;
                    }
                }
                if i == 0 {
                    break;
                }
                i -= 1;
            }
        }

        while e > 0 {
            buffer.push('0');
            buffer.push('0');
            buffer.push('0');
            buffer.push('0');
            e -= 1;
        }
        if !point_inserted {
            // Ensure we have a radix point!
            buffer.push('.');
        }
        let mut result = String::new();
        let mut skipped = buffer.chars().skip_while(|c| *c == '0').peekable();
        if self.sign < 0 {
            result.push('-');
        }
        if skipped.peek() == Some(&'.') {
            result.push('0');
        }

        loop {
            match skipped.next() {
                Some(c) => result.push(c),
                None => break,
            }
        }
        while result.ends_with("0") {
            result.pop();
        }
        return result;
    }


    /** Compare two instances.
     * @param a first instance in comparison
     * @param b second instance in comparison
     * @return -1 if a<b, 1 if a>b and 0 if a==b
     *  Note this method does not properly handle NaNs or numbers with different precision.
     */
    fn compare(a: &Dfp, b: &Dfp) -> i32 {
        // Ignore the sign of zero
        if a.mant[a.mant.len() - 1] == 0 && b.mant[b.mant.len() - 1] == 0 && a.nans == FINITE &&
           b.nans == FINITE {
            return 0;
        }
        if a.sign != b.sign {
            if a.sign == -1 {
                return -1;
            } else {
                return 1;
            }
        }
        // deal with the infinities
        if a.nans == INFINITE && b.nans == FINITE {
            return a.sign as i32;
        }
        if a.nans == FINITE && b.nans == INFINITE {
            return -b.sign as i32;
        }
        if a.nans == INFINITE && b.nans == INFINITE {
            return 0;
        }
        // Handle special case when a or b is zero, by ignoring the exponents
        if b.mant[b.mant.len() - 1] != 0 && a.mant[b.mant.len() - 1] != 0 {
            if a.exp < b.exp {
                return -a.sign as i32;
            }
            if a.exp > b.exp {
                return a.sign as i32;
            }
        }
        // compare the mantissas
        {
            let mut i: i32 = a.mant.len() as i32 - 1;
            while i >= 0 {
                let index = i as usize;
                {
                    if a.mant[index] > b.mant[index] {
                        return a.sign as i32;
                    }
                    if a.mant[index] < b.mant[index] {
                        return -a.sign as i32;
                    }
                }
                i -= 1;
            }
        }

        return 0;
    }

    /** Create an instance with a value of 0.
     * Use this internally in preference to constructors to facilitate subclasses
     * @return a new instance with a value of 0
     */
    pub fn new_instance(&self) -> Dfp {
        return Dfp::new(self.get_field());
    }

    /** Create an instance from a byte value.
     * @param x value to convert to an instance
     * @return a new instance with value x
     */
    pub fn new_instance_i8(&self, x: i8) -> Dfp {
        return Dfp::new_i8(self.get_field(), x);
    }

    /** Create an instance from an int value.
     * @param x value to convert to an instance
     * @return a new instance with value x
     */
    pub fn new_instance_i32(&self, x: i32) -> Dfp {
        return Dfp::new_i32(self.get_field(), x);
    }

    /** Create an instance from a long value.
     * @param x value to convert to an instance
     * @return a new instance with value x
     */
    pub fn new_instance_i64(&self, x: i64) -> Dfp {
        return Dfp::new_i64(self.get_field(), x);
    }

    /** Create an instance from a double value.
     * @param x value to convert to an instance
     * @return a new instance with value x
     */
    pub fn new_instance_f64(&self, x: f64) -> Dfp {
        return Dfp::new_f64(self.get_field(), x);
    }

    /** Create an instance by copying an existing one.
     * Use this internally in preference to constructors to facilitate subclasses.
     * @param d instance to copy
     * @return a new instance with the same value as d
     */
    pub fn new_instance_dfp(&self, d: &Dfp) -> Dfp {
        // make sure we don't mix number with different precision
        if self.field.get_radix_digits() != d.field.get_radix_digits() {
            self.field.set_i_e_e_e_flags_bits(FLAG_INVALID);
            let mut result: Dfp = self.new_instance_dfp(&self.get_zero());
            result.nans = QNAN;
            return self.dotrap(FLAG_INVALID, NEW_INSTANCE_TRAP, d, &result);
        }
        return d.clone();
    }

    /** Create an instance from a String representation.
     * Use this internally in preference to constructors to facilitate subclasses.
     * @param s string representation of the instance
     * @return a new instance parsed from specified string
     */
    pub fn new_instance_str(&self, s: &str) -> Result<Dfp, String> {
        return Dfp::new_str(self.get_field(), s);
    }

    /** Creates an instance with a non-finite value.
     * @param sig sign of the Dfp to create
     * @param code code of the value, must be one of {@link #INFINITE},
     * {@link #SNAN},  {@link #QNAN}
     * @return a new instance with a non-finite value
     */
    pub fn new_instance_sig_code(&self, sig: i8, code: i8) -> Dfp {
        return self.field.new_dfp_sign_nans(sig, code);
    }

    /** Get the {@link org.apache.commons.math3.Field Field} (really a {@link DfpField}) to which the instance belongs.
     * <p>
     * The field is linked to the number of digits and acts as a factory
     * for {@link Dfp} instances.
     * </p>
     * @return {@link org.apache.commons.math3.Field Field} (really a {@link DfpField}) to which the instance belongs
     */
    pub fn get_field(&self) -> &Rc<DfpField> {
        return &self.field;
    }

    /** Get the number of radix digits of the instance.
     * @return number of radix digits
     */
    pub fn get_radix_digits(&self) -> usize {
        return self.field.get_radix_digits();
    }

    /** Get the constant 0.
     * @return a Dfp with value zero
     */
    pub fn get_zero(&self) -> Dfp {
        return self.field.get_zero();
    }

    /** Get the constant 1.
     * @return a Dfp with value one
     */
    pub fn get_one(&self) -> Dfp {
        return self.field.get_one();
    }

    /** Get the constant 2.
     * @return a Dfp with value two
     */
    pub fn get_two(&self) -> Dfp {
        return self.field.get_two();
    }



    /** Shift the mantissa left, and adjust the exponent to compensate.
     */
    pub fn shift_left(&mut self) {
        {
            let mut i: usize = self.mant.len() - 1;
            while i > 0 {
                {
                    self.mant[i] = self.mant[i - 1];
                }
                i -= 1;
            }
        }

        self.mant[0] = 0;
        self.exp -= 1;
    }

    // Note that shiftRight() does not call round() as that round() itself
    // uses shiftRight()
    /** Shift the mantissa right, and adjust the exponent to compensate.
     */
    pub fn shift_right(&mut self) {
        let len = self.mant.len();
        let mut i: usize = 0;
        while i < len - 1 {
            {
                self.mant[i] = self.mant[i + 1];
            }
            i += 1;
        }

        self.mant[len - 1] = 0;
        self.exp += 1;
    }


    /** Make our exp equal to the supplied one, this may cause rounding.
     *  Also causes de-normalized numbers.  These numbers are generally
     *  dangerous because most routines assume normalized numbers.
     *  Align doesn't round, so it will return the last digit destroyed
     *  by shifting right.
     *  @param e desired exponent
     *  @return last digit destroyed by shifting right
     */
    pub fn align(&mut self, e: i32) -> i32 {
        let mut lostdigit: i32 = 0;
        let mut inexact: bool = false;
        let diff: i32 = self.exp - e;
        let mut adiff: i32 = diff;
        if adiff < 0 {
            adiff = -adiff;
        }
        if diff == 0 {
            return 0;
        }
        if adiff as usize > (self.mant.len() + 1) {
            // Special case
            let len = self.mant.len();
            self.mant.clear();
            for _ in 0..len {
                self.mant.push(0);
            }
            self.exp = e;
            self.field.set_i_e_e_e_flags_bits(FLAG_INEXACT);
            self.dotrap(FLAG_INEXACT, ALIGN_TRAP, self, self);
            return 0;
        }
        {
            let mut i: i32 = 0;
            while i < adiff {
                {
                    if diff < 0 {
                        // Keep track of loss -- only signal inexact after losing 2 digits.
                        // the first lost digit is returned to add() and may be incorporated
                        // into the result.
                        //
                        if lostdigit != 0 {
                            inexact = true;
                        }
                        lostdigit = self.mant[0];
                        self.shift_right();
                    } else {
                        self.shift_left();
                    }
                }
                i += 1;
            }
        }

        if inexact {
            self.field.set_i_e_e_e_flags_bits(FLAG_INEXACT);
            self.dotrap(FLAG_INEXACT, ALIGN_TRAP, self, self);
        }
        return lostdigit;
    }

    /** Check if instance is less than x.
     * @param x number to check instance against
     * @return true if instance is less than x and neither are NaN, false otherwise
     */
    pub fn less_than(&self, x: &Dfp) -> bool {
        // make sure we don't mix number with different precision
        if self.field.get_radix_digits() != x.field.get_radix_digits() {
            self.field.set_i_e_e_e_flags_bits(FLAG_INVALID);
            let mut result: Dfp = self.new_instance_dfp(&self.get_zero());
            result.nans = QNAN;
            self.dotrap(FLAG_INVALID, LESS_THAN_TRAP, x, &result);
            return false;
        }
        // if a nan is involved, signal invalid and return false
        if self.is_na_n() || x.is_na_n() {
            self.field.set_i_e_e_e_flags_bits(FLAG_INVALID);
            self.dotrap(FLAG_INVALID, LESS_THAN_TRAP, x, &self.get_zero().clone());
            return false;
        }
        return Dfp::compare(self, x) < 0;
    }

    /** Check if instance is greater than x.
     * @param x number to check instance against
     * @return true if instance is greater than x and neither are NaN, false otherwise
     */
    pub fn greater_than(&self, x: &Dfp) -> bool {
        // make sure we don't mix number with different precision
        if self.field.get_radix_digits() != x.field.get_radix_digits() {
            self.field.set_i_e_e_e_flags_bits(FLAG_INVALID);
            let mut result: Dfp = self.new_instance_dfp(&self.get_zero());
            result.nans = QNAN;
            self.dotrap(FLAG_INVALID, GREATER_THAN_TRAP, x, &result);
            return false;
        }
        // if a nan is involved, signal invalid and return false
        if self.is_na_n() || x.is_na_n() {
            self.field.set_i_e_e_e_flags_bits(FLAG_INVALID);
            self.dotrap(FLAG_INVALID, GREATER_THAN_TRAP, x, &self.get_zero().clone());
            return false;
        }
        return Dfp::compare(self, x) > 0;
    }

    /** Check if instance is less than or equal to 0.
     * @return true if instance is not NaN and less than or equal to 0, false otherwise
     */
    pub fn negative_or_null(&self) -> bool {
        if self.is_na_n() {
            self.field.set_i_e_e_e_flags_bits(FLAG_INVALID);
            self.dotrap(FLAG_INVALID, LESS_THAN_TRAP, self, &self.get_zero().clone());
            return false;
        }
        return (self.sign < 0) || ((self.mant[self.mant.len() - 1] == 0) && !self.is_infinite());
    }

    /** Check if instance is strictly less than 0.
     * @return true if instance is not NaN and less than or equal to 0, false otherwise
     */
    pub fn strictly_negative(&self) -> bool {
        if self.is_na_n() {
            self.field.set_i_e_e_e_flags_bits(FLAG_INVALID);
            self.dotrap(FLAG_INVALID, LESS_THAN_TRAP, self, &self.get_zero().clone());
            return false;
        }
        return (self.sign < 0) && ((self.mant[self.mant.len() - 1] != 0) || self.is_infinite());
    }

    /** Check if instance is greater than or equal to 0.
     * @return true if instance is not NaN and greater than or equal to 0, false otherwise
     */
    pub fn positive_or_null(&self) -> bool {
        if self.is_na_n() {
            self.field.set_i_e_e_e_flags_bits(FLAG_INVALID);
            self.dotrap(FLAG_INVALID, LESS_THAN_TRAP, self, &self.get_zero().clone());
            return false;
        }
        return (self.sign > 0) || ((self.mant[self.mant.len() - 1] == 0) && !self.is_infinite());
    }

    /** Check if instance is strictly greater than 0.
     * @return true if instance is not NaN and greater than or equal to 0, false otherwise
     */
    pub fn strictly_positive(&self) -> bool {
        if self.is_na_n() {
            self.field.set_i_e_e_e_flags_bits(FLAG_INVALID);
            self.dotrap(FLAG_INVALID, LESS_THAN_TRAP, self, &self.get_zero().clone());
            return false;
        }
        return (self.sign > 0) && ((self.mant[self.mant.len() - 1] != 0) || self.is_infinite());
    }


    /** Get the absolute value of instance.
     * @return absolute value of instance
     * @since 3.2
     */
    pub fn abs(&self) -> Dfp {
        let mut result: Dfp = self.clone();
        result.sign = 1;
        return result;
    }

    /** Convert this to an integer.
     * If greater than 2147483647, it returns 2147483647. If less than -2147483648 it returns -2147483648.
     * @return converted number
     */
    pub fn int_value(&self) -> i32 {
        let rounded: Dfp;
        let mut result: i32 = 0;
        rounded = self.rint();
        if rounded.greater_than(&self.new_instance_i64(2147483647)) {
            return 2147483647;
        }
        if rounded.less_than(&self.new_instance_i64(-2147483648)) {
            return -2147483648;
        }
        {
            let mut i: usize = self.mant.len() - 1;
            while i >= self.mant.len() - rounded.exp as usize {
                {
                    result = result * RADIX + rounded.mant[i];
                }
                i -= 1;
            }
        }

        if rounded.sign == -1 {
            result = -result;
        }
        return result;
    }

    /** Get the exponent of the greatest power of 10000 that is
     *  less than or equal to the absolute value of this.  I.E.  if
     *  this is 10<sup>6</sup> then log10K would return 1.
     *  @return integer base 10000 logarithm
     */
    pub fn log10_k(&self) -> i32 {
        return self.exp - 1;
    }

    /** Get the specified  power of 10000.
     * @param e desired power
     * @return 10000<sup>e</sup>
     */
    pub fn power10_k(&self, e: i32) -> Dfp {
        let mut d: Dfp = self.get_one().clone();
        d.exp = e + 1;
        return d;
    }

    /** Get the exponent of the greatest power of 10 that is less than or equal to abs(this).
     *  @return integer base 10 logarithm
     * @since 3.2
     */
    pub fn int_log10(&self) -> i32 {
        if self.mant[self.mant.len() - 1] > 1000 {
            return self.exp * 4 - 1;
        }
        if self.mant[self.mant.len() - 1] > 100 {
            return self.exp * 4 - 2;
        }
        if self.mant[self.mant.len() - 1] > 10 {
            return self.exp * 4 - 3;
        }
        return self.exp * 4 - 4;
    }

    /** Return the specified  power of 10.
     * @param e desired power
     * @return 10<sup>e</sup>
     */
    pub fn power10(&self, e: i32) -> Dfp {
        let mut d: Dfp = self.get_one().clone();
        if e >= 0 {
            d.exp = e / 4 + 1;
        } else {
            d.exp = (e + 1) / 4;
        }
        match (e % 4 + 4) % 4 {
            0 => {}
            1 => {
                d = d.multiply_i32(10);

            }
            2 => {
                d = d.multiply_i32(100);

            }
            _ => {
                d = d.multiply_i32(1000);
            }
        }
        return d;
    }

    /** Multiply this by a single digit 0<=x<radix.
     * There are speed advantages in this special case.
     * @param x multiplicand
     * @return product of this and x
     */
    fn multiply_fast(&self, x: i32) -> Dfp {
        let mut result: Dfp = self.clone();
        // handle special cases
        if self.nans != FINITE {
            if self.is_na_n() {
                return self.clone();
            }
            if self.nans == INFINITE && x != 0 {
                result = self.clone();
                return result;
            }
            if self.nans == INFINITE && x == 0 {
                self.field.set_i_e_e_e_flags_bits(FLAG_INVALID);
                result = self.get_zero().clone();
                result.nans = QNAN;
                result = self.dotrap(FLAG_INVALID,
                                     MULTIPLY_TRAP,
                                     &self.get_zero().clone(),
                                     &result);
                return result;
            }
        }
        // range check x
        if x < 0 || x >= RADIX {
            self.field.set_i_e_e_e_flags_bits(FLAG_INVALID);
            result = self.get_zero().clone();
            result.nans = QNAN;
            result = self.dotrap(FLAG_INVALID, MULTIPLY_TRAP, &result, &result);
            return result;
        }
        let mut rh: i32 = 0;
        {
            let mut i: usize = 0;
            while i < self.mant.len() {
                {
                    let r: i32 = self.mant[i] * x + rh;
                    rh = r / RADIX;
                    result.mant[i] = r - rh * RADIX;
                }
                i += 1;
            }
        }

        let mut lostdigit: i32 = 0;
        if rh != 0 {
            lostdigit = result.mant[0];
            result.shift_right();
            result.mant[self.mant.len() - 1] = rh;
        }
        if result.mant[self.mant.len() - 1] == 0 {
            // if result is zero, set exp to zero
            result.exp = 0;
        }
        let excp: i32 = result.round(lostdigit);
        if excp != 0 {
            result = self.dotrap(excp, MULTIPLY_TRAP, &result, &result);
        }
        return result;
    }


    /** Negate the mantissa of this by computing the complement.
     *  Leaves the sign bit unchanged, used internally by add.
     *  Denormalized numbers are handled properly here.
     *  @param extra ???
     *  @return ???
     */
    pub fn complement(&mut self, extra_p: i32) -> i32 {
        let mut extra = RADIX - extra_p;
        {
            let mut i: usize = 0;
            while i < self.mant.len() {
                {
                    self.mant[i] = RADIX - self.mant[i] - 1;
                }
                i += 1;
            }
        }

        let mut rh: i32 = extra / RADIX;
        extra -= rh * RADIX;
        {
            let mut i: usize = 0;
            while i < self.mant.len() {
                {
                    let r: i32 = self.mant[i] + rh;
                    rh = r / RADIX;
                    self.mant[i] = r - rh * RADIX;
                }
                i += 1;
            }
        }

        return extra;
    }

    /** Round this given the next digit n using the current rounding mode.
     * @param n ???
     * @return the IEEE flag if an exception occurred
     */
    pub fn round(&mut self, n: i32) -> i32 {
        let inc: bool;
        let len = self.mant.len();
        match self.field.get_rounding_mode() {
            RoundingMode::ROUND_DOWN => {
                inc = false;
            }
            RoundingMode::ROUND_UP => {
                // round up if n!=0
                inc = n != 0;
            }
            RoundingMode::ROUND_HALF_UP => {
                // round half up
                inc = n >= 5000;
            }
            RoundingMode::ROUND_HALF_DOWN => {
                // round half down
                inc = n > 5000;
            }
            RoundingMode::ROUND_HALF_EVEN => {
                // round half-even
                inc = n > 5000 || (n == 5000 && (self.mant[0] & 1) == 1);
            }
            RoundingMode::ROUND_HALF_ODD => {
                // round half-odd
                inc = n > 5000 || (n == 5000 && (self.mant[0] & 1) == 0);
            }
            RoundingMode::ROUND_CEIL => {
                // round ceil
                inc = self.sign == 1 && n != 0;
            }
            // ROUND_FLOOR =>
            _ => {
                // round floor
                inc = self.sign == -1 && n != 0;
            }
        }
        if inc {
            // increment if necessary
            let mut rh: i32 = 1;
            {
                let mut i: usize = 0;
                while i < self.mant.len() {
                    {
                        let r: i32 = self.mant[i] + rh;
                        rh = r / RADIX;
                        self.mant[i] = r - rh * RADIX;
                    }
                    i += 1;
                }
            }

            if rh != 0 {
                self.shift_right();
                self.mant[len - 1] = rh;
            }
        }
        // check for exceptional cases and raise signals if necessary
        if self.exp < MIN_EXP {
            // Gradual Underflow
            self.field.set_i_e_e_e_flags_bits(FLAG_UNDERFLOW);
            return FLAG_UNDERFLOW;
        }
        if self.exp > MAX_EXP {
            // Overflow
            self.field.set_i_e_e_e_flags_bits(FLAG_OVERFLOW);
            return FLAG_OVERFLOW;
        }
        if n != 0 {
            // Inexact
            self.field.set_i_e_e_e_flags_bits(FLAG_INEXACT);
            return FLAG_INEXACT;
        }
        return 0;
    }





    /** Raises a trap.  This does not set the corresponding flag however.
     *  @param type the trap type
     *  @param what - name of routine trap occurred in
     *  @param oper - input operator to function
     *  @param result - the result computed prior to the trap
     *  @return The suggested return value from the trap handler
     */
    pub fn dotrap(&self, exception_type: i32, what: &str, oper: &Dfp, result_p: &Dfp) -> Dfp {
        let mut result = result_p.clone();
        let mut def: Dfp = result.clone();
        match exception_type {
            FLAG_INVALID => {
                def = self.field.get_zero();
                def.sign = result.sign;
                def.nans = QNAN;
            }
            FLAG_DIV_ZERO => {
                if self.nans == FINITE && self.mant[self.mant.len() - 1] != 0 {
                    // normal case, we are finite, non-zero
                    def = self.field.get_zero();
                    def.sign = (self.sign * oper.sign) as i8;
                    def.nans = INFINITE;
                }
                if self.nans == FINITE && self.mant[self.mant.len() - 1] == 0 {
                    // 0/0
                    def = self.field.get_zero();
                    def.nans = QNAN;
                }
                if self.nans == INFINITE || self.nans == QNAN {
                    def = self.field.get_zero();
                    def.nans = QNAN;
                }
                if self.nans == INFINITE || self.nans == SNAN {
                    def = self.field.get_zero();
                    def.nans = QNAN;
                }
            }
            FLAG_UNDERFLOW => {
                if (result.exp + self.mant.len() as i32) < MIN_EXP {
                    def = self.field.get_zero();
                    def.sign = result.sign;
                } else {
                    // gradual underflow
                    def = result.clone();
                }
                result.exp += ERR_SCALE;
            }
            FLAG_OVERFLOW => {
                result.exp -= ERR_SCALE;
                def = self.field.get_zero();
                def.sign = result.sign;
                def.nans = INFINITE;
            }
            _ => {
                return def;
            }

        }
        self.trap(exception_type, what, oper, &def, &result)
    }

    /** Trap handler.  Subclasses may override this to provide trap
     *  functionality per IEEE 854-1987.
     *
     *  @param type  The exception type - e.g. FLAG_OVERFLOW
     *  @param what  The name of the routine we were in e.g. divide()
     *  @param oper  An operand to this function if any
     *  @param def   The default return value if trap not enabled
     *  @param result    The result that is specified to be delivered per
     *                   IEEE 854, if any
     *  @return the value that should be return by the operation triggering the trap
     */
    #[allow(unused_variables)]
    pub fn trap(&self,
                exception_type: i32,
                what: &str,
                oper: &Dfp,
                def: &Dfp,
                result: &Dfp)
                -> Dfp {
        def.clone()
    }



    /** Returns the type - one of FINITE, INFINITE, SNAN, QNAN.
     * @return type of the number
     */
    pub fn classify(&self) -> i32 {
        return self.nans as i32;
    }

    /** Creates an instance that is the same as x except that it has the sign of y.
     * abs(x) = dfp.copysign(x, dfp.one)
     * @param x number to get the value from
     * @param y number to get the sign from
     * @return a number with the value of x and the sign of y
     */
    pub fn copysign(x: &Dfp, y: &Dfp) -> Dfp {
        let mut result: Dfp = x.clone();
        result.sign = y.sign;
        return result;
    }

    /** Returns the next number greater than this one in the direction of x.
     * If this==x then simply returns this.
     * @param x direction where to look at
     * @return closest number next to instance in the direction of x
     */
    pub fn next_after(&self, x: &Dfp) -> Dfp {
        // make sure we don't mix number with different precision
        if self.field.get_radix_digits() != x.field.get_radix_digits() {
            self.field.set_i_e_e_e_flags_bits(FLAG_INVALID);
            let mut result: Dfp = self.get_zero().clone();
            result.nans = QNAN;
            return self.dotrap(FLAG_INVALID, NEXT_AFTER_TRAP, x, &result);
        }
        // if this is greater than x
        let mut up: bool = false;
        if self.less_than(x) {
            up = true;
        }
        if Dfp::compare(self, x) == 0 {
            return self.new_instance_dfp(x);
        }
        if self.less_than(&self.get_zero()) {
            up = !up;
        }
        let mut inc: Dfp;
        let mut result: Dfp;
        if up {
            inc = self.get_one().clone();
            inc.exp = self.exp - self.mant.len() as i32 + 1;
            inc.sign = self.sign;
            if *self == self.get_zero() {
                inc.exp = MIN_EXP - self.mant.len() as i32;
            }
            result = self.add(&inc);
        } else {
            inc = self.get_one().clone();
            inc.exp = self.exp;
            inc.sign = self.sign;
            if *self == inc {
                inc.exp = self.exp - self.mant.len() as i32;
            } else {
                inc.exp = self.exp - self.mant.len() as i32 + 1;
            }
            if *self == self.get_zero() {
                inc.exp = MIN_EXP - self.mant.len() as i32;
            }
            result = self.subtract(&inc);
        }
        if result.classify() == INFINITE as i32 && self.classify() != INFINITE as i32 {
            self.field.set_i_e_e_e_flags_bits(FLAG_INEXACT);
            result = self.dotrap(FLAG_INEXACT, NEXT_AFTER_TRAP, x, &result);
        }
        if result == self.get_zero() && (*self == self.get_zero()) == false {
            self.field.set_i_e_e_e_flags_bits(FLAG_INEXACT);
            result = self.dotrap(FLAG_INEXACT, NEXT_AFTER_TRAP, x, &result);
        }
        return result;
    }

    /** Convert the instance into a double.
     * @return a double approximating the instance
     * @see #toSplitDouble()
     */
    pub fn to_double(&self) -> f64 {
        if self.is_infinite() {
            if self.less_than(&self.get_zero()) {
                return f64::NEG_INFINITY;
            } else {
                return f64::INFINITY;
            }
        }
        if self.is_na_n() {
            return f64::NAN;
        }
        let mut y: Dfp = self.clone();
        let mut negate: bool = false;
        let cmp0: i32 = Dfp::compare(self, &self.get_zero());
        if cmp0 == 0 {
            return if self.sign < 0 { -0.0 } else { 0.0 };
        } else if cmp0 < 0 {
            y = self.negate();
            negate = true;
        }
        // Find the exponent, first estimate by integer log10, then adjust.
        // Should be faster than doing a natural logarithm.
        let mut exponent: i32 = (y.int_log10() as f64 * 3.32) as i32;
        if exponent < 0 {
            exponent -= 1;
        }
        let mut temp_dfp: Dfp = DfpMath::pow_i32(&self.get_two(), exponent);
        while temp_dfp.less_than(&y) || temp_dfp == y {
            temp_dfp = temp_dfp.multiply_i32(2);
            exponent += 1;
        }
        exponent -= 1;
        // We have the exponent, now work on the mantissa
        y = y.divide(&DfpMath::pow_i32(&self.get_two(), exponent)).unwrap();
        if exponent > -1023 {
            y = y.subtract(&self.get_one());
        }
        if exponent < -1074 {
            return 0.0;
        }
        if exponent > 1023 {
            return if negate {
                f64::NEG_INFINITY
            } else {
                f64::INFINITY
            };
        }
        y = y.multiply(&self.new_instance_i64(4503599627370496)).rint();
        let mut str: String = y.to_string();
        println!("str: before parse {} exponent: {}", str, exponent);
        let strlen = str.len();
        str.truncate(strlen - 1);
        let mut mantissa: i64 = str.parse::<i64>().unwrap();
        if mantissa == 4503599627370496 {
            // Handle special case where we round up to next power of two
            mantissa = 0;
            exponent += 1;
        }
        // Its going to be subnormal, so make adjustments
        if exponent <= -1023 {
            exponent -= 1;
        }
        while exponent < -1023 {
            exponent += 1;
            mantissa >>= /* >>>= */ 1;
        }
        let bits: u64 = mantissa as u64 | (((exponent + 1023) as u64) << 52);
        let mut x: f64 = long_bits_to_double(&bits);
        if negate {
            x = -x;
        }
        return x;
    }

    /** Convert the instance into a split double.
     * @return an array of two doubles which sum represent the instance
     * @see #toDouble()
     */
    pub fn to_split_double(&self) -> [f64; 2] {
        let mut split: [f64; 2] = [0.0; 2];
        let mask: u64 = 0xffffffffc0000000;
        split[0] = long_bits_to_double(&(double_to_raw_long_bits(&self.to_double()) & mask));
        split[1] = self.subtract(&self.new_instance_f64(split[0])).to_double();
        return split;
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn get_real(&self) -> f64 {
        return self.to_double();
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn add_f64(&self, a: f64) -> Dfp {
        return self.add(&self.new_instance_f64(a));
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn subtract_f64(&self, a: f64) -> Dfp {
        return self.subtract(&self.new_instance_f64(a));
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn multiply_f64(&self, a: f64) -> Dfp {
        return self.multiply(&self.new_instance_f64(a));
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn divide_f64(&self, a: f64) -> Dfp {
        return self.clone().divide(&self.new_instance_f64(a)).unwrap();
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn remainder_f64(&self, a: f64) -> Dfp {
        return self.remainder(&self.new_instance_f64(a));
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn round_to_i64(&self) -> i64 {
        return fastmath::F64::round(self.to_double());
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn signum(&self) -> Dfp {
        if self.is_na_n() || self.is_zero() {
            return self.clone();
        } else {
            return self.new_instance_i32(if self.sign > 0 { 1 } else { -1 });
        }
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn copy_sign(&self, s: &Dfp) -> Dfp {
        if (self.sign >= 0 && s.sign >= 0) || (self.sign < 0 && s.sign < 0) {
            // Sign is currently OK
            return self.clone();
        }
        // flip sign
        return self.negate();
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn copy_sign_f64(&self, s: f64) -> Dfp {
        let sb: u64 = double_to_raw_long_bits(&s);
        if (self.sign >= 0 && sb & 0x80000000_00000000 == 0) ||
           (self.sign < 0 && sb & 0x80000000_00000000 != 0) {
            // Sign is currently OK
            return self.clone();
        }
        // flip sign
        return self.negate();
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn scalb(&self, n: i32) -> Dfp {
        return self.multiply(&DfpMath::pow_i32(&self.get_two(), n));
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn hypot(&self, y: &Dfp) -> Dfp {
        return self.multiply(self).add(&y.multiply(&y.clone())).sqrt();
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn cbrt(&self) -> Dfp {
        return self.root_n(3);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn root_n(&self, n: i32) -> Dfp {
        return if self.sign >= 0 {
            DfpMath::pow_dfp(self, &self.get_one().divide_i32(n))
        } else {
            DfpMath::pow_dfp(&self.negate(), &self.get_one().divide_i32(n)).negate()
        };
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn pow_f64(&self, p: f64) -> Dfp {
        return DfpMath::pow_dfp(self, &self.new_instance_f64(p));
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn pow_i32(&self, n: i32) -> Dfp {
        return DfpMath::pow_i32(self, n);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn pow(&self, e: &Dfp) -> Dfp {
        return DfpMath::pow_dfp(self, e);
    }


    /** Check if instance is infinite.
     * @return true if instance is infinite
     */
    pub fn is_infinite(&self) -> bool {
        return self.nans == INFINITE;
    }

    /** Check if instance is not a number.
     * @return true if instance is not a number
     */
    pub fn is_na_n(&self) -> bool {
        return (self.nans == QNAN) || (self.nans == SNAN);
    }

    /** Check if instance is equal to zero.
     * @return true if instance is equal to zero
     */
    pub fn is_zero(&self) -> bool {
        if self.is_na_n() {
            self.field.set_i_e_e_e_flags_bits(FLAG_INVALID);
            self.dotrap(FLAG_INVALID, LESS_THAN_TRAP, self, &self.field.get_zero());
            return false;
        }
        return (self.mant[self.mant.len() - 1] == 0) && !self.is_infinite();
    }

    // Gets a hashCode for the instance.
    // @return a hash code value for this object
    //
    pub fn hash_code(&self) -> i32 {
        let mut res = 17 +
                      (if self.is_zero() {
            0
        } else {
            (self.sign as i32) << 8
        }) + (self.nans as i32) << 16 + self.exp;
        for i in &self.mant {
            res ^= *i;
        }
        return res;
    }
    //

    /** Round to nearest integer using the round-half-even method.
     *  That is round to nearest integer unless both are equidistant.
     *  In which case round to the even one.
     *  @return rounded value
     * @since 3.2
     */
    pub fn rint(&self) -> Dfp {
        return self.trunc(RoundingMode::ROUND_HALF_EVEN);
    }

    /** Round to an integer using the round floor mode.
     * That is, round toward -Infinity
     *  @return rounded value
     * @since 3.2
     */
    pub fn floor(&self) -> Dfp {
        return self.trunc(RoundingMode::ROUND_FLOOR);
    }

    /** Round to an integer using the round ceil mode.
     * That is, round toward +Infinity
     *  @return rounded value
     * @since 3.2
     */
    pub fn ceil(&self) -> Dfp {
        return self.trunc(RoundingMode::ROUND_CEIL);
    }

    /** Returns the IEEE remainder.
     * @param d divisor
     * @return this less n × d, where n is the integer closest to this/d
     * @since 3.2
     */
    pub fn remainder(&self, d: &Dfp) -> Dfp {
        let mut result: Dfp = self.subtract(&self.clone().divide(d).unwrap().rint().multiply(d));
        // IEEE 854-1987 says that if the result is zero, then it carries the sign of this
        if result.mant[self.mant.len() - 1] == 0 {
            result.sign = self.sign;
        }
        return result;
    }

    /** Does the integer conversions with the specified rounding.
     * @param rmode rounding mode to use
     * @return truncated value
     */
    pub fn trunc(&self, rmode: RoundingMode) -> Dfp {
        let mut changed: bool = false;
        if self.is_na_n() {
            return self.clone();
        }
        if self.nans == INFINITE {
            return self.clone();
        }
        if self.mant[self.mant.len() - 1] == 0 {
            // a is zero
            return self.clone();
        }
        // If the exponent is less than zero then we can certainly
        // return zero
        // TODO?????
        if self.exp < 0 {
            self.field.set_i_e_e_e_flags_bits(FLAG_INEXACT);
            let mut result: Dfp = self.get_zero().clone();
            result = self.dotrap(FLAG_INEXACT, TRUNC_TRAP, self, &result);
            return result;
        }
        if self.exp >= self.mant.len() as i32 {
            return self.clone();
        }
        // General case:  create another dfp, result, that contains the
        // a with the fractional part lopped off.
        let mut result: Dfp = self.clone();
        {
            let mut i: usize = 0;
            while i < self.mant.len() - result.exp as usize {
                {
                    changed |= result.mant[i] != 0;
                    result.mant[i] = 0;
                }
                i += 1;
            }
        }

        if changed {
            match rmode {
                RoundingMode::ROUND_FLOOR => {
                    if result.sign == -1 {
                        // then we must increment the mantissa by one
                        result = result.add(&self.new_instance_i32(-1));
                    }
                }
                RoundingMode::ROUND_CEIL => {
                    if result.sign == 1 {
                        // then we must increment the mantissa by one
                        result = result.add(&self.get_one());
                    }
                }
                // ROUND_HALF_EVEN =>
                // {
                // }
                //
                _ => {
                    let half: Dfp = self.new_instance_f64(0.5);
                    // difference between this and result
                    let mut a: Dfp = self.subtract(&result);
                    // force positive (take abs)
                    a.sign = 1;
                    if a.greater_than(&half) {
                        a = self.get_one().clone();
                        a.sign = self.sign;
                        result = result.add(&a);
                    }
                    // If exactly equal to 1/2 and odd then increment
                    if a == half && result.exp > 0 &&
                       (result.mant[self.mant.len() - result.exp as usize] & 1) != 0 {
                        a = self.get_one().clone();
                        a.sign = self.sign;
                        result = result.add(&a);
                    }

                }
            }
            // signal inexact
            self.field.set_i_e_e_e_flags_bits(FLAG_INEXACT);
            result = self.dotrap(FLAG_INEXACT, TRUNC_TRAP, self, &result);
            return result;
        }
        return result;
    }


    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn exp(&self) -> Dfp {
        return DfpMath::exp(self);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn expm1(&self) -> Dfp {
        return DfpMath::exp(self).subtract(&self.get_one());
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn log(&self) -> Dfp {
        return DfpMath::log(self);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn log1p(&self) -> Dfp {
        return DfpMath::log(&self.add(&self.get_one()));
    }

    // TODO: deactivate this implementation (and return type) in 4.0
    /** Get the exponent of the greatest power of 10 that is less than or equal to abs(this).
     *  @return integer base 10 logarithm
     *  @deprecated as of 3.2, replaced by {@link #intLog10()}, in 4.0 the return type
     *  will be changed to Dfp
     */
    pub fn log10(&self) -> i32 {
        return self.int_log10();
    }

    //    TODO: activate this implementation (and return type) in 4.0
    //    /** {@inheritDoc}
    //     * @since 3.2
    //     */
    //    public Dfp log10() {
    //        return DfpMath::log(this).divide(DfpMath::log(newInstance(10)));
    //    }
    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn cos(&self) -> Dfp {
        return DfpMath::cos(self);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn sin(&self) -> Dfp {
        return DfpMath::sin(self);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn tan(&self) -> Dfp {
        return DfpMath::tan(self);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn acos(&self) -> Dfp {
        return DfpMath::acos(self);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn asin(&self) -> Dfp {
        return DfpMath::asin(self);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn atan(&self) -> Dfp {
        return DfpMath::atan(self);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn atan2(&self, x: &Dfp) -> Dfp {
        // compute r = sqrt(x^2+y^2)
        let r: Dfp = x.multiply(x).add(&self.multiply(self)).sqrt();
        if x.sign >= 0 {
            // compute atan2(y, x) = 2 atan(y / (r + x))
            return self.get_two().multiply(&self.divide(&r.add(x)).unwrap().atan());
        } else {
            // compute atan2(y, x) = +/- pi - 2 atan(y / (r - x))
            let tmp: Dfp = self.get_two().multiply(&self.divide(&r.subtract(&x)).unwrap().atan());
            let pm_pi: Dfp = self.new_instance_f64(if tmp.sign <= 0 {
                -fastmath::PI
            } else {
                fastmath::PI
            });
            return pm_pi.subtract(&tmp);
        }
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn cosh(&self) -> Dfp {
        return DfpMath::exp(self).add(&DfpMath::exp(&self.negate())).divide_i32(2);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn sinh(&self) -> Dfp {
        return DfpMath::exp(self).subtract(&DfpMath::exp(&self.negate())).divide_i32(2);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn tanh(&self) -> Dfp {
        let e_plus: Dfp = DfpMath::exp(self);
        let e_minus: Dfp = DfpMath::exp(&self.negate());
        return e_plus.subtract(&e_minus).divide(&e_plus.add(&e_minus)).unwrap();
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn acosh(&self) -> Dfp {
        return self.multiply(self).subtract(&self.get_one()).sqrt().add(self).log();
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn asinh(&self) -> Dfp {
        return self.multiply(self).add(&self.get_one()).sqrt().add(self).log();
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn atanh(&self) -> Dfp {
        return self.get_one()
            .add(self)
            .divide(&self.get_one().subtract(self))
            .unwrap()
            .log()
            .divide_i32(2);
    }



    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn linear_combination_dfpn_dfpn(&self, a: &Vec<Dfp>, b: &Vec<Dfp>) -> Result<Dfp, String> {
        if a.len() != b.len() {
            Err(format!("DimensionMismatchException {} {}", a.len(), b.len()))
        } else {
            let mut r: Dfp = self.get_zero();
            {
                let mut i: usize = 0;
                while i < a.len() {
                    {
                        r = r.add(&a[i].multiply(&b[i]));
                    }
                    i += 1;
                }
            }

            return Ok(r);
        }
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn linear_combination_f64n_dfpn(&self, a: &Vec<f64>, b: &Vec<Dfp>) -> Result<Dfp, String> {
        if a.len() != b.len() {
            Err(format!("DimensionMismatchException {} {}", a.len(), b.len()))
        } else {
            let mut r: Dfp = self.get_zero();
            {
                let mut i: usize = 0;
                while i < a.len() {
                    {
                        r = r.add(&b[i].multiply_f64(a[i]));
                    }
                    i += 1;
                }
            }

            return Ok(r);
        }
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn linear_combination_dfp_dfp_2(&self, a1: &Dfp, b1: &Dfp, a2: &Dfp, b2: &Dfp) -> Dfp {
        return a1.multiply(&b1).add(&a2.multiply(&b2));
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn linear_combination_f64_dfp_2(&self, a1: f64, b1: &Dfp, a2: f64, b2: &Dfp) -> Dfp {
        return b1.multiply_f64(a1).add(&b2.multiply_f64(a2));
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn linear_combination_dfp_dfp_3(&self,
                                        a1: &Dfp,
                                        b1: &Dfp,
                                        a2: &Dfp,
                                        b2: &Dfp,
                                        a3: &Dfp,
                                        b3: &Dfp)
                                        -> Dfp {
        return a1.multiply(b1).add(&a2.multiply(b2)).add(&a3.multiply(b3));
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn linear_combination_f64_dfp_3(&self,
                                        a1: f64,
                                        b1: &Dfp,
                                        a2: f64,
                                        b2: &Dfp,
                                        a3: f64,
                                        b3: &Dfp)
                                        -> Dfp {
        return b1.multiply_f64(a1).add(&b2.multiply_f64(a2)).add(&b3.multiply_f64(a3));
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn linear_combination_dfp_dfp_4(&self,
                                        a1: &Dfp,
                                        b1: &Dfp,
                                        a2: &Dfp,
                                        b2: &Dfp,
                                        a3: &Dfp,
                                        b3: &Dfp,
                                        a4: &Dfp,
                                        b4: &Dfp)
                                        -> Dfp {
        return a1.multiply(b1).add(&a2.multiply(b2)).add(&a3.multiply(b3)).add(&a4.multiply(b4));
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn linear_combination_f64_dfp_4(&self,
                                        a1: f64,
                                        b1: &Dfp,
                                        a2: f64,
                                        b2: &Dfp,
                                        a3: f64,
                                        b3: &Dfp,
                                        a4: f64,
                                        b4: &Dfp)
                                        -> Dfp {
        return b1.multiply_f64(a1)
            .add(&b2.multiply_f64(a2))
            .add(&b3.multiply_f64(a3))
            .add(&b4.multiply_f64(a4));
    }


    /// ***********************************************************************************************************************************
    /** Returns a number that is this number with the sign bit reversed.
     * @return the opposite of this
     */
    pub fn negate(&self) -> Dfp {
        let mut result: Dfp = self.clone();
        result.sign = -result.sign as i8;
        return result;
    }

    /** Subtract x from this.
     * @param x number to subtract
     * @return difference of this and a
     */
    pub fn subtract(&self, x: &Dfp) -> Dfp {
        return self.add(&x.negate());
    }

    /** Multiply this by x.
     * @param x multiplicand
     * @return product of this and x
     */
    pub fn multiply(&self, x: &Dfp) -> Dfp {
        // make sure we don't mix number with different precision
        if self.field.get_radix_digits() != x.field.get_radix_digits() {
            self.field.set_i_e_e_e_flags_bits(FLAG_INVALID);
            let mut result: Dfp = self.get_zero();
            result.nans = QNAN;
            return self.dotrap(FLAG_INVALID, MULTIPLY_TRAP, x, &result);
        }
        let mut result: Dfp = self.get_zero();
        let mant_size = self.mant.len();
        // handle special cases
        if self.nans != FINITE || x.nans != FINITE {
            if self.is_na_n() {
                return self.clone();
            }
            if x.is_na_n() {
                return x.clone();
            }
            if self.nans == INFINITE && x.nans == FINITE && x.mant[mant_size - 1] != 0 {
                result = self.clone();
                result.sign = (self.sign * x.sign) as i8;
                return result;
            }
            if x.nans == INFINITE && self.nans == FINITE && self.mant[mant_size - 1] != 0 {
                result = x.clone();
                result.sign = (self.sign * x.sign) as i8;
                return result;
            }
            if x.nans == INFINITE && self.nans == INFINITE {
                result = self.clone();
                result.sign = (self.sign * x.sign) as i8;
                return result;
            }
            if (x.nans == INFINITE && self.nans == FINITE && self.mant[mant_size - 1] == 0) ||
               (self.nans == INFINITE && x.nans == FINITE && x.mant[mant_size - 1] == 0) {
                self.field.set_i_e_e_e_flags_bits(FLAG_INVALID);
                result = self.get_zero();
                result.nans = QNAN;
                result = self.dotrap(FLAG_INVALID, MULTIPLY_TRAP, x, &result);
                return result;
            }
        }
        // Big enough to hold even the largest result
        let mut product = Vec::with_capacity(mant_size * 2);
        for _ in 0..(mant_size * 2) {
            product.push(0)
        }
        {
            let mut i: usize = 0;
            while i < mant_size {
                {
                    // acts as a carry
                    let mut rh: i32 = 0;
                    {
                        let mut j: usize = 0;
                        while j < mant_size {
                            {
                                // multiply the 2 digits
                                let mut r: i32 = self.mant[i] * x.mant[j];
                                // add to the product digit with carry in
                                r += product[i + j] + rh;
                                rh = r / RADIX;
                                product[i + j] = r - rh * RADIX;
                            }
                            j += 1;
                        }
                    }

                    product[i + mant_size] = rh;
                }
                i += 1;
            }
        }

        // Find the most sig digit
        // default, in case result is zero
        let mut md: usize = mant_size * 2 - 1;
        {
            let mut i: i32 = mant_size as i32 * 2 - 1;
            while i >= 0 {
                {
                    if product[i as usize] != 0 {
                        md = i as usize;
                        break;
                    }
                }
                i -= 1;
            }
        }

        // Copy the digits into the result
        {
            let mut i: usize = 0;
            while i < mant_size {
                {
                    result.mant[mant_size - i - 1] = product[md - i];
                }
                i += 1;
            }
        }

        // Fixup the exponent.
        result.exp = self.exp + x.exp + md as i32 - 2 * mant_size as i32 + 1;
        result.sign = (if self.sign == x.sign { 1 } else { -1 }) as i8;
        if result.mant[mant_size - 1] == 0 {
            // if result is zero, set exp to zero
            result.exp = 0;
        }
        let excp: i32;
        if md > (mant_size - 1) {
            excp = result.round(product[md - mant_size]);
        } else {
            // has no effect except to check status
            excp = result.round(0);
        }
        if excp != 0 {
            result = self.dotrap(excp, MULTIPLY_TRAP, x, &result);
        }
        return result;
    }

    /** Multiply this by a single digit x.
     * @param x multiplicand
     * @return product of this and x
     */
    pub fn multiply_i32(&self, x: i32) -> Dfp {
        if x >= 0 && x < RADIX {
            return self.multiply_fast(x);
        } else {
            return self.multiply(&self.new_instance_i32(x));
        }
    }



    /** Divide this by divisor.
     * @param divisor divisor
     * @return quotient of this by divisor
     */
    pub fn divide(&self, divisor: &Dfp) -> Result<Dfp, String> {
        // current quotient digit we're working with
        let mut qd: i32;
        // number of significant quotient digits we have
        let mut nsqd: usize;
        // trial quotient digit
        let mut trial: i32 = 0;
        // minimum adjustment
        let mut minadj: i32;
        // Flag to indicate a good trail digit
        let mut trialgood: bool;
        // most sig digit in result
        let mut md: usize;
        // exceptions
        let excp: i32;
        // make sure we don't mix number with different precision
        if self.field.get_radix_digits() != divisor.field.get_radix_digits() {
            self.field.set_i_e_e_e_flags_bits(FLAG_INVALID);
            let mut result: Dfp = self.field.get_zero();
            result.nans = QNAN;
            return Ok(self.dotrap(FLAG_INVALID, DIVIDE_TRAP, divisor, &result));
        }
        let mut result: Dfp = self.field.get_zero();
        // handle special cases
        if self.nans != FINITE || divisor.nans != FINITE {
            if self.is_na_n() {
                return Ok(self.clone());
            }
            if divisor.is_na_n() {
                return Ok(divisor.clone());
            }
            if self.nans == INFINITE && divisor.nans == FINITE {
                result = self.clone();
                result.sign = (self.sign * divisor.sign) as i8;
                return Ok(result);
            }
            if divisor.nans == INFINITE && self.nans == FINITE {
                result = self.field.get_zero();
                result.sign = (self.sign * divisor.sign) as i8;
                return Ok(result);
            }
            if divisor.nans == INFINITE && self.nans == INFINITE {
                self.field.set_i_e_e_e_flags_bits(FLAG_INVALID);
                result = self.field.get_zero();
                result.nans = QNAN;
                result = self.dotrap(FLAG_INVALID, DIVIDE_TRAP, divisor, &result);
                return Ok(result);
            }
        }
        // Test for divide by zero
        if divisor.mant[self.mant.len() - 1] == 0 {
            self.field.set_i_e_e_e_flags_bits(FLAG_DIV_ZERO);
            result = self.field.get_zero();
            result.sign = (self.sign * divisor.sign) as i8;
            result.nans = INFINITE;
            result = self.dotrap(FLAG_DIV_ZERO, DIVIDE_TRAP, divisor, &result);
            return Ok(result);
        }
        // one extra digit needed
        let mut dividend = Vec::with_capacity(self.mant.len() + 1);
        for _ in 0..self.mant.len() + 1 {
            dividend.push(0);
        }
        // two extra digits needed 1 for overflow, 1 for rounding
        let mut quotient = Vec::with_capacity(self.mant.len() + 2);
        for _ in 0..self.mant.len() + 2 {
            quotient.push(0);
        }
        // one extra digit needed
        let mut remainder = Vec::with_capacity(self.mant.len() + 1);
        for _ in 0..self.mant.len() + 1 {
            remainder.push(0);
        }
        // Initialize our most significant digits to zero
        dividend[self.mant.len()] = 0;
        quotient[self.mant.len()] = 0;
        quotient[self.mant.len() + 1] = 0;
        remainder[self.mant.len()] = 0;
        {
            let mut i: usize = 0;
            while i < self.mant.len() {
                {
                    dividend[i] = self.mant[i];
                    quotient[i] = 0;
                    remainder[i] = 0;
                }
                i += 1;
            }
        }

        // outer loop.  Once per quotient digit
        nsqd = 0;
        {
            qd = (self.mant.len() + 1) as i32;
            while qd >= 0 {
                {
                    // Determine outer limits of our quotient digit
                    // r =  most sig 2 digits of dividend
                    let div_msb: i32 = dividend[self.mant.len()] * RADIX +
                                       dividend[self.mant.len() - 1];
                    let mut min: i32 = div_msb / (divisor.mant[self.mant.len() - 1] + 1);
                    let mut max: i32 = (div_msb + 1) / divisor.mant[self.mant.len() - 1];
                    trialgood = false;
                    while !trialgood {
                        // try the mean
                        trial = (min + max) / 2;
                        // Multiply by divisor and store as remainder
                        let mut rh: i32 = 0;
                        {
                            let mut i: usize = 0;
                            while i < self.mant.len() + 1 {
                                {
                                    let dm: i32 = if i < self.mant.len() {
                                        divisor.mant[i]
                                    } else {
                                        0
                                    };
                                    let r: i32 = (dm * trial) + rh;
                                    rh = r / RADIX;
                                    remainder[i] = r - rh * RADIX;
                                }
                                i += 1;
                            }
                        }

                        // subtract the remainder from the dividend
                        // carry in to aid the subtraction
                        rh = 1;
                        {
                            let mut i: usize = 0;
                            while i < self.mant.len() + 1 {
                                {
                                    let r: i32 = ((RADIX - 1) - remainder[i]) + dividend[i] + rh;
                                    rh = r / RADIX;
                                    remainder[i] = r - rh * RADIX;
                                }
                                i += 1;
                            }
                        }

                        // Lets analyze what we have here
                        if rh == 0 {
                            // trial is too big -- negative remainder
                            max = trial - 1;
                            continue;
                        }
                        // find out how far off the remainder is telling us we are
                        minadj = (remainder[self.mant.len()] * RADIX) +
                                 remainder[self.mant.len() - 1];
                        minadj /= divisor.mant[self.mant.len() - 1] + 1;
                        if minadj >= 2 {
                            // update the minimum
                            min = trial + minadj;
                            continue;
                        }
                        // May have a good one here, check more thoroughly.  Basically
                        // its a good one if it is less than the divisor
                        // assume false
                        trialgood = false;
                        {
                            let mut i: i32 = (self.mant.len() - 1) as i32;
                            while i >= 0 {
                                {
                                    if divisor.mant[i as usize] > remainder[i as usize] {
                                        trialgood = true;
                                    }
                                    if divisor.mant[i as usize] < remainder[i as usize] {
                                        break;
                                    }
                                }
                                i -= 1;
                            }
                        }

                        if remainder[self.mant.len()] != 0 {
                            trialgood = false;
                        }
                        if trialgood == false {
                            min = trial + 1;
                        }
                    }
                    // Great we have a digit!
                    quotient[qd as usize] = trial;
                    if trial != 0 || nsqd != 0 {
                        nsqd += 1;
                    }
                    if self.field.get_rounding_mode() == RoundingMode::ROUND_DOWN &&
                       nsqd == self.mant.len() {
                        // We have enough for this mode
                        break;
                    }
                    if nsqd > self.mant.len() {
                        // We have enough digits
                        break;
                    }
                    // move the remainder into the dividend while left shifting
                    dividend[0] = 0;
                    {
                        let mut i: usize = 0;
                        while i < self.mant.len() {
                            {
                                dividend[i + 1] = remainder[i];
                            }
                            i += 1;
                        }
                    }

                }
                qd -= 1;
            }
        }

        // Find the most sig digit
        // default
        md = self.mant.len();
        {
            let mut i: i32 = (self.mant.len() + 1) as i32;
            while i >= 0 {
                {
                    if quotient[i as usize] != 0 {
                        md = i as usize;
                        break;
                    }
                }
                i -= 1;
            }
        }

        // Copy the digits into the result
        {
            let mut i: usize = 0;
            while i < self.mant.len() {
                {
                    result.mant[self.mant.len() - i - 1] = quotient[md - i];
                }
                i += 1;
            }
        }

        // Fixup the exponent.
        result.exp = self.exp - divisor.exp + md as i32 - self.mant.len() as i32;
        result.sign = (if self.sign == divisor.sign { 1 } else { -1 }) as i8;
        if result.mant[self.mant.len() - 1] == 0 {
            // if result is zero, set exp to zero
            result.exp = 0;
        }
        if md > (self.mant.len() - 1) {
            excp = result.round(quotient[md - self.mant.len()]);
        } else {
            excp = result.round(0);
        }
        if excp != 0 {
            result = self.dotrap(excp, DIVIDE_TRAP, divisor, &result);
        }
        return Ok(result);
    }

    /** {@inheritDoc} */
    pub fn reciprocal(&self) -> Result<Dfp, String> {
        return self.field.get_one().divide(self);
    }


    /** Add x to this.
     * @param x number to add
     * @return sum of this and x
     */
    pub fn add(&self, x: &Dfp) -> Dfp {
        // make sure we don't mix number with different precision
        if self.field.get_radix_digits() != x.field.get_radix_digits() {
            self.field.set_i_e_e_e_flags_bits(FLAG_INVALID);
            let mut result: Dfp = self.field.get_zero();
            result.nans = QNAN;
            return self.dotrap(FLAG_INVALID, ADD_TRAP, x, &result);
        }
        // handle special cases
        if self.nans != FINITE || x.nans != FINITE {
            if self.is_na_n() {
                return self.clone();
            }
            if x.is_na_n() {
                return x.clone();
            }
            if self.nans == INFINITE && x.nans == FINITE {
                return self.clone();
            }
            if x.nans == INFINITE && self.nans == FINITE {
                return x.clone();
            }
            if x.nans == INFINITE && self.nans == INFINITE && self.sign == x.sign {
                return x.clone();
            }
            if x.nans == INFINITE && self.nans == INFINITE && self.sign != x.sign {
                self.field.set_i_e_e_e_flags_bits(FLAG_INVALID);
                let mut result: Dfp = self.field.get_zero();
                result.nans = QNAN;
                result = self.dotrap(FLAG_INVALID, ADD_TRAP, x, &result);
                return result;
            }
        }
        // copy this and the arg
        let mut a: Dfp = self.clone();
        let mut b: Dfp = x.clone();
        // initialize the result object
        let mut result: Dfp = self.field.get_zero();
        // Make all numbers positive, but remember their sign
        let asign: i8 = a.sign;
        let bsign: i8 = b.sign;
        a.sign = 1;
        b.sign = 1;
        // The result will be signed like the arg with greatest magnitude
        let mut rsign: i8 = bsign;
        if Dfp::compare(&a, &b) > 0 {
            rsign = asign;
        }
        // Handle special case when a or b is zero, by setting the exponent
        // of the zero number equal to the other one.  This avoids an alignment
        // which would cause catastropic loss of precision
        if b.mant[self.mant.len() - 1] == 0 {
            b.exp = a.exp;
        }
        if a.mant[self.mant.len() - 1] == 0 {
            a.exp = b.exp;
        }
        // align number with the smaller exponent
        let mut aextradigit: i32 = 0;
        let mut bextradigit: i32 = 0;
        if a.exp < b.exp {
            aextradigit = a.align(b.exp);
        } else {
            bextradigit = b.align(a.exp);
        }
        // complement the smaller of the two if the signs are different
        if asign != bsign {
            if asign == rsign {
                bextradigit = b.complement(bextradigit);
            } else {
                aextradigit = a.complement(aextradigit);
            }
        }
        // add the mantissas
        let mut rh: i32 = 0;
        // acts as a carry
        {
            let mut i: usize = 0;
            while i < self.mant.len() {
                {
                    let r: i32 = a.mant[i] + b.mant[i] + rh;
                    rh = r / RADIX;
                    result.mant[i] = r - rh * RADIX;
                }
                i += 1;
            }
        }

        result.exp = a.exp;
        result.sign = rsign;
        if rh != 0 && (asign == bsign) {
            let lostdigit: i32 = result.mant[0];
            result.shift_right();
            result.mant[self.mant.len() - 1] = rh;
            let excp: i32 = result.round(lostdigit);
            if excp != 0 {
                result = self.dotrap(excp, ADD_TRAP, x, &result);
            }
        }
        // normalize the result
        {
            let mut i: usize = 0;
            while i < self.mant.len() {
                {
                    if result.mant[self.mant.len() - 1] != 0 {
                        break;
                    }
                    result.shift_left();
                    if i == 0 {
                        result.mant[0] = aextradigit + bextradigit;
                        aextradigit = 0;
                        bextradigit = 0;
                    }
                }
                i += 1;
            }
        }

        // result is zero if after normalization the most sig. digit is zero
        if result.mant[self.mant.len() - 1] == 0 {
            result.exp = 0;
            if asign != bsign {
                // Unless adding 2 negative zeros, sign is positive
                // Per IEEE 854-1987 Section 6.3
                result.sign = 1;
            }
        }
        // Call round to test for over/under flows
        let excp: i32 = result.round(aextradigit + bextradigit);
        if excp != 0 {
            result = self.dotrap(excp, ADD_TRAP, x, &result);
        }
        return result;
    }
}
