
use dfp::dfp_field::*;
use dfp::dfp::*;

/** Name for traps triggered by pow. */
const POW_TRAP: &'static str = "pow";
pub struct DfpMath {
}

impl DfpMath {
    /** Breaks a string representation up into two dfp's.
     * <p>The two dfp are such that the sum of them is equivalent
     * to the input string, but has higher precision than using a
     * single dfp. This is useful for improving accuracy of
     * exponentiation and critical multiplies.
     * @param field field to which the Dfp must belong
     * @param a string representation to split
     * @return an array of two {@link Dfp} which sum is a
     */
    pub fn split_str(field: &DfpField, a: &str) -> [Dfp; 2] {
        let mut result: [Dfp; 2] = [field.get_zero(), field.get_zero()];
        let mut tmp = String::new();
        tmp.push_str(a);
        let mut chars = tmp.chars();
        let mut leading: bool = true;
        let mut sp: usize = 0;
        let mut sig: i32 = 0;
        let mut buf = String::new();
        {
            let mut i: usize = 0;
            while i < tmp.len() {
                {
                    let c = chars.next().unwrap();
                    buf.push(c);
                    if c >= '1' && c <= '9' {
                        leading = false;
                    }
                    if c == '.' {
                        sig += (400 - sig) % 4;
                        leading = false;
                    }
                    if sig == (field.get_radix_digits() as i32 / 2) * 4 {
                        sp = i;
                        break;
                    }
                    if c >= '0' && c <= '9' && !leading {
                        sig += 1;
                    }
                }
                i += 1;
            }
        }

        result[0] = field.new_dfp_str(buf.as_str()).unwrap();
        let mut buf2 = String::new();
        chars = tmp.chars();
        {
            let mut i: usize = 0;
            while i < tmp.len() {
                {
                    let mut c = chars.next().unwrap();

                    if c >= '0' && c <= '9' && i < sp {
                        c = '0';
                    }
                    buf2.push(c);
                }
                i += 1;
            }
        }

        result[1] = field.new_dfp_str(buf2.as_str()).unwrap();
        return result;
    }

    /** Splits a {@link Dfp} into 2 {@link Dfp}'s such that their sum is equal to the input {@link Dfp}.
     * @param a number to split
     * @return two elements array containing the split number
     */
    pub fn split(a: &Dfp) -> [Dfp; 2] {
        let shift: Dfp = a.multiply(&a.power10_k(a.get_radix_digits() as i32 / 2));
        let tmp = a.add(&shift).subtract(&shift);
        let result: [Dfp; 2] = [tmp.clone(), a.subtract(&tmp)];
        return result;
    }

    /** Multiply two numbers that are split in to two pieces that are
     *  meant to be added together.
     *  Use binomial multiplication so ab = a0 b0 + a0 b1 + a1 b0 + a1 b1
     *  Store the first term in result0, the rest in result1
     *  @param a first factor of the multiplication, in split form
     *  @param b second factor of the multiplication, in split form
     *  @return a × b, in split form
     */
    pub fn split_mult(a: &[Dfp; 2], b: &[Dfp; 2]) -> [Dfp; 2] {
        let mut result: [Dfp; 2] = [a[0].multiply(&b[0]), a[0].get_zero()];
        if result[0].classify() == INFINITE as i32 || result[0] == result[1] {
            return result;
        }
        result[1] = a[0].multiply(&b[1]).add(&a[1].multiply(&b[0])).add(&a[1].multiply(&b[1]));
        return result;
    }

    /** Divide two numbers that are split in to two pieces that are meant to be added together.
     * Inverse of split multiply above:
     *  (a+b) / (c+d) = (a/c) + ( (bc-ad)/(c**2+cd) )
     *  @param a dividend, in split form
     *  @param b divisor, in split form
     *  @return a / b, in split form
     */
    pub fn split_div(a: &[Dfp; 2], b: &[Dfp; 2]) -> [Dfp; 2] {
        let mut result: [Dfp; 2] = [a[0].divide(&b[0]).unwrap(),
                                    a[1].multiply(&b[0]).subtract(&a[0].multiply(&b[1]))];
        result[1] = result[1].divide(&b[0].multiply(&b[0]).add(&b[0].multiply(&b[1]))).unwrap();
        return result;
    }

    /** Raise a split base to the a power.
     * @param base number to raise
     * @param a power
     * @return base<sup>a</sup>
     */
    pub fn split_pow(base: &[Dfp; 2], a_p: i32) -> Dfp {
        let mut a = a_p;
        let mut invert: bool = false;
        let mut r: [Dfp; 2] = [base[0].clone(), base[1].clone()];
        let mut result: [Dfp; 2] = [base[0].get_one(), base[0].get_zero()];
        if a == 0 {
            // Special case a = 0
            return result[0].add(&result[1]);
        }
        if a < 0 {
            // If a is less than zero
            invert = true;
            a = -a;
        }
        // Exponentiate by successive squaring
        loop {
            {
                r[0] = base[0].clone();
                r[1] = base[1].clone();
                let mut trial: i32 = 1;
                let mut prevtrial: i32;
                loop {
                    prevtrial = trial;
                    trial *= 2;
                    if trial > a {
                        break;
                    }
                    r = DfpMath::split_mult(&r, &r);
                }
                trial = prevtrial;
                a -= trial;
                result = DfpMath::split_mult(&result, &r);
            }
            if !(a >= 1) {
                break;
            }
        }
        result[0] = result[0].add(&result[1]);
        if invert {
            result[0] = base[0].get_one().divide(&result[0]).unwrap();
        }
        return result[0].clone();
    }


    /** Raises base to the power a by successive squaring.
     * @param base number to raise
     * @param a power
     * @return base<sup>a</sup>
     */
    pub fn pow_i32(base: &Dfp, mut a: i32) -> Dfp {
        let mut invert: bool = false;
        let mut result: Dfp = base.get_field().get_one();
        if a == 0 {
            // Special case
            return result;
        }
        if a < 0 {
            invert = true;
            a = -a;
        }
        // Exponentiate by successive squaring
        loop {
            {
                let mut r: Dfp = base.clone();
                let mut prevr: Dfp;
                let mut trial: i32 = 1;
                let mut prevtrial: i32;
                loop {
                    {
                        prevr = r.clone();
                        prevtrial = trial;
                        r = r.multiply(&r);
                        trial *= 2;
                    }
                    if !(a > trial) {
                        break;
                    }
                }
                r = prevr;
                trial = prevtrial;
                a -= trial;
                result = result.multiply(&r);
            }
            if !(a >= 1) {
                break;
            }
        }
        if invert {
            result = base.get_one().divide(&result).unwrap();
        }
        return base.new_instance_dfp(&result);
    }

    /** Computes e to the given power.
     * a is broken into two parts, such that a = n+m  where n is an integer.
     * We use pow() to compute e<sup>n</sup> and a Taylor series to compute
     * e<sup>m</sup>.  We return e*<sup>n</sup> × e<sup>m</sup>
     * @param a power at which e should be raised
     * @return e<sup>a</sup>
     */
    pub fn exp(a: &Dfp) -> Dfp {
        let inta: Dfp = a.rint();
        let fraca: Dfp = a.subtract(&inta);
        let ia: i32 = inta.int_value();
        if ia > 2147483646 {
            // return +Infinity
            return a.new_instance_sig_code(1 as i8, INFINITE);
        }
        if ia < -2147483646 {
            // return 0;
            return a.new_instance();
        }
        let einta: Dfp = DfpMath::split_pow(&a.get_field().get_e_split(), ia);
        let efraca: Dfp = DfpMath::exp_internal(&fraca);
        return einta.multiply(&efraca);
    }

    /** Computes e to the given power.
     * Where -1 < a < 1.  Use the classic Taylor series.  1 + x**2/2! + x**3/3! + x**4/4!  ...
     * @param a power at which e should be raised
     * @return e<sup>a</sup>
     */
    pub fn exp_internal(a: &Dfp) -> Dfp {
        let mut y: Dfp = a.get_one();
        let mut x: Dfp = a.get_one();
        let mut fact: Dfp = a.get_one();
        let mut py: Dfp = y.clone();
        {
            let mut i: i32 = 1;
            while i < 90 {
                {
                    x = x.multiply(a);
                    fact = fact.divide_i32(i);
                    y = y.add(&x.multiply(&fact));
                    if y == py {
                        break;
                    }
                    py = y.clone();
                }
                i += 1;
            }
        }

        return y;
    }

    /** Returns the natural logarithm of a.
     * a is first split into three parts such that  a = (10000^h)(2^j)k.
     * ln(a) is computed by ln(a) = ln(5)*h + ln(2)*(h+j) + ln(k)
     * k is in the range 2/3 < k <4/3 and is passed on to a series expansion.
     * @param a number from which logarithm is requested
     * @return log(a)
     */
    pub fn log(a: &Dfp) -> Dfp {
        let lr: i32;
        let mut x: Dfp;
        let mut ix: i32;
        let mut p2: i32 = 0;
        // Check the arguments somewhat here
        if *a == a.get_zero() || a.less_than(&a.get_zero()) || a.is_na_n() {
            // negative, zero or NaN
            a.get_field().set_i_e_e_e_flags_bits(FLAG_INVALID);
            return a.dotrap(FLAG_INVALID,
                            "ln",
                            a,
                            &a.new_instance_sig_code(1 as i8, QNAN));
        }
        if a.classify() == INFINITE as i32 {
            return a.clone();
        }
        println!("a: {}", a.to_string());
        x = a.clone();
        println!("x: {}", x.to_string());
        lr = x.log10_k();
        println!("lr: {}", lr.to_string());
        println!("DfpMath::pow_i32(&a.new_instance_i32(10000), lr): {}",
                 DfpMath::pow_i32(&a.new_instance_i32(10000), lr).to_string());

        x = x.divide(&DfpMath::pow_i32(&a.new_instance_i32(10000), lr)).unwrap();
        // This puts x in the range 0-10000
        ix = x.floor().int_value();
        while ix > 2 {
            ix >>= 1;
            p2 += 1;
        }
        println!("p2: {}", p2);
        let spx: [Dfp; 2] = DfpMath::split(&x);
        println!("spx1: {} {}", spx[0].to_string(), spx[1].to_string());

        // use spy[0] temporarily as a divisor
        let mut tmpspy0 = DfpMath::pow_i32(&a.get_two(), p2);
        println!("spy0: {}", tmpspy0.to_string());

        let mut spx0 = spx[0].divide(&tmpspy0).unwrap();
        let mut spx1 = spx[1].divide(&tmpspy0).unwrap();
        println!("spx2: {} {}", spx0.to_string(), spx1.to_string());
        // Use spy[0] for comparison
        tmpspy0 = a.new_instance_str("1.33333").unwrap();
        while spx0.add(&spx1).greater_than(&tmpspy0) {
            spx0 = spx0.divide_i32(2);
            spx1 = spx1.divide_i32(2);
            p2 += 1;
        }
        println!("spx3: {} {}", spx0.to_string(), spx1.to_string());
        let spx: [Dfp; 2] = [spx0, spx1];
        // X is now in the range of 2/3 < x < 4/3
        let mut spz = DfpMath::log_internal(spx);
        println!("spz1: {} {}", spz[0].to_string(), spz[1].to_string());
        let str = (p2 + 4 * lr).to_string();
        spx0 = a.new_instance_str(str.as_str()).unwrap();
        spx1 = a.get_zero();
        let ln2_split = a.get_field().get_ln2_split();
        println!("ln2_split: {} {}",
                 ln2_split[0].to_string(),
                 ln2_split[1].to_string());
        let mut spy = DfpMath::split_mult(&ln2_split, &[spx0, spx1]);
        println!("spy1: {} {}", spy[0].to_string(), spy[1].to_string());
        spz[0] = spz[0].add(&spy[0]);
        spz[1] = spz[1].add(&spy[1]);
        println!("spz2: {} {}", spz[0].to_string(), spz[1].to_string());
        spx0 = a.new_instance_str((4 * lr).to_string().as_str()).unwrap();
        spx1 = a.get_zero();
        println!("spx4: {} {}", spx0.to_string(), spx1.to_string());
        spy = DfpMath::split_mult(&a.get_field().get_ln5_split(), &[spx0, spx1]);
        println!("spy2: {} {}", spy[0].to_string(), spy[1].to_string());
        spz[0] = spz[0].add(&spy[0]);
        spz[1] = spz[1].add(&spy[1]);
        println!("spz3: {} {}", spz[0].to_string(), spz[1].to_string());
        return a.new_instance_dfp(&spz[0].add(&spz[1]));
    }

    /** Computes the natural log of a number between 0 and 2.
     *  Let f(x) = ln(x),
     *
     *  We know that f'(x) = 1/x, thus from Taylor's theorum we have:
     *
     *           -----          n+1         n
     *  f(x) =   \           (-1)    (x - 1)
     *           /          ----------------    for 1 <= n <= infinity
     *           -----             n
     *
     *  or
     *                       2        3       4
     *                   (x-1)   (x-1)    (x-1)
     *  ln(x) =  (x-1) - ----- + ------ - ------ + ...
     *                     2       3        4
     *
     *  alternatively,
     *
     *                  2    3   4
     *                 x    x   x
     *  ln(x+1) =  x - -  + - - - + ...
     *                 2    3   4
     *
     *  This series can be used to compute ln(x), but it converges too slowly.
     *
     *  If we substitute -x for x above, we get
     *
     *                   2    3    4
     *                  x    x    x
     *  ln(1-x) =  -x - -  - -  - - + ...
     *                  2    3    4
     *
     *  Note that all terms are now negative.  Because the even powered ones
     *  absorbed the sign.  Now, subtract the series above from the previous
     *  one to get ln(x+1) - ln(1-x).  Note the even terms cancel out leaving
     *  only the odd ones
     *
     *                             3     5      7
     *                           2x    2x     2x
     *  ln(x+1) - ln(x-1) = 2x + --- + --- + ---- + ...
     *                            3     5      7
     *
     *  By the property of logarithms that ln(a) - ln(b) = ln (a/b) we have:
     *
     *                                3        5        7
     *      x+1           /          x        x        x          \
     *  ln ----- =   2 *  |  x  +   ----  +  ----  +  ---- + ...  |
     *      x-1           \          3        5        7          /
     *
     *  But now we want to find ln(a), so we need to find the value of x
     *  such that a = (x+1)/(x-1).   This is easily solved to find that
     *  x = (a-1)/(a+1).
     * @param a number from which logarithm is requested, in split form
     * @return log(a)
     */
    pub fn log_internal(a: [Dfp; 2]) -> [Dfp; 2] {
        // Now we want to compute x = (a-1)/(a+1) but this is prone to
        // loss of precision.  So instead, compute x = (a/4 - 1/4) / (a/4 + 1/4)
        //
        let mut t: Dfp = a[0].divide_i32(4).add(&a[1].divide_i32(4));
        let x: Dfp = t.add(&a[0].new_instance_str("-0.25").unwrap())
            .divide(&t.add(&a[0].new_instance_str("0.25").unwrap()))
            .unwrap();
        let mut y: Dfp = x.clone();
        let mut num: Dfp = x.clone();
        let mut py: Dfp = y.clone();
        let mut den: i32 = 1;
        {
            let mut i: i32 = 0;
            while i < 10000 {
                {
                    num = num.multiply(&x);
                    num = num.multiply(&x);
                    den += 2;
                    t = num.divide_i32(den);
                    y = y.add(&t);
                    if y == py {
                        break;
                    }
                    py = y.clone();
                }
                i += 1;
            }
        }

        y = y.multiply(&a[0].get_two());
        return DfpMath::split(&y);
    }



    /** Computes x to the y power.<p>
     *
     *  Uses the following method:<p>
     *
     *  <ol>
     *  <li> Set u = rint(y), v = y-u
     *  <li> Compute a = v * ln(x)
     *  <li> Compute b = rint( a/ln(2) )
     *  <li> Compute c = a - b*ln(2)
     *  <li> x<sup>y</sup> = x<sup>u</sup>  *   2<sup>b</sup> * e<sup>c</sup>
     *  </ol>
     *  if |y| > 1e8, then we compute by exp(y*ln(x))   <p>
     *
     *  <b>Special Cases</b><p>
     *  <ul>
     *  <li>  if y is 0.0 or -0.0 then result is 1.0
     *  <li>  if y is 1.0 then result is x
     *  <li>  if y is NaN then result is NaN
     *  <li>  if x is NaN and y is not zero then result is NaN
     *  <li>  if |x| > 1.0 and y is +Infinity then result is +Infinity
     *  <li>  if |x| < 1.0 and y is -Infinity then result is +Infinity
     *  <li>  if |x| > 1.0 and y is -Infinity then result is +0
     *  <li>  if |x| < 1.0 and y is +Infinity then result is +0
     *  <li>  if |x| = 1.0 and y is +/-Infinity then result is NaN
     *  <li>  if x = +0 and y > 0 then result is +0
     *  <li>  if x = +Inf and y < 0 then result is +0
     *  <li>  if x = +0 and y < 0 then result is +Inf
     *  <li>  if x = +Inf and y > 0 then result is +Inf
     *  <li>  if x = -0 and y > 0, finite, not odd integer then result is +0
     *  <li>  if x = -0 and y < 0, finite, and odd integer then result is -Inf
     *  <li>  if x = -Inf and y > 0, finite, and odd integer then result is -Inf
     *  <li>  if x = -0 and y < 0, not finite odd integer then result is +Inf
     *  <li>  if x = -Inf and y > 0, not finite odd integer then result is +Inf
     *  <li>  if x < 0 and y > 0, finite, and odd integer then result is -(|x|<sup>y</sup>)
     *  <li>  if x < 0 and y > 0, finite, and not integer then result is NaN
     *  </ul>
     *  @param x base to be raised
     *  @param y power to which base should be raised
     *  @return x<sup>y</sup>
     */
    pub fn pow_dfp(x: &Dfp, y: &Dfp) -> Dfp {
        let mut x = x.clone();
        // make sure we don't mix number with different precision
        if x.get_field().get_radix_digits() != y.get_field().get_radix_digits() {
            x.get_field().set_i_e_e_e_flags_bits(FLAG_INVALID);
            let mut result: Dfp = x.get_zero().clone();
            result.nans = QNAN;
            return x.dotrap(FLAG_INVALID, POW_TRAP, &x, &result);
        }
        let zero: Dfp = x.get_zero();
        let one: Dfp = x.get_one();
        let two: Dfp = x.get_two();
        let mut invert: bool = false;
        let ui: i32;
        // Check for special cases
        if *y == zero {
            return one.clone();
        }
        if *y == one {
            if x.is_na_n() {
                // Test for NaNs
                x.get_field().set_i_e_e_e_flags_bits(FLAG_INVALID);
                return x.dotrap(FLAG_INVALID, POW_TRAP, &x, &x);
            }
            return x.clone();
        }
        if x.is_na_n() || y.is_na_n() {
            // Test for NaNs
            x.get_field().set_i_e_e_e_flags_bits(FLAG_INVALID);
            return x.dotrap(FLAG_INVALID,
                            POW_TRAP,
                            &x,
                            &x.new_instance_sig_code(1 as i8, QNAN));
        }
        // X == 0
        if x == zero {
            if Dfp::copysign(&one, &x).greater_than(&zero) {
                // X == +0
                if y.greater_than(&zero) {
                    return zero.clone();
                } else {
                    return x.new_instance_sig_code(1 as i8, INFINITE);
                }
            } else {
                // X == -0
                if y.classify() == FINITE as i32 && y.rint() == *y && y.remainder(&two) != zero {
                    // If y is odd integer
                    if y.greater_than(&zero) {
                        return x.new_instance_dfp(&zero.negate());
                    } else {
                        return x.new_instance_dfp(&x.new_instance_sig_code(-1 as i8, INFINITE));
                    }
                } else {
                    // Y is not odd integer
                    if y.greater_than(&zero) {
                        return x.new_instance_dfp(&zero);
                    } else {
                        return x.new_instance_dfp(&x.new_instance_sig_code(1 as i8, INFINITE));
                    }
                }
            }
        }
        if x.less_than(&zero) {
            // Make x positive, but keep track of it
            x = x.negate();
            invert = true;
        }
        if x.greater_than(&one) && y.classify() == INFINITE as i32 {
            if y.greater_than(&zero) {
                return y.clone();
            } else {
                return x.new_instance_dfp(&zero);
            }
        }
        if x.less_than(&one) && y.classify() == INFINITE as i32 {
            if y.greater_than(&zero) {
                return x.new_instance_dfp(&zero);
            } else {
                return x.new_instance_dfp(&Dfp::copysign(y, &one));
            }
        }
        if x == one && y.classify() == INFINITE as i32 {
            x.get_field().set_i_e_e_e_flags_bits(FLAG_INVALID);
            return x.dotrap(FLAG_INVALID,
                            POW_TRAP,
                            &x,
                            &x.new_instance_sig_code(1 as i8, QNAN));
        }
        if x.classify() == INFINITE as i32 {
            // x = +/- inf
            if invert {
                // negative infinity
                if y.classify() == FINITE as i32 && y.rint() == *y && y.remainder(&two) != zero {
                    // If y is odd integer
                    if y.greater_than(&zero) {
                        return x.new_instance_dfp(&x.new_instance_sig_code(-1 as i8, INFINITE));
                    } else {
                        return x.new_instance_dfp(&zero.negate());
                    }
                } else {
                    // Y is not odd integer
                    if y.greater_than(&zero) {
                        return x.new_instance_dfp(&x.new_instance_sig_code(1 as i8, INFINITE));
                    } else {
                        return x.new_instance_dfp(&zero);
                    }
                }
            } else {
                // positive infinity
                if y.greater_than(&zero) {
                    return x;
                } else {
                    return x.new_instance_dfp(&zero);
                }
            }
        }
        if invert && y.rint() != *y {
            x.get_field().set_i_e_e_e_flags_bits(FLAG_INVALID);
            return x.dotrap(FLAG_INVALID,
                            POW_TRAP,
                            &x,
                            &x.new_instance_sig_code(1 as i8, QNAN));
        }
        // End special cases
        let mut r: Dfp;
        if y.less_than(&x.new_instance_i32(100000000)) &&
           y.greater_than(&x.new_instance_i32(-100000000)) {
            println!("x: {}", x.to_string());
            println!("y: {}", y.to_string());
            let u: Dfp = y.rint();
            ui = u.int_value();
            let v: Dfp = y.subtract(&u);
            println!("v: {}", v.to_string());
            if v != zero {
                let a: Dfp = v.multiply(&DfpMath::log(&x));
                println!("a: {}, ln(2): {}, log(x): {}",
                         a.to_string(),
                         x.get_field().get_ln2().to_string(),
                         DfpMath::log(&x).to_string());
                let b: Dfp = a.divide(&x.get_field().get_ln2()).unwrap().rint();
                println!("b: {}", b.to_string());
                let c: Dfp = a.subtract(&b.multiply(&x.get_field().get_ln2()));
                println!("c: {}", c.to_string());
                let split = DfpMath::split(&x);
                println!("split: {} {}", split[0].to_string(), split[1].to_string());
                r = DfpMath::split_pow(&split, ui);
                println!("r: {}", r.to_string());
                r = r.multiply(&DfpMath::pow_i32(&two, b.int_value()));
                println!("r: {}", r.to_string());
                r = r.multiply(&DfpMath::exp(&c));
                println!("r: {}", r.to_string());
            } else {
                r = DfpMath::split_pow(&DfpMath::split(&x), ui);
            }
        } else {
            // very large exponent.  |y| > 1e8
            r = DfpMath::exp(&DfpMath::log(&x).multiply(&y));
        }
        if invert && y.rint() == *y && y.remainder(&two) != zero {
            // if y is odd integer
            r = r.negate();
        }
        return x.new_instance_dfp(&r);
    }

    /** Computes sin(a)  Used when 0 < a < pi/4.
     * Uses the classic Taylor series.  x - x**3/3! + x**5/5!  ...
     * @param a number from which sine is desired, in split form
     * @return sin(a)
     */
    pub fn sin_internal(a: &[Dfp; 2]) -> Dfp {
        let mut c: Dfp = a[0].add(&a[1]);
        let mut y: Dfp = c.clone();
        c = c.multiply(&c);
        let mut x: Dfp = y.clone();
        let mut fact: Dfp = a[0].get_one();
        let mut py: Dfp = y.clone();
        {
            let mut i: usize = 3;
            while i < 90 {
                {
                    x = x.multiply(&c);
                    x = x.negate();
                    // 1 over fact
                    fact = fact.divide_i32(((i - 1) * i) as i32);
                    y = y.add(&x.multiply(&fact));
                    if y == py {
                        break;
                    }
                    py = y.clone();
                }
                i += 2;
            }
        }

        return y;
    }

    /** Computes cos(a)  Used when 0 < a < pi/4.
     * Uses the classic Taylor series for cosine.  1 - x**2/2! + x**4/4!  ...
     * @param a number from which cosine is desired, in split form
     * @return cos(a)
     */
    pub fn cos_internal(a: &[Dfp; 2]) -> Dfp {
        let one: Dfp = a[0].get_one();
        let mut x: Dfp = one.clone();
        let mut y: Dfp = one.clone();
        let mut c: Dfp = a[0].add(&a[1]);
        c = c.multiply(&c);
        let mut fact: Dfp = one.clone();
        let mut py: Dfp = y.clone();
        {
            let mut i: usize = 2;
            while i < 90 {
                {
                    x = x.multiply(&c);
                    x = x.negate();
                    // 1 over fact
                    fact = fact.divide_i32(((i - 1) * i) as i32);
                    y = y.add(&x.multiply(&fact));
                    if y == py {
                        break;
                    }
                    py = y.clone();
                }
                i += 2;
            }
        }

        return y;
    }

    /** computes the sine of the argument.
     * @param a number from which sine is desired
     * @return sin(a)
     */
    pub fn sin(a: &Dfp) -> Dfp {
        let pi: Dfp = a.get_field().get_pi();
        let zero: Dfp = a.get_field().get_zero();
        let mut neg: bool = false;
        // First reduce the argument to the range of +/- PI
        let mut x: Dfp = a.remainder(&pi.multiply_i32(2));
        // This puts x in the range 0 < x < PI
        if x.less_than(&zero) {
            x = x.negate();
            neg = true;
        }
        if x.greater_than(&pi.divide_i32(2)) {
            x = pi.subtract(&x);
        }
        let mut y: Dfp;
        if x.less_than(&pi.divide_i32(4)) {
            y = DfpMath::sin_internal(&DfpMath::split(&x));
        } else {

            let pi_split = a.get_field().get_pi_split();
            let c: [Dfp; 2] = [pi_split[0].divide_i32(2).subtract(&x), pi_split[1].divide_i32(2)];
            y = DfpMath::cos_internal(&c);
        }
        if neg {
            y = y.negate();
        }
        return a.new_instance_dfp(&y);
    }

    /** computes the cosine of the argument.
     * @param a number from which cosine is desired
     * @return cos(a)
     */
    pub fn cos(a: &Dfp) -> Dfp {
        let pi: Dfp = a.get_field().get_pi();
        let zero: Dfp = a.get_field().get_zero();
        let mut neg: bool = false;
        // First reduce the argument to the range of +/- PI
        let mut x: Dfp = a.remainder(&pi.multiply_i32(2));
        // This puts x in the range 0 < x < PI
        if x.less_than(&zero) {
            x = x.negate();
        }
        if x.greater_than(&pi.divide_i32(2)) {
            x = pi.subtract(&x);
            neg = true;
        }
        let mut y: Dfp;
        if x.less_than(&pi.divide_i32(4)) {
            let c: [Dfp; 2] = [x, zero];
            y = DfpMath::cos_internal(&c);
        } else {
            let pi_split = a.get_field().get_pi_split();
            let c = [pi_split[0].divide_i32(2).subtract(&x), pi_split[1].divide_i32(2)];
            y = DfpMath::sin_internal(&c);
        }
        if neg {
            y = y.negate();
        }
        return a.new_instance_dfp(&y);
    }

    /** computes the tangent of the argument.
     * @param a number from which tangent is desired
     * @return tan(a)
     */
    pub fn tan(a: &Dfp) -> Dfp {
        return DfpMath::sin(a).divide(&DfpMath::cos(a)).unwrap();
    }

    /** computes the arc-tangent of the argument.
     * @param a number from which arc-tangent is desired
     * @return atan(a)
     */
    pub fn atan_internal(a: &Dfp) -> Dfp {
        let mut y: Dfp = a.clone();
        let mut x: Dfp = y.clone();
        let mut py: Dfp = y.clone();
        {
            let mut i: i32 = 3;
            while i < 90 {
                {
                    x = x.multiply(a);
                    x = x.multiply(a);
                    x = x.negate();
                    y = y.add(&x.divide_i32(i));
                    if y == py {
                        break;
                    }
                    py = y.clone();
                }
                i += 2;
            }
        }

        return y;
    }

    /** computes the arc tangent of the argument
     *
     *  Uses the typical taylor series
     *
     *  but may reduce arguments using the following identity
     * tan(x+y) = (tan(x) + tan(y)) / (1 - tan(x)*tan(y))
     *
     * since tan(PI/8) = sqrt(2)-1,
     *
     * atan(x) = atan( (x - sqrt(2) + 1) / (1+x*sqrt(2) - x) + PI/8.0
     * @param a number from which arc-tangent is desired
     * @return atan(a)
     */
    pub fn atan(a: &Dfp) -> Dfp {
        let zero: Dfp = a.get_field().get_zero();
        let one: Dfp = a.get_field().get_one();
        let sqr2_split = a.get_field().get_sqr2_split();
        let pi_split = a.get_field().get_pi_split();
        let mut recp: bool = false;
        let mut neg: bool = false;
        let mut sub: bool = false;
        let ty: Dfp = sqr2_split[0].subtract(&one).add(&sqr2_split[1]);
        let mut x: Dfp = a.clone();
        if x.less_than(&zero) {
            neg = true;
            x = x.negate();
        }
        if x.greater_than(&one) {
            recp = true;
            x = one.divide(&x).unwrap();
        }
        if x.greater_than(&ty) {
            sub = true;
            let sty = [sqr2_split[0].subtract(&one), sqr2_split[1].clone()];
            let mut xs = DfpMath::split(&x);
            let mut ds = DfpMath::split_mult(&xs, &sty);
            ds[0] = ds[0].add(&one);
            xs[0] = xs[0].subtract(&sty[0]);
            xs[1] = xs[1].subtract(&sty[1]);
            xs = DfpMath::split_div(&xs, &ds);
            x = xs[0].add(&xs[1]);
            // x = x.subtract(ty).divide(dfp.one.add(x.multiply(ty)));
        }
        let mut y: Dfp = DfpMath::atan_internal(&x);
        if sub {
            y = y.add(&pi_split[0].divide_i32(8)).add(&pi_split[1].divide_i32(8));
        }
        if recp {
            y = pi_split[0].divide_i32(2).subtract(&y).add(&pi_split[1].divide_i32(2));
        }
        if neg {
            y = y.negate();
        }
        return a.new_instance_dfp(&y);
    }

    /** computes the arc-sine of the argument.
     * @param a number from which arc-sine is desired
     * @return asin(a)
     */
    pub fn asin(a: &Dfp) -> Dfp {
        return DfpMath::atan(&a.divide(&a.get_one().subtract(&a.multiply(&a)).sqrt()).unwrap());
    }

    /** computes the arc-cosine of the argument.
     * @param a number from which arc-cosine is desired
     * @return acos(a)
     */
    pub fn acos(a_p: &Dfp) -> Dfp {
        let mut a = a_p.clone();
        let mut result: Dfp;
        let mut negative: bool = false;
        if a.less_than(&a.get_zero()) {
            negative = true;
        }
        // absolute value
        a = Dfp::copysign(&a, &a.get_one());
        result = DfpMath::atan(&a.get_one().subtract(&a.multiply(&a)).sqrt().divide(&a).unwrap());
        if negative {
            result = a.get_field().get_pi().subtract(&result);
        }
        return a.new_instance_dfp(&result);
    }
}
