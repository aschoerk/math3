#[allow(unused_variables)]

#[cfg(test)]
mod tests {
    pub use util::fastmath;
    pub use util::precision;
    pub use dfp::dfp_field::*;
    pub use dfp::dfp::*;
    pub use std::f64;
    pub use std::i64;
    pub use std::i32;
    pub use hamcrest::*;
    pub use std::i8;
 
 // Generic test function.  Takes params x and y and tests them for
    // equality.  Then checks the status flags against the flags argument.
    // If the test fail, it prints the desc string
    pub fn  test(x: &Dfp,  y: &Dfp,  flags: i32,  desc: &str)   {
         let mut b: bool = x == y;
        if // NaNs involved
        !(*x == *y) && !(*x != *y) {
            b = x.to_string() == y.to_string();
        }

        if // distinguish +/- zero
        *x == x.get_zero() {
            b = b && (x.to_string() == y.to_string());
        }

        b = b && x.get_field().get_i_e_e_e_flags() == flags;
        if !b {
        	let msg = format!("assertion failed {} x = {} flags = {} y: {} expected flags: {}",desc,x.to_string(),x.get_field().get_i_e_e_e_flags(),y.to_string(), flags);
            assert!(b, msg);
        }
        x.get_field().clear_i_e_e_e_flags();
    }
    
    // utility function to help test comparisons
    pub fn  cmptst(a: &Dfp,  b: &Dfp,  op: &str,  result: bool,  num: f64)   {
        if op == "equal" {
            if a.eq(b) != result {
                assert!(false,"assersion failed.  {} compare #{}", op,  num);
            }

        }

        if op == "unequal" {
            if a.ne(b) != result {
                assert!(false,"assersion failed.  {} compare #{}",op , num);
            }

        }

        if op == "lessThan" {
            if a.less_than(b) != result {
                assert!(false,"assersion failed.  {} compare #{}",op ,  num);
            }

        }

        if op == "greaterThan" {
            if a.greater_than(b) != result {
                assert!(false,"assersion failed.  {} compare #{}",op ,  num);
            }

        }

    }


    describe! stainless {
    	before_each {
    		// Some basic setup.  Define some constants and clear the status flags
	        let field = get_dfp_field_for(&DfpField::new(20));
	        let pinf = field.new_dfp_str("1").unwrap().divide(&field.new_dfp_str("0").unwrap()).unwrap();
	        let ninf = field.new_dfp_str("-1").unwrap().divide(&field.new_dfp_str("0").unwrap()).unwrap();
	        let nan = field.new_dfp_str("0").unwrap().divide(&field.new_dfp_str("0").unwrap()).unwrap();
	        let snan = field.new_dfp_sign_nans(1 as i8, SNAN);
	        let qnan = field.new_dfp_sign_nans(1 as i8, QNAN);
	        ninf.get_field().clear_i_e_e_e_flags();
    	}
    	
    	after_each {
    		field.clear_i_e_e_e_flags();
    	}    	
    	
    	it "can compare" {
    		
        // test equal() comparison
        // check zero vs. zero
        field.clear_i_e_e_e_flags();
        // 0 == 0
        cmptst(&field.new_dfp_str("0").unwrap(), &field.new_dfp_str("0").unwrap(), "equal", true, 1.0);
        // 0 == -0
        cmptst(&field.new_dfp_str("0").unwrap(), &field.new_dfp_str("-0").unwrap(), "equal", true, 2.0);
        // -0 == -0
        cmptst(&field.new_dfp_str("-0").unwrap(), &field.new_dfp_str("-0").unwrap(), "equal", true, 3.0);
        // -0 == 0
        cmptst(&field.new_dfp_str("-0").unwrap(), &field.new_dfp_str("0").unwrap(), "equal", true, 4.0);
        // check zero vs normal numbers
        // 0 == 1
        cmptst(&field.new_dfp_str("0").unwrap(), &field.new_dfp_str("1").unwrap(), "equal", false, 5.0);
        // 1 == 0
        cmptst(&field.new_dfp_str("1").unwrap(), &field.new_dfp_str("0").unwrap(), "equal", false, 6.0);
        // -1 == 0
        cmptst(&field.new_dfp_str("-1").unwrap(), &field.new_dfp_str("0").unwrap(), "equal", false, 7.0);
        // 0 == -1
        cmptst(&field.new_dfp_str("0").unwrap(), &field.new_dfp_str("-1").unwrap(), "equal", false, 8.0);
        // 0 == 1e-131072
        cmptst(&field.new_dfp_str("0").unwrap(), &field.new_dfp_str("1e-131072").unwrap(), "equal", false, 9.0);
        // check flags
        if field.get_i_e_e_e_flags() != 0 {
            assert!(false,"assersion failed.  compare flags = {}",  field.get_i_e_e_e_flags());
        }

        // 0 == 1e-131078
        cmptst(&field.new_dfp_str("0").unwrap(), &field.new_dfp_str("1e-131078").unwrap(), "equal", false, 10.0);
        // check flags  -- underflow should be set
        if field.get_i_e_e_e_flags() != FLAG_UNDERFLOW {
            assert!(false,"assersion failed.  compare flags = {}",  field.get_i_e_e_e_flags());
        }
        field.clear_i_e_e_e_flags();
        // 0 == 1e+131071
        cmptst(&field.new_dfp_str("0").unwrap(), &field.new_dfp_str("1e+131071").unwrap(), "equal", false, 11.0);
        // check zero vs infinities
        // 0 == pinf
        cmptst(&field.new_dfp_str("0").unwrap(), &pinf, "equal", false, 12.0);
        // 0 == ninf
        cmptst(&field.new_dfp_str("0").unwrap(), &ninf, "equal", false, 13.0);
        // -0 == pinf
        cmptst(&field.new_dfp_str("-0").unwrap(), &pinf, "equal", false, 14.0);
        // -0 == ninf
        cmptst(&field.new_dfp_str("-0").unwrap(), &ninf, "equal", false, 15.0);
        // pinf == 0
        cmptst(&pinf, &field.new_dfp_str("0").unwrap(), "equal", false, 16.0);
        // ninf == 0
        cmptst(&ninf, &field.new_dfp_str("0").unwrap(), "equal", false, 17.0);
        // pinf == -0
        cmptst(&pinf, &field.new_dfp_str("-0").unwrap(), "equal", false, 18.0);
        // ninf == -0
        cmptst(&ninf, &field.new_dfp_str("-0").unwrap(), "equal", false, 19.0);
        // ninf == pinf
        cmptst(&ninf, &pinf, "equal", false, 19.10);
        // pinf == ninf
        cmptst(&pinf, &ninf, "equal", false, 19.11);
        // pinf == pinf
        cmptst(&pinf, &pinf, "equal", true, 19.12);
        // ninf == ninf
        cmptst(&ninf, &ninf, "equal", true, 19.13);
        // check some normal numbers
        // 1 == 1
        cmptst(&field.new_dfp_str("1").unwrap(), &field.new_dfp_str("1").unwrap(), "equal", true, 20.0);
        // 1 == -1
        cmptst(&field.new_dfp_str("1").unwrap(), &field.new_dfp_str("-1").unwrap(), "equal", false, 21.0);
        // -1 == -1
        cmptst(&field.new_dfp_str("-1").unwrap(), &field.new_dfp_str("-1").unwrap(), "equal", true, 22.0);
        // 1 == 1.0000000000000001
        cmptst(&field.new_dfp_str("1").unwrap(), &field.new_dfp_str("1.0000000000000001").unwrap(), "equal", false, 23.0);
        // The tests below checks to ensure that comparisons don't set FLAG_INEXACT
        // 100000 == 1.0000000000000001
        cmptst(&field.new_dfp_str("1e20").unwrap(), &field.new_dfp_str("1.0000000000000001").unwrap(), "equal", false, 24.0);
        if field.get_i_e_e_e_flags() != 0 {
            assert!(false,"assersion failed.  compare flags = {}",  field.get_i_e_e_e_flags());
        }

        cmptst(&field.new_dfp_str("0.000001").unwrap(), &field.new_dfp_str("1e-6").unwrap(), "equal", true, 25.0);
        // check some nans -- nans shouldnt equal anything
        cmptst(&snan, &snan, "equal", false, 27.0);
        cmptst(&qnan, &qnan, "equal", false, 28.0);
        cmptst(&snan, &qnan, "equal", false, 29.0);
        cmptst(&qnan, &snan, "equal", false, 30.0);
        cmptst(&qnan, &field.new_dfp_str("0").unwrap(), "equal", false, 31.0);
        cmptst(&snan, &field.new_dfp_str("0").unwrap(), "equal", false, 32.0);
        cmptst(&field.new_dfp_str("0").unwrap(), &snan, "equal", false, 33.0);
        cmptst(&field.new_dfp_str("0").unwrap(), &qnan, "equal", false, 34.0);
        cmptst(&qnan, &pinf, "equal", false, 35.0);
        cmptst(&snan, &pinf, "equal", false, 36.0);
        cmptst(&pinf, &snan, "equal", false, 37.0);
        cmptst(&pinf, &qnan, "equal", false, 38.0);
        cmptst(&qnan, &ninf, "equal", false, 39.0);
        cmptst(&snan, &ninf, "equal", false, 40.0);
        cmptst(&ninf, &snan, "equal", false, 41.0);
        cmptst(&ninf, &qnan, "equal", false, 42.0);
        cmptst(&qnan, &field.new_dfp_str("-1").unwrap(), "equal", false, 43.0);
        cmptst(&snan, &field.new_dfp_str("-1").unwrap(), "equal", false, 44.0);
        cmptst(&field.new_dfp_str("-1").unwrap(), &snan, "equal", false, 45.0);
        cmptst(&field.new_dfp_str("-1").unwrap(), &qnan, "equal", false, 46.0);
        cmptst(&qnan, &field.new_dfp_str("1").unwrap(), "equal", false, 47.0);
        cmptst(&snan, &field.new_dfp_str("1").unwrap(), "equal", false, 48.0);
        cmptst(&field.new_dfp_str("1").unwrap(), &snan, "equal", false, 49.0);
        cmptst(&field.new_dfp_str("1").unwrap(), &qnan, "equal", false, 50.0);
        cmptst(&snan.negate(), &snan, "equal", false, 51.0);
        cmptst(&qnan.negate(), &qnan, "equal", false, 52.0);
        //
        // Tests for un equal  -- do it all over again
        //
        // 0 == 0
        cmptst(&field.new_dfp_str("0").unwrap(), &field.new_dfp_str("0").unwrap(), "unequal", false, 1.0);
        // 0 == -0
        cmptst(&field.new_dfp_str("0").unwrap(), &field.new_dfp_str("-0").unwrap(), "unequal", false, 2.0);
        // -0 == -0
        cmptst(&field.new_dfp_str("-0").unwrap(), &field.new_dfp_str("-0").unwrap(), "unequal", false, 3.0);
        // -0 == 0
        cmptst(&field.new_dfp_str("-0").unwrap(), &field.new_dfp_str("0").unwrap(), "unequal", false, 4.0);
        // check zero vs normal numbers
        // 0 == 1
        cmptst(&field.new_dfp_str("0").unwrap(), &field.new_dfp_str("1").unwrap(), "unequal", true, 5.0);
        // 1 == 0
        cmptst(&field.new_dfp_str("1").unwrap(), &field.new_dfp_str("0").unwrap(), "unequal", true, 6.0);
        // -1 == 0
        cmptst(&field.new_dfp_str("-1").unwrap(), &field.new_dfp_str("0").unwrap(), "unequal", true, 7.0);
        // 0 == -1
        cmptst(&field.new_dfp_str("0").unwrap(), &field.new_dfp_str("-1").unwrap(), "unequal", true, 8.0);
        // 0 == 1e-131072
        cmptst(&field.new_dfp_str("0").unwrap(), &field.new_dfp_str("1e-131072").unwrap(), "unequal", true, 9.0);
        // check flags
        if field.get_i_e_e_e_flags() != 0 {
            assert!(false,"assersion failed.  compare flags = {}",  field.get_i_e_e_e_flags());
        }

        // 0 == 1e-131078
        cmptst(&field.new_dfp_str("0").unwrap(), &field.new_dfp_str("1e-131078").unwrap(), "unequal", true, 10.0);
        // check flags  -- underflow should be set
        if field.get_i_e_e_e_flags() != FLAG_UNDERFLOW {
            assert!(false,"assersion failed.  compare flags = {}",  field.get_i_e_e_e_flags());
        }

        field.clear_i_e_e_e_flags();
        // 0 == 1e+131071
        cmptst(&field.new_dfp_str("0").unwrap(), &field.new_dfp_str("1e+131071").unwrap(), "unequal", true, 11.0);
        // check zero vs infinities
        // 0 == pinf
        cmptst(&field.new_dfp_str("0").unwrap(), &pinf, "unequal", true, 12.0);
        // 0 == ninf
        cmptst(&field.new_dfp_str("0").unwrap(), &ninf, "unequal", true, 13.0);
        // -0 == pinf
        cmptst(&field.new_dfp_str("-0").unwrap(), &pinf, "unequal", true, 14.0);
        // -0 == ninf
        cmptst(&field.new_dfp_str("-0").unwrap(), &ninf, "unequal", true, 15.0);
        // pinf == 0
        cmptst(&pinf, &field.new_dfp_str("0").unwrap(), "unequal", true, 16.0);
        // ninf == 0
        cmptst(&ninf, &field.new_dfp_str("0").unwrap(), "unequal", true, 17.0);
        // pinf == -0
        cmptst(&pinf, &field.new_dfp_str("-0").unwrap(), "unequal", true, 18.0);
        // ninf == -0
        cmptst(&ninf, &field.new_dfp_str("-0").unwrap(), "unequal", true, 19.0);
        // ninf == pinf
        cmptst(&ninf, &pinf, "unequal", true, 19.10);
        // pinf == ninf
        cmptst(&pinf, &ninf, "unequal", true, 19.11);
        // pinf == pinf
        cmptst(&pinf, &pinf, "unequal", false, 19.12);
        // ninf == ninf
        cmptst(&ninf, &ninf, "unequal", false, 19.13);
        // check some normal numbers
        // 1 == 1
        cmptst(&field.new_dfp_str("1").unwrap(), &field.new_dfp_str("1").unwrap(), "unequal", false, 20.0);
        // 1 == -1
        cmptst(&field.new_dfp_str("1").unwrap(), &field.new_dfp_str("-1").unwrap(), "unequal", true, 21.0);
        // -1 == -1
        cmptst(&field.new_dfp_str("-1").unwrap(), &field.new_dfp_str("-1").unwrap(), "unequal", false, 22.0);
        // 1 == 1.0000000000000001
        cmptst(&field.new_dfp_str("1").unwrap(), &field.new_dfp_str("1.0000000000000001").unwrap(), "unequal", true, 23.0);
        // The tests below checks to ensure that comparisons don't set FLAG_INEXACT
        // 100000 == 1.0000000000000001
        cmptst(&field.new_dfp_str("1e20").unwrap(), &field.new_dfp_str("1.0000000000000001").unwrap(), "unequal", true, 24.0);
        if field.get_i_e_e_e_flags() != 0 {
            assert!(false,"assersion failed.  compare flags = {}",  field.get_i_e_e_e_flags());
        }

        cmptst(&field.new_dfp_str("0.000001").unwrap(), &field.new_dfp_str("1e-6").unwrap(), "unequal", false, 25.0);
        // check some nans -- nans shouldnt be unequal to anything
        cmptst(&snan, &snan, "unequal", false, 27.0);
        cmptst(&qnan, &qnan, "unequal", false, 28.0);
        cmptst(&snan, &qnan, "unequal", false, 29.0);
        cmptst(&qnan, &snan, "unequal", false, 30.0);
        cmptst(&qnan, &field.new_dfp_str("0").unwrap(), "unequal", false, 31.0);
        cmptst(&snan, &field.new_dfp_str("0").unwrap(), "unequal", false, 32.0);
        cmptst(&field.new_dfp_str("0").unwrap(), &snan, "unequal", false, 33.0);
        cmptst(&field.new_dfp_str("0").unwrap(), &qnan, "unequal", false, 34.0);
        cmptst(&qnan, &pinf, "unequal", false, 35.0);
        cmptst(&snan, &pinf, "unequal", false, 36.0);
        cmptst(&pinf, &snan, "unequal", false, 37.0);
        cmptst(&pinf, &qnan, "unequal", false, 38.0);
        cmptst(&qnan, &ninf, "unequal", false, 39.0);
        cmptst(&snan, &ninf, "unequal", false, 40.0);
        cmptst(&ninf, &snan, "unequal", false, 41.0);
        cmptst(&ninf, &qnan, "unequal", false, 42.0);
        cmptst(&qnan, &field.new_dfp_str("-1").unwrap(), "unequal", false, 43.0);
        cmptst(&snan, &field.new_dfp_str("-1").unwrap(), "unequal", false, 44.0);
        cmptst(&field.new_dfp_str("-1").unwrap(), &snan, "unequal", false, 45.0);
        cmptst(&field.new_dfp_str("-1").unwrap(), &qnan, "unequal", false, 46.0);
        cmptst(&qnan, &field.new_dfp_str("1").unwrap(), "unequal", false, 47.0);
        cmptst(&snan, &field.new_dfp_str("1").unwrap(), "unequal", false, 48.0);
        cmptst(&field.new_dfp_str("1").unwrap(), &snan, "unequal", false, 49.0);
        cmptst(&field.new_dfp_str("1").unwrap(), &qnan, "unequal", false, 50.0);
        cmptst(&snan.negate(), &snan, "unequal", false, 51.0);
        cmptst(&qnan.negate(), &qnan, "unequal", false, 52.0);
        if field.get_i_e_e_e_flags() != 0 {
            assert!(false,"assersion failed.  compare unequal flags = {}",  field.get_i_e_e_e_flags());
        }

        //
        // Tests for lessThan  -- do it all over again
        //
        // 0 < 0
        cmptst(&field.new_dfp_str("0").unwrap(), &field.new_dfp_str("0").unwrap(), "lessThan", false, 1.0);
        // 0 < -0
        cmptst(&field.new_dfp_str("0").unwrap(), &field.new_dfp_str("-0").unwrap(), "lessThan", false, 2.0);
        // -0 < -0
        cmptst(&field.new_dfp_str("-0").unwrap(), &field.new_dfp_str("-0").unwrap(), "lessThan", false, 3.0);
        // -0 < 0
        cmptst(&field.new_dfp_str("-0").unwrap(), &field.new_dfp_str("0").unwrap(), "lessThan", false, 4.0);
        // check zero vs normal numbers
        // 0 < 1
        cmptst(&field.new_dfp_str("0").unwrap(), &field.new_dfp_str("1").unwrap(), "lessThan", true, 5.0);
        // 1 < 0
        cmptst(&field.new_dfp_str("1").unwrap(), &field.new_dfp_str("0").unwrap(), "lessThan", false, 6.0);
        // -1 < 0
        cmptst(&field.new_dfp_str("-1").unwrap(), &field.new_dfp_str("0").unwrap(), "lessThan", true, 7.0);
        // 0 < -1
        cmptst(&field.new_dfp_str("0").unwrap(), &field.new_dfp_str("-1").unwrap(), "lessThan", false, 8.0);
        // 0 < 1e-131072
        cmptst(&field.new_dfp_str("0").unwrap(), &field.new_dfp_str("1e-131072").unwrap(), "lessThan", true, 9.0);
        // check flags
        if field.get_i_e_e_e_flags() != 0 {
            assert!(false,"assersion failed.  compare flags = {}",  field.get_i_e_e_e_flags());
        }

        // 0 < 1e-131078
        cmptst(&field.new_dfp_str("0").unwrap(), &field.new_dfp_str("1e-131078").unwrap(), "lessThan", true, 10.0);
        // check flags  -- underflow should be set
        if field.get_i_e_e_e_flags() != FLAG_UNDERFLOW {
            assert!(false,"assersion failed.  compare flags = {}",  field.get_i_e_e_e_flags());
        }

        field.clear_i_e_e_e_flags();
        // 0 < 1e+131071
        cmptst(&field.new_dfp_str("0").unwrap(), &field.new_dfp_str("1e+131071").unwrap(), "lessThan", true, 11.0);
        // check zero vs infinities
        // 0 < pinf
        cmptst(&field.new_dfp_str("0").unwrap(), &pinf, "lessThan", true, 12.0);
        // 0 < ninf
        cmptst(&field.new_dfp_str("0").unwrap(), &ninf, "lessThan", false, 13.0);
        // -0 < pinf
        cmptst(&field.new_dfp_str("-0").unwrap(), &pinf, "lessThan", true, 14.0);
        // -0 < ninf
        cmptst(&field.new_dfp_str("-0").unwrap(), &ninf, "lessThan", false, 15.0);
        // pinf < 0
        cmptst(&pinf, &field.new_dfp_str("0").unwrap(), "lessThan", false, 16.0);
        // ninf < 0
        cmptst(&ninf, &field.new_dfp_str("0").unwrap(), "lessThan", true, 17.0);
        // pinf < -0
        cmptst(&pinf, &field.new_dfp_str("-0").unwrap(), "lessThan", false, 18.0);
        // ninf < -0
        cmptst(&ninf, &field.new_dfp_str("-0").unwrap(), "lessThan", true, 19.0);
        // ninf < pinf
        cmptst(&ninf, &pinf, "lessThan", true, 19.10);
        // pinf < ninf
        cmptst(&pinf, &ninf, "lessThan", false, 19.11);
        // pinf < pinf
        cmptst(&pinf, &pinf, "lessThan", false, 19.12);
        // ninf < ninf
        cmptst(&ninf, &ninf, "lessThan", false, 19.13);
        // check some normal numbers
        // 1 < 1
        cmptst(&field.new_dfp_str("1").unwrap(), &field.new_dfp_str("1").unwrap(), "lessThan", false, 20.0);
        // 1 < -1
        cmptst(&field.new_dfp_str("1").unwrap(), &field.new_dfp_str("-1").unwrap(), "lessThan", false, 21.0);
        // -1 < -1
        cmptst(&field.new_dfp_str("-1").unwrap(), &field.new_dfp_str("-1").unwrap(), "lessThan", false, 22.0);
        // 1 < 1.0000000000000001
        cmptst(&field.new_dfp_str("1").unwrap(), &field.new_dfp_str("1.0000000000000001").unwrap(), "lessThan", true, 23.0);
        // The tests below checks to ensure that comparisons don't set FLAG_INEXACT
        // 100000 < 1.0000000000000001
        cmptst(&field.new_dfp_str("1e20").unwrap(), &field.new_dfp_str("1.0000000000000001").unwrap(), "lessThan", false, 24.0);
        if field.get_i_e_e_e_flags() != 0 {
            assert!(false,"assersion failed.  compare flags = {}",  field.get_i_e_e_e_flags());
        }

        cmptst(&field.new_dfp_str("0.000001").unwrap(), &field.new_dfp_str("1e-6").unwrap(), "lessThan", false, 25.0);
        // check some nans -- nans shouldnt be lessThan to anything
        cmptst(&snan, &snan, "lessThan", false, 27.0);
        cmptst(&qnan, &qnan, "lessThan", false, 28.0);
        cmptst(&snan, &qnan, "lessThan", false, 29.0);
        cmptst(&qnan, &snan, "lessThan", false, 30.0);
        cmptst(&qnan, &field.new_dfp_str("0").unwrap(), "lessThan", false, 31.0);
        cmptst(&snan, &field.new_dfp_str("0").unwrap(), "lessThan", false, 32.0);
        cmptst(&field.new_dfp_str("0").unwrap(), &snan, "lessThan", false, 33.0);
        cmptst(&field.new_dfp_str("0").unwrap(), &qnan, "lessThan", false, 34.0);
        cmptst(&qnan, &pinf, "lessThan", false, 35.0);
        cmptst(&snan, &pinf, "lessThan", false, 36.0);
        cmptst(&pinf, &snan, "lessThan", false, 37.0);
        cmptst(&pinf, &qnan, "lessThan", false, 38.0);
        cmptst(&qnan, &ninf, "lessThan", false, 39.0);
        cmptst(&snan, &ninf, "lessThan", false, 40.0);
        cmptst(&ninf, &snan, "lessThan", false, 41.0);
        cmptst(&ninf, &qnan, "lessThan", false, 42.0);
        cmptst(&qnan, &field.new_dfp_str("-1").unwrap(), "lessThan", false, 43.0);
        cmptst(&snan, &field.new_dfp_str("-1").unwrap(), "lessThan", false, 44.0);
        cmptst(&field.new_dfp_str("-1").unwrap(), &snan, "lessThan", false, 45.0);
        cmptst(&field.new_dfp_str("-1").unwrap(), &qnan, "lessThan", false, 46.0);
        cmptst(&qnan, &field.new_dfp_str("1").unwrap(), "lessThan", false, 47.0);
        cmptst(&snan, &field.new_dfp_str("1").unwrap(), "lessThan", false, 48.0);
        cmptst(&field.new_dfp_str("1").unwrap(), &snan, "lessThan", false, 49.0);
        cmptst(&field.new_dfp_str("1").unwrap(), &qnan, "lessThan", false, 50.0);
        cmptst(&snan.negate(), &snan, "lessThan", false, 51.0);
        cmptst(&qnan.negate(), &qnan, "lessThan", false, 52.0);
        //lessThan compares with nans should raise FLAG_INVALID
        if field.get_i_e_e_e_flags() != FLAG_INVALID {
            assert!(false,"assersion failed.  compare lessThan flags = {}",  field.get_i_e_e_e_flags());
        }

        field.clear_i_e_e_e_flags();
        //
        // Tests for greaterThan  -- do it all over again
        //
        // 0 > 0
        cmptst(&field.new_dfp_str("0").unwrap(), &field.new_dfp_str("0").unwrap(), "greaterThan", false, 1.0);
        // 0 > -0
        cmptst(&field.new_dfp_str("0").unwrap(), &field.new_dfp_str("-0").unwrap(), "greaterThan", false, 2.0);
        // -0 > -0
        cmptst(&field.new_dfp_str("-0").unwrap(), &field.new_dfp_str("-0").unwrap(), "greaterThan", false, 3.0);
        // -0 > 0
        cmptst(&field.new_dfp_str("-0").unwrap(), &field.new_dfp_str("0").unwrap(), "greaterThan", false, 4.0);
        // check zero vs normal numbers
        // 0 > 1
        cmptst(&field.new_dfp_str("0").unwrap(), &field.new_dfp_str("1").unwrap(), "greaterThan", false, 5.0);
        // 1 > 0
        cmptst(&field.new_dfp_str("1").unwrap(), &field.new_dfp_str("0").unwrap(), "greaterThan", true, 6.0);
        // -1 > 0
        cmptst(&field.new_dfp_str("-1").unwrap(), &field.new_dfp_str("0").unwrap(), "greaterThan", false, 7.0);
        // 0 > -1
        cmptst(&field.new_dfp_str("0").unwrap(), &field.new_dfp_str("-1").unwrap(), "greaterThan", true, 8.0);
        // 0 > 1e-131072
        cmptst(&field.new_dfp_str("0").unwrap(), &field.new_dfp_str("1e-131072").unwrap(), "greaterThan", false, 9.0);
        // check flags
        if field.get_i_e_e_e_flags() != 0 {
            assert!(false,"assersion failed.  compare flags = {}",  field.get_i_e_e_e_flags());
        }

        // 0 > 1e-131078
        cmptst(&field.new_dfp_str("0").unwrap(), &field.new_dfp_str("1e-131078").unwrap(), "greaterThan", false, 10.0);
        // check flags  -- underflow should be set
        if field.get_i_e_e_e_flags() != FLAG_UNDERFLOW {
            assert!(false,"assersion failed.  compare flags = {}",  field.get_i_e_e_e_flags());
        }

        field.clear_i_e_e_e_flags();
        // 0 > 1e+131071
        cmptst(&field.new_dfp_str("0").unwrap(), &field.new_dfp_str("1e+131071").unwrap(), "greaterThan", false, 11.0);
        // check zero vs infinities
        // 0 > pinf
        cmptst(&field.new_dfp_str("0").unwrap(), &pinf, "greaterThan", false, 12.0);
        // 0 > ninf
        cmptst(&field.new_dfp_str("0").unwrap(), &ninf, "greaterThan", true, 13.0);
        // -0 > pinf
        cmptst(&field.new_dfp_str("-0").unwrap(), &pinf, "greaterThan", false, 14.0);
        // -0 > ninf
        cmptst(&field.new_dfp_str("-0").unwrap(), &ninf, "greaterThan", true, 15.0);
        // pinf > 0
        cmptst(&pinf, &field.new_dfp_str("0").unwrap(), "greaterThan", true, 16.0);
        // ninf > 0
        cmptst(&ninf, &field.new_dfp_str("0").unwrap(), "greaterThan", false, 17.0);
        // pinf > -0
        cmptst(&pinf, &field.new_dfp_str("-0").unwrap(), "greaterThan", true, 18.0);
        // ninf > -0
        cmptst(&ninf, &field.new_dfp_str("-0").unwrap(), "greaterThan", false, 19.0);
        // ninf > pinf
        cmptst(&ninf, &pinf, "greaterThan", false, 19.10);
        // pinf > ninf
        cmptst(&pinf, &ninf, "greaterThan", true, 19.11);
        // pinf > pinf
        cmptst(&pinf, &pinf, "greaterThan", false, 19.12);
        // ninf > ninf
        cmptst(&ninf, &ninf, "greaterThan", false, 19.13);
        // check some normal numbers
        // 1 > 1
        cmptst(&field.new_dfp_str("1").unwrap(), &field.new_dfp_str("1").unwrap(), "greaterThan", false, 20.0);
        // 1 > -1
        cmptst(&field.new_dfp_str("1").unwrap(), &field.new_dfp_str("-1").unwrap(), "greaterThan", true, 21.0);
        // -1 > -1
        cmptst(&field.new_dfp_str("-1").unwrap(), &field.new_dfp_str("-1").unwrap(), "greaterThan", false, 22.0);
        // 1 > 1.0000000000000001
        cmptst(&field.new_dfp_str("1").unwrap(), &field.new_dfp_str("1.0000000000000001").unwrap(), "greaterThan", false, 23.0);
        // The tests below checks to ensure that comparisons don't set FLAG_INEXACT
        // 100000 > 1.0000000000000001
        cmptst(&field.new_dfp_str("1e20").unwrap(), &field.new_dfp_str("1.0000000000000001").unwrap(), "greaterThan", true, 24.0);
        if field.get_i_e_e_e_flags() != 0 {
            assert!(false,"assersion failed.  compare flags = {}",  field.get_i_e_e_e_flags());
        }

        cmptst(&field.new_dfp_str("0.000001").unwrap(), &field.new_dfp_str("1e-6").unwrap(), "greaterThan", false, 25.0);
        // check some nans -- nans shouldnt be greaterThan to anything
        cmptst(&snan, &snan, "greaterThan", false, 27.0);
        cmptst(&qnan, &qnan, "greaterThan", false, 28.0);
        cmptst(&snan, &qnan, "greaterThan", false, 29.0);
        cmptst(&qnan, &snan, "greaterThan", false, 30.0);
        cmptst(&qnan, &field.new_dfp_str("0").unwrap(), "greaterThan", false, 31.0);
        cmptst(&snan, &field.new_dfp_str("0").unwrap(), "greaterThan", false, 32.0);
        cmptst(&field.new_dfp_str("0").unwrap(), &snan, "greaterThan", false, 33.0);
        cmptst(&field.new_dfp_str("0").unwrap(), &qnan, "greaterThan", false, 34.0);
        cmptst(&qnan, &pinf, "greaterThan", false, 35.0);
        cmptst(&snan, &pinf, "greaterThan", false, 36.0);
        cmptst(&pinf, &snan, "greaterThan", false, 37.0);
        cmptst(&pinf, &qnan, "greaterThan", false, 38.0);
        cmptst(&qnan, &ninf, "greaterThan", false, 39.0);
        cmptst(&snan, &ninf, "greaterThan", false, 40.0);
        cmptst(&ninf, &snan, "greaterThan", false, 41.0);
        cmptst(&ninf, &qnan, "greaterThan", false, 42.0);
        cmptst(&qnan, &field.new_dfp_str("-1").unwrap(), "greaterThan", false, 43.0);
        cmptst(&snan, &field.new_dfp_str("-1").unwrap(), "greaterThan", false, 44.0);
        cmptst(&field.new_dfp_str("-1").unwrap(), &snan, "greaterThan", false, 45.0);
        cmptst(&field.new_dfp_str("-1").unwrap(), &qnan, "greaterThan", false, 46.0);
        cmptst(&qnan, &field.new_dfp_str("1").unwrap(), "greaterThan", false, 47.0);
        cmptst(&snan, &field.new_dfp_str("1").unwrap(), "greaterThan", false, 48.0);
        cmptst(&field.new_dfp_str("1").unwrap(), &snan, "greaterThan", false, 49.0);
        cmptst(&field.new_dfp_str("1").unwrap(), &qnan, "greaterThan", false, 50.0);
        cmptst(&snan.negate(), &snan, "greaterThan", false, 51.0);
        cmptst(&qnan.negate(), &qnan, "greaterThan", false, 52.0);
        //greaterThan compares with nans should raise FLAG_INVALID
        if field.get_i_e_e_e_flags() != FLAG_INVALID {
            assert!(false, "assersion failed.  compare greaterThan flags = {}", field.get_i_e_e_e_flags());
        }

        field.clear_i_e_e_e_flags();
    

    	}
    	
    	

        it "can_parse_strings" {
          test(&field.new_dfp_f64(1.00000000000001), &field.new_dfp_str("1.00000000000001").unwrap(), 16, "Parse #1");          
          test(&field.new_dfp_f64(-1.00000000000001), &field.new_dfp_str("-1.00000000000001").unwrap(), 16, "Parse #2");
          test(&field.new_dfp_f64(1.0e1), &field.new_dfp_str("1.0e1").unwrap(), 0, "Parse #3");
          test(&field.new_dfp_f64(-1.0e1), &field.new_dfp_str("-1.0e1").unwrap(), 0, "Parse #4");
          test(&field.new_dfp_f64(25.0e-2), &field.new_dfp_str("25.0e-2").unwrap(), 0, "Parse #5");
          test(&field.new_dfp_f64(0.0000000000000001), &field.new_dfp_str("0.0000000000000001").unwrap(), 16, "Parse #2.1");
          test(&field.new_dfp_f64(-0.0000000000000001), &field.new_dfp_str("-0.0000000000000001").unwrap(), 16, "Parse #2.2");
          test(&field.new_dfp_f64(0.0e1), &field.new_dfp_str("0.0e1").unwrap(), 0, "Parse #2.3");
          test(&field.new_dfp_f64(-0.0e1), &field.new_dfp_str("-0.0e1").unwrap(), 0, "Parse #2.4");
          test(&field.new_dfp_f64(0.0e-1), &field.new_dfp_str("0.0e-1").unwrap(), 0, "Parse #2.5");
          test(&field.new_dfp_f64(0.1000000000000001), &field.new_dfp_str("0.1000000000000001").unwrap(), 16, "Parse #3.1");
          test(&field.new_dfp_f64(-0.1000000000000001), &field.new_dfp_str("-0.1000000000000001").unwrap(), 16, "Parse #3.2");
          test(&field.new_dfp_f64(0.1e1), &field.new_dfp_str("0.1e1").unwrap(), 0, "Parse #3.3");
          test(&field.new_dfp_f64(-0.1e1), &field.new_dfp_str("-0.1e1").unwrap(), 0, "Parse #3.4");
          test(&field.new_dfp_f64(0.1e-1), &field.new_dfp_str("0.1e-1").unwrap(), 16, "Parse #3.5");        	
        }


        it "can construct by byte" {
           assert_eq!("0.", Dfp::new_i8(&field, 0 as i8).to_string());
	        assert_eq!("1.", Dfp::new_i8(&field, 1 as i8).to_string());
	        assert_eq!("-1.", Dfp::new_i8(&field, -1 as i8).to_string());
	        assert_eq!("-128.", Dfp::new_i8(&field, i8::MIN).to_string());
	        assert_eq!("127.", Dfp::new_i8(&field, i8::MAX).to_string());
        }
        
        it "can construct by integer" {
        	assert_eq!("0.", Dfp::new_i32(&field, 0).to_string());
	        assert_eq!("1.", Dfp::new_i32(&field, 1).to_string());
	        assert_eq!("-1.", Dfp::new_i32(&field, -1).to_string());
	        assert_eq!("1234567890.", Dfp::new_i32(&field, 1234567890).to_string());
	        assert_eq!("-1234567890.", Dfp::new_i32(&field, -1234567890).to_string());
	        assert_eq!("-2147483648.", Dfp::new_i32(&field, i32::MIN).to_string());
	        assert_eq!("2147483647.", Dfp::new_i32(&field, i32::MAX).to_string());
        }
        it "can construct by long" {
        	 assert_eq!("0.", Dfp::new_i64(&field, 0).to_string());
	        assert_eq!("1.", Dfp::new_i64(&field, 1).to_string());
	        assert_eq!("-1.", Dfp::new_i64(&field, -1).to_string());
	        assert_eq!("1234567890.", Dfp::new_i64(&field, 1234567890).to_string());
	        assert_eq!("-1234567890.", Dfp::new_i64(&field, -1234567890).to_string());
	        assert_eq!("-9223372036854775808.", Dfp::new_i64(&field, i64::MIN).to_string());
	        assert_eq!("9223372036854775807.", Dfp::new_i64(&field, i64::MAX).to_string());
        }
        
        it "can_add" {
        	test(// Basic tests   1+1 = 2
        &field.new_dfp_str("1").unwrap().add(&field.new_dfp_str("1").unwrap()), &field.new_dfp_str("2").unwrap(), 0, "Add #1");
        test(// 1 + (-1) = 0
        &field.new_dfp_str("1").unwrap().add(&field.new_dfp_str("-1").unwrap()), &field.new_dfp_str("0").unwrap(), 0, "Add #2");
        test(// (-1) + 1 = 0
        &field.new_dfp_str("-1").unwrap().add(&field.new_dfp_str("1").unwrap()), &field.new_dfp_str("0").unwrap(), 0, "Add #3");
        test(// (-1) + (-1) = -2
        &field.new_dfp_str("-1").unwrap().add(&field.new_dfp_str("-1").unwrap()), &field.new_dfp_str("-2").unwrap(), 0, "Add #4");
        // rounding mode is round half even
        test(// rounding on add
        &field.new_dfp_str("1").unwrap().add(&field.new_dfp_str("1e-16").unwrap()), &field.new_dfp_str("1.0000000000000001").unwrap(), 0, "Add #5");
        test(// rounding on add
        &field.new_dfp_str("1").unwrap().add(&field.new_dfp_str("1e-17").unwrap()), &field.new_dfp_str("1").unwrap(), FLAG_INEXACT, "Add #6");
        test(// rounding on add
        &field.new_dfp_str("0.90999999999999999999").unwrap().add(&field.new_dfp_str("0.1").unwrap()), &field.new_dfp_str("1.01").unwrap(), FLAG_INEXACT, "Add #7");
        test(// rounding on add
        &field.new_dfp_str(".10000000000000005000").unwrap().add(&field.new_dfp_str(".9").unwrap()), &field.new_dfp_str("1.").unwrap(), FLAG_INEXACT, "Add #8");
        test(// rounding on add
        &field.new_dfp_str(".10000000000000015000").unwrap().add(&field.new_dfp_str(".9").unwrap()), &field.new_dfp_str("1.0000000000000002").unwrap(), FLAG_INEXACT, "Add #9");
        test(// rounding on add
        &field.new_dfp_str(".10000000000000014999").unwrap().add(&field.new_dfp_str(".9").unwrap()), &field.new_dfp_str("1.0000000000000001").unwrap(), FLAG_INEXACT, "Add #10");
        test(// rounding on add
        &field.new_dfp_str(".10000000000000015001").unwrap().add(&field.new_dfp_str(".9").unwrap()), &field.new_dfp_str("1.0000000000000002").unwrap(), FLAG_INEXACT, "Add #11");
        test(// rounding on add
        &field.new_dfp_str(".11111111111111111111").unwrap().add(&field.new_dfp_str("11.1111111111111111").unwrap()), &field.new_dfp_str("11.22222222222222222222").unwrap(), FLAG_INEXACT, "Add #12");
        test(// rounding on add
        &field.new_dfp_str(".11111111111111111111").unwrap().add(&field.new_dfp_str("1111111111111111.1111").unwrap()), &field.new_dfp_str("1111111111111111.2222").unwrap(), FLAG_INEXACT, "Add #13");
        test(// rounding on add
        &field.new_dfp_str(".11111111111111111111").unwrap().add(&field.new_dfp_str("11111111111111111111").unwrap()), &field.new_dfp_str("11111111111111111111").unwrap(), FLAG_INEXACT, "Add #14");
        test(// overflow on add
        &field.new_dfp_str("9.9999999999999999999e131071").unwrap().add(&field.new_dfp_str("-1e131052").unwrap()), &field.new_dfp_str("9.9999999999999999998e131071").unwrap(), 0, "Add #15");
        test(// overflow on add
        &field.new_dfp_str("9.9999999999999999999e131071").unwrap().add(&field.new_dfp_str("1e131052").unwrap()), &pinf, FLAG_OVERFLOW, "Add #16");        
        test(// overflow on add
        &field.new_dfp_str("-9.9999999999999999999e131071").unwrap().add(&field.new_dfp_str("-1e131052").unwrap()), &ninf, FLAG_OVERFLOW, "Add #17");        
        test(// overflow on add
        &field.new_dfp_str("-9.9999999999999999999e131071").unwrap().add(&field.new_dfp_str("1e131052").unwrap()), &field.new_dfp_str("-9.9999999999999999998e131071").unwrap(), 0, "Add #18");
        test(// underflow on add
        &field.new_dfp_str("1e-131072").unwrap().add(&field.new_dfp_str("1e-131072").unwrap()), &field.new_dfp_str("2e-131072").unwrap(), 0, "Add #19");
        test(// underflow on add
        &field.new_dfp_str("1.0000000000000001e-131057").unwrap().add(&field.new_dfp_str("-1e-131057").unwrap()), &field.new_dfp_str("1e-131073").unwrap(), FLAG_UNDERFLOW, "Add #20");
        test(// underflow on add
        &field.new_dfp_str("1.1e-131072").unwrap().add(&field.new_dfp_str("-1e-131072").unwrap()), &field.new_dfp_str("1e-131073").unwrap(), FLAG_UNDERFLOW, "Add #21");
        test(// underflow on add
        &field.new_dfp_str("1.0000000000000001e-131072").unwrap().add(&field.new_dfp_str("-1e-131072").unwrap()), &field.new_dfp_str("1e-131088").unwrap(), FLAG_UNDERFLOW, "Add #22");
        test(// underflow on add
        &field.new_dfp_str("1.0000000000000001e-131078").unwrap().add(&field.new_dfp_str("-1e-131078").unwrap()), &field.new_dfp_str("0").unwrap(), FLAG_UNDERFLOW, "Add #23");
        test(// loss of precision on alignment?
        &field.new_dfp_str("1.0").unwrap().add(&field.new_dfp_str("-1e-20").unwrap()), &field.new_dfp_str("0.99999999999999999999").unwrap(), 0, "Add #23.1");
        test(// proper normalization?
        &field.new_dfp_str("-0.99999999999999999999").unwrap().add(&field.new_dfp_str("1").unwrap()), &field.new_dfp_str("0.00000000000000000001").unwrap(), 0, "Add #23.2");
        test(// adding zeros
        &field.new_dfp_str("1").unwrap().add(&field.new_dfp_str("0").unwrap()), &field.new_dfp_str("1").unwrap(), 0, "Add #24");
        test(// adding zeros
        &field.new_dfp_str("0").unwrap().add(&field.new_dfp_str("0").unwrap()), &field.new_dfp_str("0").unwrap(), 0, "Add #25");
        test(// adding zeros
        &field.new_dfp_str("-0").unwrap().add(&field.new_dfp_str("0").unwrap()), &field.new_dfp_str("0").unwrap(), 0, "Add #26");
        test(// adding zeros
        &field.new_dfp_str("0").unwrap().add(&field.new_dfp_str("-0").unwrap()), &field.new_dfp_str("0").unwrap(), 0, "Add #27");
        test(// adding zeros
        &field.new_dfp_str("-0").unwrap().add(&field.new_dfp_str("-0").unwrap()), &field.new_dfp_str("-0").unwrap(), 0, "Add #28");
        test(// adding zeros
        &field.new_dfp_str("1e-20").unwrap().add(&field.new_dfp_str("0").unwrap()), &field.new_dfp_str("1e-20").unwrap(), 0, "Add #29");
        test(// adding zeros
        &field.new_dfp_str("1e-40").unwrap().add(&field.new_dfp_str("0").unwrap()), &field.new_dfp_str("1e-40").unwrap(), 0, "Add #30");
        test(// adding infinities
        &pinf.add(&ninf), &nan, FLAG_INVALID, "Add #31");
        test(// adding infinities
        &ninf.add(&pinf), &nan, FLAG_INVALID, "Add #32");
        test(// adding infinities
        &ninf.add(&ninf), &ninf, 0, "Add #33");
        test(// adding infinities
        &pinf.add(&pinf), &pinf, 0, "Add #34");
        test(// adding infinities
        &pinf.add(&field.new_dfp_str("0").unwrap()), &pinf, 0, "Add #35");
        test(// adding infinities
        &pinf.add(&field.new_dfp_str("-1e131071").unwrap()), &pinf, 0, "Add #36");
        test(// adding infinities
        &pinf.add(&field.new_dfp_str("1e131071").unwrap()), &pinf, 0, "Add #37");
        test(// adding infinities
        &field.new_dfp_str("0").unwrap().add(&pinf), &pinf, 0, "Add #38");
        test(// adding infinities
        &field.new_dfp_str("-1e131071").unwrap().add(&pinf), &pinf, 0, "Add #39");
        test(// adding infinities
        &field.new_dfp_str("1e131071").unwrap().add(&pinf), &pinf, 0, "Add #40");
        test(// adding infinities
        &ninf.add(&field.new_dfp_str("0").unwrap()), &ninf, 0, "Add #41");
        test(// adding infinities
        &ninf.add(&field.new_dfp_str("-1e131071").unwrap()), &ninf, 0, "Add #42");
        test(// adding infinities
        &ninf.add(&field.new_dfp_str("1e131071").unwrap()), &ninf, 0, "Add #43");
        test(// adding infinities
        &field.new_dfp_str("0").unwrap().add(&ninf), &ninf, 0, "Add #44");
        test(// adding infinities
        &field.new_dfp_str("-1e131071").unwrap().add(&ninf), &ninf, 0, "Add #45");
        test(// adding infinities
        &field.new_dfp_str("1e131071").unwrap().add(&ninf), &ninf, 0, "Add #46");
        test(// overflow
        &field.new_dfp_str("9.9999999999999999999e131071").unwrap().add(&field.new_dfp_str("5e131051").unwrap()), &pinf, FLAG_OVERFLOW, "Add #47");
        test(// overflow
        &field.new_dfp_str("9.9999999999999999999e131071").unwrap().add(&field.new_dfp_str("4.9999999999999999999e131051").unwrap()), &field.new_dfp_str("9.9999999999999999999e131071").unwrap(), FLAG_INEXACT, "Add #48");
        test(&nan.add(&field.new_dfp_str("1").unwrap()), &nan, 0, "Add #49");
        test(&field.new_dfp_str("1").unwrap().add(&nan), &nan, 0, "Add #50");
        test(&field.new_dfp_str("12345678123456781234").unwrap().add(&field.new_dfp_str("0.12345678123456781234").unwrap()), &field.new_dfp_str("12345678123456781234").unwrap(), FLAG_INEXACT, "Add #51");
        test(&field.new_dfp_str("12345678123456781234").unwrap().add(&field.new_dfp_str("123.45678123456781234").unwrap()), &field.new_dfp_str("12345678123456781357").unwrap(), FLAG_INEXACT, "Add #52");
        test(&field.new_dfp_str("123.45678123456781234").unwrap().add(&field.new_dfp_str("12345678123456781234").unwrap()), &field.new_dfp_str("12345678123456781357").unwrap(), FLAG_INEXACT, "Add #53");
        test(&field.new_dfp_str("12345678123456781234").unwrap().add(&field.new_dfp_str(".00001234567812345678").unwrap()), &field.new_dfp_str("12345678123456781234").unwrap(), FLAG_INEXACT, "Add #54");       
        test(&field.new_dfp_str("12345678123456781234").unwrap().add(&field.new_dfp_str(".00000000123456781234").unwrap()), &field.new_dfp_str("12345678123456781234").unwrap(), FLAG_INEXACT, "Add #55");
        test(&field.new_dfp_str("-0").unwrap().add(&field.new_dfp_str("-0").unwrap()), &field.new_dfp_str("-0").unwrap(), 0, "Add #56");
        test(&field.new_dfp_str("0").unwrap().add(&field.new_dfp_str("-0").unwrap()), &field.new_dfp_str("0").unwrap(), 0, "Add #57");
        test(&field.new_dfp_str("-0").unwrap().add(&field.new_dfp_str("0").unwrap()), &field.new_dfp_str("0").unwrap(), 0, "Add #58");
        test(&field.new_dfp_str("0").unwrap().add(&field.new_dfp_str("0").unwrap()), &field.new_dfp_str("0").unwrap(), 0, "Add #59");
        
        }
        
        it "can multiply" {
        	 test(&// Basic tests   1*1 = 1
        field.new_dfp_str("1").unwrap().multiply(&field.new_dfp_str("1").unwrap()), &field.new_dfp_str("1").unwrap(), 0, "Multiply #1");
        test(&// Basic tests   1*1 = 1
        field.new_dfp_str("1").unwrap().multiply_i32(1), &field.new_dfp_str("1").unwrap(), 0, "Multiply #2");
        test(&// Basic tests   -1*1 = -1
        field.new_dfp_str("-1").unwrap().multiply(&field.new_dfp_str("1").unwrap()), &field.new_dfp_str("-1").unwrap(), 0, "Multiply #3");
        test(&// Basic tests   -1*1 = -1
        field.new_dfp_str("-1").unwrap().multiply_i32(1), &field.new_dfp_str("-1").unwrap(), 0, "Multiply #4");
        // basic tests with integers
        test(&field.new_dfp_str("2").unwrap().multiply(&field.new_dfp_str("3").unwrap()), &field.new_dfp_str("6").unwrap(), 0, "Multiply #5");
        test(&field.new_dfp_str("2").unwrap().multiply_i32(3), &field.new_dfp_str("6").unwrap(), 0, "Multiply #6");
        test(&field.new_dfp_str("-2").unwrap().multiply(&field.new_dfp_str("3").unwrap()), &field.new_dfp_str("-6").unwrap(), 0, "Multiply #7");
        test(&field.new_dfp_str("-2").unwrap().multiply_i32(3), &field.new_dfp_str("-6").unwrap(), 0, "Multiply #8");
        test(&field.new_dfp_str("2").unwrap().multiply(&field.new_dfp_str("-3").unwrap()), &field.new_dfp_str("-6").unwrap(), 0, "Multiply #9");
        test(&field.new_dfp_str("-2").unwrap().multiply(&field.new_dfp_str("-3").unwrap()), &field.new_dfp_str("6").unwrap(), 0, "Multiply #10");
        //multiply by zero
        test(&field.new_dfp_str("-2").unwrap().multiply(&field.new_dfp_str("0").unwrap()), &field.new_dfp_str("-0").unwrap(), 0, "Multiply #11");
        test(&field.new_dfp_str("-2").unwrap().multiply_i32(0), &field.new_dfp_str("-0").unwrap(), 0, "Multiply #12");
        test(&field.new_dfp_str("2").unwrap().multiply(&field.new_dfp_str("0").unwrap()), &field.new_dfp_str("0").unwrap(), 0, "Multiply #13");
        test(&field.new_dfp_str("2").unwrap().multiply_i32(0), &field.new_dfp_str("0").unwrap(), 0, "Multiply #14");
        test(&field.new_dfp_str("2").unwrap().multiply(&pinf), &pinf, 0, "Multiply #15");
        test(&field.new_dfp_str("2").unwrap().multiply(&ninf), &ninf, 0, "Multiply #16");
        test(&field.new_dfp_str("-2").unwrap().multiply(&pinf), &ninf, 0, "Multiply #17");
        test(&field.new_dfp_str("-2").unwrap().multiply(&ninf), &pinf, 0, "Multiply #18");
        test(&ninf.multiply(&field.new_dfp_str("-2").unwrap()), &pinf, 0, "Multiply #18.1");
        test(&field.new_dfp_str("5e131071").unwrap().multiply_i32(2), &pinf, FLAG_OVERFLOW, "Multiply #19");
        test(&field.new_dfp_str("5e131071").unwrap().multiply(&field.new_dfp_str("1.999999999999999").unwrap()), &field.new_dfp_str("9.9999999999999950000e131071").unwrap(), 0, "Multiply #20");
        test(&field.new_dfp_str("-5e131071").unwrap().multiply_i32(2), &ninf, FLAG_OVERFLOW, "Multiply #22");
        test(&field.new_dfp_str("-5e131071").unwrap().multiply(&field.new_dfp_str("1.999999999999999").unwrap()), &field.new_dfp_str("-9.9999999999999950000e131071").unwrap(), 0, "Multiply #23");
        test(&field.new_dfp_str("1e-65539").unwrap().multiply(&field.new_dfp_str("1e-65539").unwrap()), &field.new_dfp_str("1e-131078").unwrap(), FLAG_UNDERFLOW, "Multiply #24");
        test(&field.new_dfp_str("1").unwrap().multiply(&nan), &nan, 0, "Multiply #25");
        test(&nan.multiply(&field.new_dfp_str("1").unwrap()), &nan, 0, "Multiply #26");
        test(&nan.multiply(&pinf), &nan, 0, "Multiply #27");
        test(&pinf.multiply(&nan), &nan, 0, "Multiply #27");
        test(&pinf.multiply(&field.new_dfp_str("0").unwrap()), &nan, FLAG_INVALID, "Multiply #28");
        test(&field.new_dfp_str("0").unwrap().multiply(&pinf), &nan, FLAG_INVALID, "Multiply #29");
        test(&pinf.multiply(&pinf), &pinf, 0, "Multiply #30");
        test(&ninf.multiply(&pinf), &ninf, 0, "Multiply #31");
        test(&pinf.multiply(&ninf), &ninf, 0, "Multiply #32");
        test(&ninf.multiply(&ninf), &pinf, 0, "Multiply #33");
        test(&pinf.multiply_i32(1), &pinf, 0, "Multiply #34");
        test(&pinf.multiply_i32(0), &nan, FLAG_INVALID, "Multiply #35");
        test(&nan.multiply_i32(1), &nan, 0, "Multiply #36");
        test(&field.new_dfp_str("1").unwrap().multiply_i32(10000), &field.new_dfp_str("10000").unwrap(), 0, "Multiply #37");
        test(&field.new_dfp_str("2").unwrap().multiply_i32(1000000), &field.new_dfp_str("2000000").unwrap(), 0, "Multiply #38");
        test(&field.new_dfp_str("1").unwrap().multiply_i32(-1), &field.new_dfp_str("-1").unwrap(), 0, "Multiply #39");
        }

    
    
     it "can divide"  {
        test(&// divide by NaN = NaN
        field.new_dfp_str("1").unwrap().divide(&nan).unwrap(), &nan, 0, "Divide #1");
        test(&// NaN / number = NaN
        nan.divide(&field.new_dfp_str("1").unwrap()).unwrap(), &nan, 0, "Divide #2");
        test(&pinf.divide(&field.new_dfp_str("1").unwrap()).unwrap(), &pinf, 0, "Divide #3");
        test(&pinf.divide(&field.new_dfp_str("-1").unwrap()).unwrap(), &ninf, 0, "Divide #4");
        test(&pinf.divide(&pinf).unwrap(), &nan, FLAG_INVALID, "Divide #5");
        test(&ninf.divide(&pinf).unwrap(), &nan, FLAG_INVALID, "Divide #6");
        test(&pinf.divide(&ninf).unwrap(), &nan, FLAG_INVALID, "Divide #7");
        test(&ninf.divide(&ninf).unwrap(), &nan, FLAG_INVALID, "Divide #8");
        test(&field.new_dfp_str("0").unwrap().divide(&field.new_dfp_str("0").unwrap()).unwrap(), &nan, FLAG_DIV_ZERO, "Divide #9");
        test(&field.new_dfp_str("1").unwrap().divide(&field.new_dfp_str("0").unwrap()).unwrap(), &pinf, FLAG_DIV_ZERO, "Divide #10");
        test(&field.new_dfp_str("1").unwrap().divide(&field.new_dfp_str("-0").unwrap()).unwrap(), &ninf, FLAG_DIV_ZERO, "Divide #11");
        test(&field.new_dfp_str("-1").unwrap().divide(&field.new_dfp_str("0").unwrap()).unwrap(), &ninf, FLAG_DIV_ZERO, "Divide #12");
        test(&field.new_dfp_str("-1").unwrap().divide(&field.new_dfp_str("-0").unwrap()).unwrap(), &pinf, FLAG_DIV_ZERO, "Divide #13");
        test(&field.new_dfp_str("1").unwrap().divide(&field.new_dfp_str("3").unwrap()).unwrap(), &field.new_dfp_str("0.33333333333333333333").unwrap(), FLAG_INEXACT, "Divide #14");
        test(&field.new_dfp_str("1").unwrap().divide(&field.new_dfp_str("6").unwrap()).unwrap(), &field.new_dfp_str("0.16666666666666666667").unwrap(), FLAG_INEXACT, "Divide #15");
        test(&field.new_dfp_str("10").unwrap().divide(&field.new_dfp_str("6").unwrap()).unwrap(), &field.new_dfp_str("1.6666666666666667").unwrap(), FLAG_INEXACT, "Divide #16");
        test(&field.new_dfp_str("100").unwrap().divide(&field.new_dfp_str("6").unwrap()).unwrap(), &field.new_dfp_str("16.6666666666666667").unwrap(), FLAG_INEXACT, "Divide #17");
        test(&field.new_dfp_str("1000").unwrap().divide(&field.new_dfp_str("6").unwrap()).unwrap(), &field.new_dfp_str("166.6666666666666667").unwrap(), FLAG_INEXACT, "Divide #18");
        test(&field.new_dfp_str("10000").unwrap().divide(&field.new_dfp_str("6").unwrap()).unwrap(), &field.new_dfp_str("1666.6666666666666667").unwrap(), FLAG_INEXACT, "Divide #19");
        test(&field.new_dfp_str("1").unwrap().divide(&field.new_dfp_str("1").unwrap()).unwrap(), &field.new_dfp_str("1").unwrap(), 0, "Divide #20");
        test(&field.new_dfp_str("1").unwrap().divide(&field.new_dfp_str("-1").unwrap()).unwrap(), &field.new_dfp_str("-1").unwrap(), 0, "Divide #21");
        test(&field.new_dfp_str("-1").unwrap().divide(&field.new_dfp_str("1").unwrap()).unwrap(), &field.new_dfp_str("-1").unwrap(), 0, "Divide #22");
        test(&field.new_dfp_str("-1").unwrap().divide(&field.new_dfp_str("-1").unwrap()).unwrap(), &field.new_dfp_str("1").unwrap(), 0, "Divide #23");
        test(&field.new_dfp_str("1e-65539").unwrap().divide(&field.new_dfp_str("1e65539").unwrap()).unwrap(), &field.new_dfp_str("1e-131078").unwrap(), FLAG_UNDERFLOW, "Divide #24");
        test(&field.new_dfp_str("1e65539").unwrap().divide(&field.new_dfp_str("1e-65539").unwrap()).unwrap(), &pinf, FLAG_OVERFLOW, "Divide #24");
        test(&// test trial-divisor too high
        field.new_dfp_str("2").unwrap().divide(&field.new_dfp_str("1.5").unwrap()).unwrap(), &field.new_dfp_str("1.3333333333333333").unwrap(), FLAG_INEXACT, "Divide #25");
        test(&field.new_dfp_str("2").unwrap().divide(&pinf).unwrap(), &field.new_dfp_str("0").unwrap(), 0, "Divide #26");
        test(&field.new_dfp_str("2").unwrap().divide(&ninf).unwrap(), &field.new_dfp_str("-0").unwrap(), 0, "Divide #27");
        test(&field.new_dfp_str("0").unwrap().divide(&field.new_dfp_str("1").unwrap()).unwrap(), &field.new_dfp_str("0").unwrap(), 0, "Divide #28");
    }

    it "can reciproke"    {
        test(&nan.reciprocal().unwrap(), &nan, 0, "Reciprocal #1");
        test(&field.new_dfp_str("0").unwrap().reciprocal().unwrap(), &pinf, FLAG_DIV_ZERO, "Reciprocal #2");
        test(&field.new_dfp_str("-0").unwrap().reciprocal().unwrap(), &ninf, FLAG_DIV_ZERO, "Reciprocal #3");
        test(&field.new_dfp_str("3").unwrap().reciprocal().unwrap(), &field.new_dfp_str("0.33333333333333333333").unwrap(), FLAG_INEXACT, "Reciprocal #4");
        test(&field.new_dfp_str("6").unwrap().reciprocal().unwrap(), &field.new_dfp_str("0.16666666666666666667").unwrap(), FLAG_INEXACT, "Reciprocal #5");
        test(&field.new_dfp_str("1").unwrap().reciprocal().unwrap(), &field.new_dfp_str("1").unwrap(), 0, "Reciprocal #6");
        test(&field.new_dfp_str("-1").unwrap().reciprocal().unwrap(), &field.new_dfp_str("-1").unwrap(), 0, "Reciprocal #7");
        test(&pinf.reciprocal().unwrap(), &field.new_dfp_str("0").unwrap(), 0, "Reciprocal #8");
        test(&ninf.reciprocal().unwrap(), &field.new_dfp_str("-0").unwrap(), 0, "Reciprocal #9");
    }

     it "can divide int"   {
        test(&// NaN / number = NaN
        nan.divide_i32(1), &nan, 0, "DivideInt #1");
        test(&pinf.divide_i32(1), &pinf, 0, "DivideInt #2");
        test(&field.new_dfp_str("0").unwrap().divide_i32(0), &nan, FLAG_DIV_ZERO, "DivideInt #3");
        test(&field.new_dfp_str("1").unwrap().divide_i32(0), &pinf, FLAG_DIV_ZERO, "DivideInt #4");
        test(&field.new_dfp_str("-1").unwrap().divide_i32(0), &ninf, FLAG_DIV_ZERO, "DivideInt #5");
        test(&field.new_dfp_str("1").unwrap().divide_i32(3), &field.new_dfp_str("0.33333333333333333333").unwrap(), FLAG_INEXACT, "DivideInt #6");
        test(&field.new_dfp_str("1").unwrap().divide_i32(6), &field.new_dfp_str("0.16666666666666666667").unwrap(), FLAG_INEXACT, "DivideInt #7");
        test(&field.new_dfp_str("10").unwrap().divide_i32(6), &field.new_dfp_str("1.6666666666666667").unwrap(), FLAG_INEXACT, "DivideInt #8");
        test(&field.new_dfp_str("100").unwrap().divide_i32(6), &field.new_dfp_str("16.6666666666666667").unwrap(), FLAG_INEXACT, "DivideInt #9");
        test(&field.new_dfp_str("1000").unwrap().divide_i32(6), &field.new_dfp_str("166.6666666666666667").unwrap(), FLAG_INEXACT, "DivideInt #10");
        test(&field.new_dfp_str("10000").unwrap().divide_i32(6), &field.new_dfp_str("1666.6666666666666667").unwrap(), FLAG_INEXACT, "DivideInt #20");
        test(&field.new_dfp_str("1").unwrap().divide_i32(1), &field.new_dfp_str("1").unwrap(), 0, "DivideInt #21");
        test(&field.new_dfp_str("1e-131077").unwrap().divide_i32(10), &field.new_dfp_str("1e-131078").unwrap(), FLAG_UNDERFLOW, "DivideInt #22");
        test(&field.new_dfp_str("0").unwrap().divide_i32(1), &field.new_dfp_str("0").unwrap(), 0, "DivideInt #23");
        test(&field.new_dfp_str("1").unwrap().divide_i32(10000), &nan, FLAG_INVALID, "DivideInt #24");
        test(&field.new_dfp_str("1").unwrap().divide_i32(-1), &nan, FLAG_INVALID, "DivideInt #25");
    }


    it "does next_after"   {
        test(&field.new_dfp_str("1").unwrap().next_after(&pinf), &field.new_dfp_str("1.0000000000000001").unwrap(), 0, "NextAfter #1");
        test(&field.new_dfp_str("1.0000000000000001").unwrap().next_after(&ninf), &field.new_dfp_str("1").unwrap(), 0, "NextAfter #1.5");
        test(&field.new_dfp_str("1").unwrap().next_after(&ninf), &field.new_dfp_str("0.99999999999999999999").unwrap(), 0, "NextAfter #2");
        test(&field.new_dfp_str("0.99999999999999999999").unwrap().next_after(&field.new_dfp_str("2").unwrap()), &field.new_dfp_str("1").unwrap(), 0, "NextAfter #3");
        test(&field.new_dfp_str("-1").unwrap().next_after(&ninf), &field.new_dfp_str("-1.0000000000000001").unwrap(), 0, "NextAfter #4");
        test(&field.new_dfp_str("-1").unwrap().next_after(&pinf), &field.new_dfp_str("-0.99999999999999999999").unwrap(), 0, "NextAfter #5");
        test(&field.new_dfp_str("-0.99999999999999999999").unwrap().next_after(&field.new_dfp_str("-2").unwrap()), &field.new_dfp_str("-1").unwrap(), 0, "NextAfter #6");
        test(&field.new_dfp_str("2").unwrap().next_after(&field.new_dfp_str("2").unwrap()), &field.new_dfp_str("2").unwrap(), 0, "NextAfter #7");
        test(&field.new_dfp_str("0").unwrap().next_after(&field.new_dfp_str("0").unwrap()), &field.new_dfp_str("0").unwrap(), 0, "NextAfter #8");
        test(&field.new_dfp_str("-2").unwrap().next_after(&field.new_dfp_str("-2").unwrap()), &field.new_dfp_str("-2").unwrap(), 0, "NextAfter #9");
        test(&field.new_dfp_str("0").unwrap().next_after(&field.new_dfp_str("1").unwrap()), &field.new_dfp_str("1e-131092").unwrap(), FLAG_UNDERFLOW, "NextAfter #10");
        test(&field.new_dfp_str("0").unwrap().next_after(&field.new_dfp_str("-1").unwrap()), &field.new_dfp_str("-1e-131092").unwrap(), FLAG_UNDERFLOW, "NextAfter #11");                
        test(&field.new_dfp_str("-1e-131092").unwrap().next_after(&pinf), &field.new_dfp_str("-0").unwrap(), FLAG_UNDERFLOW | FLAG_INEXACT, "Next After #12");
        test(&field.new_dfp_str("1e-131092").unwrap().next_after(&ninf), &field.new_dfp_str("0").unwrap(), FLAG_UNDERFLOW | FLAG_INEXACT, "Next After #13");
        test(&field.new_dfp_str("9.9999999999999999999e131078").unwrap().next_after(&pinf), &pinf, FLAG_OVERFLOW | FLAG_INEXACT, "Next After #14");
        
    }

    it "does to_string"   {
        assert!( "Infinity" ==  &pinf.to_string(), "toString #1");
        assert!( "-Infinity" ==  &ninf.to_string(), "toString #2");
        assert!( "NaN" ==  &nan.to_string(), "toString #3");
        assert!( "NaN" ==  &field.new_dfp_sign_nans(1 as i8, QNAN).to_string(), "toString #4");
        assert!( "NaN" ==  &field.new_dfp_sign_nans(1 as i8, SNAN).to_string(), "toString #5");
        assert!( "1.2300000000000000e100" ==  &field.new_dfp_str("1.23e100").unwrap().to_string(), "toString #6");
        assert!( "-1.2300000000000000e100" ==  &field.new_dfp_str("-1.23e100").unwrap().to_string(), "toString #7");
        assert!( "12345678.1234" ==  &field.new_dfp_str("12345678.1234").unwrap().to_string(), "toString #8");
        assert!( "0.00001234" ==  &field.new_dfp_str("0.00001234").unwrap().to_string(), "toString #9");
        
    }

    it "does round"   {
        field.set_rounding_mode(RoundingMode::ROUND_DOWN);
        // Round down
        test(&field.new_dfp_str("12345678901234567890").unwrap().add(&field.new_dfp_str("0.9").unwrap()), &field.new_dfp_str("12345678901234567890").unwrap(), FLAG_INEXACT, "Round #1");
        test(&field.new_dfp_str("12345678901234567890").unwrap().add(&field.new_dfp_str("0.99999999").unwrap()), &field.new_dfp_str("12345678901234567890").unwrap(), FLAG_INEXACT, "Round #2");
        test(&field.new_dfp_str("-12345678901234567890").unwrap().add(&field.new_dfp_str("-0.99999999").unwrap()), &field.new_dfp_str("-12345678901234567890").unwrap(), FLAG_INEXACT, "Round #3");
        field.set_rounding_mode(RoundingMode::ROUND_UP);
        // Round up
        test(&field.new_dfp_str("12345678901234567890").unwrap().add(&field.new_dfp_str("0.1").unwrap()), &field.new_dfp_str("12345678901234567891").unwrap(), FLAG_INEXACT, "Round #4");
        test(&field.new_dfp_str("12345678901234567890").unwrap().add(&field.new_dfp_str("0.0001").unwrap()), &field.new_dfp_str("12345678901234567891").unwrap(), FLAG_INEXACT, "Round #5");
        test(&field.new_dfp_str("-12345678901234567890").unwrap().add(&field.new_dfp_str("-0.1").unwrap()), &field.new_dfp_str("-12345678901234567891").unwrap(), FLAG_INEXACT, "Round #6");
        test(&field.new_dfp_str("-12345678901234567890").unwrap().add(&field.new_dfp_str("-0.0001").unwrap()), &field.new_dfp_str("-12345678901234567891").unwrap(), FLAG_INEXACT, "Round #7");
        field.set_rounding_mode(RoundingMode::ROUND_HALF_UP);
        // Round half up
        test(&field.new_dfp_str("12345678901234567890").unwrap().add(&field.new_dfp_str("0.4999").unwrap()), &field.new_dfp_str("12345678901234567890").unwrap(), FLAG_INEXACT, "Round #8");
        test(&field.new_dfp_str("12345678901234567890").unwrap().add(&field.new_dfp_str("0.5000").unwrap()), &field.new_dfp_str("12345678901234567891").unwrap(), FLAG_INEXACT, "Round #9");
        test(&field.new_dfp_str("-12345678901234567890").unwrap().add(&field.new_dfp_str("-0.4999").unwrap()), &field.new_dfp_str("-12345678901234567890").unwrap(), FLAG_INEXACT, "Round #10");
        test(&field.new_dfp_str("-12345678901234567890").unwrap().add(&field.new_dfp_str("-0.5000").unwrap()), &field.new_dfp_str("-12345678901234567891").unwrap(), FLAG_INEXACT, "Round #11");
        field.set_rounding_mode(RoundingMode::ROUND_HALF_DOWN);
        // Round half down
        test(&field.new_dfp_str("12345678901234567890").unwrap().add(&field.new_dfp_str("0.5001").unwrap()), &field.new_dfp_str("12345678901234567891").unwrap(), FLAG_INEXACT, "Round #12");
        test(&field.new_dfp_str("12345678901234567890").unwrap().add(&field.new_dfp_str("0.5000").unwrap()), &field.new_dfp_str("12345678901234567890").unwrap(), FLAG_INEXACT, "Round #13");
        test(&field.new_dfp_str("-12345678901234567890").unwrap().add(&field.new_dfp_str("-0.5001").unwrap()), &field.new_dfp_str("-12345678901234567891").unwrap(), FLAG_INEXACT, "Round #14");
        test(&field.new_dfp_str("-12345678901234567890").unwrap().add(&field.new_dfp_str("-0.5000").unwrap()), &field.new_dfp_str("-12345678901234567890").unwrap(), FLAG_INEXACT, "Round #15");
        field.set_rounding_mode(RoundingMode::ROUND_HALF_ODD);
        // Round half odd
        test(&field.new_dfp_str("12345678901234567890").unwrap().add(&field.new_dfp_str("0.5000").unwrap()), &field.new_dfp_str("12345678901234567891").unwrap(), FLAG_INEXACT, "Round #16");
        test(&field.new_dfp_str("12345678901234567891").unwrap().add(&field.new_dfp_str("0.5000").unwrap()), &field.new_dfp_str("12345678901234567891").unwrap(), FLAG_INEXACT, "Round #17");
        test(&field.new_dfp_str("-12345678901234567890").unwrap().add(&field.new_dfp_str("-0.5000").unwrap()), &field.new_dfp_str("-12345678901234567891").unwrap(), FLAG_INEXACT, "Round #18");
        test(&field.new_dfp_str("-12345678901234567891").unwrap().add(&field.new_dfp_str("-0.5000").unwrap()), &field.new_dfp_str("-12345678901234567891").unwrap(), FLAG_INEXACT, "Round #19");
        field.set_rounding_mode(RoundingMode::ROUND_CEIL);
        // Round ceil
        test(&field.new_dfp_str("12345678901234567890").unwrap().add(&field.new_dfp_str("0.0001").unwrap()), &field.new_dfp_str("12345678901234567891").unwrap(), FLAG_INEXACT, "Round #20");
        test(&field.new_dfp_str("-12345678901234567890").unwrap().add(&field.new_dfp_str("-0.9999").unwrap()), &field.new_dfp_str("-12345678901234567890").unwrap(), FLAG_INEXACT, "Round #21");
        field.set_rounding_mode(RoundingMode::ROUND_FLOOR);
        // Round floor
        test(&field.new_dfp_str("12345678901234567890").unwrap().add(&field.new_dfp_str("0.9999").unwrap()), &field.new_dfp_str("12345678901234567890").unwrap(), FLAG_INEXACT, "Round #22");
        test(&field.new_dfp_str("-12345678901234567890").unwrap().add(&field.new_dfp_str("-0.0001").unwrap()), &field.new_dfp_str("-12345678901234567891").unwrap(), FLAG_INEXACT, "Round #23");
        // reset
        field.set_rounding_mode(RoundingMode::ROUND_HALF_EVEN);
    }

    it "does ceil"   {
        test(&field.new_dfp_str("1234.0000000000000001").unwrap().ceil(), &field.new_dfp_str("1235").unwrap(), FLAG_INEXACT, "Ceil #1");
    }

    it "does floor"  {
        test(&field.new_dfp_str("1234.9999999999999999").unwrap().floor(), &field.new_dfp_str("1234").unwrap(), FLAG_INEXACT, "Floor #1");
    }

    it "does rint"   {
        test(&field.new_dfp_str("1234.50000000001").unwrap().rint(), &field.new_dfp_str("1235").unwrap(), FLAG_INEXACT, "Rint #1");
        test(&field.new_dfp_str("1234.5000").unwrap().rint(), &field.new_dfp_str("1234").unwrap(), FLAG_INEXACT, "Rint #2");
        test(&field.new_dfp_str("1235.5000").unwrap().rint(), &field.new_dfp_str("1236").unwrap(), FLAG_INEXACT, "Rint #3");
    }

    it "does copy sign"  {
        test(&Dfp::copysign(&field.new_dfp_str("1234.").unwrap(), &field.new_dfp_str("-1").unwrap()), &field.new_dfp_str("-1234").unwrap(), 0, "CopySign #1");
        test(&Dfp::copysign(&field.new_dfp_str("-1234.").unwrap(), &field.new_dfp_str("-1").unwrap()), &field.new_dfp_str("-1234").unwrap(), 0, "CopySign #2");
        test(&Dfp::copysign(&field.new_dfp_str("-1234.").unwrap(), &field.new_dfp_str("1").unwrap()), &field.new_dfp_str("1234").unwrap(), 0, "CopySign #3");
        test(&Dfp::copysign(&field.new_dfp_str("1234.").unwrap(), &field.new_dfp_str("1").unwrap()), &field.new_dfp_str("1234").unwrap(), 0, "CopySign #4");
    }

    it "does int value"   {
        assert!( 1234 ==  field.new_dfp_str("1234").unwrap().int_value(), "intValue #1");
        assert!( -1234 ==  field.new_dfp_str("-1234").unwrap().int_value(), "intValue #2");
        assert!( 1234 ==  field.new_dfp_str("1234.5").unwrap().int_value(), "intValue #3");
        assert!( 1235 ==  field.new_dfp_str("1234.500001").unwrap().int_value(), "intValue #4");
        assert!( 2147483647 ==  field.new_dfp_str("1e1000").unwrap().int_value(), "intValue #5");
        assert!( -2147483648 ==  field.new_dfp_str("-1e1000").unwrap().int_value(), "intValue #6");
    }

    it "does log10k"  {
        assert!( 1 ==  field.new_dfp_str("123456").unwrap().log10_k(), "log10K #1");
        assert!( 2 ==  field.new_dfp_str("123456789").unwrap().log10_k(), "log10K #2");
        assert!( 0 ==  field.new_dfp_str("2").unwrap().log10_k(), "log10K #3");
        assert!( 0 ==  field.new_dfp_str("1").unwrap().log10_k(), "log10K #3");
        assert!( -1 ==  field.new_dfp_str("0.1").unwrap().log10_k(), "log10K #4");
    }

    it "does power10_k"   {
         let d: Dfp = field.new_dfp();
        test(&d.power10_k(0), &field.new_dfp_str("1").unwrap(), 0, "Power10 #1");
        test(&d.power10_k(1), &field.new_dfp_str("10000").unwrap(), 0, "Power10 #2");
        test(&d.power10_k(2), &field.new_dfp_str("100000000").unwrap(), 0, "Power10 #3");
        test(&d.power10_k(-1), &field.new_dfp_str("0.0001").unwrap(), 0, "Power10 #4");
        test(&d.power10_k(-2), &field.new_dfp_str("0.00000001").unwrap(), 0, "Power10 #5");
        test(&d.power10_k(-3), &field.new_dfp_str("0.000000000001").unwrap(), 0, "Power10 #6");
    }

    it "does log10"   {
        assert!( 1 ==  field.new_dfp_str("12").unwrap().int_log10(), "log10 #1");
        assert!( 2 ==  field.new_dfp_str("123").unwrap().int_log10(), "log10 #2");
        assert!( 3 ==  field.new_dfp_str("1234").unwrap().int_log10(), "log10 #3");
        assert!( 4 ==  field.new_dfp_str("12345").unwrap().int_log10(), "log10 #4");
        assert!( 5 ==  field.new_dfp_str("123456").unwrap().int_log10(), "log10 #5");
        assert!( 6 ==  field.new_dfp_str("1234567").unwrap().int_log10(), "log10 #6");
        assert!( 7 ==  field.new_dfp_str("12345678").unwrap().int_log10(), "log10 #6");
        assert!( 8 ==  field.new_dfp_str("123456789").unwrap().int_log10(), "log10 #7");
        assert!( 9 ==  field.new_dfp_str("1234567890").unwrap().int_log10(), "log10 #8");
        assert!( 10 ==  field.new_dfp_str("12345678901").unwrap().int_log10(), "log10 #9");
        assert!( 11 ==  field.new_dfp_str("123456789012").unwrap().int_log10(), "log10 #10");
        assert!( 12 ==  field.new_dfp_str("1234567890123").unwrap().int_log10(), "log10 #11");
        assert!( 0 ==  field.new_dfp_str("2").unwrap().int_log10(), "log10 #12");
        assert!( 0 ==  field.new_dfp_str("1").unwrap().int_log10(), "log10 #13");
        assert!( -1 ==  field.new_dfp_str("0.12").unwrap().int_log10(), "log10 #14");
        assert!( -2 ==  field.new_dfp_str("0.012").unwrap().int_log10(), "log10 #15");
    }

    it "does power10"  {
         let d: Dfp = field.new_dfp();
        test(&d.power10(0), &field.new_dfp_str("1").unwrap(), 0, "Power10 #1");
        test(&d.power10(1), &field.new_dfp_str("10").unwrap(), 0, "Power10 #2");
        test(&d.power10(2), &field.new_dfp_str("100").unwrap(), 0, "Power10 #3");
        test(&d.power10(3), &field.new_dfp_str("1000").unwrap(), 0, "Power10 #4");
        test(&d.power10(4), &field.new_dfp_str("10000").unwrap(), 0, "Power10 #5");
        test(&d.power10(5), &field.new_dfp_str("100000").unwrap(), 0, "Power10 #6");
        test(&d.power10(6), &field.new_dfp_str("1000000").unwrap(), 0, "Power10 #7");
        test(&d.power10(7), &field.new_dfp_str("10000000").unwrap(), 0, "Power10 #8");
        test(&d.power10(8), &field.new_dfp_str("100000000").unwrap(), 0, "Power10 #9");
        test(&d.power10(9), &field.new_dfp_str("1000000000").unwrap(), 0, "Power10 #10");
        test(&d.power10(-1), &field.new_dfp_str(".1").unwrap(), 0, "Power10 #11");
        test(&d.power10(-2), &field.new_dfp_str(".01").unwrap(), 0, "Power10 #12");
        test(&d.power10(-3), &field.new_dfp_str(".001").unwrap(), 0, "Power10 #13");
        test(&d.power10(-4), &field.new_dfp_str(".0001").unwrap(), 0, "Power10 #14");
        test(&d.power10(-5), &field.new_dfp_str(".00001").unwrap(), 0, "Power10 #15");
        test(&d.power10(-6), &field.new_dfp_str(".000001").unwrap(), 0, "Power10 #16");
        test(&d.power10(-7), &field.new_dfp_str(".0000001").unwrap(), 0, "Power10 #17");
        test(&d.power10(-8), &field.new_dfp_str(".00000001").unwrap(), 0, "Power10 #18");
        test(&d.power10(-9), &field.new_dfp_str(".000000001").unwrap(), 0, "Power10 #19");
        test(&d.power10(-10), &field.new_dfp_str(".0000000001").unwrap(), 0, "Power10 #20");
    }

    it "does remainder"  {
        test(&field.new_dfp_str("10").unwrap().remainder(&field.new_dfp_str("3").unwrap()), &field.new_dfp_str("1").unwrap(), FLAG_INEXACT, "Remainder #1");
        test(&field.new_dfp_str("9").unwrap().remainder(&field.new_dfp_str("3").unwrap()), &field.new_dfp_str("0").unwrap(), 0, "Remainder #2");
        test(&field.new_dfp_str("-9").unwrap().remainder(&field.new_dfp_str("3").unwrap()), &field.new_dfp_str("-0").unwrap(), 0, "Remainder #3");
    }

    it "does sqrt"  {
        test(&field.new_dfp_str("0").unwrap().sqrt(), &field.new_dfp_str("0").unwrap(), 0, "Sqrt #1");
        test(&field.new_dfp_str("-0").unwrap().sqrt(), &field.new_dfp_str("-0").unwrap(), 0, "Sqrt #2");
        test(&field.new_dfp_str("1").unwrap().sqrt(), &field.new_dfp_str("1").unwrap(), 0, "Sqrt #3");
        test(&field.new_dfp_str("2").unwrap().sqrt(), &field.new_dfp_str("1.4142135623730950").unwrap(), FLAG_INEXACT, "Sqrt #4");
        test(&field.new_dfp_str("3").unwrap().sqrt(), &field.new_dfp_str("1.7320508075688773").unwrap(), FLAG_INEXACT, "Sqrt #5");
        test(&field.new_dfp_str("5").unwrap().sqrt(), &field.new_dfp_str("2.2360679774997897").unwrap(), FLAG_INEXACT, "Sqrt #6");
        test(&field.new_dfp_str("500").unwrap().sqrt(), &field.new_dfp_str("22.3606797749978970").unwrap(), FLAG_INEXACT, "Sqrt #6.2");
        test(&field.new_dfp_str("50000").unwrap().sqrt(), &field.new_dfp_str("223.6067977499789696").unwrap(), FLAG_INEXACT, "Sqrt #6.3");
        test(&field.new_dfp_str("-1").unwrap().sqrt(), &nan, FLAG_INVALID, "Sqrt #7");
        test(&pinf.sqrt(), &pinf, 0, "Sqrt #8");
        test(&field.new_dfp_sign_nans(1 as i8, QNAN).sqrt(), &nan, 0, "Sqrt #9");
        test(&field.new_dfp_sign_nans(1 as i8, SNAN).sqrt(), &nan, FLAG_INVALID, "Sqrt #10");
    }

    it "does issue567"  {
         let field: DfpField = DfpField::new(100);
        assert_that(0.0, is(close_to(field.get_zero().to_double(), *precision::SAFE_MIN)));
        assert_that(0.0, is(close_to(field.new_dfp_f64(0.0).to_double(), *precision::SAFE_MIN)));
        
        assert_that(-1.0, is(close_to(fastmath::F64::copy_sign(1.0, field.new_dfp_f64(-0.0).to_double()), *precision::EPSILON)));
        assert_that(1.0, is(close_to(fastmath::F64::copy_sign(1.0, field.new_dfp_f64(0.0).to_double()), *precision::EPSILON)));
        
    }


    it "does is zero"   {
        assert!(field.get_zero().is_zero());
        assert!(field.get_zero().negate().is_zero());
        assert!(field.new_dfp_f64(0.0).is_zero());
        assert!(field.new_dfp_f64(-0.0).is_zero());
        assert!(!field.new_dfp_f64(1.0e-90).is_zero());
        assert!(!nan.is_zero());
        assert!(!nan.negate().is_zero());
        assert!(!pinf.is_zero());
        assert!(!pinf.negate().is_zero());
        assert!(!ninf.is_zero());
        assert!(!ninf.negate().is_zero());
    }

     it "does sign predicates"   {
        assert!(field.get_zero().negative_or_null());
        assert!(field.get_zero().positive_or_null());
        assert!(!field.get_zero().strictly_negative());
        assert!(!field.get_zero().strictly_positive());
        assert!(field.get_zero().negate().negative_or_null());
        assert!(field.get_zero().negate().positive_or_null());
        assert!(!field.get_zero().negate().strictly_negative());
        assert!(!field.get_zero().negate().strictly_positive());
        assert!(!field.get_one().negative_or_null());
        assert!(field.get_one().positive_or_null());
        assert!(!field.get_one().strictly_negative());
        assert!(field.get_one().strictly_positive());
        assert!(field.get_one().negate().negative_or_null());
        assert!(!field.get_one().negate().positive_or_null());
        assert!(field.get_one().negate().strictly_negative());
        assert!(!field.get_one().negate().strictly_positive());
        assert!(!nan.negative_or_null());
        assert!(!nan.positive_or_null());
        assert!(!nan.strictly_negative());
        assert!(!nan.strictly_positive());
        assert!(!nan.negate().negative_or_null());
        assert!(!nan.negate().positive_or_null());
        assert!(!nan.negate().strictly_negative());
        assert!(!nan.negate().strictly_positive());
        assert!(!pinf.negative_or_null());
        assert!(pinf.positive_or_null());
        assert!(!pinf.strictly_negative());
        assert!(pinf.strictly_positive());
        assert!(pinf.negate().negative_or_null());
        assert!(!pinf.negate().positive_or_null());
        assert!(pinf.negate().strictly_negative());
        assert!(!pinf.negate().strictly_positive());
        assert!(ninf.negative_or_null());
        assert!(!ninf.positive_or_null());
        assert!(ninf.strictly_negative());
        assert!(!ninf.strictly_positive());
        assert!(!ninf.negate().negative_or_null());
        assert!(ninf.negate().positive_or_null());
        assert!(!ninf.negate().strictly_negative());
        assert!(ninf.negate().strictly_positive());
    }
               

    it "does special constructors"  {
        assert_eq!(ninf, field.new_dfp_f64(f64::NEG_INFINITY));
        assert_eq!(ninf, field.new_dfp_str("-Infinity").unwrap());
        assert_eq!(pinf, field.new_dfp_f64(f64::INFINITY));
        assert_eq!(pinf, field.new_dfp_str("Infinity").unwrap());
        assert!(&field.new_dfp_f64(f64::NAN).is_na_n());
        assert!(&field.new_dfp_str("NaN").unwrap().is_na_n());
    }

    it "follows equals hashcode contract"  {
         let var1: DfpField = DfpField::new(1);
         let var6: Dfp = var1.new_dfp_f64(-0.0);
         let var5: Dfp = var1.new_dfp_f64(0.0);
        // Checks the contract:  equals-hashcode on var5 and var6
        assert!( if var5 == var6 { var5.hash_code() == var6.hash_code() } else { true });
    }
    

    }

}
