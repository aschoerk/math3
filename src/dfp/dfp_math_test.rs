#[allow(unused_variables)]

#[cfg(test)]
mod tests {
    pub use util::fastmath;
    pub use util::precision;
    pub use dfp::dfp_field::*;
    pub use dfp::dfp_math::*;
    pub use dfp::dfp::*;
    pub use std::f64;
    pub use std::i64;
    pub use std::i32;
    pub use hamcrest::*;
    pub use std::i8;
 
 // Generic test function.  Takes params x and y and tests them for
    // equality.  Then checks the status flags against the flags argument.
    // If the test fail, it prints the desc string
    pub fn  test(x: &Dfp,  y: &Dfp,  flags: i32,  desc: &str)   {
         let mut b: bool = x == y;
        if // NaNs involved
        !(*x == *y) && !(*x != *y) {
            b = x.to_string() == y.to_string();
        }

        if // distinguish +/- zero
        *x == x.get_zero() {
            b = b && (x.to_string() == y.to_string());
        }

        b = b && x.get_field().get_i_e_e_e_flags() == flags;
        if !b {
        	let msg = format!("assertion failed {} x = {} flags = {} y: {} expected flags: {}",desc,x.to_string(),x.get_field().get_i_e_e_e_flags(),y.to_string(), flags);
            assert!(b, msg);
        }
        x.get_field().clear_i_e_e_e_flags();
    }
    
    describe! stainless {
    	before_each {
    		// Some basic setup.  Define some constants and clear the status flags
	        let factory = get_dfp_field_for(&DfpField::new(20));
	        let pinf = &factory.new_dfp_str("1").unwrap().divide(&&factory.new_dfp_str("0").unwrap()).unwrap();
	        let ninf = &factory.new_dfp_str("-1").unwrap().divide(&&factory.new_dfp_str("0").unwrap()).unwrap();
	        let nan = &factory.new_dfp_str("0").unwrap().divide(&&factory.new_dfp_str("0").unwrap()).unwrap();
	        let snan = &factory.new_dfp_sign_nans(1 as i8, SNAN);
	        let qnan = &factory.new_dfp_sign_nans(1 as i8, QNAN);
	        factory.clear_i_e_e_e_flags();
	        // force loading of dfpmath
	        let pi: Dfp = factory.get_pi();
	        factory.clear_i_e_e_e_flags();
    	}
    	
    	after_each {
    		factory.clear_i_e_e_e_flags();
    	}    	
    	
    	it "can pow" {
        // Test special cases  exponent of zero
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("0").unwrap(), &factory.new_dfp_str("0").unwrap()), &factory.new_dfp_str("1").unwrap(), 0, "pow #1");
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("0").unwrap(), &factory.new_dfp_str("-0").unwrap()), &factory.new_dfp_str("1").unwrap(), 0, "pow #2");
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("2").unwrap(), &factory.new_dfp_str("0").unwrap()), &factory.new_dfp_str("1").unwrap(), 0, "pow #3");
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("-2").unwrap(), &factory.new_dfp_str("-0").unwrap()), &factory.new_dfp_str("1").unwrap(), 0, "pow #4");
        test(&DfpMath::pow_dfp(pinf, &factory.new_dfp_str("-0").unwrap()), &factory.new_dfp_str("1").unwrap(), 0, "pow #5");
        test(&DfpMath::pow_dfp(pinf, &factory.new_dfp_str("0").unwrap()), &factory.new_dfp_str("1").unwrap(), 0, "pow #6");
        test(&DfpMath::pow_dfp(ninf, &factory.new_dfp_str("-0").unwrap()), &factory.new_dfp_str("1").unwrap(), 0, "pow #7");
        test(&DfpMath::pow_dfp(ninf, &factory.new_dfp_str("0").unwrap()), &factory.new_dfp_str("1").unwrap(), 0, "pow #8");
        test(&DfpMath::pow_dfp(qnan, &factory.new_dfp_str("0").unwrap()), &factory.new_dfp_str("1").unwrap(), 0, "pow #8");
        // exponent of one
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("0").unwrap(), &factory.new_dfp_str("1").unwrap()), &factory.new_dfp_str("0").unwrap(), 0, "pow #9");
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("-0").unwrap(), &factory.new_dfp_str("1").unwrap()), &factory.new_dfp_str("-0").unwrap(), 0, "pow #10");
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("2").unwrap(), &factory.new_dfp_str("1").unwrap()), &factory.new_dfp_str("2").unwrap(), 0, "pow #11");
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("-2").unwrap(), &factory.new_dfp_str("1").unwrap()), &factory.new_dfp_str("-2").unwrap(), 0, "pow #12");
        test(&DfpMath::pow_dfp(pinf, &factory.new_dfp_str("1").unwrap()), pinf, 0, "pow #13");
        test(&DfpMath::pow_dfp(ninf, &factory.new_dfp_str("1").unwrap()), ninf, 0, "pow #14");
        test(&DfpMath::pow_dfp(qnan, &factory.new_dfp_str("1").unwrap()), qnan, FLAG_INVALID, "pow #14.1");
        // exponent of NaN
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("0").unwrap(), qnan), qnan, FLAG_INVALID, "pow #15");
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("-0").unwrap(), qnan), qnan, FLAG_INVALID, "pow #16");
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("2").unwrap(), qnan), qnan, FLAG_INVALID, "pow #17");
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("-2").unwrap(), qnan), qnan, FLAG_INVALID, "pow #18");
        test(&DfpMath::pow_dfp(pinf, qnan), qnan, FLAG_INVALID, "pow #19");
        test(&DfpMath::pow_dfp(ninf, qnan), qnan, FLAG_INVALID, "pow #20");
        test(&DfpMath::pow_dfp(qnan, qnan), qnan, FLAG_INVALID, "pow #21");
        // radix of NaN
        test(&DfpMath::pow_dfp(qnan, &factory.new_dfp_str("1").unwrap()), qnan, FLAG_INVALID, "pow #22");
        test(&DfpMath::pow_dfp(qnan, &factory.new_dfp_str("-1").unwrap()), qnan, FLAG_INVALID, "pow #23");
        test(&DfpMath::pow_dfp(qnan, pinf), qnan, FLAG_INVALID, "pow #24");
        test(&DfpMath::pow_dfp(qnan, ninf), qnan, FLAG_INVALID, "pow #25");
        test(&DfpMath::pow_dfp(qnan, qnan), qnan, FLAG_INVALID, "pow #26");
        // (x > 1) ^ pinf = pinf,    (x < -1) ^ pinf = pinf
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("2").unwrap(), pinf), pinf, 0, "pow #27");
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("-2").unwrap(), pinf), pinf, 0, "pow #28");
        test(&DfpMath::pow_dfp(pinf, pinf), pinf, 0, "pow #29");
        test(&DfpMath::pow_dfp(ninf, pinf), pinf, 0, "pow #30");
        // (x > 1) ^ ninf = +0,    (x < -1) ^ ninf = +0
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("2").unwrap(), ninf), &factory.get_zero(), 0, "pow #31");
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("-2").unwrap(), ninf), &factory.get_zero(), 0, "pow #32");
        test(&DfpMath::pow_dfp(pinf, ninf), &factory.get_zero(), 0, "pow #33");
        test(&DfpMath::pow_dfp(ninf, ninf), &factory.get_zero(), 0, "pow #34");
        // (-1 < x < 1) ^ pinf = 0
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("0.5").unwrap(), pinf), &factory.get_zero(), 0, "pow #35");
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("-0.5").unwrap(), pinf), &factory.get_zero(), 0, "pow #36");
        // (-1 < x < 1) ^ ninf = pinf
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("0.5").unwrap(), ninf), pinf, 0, "pow #37");
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("-0.5").unwrap(), ninf), pinf, 0, "pow #38");
        // +/- 1  ^ +/-inf  = NaN
        test(&DfpMath::pow_dfp(&factory.get_one(), pinf), qnan, FLAG_INVALID, "pow #39");
        test(&DfpMath::pow_dfp(&factory.get_one(), ninf), qnan, FLAG_INVALID, "pow #40");
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("-1").unwrap(), pinf), qnan, FLAG_INVALID, "pow #41");
        test(&DfpMath::pow_dfp(&factory.get_one().negate(), ninf), qnan, FLAG_INVALID, "pow #42");
        // +0  ^ +anything except 0, NAN  = +0
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("0").unwrap(), &factory.new_dfp_str("1").unwrap()), &factory.new_dfp_str("0").unwrap(), 0, "pow #43");
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("0").unwrap(), &factory.new_dfp_str("1e30").unwrap()), &factory.new_dfp_str("0").unwrap(), 0, "pow #44");
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("0").unwrap(), &factory.new_dfp_str("1e-30").unwrap()), &factory.new_dfp_str("0").unwrap(), 0, "pow #45");
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("0").unwrap(), pinf), &factory.new_dfp_str("0").unwrap(), 0, "pow #46");
        // -0  ^ +anything except 0, NAN, odd integer  = +0
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("-0").unwrap(), &factory.new_dfp_str("2").unwrap()), &factory.new_dfp_str("0").unwrap(), 0, "pow #47");
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("-0").unwrap(), &factory.new_dfp_str("1e30").unwrap()), &factory.new_dfp_str("0").unwrap(), 0, "pow #48");
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("-0").unwrap(), &factory.new_dfp_str("1e-30").unwrap()), &factory.new_dfp_str("0").unwrap(), FLAG_INEXACT, "pow #49");
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("-0").unwrap(), pinf), &factory.new_dfp_str("0").unwrap(), 0, "pow #50");
        // +0  ^ -anything except 0, NAN  = +INF
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("0").unwrap(), &factory.new_dfp_str("-1").unwrap()), pinf, 0, "pow #51");
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("0").unwrap(), &factory.new_dfp_str("-1e30").unwrap()), pinf, 0, "pow #52");
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("0").unwrap(), &factory.new_dfp_str("-1e-30").unwrap()), pinf, 0, "pow #53");
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("0").unwrap(), ninf), pinf, 0, "pow #54");
        // -0  ^ -anything except 0, NAN, odd integer  = +INF
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("-0").unwrap(), &factory.new_dfp_str("-2").unwrap()), pinf, 0, "pow #55");
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("-0").unwrap(), &factory.new_dfp_str("-1e30").unwrap()), pinf, 0, "pow #56");
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("-0").unwrap(), &factory.new_dfp_str("-1e-30").unwrap()), pinf, FLAG_INEXACT, "pow #57");
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("-0").unwrap(), ninf), pinf, 0, "pow #58");
        // -0  ^ -odd integer   =  -INF
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("-0").unwrap(), &factory.new_dfp_str("-1").unwrap()), ninf, FLAG_INEXACT, "pow #59");
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("-0").unwrap(), &factory.new_dfp_str("-12345").unwrap()), ninf, FLAG_INEXACT, "pow #60");
        // -0  ^ +odd integer   =  -0
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("-0").unwrap(), &factory.new_dfp_str("3").unwrap()), &factory.new_dfp_str("-0").unwrap(), FLAG_INEXACT, "pow #61");
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("-0").unwrap(), &factory.new_dfp_str("12345").unwrap()), &factory.new_dfp_str("-0").unwrap(), FLAG_INEXACT, "pow #62");
        // pinf  ^ +anything   = pinf
        test(&DfpMath::pow_dfp(pinf, &factory.new_dfp_str("3").unwrap()), pinf, 0, "pow #63");
        test(&DfpMath::pow_dfp(pinf, &factory.new_dfp_str("1e30").unwrap()), pinf, 0, "pow #64");
        test(&DfpMath::pow_dfp(pinf, &factory.new_dfp_str("1e-30").unwrap()), pinf, 0, "pow #65");
        test(&DfpMath::pow_dfp(pinf, pinf), pinf, 0, "pow #66");
        // pinf  ^ -anything   = +0
        test(&DfpMath::pow_dfp(pinf, &factory.new_dfp_str("-3").unwrap()), &factory.get_zero(), 0, "pow #67");
        test(&DfpMath::pow_dfp(pinf, &factory.new_dfp_str("-1e30").unwrap()), &factory.get_zero(), 0, "pow #68");
        test(&DfpMath::pow_dfp(pinf, &factory.new_dfp_str("-1e-30").unwrap()), &factory.get_zero(), 0, "pow #69");
        test(&DfpMath::pow_dfp(pinf, ninf), &factory.get_zero(), 0, "pow #70");
        // ninf  ^ anything   = -0 ^ -anything
        // ninf  ^ -anything except 0, NAN, odd integer  = +0
        test(&DfpMath::pow_dfp(ninf, &factory.new_dfp_str("-2").unwrap()), &factory.new_dfp_str("0").unwrap(), 0, "pow #71");
        test(&DfpMath::pow_dfp(ninf, &factory.new_dfp_str("-1e30").unwrap()), &factory.new_dfp_str("0").unwrap(), 0, "pow #72");
        test(&DfpMath::pow_dfp(ninf, &factory.new_dfp_str("-1e-30").unwrap()), &factory.new_dfp_str("0").unwrap(), FLAG_INEXACT, "pow #73");
        test(&DfpMath::pow_dfp(ninf, ninf), &factory.new_dfp_str("0").unwrap(), 0, "pow #74");
        // ninf  ^ +anything except 0, NAN, odd integer  = +INF
        test(&DfpMath::pow_dfp(ninf, &factory.new_dfp_str("2").unwrap()), pinf, 0, "pow #75");
        test(&DfpMath::pow_dfp(ninf, &factory.new_dfp_str("1e30").unwrap()), pinf, 0, "pow #76");
        test(&DfpMath::pow_dfp(ninf, &factory.new_dfp_str("1e-30").unwrap()), pinf, FLAG_INEXACT, "pow #77");
        test(&DfpMath::pow_dfp(ninf, pinf), pinf, 0, "pow #78");
        // ninf  ^ +odd integer   =  -INF
        test(&DfpMath::pow_dfp(ninf, &factory.new_dfp_str("3").unwrap()), ninf, FLAG_INEXACT, "pow #79");
        test(&DfpMath::pow_dfp(ninf, &factory.new_dfp_str("12345").unwrap()), ninf, FLAG_INEXACT, "pow #80");
        // ninf  ^ -odd integer   =  -0
        test(&DfpMath::pow_dfp(ninf, &factory.new_dfp_str("-3").unwrap()), &factory.new_dfp_str("-0").unwrap(), FLAG_INEXACT, "pow #81");
        test(&DfpMath::pow_dfp(ninf, &factory.new_dfp_str("-12345").unwrap()), &factory.new_dfp_str("-0").unwrap(), FLAG_INEXACT, "pow #82");
        // -anything ^ integer
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("-2").unwrap(), &factory.new_dfp_str("3").unwrap()), &factory.new_dfp_str("-8").unwrap(), FLAG_INEXACT, "pow #83");
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("-2").unwrap(), &factory.new_dfp_str("16").unwrap()), &factory.new_dfp_str("65536").unwrap(), 0, "pow #84");
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("-2").unwrap(), &factory.new_dfp_str("-3").unwrap()), &factory.new_dfp_str("-0.125").unwrap(), FLAG_INEXACT, "pow #85");
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("-2").unwrap(), &factory.new_dfp_str("-4").unwrap()), &factory.new_dfp_str("0.0625").unwrap(), 0, "pow #86");
        // -anything ^ noninteger = NaN
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("-2").unwrap(), &factory.new_dfp_str("-4.1").unwrap()), qnan, FLAG_INVALID | FLAG_INEXACT, "pow #87");
        // Some fractional cases.
        test(&DfpMath::pow_dfp(&factory.new_dfp_str("2").unwrap(), &factory.new_dfp_str("1.5").unwrap()), &factory.new_dfp_str("2.8284271247461901").unwrap(), FLAG_INEXACT, "pow #88");
    }

    it "can sin"  {
        test(&DfpMath::sin(pinf), nan, FLAG_INVALID | FLAG_INEXACT, "sin #1");
        test(&DfpMath::sin(nan), nan, FLAG_INVALID | FLAG_INEXACT, "sin #2");
        test(&DfpMath::sin(&factory.get_zero()), &factory.get_zero(), FLAG_INEXACT, "sin #3");
        test(&DfpMath::sin(&factory.get_pi()), &factory.get_zero(), FLAG_INEXACT, "sin #4");
        test(&DfpMath::sin(&factory.get_pi().negate()), &factory.new_dfp_str("-0").unwrap(), FLAG_INEXACT, "sin #5");
        test(&DfpMath::sin(&factory.get_pi().multiply_i32(2)), &factory.get_zero(), FLAG_INEXACT, "sin #6");
        test(&DfpMath::sin(&factory.get_pi().divide_i32(2)), &factory.get_one(), FLAG_INEXACT, "sin #7");
        test(&DfpMath::sin(&factory.get_pi().divide_i32(2).negate()), &factory.get_one().negate(), FLAG_INEXACT, "sin #8");
        test(&// pi/4
        DfpMath::sin(&DfpMath::atan(&factory.get_one())), &factory.new_dfp_str("0.5").unwrap().sqrt(), FLAG_INEXACT, "sin #9");
        test(&// -pi/4
        DfpMath::sin(&DfpMath::atan(&factory.get_one())).negate(), &factory.new_dfp_str("0.5").unwrap().sqrt().negate(), FLAG_INEXACT, "sin #10");
        test(&// -pi/4
        DfpMath::sin(&DfpMath::atan(&factory.get_one())).negate(), &factory.new_dfp_str("0.5").unwrap().sqrt().negate(), FLAG_INEXACT, "sin #11");
        test(&DfpMath::sin(&factory.new_dfp_str("0.1").unwrap()), &factory.new_dfp_str("0.0998334166468281523").unwrap(), FLAG_INEXACT, "sin #12");
        test(&DfpMath::sin(&factory.new_dfp_str("0.2").unwrap()), &factory.new_dfp_str("0.19866933079506121546").unwrap(), FLAG_INEXACT, "sin #13");
        test(&DfpMath::sin(&factory.new_dfp_str("0.3").unwrap()), &factory.new_dfp_str("0.2955202066613395751").unwrap(), FLAG_INEXACT, "sin #14");
        test(&DfpMath::sin(&factory.new_dfp_str("0.4").unwrap()), &factory.new_dfp_str("0.38941834230865049166").unwrap(), FLAG_INEXACT, "sin #15");
        test(&DfpMath::sin(&factory.new_dfp_str("0.5").unwrap()), &// off by one ULP
        factory.new_dfp_str("0.47942553860420300026").unwrap(), FLAG_INEXACT, "sin #16");
        test(&DfpMath::sin(&factory.new_dfp_str("0.6").unwrap()), &// off by one ULP
        factory.new_dfp_str("0.56464247339503535721").unwrap(), FLAG_INEXACT, "sin #17");
        test(&DfpMath::sin(&factory.new_dfp_str("0.7").unwrap()), &factory.new_dfp_str("0.64421768723769105367").unwrap(), FLAG_INEXACT, "sin #18");
        test(&DfpMath::sin(&factory.new_dfp_str("0.8").unwrap()), &factory.new_dfp_str("0.71735609089952276163").unwrap(), FLAG_INEXACT, "sin #19");
        test(&// off by one ULP
        DfpMath::sin(&factory.new_dfp_str("0.9").unwrap()), &factory.new_dfp_str("0.78332690962748338847").unwrap(), FLAG_INEXACT, "sin #20");
        test(&DfpMath::sin(&factory.new_dfp_str("1.0").unwrap()), &factory.new_dfp_str("0.84147098480789650666").unwrap(), FLAG_INEXACT, "sin #21");
        test(&DfpMath::sin(&factory.new_dfp_str("1.1").unwrap()), &factory.new_dfp_str("0.89120736006143533995").unwrap(), FLAG_INEXACT, "sin #22");
        test(&DfpMath::sin(&factory.new_dfp_str("1.2").unwrap()), &factory.new_dfp_str("0.93203908596722634968").unwrap(), FLAG_INEXACT, "sin #23");
        test(&DfpMath::sin(&factory.new_dfp_str("1.3").unwrap()), &factory.new_dfp_str("0.9635581854171929647").unwrap(), FLAG_INEXACT, "sin #24");
        test(&DfpMath::sin(&factory.new_dfp_str("1.4").unwrap()), &factory.new_dfp_str("0.98544972998846018066").unwrap(), FLAG_INEXACT, "sin #25");
        test(&DfpMath::sin(&factory.new_dfp_str("1.5").unwrap()), &factory.new_dfp_str("0.99749498660405443096").unwrap(), FLAG_INEXACT, "sin #26");
        test(&DfpMath::sin(&factory.new_dfp_str("1.6").unwrap()), &factory.new_dfp_str("0.99957360304150516323").unwrap(), FLAG_INEXACT, "sin #27");
    }
    	
    
    

    }

}
