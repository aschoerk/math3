#![allow(non_camel_case_types)]
use dfp::dfp::*;
use std::cell::*;
use std::rc::*;
use std::collections::*;
use std::hash::{Hash, Hasher};

thread_local!(static DFP_FIELDS: RefCell<HashMap<usize,Rc<DfpField>>> = RefCell::new(HashMap::new()));

thread_local!(static DFP_FIELD_STRING_CONSTANTS: RefCell<Vec<DfpFieldStringConstants>> = RefCell::new(Vec::new()));

pub fn get_dfp_field_for(field: &DfpField) -> Rc<DfpField> {
    DFP_FIELDS.with(|hmref| {
        let mut found = false;
        let content: Rc<DfpField> = {
            let hm = hmref.borrow();
            if hm.contains_key(&field.get_radix_digits()) {
                let tmp = hm.get(&field.get_radix_digits());
                let rc = tmp.unwrap();
                match rc.constants {
                    None => {
                        match field.constants {
                            None => {
                                found = true;
                                rc.clone()
                            }
                            _ => Rc::new(field.clone()),	
                        }
                    }
                    _ => {
                        found = true;
                        rc.clone()
                    }             	
                }
            } else {
                Rc::new(field.clone())
            }
        };
        if !found {
            let mut hm = hmref.borrow_mut();
            hm.insert(field.get_radix_digits(), content.clone());
        }
        content
    })
}

fn get_dfp_field_string_constants_for(field: &Rc<DfpField>) -> DfpFieldStringConstants {
    DFP_FIELD_STRING_CONSTANTS.with(|string_constants_ref| {
        let mut change_it = true;
        let content: DfpFieldStringConstants = {
            let tmp = string_constants_ref.borrow();
            if tmp.len() > 0 {
                let string_constants: &DfpFieldStringConstants = &tmp[0];
                if string_constants.sqr2_string.len() >= field.get_radix_digits() * 4 - 3 {
                    DfpFieldStringConstants::compute_string_constants(field)
                } else {
                    change_it = false;
                    string_constants.clone()
                }
            } else {
                DfpFieldStringConstants::compute_string_constants(field)
            }
        };
        if change_it {
            let mut tmp = string_constants_ref.borrow_mut();
            tmp.clear();
            tmp.push(content.clone());
        }
        content
    })
}

/** IEEE 854-1987 flag for invalid operation. */
pub const FLAG_INVALID: i32 = 1;

/** IEEE 854-1987 flag for division by zero. */
pub const FLAG_DIV_ZERO: i32 = 2;

/** IEEE 854-1987 flag for overflow. */
pub const FLAG_OVERFLOW: i32 = 4;

/** IEEE 854-1987 flag for underflow. */
pub const FLAG_UNDERFLOW: i32 = 8;

/** IEEE 854-1987 flag for inexact result. */
pub const FLAG_INEXACT: i32 = 16;


#[derive(Copy,Clone,PartialEq, Eq, Hash)]
/** Enumerate for rounding modes. */
pub enum RoundingMode {
    /** Rounds toward zero (truncation). */
    ROUND_DOWN,
    /** Rounds away from zero if discarded digit is non-zero. */
    ROUND_UP,
    /** Rounds towards nearest unless both are equidistant in which case it rounds away from zero. */
    ROUND_HALF_UP,
    /** Rounds towards nearest unless both are equidistant in which case it rounds toward zero. */
    ROUND_HALF_DOWN,
    /** Rounds towards nearest unless both are equidistant in which case it rounds toward the even neighbor.
     * This is the default as  specified by IEEE 854-1987
     */
    ROUND_HALF_EVEN,
    /** Rounds towards nearest unless both are equidistant in which case it rounds toward the odd neighbor.  */
    ROUND_HALF_ODD,
    /** Rounds towards positive infinity. */
    ROUND_CEIL,
    /** Rounds towards negative infinity. */
    ROUND_FLOOR,
}



pub trait DfpFieldTrait {
    fn get_radix_digits(&self) -> usize;

    fn get_zero(&self) -> Dfp;
    fn get_one(&self) -> Dfp;
    fn get_two(&self) -> Dfp;

    fn get_rounding_mode(&self) -> RoundingMode;

    fn set_rounding_mode(&self, RoundingMode);

    fn set_i_e_e_e_flags(&self, flags: i32);

    fn set_i_e_e_e_flags_bits(&self, flags: i32);

    fn get_i_e_e_e_flags(&self) -> i32;

    fn clear_i_e_e_e_flags(&self);
}

#[derive(Clone)]
pub struct DfpFieldStringConstants {
    pub sqr2_string: String,

    // Note: the static strings are set up (once) by the ctor and @GuardedBy("DfpField.class")
    /** High precision string representation of √2 / 2. */
    pub sqr2_reciprocal_string: String,

    /** High precision string representation of √3. */
    pub sqr3_string: String,

    /** High precision string representation of √3 / 3. */
    pub sqr3_reciprocal_string: String,

    /** High precision string representation of π. */
    pub pi_string: String,

    /** High precision string representation of e. */
    pub e_string: String,

    /** High precision string representation of ln(2). */
    pub ln2_string: String,

    /** High precision string representation of ln(5). */
    pub ln5_string: String,

    /** High precision string representation of ln(10). */
    pub ln10_string: String,
}

impl DfpFieldStringConstants {
    fn compute_string_constants(high_precision_field_p: &Rc<DfpField>) -> DfpFieldStringConstants {
        let high_precision_field = get_dfp_field_for(&DfpField {
            radix_digits: if high_precision_field_p.get_radix_digits() < 50 {
                50
            } else {
                high_precision_field_p.get_radix_digits()
            },

            ieee_flags: Cell::new(0i32),

            rounding_mode: Cell::new(RoundingMode::ROUND_HALF_EVEN),

            constants: None,
        });
        // recompute the string representation of the transcendental constants
        let high_precision_one: Dfp = Dfp::new_i32(&high_precision_field, 1);
        let high_precision_two: Dfp = Dfp::new_i32(&high_precision_field, 2);
        let high_precision_three: Dfp = Dfp::new_i32(&high_precision_field, 3);
        let high_precision_sqr2: Dfp = high_precision_two.sqrt();

        let high_precision_sqr3: Dfp = high_precision_three.sqrt();

        // let pi_string = compute_pi(high_precision_one, high_precision_two, high_precision_three).to_string();
        // let e_string = compute_exp(high_precision_one, high_precision_one).to_string();
        // let ln2_string = compute_ln(high_precision_two, high_precision_one, high_precision_two).to_string();
        // let ln5_string = compute_ln(Dfp::new(high_precision_field, 5), high_precision_one, high_precision_two).to_string();
        // let ln10_string = compute_ln(Dfp::new(high_precision_field, 10), high_precision_one, high_precision_two).to_string();
        let res = DfpFieldStringConstants {
            sqr2_string: high_precision_sqr2.to_string(),
            sqr2_reciprocal_string: high_precision_one.divide(&high_precision_sqr2)
                .unwrap()
                .to_string(),
            sqr3_string: high_precision_sqr3.to_string(),
            sqr3_reciprocal_string: high_precision_one.divide(&high_precision_sqr3)
                .unwrap()
                .to_string(),
            pi_string: DfpField::compute_pi(&high_precision_one,
                                            &high_precision_two,
                                            &high_precision_three)
                .to_string(),

            e_string: DfpField::compute_exp(&high_precision_one, &high_precision_one).to_string(),

            ln2_string: DfpField::compute_ln(&high_precision_two,
                                             &high_precision_one,
                                             &high_precision_two)
                .to_string(),

            ln5_string: DfpField::compute_ln(&Dfp::new_i32(&high_precision_field, 5),
                                             &high_precision_one,
                                             &high_precision_two)
                .to_string(),

            ln10_string: DfpField::compute_ln(&Dfp::new_i32(&high_precision_field, 10),
                                              &high_precision_one,
                                              &high_precision_two)
                .to_string(),
        };
        println!("fieldnum: {}",high_precision_field.get_radix_digits());
        println!("sqr2_string: {}",res.sqr2_string);
        println!("sqr2_reciprocal_string: {}",res.sqr2_reciprocal_string);
        println!("sqr3_string: {}",res.sqr3_string);
        println!("sqr3_reciprocal_string: {}",res.sqr3_reciprocal_string);
        println!("pi_string: {}",res.pi_string);
        println!("e_string: {}",res.e_string);
        println!("ln2_string: {}",res.ln2_string);
        println!("ln5_string: {}",res.ln5_string);
        println!("ln10_string: {}",res.ln10_string);
        res

    }
}

pub struct DfpFieldConstants {
    /** A {@link Dfp} with value √2. */
    sqr2: Dfp,

    /** A two elements {@link Dfp} array with value √2 split in two pieces. */
    sqr2_split: [Dfp; 2],

    /** A {@link Dfp} with value √2 / 2. */
    sqr2_reciprocal: Dfp,

    /** A {@link Dfp} with value √3. */
    sqr3: Dfp,

    /** A {@link Dfp} with value √3 / 3. */
    sqr3_reciprocal: Dfp,

    /** A {@link Dfp} with value π. */
    pi: Dfp,

    /** A two elements {@link Dfp} array with value π split in two pieces. */
    pi_split: [Dfp; 2],

    /** A {@link Dfp} with value e. */
    e: Dfp,

    /** A two elements {@link Dfp} arrayflags: i32 with value e split in two pieces. */
    e_split: [Dfp; 2],

    /** A {@link Dfp} with value ln(2). */
    ln2: Dfp,

    /** A two elements {@link Dfp} array with value ln(2) split in two pieces. */
    ln2_split: [Dfp; 2],

    /** A {@link Dfp} with value ln(5). */
    ln5: Dfp,

    /** A two elements {@link Dfp} array with value ln(5) split in two pieces. */
    ln5_split: [Dfp; 2],

    /** A {@link Dfp} with value ln(10). */
    ln10: Dfp,
}

impl DfpFieldConstants {
    pub fn compute_dfp_field_constants(field: &Rc<DfpField>) -> DfpFieldConstants {
        let string_constants = get_dfp_field_string_constants_for(field);
        DfpFieldConstants {
            sqr2: Dfp::new_str(&field, string_constants.sqr2_string.as_str()).unwrap(),
            sqr2_split: field.split(&string_constants.sqr2_string),
            sqr2_reciprocal: Dfp::new_str(&field, string_constants.sqr2_reciprocal_string.as_str())
                .unwrap(),
            sqr3: Dfp::new_str(&field, string_constants.sqr3_string.as_str()).unwrap(),
            sqr3_reciprocal: Dfp::new_str(&field, string_constants.sqr3_reciprocal_string.as_str())
                .unwrap(),
            pi: Dfp::new_str(&field, string_constants.pi_string.as_str()).unwrap(),
            pi_split: field.split(&string_constants.pi_string),
            e: Dfp::new_str(&field, string_constants.e_string.as_str()).unwrap(),
            e_split: field.split(&string_constants.e_string),
            ln2: Dfp::new_str(&field, string_constants.ln2_string.as_str()).unwrap(),
            ln2_split: field.split(&string_constants.ln2_string),
            ln5: Dfp::new_str(&field, string_constants.ln5_string.as_str()).unwrap(),
            ln5_split: field.split(&string_constants.ln5_string),
            ln10: Dfp::new_str(&field, string_constants.ln10_string.as_str()).unwrap(),
        }
    }
}

#[derive(Clone)]
pub struct DfpField {
    radix_digits: usize,

    ieee_flags: Cell<i32>,

    rounding_mode: Cell<RoundingMode>,

    constants: Option<Rc<DfpFieldConstants>>,
}

impl PartialEq for DfpField {
    fn eq(&self, other: &DfpField) -> bool {
        self.radix_digits == other.radix_digits && self.rounding_mode == other.rounding_mode
    }
}

impl Hash for DfpField {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.radix_digits.hash(state);
        self.get_rounding_mode().hash(state);
    }
}

impl DfpFieldTrait for DfpField {
    fn get_radix_digits(&self) -> usize {
        self.radix_digits
    }

    fn get_zero(&self) -> Dfp {
        Dfp::new_i64(&get_dfp_field_for(self), 0)
    }

    fn get_one(&self) -> Dfp {
        Dfp::new_i64(&get_dfp_field_for(self), 1)
    }

    fn get_two(&self) -> Dfp {
        Dfp::new_i64(&get_dfp_field_for(self), 2)
    }

    fn get_rounding_mode(&self) -> RoundingMode {
        self.rounding_mode.get()
    }

    fn set_rounding_mode(&self, rounding_mode: RoundingMode) {
        self.rounding_mode.set(rounding_mode);

    }

    fn set_i_e_e_e_flags(&self, flags: i32) {
        self.ieee_flags.set(flags &
                            (FLAG_INVALID | FLAG_DIV_ZERO | FLAG_OVERFLOW | FLAG_UNDERFLOW |
                             FLAG_INEXACT));
    }

    fn set_i_e_e_e_flags_bits(&self, bits: i32) {
        let ieeeflags = self.ieee_flags.get();
        self.ieee_flags.set((ieeeflags | bits) &
                            (FLAG_INVALID | FLAG_DIV_ZERO | FLAG_OVERFLOW | FLAG_UNDERFLOW |
                             FLAG_INEXACT));
    }

    fn get_i_e_e_e_flags(&self) -> i32 {
        self.ieee_flags.get()
    }

    fn clear_i_e_e_e_flags(&self) {
        self.ieee_flags.set(0);
    }
}

impl DfpField {
    pub fn new(decimal_digits: usize) -> DfpField {
        let tmpres = Rc::new(DfpField {
            radix_digits: if decimal_digits < 13 {
                4
            } else {
                (decimal_digits + 3) / 4
            },
            ieee_flags: Cell::new(0),
            rounding_mode: Cell::new(RoundingMode::ROUND_HALF_EVEN),
            constants: None,
        });
        let res = DfpField {
            radix_digits: tmpres.radix_digits,
            ieee_flags: Cell::new(0),
            rounding_mode: Cell::new(RoundingMode::ROUND_HALF_EVEN),
            constants: Some(Rc::new(DfpFieldConstants::compute_dfp_field_constants(&tmpres))),
        };

        res
    }

    /** Creates a {@link Dfp} with a non-finite value.
     * @param sign sign of the Dfp to create
     * @param nans code of the value, must be one of {@link Dfp#INFINITE},
     * {@link Dfp#SNAN},  {@link Dfp#QNAN}
     * @return a new {@link Dfp} with a non-finite value
     */
    pub fn new_dfp_sign_nans(&self, sign: i8, nans: i8) -> Dfp {
        return Dfp::new_sign_nans(&get_dfp_field_for(self), sign, nans);
    }

    /** Makes a {@link Dfp} with a value of 0.
     * @return a new {@link Dfp} with a value of 0
     */
    pub fn new_dfp(&self) -> Dfp {
        return Dfp::new(&get_dfp_field_for(self));
    }

    /** Create an instance from a byte value.
     * @param x value to convert to an instance
     * @return a new {@link Dfp} with the same value as x
     */
    pub fn new_dfp_i8(&self, x: i8) -> Dfp {
        return Dfp::new_i8(&get_dfp_field_for(self), x);
    }

    /** Create an instance from an int value.
     * @param x value to convert to an instance
     * @return a new {@link Dfp} with the same value as x
     */
    pub fn new_dfp_i32(&self, x: i32) -> Dfp {
        return Dfp::new_i32(&get_dfp_field_for(self), x);
    }

    /** Create an instance from a long value.
     * @param x value to convert to an instance
     * @return a new {@link Dfp} with the same value as x
     */
    pub fn new_dfp_i64(&self, x: i64) -> Dfp {
        return Dfp::new_i64(&get_dfp_field_for(self), x);
    }

    /** Create an instance from a double value.
     * @param x value to convert to an instance
     * @return a new {@link Dfp} with the same value as x
     */
    pub fn new_dfp_f64(&self, x: f64) -> Dfp {
        return Dfp::new_f64(&get_dfp_field_for(self), x);
    }

    /** Copy constructor.
     * @param d instance to copy
     * @return a new {@link Dfp} with the same value as d
     */
    pub fn new_dfp_dfp(&self, d: &Dfp) -> Dfp {
        return d.clone();
    }

    /** Create a {@link Dfp} given a String representation.
     * @param s string representation of the instance
     * @return a new {@link Dfp} parsed from specified string
     */
    pub fn new_dfp_str(&self, s: &str) -> Result<Dfp, String> {
        return Dfp::new_str(&get_dfp_field_for(self), s);
    }

    fn get_constants(&self) -> Rc<DfpFieldConstants> {
        let f = get_dfp_field_for(self);
        f.constants.clone().unwrap()
    }

    /** Get the constant √2.
     * @return a {@link Dfp} with value √2
     */
    pub fn get_sqr2(&self) -> Dfp {
        return self.get_constants().sqr2.clone();
    }

    /** Get the constant √2 split in two pieces.
     * @return a {@link Dfp} with value √2 split in two pieces
     */
    pub fn get_sqr2_split(&self) -> [Dfp; 2] {
        return [self.get_constants().sqr2_split[0].clone(),
                self.get_constants().sqr2_split[1].clone()];
    }

    /** Get the constant √2 / 2.
     * @return a {@link Dfp} with value √2 / 2
     */
    pub fn get_sqr2_reciprocal(&self) -> Dfp {
        return self.get_constants().sqr2_reciprocal.clone();
    }

    /** Get the constant √3.
     * @return a {@link Dfp} with value √3
     */
    pub fn get_sqr3(&self) -> Dfp {
        return self.get_constants().sqr3.clone();
    }

    /** Get the constant √3 / 3.
     * @return a {@link Dfp} with value √3 / 3
     */
    pub fn get_sqr3_reciprocal(&self) -> Dfp {
        return self.get_constants().sqr3_reciprocal.clone();
    }

    /** Get the constant π.
     * @return a {@link Dfp} with value π
     */
    pub fn get_pi(&self) -> Dfp {
        return self.get_constants().pi.clone();
    }

    /** Get the constant π split in two pieces.
     * @return a {@link Dfp} with value π split in two pieces
     */
    pub fn get_pi_split(&self) -> [Dfp; 2] {
        return [self.get_constants().pi_split[0].clone(), self.get_constants().pi_split[1].clone()];
    }

    /** Get the constant e.
     * @return a {@link Dfp} with value e
     */
    pub fn get_e(&self) -> Dfp {
        return self.get_constants().e.clone();
    }

    /** Get the constant e split in two pieces.
     * @return a {@link Dfp} with value e split in two pieces
     */
    pub fn get_e_split(&self) -> [Dfp; 2] {
        return [self.get_constants().e_split[0].clone(), self.get_constants().e_split[1].clone()];
    }

    /** Get the constant ln(2).
     * @return a {@link Dfp} with value ln(2)
     */
    pub fn get_ln2(&self) -> Dfp {
        return self.get_constants().ln2.clone();
    }

    /** Get the constant ln(2) split in two pieces.
     * @return a {@link Dfp} with value ln(2) split in two pieces
     */
    pub fn get_ln2_split(&self) -> [Dfp; 2] {
        return [self.get_constants().ln2_split[0].clone(),
                self.get_constants().ln2_split[1].clone()];
    }

    /** Get the constant ln(5).
     * @return a {@link Dfp} with value ln(5)
     */
    pub fn get_ln5(&self) -> Dfp {
        return self.get_constants().ln5.clone();
    }

    /** Get the constant ln(5) split in two pieces.
     * @return a {@link Dfp} with value ln(5) split in two pieces
     */
    pub fn get_ln5_split(&self) -> [Dfp; 2] {
        return [self.get_constants().ln5_split[0].clone(),
                self.get_constants().ln5_split[1].clone()];
    }

    /** Get the constant ln(10).
     * @return a {@link Dfp} with value ln(10)
     */
    pub fn get_ln10(&self) -> Dfp {
        return self.get_constants().ln10.clone();
    }


    /** Breaks a string representation up into two {@link Dfp}'s.
     * The split is such that the sum of them is equivalent to the input string,
     * but has higher precision than using a single Dfp.
     * @param a string representation of the number to split
     * @return an array of two {@link Dfp Dfp} instances which sum equals a
     */
    fn split(&self, a: &String) -> [Dfp; 2] {
        let mut result: [Dfp; 2] = [self.get_zero(), self.get_zero()];
        let mut leading: bool = true;
        let mut sp: i32 = 0;
        let mut sig: i32 = 0;
        let mut buf = String::new();
        let mut chars = a.chars();
        for i in 0..a.len() {
            let c = chars.next().unwrap();
            if c >= '1' && c <= '9' {
                leading = false;
            }
            if c == '.' {
                sig += (400 - sig) % 4;
                leading = false;
            }
            if sig == (self.get_radix_digits() as i32 / 2) * 4 {
                sp = i as i32;
                break;
            }
            if c >= '0' && c <= '9' && !leading {
                sig += 1;
            }
            buf.push(c);
        }
        result[0] = Dfp::new_str(&get_dfp_field_for(self), buf.as_str()).unwrap();
        chars = a.chars();
        buf.clear();
        for i in 0..a.len() {
            let mut c = chars.next().unwrap();
            if c >= '0' && c <= '9' && i < sp as usize {
                c = '0';
            }
            buf.push(c);
        }
        result[1] = Dfp::new_str(&get_dfp_field_for(self), buf.as_str()).unwrap();
        return result;
    }

    /** Compute π using Jonathan and Peter Borwein quartic formula.
     * @param one constant with value 1 at desired precision
     * @param two constant with value 2 at desired precision
     * @param three constant with value 3 at desired precision
     * @return π
     */
    fn compute_pi(one: &Dfp, two: &Dfp, three: &Dfp) -> Dfp {
        let sqrt2: Dfp = two.sqrt();
        let mut yk: Dfp = sqrt2.subtract(one);
        let four: Dfp = two.add(two);
        let mut two2kp3: Dfp = two.clone();
        let mut ak: Dfp = two.multiply(&three.subtract(&two.multiply(&sqrt2)));
        // So the limit here is considered sufficient for most purposes ...
        for _ in 1..20 {
            let yk_m1: Dfp = yk.clone();
            let y2: Dfp = yk.multiply(&yk);
            let one_minus_y4: Dfp = one.subtract(&y2.multiply(&y2));
            let s: Dfp = one_minus_y4.sqrt().sqrt();
            yk = one.subtract(&s).divide(&one.add(&s)).unwrap();
            two2kp3 = two2kp3.multiply(&four);
            let p: Dfp = one.add(&yk);
            let p2: Dfp = p.multiply(&p);
            ak = ak.multiply(&p2.multiply(&p2))
                .subtract(&two2kp3.multiply(&yk).multiply(&one.add(&yk).add(&yk.multiply(&yk))));
            if yk == yk_m1 {
                break;
            }
        }
        return one.divide(&ak).unwrap();
    }

    /** Compute exp(a).
     * @param a number for which we want the exponential
     * @param one constant with value 1 at desired precision
     * @return exp(a)
     */
    pub fn compute_exp(a: &Dfp, one: &Dfp) -> Dfp {
        let mut y: Dfp = one.clone();
        let mut py: Dfp = one.clone();
        let mut f: Dfp = one.clone();
        let mut fi: Dfp = one.clone();
        let mut x: Dfp = one.clone();
        for _ in 0..10000 {
            x = x.multiply(a);
            y = y.add(&x.divide(&f).unwrap());
            fi = fi.add(&one);
            f = f.multiply(&fi);
            if y == py {
                break;
            }
            py = y.clone();
        }
        return y;
    }

    /** Compute ln(a).
     *
     *  Let f(x) = ln(x),
     *
     *  We know that f'(x) = 1/x, thus from Taylor's theorem we have:
     *
     *           -----          n+1         n
     *  f(x) =   \           (-1)    (x - 1)
     *           /          ----------------    for 1 <= n <= infinity
     *           -----             n
     *
     *  or
     *                       2        3       4
     *                   (x-1)   (x-1)    (x-1)
     *  ln(x) =  (x-1) - ----- + ------ - ------ + ...
     *                     2       3        4
     *
     *  alternatively,
     *
     *                  2    3   4
     *                 x    x   x
     *  ln(x+1) =  x - -  + - - - + ...
     *                 2    3   4
     *
     *  This series can be used to compute ln(x), but it converges too slowly.
     *
     *  If we substitute -x for x above, we get
     *
     *                   2    3    4
     *                  x    x    x
     *  ln(1-x) =  -x - -  - -  - - + ...
     *                  2    3    4
     *
     *  Note that all terms are now negative.  Because the even powered ones
     *  absorbed the sign.  Now, subtract the series above from the previous
     *  one to get ln(x+1) - ln(1-x).  Note the even terms cancel out leaving
     *  only the odd ones
     *
     *                             3     5      7
     *                           2x    2x     2x
     *  ln(x+1) - ln(x-1) = 2x + --- + --- + ---- + ...
     *                            3     5      7
     *
     *  By the property of logarithms that ln(a) - ln(b) = ln (a/b) we have:
     *
     *                                3        5        7
     *      x+1           /          x        x        x          \
     *  ln ----- =   2 *  |  x  +   ----  +  ----  +  ---- + ...  |
     *      x-1           \          3        5        7          /
     *
     *  But now we want to find ln(a), so we need to find the value of x
     *  such that a = (x+1)/(x-1).   This is easily solved to find that
     *  x = (a-1)/(a+1).
     * @param a number for which we want the exponential
     * @param one constant with value 1 at desired precision
     * @param two constant with value 2 at desired precision
     * @return ln(a)
     */
    pub fn compute_ln(a: &Dfp, one: &Dfp, two: &Dfp) -> Dfp {
        let mut den: i32 = 1;
        let x: Dfp = a.add(&Dfp::new_i32(a.get_field(), -1)).divide(&a.add(&one)).unwrap();
        let mut y: Dfp = x.clone();
        let mut num: Dfp = x.clone();
        let mut py: Dfp = y.clone();
        for _ in 0..10000 {
            num = num.multiply(&x);
            num = num.multiply(&x);
            den += 2;
            let t: Dfp = num.divide_i32(den);
            y = y.add(&t);
            if y == py {
                break;
            }
            py = y.clone();
        }
        return y.multiply(two);
    }
}
