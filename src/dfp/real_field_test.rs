

#[cfg(test)] 
mod tests {
	use util::fastmath;
	use util::rsutils::*;
	use dfp::dfp_field::*;
	use dfp::dfp_math::*;
	use dfp::dfp::*;
	use std::usize;
	use std::f64;
	use std::rc::*;
	use std::char::*;
	use hamcrest::*;	
	#[macro_use]
	use assert;	
	use base;

struct ExtendedFieldElementAbstractTest<T> {
	data: T
}

impl<T: base::RealFieldElement<T>> ExtendedFieldElementAbstractTest<T> {
	
    pub fn  test_add_field()   {
         {
             let mut x: f64 = -3.0;
            while x < 3.0 {
                {
                     {
                         let mut y: f64 = -3.0;
                        while y < 3.0 {
                            {
                                check_relative(x + y, 
                                	&build::<T>(x).add(&build::<T>(y)));
                            }
                            y += 0.2;
                         }
                     }

                }
                x += 0.2;
             }
         }

    }
    
     
    pub fn  test_add_double()   {
         {
             let mut x: f64 = -3.0;
            while x < 3.0 {
                {
                     {
                         let mut y: f64 = -3.0;
                        while y < 3.0 {
                            {
                                check_relative(x + y, &build::<T>(x).add_f64(y));
                            }
                            y += 0.2;
                         }
                     }

                }
                x += 0.2;
             }
         }

    }

    
    pub fn  test_subtract_field()   {
         {
             let mut x: f64 = -3.0;
            while x < 3.0 {
                {
                     {
                         let mut y: f64 = -3.0;
                        while y < 3.0 {
                            {
                                check_relative(x - y, &build::<T>(x).subtract(&build::<T>(y)));
                            }
                            y += 0.2;
                         }
                     }

                }
                x += 0.2;
             }
         }

    }

    
    pub fn  test_subtract_double()   {
         {
             let mut x: f64 = -3.0;
            while x < 3.0 {
                {
                     {
                         let mut y: f64 = -3.0;
                        while y < 3.0 {
                            {
                                check_relative(x - y, &build::<T>(x).subtract_f64(y));
                            }
                            y += 0.2;
                         }
                     }

                }
                x += 0.2;
             }
         }

    }

    
    pub fn  test_multiply_field()   {
         {
             let mut x: f64 = -3.0;
            while x < 3.0 {
                {
                     {
                         let mut y: f64 = -3.0;
                        while y < 3.0 {
                            {
                                check_relative(x * y, &build::<T>(x).multiply(&build::<T>(y)));
                            }
                            y += 0.2;
                         }
                     }

                }
                x += 0.2;
             }
         }

    }

    
    pub fn  test_multiply_double()   {
         {
             let mut x: f64 = -3.0;
            while x < 3.0 {
                {
                     {
                         let mut y: f64 = -3.0;
                        while y < 3.0 {
                            {
                                check_relative(x * y, &build::<T>(x).multiply_f64(y));
                            }
                            y += 0.2;
                         }
                     }

                }
                x += 0.2;
             }
         }

    }

    
    pub fn  test_multiply_int()   {
         {
             let mut x: f64 = -3.0;
            while x < 3.0 {
                {
                     {
                         let mut y: i32 = -10;
                        while y < 10 {
                            {
                                check_relative(x * y as f64, &build::<T>(x).multiply_i32(y));
                            }
                            y += 1;
                         }
                     }

                }
                x += 0.2;
             }
         }

    }

    
    pub fn  test_divide_field()   {
         {
             let mut x: f64 = -3.0;
            while x < 3.0 {
                {
                     {
                         let mut y: f64 = -3.0;
                        while y < 3.0 {
                            {
                                check_relative(x / y, &build::<T>(x).divide(&build::<T>(y)).unwrap());
                            }
                            y += 0.2;
                         }
                     }

                }
                x += 0.2;
             }
         }

    }

    
    pub fn  test_divide_double()   {
         {
             let mut x: f64 = -3.0;
            while x < 3.0 {
                {
                     {
                         let mut y: f64 = -3.0;
                        while y < 3.0 {
                            {
                                check_relative(x / y, &build::<T>(x).divide_f64(y));
                            }
                            y += 0.2;
                         }
                     }

                }
                x += 0.2;
             }
         }

    }

    /*
    pub fn  test_remainder_field()   {
         {
             let mut x: f64 = -3.0;
            while x < 3.0 {
                {
                     {
                         let mut y: f64 = -3.0;
                        while y < 3.0 {
                            {
                                check_relative(fastmath::F64::IEEEremainder(x, y), &build::<T>(x).remainder(&build::<T>(y)));
                            }
                            y += 0.2;
                         }
                     }

                }
                x += 0.2;
             }
         }

    }

    
    pub fn  test_remainder_double()   {
         {
             let mut x: f64 = -3.0;
            while x < 3.0 {
                {
                     {
                         let mut y: f64 = -3.2;
                        while y < 3.2 {
                            {
                                check_relative(fastmath::F64::IEEEremainder(x, y), &build::<T>(x).remainder(y));
                            }
                            y += 0.25;
                         }
                     }

                }
                x += 0.2;
             }
         }

    }
    

    
    pub fn  test_cos()   {
         {
             let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::cos(x), &build::<T>(x).cos());
                }
                x += 0.05;
             }
         }

    }

    
    pub fn  test_acos()   {
         {
             let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::acos(x), &build::<T>(x).acos());
                }
                x += 0.05;
             }
         }

    }

    
    pub fn  test_sin()   {
         {
             let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::sin(x), &build::<T>(x).sin());
                }
                x += 0.05;
             }
         }

    }

    
    pub fn  test_asin()   {
         {
             let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::asin(x), &build::<T>(x).asin());
                }
                x += 0.05;
             }
         }

    }

    
    pub fn  test_tan()   {
         {
             let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::tan(x), &build::<T>(x).tan());
                }
                x += 0.05;
             }
         }

    }

    
    pub fn  test_atan()   {
         {
             let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::atan(x), &build::<T>(x).atan());
                }
                x += 0.05;
             }
         }

    }

    
    pub fn  test_atan2()   {
         {
             let mut x: f64 = -3.0;
            while x < 3.0 {
                {
                     {
                         let mut y: f64 = -3.0;
                        while y < 3.0 {
                            {
                                check_relative(fastmath::F64::atan2(x, y), &build::<T>(x).atan2(&build::<T>(y)));
                            }
                            y += 0.2;
                         }
                     }

                }
                x += 0.2;
             }
         }

    }
    */

    
    pub fn  test_cosh()   {
         {
             let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::cosh(x), &build::<T>(x).cosh());
                }
                x += 0.05;
             }
         }

    }


/*    
    pub fn  test_acosh()   {
         {
             let mut x: f64 = 1.1;
            while x < 5.0 {
                {
                    check_relative(fastmath::F64::acosh(x), &build::<T>(x).acosh());
                }
                x += 0.05;
             }
         }

    }
*/
    
    pub fn  test_sinh()   {
         {
             let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::sinh(x), &build::<T>(x).sinh());
                }
                x += 0.05;
             }
         }

    }

 /*   
    pub fn  test_asinh()   {
         {
             let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::asinh(x), &build::<T>(x).asinh());
                }
                x += 0.05;
             }
         }

    }
*/
    
    pub fn  test_tanh()   {
         {
             let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::tanh(x), &build::<T>(x).tanh());
                }
                x += 0.05;
             }
         }

    }

  /*  
    pub fn  test_atanh()   {
         {
             let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::atanh(x), &build::<T>(x).atanh());
                }
                x += 0.05;
             }
         }

    }
*/
    
    pub fn  test_sqrt()   {
         {
             let mut x: f64 = 0.01;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::sqrt(x), &build::<T>(x).sqrt());
                }
                x += 0.05;
             }
         }

    }

    /*
    pub fn  test_cbrt()   {
         {
             let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::cbrt(x), &build::<T>(x).cbrt());
                }
                x += 0.05;
             }
         }

    }

    
    pub fn  test_hypot()   {
         {
             let mut x: f64 = -3.0;
            while x < 3.0 {
                {
                     {
                         let mut y: f64 = -3.0;
                        while y < 3.0 {
                            {
                                check_relative(fastmath::F64::hypot(x, y), &build::<T>(x).hypot(&build::<T>(y)));
                            }
                            y += 0.2;
                         }
                     }

                }
                x += 0.2;
             }
         }

    }

    
    pub fn  test_root_n()   {
         {
             let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                     {
                         let mut n: i32 = 1;
                        while n < 5 {
                            {
                                if x < 0 {
                                    if n % 2 == 1 {
                                        check_relative(-fastmath::F64::pow(-x, 1.0 / n), &build::<T>(x).root_n(n));
                                    }
                                } else {
                                    check_relative(fastmath::F64::pow(x, 1.0 / n), &build::<T>(x).root_n(n));
                                }
                            }
                            n+= 1;
                         }
                     }

                }
                x += 0.05;
             }
         }

    }

    
    pub fn  test_pow_field()   {
         {
             let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                     {
                         let mut y: f64 = 0.1;
                        while y < 4 {
                            {
                                check_relative(fastmath::F64::pow(x, y), &build::<T>(x).pow(&build::<T>(y)));
                            }
                            y += 0.2;
                         }
                     }

                }
                x += 0.05;
             }
         }

    }

    
    pub fn  test_pow_double()   {
         {
             let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                     {
                         let mut y: f64 = 0.1;
                        while y < 4 {
                            {
                                check_relative(fastmath::F64::pow(x, y), &build::<T>(x).pow(y));
                            }
                            y += 0.2;
                         }
                     }

                }
                x += 0.05;
             }
         }

    }

    
    pub fn  test_pow_int()   {
         {
             let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                     {
                         let mut n: i32 = 0;
                        while n < 5 {
                            {
                                check_relative(fastmath::F64::pow(x, n), &build::<T>(x).pow(n));
                            }
                            n+= 1;
                         }
                     }

                }
                x += 0.05;
             }
         }

    }
*/
    
    pub fn  test_exp()   {
         {
             let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::exp(x), &build::<T>(x).exp());
                }
                x += 0.05;
             }
         }

    }

    
    pub fn  test_expm1()   {
         {
             let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::expm1(x), &build::<T>(x).expm1());
                }
                x += 0.05;
             }
         }

    }

    
    pub fn  test_log()   {
         {
             let mut x: f64 = 0.01;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::log(x), &build::<T>(x).log());
                }
                x += 0.05;
             }
         }

    }

    /*
    pub fn  test_log1p()   {
         {
             let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::log1p(x), &build::<T>(x).log1p());
                }
                x += 0.05;
             }
         }

    }
    */

    //  TODO: add this test in 4.0, as it is not possible to do it in 3.2
    //  due to incompatibility of the return type in the Dfp class
    //    @Test
    //    public void testLog10() {
    //        for (double x = -0.9; x < 0.9; x += 0.05) {
    //            checkRelative(fastmath::F64::log10(x), &build::<T>(x).log10());
    //        }
    //    }
    
    pub fn  test_abs()   {
         {
             let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::abs(x), &build::<T>(x).abs());
                }
                x += 0.05;
             }
         }

    }

    
    pub fn  test_ceil()   {
         {
             let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::ceil(x), &build::<T>(x).ceil());
                }
                x += 0.05;
             }
         }

    }

    
    pub fn  test_floor()   {
         {
             let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::floor(x), &build::<T>(x).floor());
                }
                x += 0.05;
             }
         }

    }

    
    pub fn  test_rint()   {
         {
             let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::rint(x), &build::<T>(x).rint());
                }
                x += 0.05;
             }
         }

    }

    
    pub fn  test_round()   {
         {
             let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    assert!(fastmath::F64::round(x) == build::<T>(x).round());
                }
                x += 0.05;
             }
         }
 
    }


/*
    
    pub fn  test_signum()   {
         {
             let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::signum(x), &build::<T>(x).signum());
                }
                x += 0.05;
             }
         }

    }

    
    pub fn  test_copy_sign_field()   {
         {
             let mut x: f64 = -3.0;
            while x < 3.0 {
                {
                     {
                         let mut y: f64 = -3.0;
                        while y < 3.0 {
                            {
                                check_relative(fastmath::F64::copy_sign(x, y), &build::<T>(x).copy_sign(&build::<T>(y)));
                            }
                            y += 0.2;
                         }
                     }

                }
                x += 0.2;
             }
         }

    }

    
    pub fn  test_copy_sign_double()   {
         {
             let mut x: f64 = -3.0;
            while x < 3.0 {
                {
                     {
                         let mut y: f64 = -3.0;
                        while y < 3.0 {
                            {
                                check_relative(fastmath::F64::copy_sign(x, y), &build::<T>(x).copy_sign(y));
                            }
                            y += 0.2;
                         }
                     }

                }
                x += 0.2;
             }
         }

    }
*/
    
    pub fn  test_scalb()   {
         {
             let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                     {
                         let mut n: i32 = -100;
                        while n < 100 {
                            {
                                check_relative(fastmath::F64::scalb(x, n), &build::<T>(x).scalb(n));
                            }
                            n+= 1;
                         }
                     }

                }
                x += 0.05;
             }
         }

    }
    
    
}

pub fn check_relative<T: base::RealFieldElement<T>>(expected: f64,  obtained: &T)   {	
        assert_within_delta!(expected, obtained.get_real(), 1.0e-15 * (1.0 + fastmath::F64::abs(expected)));
    }


pub fn build<T: base::RealFieldElement<T>> (x: f64) -> T {
	T::new_f64(x)
}



	#[test]
	pub fn test_add_field() {
		ExtendedFieldElementAbstractTest::<Dfp>::test_add_field();
	}

	#[test]
	pub fn test_add_double() {
		ExtendedFieldElementAbstractTest::<Dfp>::test_add_double();
	}
	
		#[test]
	pub fn test_subtract_field() {
		ExtendedFieldElementAbstractTest::<Dfp>::test_subtract_field();
	}

		#[test]
	pub fn test_subtract_double() {
		ExtendedFieldElementAbstractTest::<Dfp>::test_subtract_double();
	}

		#[test]
	pub fn test_multiply_field() {
		ExtendedFieldElementAbstractTest::<Dfp>::test_multiply_field();
	}

		#[test]
	pub fn test_multiply_double() {
		ExtendedFieldElementAbstractTest::<Dfp>::test_multiply_double();
	}

	#[test]
	pub fn test_divide_field() {
		ExtendedFieldElementAbstractTest::<Dfp>::test_divide_field();
	}

		#[test]
	pub fn test_divide_double() {
		ExtendedFieldElementAbstractTest::<Dfp>::test_divide_double();
	}
	
		#[test]
	pub fn test_cosh() {
		ExtendedFieldElementAbstractTest::<Dfp>::test_cosh();
	}
		#[test]
	pub fn test_sinh() {
		ExtendedFieldElementAbstractTest::<Dfp>::test_sinh();
	}
		#[test]
	pub fn test_tanh() {
		ExtendedFieldElementAbstractTest::<Dfp>::test_tanh();
	}
		#[test]
	pub fn test_sqrt() {
		ExtendedFieldElementAbstractTest::<Dfp>::test_sqrt();
	}
		#[test]
	pub fn test_exp() {
		ExtendedFieldElementAbstractTest::<Dfp>::test_exp();
	}
		#[test]
	pub fn test_expm1() {
		ExtendedFieldElementAbstractTest::<Dfp>::test_expm1();
	}
		#[test]
	pub fn test_log() {
		ExtendedFieldElementAbstractTest::<Dfp>::test_log();
	}
		#[test]
	pub fn test_abs() {
		ExtendedFieldElementAbstractTest::<Dfp>::test_abs();
	}
		#[test]
	pub fn test_ceil() {
		ExtendedFieldElementAbstractTest::<Dfp>::test_ceil();
	}
		#[test]
	pub fn test_floor() {
		ExtendedFieldElementAbstractTest::<Dfp>::test_floor();
	}
		#[test]
	pub fn test_rint() {
		ExtendedFieldElementAbstractTest::<Dfp>::test_rint();
	}
		#[test]
	pub fn test_round() {
		ExtendedFieldElementAbstractTest::<Dfp>::test_round();
	}
		#[test]
	pub fn test_scalb() {
		ExtendedFieldElementAbstractTest::<Dfp>::test_scalb();
	}


}


