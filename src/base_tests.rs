
use base;
use util::fastmath;

pub fn check_relative<T: base::RealFieldElement<T>>(expected: f64, obtained: &T) {
    // assert_within_delta!(expected, obtained.get_real(), );
    if fastmath::F64::abs(expected - obtained.get_real()) >
       1.0e-15 * (1.0 + fastmath::F64::abs(expected)) {
        panic!("check_relative: {} {} ", expected, obtained.get_real())
    }
}




pub trait ExtendedFieldElementAbstractTest<T: base::RealFieldElement<T>> {
    fn build(&self, x: f64) -> T;

    fn test_add_field(&self) {
        {
            let mut x: f64 = -3.0;
            while x < 3.0 {
                {
                    {
                        let mut y: f64 = -3.0;
                        while y < 3.0 {
                            {
                                check_relative(x + y, &self.build(x).add(&self.build(y)));
                            }
                            y += 0.2;
                        }
                    }

                }
                x += 0.2;
            }
        }

    }


    fn test_add_double(&self) {
        {
            let mut x: f64 = -3.0;
            while x < 3.0 {
                {
                    {
                        let mut y: f64 = -3.0;
                        while y < 3.0 {
                            {
                                check_relative(x + y, &self.build(x).add_f64(y));
                            }
                            y += 0.2;
                        }
                    }

                }
                x += 0.2;
            }
        }

    }


    fn test_subtract_field(&self) {
        {
            let mut x: f64 = -3.0;
            while x < 3.0 {
                {
                    {
                        let mut y: f64 = -3.0;
                        while y < 3.0 {
                            {
                                check_relative(x - y, &self.build(x).subtract(&self.build(y)));
                            }
                            y += 0.2;
                        }
                    }

                }
                x += 0.2;
            }
        }

    }


    fn test_subtract_double(&self) {
        {
            let mut x: f64 = -3.0;
            while x < 3.0 {
                {
                    {
                        let mut y: f64 = -3.0;
                        while y < 3.0 {
                            {
                                check_relative(x - y, &self.build(x).subtract_f64(y));
                            }
                            y += 0.2;
                        }
                    }

                }
                x += 0.2;
            }
        }

    }


    fn test_multiply_field(&self) {
        {
            let mut x: f64 = -3.0;
            while x < 3.0 {
                {
                    {
                        let mut y: f64 = -3.0;
                        while y < 3.0 {
                            {
                                check_relative(x * y, &self.build(x).multiply(&self.build(y)));
                            }
                            y += 0.2;
                        }
                    }

                }
                x += 0.2;
            }
        }

    }


    fn test_multiply_double(&self) {
        {
            let mut x: f64 = -3.0;
            while x < 3.0 {
                {
                    {
                        let mut y: f64 = -3.0;
                        while y < 3.0 {
                            {
                                check_relative(x * y, &self.build(x).multiply_f64(y));
                            }
                            y += 0.2;
                        }
                    }

                }
                x += 0.2;
            }
        }

    }


    fn test_multiply_int(&self) {
        {
            let mut x: f64 = -3.0;
            while x < 3.0 {
                {
                    {
                        let mut y: i32 = -10;
                        while y < 10 {
                            {
                                check_relative(x * y as f64, &self.build(x).multiply_i32(y));
                            }
                            y += 1;
                        }
                    }

                }
                x += 0.2;
            }
        }

    }


    fn test_divide_field(&self) {
        {
            let mut x: f64 = -3.0;
            while x < 3.0 {
                {
                    {
                        let mut y: f64 = -3.0;
                        while y < 3.0 {
                            {
                                check_relative(x / y,
                                               &self.build(x).divide(&self.build(y)).unwrap());
                            }
                            y += 0.2;
                        }
                    }

                }
                x += 0.2;
            }
        }

    }


    fn test_divide_double(&self) {
        {
            let mut x: f64 = -3.0;
            while x < 3.0 {
                {
                    {
                        let mut y: f64 = -3.0;
                        while y < 3.0 {
                            {
                                check_relative(x / y, &self.build(x).divide_f64(y));
                            }
                            y += 0.2;
                        }
                    }

                }
                x += 0.2;
            }
        }

    }


    fn test_remainder_field(&self) {
        {
            let mut x: f64 = -3.0;
            while x < 3.0 {
                {
                    {
                        let mut y: f64 = -3.0;
                        while y < 3.0 {
                            {
                                check_relative(fastmath::F64::ieee_remainder(x, y),
                                               &self.build(x).remainder(&self.build(y)).unwrap());
                            }
                            y += 0.2;
                        }
                    }

                }
                x += 0.2;
            }
        }

    }

    fn test_remainder_double(&self) {
        {
            let mut x: f64 = -3.0;
            while x < 3.0 {
                {
                    {
                        let mut y: f64 = -3.2;
                        while y < 3.2 {
                            {
                                check_relative(fastmath::F64::ieee_remainder(x, y),
                                               &self.build(x).remainder_f64(y));
                            }
                            y += 0.25;
                        }
                    }

                }
                x += 0.2;
            }
        }

    }



    fn test_cos(&self) {
        {
            let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::cos(x), &self.build(x).cos());
                }
                x += 0.05;
            }
        }

    }



    fn test_sin(&self) {
        {
            let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::sin(x), &self.build(x).sin());
                }
                x += 0.05;
            }
        }

    }


    fn test_asin(&self) {
        {
            let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::asin(x), &self.build(x).asin());
                }
                x += 0.05;
            }
        }

    }


    fn test_tan(&self) {
        {
            let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::tan(x), &self.build(x).tan());
                }
                x += 0.05;
            }
        }

    }


    fn test_atan(&self) {
        {
            let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::atan(x), &self.build(x).atan());
                }
                x += 0.05;
            }
        }

    }


    fn test_atan2(&self) {
        {
            let mut x: f64 = -3.0;
            while x < 3.0 {
                {
                    {
                        let mut y: f64 = -3.0;
                        while y < 3.0 {
                            {
                                check_relative(fastmath::F64::atan2(x, y),
                                               &self.build(x).atan2(&self.build(y)).unwrap());
                            }
                            y += 0.2;
                        }
                    }

                }
                x += 0.2;
            }
        }

    }



    fn test_cosh(&self) {
        {
            let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::cosh(x), &self.build(x).cosh());
                }
                x += 0.05;
            }
        }

    }



    fn test_acosh(&self) {
        {
            let mut x: f64 = 1.1;
            while x < 5.0 {
                {
                    check_relative(fastmath::F64::acosh(x), &self.build(x).acosh());
                }
                x += 0.05;
            }
        }

    }


    fn test_sinh(&self) {
        {
            let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::sinh(x), &self.build(x).sinh());
                }
                x += 0.05;
            }
        }

    }


    fn test_asinh(&self) {
        {
            let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::asinh(x), &self.build(x).asinh());
                }
                x += 0.05;
            }
        }

    }


    fn test_tanh(&self) {
        {
            let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::tanh(x), &self.build(x).tanh());
                }
                x += 0.05;
            }
        }

    }


    fn test_atanh(&self) {
        {
            let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::atanh(x), &self.build(x).atanh());
                }
                x += 0.05;
            }
        }

    }


    fn test_sqrt(&self) {
        {
            let mut x: f64 = 0.01;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::sqrt(x), &self.build(x).sqrt());
                }
                x += 0.05;
            }
        }

    }


    fn test_cbrt(&self) {
        {
            let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::cbrt(x), &self.build(x).cbrt());
                }
                x += 0.05;
            }
        }

    }


    fn test_hypot(&self) {
        {
            let mut x: f64 = -3.0;
            while x < 3.0 {
                {
                    {
                        let mut y: f64 = -3.0;
                        while y < 3.0 {
                            {
                                check_relative(fastmath::F64::hypot(x, y),
                                               &self.build(x).hypot(&self.build(y)).unwrap());
                            }
                            y += 0.2;
                        }
                    }

                }
                x += 0.2;
            }
        }

    }


    fn test_root_n(&self) {
        {
            let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    {
                        let mut n: i32 = 1;
                        while n < 5 {
                            {
                                if x < 0.0 {
                                    if n % 2 == 1 {
                                        check_relative(-fastmath::F64::pow(-x, 1.0 / n as f64),
                                                       &self.build(x).root_n(n));
                                    }
                                } else {
                                    check_relative(fastmath::F64::pow(x, 1.0 / n as f64),
                                                   &self.build(x).root_n(n));
                                }
                            }
                            n += 1;
                        }
                    }

                }
                x += 0.05;
            }
        }

    }


    fn test_pow_field(&self) {
        {
            let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    {
                        let mut y: f64 = 0.1;
                        while y < 4.0 {
                            {
                                check_relative(fastmath::F64::pow(x, y),
                                               &self.build(x).pow(&self.build(y)).unwrap());
                            }
                            y += 0.2;
                        }
                    }

                }
                x += 0.05;
            }
        }

    }


    fn test_pow_double(&self) {
        {
            let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    {
                        let mut y: f64 = 0.1;
                        while y < 4.0 {
                            {
                                check_relative(fastmath::F64::pow(x, y), &self.build(x).pow_f64(y));
                            }
                            y += 0.2;
                        }
                    }

                }
                x += 0.05;
            }
        }

    }


    fn test_pow_int(&self) {
        {
            let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    {
                        let mut n: i32 = 0;
                        while n < 5 {
                            {
                                check_relative(fastmath::F64::pow_i32(x, n),
                                               &self.build(x).pow_i32(n));
                            }
                            n += 1;
                        }
                    }

                }
                x += 0.05;
            }
        }

    }


    fn test_exp(&self) {
        {
            let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::exp(x), &self.build(x).exp());
                }
                x += 0.05;
            }
        }

    }


    fn test_expm1(&self) {
        {
            let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::expm1(x), &self.build(x).expm1());
                }
                x += 0.05;
            }
        }

    }


    fn test_log(&self) {
        {
            let mut x: f64 = 0.01;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::log(x), &self.build(x).log());
                }
                x += 0.05;
            }
        }

    }


    fn test_log1p(&self) {
        {
            let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::log1p(x), &self.build(x).log1p());
                }
                x += 0.05;
            }
        }

    }


    //  TODO: add this test in 4.0, as it is not possible to do it in 3.2
    //  due to incompatibility of the return type in the Dfp class
    //    @Test
    //    public void testLog10() {
    //        for (double x = -0.9; x < 0.9; x += 0.05) {
    //            checkRelative(fastmath::F64::log10(x), &self.build(x).log10());
    //        }
    //    }

    fn test_abs(&self) {
        {
            let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::abs(x), &self.build(x).abs());
                }
                x += 0.05;
            }
        }

    }


    fn test_ceil(&self) {
        {
            let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::ceil(x), &self.build(x).ceil());
                }
                x += 0.05;
            }
        }

    }


    fn test_floor(&self) {
        {
            let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::floor(x), &self.build(x).floor());
                }
                x += 0.05;
            }
        }

    }


    fn test_rint(&self) {
        {
            let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::rint(x), &self.build(x).rint());
                }
                x += 0.05;
            }
        }

    }


    fn test_round(&self) {
        {
            let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    assert!(fastmath::F64::round(x) == self.build(x).round());
                }
                x += 0.05;
            }
        }

    }




    fn test_signum(&self) {
        {
            let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    check_relative(fastmath::F64::signum(x), &self.build(x).signum());
                }
                x += 0.05;
            }
        }

    }


    fn test_copy_sign_field(&self) {
        {
            let mut x: f64 = -3.0;
            while x < 3.0 {
                {
                    {
                        let mut y: f64 = -3.0;
                        while y < 3.0 {
                            {
                                check_relative(fastmath::F64::copy_sign(x, y),
                                               &self.build(x).copy_sign(&self.build(y)));
                            }
                            y += 0.2;
                        }
                    }

                }
                x += 0.2;
            }
        }

    }


    fn test_copy_sign_double(&self) {
        {
            let mut x: f64 = -3.0;
            while x < 3.0 {
                {
                    {
                        let mut y: f64 = -3.0;
                        while y < 3.0 {
                            {
                                check_relative(fastmath::F64::copy_sign(x, y),
                                               &self.build(x).copy_sign_f64(y));
                            }
                            y += 0.2;
                        }
                    }

                }
                x += 0.2;
            }
        }

    }


    fn test_scalb(&self) {
        {
            let mut x: f64 = -0.9;
            while x < 0.9 {
                {
                    {
                        let mut n: i32 = -100;
                        while n < 100 {
                            {
                                check_relative(fastmath::F64::scalb(x, n), &self.build(x).scalb(n));
                            }
                            n += 1;
                        }
                    }

                }
                x += 0.05;
            }
        }

    }
}
