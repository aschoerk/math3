
// Licensed to the Apache Software Foundation (ASF) under one or more
// contributor license agreements.  See the NOTICE file distributed with
// this work for additional information regarding copyright ownership.
// The ASF licenses this file to You under the Apache License, Version 2.0
// (the "License"); you may not use this file except in compliance with
// the License.  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
   #![allow(unused_imports)]
use util::math_arrays::*;
use util::test_utils::*;
use util::precision;
use util::fastmath;
use util::fastmath::F64;
use analysis::traits::*;
use analysis::polynomials::polynomial_spline_function::*;
use analysis::polynomials::polynomial_function::*;
use super::spline::*;
use super::traits::*;
use random::bits_stream_generator::*;
use random::well_generator::*;

/** error tolerance for spline interpolator value at knot points */
const KNOT_TOLERANCE: f64 = 1E-14;

/** error tolerance for interpolating polynomial coefficients */
const COEFFICIENT_TOLERANCE: f64 = 1E-14;

/** error tolerance for interpolated values -- high value is from sin test */
const INTERPOLATION_TOLERANCE: f64 = 1E-14;

/**
     * verifies that f(x[i]) = y[i] for i = 0..n-1 where n is common length.
     */
pub fn verify_interpolation(f: &UnivariateFunction, x: &Vec<f64>, y: &Vec<f64>) {
    {
        let mut i: usize = 0;
        while i < x.len() {
            {
                assert_within_delta!(f.value(x[i]), y[i], KNOT_TOLERANCE);
            }
            i += 1;
        }
    }

}

/**
     * Verifies that interpolating polynomials satisfy consistency requirement:
     *    adjacent polynomials must agree through two derivatives at knot points
     */
pub fn verify_consistency(f: &PolynomialSplineFunction, x: &Vec<f64>) {
    let polynomials = f.get_polynomials();
    {
        let mut i: usize = 1;
        while i < x.len() - 2 {
            {
                // evaluate polynomials and derivatives at x[i + 1]
                assert_within_delta!(polynomials[i].value(x[i + 1] - x[i]), polynomials[i + 1].value(0.0), 0.1);
                assert_within_delta!(
                    	polynomials[i].derivative().value(x[i + 1] - x[i]),
                    	polynomials[i + 1].derivative().value(0.0), 0.5);
                assert_within_delta!(
                   	polynomials[i].polynomial_derivative().unwrap().derivative().value(x[i + 1] - x[i]),
                   	polynomials[i + 1].polynomial_derivative().unwrap().derivative().value(0.0), 0.5);
            }
            i += 1;
        }
    }

}


#[test]
pub fn test_interpolate_linear_degenerate_two_segment() {
    let tolerance: f64 = 1e-15;
    let x = vec![0.0, 0.5, 1.0, ];
    let y = vec![0.0, 0.5, 1.0, ];
    let f: PolynomialSplineFunction = (SplineInterpolator {}).interpolate(&x, &y).unwrap();
    verify_interpolation(&f, &x, &y);
    verify_consistency(&f, &x);
    // Verify coefficients using analytical values
    let polynomials: Vec<PolynomialFunction> = f.get_polynomials();
    let mut target = vec![y[0], 1.0, ];
    TestUtils::assert_equals_f64_arrays(&polynomials[0].get_coefficients(),
                                        &target,
                                        COEFFICIENT_TOLERANCE);
    target = vec![y[1], 1.0, ];
    TestUtils::assert_equals_f64_arrays(&polynomials[1].get_coefficients(),
                                        &target,
                                        COEFFICIENT_TOLERANCE);
    // Check interpolation
    assert_within_delta!(0.0, f.value(0.0), tolerance);
    assert_within_delta!(0.4, f.value(0.4), tolerance);
    assert_within_delta!(1.0, f.value(1.0), tolerance);
}


#[test]
pub fn test_interpolate_linear_degenerate_three_segment() {
    let tolerance: f64 = 1e-15;
    let x = vec![0.0, 0.5, 1.0, 1.5, ];
    let y = vec![0.0, 0.5, 1.0, 1.5, ];

    let f: PolynomialSplineFunction = (SplineInterpolator {}).interpolate(&x, &y).unwrap();
    verify_interpolation(&f, &x, &y);
    // Verify coefficients using analytical values
    let polynomials: Vec<PolynomialFunction> = f.get_polynomials();
    let mut target = vec![y[0], 1.0, ];
    TestUtils::assert_equals_f64_arrays(&polynomials[0].get_coefficients(),
                                        &target,
                                        COEFFICIENT_TOLERANCE);
    target = vec![y[1], 1.0, ];
    TestUtils::assert_equals_f64_arrays(&polynomials[1].get_coefficients(),
                                        &target,
                                        COEFFICIENT_TOLERANCE);
    target = vec![y[2], 1.0, ];
    TestUtils::assert_equals_f64_arrays(&polynomials[2].get_coefficients(),
                                        &target,
                                        COEFFICIENT_TOLERANCE);
    // Check interpolation
    assert_within_delta!(0.0, f.value(0.0), tolerance);
    assert_within_delta!(1.4, f.value(1.4), tolerance);
    assert_within_delta!(1.5, f.value(1.5), tolerance);
}


#[test]
pub fn test_interpolate_linear() {
    let x = vec![0.0, 0.5, 1.0, ];
    let y = vec![0.0, 0.5, 0.0, ];
    let f: PolynomialSplineFunction = (SplineInterpolator {}).interpolate(&x, &y).unwrap();
    verify_interpolation(&f, &x, &y);
    verify_consistency(&f, &x);
    // Verify coefficients using analytical values
    let polynomials: Vec<PolynomialFunction> = f.get_polynomials();
    let mut target = vec![y[0], 1.5, 0.0, -2.0, ];
    TestUtils::assert_equals_f64_arrays(&polynomials[0].get_coefficients(),
                                        &target,
                                        COEFFICIENT_TOLERANCE);
    target = vec![y[1], 0.0, -3.0, 2.0, ];
    TestUtils::assert_equals_f64_arrays(&polynomials[1].get_coefficients(),
                                        &target,
                                        COEFFICIENT_TOLERANCE);
}



#[test]
pub fn test_interpolate_sin() {
    let sine_coefficient_tolerance: f64 = 1e-6;
    let sine_interpolation_tolerance: f64 = 0.0043;
    let x = vec![0.0, fastmath::PI / 6.0, fastmath::PI / 2.0, 5.0 * fastmath::PI / 6.0, fastmath::PI, 7.0 * fastmath::PI / 6.0, 3.0 * fastmath::PI / 2.0, 11.0 * fastmath::PI / 6.0, 2.0 * fastmath::PI, ]
        ;
    let y = vec![0.0, 0.5, 1.0, 0.5, 0.0, -0.5, -1.0, -0.5, 0.0, ];
    let f: PolynomialSplineFunction = (SplineInterpolator {}).interpolate(&x, &y).unwrap();
    verify_interpolation(&f, &x, &y);
    verify_consistency(&f, &x);
    // Check coefficients against values computed using R (version 1.8.1, Red Hat Linux 9)
    //
    // To replicate in R:
    //     x[1] <- 0
    //     x[2] <- pi / 6, etc, same for y[] (could use y <- scan() for y values)
    //     g <- splinefun(x, y, "natural")
    //     splinecoef <- eval(expression(z), envir = environment(g))
    //     print(splinecoef)
    //
    let polynomials: Vec<PolynomialFunction> = f.get_polynomials();
    let mut target = vec![y[0], 1.002676, 0.0, -0.17415829, ];
    TestUtils::assert_equals_f64_arrays(&polynomials[0].get_coefficients(),
                                        &target,
                                        sine_coefficient_tolerance);
    target = vec![y[1], 8.594367e-01, -2.735672e-01, -0.08707914, ];
    TestUtils::assert_equals_f64_arrays(&polynomials[1].get_coefficients(),
                                        &target,
                                        sine_coefficient_tolerance);
    target = vec![y[2], 1.471804e-17, -5.471344e-01, 0.08707914, ];
    TestUtils::assert_equals_f64_arrays(&polynomials[2].get_coefficients(),
                                        &target,
                                        sine_coefficient_tolerance);
    target = vec![y[3], -8.594367e-01, -2.735672e-01, 0.17415829, ];
    TestUtils::assert_equals_f64_arrays(&polynomials[3].get_coefficients(),
                                        &target,
                                        sine_coefficient_tolerance);
    target = vec![y[4], -1.002676, 6.548562e-17, 0.17415829, ];
    TestUtils::assert_equals_f64_arrays(&polynomials[4].get_coefficients(),
                                        &target,
                                        sine_coefficient_tolerance);
    target = vec![y[5], -8.594367e-01, 2.735672e-01, 0.08707914, ];
    TestUtils::assert_equals_f64_arrays(&polynomials[5].get_coefficients(),
                                        &target,
                                        sine_coefficient_tolerance);
    target = vec![y[6], 3.466465e-16, 5.471344e-01, -0.08707914, ];
    TestUtils::assert_equals_f64_arrays(&polynomials[6].get_coefficients(),
                                        &target,
                                        sine_coefficient_tolerance);
    target = vec![y[7], 8.594367e-01, 2.735672e-01, -0.17415829, ];
    TestUtils::assert_equals_f64_arrays(&polynomials[7].get_coefficients(),
                                        &target,
                                        sine_coefficient_tolerance);
    // Check interpolation
    assert_within_delta!(F64::sqrt(2.0) / 2.0, f.value(fastmath::PI / 4.0), sine_interpolation_tolerance);
    assert_within_delta!(F64::sqrt(2.0) / 2.0, f.value(3.0 * fastmath::PI / 4.0), sine_interpolation_tolerance);
}



#[test]
pub fn test_illegal_arguments() {

    let xval = vec![0.0, 1.0, ];
    let yval = vec![0.0, 1.0, 2.0, ];
    assert!((SplineInterpolator {}).interpolate(&xval, &yval).is_err());

    let xval = vec![0.0, 1.0, 0.5, ];
    let yval = vec![0.0, 1.0, 2.0, ];
    assert!((SplineInterpolator {}).interpolate(&xval, &yval).is_err());


    let xval = vec![0.0, 1.0, ];
    let yval = vec![0.0, 1.0, ];
    assert!((SplineInterpolator {}).interpolate(&xval, &yval).is_err());


}


 #[test]
    pub fn  test_akima_illegal_arguments()   {
        // Data set arrays of different size.
         let i = AkimaSplineInterpolator::new();
       
             let yval = vec![0.0, 1.0, 2.0, 3.0, 4.0, ]
            ;
            assert!(i.interpolate(null, yval).is_err());
            
             let xval = vec![0.0, 1.0, 2.0, 3.0, ]
            ;
             let yval = vec![0.0, 1.0, 2.0, 3.0, ]
            ;
            assert!(i.interpolate(xval, yval).is_err());;
           
             let xval = vec![0.0, 1.0, 2.0, 3.0, 4.0, ]
            ;
             let yval = vec![0.0, 1.0, 2.0, 3.0, 4.0, 5.0, ]
            ;
            assert!(i.interpolate(xval, yval).is_err());;
            
             let xval = vec![0.0, 1.0, 0.5, 7.0, 3.5, 2.2, 8.0, ]
            ;
             let yval = vec![0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, ]
            ;
            assert!(i.interpolate(xval, yval).is_err());;
            

    }

    /*
     * Interpolate a straight line. <p> y = 2 x - 5 <p> Tolerances determined by performing same calculation using
     * Math.NET over ten runs of 100 random number draws for the same function over the same span with the same number
     * of elements
     */
    #[test]
    pub fn  test_akima_interpolate_line()   {
         let number_of_elements: i32 = 10;
         let minimum_x: f64 = -10;
         let maximum_x: f64 = 10;
         let number_of_samples: i32 = 100;
         let interpolation_tolerance: f64 = 1e-15;
         let max_tolerance: f64 = 1e-15;
         let f: UnivariateFunction = UnivariateFunctionContainer::new(Box::new(|x: f64| 2.0 * x - 5.0));
         
         test_akima_interpolation(minimum_x, maximum_x, number_of_elements, number_of_samples, &f as UnivariateFunction, interpolation_tolerance, max_tolerance);
    }

    /*
     * Interpolate a straight line. <p> y = 3 x<sup>2</sup> - 5 x + 7 <p> Tolerances determined by performing same
     * calculation using Math.NET over ten runs of 100 random number draws for the same function over the same span with
     * the same number of elements
     */
    #[test]
    pub fn  test_akima_interpolate_parabola()   {
         let number_of_elements: i32 = 10;
         let minimum_x: f64 = -10;
         let maximum_x: f64 = 10;
         let number_of_samples: i32 = 100;
         let interpolation_tolerance: f64 = 7e-15;
         let max_tolerance: f64 = 6e-14;
         let f: UnivariateFunction = UnivariateFunctionContainer::new(Box::new(|x: f64| (3.0 * x * x) - (5.0 * x) + 7));
         
        test_akima_interpolation(minimum_x, maximum_x, number_of_elements, number_of_samples, &f, interpolation_tolerance, max_tolerance);
    }

    /*
     * Interpolate a straight line. <p> y = 3 x<sup>3</sup> - 0.5 x<sup>2</sup> + x - 1 <p> Tolerances determined by
     * performing same calculation using Math.NET over ten runs of 100 random number draws for the same function over
     * the same span with the same number of elements
     */
    #[test]
    pub fn  test_akima_interpolate_cubic()   {
         let number_of_elements: i32 = 10;
         let minimum_x: f64 = -3;
         let maximum_x: f64 = 3;
         let number_of_samples: i32 = 100;
         let interpolation_tolerance: f64 = 0.37;
         let max_tolerance: f64 = 3.8;
         let f: UnivariateFunction = UnivariateFunctionContainer::new(Box::new(|x: f64| (3.0 * x * x * x) - (0.5 * x * x) + (1.0 * x) - 1));
         
        test_akima_interpolation(minimum_x, maximum_x, number_of_elements, number_of_samples, &f, interpolation_tolerance, max_tolerance);
    }

    fn  test_akima_interpolation(minimum_x: f64,  maximum_x: f64,  number_of_elements: usize,  number_of_samples: usize,  
    	f: &UnivariateFunction,  tolerance: f64,  max_tolerance: f64)   {
         let mut expected: f64;
         let mut actual: f64;
         let current_x: f64;
         let delta: f64 = (maximum_x - minimum_x) / (number_of_elements as f64);
         let x_values = vec![0.0; number_of_elements];
         let y_values = vec![0.0; number_of_elements];
         {
             let mut i: usize = 0;
            while i < number_of_elements {
                {
                    x_values[i] = minimum_x + delta * i as f64;
                    y_values[i] = f.value(x_values[i]);
                }
                i += 1;
             }
         }

         let interpolation = AkimaSplineInterpolator{}.interpolate(&x_values, &y_values).unwrap();
         {
             let mut i: usize = 0;
            while i < number_of_elements {
                {
                    current_x = x_values[i];
                    expected = f.value(current_x);
                    actual = interpolation.value(current_x);
                    assert!(precision::F64::equals(expected, actual));
                }
                i += 1;
             }
         }

        // "tol" depends on the seed.
        let mut rng = WellGeneratorFactory::new_well_19937c();
        rng.set_seed_i32(&1234567);
         
         let dist_x: UniformRealDistribution = UniformRealDistribution::new(&rng, x_values[0], x_values[x_values.len() - 1]);
         let sum_error: f64 = 0.0;
         {
             let mut i: usize = 0;
            while i < number_of_samples {
                {
                    current_x = dist_x.sample();
                    expected = f.value(current_x);
                    actual = interpolation.value(current_x);
                    sum_error += F64::abs(actual - expected);
                    assert_within_delta!(expected, actual, max_tolerance);
                }
                i += 1;
             }
         }

        assert_within_delta!(0.0, (sum_error / number_of_samples as f64), tolerance);
    }

                
