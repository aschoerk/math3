
// Licensed to the Apache Software Foundation (ASF) under one or more
// contributor license agreements.  See the NOTICE file distributed with
// this work for additional information regarding copyright ownership.
// The ASF licenses this file to You under the Apache License, Version 2.0
// (the "License"); you may not use this file except in compliance with
// the License.  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//


use std::rc::*;
use java::exc::*;
use util::math_arrays::*;
use util::fastmath::F64;
use util::precision;
use analysis::polynomials::polynomial_spline_function::*;
use analysis::polynomials::polynomial_function::*;
use analysis::traits::*;
use analysis::interpolation::traits::*;
/**
 * Computes a natural (also known as "free", "unclamped") cubic spline interpolation for the data set.
 * <p>
 * The {@link #interpolate(double[], double[])} method returns a {@link PolynomialSplineFunction}
 * consisting of n cubic polynomials, defined over the subintervals determined by the x values,
 * {@code x[0] < x[i] ... < x[n].}  The x values are referred to as "knot points."
 * <p>
 * The value of the PolynomialSplineFunction at a point x that is greater than or equal to the smallest
 * knot point and strictly less than the largest knot point is computed by finding the subinterval to which
 * x belongs and computing the value of the corresponding polynomial at <code>x - x[i] </code> where
 * <code>i</code> is the index of the subinterval.  See {@link PolynomialSplineFunction} for more details.
 * </p>
 * <p>
 * The interpolating polynomials satisfy: <ol>
 * <li>The value of the PolynomialSplineFunction at each of the input x values equals the
 *  corresponding y value.</li>
 * <li>Adjacent polynomials are equal through two derivatives at the knot points (i.e., adjacent polynomials
 *  "match up" at the knot points, as do their first and second derivatives).</li>
 * </ol>
 * <p>
 * The cubic spline interpolation algorithm implemented is as described in R.L. Burden, J.D. Faires,
 * <u>Numerical Analysis</u>, 4th Ed., 1989, PWS-Kent, ISBN 0-53491-585-X, pp 126-131.
 * </p>
 *
 */
pub struct SplineInterpolator {
}

impl UnivariateInterpolator for SplineInterpolator {
    fn interpolate(&self,
                   xval: &Vec<f64>,
                   yval: &Vec<f64>)
                   -> Result<UnivariateFunctionContainer, Rc<Exception>> {
        let tmp: PolynomialSplineFunction = try!(self.interpolate(xval, yval));
        Ok(UnivariateFunctionContainer::new(Box::new(move |x: f64| tmp.value(x))))
    }
}

impl SplineInterpolator {
    /**
     * Computes an interpolating function for the data set.
     * @param x the arguments for the interpolation points
     * @param y the values for the interpolation points
     * @return a function which interpolates the data set
     * @throws DimensionMismatchException if {@code x} and {@code y}
     * have different sizes.
     * @throws NonMonotonicSequenceException if {@code x} is not sorted in
     * strict increasing order.
     * @throws NumberIsTooSmallException if the size of {@code x} is smaller
     * than 3.
     */
    pub fn interpolate(&self,
                       x: &Vec<f64>,
                       y: &Vec<f64>)
                       -> Result<PolynomialSplineFunction, Rc<Exception>> {
        if x.len() != y.len() {
            return Err(Exc::new_msg("DimensionMismatchException",
                                    format!("mismatch in {} and {}",x.len(), y.len())));
        }
        if x.len() < 3 {
            return Err(Exc::new_msg("NumberIsTooSmallException",
                                    format!("number of points ({0})",x.len())));
        }
        // Number of intervals.  The number of data points is n + 1.
        let n: usize = x.len() - 1;
        try!(MathArrays::check_order_inc_strict(x));
        // Differences between knot points
        let mut h = Vec::new();
        {
            let mut i: usize = 0;
            while i < n {
                {
                    h.push(x[i + 1] - x[i]);
                }
                i += 1;
            }
        }

        let mut mu = Vec::new();
        let mut z = Vec::new();
        mu.push(0.0);
        z.push(0.0);
        let mut g;/*  throws DimensionMismatchException, NumberIsTooSmallException, NonMonotonicSequenceException */
        {
            let mut i: usize = 1;
            while i < n {
                {
                    g = 2.0 * (x[i + 1] - x[i - 1]) - h[i - 1] * mu[i - 1];
                    mu.push(h[i] / g);
                    let tmp = (3.0 *
                               (y[i + 1] * h[i - 1] - y[i] * (x[i + 1] - x[i - 1]) +
                                y[i - 1] * h[i]) /
                               (h[i - 1] * h[i]) -
                               h[i - 1] * z[i - 1]) / g;
                    z.push(tmp);
                }
                i += 1;
            }
        }
        z.push(0.0);

        // cubic spline coefficients --  b is linear, c quadratic, d is cubic (original y's are constants)
        let mut b = vec![0.0; n];
        let mut c = vec![0.0; n + 1];
        let mut d = vec![0.0; n];
        z[n] = 0.0;
        c[n] = 0.0;
        {
            // throws DimensionMismatchException, NumberIsTooSmallException, NonMonotonicSequenceException
            let mut j: usize = n - 1;
            loop {
                {
                    c[j] = z[j] - mu[j] * c[j + 1];
                    b[j] = (y[j + 1] - y[j]) / h[j] - h[j] * (c[j + 1] + 2.0 * c[j]) / 3.0;
                    d[j] = (c[j + 1] - c[j]) / (3.0 * h[j]);
                }
                if j == 0 {
                    break;
                }
                j -= 1;
            }
        }

        let mut polynomials = Vec::new();
        let mut coefficients = vec![0.0; 4];
        {
            let mut i: usize = 0;
            while i < n {
                {
                    coefficients[0] = y[i];
                    coefficients[1] = b[i];
                    coefficients[2] = c[i];
                    coefficients[3] = d[i];
                    let polynomial = try!(PolynomialFunction::new(&coefficients));
                    polynomials.push(polynomial);
                }
                i += 1;
            }
        }

        return PolynomialSplineFunction::new(&x, &polynomials);
    }
}



/**
 * Computes a cubic spline interpolation for the data set using the Akima
 * algorithm, as originally formulated by Hiroshi Akima in his 1970 paper
 * "A New Method of Interpolation and Smooth Curve Fitting Based on Local Procedures."
 * J. ACM 17, 4 (October 1970), 589-602. DOI=10.1145/321607.321609
 * http://doi.acm.org/10.1145/321607.321609
 * <p>
 * This implementation is based on the Akima implementation in the CubicSpline
 * class in the Math.NET Numerics library. The method referenced is
 * CubicSpline.InterpolateAkimaSorted
 * </p>
 * <p>
 * The {@link #interpolate(double[], double[]) interpolate} method returns a
 * {@link PolynomialSplineFunction} consisting of n cubic polynomials, defined
 * over the subintervals determined by the x values, {@code x[0] < x[i] ... < x[n]}.
 * The Akima algorithm requires that {@code n >= 5}.
 * </p>
 */
/** The minimum number of points that are needed to compute the function. */
const AKIMA_MINIMUM_NUMBER_POINTS: usize = 5;

pub struct AkimaSplineInterpolator {
}

impl UnivariateInterpolator for AkimaSplineInterpolator {
    fn interpolate(&self,
                   xval: &Vec<f64>,
                   yval: &Vec<f64>)
                   -> Result<UnivariateFunctionContainer, Rc<Exception>> {
        let tmp: PolynomialSplineFunction = try!(self.interpolate(xval, yval));
        Ok(UnivariateFunctionContainer::new(Box::new(move |x: f64| tmp.value(x))))
    }
}


impl AkimaSplineInterpolator {
    /**
     * Computes an interpolating function for the data set.
     *
     * @param xvals the arguments for the interpolation points
     * @param yvals the values for the interpolation points
     * @return a function which interpolates the data set
     * @throws DimensionMismatchException if {@code xvals} and {@code yvals} have
     *         different sizes.
     * @throws NonMonotonicSequenceException if {@code xvals} is not sorted in
     *         strict increasing order.
     * @throws NumberIsTooSmallException if the size of {@code xvals} is smaller
     *         than 5.
     */
    pub fn interpolate(&self,
                       xvals: &Vec<f64>,
                       yvals: &Vec<f64>)
                       -> Result<PolynomialSplineFunction, Rc<Exception>> {
        if xvals.len() != yvals.len() {
            return Err(Exc::new_msg("DimensionMismatchException",
                                    format!("mismatch in {} and {}",xvals.len(), yvals.len())));
        }
        if xvals.len() < AKIMA_MINIMUM_NUMBER_POINTS {
            return Err(Exc::new_msg("NumberIsTooSmallException", format!("number of points ({}) less than {}",xvals.len(), AKIMA_MINIMUM_NUMBER_POINTS)));
        }

        try!(MathArrays::check_order_inc_strict(&xvals));
        let number_of_diff_and_weight_elements: usize = xvals.len() - 1;
        let mut differences = Vec::new();
        let mut weights = Vec::new();
        {
            let mut i: usize = 0;
            while i < number_of_diff_and_weight_elements {
                {
                    differences.push((yvals[i + 1] - yvals[i]) / (xvals[i + 1] - xvals[i]));
                }
                i += 1;
            }
        }

        {
            let mut i: usize = 1;
            weights.push(0.0);
            while i < number_of_diff_and_weight_elements {
                {
                    weights.push(F64::abs(differences[i] - differences[i - 1]));
                }
                i += 1;
            }
        }

        // Prepare Hermite interpolation scheme.
        let mut first_derivatives = vec![0.0; xvals.len()];
        {
            let mut i: usize = 2;
            while i < first_derivatives.len() - 2 {
                {
                    let w_p: f64 = weights[i + 1];
                    let w_m: f64 = weights[i - 1];
                    if precision::F64::equals(w_p, 0.0) && precision::F64::equals(w_m, 0.0) {
                        let xv: f64 = xvals[i];
                        let xv_p: f64 = xvals[i + 1];
                        let xv_m: f64 = xvals[i - 1];
                        first_derivatives[i] = (((xv_p - xv) * differences[i - 1]) +
                                                ((xv - xv_m) * differences[i])) /
                                               (xv_p - xv_m);
                    } else {
                        first_derivatives[i] =
                            ((w_p * differences[i - 1]) + (w_m * differences[i])) / (w_p + w_m);
                    }
                }
                i += 1;
            }
        }

        first_derivatives[0] = self.differentiate_three_point(&xvals, &yvals, 0, 0, 1, 2);
        first_derivatives[1] = self.differentiate_three_point(&xvals, &yvals, 1, 0, 1, 2);
        first_derivatives[xvals.len() - 2] = self.differentiate_three_point(&xvals,
                                                                            &yvals,
                                                                            xvals.len() - 2,
                                                                            xvals.len() - 3,
                                                                            xvals.len() - 2,
                                                                            xvals.len() - 1);
        first_derivatives[xvals.len() - 1] = self.differentiate_three_point(&xvals,
                                                                            &yvals,
                                                                            xvals.len() - 1,
                                                                            xvals.len() - 3,
                                                                            xvals.len() - 2,
                                                                            xvals.len() - 1);
        return self.interpolate_hermite_sorted(&xvals, &yvals, &first_derivatives);
    }

    /**
     * Three point differentiation helper, modeled off of the same method in the
     * Math.NET CubicSpline class. This is used by both the Apache Math and the
     * Math.NET Akima Cubic Spline algorithms
     *
     * @param xvals x values to calculate the numerical derivative with
     * @param yvals y values to calculate the numerical derivative with
     * @param indexOfDifferentiation index of the elemnt we are calculating the derivative around
     * @param indexOfFirstSample index of the first element to sample for the three point method
     * @param indexOfSecondsample index of the second element to sample for the three point method
     * @param indexOfThirdSample index of the third element to sample for the three point method
     * @return the derivative
     */
    fn differentiate_three_point(&self,
                                 xvals: &Vec<f64>,
                                 yvals: &Vec<f64>,
                                 index_of_differentiation: usize,
                                 index_of_first_sample: usize,
                                 index_of_secondsample: usize,
                                 index_of_third_sample: usize)
                                 -> f64 {
        let x0: f64 = yvals[index_of_first_sample];
        let x1: f64 = yvals[index_of_secondsample];
        let x2: f64 = yvals[index_of_third_sample];
        let t: f64 = xvals[index_of_differentiation] - xvals[index_of_first_sample];
        let t1: f64 = xvals[index_of_secondsample] - xvals[index_of_first_sample];
        let t2: f64 = xvals[index_of_third_sample] - xvals[index_of_first_sample];
        let a: f64 = (x2 - x0 - (t2 / t1 * (x1 - x0))) / (t2 * t2 - t1 * t2);
        let b: f64 = (x1 - x0 - a * t1 * t1) / t1;
        return (2.0 * a * t) + b;
    }

    /**
     * Creates a Hermite cubic spline interpolation from the set of (x,y) value
     * pairs and their derivatives. This is modeled off of the
     * InterpolateHermiteSorted method in the Math.NET CubicSpline class.
     *
     * @param xvals x values for interpolation
     * @param yvals y values for interpolation
     * @param firstDerivatives first derivative values of the function
     * @return polynomial that fits the function
     */
    fn interpolate_hermite_sorted(&self,
                                  xvals: &Vec<f64>,
                                  yvals: &Vec<f64>,
                                  first_derivatives: &Vec<f64>)
                                  -> Result<PolynomialSplineFunction, Rc<Exception>> {
        if xvals.len() != yvals.len() {
            return Err(Exc::new_msg("DimensionMismatchException",
                                    format!("mismatch in {} and {}",xvals.len(), yvals.len())));
        }
        if xvals.len() != first_derivatives.len() {
            return Err(Exc::new_msg("DimensionMismatchException", format!("mismatch: {} should be first_derivatives_len {}",xvals.len(), first_derivatives.len())));
        }
        let minimum_length: usize = 2;
        if xvals.len() < minimum_length {
            return Err(Exc::new_msg("NumberIsTooSmallException", format!("number of points ({}) less than {}",xvals.len(), minimum_length)));
        }
        let size: usize = xvals.len() - 1;
        let mut polynomials = Vec::new();
        let mut coefficients = vec![0.0; 4];
        {
            let mut i: usize = 0;
            while i < size {
                {
                    let w: f64 = xvals[i + 1] - xvals[i];
                    let w2: f64 = w * w;
                    let yv: f64 = yvals[i];
                    let yv_p: f64 = yvals[i + 1];
                    let fd: f64 = first_derivatives[i];
                    let fd_p: f64 = first_derivatives[i + 1];
                    coefficients[0] = yv;
                    coefficients[1] = first_derivatives[i];
                    coefficients[2] = (3.0 * (yv_p - yv) / w - 2.0 * fd - fd_p) / w;
                    coefficients[3] = (2.0 * (yv - yv_p) / w + fd + fd_p) / w2;
                    polynomials.push(try!(PolynomialFunction::new(&coefficients)));
                }
                i += 1;
            }
        }

        return PolynomialSplineFunction::new(&xvals, &polynomials);
    }
}
