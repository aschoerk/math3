use std::rc::*;
use java::exc::*;
use analysis::traits::*;

pub trait UnivariateInterpolator {
    /**
     * Compute an interpolating function for the dataset.
     *
     * @param xval Arguments for the interpolation points.
     * @param yval Values for the interpolation points.
     * @return a function which interpolates the dataset.
     * @throws MathIllegalArgumentException
     * if the arguments violate assumptions made by the interpolation
     * algorithm.
     * @throws DimensionMismatchException if arrays lengthes do not match
     */
    fn interpolate(&self,
                   xval: &Vec<f64>,
                   yval: &Vec<f64>)
                   -> Result<UnivariateFunctionContainer, Rc<Exception>>;
}

pub trait MultivariateInterpolator {
    /**
     * Computes an interpolating function for the data set.
     *
     * @param xval the arguments for the interpolation points.
     * {@code xval[i][0]} is the first component of interpolation point
     * {@code i}, {@code xval[i][1]} is the second component, and so on
     * until {@code xval[i][d-1]}, the last component of that interpolation
     * point (where {@code d} is thus the dimension of the space).
     * @param yval the values for the interpolation points
     * @return a function which interpolates the data set
     * @throws MathIllegalArgumentException if the arguments violate assumptions
     * made by the interpolation algorithm.
     * @throws DimensionMismatchException when the array dimensions are not consistent.
     * @throws NoDataException if an array has zero-length.
     * @throws NullArgumentException if the arguments are {@code null}.
     */
    fn interpolate(&self,
                   xval: &Vec<Vec<f64>>,
                   yval: &Vec<f64>)
                   -> Result<MultivariateFunctionContainer, Rc<Exception>>;
}
