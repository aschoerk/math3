                
                   /*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * Test for {@link function_utils}.
 */
use util::fastmath::F64;
use util::fastmath;
use assert;
use std::rc::*;
use std::f64;
use analysis::functions::*;
use analysis::function_utils;
use analysis::traits::*;
use analysis::differentiation::derivative_structure::*;
use random::bits_stream_generator::*;
use random::mersenne_twister::*;

fn geteps1() -> f64 {
	return F64::ulp(1.0);
}

fn create_generator() -> MersenneTwister {
   // Box::new(MersenneTwister::new_i64(&6176597458463500194))
   MersenneTwister::new_i64(&6176597458463500194)
}

/*

#[test]
pub fn  test_compose()   {
     let id= Box::new(Identity{});
    assert_within_delta!(3.0, function_utils::compose(vec![id.clone(), id.clone(), id.clone()]).value(3.0), geteps1());

    
     let c= Box::new(Constant::new(4.0));
    assert_within_delta!(4.0, function_utils::compose(vec![id.clone(), c.clone()]).value(3.0), geteps1());
    assert_within_delta!(4.0, function_utils::compose(vec![c.clone(), id.clone()]).value(3.0), geteps1());
     let m= Box::new(Minus{});
    assert_within_delta!(-3.0, function_utils::compose(vec![m.clone()]).value(3.0), geteps1());
    assert_within_delta!(3.0, function_utils::compose(vec![m.clone(), m.clone()]).value(3.0), geteps1());
     let inv= Box::new(Inverse {});
    assert_within_delta!(-0.25, function_utils::compose(vec![inv.clone(), m.clone(), c.clone(), id.clone()]).value(3.0), geteps1());
     let pow= Box::new(Power::new(2.0));
    assert_within_delta!(81.0, function_utils::compose(vec![pow.clone(), pow.clone()]).value(3.0), geteps1());
    
}


#[test]
pub fn  test_compose_differentiable()   {
     let id= Rc::new(Identity {});
    assert_within_delta!(1.0, function_utils::compose_ds(vec![id.clone(), id.clone(), id.clone()]).value_ds(&DerivativeStructure::new(1, 1, 0, 3.0).unwrap()).unwrap().get_partial_derivative(&vec![1]).unwrap(), geteps1());
     let c= Rc::new(Constant::new(4.0));
    assert_within_delta!(0.0, function_utils::compose_ds(vec![id.clone(), c.clone()]).value_ds(&DerivativeStructure::new(1, 1, 0, 3.0).unwrap()).unwrap().get_partial_derivative(&vec![1]).unwrap(), geteps1());
    assert_within_delta!(0.0, function_utils::compose_ds(vec![c.clone(), id.clone()]).value_ds(&DerivativeStructure::new(1, 1, 0, 3.0).unwrap()).unwrap().get_partial_derivative(&vec![1]).unwrap(), geteps1());
     let m= Rc::new(Minus {});
    assert_within_delta!(-1.0, function_utils::compose_ds(vec![m.clone()]).value_ds(&DerivativeStructure::new(1, 1, 0, 3.0).unwrap()).unwrap().get_partial_derivative(&vec![1]).unwrap(), geteps1());
    assert_within_delta!(1.0, function_utils::compose_ds(vec![m.clone(), m.clone()]).value_ds(&DerivativeStructure::new(1, 1, 0, 3.0).unwrap()).unwrap().get_partial_derivative(&vec![1]).unwrap(), geteps1());
     let inv= Rc::new(Inverse {});
    assert_within_delta!(0.25, function_utils::compose_ds(vec![inv.clone(), m.clone(), id.clone()]).value_ds(&DerivativeStructure::new(1, 1, 0, 2.0).unwrap()).unwrap().get_partial_derivative(&vec![1]).unwrap(), geteps1());
     let pow= Rc::new(Power::new(2.0));
    assert_within_delta!(108.0, function_utils::compose_ds(vec![pow.clone(), pow.clone()]).value_ds(&DerivativeStructure::new(1, 1, 0, 3.0).unwrap()).unwrap().get_partial_derivative(&vec![1]).unwrap(), geteps1());
     let log= Rc::new(Log {});
     let a: f64 = 9876.54321;
    assert_within_delta!(pow.value_ds(&DerivativeStructure::new(1, 1, 0, a).unwrap()).unwrap().get_partial_derivative(&vec![1]).unwrap() / pow.clone().value(a), 
    		function_utils::compose_ds(vec![log.clone(), pow.clone()]).value_ds(&DerivativeStructure::new(1, 1, 0, a).unwrap()).unwrap().get_partial_derivative(&vec![1]).unwrap(), geteps1());
}


#[test]
pub fn  test_add()   {
     let id = Box::new( Identity {});
     let c= Box::new( Constant::new(4.0));
     let m= Box::new(  Minus {});
     let inv = Box::new( Inverse {});
    assert_within_delta!(4.5, function_utils::add(vec![inv.clone(), m.clone(), c.clone(), id.clone()]).value(2.0), geteps1());
    assert_within_delta!(4.0 + 2.0, function_utils::add(vec![c.clone(), id.clone()]).value(2.0), geteps1());
    assert_within_delta!(4.0 - 2.0, function_utils::add(vec![c.clone(), Box::new(function_utils::compose(vec![m.clone(), id.clone()]))]).value(2.0), geteps1());
}

#[test]
pub fn  test_add_differentiable()   {
     let sin= Rc::new(Sin {});
     let c= Rc::new(Constant::new(4.0));
     let m= Rc::new(Minus {});
     let inv= Rc::new(Inverse {});
     let a: f64 = 123.456;
    assert_within_delta!(-1.0 / (a * a) - 1.0 + F64::cos(a), 
    	function_utils::add_ds(vec![inv, m, c, sin])
    	.value_ds(&DerivativeStructure::new(1, 1, 0, a).unwrap()).unwrap()
    	.get_partial_derivative(&vec![1]).unwrap(), geteps1());
}



#[test]
pub fn  test_multiply()   {
     let c= Box::new(Constant::new(4.0));
    assert_within_delta!(16.0, function_utils::multiply(vec![c.clone(), c.clone()]).value(12345.0), geteps1());
     let inv= Box::new(Inverse {});
     let pow= Box::new(Power::new(2.0));
    assert_within_delta!(1.0, function_utils::multiply(vec![Box::new(function_utils::compose(vec![inv, pow.clone()])), pow]).value(3.5), geteps1());
}
 


#[test]
pub fn  test_multiply_differentiable()   {
     let c= Rc::new(Constant::new(4.0));
     let id= Rc::new(Identity {});
     let a: f64 = 1.2345678;
    assert_within_delta!(8.0 * a, function_utils::multiply_ds(vec![c.clone(), id.clone(), id.clone()])
    	.value_ds(&DerivativeStructure::new(1, 1, 0, a).unwrap()).unwrap()
    	.get_partial_derivative(&vec![1]).unwrap(), 
    	geteps1());
     let inv= Rc::new(Inverse {});
     let pow= Rc::new(Power::new(2.5));
     let cos= Rc::new(Cos {});
    assert_within_delta!(1.5 * F64::sqrt(a) * F64::cos(a) - F64::pow(a, 1.5) * F64::sin(a), 
    	function_utils::multiply_ds(vec![inv.clone(), pow.clone(), cos])
    	.value_ds(&DerivativeStructure::new(1, 1, 0, a).unwrap()).unwrap()
    	.get_partial_derivative(&vec![1]).unwrap(), 
    	geteps1());
     let cosh= Rc::new(Cosh {});
    assert_within_delta!(1.5 * F64::sqrt(a) * F64::cosh(a) + F64::pow(a, 1.5) * F64::sinh(a), 
    	function_utils::multiply_ds(vec![inv.clone(), pow.clone(), cosh])
    	.value_ds(&DerivativeStructure::new(1, 1, 0, a).unwrap()).unwrap()
    	.get_partial_derivative(&vec![1]).unwrap(), 8.0 * 
    	geteps1());
}
*/

#[test]
pub fn  test_combine()   {
     let mut bi = Box::new(Add {});
     let id = Box::new(Identity {});
     let m = Box::new( Minus {});
     let mut c: UnivariateFunctionContainer = function_utils::combine(bi, id.clone(), m);
    assert_within_delta!(0.0, c.value(2.3456), geteps1());
    let bi2 = Box::new(Multiply {});
     let inv = Box::new(Inverse {});
    c = function_utils::combine(bi2, id, inv);
    assert_within_delta!(1.0, c.value(2.3456), geteps1());
}


#[test]
pub fn  test_collector()   {
     let mut bi = Box::new(Add {});
     let mut coll: MultivariateFunctionContainer = function_utils::collector(bi, 0.0);
    assert_within_delta!(10.0, coll.value(&vec![1.0, 2.0, 3.0, 4.0, ]), geteps1());
    let mut bi = Box::new(Multiply {});
    coll = function_utils::collector(bi, 1.0);
    assert_within_delta!(24.0, coll.value(&vec![1.0, 2.0, 3.0, 4.0, ]), geteps1());
    let mut bi = Box::new(Max {});
    coll = function_utils::collector(bi, f64::NEG_INFINITY);
    assert_within_delta!(10.0, coll.value(&vec![1.0, -2.0, 7.5, 10.0, -24.0, 9.99, ]), 0.0);
    let mut bi = Box::new(Min {});
    coll = function_utils::collector(bi, f64::INFINITY);
    assert_within_delta!(-24.0, coll.value(&vec![1.0, -2.0, 7.5, 10.0, -24.0, 9.99, ]), 0.0);

}


#[test]
pub fn  test_sinc()   {
     let div = Box::new(Divide {});
     let sin = Box::new(Sin {});
     let id = Box::new(Identity {});
     let sinc1 = Box::new(function_utils::combine(div, sin, id));
     let sinc2 = Box::new(Sinc::new_non_normalized());
     {
         let mut random_gen = create_generator();
         let mut i: usize = 0;
        while i < 10 {
            {
                 let x: f64 = random_gen.next_double();
                assert_within_delta!(sinc1.value(x), sinc2.value(x), geteps1());
            }
            i += 1;
         }
     }

}


#[test]
pub fn  test_fixing_arguments()   {
     let scaler = function_utils::fix1st_argument(Box::new(Multiply {}), 10.0);
    assert_within_delta!(1.23456, scaler.value(0.123456), geteps1());
     let pow1 = Box::new(Power::new(2.0));
     let pow2 = Box::new (function_utils::fix2nd_argument(Box::new(Pow {}), 2.0));
     {
         let mut random_gen = create_generator();
         let mut i: i32 = 0;
        while i < 10 {
            {
                 let x: f64 = random_gen.next_double() * 10.0;
                assert_within_delta!(pow1.value(x), pow2.value(x), 0.0);
            }
            i += 1;
         }
     }

}


#[test]
pub fn  test_sample_wrong_bounds()   {
    assert!(function_utils::sample(Box::new(Sin {}), fastmath::PI, 0.0, 10).is_err());
}


#[test]
pub fn  test_sample_null_number_of_points()   {
    assert!(function_utils::sample(Box::new(Sin {}), 0.0, fastmath::PI, 0).is_err());
}

#[test]
pub fn  test_sample()   {
     let n: usize = 11;
     let min: f64 = 0.0;
     let max: f64 = fastmath::PI;
     let actual: Vec<f64> = function_utils::sample(Box::new(Sin {}), min, max, n).unwrap();
     for i in 0..n {
             let x: f64 = min + (max - min) / n as f64 * i as f64;
            assert_within_delta!(F64::sin(x), actual[i], 0.0, format!("x = {}", x));
     }
}

                   /*
#[test]
pub fn  test_to_differentiable_univariate_function()   {
    // Sin implements both UnivariateDifferentiableFunction and DifferentiableUnivariateFunction
     let sin: Sin = Sin {};
     let converted: DifferentiableUnivariateFunction = function_utils::to_differentiable_univariate_function(&sin);
     {
         let mut x: f64 = 0.1;
        while x < 0.5 {
            {
                assert_within_delta!(&sin.value(x), &converted.value(x), 1.0e-10);
                assert_within_delta!(&sin.derivative().value(x), &converted.derivative().value(x), 1.0e-10);
            }
            x += 0.01;
         }
     }

}

#[test]
pub fn  test_to_univariate_differential()   {
    // Sin implements both UnivariateDifferentiableFunction and DifferentiableUnivariateFunction
     let sin: Sin = Sin {};
     let converted= Box::new(function_utils::to_univariate_differential(&sin));
     {
         let mut x: f64 = 0.1;
        while x < 0.5 {
            {
                 let t: DerivativeStructure = DerivativeStructure::new(2.0, 1.0, x, 1.0, 2.0);
                assert_within_delta!(&sin.value(&t).get_value(), &converted.value(&t).get_value(), 1.0e-10);
                assert_within_delta!(&sin.value(&t).get_partial_derivative(1, 0), &converted.value(&t).get_partial_derivative(1, 0), 1.0e-10);
                assert_within_delta!(&sin.value(&t).get_partial_derivative(0, 1), &converted.value(&t).get_partial_derivative(0, 1), 1.0e-10);
            }
            x += 0.01;
         }
     }

}

#[test]
pub fn  test_to_differentiable_multivariate_function()   {
     let hypot: MultivariateDifferentiableFunction = MultivariateDifferentiableFunction {} {

        pub fn  value(&self,  point: &Vec<f64>) -> f64  {
            return F64::hypot(point[0], point[1]);
        }

        pub fn  value(&self,  point: &Vec<DerivativeStructure>) -> DerivativeStructure  {
            return DerivativeStructure::hypot(point[0], point[1]);
        }
    };
     let converted: DifferentiableMultivariateFunction = function_utils::to_differentiable_multivariate_function(&hypot);
     {
         let mut x: f64 = 0.1;
        while x < 0.5 {
            {
                 {
                     let mut y: f64 = 0.1;
                    while y < 0.5 {
                        {
                             let point : vec![f64; 2] = vec![x, y, ]
                            ;
                            assert_within_delta!(&hypot.value(&point), &converted.value(&point), 1.0e-10);
                            assert_within_delta!(x / hypot.value(&point), converted.gradient().value(&point)[0], 1.0e-10);
                            assert_within_delta!(y / hypot.value(&point), converted.gradient().value(&point)[1], 1.0e-10);
                        }
                        y += 0.01;
                     }
                 }

            }
            x += 0.01;
         }
     }

}

#[test]
pub fn  test_to_multivariate_differentiable_function()   {
     let hypot: DifferentiableMultivariateFunction = DifferentiableMultivariateFunction {} {

        pub fn  value(&self,  point: &Vec<f64>) -> f64  {
            return F64::hypot(point[0], point[1]);
        }

        pub fn  partial_derivative(&self,  k: i32) -> MultivariateFunction  {
            return MultivariateFunction {} {

                pub fn  value(&self,  point: &Vec<f64>) -> f64  {
                    return point[k] / F64::hypot(point[0], point[1]);
                }
            };
        }

        pub fn  gradient(&self) -> MultivariateVectorFunction  {
            return MultivariateVectorFunction {} {

                pub fn  value(&self,  point: &Vec<f64>) -> Vec<f64>  {
                     let h: f64 = F64::hypot(point[0], point[1]);
                    return  : vec![f64; 2] = vec![point[0] / h, point[1] / h, ]
                    ;
                }
            };
        }
    };
     let converted: MultivariateDifferentiableFunction = function_utils::to_multivariate_differentiable_function(&hypot);
     {
         let mut x: f64 = 0.1;
        while x < 0.5 {
            {
                 {
                     let mut y: f64 = 0.1;
                    while y < 0.5 {
                        {
                             let t : vec![DerivativeStructure; 2] = vec![DerivativeStructure::new(3.0, 1.0, x, 1.0, 2.0, 3.0), DerivativeStructure::new(3.0, 1.0, y, 4.0, 5.0, 6.0), ]
                            ;
                             let h: DerivativeStructure = DerivativeStructure::hypot(t[0], t[1]);
                            assert_within_delta!(&h.get_value(), &converted.value(&t).get_value(), 1.0e-10);
                            assert_within_delta!(&h.get_partial_derivative(1, 0, 0), &converted.value(&t).get_partial_derivative(1, 0, 0), 1.0e-10);
                            assert_within_delta!(&h.get_partial_derivative(0, 1, 0), &converted.value(&t).get_partial_derivative(0, 1, 0), 1.0e-10);
                            assert_within_delta!(&h.get_partial_derivative(0, 0, 1), &converted.value(&t).get_partial_derivative(0, 0, 1), 1.0e-10);
                        }
                        y += 0.01;
                     }
                 }

            }
            x += 0.01;
         }
     }

}


*/
                
                