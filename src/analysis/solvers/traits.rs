                
                   /*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/** Default relative accuracy. */
 const DEFAULT_RELATIVE_ACCURACY: f64 = 1e-14;

/** Default function value accuracy. */
 const DEFAULT_FUNCTION_VALUE_ACCURACY: f64 = 1e-15;
 
 trait SolverAccuracy {
 	 /**
     * Get the absolute accuracy of the solver.  Solutions returned by the
     * solver should be accurate to this tolerance, i.e., if ε is the
     * absolute accuracy of the solver and {@code v} is a value returned by
     * one of the {@code solve} methods, then a root of the function should
     * exist somewhere in the interval ({@code v} - ε, {@code v} + ε).
     *
     * @return the absolute accuracy.
     */
    fn  get_absolute_accuracy(&self) -> f64 ;

    /**
     * Get the relative accuracy of the solver.  The contract for relative
     * accuracy is the same as {@link #getAbsoluteAccuracy()}, but using
     * relative, rather than absolute error.  If ρ is the relative accuracy
     * configured for a solver and {@code v} is a value returned, then a root
     * of the function should exist somewhere in the interval
     * ({@code v} - ρ {@code v}, {@code v} + ρ {@code v}).
     *
     * @return the relative accuracy.
     */
    fn  get_relative_accuracy(&self) -> f64 ;

    /**
     * Get the function value accuracy of the solver.  If {@code v} is
     * a value returned by the solver for a function {@code f},
     * then by contract, {@code |f(v)|} should be less than or equal to
     * the function value accuracy configured for the solver.
     *
     * @return the function value accuracy.
     */
    fn  get_function_value_accuracy(&self) -> f64 ;

 }
 
 trait SolverEvaluations {
 	/**
     * Get the maximum number of function evaluations.
     *
     * @return the maximum number of function evaluations.
     */
    fn  get_max_evaluations(&self) -> i32 ;

    /**
     * Get the number of evaluations of the objective function.
     * The number of evaluations corresponds to the last call to the
     * {@code optimize} method. It is 0 if the method has not been
     * called yet.
     *
     * @return the number of evaluations of the objective function.
     */
    fn  get_evaluations(&self) -> i32 ;

 }
 
 trait BaseUnivariateSolver<FUNC: UnivariateFunction>: SolverAccuracy + SolverEvaluations {
 	 
   
    /**
     * Solve for a zero root in the given interval.
     * A solver may require that the interval brackets a single zero root.
     * Solvers that do require bracketing should be able to handle the case
     * where one of the endpoints is itself a root.
     *
     * @param maxEval Maximum number of evaluations.
     * @param f Function to solve.
     * @param min Lower bound for the interval.
     * @param max Upper bound for the interval.
     * @return a value where the function is zero.
     * @throws MathIllegalArgumentException
     * if the arguments do not satisfy the requirements specified by the solver.
     * @throws TooManyEvaluationsException if
     * the allowed number of evaluations is exceeded.
     */
    fn  solve_with_limits(&self,  max_eval: i32,  f: &FUNC,  min: f64,  max: f64) -> /*  throws MathIllegalArgumentException, TooManyEvaluationsException */Result<f64, Rc<Exception>>  ;

    /**
     * Solve for a zero in the given interval, start at {@code startValue}.
     * A solver may require that the interval brackets a single zero root.
     * Solvers that do require bracketing should be able to handle the case
     * where one of the endpoints is itself a root.
     *
     * @param maxEval Maximum number of evaluations.
     * @param f Function to solve.
     * @param min Lower bound for the interval.
     * @param max Upper bound for the interval.
     * @param startValue Start value to use.
     * @return a value where the function is zero.
     * @throws MathIllegalArgumentException
     * if the arguments do not satisfy the requirements specified by the solver.
     * @throws TooManyEvaluationsException if
     * the allowed number of evaluations is exceeded.
     */
    fn  solve_with_limits_and_startvalue(&self,  max_eval: i32,  f: &FUNC,  min: f64,  max: f64,  start_value: f64) -> /*  throws MathIllegalArgumentException, TooManyEvaluationsException */Result<f64, Rc<Exception>>  ;

    /**
     * Solve for a zero in the vicinity of {@code startValue}.
     *
     * @param f Function to solve.
     * @param startValue Start value to use.
     * @return a value where the function is zero.
     * @param maxEval Maximum number of evaluations.
     * @throws org.apache.commons.math3.exception.MathIllegalArgumentException
     * if the arguments do not satisfy the requirements specified by the solver.
     * @throws org.apache.commons.math3.exception.TooManyEvaluationsException if
     * the allowed number of evaluations is exceeded.
     */
    fn  solve_with_start_value(&self,  max_eval: i32,  f: &FUNC,  start_value: f64) -> f64 ;
 }

 /**
 * Provide a default implementation for several functions useful to generic
 * solvers.
 * The default values for relative and function tolerances are 1e-14
 * and 1e-15, respectively. It is however highly recommended to not
 * rely on the default, but rather carefully consider values that match
 * user's expectations, as well as the specifics of each implementation.
 *
 * @param <FUNC> Type of function to solve.
 *
 * @since 2.0
 */

pub struct BaseAbstractUnivariateSolverData<FUNC: UnivariateFunction> {

    /** Function value accuracy. */
     pub function_value_accuracy: f64,

    /** Absolute accuracy. */
     pub absolute_accuracy: f64,

    /** Relative accuracy. */
     pub relative_accuracy: f64,

    /** Evaluations counter. */
     pub evaluations: i32,

    /** Lower end of search interval. */
     pub search_min: f64,

    /** Higher end of search interval. */
     pub search_max: f64,

    /** Initial guess. */
     pub search_start: f64,

    
}

impl BaseAbstractUnivariateSolverData {

    /**
     * Construct a solver with given absolute accuracy.
     *
     * @param absoluteAccuracy Maximum absolute error.
     */
    pub fn new_with_abs_accuracy( absolute_accuracy: f64) -> BaseAbstractUnivariateSolverData {
        new(DEFAULT_RELATIVE_ACCURACY, absolute_accuracy, DEFAULT_FUNCTION_VALUE_ACCURACY)
    }

    /**
     * Construct a solver with given accuracies.
     *
     * @param relativeAccuracy Maximum relative error.
     * @param absoluteAccuracy Maximum absolute error.
     */
    pub fn new_with_rel_abs_accuracy( relative_accuracy: f64,  absolute_accuracy: f64) -> BaseAbstractUnivariateSolver {
    	new(relative_accuracy, absolute_accuracy, DEFAULT_FUNCTION_VALUE_ACCURACY)
    }

    /**
     * Construct a solver with given accuracies.
     *
     * @param relativeAccuracy Maximum relative error.
     * @param absoluteAccuracy Maximum absolute error.
     * @param functionValueAccuracy Maximum function value error.
     */
    pub fn new( relative_accuracy: f64,  absolute_accuracy: f64,  function_value_accuracy: f64) -> BaseAbstractUnivariateSolver {
    	BaseAbstractUnivariateSolverData {
         absoluteAccuracy: absolute_accuracy,
         relativeAccuracy: relative_accuracy,
         functionValueAccuracy: function_value_accuracy,
         evaluations: 0 // IntegerSequence::Incrementor::create();
    	}
    }

    /** {@inheritDoc} */
    pub fn  get_max_evaluations(&self) -> i32  {
        return self.evaluations.get_maximal_count();
    }

    /** {@inheritDoc} */
    pub fn  get_evaluations(&self) -> i32  {
        return self.evaluations.get_count();
    }

    /**
     * @return the lower end of the search interval.
     */
    pub fn  get_min(&self) -> f64  {
        return self.search_min;
    }

    /**
     * @return the higher end of the search interval.
     */
    pub fn  get_max(&self) -> f64  {
        return self.search_max;
    }

    /**
     * @return the initial guess.
     */
    pub fn  get_start_value(&self) -> f64  {
        return self.search_start;
    }

    /**
     * {@inheritDoc}
     */
    pub fn  get_absolute_accuracy(&self) -> f64  {
        return self.absolute_accuracy;
    }

    /**
     * {@inheritDoc}
     */
    pub fn  get_relative_accuracy(&self) -> f64  {
        return self.relative_accuracy;
    }

    /**
     * {@inheritDoc}
     */
    pub fn  get_function_value_accuracy(&self) -> f64  {
        return self.function_value_accuracy;
    }

    /**
     * Compute the objective function value.
     *
     * @param point Point at which the objective function must be evaluated.
     * @return the objective function value at specified point.
     * @throws TooManyEvaluationsException if the maximal number of evaluations
     * is exceeded.
     */
    pub fn  compute_objective_value(&self,  point: f64) -> /*  throws TooManyEvaluationsException */Result<f64, Rc<Exception>>   {
        self.increment_evaluation_count();
        return Ok(self.function.value(point));
    }

    /**
     * Prepare for computation.
     * Subclasses must call this method if they override any of the
     * {@code solve} methods.
     *
     * @param f Function to solve.
     * @param min Lower bound for the interval.
     * @param max Upper bound for the interval.
     * @param startValue Start value to use.
     * @param maxEval Maximum number of evaluations.
     * @exception NullArgumentException if f is null
     */
    pub fn  setup(&self,  max_eval: i32,  f: &FUNC,  min: f64,  max: f64,  start_value: f64)  -> /*  throws NullArgumentException */Result<Void, Rc<Exception>>   {
        // Checks.
        MathUtils::check_not_null(f);
        // Reset.
        self.search_min = min;
        self.search_max = max;
        self.search_start = start_value;
        self.function = f;
        self.evaluations = self.evaluations.with_maximal_count(max_eval).with_start(0);
    }

    /** {@inheritDoc} */
    pub fn  solve(&self,  max_eval: i32,  f: &FUNC,  min: f64,  max: f64,  start_value: f64) -> /*  throws TooManyEvaluationsException, NoBracketingException */Result<f64, Rc<Exception>>   {
        // Initialization.
        self.setup(max_eval, f, min, max, start_value);
        // Perform computation.
        return Ok(self.do_solve());
    }

    /** {@inheritDoc} */
    pub fn  solve(&self,  max_eval: i32,  f: &FUNC,  min: f64,  max: f64) -> f64  {
        return Ok(self.solve(max_eval, f, min, max, min + 0.5 * (max - min)));
    }

    /** {@inheritDoc} */
    pub fn  solve(&self,  max_eval: i32,  f: &FUNC,  start_value: f64) -> /*  throws TooManyEvaluationsException, NoBracketingException */Result<f64, Rc<Exception>>   {
        return Ok(self.solve(max_eval, f, Double::NaN, Double::NaN, start_value));
    }

    /**
     * Method for implementing actual optimization algorithms in derived
     * classes.
     *
     * @return the root.
     * @throws TooManyEvaluationsException if the maximal number of evaluations
     * is exceeded.
     * @throws NoBracketingException if the initial search interval does not bracket
     * a root and the solver requires it.
     */
    pub fn  do_solve(&self) -> /*  throws TooManyEvaluationsException, NoBracketingException */Result<f64, Rc<Exception>>  ;

    /**
     * Check whether the function takes opposite signs at the endpoints.
     *
     * @param lower Lower endpoint.
     * @param upper Upper endpoint.
     * @return {@code true} if the function values have opposite signs at the
     * given points.
     */
    pub fn  is_bracketing(&self,  lower: f64,  upper: f64) -> bool  {
        return UnivariateSolverUtils::is_bracketing(self.function, lower, upper);
    }

    /**
     * Check whether the arguments form a (strictly) increasing sequence.
     *
     * @param start First number.
     * @param mid Second number.
     * @param end Third number.
     * @return {@code true} if the arguments form an increasing sequence.
     */
    pub fn  is_sequence(&self,  start: f64,  mid: f64,  end: f64) -> bool  {
        return UnivariateSolverUtils::is_sequence(start, mid, end);
    }

    /**
     * Check that the endpoints specify an interval.
     *
     * @param lower Lower endpoint.
     * @param upper Upper endpoint.
     * @throws NumberIsTooLargeException if {@code lower >= upper}.
     */
    pub fn  verify_interval(&self,  lower: f64,  upper: f64)  -> /*  throws NumberIsTooLargeException */Result<Void, Rc<Exception>>   {
        UnivariateSolverUtils::verify_interval(lower, upper);
    }

    /**
     * Check that {@code lower < initial < upper}.
     *
     * @param lower Lower endpoint.
     * @param initial Initial value.
     * @param upper Upper endpoint.
     * @throws NumberIsTooLargeException if {@code lower >= initial} or
     * {@code initial >= upper}.
     */
    pub fn  verify_sequence(&self,  lower: f64,  initial: f64,  upper: f64)  -> /*  throws NumberIsTooLargeException */Result<Void, Rc<Exception>>   {
        UnivariateSolverUtils::verify_sequence(lower, initial, upper);
    }

    /**
     * Check that the endpoints specify an interval and the function takes
     * opposite signs at the endpoints.
     *
     * @param lower Lower endpoint.
     * @param upper Upper endpoint.
     * @throws NullArgumentException if the function has not been set.
     * @throws NoBracketingException if the function has the same sign at
     * the endpoints.
     */
    pub fn  verify_bracketing(&self,  lower: f64,  upper: f64)  -> /*  throws NullArgumentException, NoBracketingException */Result<Void, Rc<Exception>>   {
        UnivariateSolverUtils::verify_bracketing(self.function, lower, upper);
    }

    /**
     * Increment the evaluation count by one.
     * Method {@link #computeObjectiveValue(double)} calls this method internally.
     * It is provided for subclasses that do not exclusively use
     * {@code computeObjectiveValue} to solve the function.
     * See e.g. {@link AbstractUnivariateDifferentiableSolver}.
     *
     * @throws TooManyEvaluationsException when the allowed number of function
     * evaluations has been exhausted.
     */
    pub fn  increment_evaluation_count(&self)  -> /*  throws TooManyEvaluationsException */Result<Void, Rc<Exception>>   {
        let tryResult1 = 0;
        'try1: loop {
        {
            self.evaluations.increment();
        }
        break 'try1
        }
        match tryResult1 {
             catch ( e: &MaxCountExceededException) {
                throw TooManyEvaluationsException::new(&e.get_max());
            }  0 => break
        }

    }
}


                
                