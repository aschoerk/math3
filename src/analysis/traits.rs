
use base::RealFieldElement;
use analysis::differentiation::derivative_structure::*;
use std::rc::*;
use java::exc::*;
/**
 * An interface representing a univariate real function.
 * <p>
 * When a <em>user-defined</em> function encounters an error during
 * evaluation, the {@link #value(double) value} method should throw a
 * <em>user-defined</em> unchecked exception.</p>
 * <p>
 * The following code excerpt shows the recommended way to do that using
 * a root solver as an example, but the same construct is applicable to
 * ODE integrators or optimizers.</p>
 *
 * <pre>
 * private static class LocalException extends RuntimeException {
 *     // The x value that caused the problem.
 *     private final double x;
 *
 *     public LocalException(double x) {
 *         this.x = x;
 *     }
 *
 *     public double getX() {
 *         return x;
 *     }
 * }
 *
 * private static class MyFunction implements UnivariateFunction {
 *     public double value(double x) {
 *         double y = hugeFormula(x);
 *         if (somethingBadHappens) {
 *           throw new LocalException(x);
 *         }
 *         return y;
 *     }
 * }
 *
 * public void compute() {
 *     try {
 *         solver.solve(maxEval, new MyFunction(a, b, c), min, max);
 *     } catch (LocalException le) {
 *         // Retrieve the x value.
 *     }
 * }
 * </pre>
 *
 * As shown, the exception is local to the user's code and it is guaranteed
 * that Apache Commons Math will not catch it.
 *
 */
pub trait UnivariateFunction {
    /**
     * Compute the value of the function.
     *
     * @param x Point at which the function value should be computed.
     * @return the value of the function.
     * @throws IllegalArgumentException when the activated method itself can
     * ascertain that a precondition, specified in the API expressed at the
     * level of the activated method, has been violated.
     * When Commons Math throws an {@code IllegalArgumentException}, it is
     * usually the consequence of checking the actual parameters passed to
     * the method.
     */
    fn value(&self, x: f64) -> f64;
}


/**
 * An interface representing a univariate vectorial function.
 *
 * @since 2.0
 */
pub trait UnivariateVectorFunction {
    /**
     * Compute the value for the function.
     * @param x the point for which the function value should be computed
     * @return the value
     */
    fn value(&self, x: f64) -> Vec<f64>;
}


/**
 * An interface representing a univariate matrix function.
 *
 * @since 2.0
 */
pub trait UnivariateMatrixFunction {
    /**
     * Compute the value for the function.
     * @param x the point for which the function value should be computed
     * @return the value
     */
    fn value(&self, x: f64) -> Vec<Vec<f64>>;
}


/**
 * An interface representing a bivariate real function.
 *
 * @since 2.1
 */
pub trait BivariateFunction {
    /**
     * Compute the value for the function.
     *
     * @param x Abscissa for which the function value should be computed.
     * @param y Ordinate for which the function value should be computed.
     * @return the value.
     */
    fn value(&self, x: f64, y: f64) -> f64;
}



/**
 * An interface representing a trivariate real function.
 *
 * @since 2.2
 */
pub trait TrivariateFunction {
    /**
     * Compute the value for the function.
     *
     * @param x x-coordinate for which the function value should be computed.
     * @param y y-coordinate for which the function value should be computed.
     * @param z z-coordinate for which the function value should be computed.
     * @return the value.
     */
    fn value(&self, x: f64, y: f64, z: f64) -> f64;
}


/**
 * An interface representing a real function that depends on one independent
 * variable plus some extra parameters.
 *
 * @since 3.0 
 */
pub trait ParametricUnivariateFunction {
    /**
     * Compute the value of the function.
     *
     * @param x Point for which the function value should be computed.
     * @param parameters Function parameters.
     * @return the value.
     */
    fn value(&self, x: f64, parameters: Vec<f64>) -> f64;

    /**
     * Compute the gradient of the function with respect to its parameters.
     *
     * @param x Point for which the function value should be computed.
     * @param parameters Function parameters.
     * @return the value.
     */
    fn gradient(&self, x: f64, parameters: Vec<f64>) -> Vec<f64>;
}


/**
 * An interface representing a multivariate real function.
 *
 * @since 2.0
 */
pub trait MultivariateFunction {
    /**
     * Compute the value for the function at the given point.
     *
     * @param point Point at which the function must be evaluated.
     * @return the function value for the given point.
     * @throws org.apache.commons.math3.exception.DimensionMismatchException
     * if the parameter's dimension is wrong for the function being evaluated.
     * @throws  org.apache.commons.math3.exception.MathIllegalArgumentException
     * when the activated method itself can ascertain that preconditions,
     * specified in the API expressed at the level of the activated method,
     * have been violated.  In the vast majority of cases where Commons Math
     * throws this exception, it is the result of argument checking of actual
     * parameters immediately passed to a method.
     */
    fn value(&self, point: &Vec<f64>) -> f64;
}


/**
 * An interface representing a multivariate vectorial function.
 * @since 2.0
 */
pub trait MultivariateVectorFunction {
    /**
     * Compute the value for the function at the given point.
     * @param point point at which the function must be evaluated
     * @return function value for the given point
     * @exception IllegalArgumentException if point's dimension is wrong
     */
    fn value(&self, point: &Vec<f64>) -> Result<Vec<f64>, &'static str>;
}


/**
 * An interface representing a multivariate matrix function.
 * @since 2.0
 */
pub trait MultivariateMatrixFunction {
    fn value(&self, point: &Vec<f64>) -> Result<Vec<Vec<f64>>, &'static str>;
}


/**
 * An interface representing a univariate real function.
 * <p>
 * When a <em>user-defined</em> function encounters an error during
 * evaluation, the {@link #value(RealFieldElement) value} method should throw a
 * <em>user-defined</em> unchecked exception.</p>
 * <p>
 * The following code excerpt shows the recommended way to do that using
 * a root solver as an example, but the same construct is applicable to
 * ODE integrators or optimizers.</p>
 *
 * <pre>
 * private static class LocalException extends RuntimeException {
 *     // The x value that caused the problem.
 *     private final SomeFieldType x;
 *
 *     public LocalException(SomeFieldType x) {
 *         this.x = x;
 *     }
 *
 *     public double getX() {
 *         return x;
 *     }
 * }
 *
 * private static class MyFunction implements FieldUnivariateFunction<SomeFieldType> {
 *     public SomeFieldType value(SomeFieldType x) {
 *         SomeFieldType y = hugeFormula(x);
 *         if (somethingBadHappens) {
 *           throw new LocalException(x);
 *         }
 *         return y;
 *     }
 * }
 *
 * public void compute() {
 *     try {
 *         solver.solve(maxEval, new MyFunction(a, b, c), min, max);
 *     } catch (LocalException le) {
 *         // Retrieve the x value.
 *     }
 * }
 * </pre>
 *
 * As shown, the exception is local to the user's code and it is guaranteed
 * that Apache Commons Math will not catch it.
 *
 * @param <T> the type of the field elements
 * @since 3.6
 * @see UnivariateFunction
 */
pub trait RealFieldUnivariateFunction<T: RealFieldElement<T>> {
    /**
     * Compute the value of the function.
     *
     * @param x Point at which the function value should be computed.
     * @return the value of the function.
     * @throws IllegalArgumentException when the activated method itself can
     * ascertain that a precondition, specified in the API expressed at the
     * level of the activated method, has been violated.
     * When Commons Math throws an {@code IllegalArgumentException}, it is
     * usually the consequence of checking the actual parameters passed to
     * the method.
     */
    fn value(&self, x: &T) -> T;
}





/** Interface for univariate functions derivatives.
 * <p>This interface represents a simple function which computes
 * both the value and the first derivative of a mathematical function.
 * The derivative is computed with respect to the input variable.</p>
 * @see UnivariateDifferentiableFunction
 * @see UnivariateFunctionDifferentiator
 * @since 3.1
 */
pub trait UnivariateDifferentiableFunction: UnivariateFunction {
    /** Simple mathematical function.
     * <p>{@link UnivariateDifferentiableFunction} classes compute both the
     * value and the first derivative of the function.</p>
     * @param t function input value
     * @return function result
     * @exception DimensionMismatchException if t is inconsistent with the
     * function's free parameters or order
     */
    fn value_ds(&self, t: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>>;
}

/**
 * Extension of {@link UnivariateFunction} representing a differentiable univariate real function.
 *
 * @deprecated as of 3.1 replaced by {@link org.apache.commons.math3.analysis.differentiation.UnivariateDifferentiableFunction}
 */
pub trait DifferentiableUnivariateFunction: UnivariateFunction {
    /**
     * Returns the derivative of the function
     *
     * @return  the derivative function
     */
    fn derivative(&self) -> UnivariateFunctionContainer;
}

pub struct UnivariateFunctionContainer {
    pub f: Box<Fn(f64) -> f64>,
}

impl UnivariateFunction for UnivariateFunctionContainer {
    fn value(&self, x: f64) -> f64 {

        (self.f)(x)
    }
}

impl UnivariateFunctionContainer {
    pub fn new(f: Box<Fn(f64) -> f64>) -> UnivariateFunctionContainer {
        UnivariateFunctionContainer { f: f }

    }
}

pub struct UnivariateDifferentiableFunctionContainer {
    pub f: Box<Fn(f64) -> f64>,

    pub f_ds: Box<Fn(&DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>>>,
}

impl UnivariateFunction for UnivariateDifferentiableFunctionContainer {
    fn value(&self, x: f64) -> f64 {

        (self.f)(x)
    }
}

impl UnivariateDifferentiableFunction for UnivariateDifferentiableFunctionContainer {
    fn value_ds(&self, t: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {

        (self.f_ds)(t)
    }
}




pub struct MultivariateFunctionContainer {
    pub f: Box<Fn(&Vec<f64>) -> f64>,
}

impl MultivariateFunction for MultivariateFunctionContainer {
    fn value(&self, point: &Vec<f64>) -> f64 {

        (self.f)(point)
    }
}
