use analysis::traits::*;
use analysis::differentiation::derivative_structure::*;
use std::rc::*;
use std::f64;
use java::exc::*;
use analysis::functions::*;

/**
     * Composes functions.
     * <p>
     * The functions in the argument list are composed sequentially, in the
     * given order.  For example, compose(f1,f2,f3) acts like f1(f2(f3(x))).</p>
     *
     * @param f List of functions.
     * @return the composite function.
     */
pub fn compose(f: Vec<Box<UnivariateFunction>>) -> UnivariateFunctionContainer {
    UnivariateFunctionContainer::new(Box::new(move |x: f64| {
        let mut r: f64 = x;
        for i in (0..f.len()).rev() {
            r = f[i].value(r);
        }
        return r;
    }))

}

/**
     * Adds functions.
     *
     * @param f List of functions.
     * @return a function that computes the sum of the functions.
     */
pub fn add(f: Vec<Box<UnivariateFunction>>) -> UnivariateFunctionContainer {
    UnivariateFunctionContainer::new(Box::new(move |x: f64| {
        let mut r: f64 = f[0].value(x);
        for i in 1..f.len() {
            r += f[i].value(x);
        }

        return r;
    }))
}

/**
     * Multiplies functions.
     *
     * @param f List of functions.
     * @return a function that computes the product of the functions.
     */
pub fn multiply(f: Vec<Box<UnivariateFunction>>) -> UnivariateFunctionContainer {
    UnivariateFunctionContainer::new(Box::new(move |x: f64| {
        let mut r: f64 = f[0].value(x);
        for i in 1..f.len() {
            r *= f[i].value(x);
        }

        return r;
    }))
}




/**
     * Composes functions.
     * <p>
     * The functions in the argument list are composed sequentially, in the
     * given order.  For example, compose(f1,f2,f3) acts like f1(f2(f3(x))).</p>
     *
     * @param f List of functions.
     * @return the composite function.
     * @since 3.1
     */
pub fn compose_ds(f: Vec<Rc<UnivariateDifferentiableFunction>>)
                  -> UnivariateDifferentiableFunctionContainer {
    let f_ds = f.clone();
    UnivariateDifferentiableFunctionContainer {
        f: Box::new(move |t: f64| {
            let mut r: f64 = t;
            for i in (0..f.len()).rev() {
                r = f[i].value(r);
            }
            return r;
        }),

        f_ds: Box::new(move |t: &DerivativeStructure| {
            let mut r: DerivativeStructure = t.clone();
            for i in (0..f_ds.len()).rev() {
                r = try!(f_ds[i].value_ds(&r));
            }
            return Ok(r);
        }),
    }
}

/**
     * Adds functions.
     *
     * @param f List of functions.
     * @return a function that computes the sum of the functions.
     * @since 3.1
     */
pub fn add_ds(f: Vec<Rc<UnivariateDifferentiableFunction>>)
              -> UnivariateDifferentiableFunctionContainer {
    let f_ds = f.clone();
    UnivariateDifferentiableFunctionContainer {
        f: Box::new(move |t: f64| {
            let mut r: f64 = f[0].value(t);
            for i in (1..f.len()).rev() {
                r += f[i].value(t);
            }
            return r;
        }),
        f_ds: Box::new(move |t: &DerivativeStructure| {
            let mut r = try!(f_ds[0].value_ds(&t));
            for i in (1..f_ds.len()).rev() {
                let tmp = try!(f_ds[i].value_ds(&t));
                r = try!(r.add(&tmp));
            }
            return Ok(r);
        }),
    }
}


/**
     * Multiplies functions.
     *
     * @param f List of functions.
     * @return a function that computes the product of the functions.
     * @since 3.1
     */
pub fn multiply_ds(f: Vec<Rc<UnivariateDifferentiableFunction>>)
                   -> UnivariateDifferentiableFunctionContainer {
    let f_ds = f.clone();
    UnivariateDifferentiableFunctionContainer {
        f: Box::new(move |t: f64| {
            let mut r: f64 = f[0].value(t);
            for i in (1..f.len()).rev() {
                r *= f[i].value(t);
            }
            return r;
        }),
        f_ds: Box::new(move |t: &DerivativeStructure| {
            let mut r = try!(f_ds[0].value_ds(&t));
            for i in (1..f_ds.len()).rev() {
                let tmp = try!(f_ds[i].value_ds(&t));
                r = try!(r.multiply(&tmp));
            }
            return Ok(r);
        }),
    }
}

/**
     * Returns the univariate function
     * {@code h(x) = combiner(f(x), g(x)).}
     *
     * @param combiner Combiner function.
     * @param f Function.
     * @param g Function.
     * @return the composite function.
     */
pub fn combine(combiner: Box<BivariateFunction>,
               f: Box<UnivariateFunction>,
               g: Box<UnivariateFunction>)
               -> UnivariateFunctionContainer {
    UnivariateFunctionContainer::new(Box::new(move |x: f64| {
        combiner.value(f.value(x), g.value(x))
    }))
}


/**
     * Returns a MultivariateFunction h(x[]) defined by <pre> <code>
     * h(x[]) = combiner(...combiner(combiner(initialValue,f(x[0])),f(x[1]))...),f(x[x.length-1]))
     * </code></pre>
     *
     * @param combiner Combiner function.
     * @param f Function.
     * @param initialValue Initial value.
     * @return a collector function.
     */
pub fn collector_by(combiner: Box<BivariateFunction>,
                    f: Box<UnivariateFunction>,
                    initial_value: f64)
                    -> MultivariateFunctionContainer {
    MultivariateFunctionContainer {
        f: Box::new(move |point| {
            let mut result: f64 = combiner.value(initial_value, f.value(point[0]));
            for i in 1..point.len() {
                result = combiner.value(result, f.value(point[i]));
            }
            result
        }),
    }
}

/**
         * Returns a MultivariateFunction h(x[]) defined by <pre> <code>
         * h(x[]) = combiner(...combiner(combiner(initialValue,x[0]),x[1])...),x[x.length-1])
         * </code></pre>
         *
         * @param combiner Combiner function.
         * @param initialValue Initial value.
         * @return a collector function.
         */
pub fn collector(combiner: Box<BivariateFunction>,
                 initial_value: f64)
                 -> MultivariateFunctionContainer {
    return collector_by(combiner, Box::new(Identity {}), initial_value);
}


/**
 * Creates a unary function by fixing the first argument of a binary function.
 *
 * @param f Binary function.
 * @param fixed value to which the first argument of {@code f} is set.
 * @return the unary function h(x) = f(fixed, x)
 */
pub fn fix1st_argument(f: Box<BivariateFunction>, fixed: f64) -> UnivariateFunctionContainer {
    UnivariateFunctionContainer { f: Box::new(move |x| f.value(fixed, x)) }
}

/**
  * Creates a unary function by fixing the second argument of a binary function.
  *
  * @param f Binary function.
  * @param fixed value to which the second argument of {@code f} is set.
  * @return the unary function h(x) = f(x, fixed)
  */
pub fn fix2nd_argument(f: Box<BivariateFunction>, fixed: f64) -> UnivariateFunctionContainer {
    UnivariateFunctionContainer { f: Box::new(move |x| f.value(x, fixed)) }
}

/**
    * Samples the specified univariate real function on the specified interval.
    * <p>
    * The interval is divided equally into {@code n} sections and sample points
    * are taken from {@code min} to {@code max - (max - min) / n}; therefore
    * {@code f} is not sampled at the upper bound {@code max}.</p>
    *
    * @param f Function to be sampled
    * @param min Lower bound of the interval (included).
    * @param max Upper bound of the interval (excluded).
    * @param n Number of sample points.
    * @return the array of samples.
    * @throws NumberIsTooLargeException if the lower bound {@code min} is
    * greater than, or equal to the upper bound {@code max}.
    * @throws NotStrictlyPositiveException if the number of sample points
    * {@code n} is negative.
    */
pub fn sample(f: Box<UnivariateFunction>,
              min: f64,
              max: f64,
              n: usize)
              -> Result<Vec<f64>, Rc<Exception>> {
    if n == 0 {
        return Err(Exc::new_msg("NotStrictlyPositiveException",
                                format!("number of sample is not positive: {0}", n)));
    }
    if min >= max {
        return Err(Exc::new_msg("NumberIsTooLargeException",
                                format!("min: {} >= max: {}", min, max)));
    }
    let mut s = vec![0.0; n];
    let h: f64 = (max - min) / n as f64;

    for i in 0..n {
        s[i] = f.value(min + i as f64 * h);
    }

    return Ok(s);
}


/**
     * Convert a {@link UnivariateDifferentiableFunction} into a {@link DifferentiableUnivariateFunction}.
     *
     * @param f function to convert
     * @return converted function
     * @deprecated this conversion method is temporary in version 3.1, as the {@link
     * DifferentiableUnivariateFunction} interface itself is deprecated
     */
pub fn to_differentiable_univariate_function(f: Box<UnivariateDifferentiableFunction>)
                                             -> UnivariateFunctionContainer {

    UnivariateFunctionContainer::new(Box::new(move |x: f64| {
        f.value_ds(&DerivativeStructure::new(1, 1, 0, x).unwrap())
            .unwrap()
            .get_partial_derivative(&vec![1])
            .unwrap()
    }))
}
