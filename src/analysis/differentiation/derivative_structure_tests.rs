#![allow(unused_imports)]
use assert;
use base_tests::*;
use analysis::differentiation::derivative_structure::*;
use std::f64;
use util::rsutils;
use util::fastmath;
use util::fastmath::F64;
use util::arithmetic_utils;
use util::combinatorics_utils::*;
use util::test_utils::TestUtils;
use analysis::traits::*;
use analysis::polynomials::polynomial_function::*;
use random::bits_stream_generator::*;
use random::well_generator::*;

fn  check_equals(ds1: &DerivativeStructure,  ds2: &DerivativeStructure,  epsilon: f64)   {
        // check dimension
        assert_eq!(&ds1.get_free_parameters(), &ds2.get_free_parameters());
        assert_eq!(&ds1.get_order(), &ds2.get_order());
         let mut derivatives = vec![0i32; ds1.get_free_parameters()];
         let mut sum: usize = 0;
        loop {
            if sum <= ds1.get_order() {
                assert_within_delta!(ds1.get_partial_derivative(&derivatives).unwrap(), ds2.get_partial_derivative(&derivatives).unwrap(), epsilon);
            }
             let mut increment: bool = true;
            sum = 0;
             {
                 let mut i: i32 = derivatives.len() as i32 - 1;
                while i >= 0 {
                    {
                        if increment {
                            if derivatives[i as usize] == ds1.get_order() as i32 {
                                derivatives[i as usize] = 0;
                            } else {
                                derivatives[i as usize] += 1;
                                increment = false;
                            }
                        }
                        sum += derivatives[i as usize] as usize;
                    }
                    i -= 1;
                 }
             }

            if increment {
                return;
            }
        }
    }

fn  check_f0_f1(ds: &DerivativeStructure,  value: f64,  derivatives: &Vec<f64>)   {
        // check dimension
        assert_eq!(derivatives.len(), ds.get_free_parameters());
        // check value, directly and also as 0th order derivative
        assert_within_delta!(value, ds.get_value(), 1.0e-15);
        assert_within_delta!(value, ds.get_partial_derivative(&vec![0; ds.get_free_parameters()]).unwrap(), 1.0e-15);
        // check first order derivatives
         {
             let mut i: usize = 0;
            while i < derivatives.len() {
                {
                     let mut orders = vec![0; derivatives.len()];
                    orders[i] = 1;
                    assert_within_delta!(derivatives[i], ds.get_partial_derivative(&orders).unwrap(), 1.0e-15);
                }
                i += 1;
             }
         }

    }


struct DerivativStructureTest {

}


impl ExtendedFieldElementAbstractTest<DerivativeStructure> for DerivativStructureTest {

	fn build(&self, x: f64) -> DerivativeStructure {
		DerivativeStructure::new(2, 1, 0, x).unwrap()
	}

}


fn test() -> DerivativStructureTest {
	DerivativStructureTest {

	}
}

/*
#[test]
pub fn extended_field_element_abstract_test() {
	
	test().test_add_field();
	test().test_add_double();
	test().test_subtract_field();
	test().test_subtract_double();
	test().test_multiply_field();
	test().test_multiply_double();
	test().test_multiply_int();
	test().test_divide_field();
	test().test_divide_double();	
	test().test_remainder_field();
	test().test_remainder_double();
	test().test_cos();
	test().test_cos();
	
	test().test_sin();
	test().test_asin();
	test().test_tan();
	test().test_atan();
	test().test_atan2();
	test().test_cosh();
	test().test_acosh();
	test().test_sinh();
	test().test_asinh();
	test().test_tanh();
	test().test_atanh();
	test().test_sqrt();
	test().test_cbrt();
	test().test_hypot();
	test().test_root_n();
	test().test_pow_field();
	test().test_pow_double();
	test().test_pow_int();
	test().test_exp();
	test().test_expm1();
	test().test_log();
	test().test_log1p();
	test().test_abs();
	test().test_ceil();
	test().test_floor();
	test().test_rint();
	test().test_round();
	test().test_signum();
	test().test_copy_sign_field();
	test().test_copy_sign_double();
	test().test_scalb();
	
}



	#[test]
    pub fn  test_wrong_variable_index()   {
        assert!(DerivativeStructure::new(3, 1, 3, 1.0).is_err());
    }

    #[test]
    pub fn  test_missing_orders()   {
        assert!(DerivativeStructure::new(3, 1, 0, 1.0).unwrap().get_partial_derivative(&vec![0i32, 1]).is_err());
    }

    #[test]
    pub fn  test_too_large_order()   {
        assert!(DerivativeStructure::new(3, 1, 0, 1.0).unwrap().get_partial_derivative(&vec![1i32, 1, 2]).is_err());
    }
    
    
    #[test]
    pub fn  test_variable_without_derivative0()   {
         let v: DerivativeStructure = DerivativeStructure::new(1, 0, 0, 1.0).unwrap();
        assert_within_delta!(1.0, v.get_value(), 1.0e-15);
    }

    #[test]
    pub fn  test_variable_without_derivative1()   {
         let v: DerivativeStructure = DerivativeStructure::new(1, 0, 0, 1.0).unwrap();
        assert!(v.get_partial_derivative(&vec![1]).is_err());
    }


    #[test]
    pub fn  test_variable()   {
         {
             let mut max_order: usize = 1;
            while max_order < 5 {
                {
                    check_f0_f1(&DerivativeStructure::new(3, max_order, 0, 1.0).unwrap(), 1.0, &vec![1.0, 0.0, 0.0]);
                    check_f0_f1(&DerivativeStructure::new(3, max_order, 1, 2.0).unwrap(), 2.0, &vec![0.0, 1.0, 0.0]);
                    check_f0_f1(&DerivativeStructure::new(3, max_order, 2, 3.0).unwrap(), 3.0, &vec![0.0, 0.0, 1.0]);
                }
                max_order += 1;
             }
         }

    }


 #[test]
    pub fn  test_constant()   {
         {
             let mut max_order: usize = 1;
            while max_order < 5 {
                {
                    check_f0_f1(&DerivativeStructure::new_parameters_order_value(3, max_order, fastmath::PI).unwrap(), fastmath::PI, &vec![0.0, 0.0, 0.0]);
                }
                max_order += 1;
             }
         }

    }



    #[test]
    pub fn  test_create_constant()   {
         let a: DerivativeStructure = DerivativeStructure::new(3, 2, 0, 1.3).unwrap();
         let b: DerivativeStructure = a.create_constant(2.5).unwrap();
        assert_eq!(a.get_free_parameters(), b.get_free_parameters());
        assert_eq!(a.get_order(), b.get_order());
        check_equals(&a.get_field().get_one().multiply_f64(2.5).unwrap(), &b, 1.0e-15);
    }

  #[test]
    pub fn  test_primitive_add()   {
         {
             let mut max_order: usize = 1;
            while max_order < 5 {
                {
                    check_f0_f1(&DerivativeStructure::new(3, max_order, 0, 1.0).unwrap().add_f64(5.0).unwrap(), 6.0, &vec![1.0, 0.0, 0.0]);
                    check_f0_f1(&DerivativeStructure::new(3, max_order, 1, 2.0).unwrap().add_f64(5.0).unwrap(), 7.0, &vec![0.0, 1.0, 0.0]);
                    check_f0_f1(&DerivativeStructure::new(3, max_order, 2, 3.0).unwrap().add_f64(5.0).unwrap(), 8.0, &vec![0.0, 0.0, 1.0]);
                }
                max_order += 1;
             }
         }

    }

    #[test]
    pub fn  test_add()   {
         {
             let mut max_order: usize = 1;
            while max_order < 5 {
                {
                     let x: DerivativeStructure = DerivativeStructure::new(3, max_order, 0, 1.0).unwrap();
                     let y: DerivativeStructure = DerivativeStructure::new(3, max_order, 1, 2.0).unwrap();
                     let z: DerivativeStructure = DerivativeStructure::new(3, max_order, 2, 3.0).unwrap();
                     let xyz: DerivativeStructure = x.add(&y.add(&z).unwrap()).unwrap();
                    check_f0_f1(&xyz, x.get_value() + y.get_value() + z.get_value(), &vec![1.0, 1.0, 1.0]);
                }
                max_order += 1;
             }
         }

    }

#[test]
    pub fn  test_primitive_subtract()   {
         {
             let mut max_order: usize = 1;
            while max_order < 5 {
                {
                    check_f0_f1(&DerivativeStructure::new(3, max_order, 0, 1.0).unwrap().subtract_f64(5.0).unwrap(), -4.0, &vec![1.0, 0.0, 0.0]);
                    check_f0_f1(&DerivativeStructure::new(3, max_order, 1, 2.0).unwrap().subtract_f64(5.0).unwrap(), -3.0, &vec![0.0, 1.0, 0.0]);
                    check_f0_f1(&DerivativeStructure::new(3, max_order, 2, 3.0).unwrap().subtract_f64(5.0).unwrap(), -2.0, &vec![0.0, 0.0, 1.0]);
                }
                max_order += 1;
             }
         }

    }

    #[test]
    pub fn  test_subtract()   {
         {
             let mut max_order: usize = 1;
            while max_order < 5 {
                {
                     let x: DerivativeStructure = DerivativeStructure::new(3, max_order, 0, 1.0).unwrap();
                     let y: DerivativeStructure = DerivativeStructure::new(3, max_order, 1, 2.0).unwrap();
                     let z: DerivativeStructure = DerivativeStructure::new(3, max_order, 2, 3.0).unwrap();
                     let xyz: DerivativeStructure = x.subtract(&y.subtract(&z).unwrap()).unwrap();
                    check_f0_f1(&xyz, x.get_value() - (y.get_value() - z.get_value()), &vec![1.0, -1.0, 1.0]);
                }
                max_order += 1;
             }
         }

    }



    #[test]
    pub fn  test_primitive_multiply()   {
         {
             let mut max_order: usize = 1;
            while max_order < 5 {
                {
                    check_f0_f1(&DerivativeStructure::new(3, max_order, 0, 1.0).unwrap().multiply_f64(5.0).unwrap(), 5.0, &vec![5.0, 0.0, 0.0]);
                    check_f0_f1(&DerivativeStructure::new(3, max_order, 1, 2.0).unwrap().multiply_f64(5.0).unwrap(), 10.0, &vec![0.0, 5.0, 0.0]);
                    check_f0_f1(&DerivativeStructure::new(3, max_order, 2, 3.0).unwrap().multiply_f64(5.0).unwrap(), 15.0, &vec![0.0, 0.0, 5.0]);
                }
                max_order += 1;
             }
         }

    }

    #[test]
    pub fn  test_multiply()   {
         {
             let mut max_order: usize = 1;
            while max_order < 5 {
                {
                     let x: DerivativeStructure = DerivativeStructure::new(3, max_order, 0, 1.0).unwrap();
                     let y: DerivativeStructure = DerivativeStructure::new(3, max_order, 1, 2.0).unwrap();
                     let z: DerivativeStructure = DerivativeStructure::new(3, max_order, 2, 3.0).unwrap();
                     let xyz: DerivativeStructure = x.multiply(&y.multiply(&z).unwrap()).unwrap();
                     {
                         let mut i: usize = 0;
                        while i <= max_order {
                            {
                                 {
                                     let mut j: usize = 0;
                                    while j <= max_order {
                                        {
                                             {
                                                 let mut k: usize = 0;
                                                while k <= max_order {
                                                    {
                                                        if i + j + k <= max_order {
                                                            assert_within_delta!(
                                                            	( if i == 0 { x.get_value() } 
                                                            		else { 
                                                            			( if i == 1 { 1.0 } 
                                                            				else { 0.0 }) }) * 
                                                            	( if j == 0 { y.get_value() } 
                                                            		else { 
                                                            			( if j == 1 { 1.0 } 
                                                            				else { 0.0 }) }) * 
                                                            	( if k == 0 { z.get_value() }
                                                            		 else { ( if k == 1 { 1.0 } else { 0.0 }) }), 
                                                            	xyz.get_partial_derivative(&vec![i as i32, j as i32, k as i32]).unwrap(), 
                                                            	1.0e-15);
                                                        }
                                                    }
                                                    k += 1;
                                                 }
                                             }

                                        }
                                        j += 1;
                                     }
                                 }

                            }
                            i += 1;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }

    #[test]
    pub fn  test_negate()   {
         {
             let mut max_order: usize = 1;
            while max_order < 5 {
                {
                    check_f0_f1(&DerivativeStructure::new(3, max_order, 0, 1.0).unwrap().negate().unwrap(), -1.0, &vec![-1.0, 0.0, 0.0]);
                    check_f0_f1(&DerivativeStructure::new(3, max_order, 1, 2.0).unwrap().negate().unwrap(), -2.0, &vec![0.0, -1.0, 0.0]);
                    check_f0_f1(&DerivativeStructure::new(3, max_order, 2, 3.0).unwrap().negate().unwrap(), -3.0, &vec![0.0, 0.0, -1.0]);
                }
                max_order += 1;
             }
         }

    }

    #[test]
    pub fn  test_reciprocal()   {
         {
             let mut x: f64 = 0.1;
            while x < 1.2 {
                {
                     let r: DerivativeStructure = DerivativeStructure::new(1, 6, 0, x).unwrap().reciprocal().unwrap();
                    assert_within_delta!(1.0 / x, r.get_value(), 1.0e-15);
                     {
                         let mut i: usize = 1;
                        while i < r.get_order() {
                            {
                                 let expected: f64 = (arithmetic_utils::I64::pow_i32(-1, i as i32).unwrap() 
                                 	* CombinatoricsUtils::factorial(i as i32).unwrap()) as f64 / F64::pow_i32(x, i as i32 + 1);
                                assert_within_delta!(expected, r.get_partial_derivative(&vec![i as i32]).unwrap(), 1.0e-15 * F64::abs(expected));
                            }
                            i += 1;
                         }
                     }

                }
                x += 0.1;
             }
         }

    }

    #[test]
    pub fn  test_pow()   {
         {
             let mut max_order: usize = 1;
            while max_order < 5 {
                {
                     {
                         let mut n: usize = 0;
                        while n < 10 {
                            {
                                 let x: DerivativeStructure = DerivativeStructure::new(3, max_order, 0, 1.0).unwrap();
                                 let y: DerivativeStructure = DerivativeStructure::new(3, max_order, 1, 2.0).unwrap();
                                 let z: DerivativeStructure = DerivativeStructure::new(3, max_order, 2, 3.0).unwrap();
                                 let tmp1 = x.add(&y).unwrap().add(&z).unwrap();
                                 let tmp2 = x.multiply(&y).unwrap().multiply(&z).unwrap();
                                 let list = vec![x, y, z, tmp1, tmp2];
                                if n == 0 {
                                    for ds in list.iter() {
                                        check_equals(&ds.get_field().get_one(), &ds.pow_f64(n as f64).unwrap(), 1.0e-15);
                                    }
                                } else if n == 1 {
                                    for ds in list.iter() {
                                        check_equals(&ds, &ds.pow_f64(n as f64).unwrap(), 1.0e-15);
                                    }
                                } else {
                                    for ds in list.iter() {
                                         let mut p: DerivativeStructure = ds.get_field().get_one();
                                         {
                                             let mut i: usize = 0;
                                            while i < n {
                                                {
                                                    p = p.multiply(&ds).unwrap();
                                                }
                                                i += 1;
                                             }
                                         }

                                        check_equals(&p, &ds.pow_f64(n as f64).unwrap(), 1.0e-15);
                                    }
                                }
                            }
                            n += 1;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }


    
    #[test]
    pub fn  test_pow_double_ds()   {
         {
             let mut max_order: usize = 1;
            while max_order < 5 {
                {
                     let x: DerivativeStructure = DerivativeStructure::new(3, max_order, 0, 0.1).unwrap();
                     let y: DerivativeStructure = DerivativeStructure::new(3, max_order, 1, 0.2).unwrap();
                     let z: DerivativeStructure = DerivativeStructure::new(3, max_order, 2, 0.3).unwrap();
                      let tmp1 = x.add(&y).unwrap().add(&z).unwrap();
                     let tmp2 = x.multiply(&y).unwrap().multiply(&z).unwrap();
                     let zero = x.get_field().get_zero();
                     let list = vec![x, y, z, tmp1, tmp2];
                    for ds in list.iter() {
                        // the special case a = 0 is included here
                        for a in vec![0.0, 0.1, 1.0, 2.0, 5.0, ].iter()
                         {
                             let reference: DerivativeStructure =  
                             if *a == 0.0 { zero.clone() } 
                             else { DerivativeStructure::new_parameters_order_value(3, max_order, *a).unwrap().pow(&ds).unwrap() };
                             let result: DerivativeStructure = DerivativeStructure::pow_f64_static(*a, &ds).unwrap();
                            check_equals(&reference, &result, 1.0e-15);
                        }
                    }
                    // negative base: -1^x can be evaluated for integers only, so value is sometimes OK, derivatives are always NAN
                     let neg_even_integer: DerivativeStructure = DerivativeStructure::pow_f64_static(-2.0, &DerivativeStructure::new(3, max_order, 0, 2.0).unwrap()).unwrap();
                     assert_within_delta!(4.0, neg_even_integer.get_value(), 1.0e-15);
                    assert!(f64::is_nan(neg_even_integer.get_partial_derivative(&vec![1, 0, 0]).unwrap()));
                     let neg_odd_integer: DerivativeStructure = DerivativeStructure::pow_f64_static(-2.0, &DerivativeStructure::new(3, max_order, 0, 3.0).unwrap()).unwrap();
                     assert_within_delta!(-8.0, neg_odd_integer.get_value(), 1.0e-15);
                    assert!(f64::is_nan(neg_odd_integer.get_partial_derivative(&vec![1, 0, 0]).unwrap()));
                     let neg_non_integer: DerivativeStructure = DerivativeStructure::pow_f64_static(-2.0, &DerivativeStructure::new(3, max_order, 0, 2.001).unwrap()).unwrap();
                    assert!(f64::is_nan(neg_non_integer.get_value()));
                    assert!(f64::is_nan(neg_non_integer.get_partial_derivative(&vec![1, 0, 0]).unwrap()));
                     let zero_neg: DerivativeStructure = DerivativeStructure::pow_f64_static(0.0, &DerivativeStructure::new(3, max_order, 0, -1.0).unwrap()).unwrap();
                    assert!(f64::is_nan(zero_neg.get_value()));
                    assert!(f64::is_nan(zero_neg.get_partial_derivative(&vec![1, 0, 0]).unwrap()));
                     let pos_neg: DerivativeStructure = DerivativeStructure::pow_f64_static(2.0, &DerivativeStructure::new(3, max_order, 0, -2.0).unwrap()).unwrap();
                     assert_within_delta!(1.0 / 4.0, pos_neg.get_value(), 1.0e-15);
                     assert_within_delta!(F64::log(2.0) / 4.0, pos_neg.get_partial_derivative(&vec![1, 0, 0]).unwrap(), 1.0e-15);
                    // very special case: a = 0 and power = 0
                     let zero_zero: DerivativeStructure = DerivativeStructure::pow_f64_static(0.0, &DerivativeStructure::new(3, max_order, 0, 0.0).unwrap()).unwrap();
                    // this should be OK for simple first derivative with one variable only ...
                     assert_within_delta!(1.0, zero_zero.get_value(), 1.0e-15);
                     assert_within_delta!(f64::NEG_INFINITY, zero_zero.get_partial_derivative(&vec![1, 0, 0]).unwrap(), 1.0e-15);
                    // the following checks show a LIMITATION of the current implementation
                    // we have no way to tell x is a pure linear variable x = 0
                    // we only say: "x is a structure with value = 0.0,
                    // first derivative with respect to x = 1.0, and all other derivatives
                    // (first order with respect to y and z and higher derivatives) all 0.0.
                    // We have function f(x) = a^x and x = 0 so we compute:
                    // f(0) = 1, f'(0) = ln(a), f''(0) = ln(a)^2. The limit of these values
                    // when a converges to 0 implies all derivatives keep switching between
                    // +infinity and -infinity.
                    //
                    // Function composition rule for first derivatives is:
                    // d[f(g(x,y,z))]/dy = f'(g(x,y,z)) * dg(x,y,z)/dy
                    // so given that in our case x represents g and does not depend
                    // on y or z, we have dg(x,y,z)/dy = 0
                    // applying the composition rules gives:
                    // d[f(g(x,y,z))]/dy = f'(g(x,y,z)) * dg(x,y,z)/dy
                    //                 = -infinity * 0
                    //                 = NAN
                    // if we knew x is really the x variable and not the identity
                    // function applied to x, we would not have computed f'(g(x,y,z)) * dg(x,y,z)/dy
                    // and we would have found that the result was 0 and not NAN
                    assert!(f64::is_nan(zero_zero.get_partial_derivative(&vec![0, 1, 0]).unwrap()));
                    assert!(f64::is_nan(zero_zero.get_partial_derivative(&vec![0, 0, 1]).unwrap()));
                    // and we would have found that the result was +infinity and not NAN
                    if max_order > 1 {
                        assert!(f64::is_nan(zero_zero.get_partial_derivative(&vec![2, 0, 0]).unwrap()));
                        assert!(f64::is_nan(zero_zero.get_partial_derivative(&vec![0, 2, 0]).unwrap()));
                        assert!(f64::is_nan(zero_zero.get_partial_derivative(&vec![0, 0, 2]).unwrap()));
                        assert!(f64::is_nan(zero_zero.get_partial_derivative(&vec![1, 1, 0]).unwrap()));
                        assert!(f64::is_nan(zero_zero.get_partial_derivative(&vec![0, 1, 1]).unwrap()));
                        assert!(f64::is_nan(zero_zero.get_partial_derivative(&vec![1, 1, 0]).unwrap()));
                    }
                }
                max_order += 1;
             }
         }
    }
         
         
         
         #[test]
	    pub fn  test_expression()   {
         let epsilon: f64 = 2.5e-13;
         {
             let mut x: f64 = 0.0;
            while x < 2.0 {
                {
                     let ds_x: DerivativeStructure = DerivativeStructure::new(3, 5, 0, x).unwrap();
                     {
                         let mut y: f64 = 0.0;
                        while y < 2.0 {
                            {
                                 let ds_y: DerivativeStructure = DerivativeStructure::new(3, 5, 1, y).unwrap();
                                 {
                                     let mut z: f64 = 0.0;
                                    while z > -2.0 {
                                        {
                                             let ds_z: DerivativeStructure = DerivativeStructure::new(3, 5, 2, z).unwrap();
                                            // f(x, y, z) = x + 5 x y - 2 z + (8 z x - y)^3
                                             let ds: DerivativeStructure = DerivativeStructure::new_4d(
                                             	1.0, &ds_x, 
                                             	5.0, &ds_x.multiply(&ds_y).unwrap(), 
                                             	-2.0, &ds_z, 
                                             	1.0, &DerivativeStructure::new_2d(
                                             		8.0, &ds_z.multiply(&ds_x).unwrap(), 
                                             		-1.0, &ds_y)
                                             	.unwrap().pow_i32(3).unwrap()).unwrap();
                                             let ds_other: DerivativeStructure = DerivativeStructure::new_3d(
                                             	1.0, &ds_x, 
                                             	5.0, &ds_x.multiply(&ds_y).unwrap(), 
                                             	-2.0, &ds_z).unwrap()
                                             .add(
                                             		&DerivativeStructure::new_2d(
                                             			8.0, &ds_z.multiply(&ds_x).unwrap(), 
                                             			-1.0, &ds_y).unwrap()
                                             		.pow_i32(3).unwrap()
                                             	).unwrap();
                                             let f: f64 = x + 5.0 * x * y - 2.0 * z + F64::pow_i32(8.0 * z * x - y, 3);
                                            assert_within_delta!(f, ds.get_value(), F64::abs(epsilon * f));
                                            assert_within_delta!(f, ds_other.get_value(), F64::abs(epsilon * f));
                                            // df/dx = 1 + 5 y + 24 (8 z x - y)^2 z
                                             let dfdx: f64 = 1.0 + 5.0 * y + 24.0 * z * F64::pow_i32(8.0 * z * x - y, 2);
                                            assert_within_delta!(dfdx, ds.get_partial_derivative(&vec![1, 0, 0]).unwrap(), F64::abs(epsilon * dfdx));
                                            assert_within_delta!(dfdx, ds_other.get_partial_derivative(&vec![1, 0, 0]).unwrap(), F64::abs(epsilon * dfdx));
                                            // df/dxdy = 5 + 48 z*(y - 8 z x)
                                             let dfdxdy: f64 = 5.0 + 48.0 * z * (y - 8.0 * z * x);
                                            assert_within_delta!(dfdxdy, ds.get_partial_derivative(&vec![1, 1, 0]).unwrap(), F64::abs(epsilon * dfdxdy));
                                            assert_within_delta!(dfdxdy, ds_other.get_partial_derivative(&vec![1, 1, 0]).unwrap(), F64::abs(epsilon * dfdxdy));
                                            // df/dxdydz = 48 (y - 16 z x)
                                             let dfdxdydz: f64 = 48.0 * (y - 16.0 * z * x);
                                            assert_within_delta!(dfdxdydz, ds.get_partial_derivative(&vec![1, 1, 1]).unwrap(), F64::abs(epsilon * dfdxdydz));
                                            assert_within_delta!(dfdxdydz, ds_other.get_partial_derivative(&vec![1, 1, 1]).unwrap(), F64::abs(epsilon * dfdxdydz));
                                        }
                                        z -= 0.2;
                                     }
                                 }

                            }
                            y += 0.2;
                         }
                     }

                }
                x += 0.2;
             }
         }

    }


    #[test]
    pub fn  test_composition_one_variable_x()   {
         let epsilon: f64 = 1.0e-13;
         {
             let mut max_order: usize = 0;
            while max_order < 5 {
                {
                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(1, max_order, 0, x).unwrap();
                                 {
                                     let mut y: f64 = 0.1;
                                    while y < 1.2 {
                                        {
                                             let ds_y: DerivativeStructure = DerivativeStructure::new_parameters_order_value(1, max_order, y).unwrap();
                                             let f: DerivativeStructure = ds_x.divide(&ds_y).unwrap().sqrt().unwrap();
                                             let f0: f64 = F64::sqrt(x / y);
                                            assert_within_delta!(f0, f.get_value(), F64::abs(epsilon * f0));
                                            if f.get_order() > 0 {
                                                 let f1: f64 = 1.0 / (2.0 * F64::sqrt(x * y));
                                                assert_within_delta!(f1, f.get_partial_derivative(&vec![1]).unwrap(), F64::abs(epsilon * f1));
                                                if f.get_order() > 1 {
                                                     let f2: f64 = -f1 / (2.0 * x);
                                                    assert_within_delta!(f2, f.get_partial_derivative(&vec![2]).unwrap(), F64::abs(epsilon * f2));
                                                    if f.get_order() > 2 {
                                                         let f3: f64 = (f0 + x / (2.0 * y * f0)) / (4.0 * x * x * x);
                                                        assert_within_delta!(f3, f.get_partial_derivative(&vec![3]).unwrap(), F64::abs(epsilon * f3));
                                                    }
                                                }
                                            }
                                        }
                                        y += 0.1;
                                     }
                                 }

                            }
                            x += 0.1;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }
    
     #[test]
    pub fn  test_trigo()   {
         let epsilon: f64 = 2.0e-12;
         {
             let mut max_order: usize = 0;
            while max_order < 5 {
                {
                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(3, max_order, 0, x).unwrap();
                                 {
                                     let mut y: f64 = 0.1;
                                    while y < 1.2 {
                                        {
                                             let ds_y: DerivativeStructure = DerivativeStructure::new(3, max_order, 1, y).unwrap();
                                             {
                                                 let mut z: f64 = 0.1;
                                                while z < 1.2 {
                                                    {
                                                         let ds_z: DerivativeStructure = DerivativeStructure::new(3, max_order, 2, z).unwrap();
                                                         let f: DerivativeStructure = ds_x.divide(&ds_y.cos().unwrap().add(&ds_z.tan().unwrap()).unwrap()).unwrap().sin().unwrap();
                                                         let a: f64 = F64::cos(y) + F64::tan(z);
                                                         let f0: f64 = F64::sin(x / a);
                                                        assert_within_delta!(f0, f.get_value(), F64::abs(epsilon * f0));
                                                        if f.get_order() > 0 {
                                                             let dfdx: f64 = F64::cos(x / a) / a;
                                                            assert_within_delta!(dfdx, f.get_partial_derivative(&vec![1, 0, 0]).unwrap(), F64::abs(epsilon * dfdx));
                                                             let dfdy: f64 = x * F64::sin(y) * dfdx / a;
                                                            assert_within_delta!(dfdy, f.get_partial_derivative(&vec![0, 1, 0]).unwrap(), F64::abs(epsilon * dfdy));
                                                             let cz: f64 = F64::cos(z);
                                                             let cz2: f64 = cz * cz;
                                                             let dfdz: f64 = -x * dfdx / (a * cz2);
                                                            assert_within_delta!(dfdz, f.get_partial_derivative(&vec![0, 0, 1]).unwrap(), F64::abs(epsilon * dfdz));
                                                            if f.get_order() > 1 {
                                                                 let df2dx2: f64 = -(f0 / (a * a));
                                                                assert_within_delta!(df2dx2, f.get_partial_derivative(&vec![2, 0, 0]).unwrap(), F64::abs(epsilon * df2dx2));
                                                                 let df2dy2: f64 = x * F64::cos(y) * dfdx / a - x * x * F64::sin(y) * F64::sin(y) * f0 / (a * a * a * a) + 2.0 * F64::sin(y) * dfdy / a;
                                                                assert_within_delta!(df2dy2, f.get_partial_derivative(&vec![0, 2, 0]).unwrap(), F64::abs(epsilon * df2dy2));
                                                                 let c4: f64 = cz2 * cz2;
                                                                 let df2dz2: f64 = x * (2.0 * a * (1.0 - a * cz * F64::sin(z)) * dfdx - x * f0 / a) / (a * a * a * c4);
                                                                assert_within_delta!(df2dz2, f.get_partial_derivative(&vec![0, 0, 2]).unwrap(), F64::abs(epsilon * df2dz2));
                                                                 let df2dxdy: f64 = dfdy / x - x * F64::sin(y) * f0 / (a * a * a);
                                                                assert_within_delta!(df2dxdy, f.get_partial_derivative(&vec![1, 1, 0]).unwrap(), F64::abs(epsilon * df2dxdy));
                                                            }
                                                        }
                                                    }
                                                    z += 0.1;
                                                 }
                                             }

                                        }
                                        y += 0.1;
                                     }
                                 }

                            }
                            x += 0.1;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }

     #[test]
    pub fn  test_sqrt_definition()   {
         let epsilon = vec![5.0e-16, 5.0e-16, 2.0e-15, 5.0e-14, 2.0e-12, ]
        ;
         {
             let mut max_order: usize = 0;
            while max_order < 5 {
                {
                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(1, max_order, 0, x).unwrap();
                                 let sqrt1: DerivativeStructure = ds_x.pow_f64(0.5).unwrap();
                                 let sqrt2: DerivativeStructure = ds_x.sqrt().unwrap();
                                 let zero: DerivativeStructure = sqrt1.subtract(&sqrt2).unwrap();
                                 {
                                     let mut n: usize = 0;
                                    while n <= max_order {
                                        {
                                            assert_within_delta!(0.0, zero.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon[n]);
                                        }
                                        n += 1;
                                     }
                                 }

                            }
                            x += 0.001;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }
    
    
    #[test]
    pub fn  test_root_n_singularity()   {
         {
             let mut n: i32 = 2;
            while n < 10 {
                {
                     {
                         let mut max_order: usize = 0;
                        while max_order < 12 {
                            {
                                let ds_zero: DerivativeStructure = DerivativeStructure::new(1, max_order, 0, 0.0).unwrap();
                                let root_n: DerivativeStructure = ds_zero.root_n(n).unwrap();
                                assert_within_delta!(0.0, root_n.get_value(), 1.0e-20);
                                if max_order > 0 {
                                    assert!(&f64::is_infinite(root_n.get_partial_derivative(&vec![1]).unwrap()));
                                    assert!(root_n.get_partial_derivative(&vec![1]).unwrap() > 0.0);
                                     {
                                         let mut order: usize = 2;
                                        while order <= max_order {
                                            {
                                                // the following checks shows a LIMITATION of the current implementation
                                                // we have no way to tell dsZero is a pure linear variable x = 0
                                                // we only say: "dsZero is a structure with value = 0.0,
                                                // first derivative = 1.0, second and higher derivatives = 0.0".
                                                // Function composition rule for second derivatives is:
                                                // d2[f(g(x))]/dx2 = f''(g(x)) * [g'(x)]^2 + f'(g(x)) * g''(x)
                                                // when function f is the nth root and x = 0 we have:
                                                // f(0) = 0, f'(0) = +infinity, f''(0) = -infinity (and higher
                                                // derivatives keep switching between +infinity and -infinity)
                                                // so given that in our case dsZero represents g, we have g(x) = 0,
                                                // g'(x) = 1 and g''(x) = 0
                                                // applying the composition rules gives:
                                                // d2[f(g(x))]/dx2 = f''(g(x)) * [g'(x)]^2 + f'(g(x)) * g''(x)
                                                //                 = -infinity * 1^2 + +infinity * 0
                                                //                 = -infinity + NAN
                                                //                 = NAN
                                                // if we knew dsZero is really the x variable and not the identity
                                                // function applied to x, we would not have computed f'(g(x)) * g''(x)
                                                // and we would have found that the result was -infinity and not NAN
                                                assert!(f64::is_nan(root_n.get_partial_derivative(&vec![order as i32]).unwrap()));
                                            }
                                            order += 1;
                                         }
                                     }

                                }
                                // the following shows that the limitation explained above is NOT a bug...
                                // if we set up the higher order derivatives for g appropriately, we do
                                // compute the higher order derivatives of the composition correctly
                                 let mut g_derivatives = vec![0.0; 1 + max_order];
                                g_derivatives[0] = 0.0;
                                 {
                                     let mut k: usize = 1;
                                    while k <= max_order {
                                        {
                                            g_derivatives[k] = F64::pow_i32(-1.0, k as i32 + 1);
                                        }
                                        k += 1;
                                     }
                                 }

                                 let correct_root: DerivativeStructure = DerivativeStructure::new_parameters_order_values(1, max_order, &g_derivatives).unwrap().root_n(n).unwrap();
                                assert_within_delta!(0.0, correct_root.get_value(), 1.0e-20);
                                if max_order > 0 {
                                    assert!(f64::is_infinite(correct_root.get_partial_derivative(&vec![1]).unwrap()));
                                    assert!(correct_root.get_partial_derivative(&vec![1]).unwrap() > 0.0);
                                     {
                                         let mut order: usize = 2;
                                        while order <= max_order {
                                            {
                                                assert!(f64::is_infinite(correct_root.get_partial_derivative(&vec![order as i32]).unwrap()));
                                                if (order % 2) == 0 {
                                                    assert!(correct_root.get_partial_derivative(&vec![order as i32]).unwrap() < 0.0);
                                                } else {
                                                    assert!(correct_root.get_partial_derivative(&vec![order as i32]).unwrap() > 0.0);
                                                }
                                            }
                                            order += 1;
                                         }
                                     }

                                }
                            }
                            max_order += 1;
                         }
                     }

                }
                n += 1;
             }
         }

    }
    
    
    #[test]
    pub fn  test_sqrt_pow2()   {
         let epsilon = vec![1.0e-16, 3.0e-16, 2.0e-15, 6.0e-14, 6.0e-12, ];
         {
             let mut max_order: usize = 0;
            while max_order < 5 {
                {
                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(1, max_order, 0, x).unwrap();
                                 let rebuilt_x: DerivativeStructure = ds_x.multiply(&ds_x).unwrap().sqrt().unwrap();
                                 let zero: DerivativeStructure = rebuilt_x.subtract(&ds_x).unwrap();
                                 {
                                     let mut n: usize = 0;
                                    while n <= max_order {
                                        {
                                            assert_within_delta!(0.0, zero.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon[n]);
                                        }
                                        n += 1;
                                     }
                                 }

                            }
                            x += 0.001;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }

 #[test]
    pub fn  test_cbrt_definition()   {
         let epsilon = vec![4.0e-16, 9.0e-16, 6.0e-15, 2.0e-13, 4.0e-12, ]
        ;
         {
             let mut max_order: usize = 0;
            while max_order < 5 {
                {
                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(1, max_order, 0, x).unwrap();
                                 let cbrt1: DerivativeStructure = ds_x.pow_f64(1.0 / 3.0).unwrap();
                                 let cbrt2: DerivativeStructure = ds_x.cbrt().unwrap();
                                 let zero: DerivativeStructure = cbrt1.subtract(&cbrt2).unwrap();
                                 {
                                     let mut n: usize = 0;
                                    while n <= max_order {
                                        {
                                            assert_within_delta!(0.0, zero.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon[n]);
                                        }
                                        n += 1;
                                     }
                                 }

                            }
                            x += 0.001;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }


 #[test]
    pub fn  test_cbrt_pow3()   {
         let epsilon = vec![1.0e-16, 5.0e-16, 8.0e-15, 3.0e-13, 4.0e-11, ]
        ;
         {
            let mut max_order: usize = 0;
            while max_order < 5 {
                {
                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(1, max_order, 0, x).unwrap();
                                 let rebuilt_x: DerivativeStructure = ds_x.multiply(&ds_x.multiply(&ds_x).unwrap()).unwrap().cbrt().unwrap();
                                 let zero: DerivativeStructure = rebuilt_x.subtract(&ds_x).unwrap();
                                 {
                                     let mut n: usize = 0;
                                    while n <= max_order {
                                        {
                                            assert_within_delta!(0.0, zero.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon[n]);
                                        }
                                        n += 1;
                                     }
                                 }

                            }
                            x += 0.001;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }

 #[test]
    pub fn  test_pow_reciprocal_pow()   {
         let epsilon = vec![2.0e-15, 2.0e-14, 3.0e-13, 8.0e-12, 3.0e-10, ]
        ;
         {
             let mut max_order: usize = 0;
            while max_order < 5 {
                {
                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(2, max_order, 0, x).unwrap();
                                 {
                                     let mut y: f64 = 0.1;
                                    while y < 1.2 {
                                        {
                                             let ds_y: DerivativeStructure = DerivativeStructure::new(2, max_order, 1, y).unwrap();
                                             let rebuilt_x: DerivativeStructure = ds_x.pow(&ds_y).unwrap().pow(&ds_y.reciprocal().unwrap()).unwrap();
                                             let zero: DerivativeStructure = rebuilt_x.subtract(&ds_x).unwrap();
                                             {
                                                 let mut n: usize = 0;
                                                while n <= max_order {
                                                    {
                                                         {
                                                             let mut m: usize = 0;
                                                            while m <= max_order {
                                                                {
                                                                    if n + m <= max_order {
                                                                        assert_within_delta!(0.0, zero.get_partial_derivative(&vec![n as i32, m as i32]).unwrap(), epsilon[n + m]);
                                                                    }
                                                                }
                                                                m += 1;
                                                             }
                                                         }

                                                    }
                                                    n += 1;
                                                 }
                                             }

                                        }
                                        y += 0.01;
                                     }
                                 }

                            }
                            x += 0.01;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }


#[test]
    pub fn  test_hypot_definition()   {
         let epsilon: f64 = 1.0e-20;
         {
             let mut max_order: usize = 0;
            while max_order < 5 {
                {
                     {
                         let mut x: f64 = -1.7;
                        while x < 2.0 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(2, max_order, 0, x).unwrap();
                                 {
                                     let mut y: f64 = -1.7;
                                    while y < 2.0 {
                                        {
                                             let ds_y: DerivativeStructure = DerivativeStructure::new(2, max_order, 1, y).unwrap();
                                             let hypot: DerivativeStructure = DerivativeStructure::hypot_static(&ds_y, &ds_x).unwrap();
                                             let reference: DerivativeStructure = ds_x.multiply(&ds_x).unwrap().add(&ds_y.multiply(&ds_y).unwrap()).unwrap().sqrt().unwrap();
                                             let zero: DerivativeStructure = hypot.subtract(&reference).unwrap();
                                             {
                                                 let mut n: usize = 0;
                                                while n <= max_order {
                                                    {
                                                         {
                                                             let mut m: usize = 0;
                                                            while m <= max_order {
                                                                {
                                                                    if n + m <= max_order {
                                                                        assert_within_delta!(0.0, zero.get_partial_derivative(&vec![n as i32, m as i32]).unwrap(), epsilon);
                                                                    }
                                                                }
                                                                m += 1;
                                                             }
                                                         }

                                                    }
                                                    n += 1;
                                                 }
                                             }

                                        }
                                        y += 0.2;
                                     }
                                 }

                            }
                            x += 0.2;
                         }
                     }

                }
                max_order += 1;
             }
         }
    }


 #[test]
    pub fn  test_hypot_no_overflow()   {
         let ds_x: DerivativeStructure = DerivativeStructure::new(2, 5, 0, 3.0e250).unwrap();
         let ds_y: DerivativeStructure = DerivativeStructure::new(2, 5, 1, -4.0e250).unwrap();
         let hypot: DerivativeStructure = DerivativeStructure::hypot_static(&ds_x, &ds_y).unwrap();
        assert_within_delta!(5.0e250, hypot.get_value(), 1.0e235);
        assert_within_delta!(ds_x.get_value() / hypot.get_value(), hypot.get_partial_derivative(&vec![1, 0]).unwrap(), 1.0e-10);
        assert_within_delta!(ds_y.get_value() / hypot.get_value(), hypot.get_partial_derivative(&vec![0, 1]).unwrap(), 1.0e-10);
         let sqrt: DerivativeStructure = ds_x.multiply(&ds_x).unwrap().add(&ds_y.multiply(&ds_y).unwrap()).unwrap().sqrt().unwrap();
        assert!(f64::is_infinite(sqrt.get_value()));
    }

    #[test]
    pub fn  test_hypot_neglectible()   {
         let ds_small: DerivativeStructure = DerivativeStructure::new(2, 5, 0, 3.0e-10).unwrap();
         let ds_large: DerivativeStructure = DerivativeStructure::new(2, 5, 1, -4.0e25).unwrap();
        assert_within_delta!(ds_large.abs().unwrap().get_value(), DerivativeStructure::hypot_static(&ds_small, &ds_large).unwrap().get_value(), 1.0e-10);
        assert_within_delta!(0.0, DerivativeStructure::hypot_static(&ds_small, &ds_large).unwrap().get_partial_derivative(&vec![1, 0]).unwrap(), 1.0e-10);
        assert_within_delta!(-1.0, DerivativeStructure::hypot_static(&ds_small, &ds_large).unwrap().get_partial_derivative(&vec![0, 1]).unwrap(), 1.0e-10);
        assert_within_delta!(ds_large.abs().unwrap().get_value(), DerivativeStructure::hypot_static(&ds_large, &ds_small).unwrap().get_value(), 1.0e-10);
        assert_within_delta!(0.0, DerivativeStructure::hypot_static(&ds_large, &ds_small).unwrap().get_partial_derivative(&vec![1, 0]).unwrap(), 1.0e-10);
        assert_within_delta!(-1.0, DerivativeStructure::hypot_static(&ds_large, &ds_small).unwrap().get_partial_derivative(&vec![0, 1]).unwrap(), 1.0e-10);
    }
    
    
    #[test]
    pub fn  test_hypot_special()   {
        assert!(f64::is_nan(DerivativeStructure::hypot_static(&DerivativeStructure::new(2, 5, 0, f64::NAN).unwrap(), &DerivativeStructure::new(2, 5, 0, 3.0e250).unwrap()).unwrap().get_value()));
        assert!(f64::is_nan(DerivativeStructure::hypot_static(&DerivativeStructure::new(2, 5, 0, 3.0e250).unwrap(), &DerivativeStructure::new(2, 5, 0, f64::NAN).unwrap()).unwrap().get_value()));
        assert!(f64::is_infinite(DerivativeStructure::hypot(&DerivativeStructure::new(2, 5, 0, f64::INFINITY).unwrap(), &DerivativeStructure::new(2, 5, 0, 3.0e250).unwrap()).unwrap().get_value()));
        assert!(f64::is_infinite(DerivativeStructure::hypot(&DerivativeStructure::new(2, 5, 0, 3.0e250).unwrap(), &DerivativeStructure::new(2, 5, 0, f64::INFINITY).unwrap()).unwrap().get_value()));
    }

#[test]
    pub fn  test_primitive_remainder()   {
         let epsilon: f64 = 1.0e-15;
         {
             let mut max_order: usize = 0;
            while max_order < 5 {
                {
                     {
                         let mut x: f64 = -1.7;
                        while x < 2.0 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(2, max_order, 0, x).unwrap();
                                 {
                                     let mut y: f64 = -1.7;
                                    while y < 2.0 {
                                        {
                                             let remainder: DerivativeStructure = ds_x.remainder_f64(y).unwrap();
                                             let reference: DerivativeStructure = ds_x.subtract_f64(x - F64::ieee_remainder(x, y)).unwrap();
                                             let zero: DerivativeStructure = remainder.subtract(&reference).unwrap();
                                             {
                                                 let mut n: usize = 0;
                                                while n <= max_order {
                                                    {
                                                         {
                                                             let mut m: usize = 0;
                                                            while m <= max_order {
                                                                {
                                                                    if n + m <= max_order {
                                                                        assert_within_delta!(0.0, zero.get_partial_derivative(&vec![n as i32, m as i32]).unwrap(), epsilon);
                                                                    }
                                                                }
                                                                m += 1;
                                                             }
                                                         }

                                                    }
                                                    n += 1;
                                                 }
                                             }

                                        }
                                        y += 0.2;
                                     }
                                 }

                            }
                            x += 0.2;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }    
    
    #[test]
    pub fn  test_remainder()   {
         let epsilon: f64 = 2.0e-15;
         {
             let mut max_order: usize = 0;
            while max_order < 5 {
                {
                     {
                         let mut x: f64 = -1.7;
                        while x < 2.0 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(2, max_order, 0, x).unwrap();
                                 {
                                     let mut y: f64 = -1.7;
                                    while y < 2.0 {
                                        {
                                             let ds_y: DerivativeStructure = DerivativeStructure::new(2, max_order, 1, y).unwrap();
                                             let remainder: DerivativeStructure = ds_x.remainder(&ds_y).unwrap();
                                             let reference: DerivativeStructure = ds_x.subtract(&ds_y.multiply_f64((x - F64::ieee_remainder(x, y)) / y).unwrap()).unwrap();
                                             let zero: DerivativeStructure = remainder.subtract(&reference).unwrap();
                                             {
                                                 let mut n: usize = 0;
                                                while n <= max_order {
                                                    {
                                                         {
                                                             let mut m: usize = 0;
                                                            while m <= max_order {
                                                                {
                                                                    if n + m <= max_order {
                                                                        assert_within_delta!(0.0, zero.get_partial_derivative(&vec![n as i32, m as i32]).unwrap(), epsilon);
                                                                    }
                                                                }
                                                                m += 1;
                                                             }
                                                         }

                                                    }
                                                    n += 1;
                                                 }
                                             }

                                        }
                                        y += 0.2;
                                     }
                                 }

                            }
                            x += 0.2;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }

 #[test]
    pub fn  test_exp()   {
         let epsilon = vec![1.0e-16, 1.0e-16, 1.0e-16, 1.0e-16, 1.0e-16, ]
        ;
         {
             let mut max_order: usize = 0;
            while max_order < 5 {
                {
                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let ref_exp: f64 = F64::exp(x);
                                 let exp: DerivativeStructure = DerivativeStructure::new(1, max_order, 0, x).unwrap().exp().unwrap();
                                 {
                                     let mut n: usize = 0;
                                    while n <= max_order {
                                        {
                                            assert_within_delta!(ref_exp, exp.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon[n]);
                                        }
                                        n += 1;
                                     }
                                 }

                            }
                            x += 0.001;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }


    #[test]
    pub fn  test_expm1_definition()   {
         let epsilon: f64 = 3.0e-16;
         {
             let mut max_order: usize = 0;
            while max_order < 5 {
                {
                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(1, max_order, 0, x).unwrap();
                                 let expm11: DerivativeStructure = ds_x.expm1().unwrap();
                                 let expm12: DerivativeStructure = ds_x.exp().unwrap().subtract(&ds_x.get_field().get_one()).unwrap();
                                 let zero: DerivativeStructure = expm11.subtract(&expm12).unwrap();
                                 {
                                     let mut n: usize = 0;
                                    while n <= max_order {
                                        {
                                            assert_within_delta!(0.0, zero.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon);
                                        }
                                        n += 1;
                                     }
                                 }

                            }
                            x += 0.001;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }

 #[test]
    pub fn  test_log()   {
         let epsilon  = vec![1.0e-16, 1.0e-16, 3.0e-14, 7.0e-13, 3.0e-11, ]
        ;
         {
             let mut max_order: usize = 0;
            while max_order < 5 {
                {
                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let log: DerivativeStructure = DerivativeStructure::new(1, max_order, 0, x).unwrap().log().unwrap();
                                assert_within_delta!(F64::log(x), log.get_value(), epsilon[0]);
                                 {
                                     let mut n: usize = 1;
                                    while n <= max_order {
                                        {
                                             let ref_der: f64 = (-CombinatoricsUtils::factorial(n as i32 - 1).unwrap()) as f64 / F64::pow_i32(-x, n as i32);
                                            assert_within_delta!(ref_der, log.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon[n]);
                                        }
                                        n += 1;
                                     }
                                 }

                            }
                            x += 0.001;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }



    #[test]
    pub fn  test_log1p_definition()   {
         let epsilon: f64 = 3.0e-16;
         {
             let mut max_order: usize = 0;
            while max_order < 5 {
                {
                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(1, max_order, 0, x).unwrap();
                                 let log1p1: DerivativeStructure = ds_x.log1p().unwrap();
                                 let log1p2: DerivativeStructure = ds_x.add(&ds_x.get_field().get_one()).unwrap().log().unwrap();
                                 let zero: DerivativeStructure = log1p1.subtract(&log1p2).unwrap();
                                 {
                                     let mut n: usize = 0;
                                    while n <= max_order {
                                        {
                                            assert_within_delta!(0.0, zero.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon);
                                        }
                                        n += 1;
                                     }
                                 }

                            }
                            x += 0.001;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }

    #[test]
    pub fn  test_log10_definition()   {
         let epsilon  = vec![3.0e-16, 3.0e-16, 8.0e-15, 3.0e-13, 8.0e-12, ]
        ;
         {
             let mut max_order: usize = 0;
            while max_order < 5 {
                {
                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(1, max_order, 0, x).unwrap();
                                 let log101: DerivativeStructure = ds_x.log10().unwrap();
                                 let log102: DerivativeStructure = ds_x.log().unwrap().divide_f64(F64::log(10.0)).unwrap();
                                 let zero: DerivativeStructure = log101.subtract(&log102).unwrap();
                                 {
                                     let mut n: usize = 0;
                                    while n <= max_order {
                                        {
                                            assert_within_delta!(0.0, zero.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon[n]);
                                        }
                                        n += 1;
                                     }
                                 }

                            }
                            x += 0.001;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }

    #[test]
    pub fn  test_log_exp()   {
         let epsilon  = vec![2.0e-16, 2.0e-16, 3.0e-16, 2.0e-15, 6.0e-15, ]
        ;
         {
             let mut max_order: usize = 0;
            while max_order < 5 {
                {
                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(1, max_order, 0, x).unwrap();
                                 let rebuilt_x: DerivativeStructure = ds_x.exp().unwrap().log().unwrap();
                                 let zero: DerivativeStructure = rebuilt_x.subtract(&ds_x).unwrap();
                                 {
                                     let mut n: usize = 0;
                                    while n <= max_order {
                                        {
                                            assert_within_delta!(0.0, zero.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon[n]);
                                        }
                                        n += 1;
                                     }
                                 }

                            }
                            x += 0.001;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }

    #[test]
    pub fn  test_log1p_expm1()   {
         let epsilon  = vec![6.0e-17, 3.0e-16, 5.0e-16, 9.0e-16, 6.0e-15, ]
        ;
         {
             let mut max_order: usize = 0;
            while max_order < 5 {
                {
                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(1, max_order, 0, x).unwrap();
                                 let rebuilt_x: DerivativeStructure = ds_x.expm1().unwrap().log1p().unwrap();
                                 let zero: DerivativeStructure = rebuilt_x.subtract(&ds_x).unwrap();
                                 {
                                     let mut n: usize = 0;
                                    while n <= max_order {
                                        {
                                            assert_within_delta!(0.0, zero.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon[n]);
                                        }
                                        n += 1;
                                     }
                                 }

                            }
                            x += 0.001;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }

    #[test]
    pub fn  test_log10_power()   {
         let epsilon  = vec![3.0e-16, 3.0e-16, 9.0e-16, 6.0e-15, 6.0e-14, ]
        ;
         {
             let mut max_order: usize = 0;
            while max_order < 5 {
                {
                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(1, max_order, 0, x).unwrap();
                                 let rebuilt_x: DerivativeStructure = DerivativeStructure::new_parameters_order_value(1, max_order, 10.0).unwrap().pow(&ds_x).unwrap().log10().unwrap();
                                 let zero: DerivativeStructure = rebuilt_x.subtract(&ds_x).unwrap();
                                 {
                                     let mut n: usize = 0;
                                    while n <= max_order {
                                        {
                                            assert_within_delta!(0.0, zero.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon[n]);
                                        }
                                        n += 1;
                                     }
                                 }

                            }
                            x += 0.001;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }


       #[test]
    pub fn  test_sin_cos()   {
         let epsilon: f64 = 5.0e-16;
         {
             let mut max_order: usize = 0;
            while max_order < 6 {
                {
                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(1, max_order, 0, x).unwrap();
                                 let sin: DerivativeStructure = ds_x.sin().unwrap();
                                 let cos: DerivativeStructure = ds_x.cos().unwrap();
                                 let s: f64 = F64::sin(x);
                                 let c: f64 = F64::cos(x);
                                 {
                                     let mut n: usize = 0;
                                    while n <= max_order {
                                        {
                                            match n % 4 {
                                                  0 => 
                                                     {
                                                        assert_within_delta!(s, sin.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon);
                                                        assert_within_delta!(c, cos.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon);
                                                        break;
                                                    }
                                                  1 => 
                                                     {
                                                        assert_within_delta!(c, sin.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon);
                                                        assert_within_delta!(-s, cos.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon);
                                                        break;
                                                    }
                                                  2 => 
                                                     {
                                                        assert_within_delta!(-s, sin.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon);
                                                        assert_within_delta!(-c, cos.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon);
                                                        break;
                                                    }
                                                _ => 
                                                     {
                                                        assert_within_delta!(-c, sin.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon);
                                                        assert_within_delta!(s, cos.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon);
                                                        break;
                                                    }
                                            }
                                        }
                                        n += 1;
                                     }
                                 }

                            }
                            x += 0.001;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }



    #[test]
    pub fn  test_sin_asin()   {
         let epsilon  = vec![3.0e-16, 5.0e-16, 3.0e-15, 2.0e-14, 4.0e-13, ]
        ;
         {
             let mut max_order: usize = 0;
            while max_order < 5 {
                {
                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(1, max_order, 0, x).unwrap();
                                 let rebuilt_x: DerivativeStructure = ds_x.sin().unwrap().asin().unwrap();
                                 let zero: DerivativeStructure = rebuilt_x.subtract(&ds_x).unwrap();
                                 {
                                     let mut n: usize = 0;
                                    while n <= max_order {
                                        {
                                            assert_within_delta!(0.0, zero.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon[n]);
                                        }
                                        n += 1;
                                     }
                                 }

                            }
                            x += 0.001;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }

    #[test]
    pub fn  test_cos_acos()   {
         let epsilon  = vec![6.0e-16, 6.0e-15, 2.0e-13, 4.0e-12, 2.0e-10, ]
        ;
         {
             let mut max_order: usize = 0;
            while max_order < 5 {
                {
                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(1, max_order, 0, x).unwrap();
                                 let rebuilt_x: DerivativeStructure = ds_x.cos().unwrap().acos().unwrap();
                                 let zero: DerivativeStructure = rebuilt_x.subtract(&ds_x).unwrap();
                                 {
                                     let mut n: usize = 0;
                                    while n <= max_order {
                                        {
                                            assert_within_delta!(0.0, zero.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon[n]);
                                        }
                                        n += 1;
                                     }
                                 }

                            }
                            x += 0.001;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }

    #[test]
    pub fn  test_tan_atan()   {
         let epsilon  = vec![6.0e-17, 2.0e-16, 2.0e-15, 4.0e-14, 2.0e-12, ]
        ;
         {
             let mut max_order: usize = 0;
            while max_order < 5 {
                {
                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(1, max_order, 0, x).unwrap();
                                 let rebuilt_x: DerivativeStructure = ds_x.tan().unwrap().atan().unwrap();
                                 let zero: DerivativeStructure = rebuilt_x.subtract(&ds_x).unwrap();
                                 {
                                     let mut n: usize = 0;
                                    while n <= max_order {
                                        {
                                            assert_within_delta!(0.0, zero.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon[n]);
                                        }
                                        n += 1;
                                     }
                                 }

                            }
                            x += 0.001;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }
    
    
    #[test]
    pub fn  test_tangent_definition()   {
         let epsilon  = vec![5.0e-16, 2.0e-15, 3.0e-14, 5.0e-13, 2.0e-11, ]
        ;
         {
             let mut max_order: usize = 0;
            while max_order < 5 {
                {
                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(1, max_order, 0, x).unwrap();
                                 let tan1: DerivativeStructure = ds_x.sin().unwrap().divide(&ds_x.cos().unwrap()).unwrap();
                                 let tan2: DerivativeStructure = ds_x.tan().unwrap();
                                 let zero: DerivativeStructure = tan1.subtract(&tan2).unwrap();
                                 {
                                     let mut n: usize = 0;
                                    while n <= max_order {
                                        {
                                            assert_within_delta!(0.0, zero.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon[n], "max_order: {} x: {} n: {}", max_order, x, n);
                                        }
                                        n += 1;
                                     }
                                 }

                            }
                            x += 0.001;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }


    #[test]
    pub fn  test_atan2()   {
         let epsilon  = vec![5.0e-16, 3.0e-15, 2.2e-14, 1.0e-12, 8.0e-11, ]
        ;
         {
             let mut max_order: usize = 0;
            while max_order < 5 {
                {
                     {
                         let mut x: f64 = -1.7;
                        while x < 2.0 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(2, max_order, 0, x).unwrap();
                                 {
                                     let mut y: f64 = -1.7;
                                    while y < 2.0 {
                                        {
                                             let ds_y: DerivativeStructure = DerivativeStructure::new(2, max_order, 1, y).unwrap();
                                             let atan2: DerivativeStructure = DerivativeStructure::atan2_static(&ds_y, &ds_x).unwrap();
                                             let mut reference: DerivativeStructure = ds_y.divide(&ds_x).unwrap().atan().unwrap();
                                            if x < 0.0 {
                                                reference =  if y < 0.0 { reference.subtract_f64(fastmath::PI).unwrap() } else { reference.add_f64(fastmath::PI).unwrap() };
                                            }
                                             let zero: DerivativeStructure = atan2.subtract(&reference).unwrap();
                                             {
                                                 let mut n: usize = 0;
                                                while n <= max_order {
                                                    {
                                                         {
                                                             let mut m: usize = 0;
                                                            while m <= max_order {
                                                                {
                                                                    if n + m <= max_order {
                                                                        assert_within_delta!(0.0, zero.get_partial_derivative(&vec![n as i32, m as i32]).unwrap(), epsilon[n + m]);
                                                                    }
                                                                }
                                                                m += 1;
                                                             }
                                                         }

                                                    }
                                                    n += 1;
                                                 }
                                             }

                                        }
                                        y += 0.2;
                                     }
                                 }

                            }
                            x += 0.2;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }


    #[test]
    pub fn  test_atan2_special_cases()   {
         let pp: DerivativeStructure = DerivativeStructure::atan2_static(&DerivativeStructure::new(2, 2, 1, 0.0).unwrap(), &DerivativeStructure::new(2, 2, 1, 0.0).unwrap()).unwrap();
        assert_within_delta!(0.0, pp.get_value(), 1.0e-15);
        assert_within_delta!(1.0, F64::copy_sign(1.0, pp.get_value()), 1.0e-15);
         let pn: DerivativeStructure = DerivativeStructure::atan2_static(&DerivativeStructure::new(2, 2, 1, 0.0).unwrap(), &DerivativeStructure::new(2, 2, 1, -0.0).unwrap()).unwrap();
        assert_within_delta!(fastmath::PI, pn.get_value(), 1.0e-15);
         let np: DerivativeStructure = DerivativeStructure::atan2_static(&DerivativeStructure::new(2, 2, 1, -0.0).unwrap(), &DerivativeStructure::new(2, 2, 1, 0.0).unwrap()).unwrap();
        assert_within_delta!(0.0, np.get_value(), 1.0e-15);
        assert_within_delta!(-1.0, F64::copy_sign(1.0, np.get_value()), 1.0e-15);
         let nn: DerivativeStructure = DerivativeStructure::atan2_static(&DerivativeStructure::new(2, 2, 1, -0.0).unwrap(), &DerivativeStructure::new(2, 2, 1, -0.0).unwrap()).unwrap();
        assert_within_delta!(-fastmath::PI, nn.get_value(), 1.0e-15);
    }

    #[test]
    pub fn  test_sinh_definition()   {
         let epsilon  = vec![3.0e-16, 3.0e-16, 5.0e-16, 2.0e-15, 6.0e-15, ]
        ;
         {
             let mut max_order: usize = 0;
            while max_order < 5 {
                {
                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(1, max_order, 0, x).unwrap();
                                 let sinh1: DerivativeStructure = ds_x.exp().unwrap().subtract(&ds_x.exp().unwrap().reciprocal().unwrap()).unwrap().multiply_f64(0.5).unwrap();
                                 let sinh2: DerivativeStructure = ds_x.sinh().unwrap();
                                 let zero: DerivativeStructure = sinh1.subtract(&sinh2).unwrap();
                                 {
                                     let mut n: usize = 0;
                                    while n <= max_order {
                                        {
                                            assert_within_delta!(0.0, zero.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon[n]);
                                        }
                                        n += 1;
                                     }
                                 }

                            }
                            x += 0.001;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }

 #[test]
    pub fn  test_cosh_definition()   {
         let epsilon  = vec![3.0e-16, 3.0e-16, 5.0e-16, 2.0e-15, 6.0e-15, ]
        ;
         {
             let mut max_order: usize = 0;
            while max_order < 5 {
                {
                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(1, max_order, 0, x).unwrap();
                                 let cosh1: DerivativeStructure = ds_x.exp().unwrap().add(&ds_x.exp().unwrap().reciprocal().unwrap()).unwrap().multiply_f64(0.5).unwrap();
                                 let cosh2: DerivativeStructure = ds_x.cosh().unwrap();
                                 let zero: DerivativeStructure = cosh1.subtract(&cosh2).unwrap();
                                 {
                                     let mut n: usize = 0;
                                    while n <= max_order {
                                        {
                                            assert_within_delta!(0.0, zero.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon[n]);
                                        }
                                        n += 1;
                                     }
                                 }

                            }
                            x += 0.001;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }

    #[test]
    pub fn  test_tanh_definition()   {
         let epsilon  = vec![3.0e-16, 5.0e-16, 7.0e-16, 3.0e-15, 2.0e-14, ]
        ;
         {
             let mut max_order: usize = 0;
            while max_order < 5 {
                {
                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(1, max_order, 0, x).unwrap();
                                 let tanh1: DerivativeStructure = 
                                 ds_x.exp().unwrap()
	                                 .subtract(&ds_x.exp().unwrap().reciprocal().unwrap()).unwrap()
	                                 .divide(&ds_x.exp().unwrap().add(&ds_x.exp().unwrap().reciprocal().unwrap()).unwrap()).unwrap();
                                 let tanh2: DerivativeStructure = ds_x.tanh().unwrap();
                                 let zero: DerivativeStructure = tanh1.subtract(&tanh2).unwrap();
                                 {
                                     let mut n: usize = 0;
                                    while n <= max_order {
                                        {
                                            assert_within_delta!(0.0, zero.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon[n]);
                                        }
                                        n += 1;
                                     }
                                 }

                            }
                            x += 0.001;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }

    #[test]
    pub fn  test_sinh_asinh()   {
         let epsilon  = vec![3.0e-16, 3.0e-16, 4.0e-16, 7.0e-16, 3.0e-15, 8.0e-15, ]
        ;
         {
             let mut max_order: usize = 0;
            while max_order < 6 {
                {
                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(1, max_order, 0, x).unwrap();
                                 let rebuilt_x: DerivativeStructure = ds_x.sinh().unwrap().asinh().unwrap();
                                 let zero: DerivativeStructure = rebuilt_x.subtract(&ds_x).unwrap();
                                 {
                                     let mut n: usize = 0;
                                    while n <= max_order {
                                        {
                                            assert_within_delta!(0.0, zero.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon[n]);
                                        }
                                        n += 1;
                                     }
                                 }

                            }
                            x += 0.001;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }

    #[test]
    pub fn  test_cosh_acosh()   {
         let epsilon  = vec![2.0e-15, 1.0e-14, 2.0e-13, 6.0e-12, 3.0e-10, 2.0e-8, ]
        ;
         {
             let mut max_order: usize = 0;
            while max_order < 6 {
                {
                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(1, max_order, 0, x).unwrap();
                                 let rebuilt_x: DerivativeStructure = ds_x.cosh().unwrap().acosh().unwrap();
                                 let zero: DerivativeStructure = rebuilt_x.subtract(&ds_x).unwrap();
                                 {
                                     let mut n: usize = 0;
                                    while n <= max_order {
                                        {
                                            assert_within_delta!(0.0, zero.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon[n]);
                                        }
                                        n += 1;
                                     }
                                 }

                            }
                            x += 0.001;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }

    #[test]
    pub fn  test_tanh_atanh()   {
         let epsilon  = vec![3.0e-16, 2.0e-16, 7.0e-16, 4.0e-15, 3.0e-14, 4.0e-13, ]
        ;
         {
             let mut max_order: usize = 0;
            while max_order < 6 {
                {
                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(1, max_order, 0, x).unwrap();
                                 let rebuilt_x: DerivativeStructure = ds_x.tanh().unwrap().atanh().unwrap();
                                 let zero: DerivativeStructure = rebuilt_x.subtract(&ds_x).unwrap();
                                 {
                                     let mut n: usize = 0;
                                    while n <= max_order {
                                        {
                                            assert_within_delta!(0.0, zero.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon[n]);
                                        }
                                        n += 1;
                                     }
                                 }

                            }
                            x += 0.001;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }


#[test]
    pub fn  test_composition_one_variable_y()   {
         let epsilon: f64 = 1.0e-13;
         {
             let mut max_order: usize = 0;
            while max_order < 5 {
                {
                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new_parameters_order_value(1, max_order, x).unwrap();
                                 {
                                     let mut y: f64 = 0.1;
                                    while y < 1.2 {
                                        {
                                             let ds_y: DerivativeStructure = DerivativeStructure::new(1, max_order, 0, y).unwrap();
                                             let f: DerivativeStructure = ds_x.divide(&ds_y).unwrap().sqrt().unwrap();
                                             let f0: f64 = F64::sqrt(x / y);
                                            assert_within_delta!(f0, f.get_value(), F64::abs(epsilon * f0));
                                            if f.get_order() > 0 {
                                                 let f1: f64 = -x / (2.0 * y * y * f0);
                                                assert_within_delta!(f1, f.get_partial_derivative(&vec![1]).unwrap(), F64::abs(epsilon * f1));
                                                if f.get_order() > 1 {
                                                     let f2: f64 = (f0 - x / (4.0 * y * f0)) / (y * y);
                                                    assert_within_delta!(f2, f.get_partial_derivative(&vec![2]).unwrap(), F64::abs(epsilon * f2));
                                                    if f.get_order() > 2 {
                                                         let f3: f64 = (x / (8.0 * y * f0) - 2.0 * f0) / (y * y * y);
                                                        assert_within_delta!(f3, f.get_partial_derivative(&vec![3]).unwrap(), F64::abs(epsilon * f3));
                                                    }
                                                }
                                            }
                                        }
                                        y += 0.1;
                                     }
                                 }

                            }
                            x += 0.1;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }
    
     #[test]
    pub fn  test_taylor_polynomial()   {
         {
             let mut x: f64 = 0.0;
            while x < 1.2 {
                {
                     let ds_x: DerivativeStructure = DerivativeStructure::new(3, 4, 0, x).unwrap();
                     {
                         let mut y: f64 = 0.0;
                        while y < 1.2 {
                            {
                                 let ds_y: DerivativeStructure = DerivativeStructure::new(3, 4, 1, y).unwrap();
                                 {
                                     let mut z: f64 = 0.0;
                                    while z < 1.2 {
                                        {
                                             let ds_z: DerivativeStructure = DerivativeStructure::new(3, 4, 2, z).unwrap();
                                             let f: DerivativeStructure = ds_x.multiply(&ds_y).unwrap().add(&ds_z).unwrap().multiply(&ds_x).unwrap().multiply(&ds_y).unwrap();
                                             {
                                                 let mut dx: f64 = -0.2;
                                                while dx < 0.2 {
                                                    {
                                                         {
                                                             let mut dy: f64 = -0.2;
                                                            while dy < 0.2 {
                                                                {
                                                                     {
                                                                         let mut dz: f64 = -0.2;
                                                                        while dz < 0.2 {
                                                                            {
                                                                                 let reference: f64 = (x + dx) * (y + dy) * ((x + dx) * (y + dy) + (z + dz));
                                                                                assert_within_delta!(reference, f.taylor(&vec![dx, dy, dz]).unwrap(), 2.0e-15);
                                                                            }
                                                                            dz += 0.1;
                                                                         }
                                                                     }

                                                                }
                                                                dy += 0.1;
                                                             }
                                                         }

                                                    }
                                                    dx += 0.2;
                                                 }
                                             }

                                        }
                                        z += 0.2;
                                     }
                                 }

                            }
                            y += 0.2;
                         }
                     }

                }
                x += 0.1;
             }
         }

    }
    
    
     #[test]
    pub fn  test_taylor_atan2()   {
         let expected  = vec![0.214, 0.0241, 0.00422, 6.48e-4, 8.04e-5, ]
        ;
         let x0: f64 = 0.1;
         let y0: f64 = -0.3;
         {
             let mut max_order: usize = 0;
            while max_order < 5 {
                {
                     let ds_x: DerivativeStructure = DerivativeStructure::new(2, max_order, 0, x0).unwrap();
                     let ds_y: DerivativeStructure = DerivativeStructure::new(2, max_order, 1, y0).unwrap();
                     let atan2: DerivativeStructure = DerivativeStructure::atan2(&ds_y, &ds_x).unwrap();
                     let mut max_error: f64 = 0.0;
                     {
                         let mut dx: f64 = -0.05;
                        while dx < 0.05 {
                            {
                                 {
                                     let mut dy: f64 = -0.05;
                                    while dy < 0.05 {
                                        {
                                             let reference: f64 = F64::atan2(y0 + dy, x0 + dx);
                                            max_error = F64::max(max_error, F64::abs(reference - atan2.taylor(&vec![dx, dy]).unwrap()));
                                        }
                                        dy += 0.001;
                                     }
                                 }

                            }
                            dx += 0.001;
                         }
                     }

                    assert_within_delta!(0.0, expected[max_order] - max_error, 0.01 * expected[max_order]);
                }
                max_order += 1;
             }
         }

    }
    
    
    #[test]
    pub fn  test_abs()   {
         let minus_one: DerivativeStructure = DerivativeStructure::new(1, 1, 0, -1.0).unwrap();
        assert_within_delta!(1.0, minus_one.abs().unwrap().get_partial_derivative(&vec![0]).unwrap(), 1.0e-15);
        assert_within_delta!(-1.0, minus_one.abs().unwrap().get_partial_derivative(&vec![1]).unwrap(), 1.0e-15);
         let plus_one: DerivativeStructure = DerivativeStructure::new(1, 1, 0, 1.0).unwrap();
        assert_within_delta!(1.0, plus_one.abs().unwrap().get_partial_derivative(&vec![0]).unwrap(), 1.0e-15);
        assert_within_delta!(1.0, plus_one.abs().unwrap().get_partial_derivative(&vec![1]).unwrap(), 1.0e-15);
         let minus_zero: DerivativeStructure = DerivativeStructure::new(1, 1, 0, -0.0).unwrap();
        assert_within_delta!(0.0, minus_zero.abs().unwrap().get_partial_derivative(&vec![0]).unwrap(), 1.0e-15);
        assert_within_delta!(-1.0, minus_zero.abs().unwrap().get_partial_derivative(&vec![1]).unwrap(), 1.0e-15);
         let plus_zero: DerivativeStructure = DerivativeStructure::new(1, 1, 0, 0.0).unwrap();
        assert_within_delta!(0.0, plus_zero.abs().unwrap().get_partial_derivative(&vec![0]).unwrap(), 1.0e-15);
        assert_within_delta!(1.0, plus_zero.abs().unwrap().get_partial_derivative(&vec![1]).unwrap(), 1.0e-15);
    }
    
     #[test]
    pub fn  test_signum()   {
         let minus_one: DerivativeStructure = DerivativeStructure::new(1, 1, 0, -1.0).unwrap();
        assert_within_delta!(-1.0, minus_one.signum().unwrap().get_partial_derivative(&vec![0]).unwrap(), 1.0e-15);
        assert_within_delta!(0.0, minus_one.signum().unwrap().get_partial_derivative(&vec![1]).unwrap(), 1.0e-15);
         let plus_one: DerivativeStructure = DerivativeStructure::new(1, 1, 0, 1.0).unwrap();
        assert_within_delta!(1.0, plus_one.signum().unwrap().get_partial_derivative(&vec![0]).unwrap(), 1.0e-15);
        assert_within_delta!(0.0, plus_one.signum().unwrap().get_partial_derivative(&vec![1]).unwrap(), 1.0e-15);
         let minus_zero: DerivativeStructure = DerivativeStructure::new(1, 1, 0, -0.0).unwrap();
        assert_within_delta!(-0.0, minus_zero.signum().unwrap().get_partial_derivative(&vec![0]).unwrap(), 1.0e-15);
        assert!(rsutils::double_to_raw_long_bits(&minus_zero.signum().unwrap().get_value()) & 0x80000000_00000000 != 0);
        assert_within_delta!(0.0, minus_zero.signum().unwrap().get_partial_derivative(&vec![1]).unwrap(), 1.0e-15);
         let plus_zero: DerivativeStructure = DerivativeStructure::new(1, 1, 0, 0.0).unwrap();
        assert_within_delta!(0.0, plus_zero.signum().unwrap().get_partial_derivative(&vec![0]).unwrap(), 1.0e-15);
        assert!(rsutils::double_to_raw_long_bits(&plus_zero.signum().unwrap().get_value()) == 0);
        assert_within_delta!(0.0, plus_zero.signum().unwrap().get_partial_derivative(&vec![1]).unwrap(), 1.0e-15);
    }

    #[test]
    pub fn  test_ceil_floor_rint_long()   {
         let x: DerivativeStructure = DerivativeStructure::new(1, 1, 0, -1.5).unwrap();
        assert_within_delta!(-1.5, x.get_partial_derivative(&vec![0]).unwrap(), 1.0e-15);
        assert_within_delta!(1.0, x.get_partial_derivative(&vec![1]).unwrap(), 1.0e-15);
        assert_within_delta!(-1.0, x.ceil().unwrap().get_partial_derivative(&vec![0]).unwrap(), 1.0e-15);
        assert_within_delta!(0.0, x.ceil().unwrap().get_partial_derivative(&vec![1]).unwrap(), 1.0e-15);
        assert_within_delta!(-2.0, x.floor().unwrap().get_partial_derivative(&vec![0]).unwrap(), 1.0e-15);
        assert_within_delta!(0.0, x.floor().unwrap().get_partial_derivative(&vec![1]).unwrap(), 1.0e-15);
        assert_within_delta!(-2.0, x.rint().unwrap().get_partial_derivative(&vec![0]).unwrap(), 1.0e-15);
        assert_within_delta!(0.0, x.rint().unwrap().get_partial_derivative(&vec![1]).unwrap(), 1.0e-15);
        assert_within_delta!(-2.0, x.subtract(&x.get_field().get_one()).unwrap().rint().unwrap().get_partial_derivative(&vec![0]).unwrap(), 1.0e-15);
        assert_eq!(-1, x.round());
    }

    #[test]
    pub fn  test_copy_sign()   {
         let minus_one: DerivativeStructure = DerivativeStructure::new(1, 1, 0, -1.0).unwrap();
        assert_within_delta!(1.0, minus_one.copy_sign_f64(1.0).unwrap().get_partial_derivative(&vec![0]).unwrap(), 1.0e-15);
        assert_within_delta!(-1.0, minus_one.copy_sign_f64(1.0).unwrap().get_partial_derivative(&vec![1]).unwrap(), 1.0e-15);
        assert_within_delta!(-1.0, minus_one.copy_sign_f64(-1.0).unwrap().get_partial_derivative(&vec![0]).unwrap(), 1.0e-15);
        assert_within_delta!(1.0, minus_one.copy_sign_f64(-1.0).unwrap().get_partial_derivative(&vec![1]).unwrap(), 1.0e-15);
        assert_within_delta!(1.0, minus_one.copy_sign_f64(0.0).unwrap().get_partial_derivative(&vec![0]).unwrap(), 1.0e-15);
        assert_within_delta!(-1.0, minus_one.copy_sign_f64(0.0).unwrap().get_partial_derivative(&vec![1]).unwrap(), 1.0e-15);
        assert_within_delta!(-1.0, minus_one.copy_sign_f64(-0.0).unwrap().get_partial_derivative(&vec![0]).unwrap(), 1.0e-15);
        assert_within_delta!(1.0, minus_one.copy_sign_f64(-0.0).unwrap().get_partial_derivative(&vec![1]).unwrap(), 1.0e-15);
        assert_within_delta!(1.0, minus_one.copy_sign_f64(f64::NAN).unwrap().get_partial_derivative(&vec![0]).unwrap(), 1.0e-15);
        assert_within_delta!(-1.0, minus_one.copy_sign_f64(f64::NAN).unwrap().get_partial_derivative(&vec![1]).unwrap(), 1.0e-15);
         let plus_one: DerivativeStructure = DerivativeStructure::new(1, 1, 0, 1.0).unwrap();
        assert_within_delta!(1.0, plus_one.copy_sign_f64(1.0).unwrap().get_partial_derivative(&vec![0]).unwrap(), 1.0e-15);
        assert_within_delta!(1.0, plus_one.copy_sign_f64(1.0).unwrap().get_partial_derivative(&vec![1]).unwrap(), 1.0e-15);
        assert_within_delta!(-1.0, plus_one.copy_sign_f64(-1.0).unwrap().get_partial_derivative(&vec![0]).unwrap(), 1.0e-15);
        assert_within_delta!(-1.0, plus_one.copy_sign_f64(-1.0).unwrap().get_partial_derivative(&vec![1]).unwrap(), 1.0e-15);
        assert_within_delta!(1.0, plus_one.copy_sign_f64(0.0).unwrap().get_partial_derivative(&vec![0]).unwrap(), 1.0e-15);
        assert_within_delta!(1.0, plus_one.copy_sign_f64(0.0).unwrap().get_partial_derivative(&vec![1]).unwrap(), 1.0e-15);
        assert_within_delta!(-1.0, plus_one.copy_sign_f64(-0.0).unwrap().get_partial_derivative(&vec![0]).unwrap(), 1.0e-15);
        assert_within_delta!(-1.0, plus_one.copy_sign_f64(-0.0).unwrap().get_partial_derivative(&vec![1]).unwrap(), 1.0e-15);
        assert_within_delta!(1.0, plus_one.copy_sign_f64(f64::NAN).unwrap().get_partial_derivative(&vec![0]).unwrap(), 1.0e-15);
        assert_within_delta!(1.0, plus_one.copy_sign_f64(f64::NAN).unwrap().get_partial_derivative(&vec![1]).unwrap(), 1.0e-15);
    }


#[test]
    pub fn  test_to_degrees_definition()   {
         let epsilon: f64 = 3.0e-16;
         {
             let mut max_order: usize = 0;
            while max_order < 6 {
                {
                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(1, max_order, 0, x).unwrap();
                                assert_within_delta!(F64::to_degrees(x), ds_x.to_degrees().unwrap().get_value(), epsilon);
                                 {
                                     let mut n: usize = 1;
                                    while n <= max_order {
                                        {
                                            if n == 1 {
                                                assert_within_delta!(180.0 / fastmath::PI, ds_x.to_degrees().unwrap().get_partial_derivative(&vec![1]).unwrap(), epsilon);
                                            } else {
                                                assert_within_delta!(0.0, ds_x.to_degrees().unwrap().get_partial_derivative(&vec![n as i32]).unwrap(), epsilon);
                                            }
                                        }
                                        n += 1;
                                     }
                                 }

                            }
                            x += 0.001;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }

    #[test]
    pub fn  test_to_radians_definition()   {
         let epsilon: f64 = 3.0e-16;
         {
             let mut max_order: usize = 0;
            while max_order < 6 {
                {
                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(1, max_order, 0, x).unwrap();
                                assert_within_delta!(F64::to_radians(x), ds_x.to_radians().unwrap().get_value(), epsilon);
                                 {
                                     let mut n: usize = 1;
                                    while n <= max_order {
                                        {
                                            if n == 1 {
                                                assert_within_delta!(fastmath::PI / 180.0, ds_x.to_radians().unwrap().get_partial_derivative(&vec![1]).unwrap(), epsilon);
                                            } else {
                                                assert_within_delta!(0.0, ds_x.to_radians().unwrap().get_partial_derivative(&vec![n as i32]).unwrap(), epsilon);
                                            }
                                        }
                                        n += 1;
                                     }
                                 }

                            }
                            x += 0.001;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }

    #[test]
    pub fn  test_deg_rad()   {
         let epsilon: f64 = 3.0e-16;
         {
             let mut max_order: usize = 0;
            while max_order < 6 {
                {
                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(1, max_order, 0, x).unwrap();
                                 let rebuilt_x: DerivativeStructure = ds_x.to_degrees().unwrap().to_radians().unwrap();
                                 let zero: DerivativeStructure = rebuilt_x.subtract(&ds_x).unwrap();
                                 {
                                     let mut n: usize = 0;
                                    while n <= max_order {
                                        {
                                            assert_within_delta!(0.0, zero.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon);
                                        }
                                        n += 1;
                                     }
                                 }

                            }
                            x += 0.001;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }


   #[test]
    pub fn  test_compose_mismatched_dimensions()   {
        assert!(DerivativeStructure::new(1, 3, 0, 1.2).unwrap().compose(&vec![0.0; 3]).is_err());
    }



     #[test]
    pub fn  test_compose()   {
         let epsilon  = vec![1.0e-20, 5.0e-14, 2.0e-13, 3.0e-13, 2.0e-13, 1.0e-20, ]
        ;
         let poly: PolynomialFunction = PolynomialFunction::new(&vec![1.0, 2.0, 3.0, 4.0, 5.0, 6.0, ]).unwrap();
         {
             let mut max_order: usize = 0;
            while max_order < 6 {
                {
                     let mut p = Vec::new();
                    p.push(poly.clone());
                     {
                         let mut i: usize = 1;
                        while i <= max_order {
                            {
                                let p_derivative = p[i - 1].polynomial_derivative().unwrap();
                                p.push(p_derivative);
                            }
                            i += 1;
                         }
                     }

                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(1, max_order, 0, x).unwrap();
                                 let mut ds_y1: DerivativeStructure = ds_x.get_field().get_zero();
                                 {
                                     let mut i: usize = poly.degree() as usize;
                                    loop {
                                        {
                                            ds_y1 = ds_y1.multiply(&ds_x).unwrap().add_f64(poly.get_coefficients()[i]).unwrap();
                                        }
                                        if i == 0 { break; }
                                        i -= 1;
                                     }
                                 }

                                 let mut f = Vec::new();
                                 {
                                     let mut i: usize = 0;
                                    while i < max_order + 1 {
                                        {
                                            f.push(p[i].value(x));
                                        }
                                        i += 1;
                                     }
                                 }

                                 let ds_y2: DerivativeStructure = ds_x.compose(&f).unwrap();
                                 let zero: DerivativeStructure = ds_y1.subtract(&ds_y2).unwrap();
                                 {
                                     let mut n: usize = 0;
                                    while n <= max_order {
                                        {
                                            assert_within_delta!(0.0, zero.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon[n]);
                                        }
                                        n += 1;
                                     }
                                 }

                            }
                            x += 0.001;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }

    


 #[test]
    pub fn  test_field()   {
         {
             let mut max_order: usize = 1;
            while max_order < 5 {
                {
                     let x: DerivativeStructure = DerivativeStructure::new(3, max_order, 0, 1.0).unwrap();
                    check_f0_f1(&x.get_field().get_zero(), 0.0, &vec![0.0, 0.0, 0.0]);
                    check_f0_f1(&x.get_field().get_one(), 1.0, &vec![0.0, 0.0, 0.0]);
                    assert_eq!(max_order, x.get_field().get_zero().get_order());
                    assert_eq!(3, x.get_field().get_zero().get_free_parameters());
                }
                max_order += 1;
             }
         }
    }

    #[test]
    pub fn  test_one_parameter_constructor()   {
         let x: f64 = 1.2;
         let cos: f64 = F64::cos(x);
         let sin: f64 = F64::sin(x);
         let y_ref: DerivativeStructure = DerivativeStructure::new(1, 4, 0, x).unwrap().cos().unwrap();
         assert!(DerivativeStructure::new_parameters_order_values(1, 4, &vec![0.0, 0.0]).is_err());

         let derivatives = vec![cos, -sin, -cos, sin, cos, ]
        ;
         let y: DerivativeStructure = DerivativeStructure::new_parameters_order_values(1, 4, &derivatives).unwrap();
        check_equals(&y_ref, &y, 1.0e-15);
        TestUtils::assert_equals_f64_arrays(&derivatives, &y.get_all_derivatives(), 1.0e-15);
    }

    #[test]
    pub fn  test_one_order_constructor()   {
         let x: f64 = 1.2;
         let y: f64 = 2.4;
         let z: f64 = 12.5;
         let x_ref: DerivativeStructure = DerivativeStructure::new(3, 1, 0, x).unwrap();
         let y_ref: DerivativeStructure = DerivativeStructure::new(3, 1, 1, y).unwrap();
         let z_ref: DerivativeStructure = DerivativeStructure::new(3, 1, 2, z).unwrap();
         assert!(DerivativeStructure::new_parameters_order_values(3, 1, &vec![x + y - z, 1.0, 1.0]).is_err());

         let derivatives = vec![x + y - z, 1.0, 1.0, -1.0, ]
        ;
         let t: DerivativeStructure = DerivativeStructure::new_parameters_order_values(3, 1, &derivatives).unwrap();
        check_equals(&x_ref.add(&y_ref.subtract(&z_ref).unwrap()).unwrap(), &t, 1.0e-15);
        TestUtils::assert_equals_f64_arrays(&derivatives, &x_ref.add(&y_ref.subtract(&z_ref).unwrap()).unwrap().get_all_derivatives(), 1.0e-15);
    }

    #[test]
    pub fn  test_linear_combination1_d_s_d_s()   {
         let a  = vec![
            DerivativeStructure::new(6, 1, 0, -1321008684645961.0 / 268435456.0).unwrap(),
            DerivativeStructure::new(6, 1, 1, -5774608829631843.0 / 268435456.0).unwrap(),
            DerivativeStructure::new(6, 1, 2, -7645843051051357.0 / 8589934592.0).unwrap(), ]
        ;
         let b  = vec![
            DerivativeStructure::new(6, 1, 3, -5712344449280879.0 / 2097152.0).unwrap(),
            DerivativeStructure::new(6, 1, 4, -4550117129121957.0 / 2097152.0).unwrap(),
            DerivativeStructure::new(6, 1, 5, 8846951984510141.0 / 131072.0).unwrap(), ]
        ;
         let ab_sum_inline: DerivativeStructure = a[0].linear_combination_3_factors(&a[0], &b[0], &a[1], &b[1], &a[2], &b[2]).unwrap();
         let ab_sum_array: DerivativeStructure = a[0].linear_combination_vec(&a, &b).unwrap();
        assert_within_delta!(ab_sum_inline.get_value(), ab_sum_array.get_value(), 0.0);
        assert_within_delta!(-1.8551294182586248737720779899, ab_sum_inline.get_value(), 1.0e-15);
        assert_within_delta!(b[0].get_value(), ab_sum_inline.get_partial_derivative(&vec![1, 0, 0, 0, 0, 0]).unwrap(), 1.0e-15);
        assert_within_delta!(b[1].get_value(), ab_sum_inline.get_partial_derivative(&vec![0, 1, 0, 0, 0, 0]).unwrap(), 1.0e-15);
        assert_within_delta!(b[2].get_value(), ab_sum_inline.get_partial_derivative(&vec![0, 0, 1, 0, 0, 0]).unwrap(), 1.0e-15);
        assert_within_delta!(a[0].get_value(), ab_sum_inline.get_partial_derivative(&vec![0, 0, 0, 1, 0, 0]).unwrap(), 1.0e-15);
        assert_within_delta!(a[1].get_value(), ab_sum_inline.get_partial_derivative(&vec![0, 0, 0, 0, 1, 0]).unwrap(), 1.0e-15);
        assert_within_delta!(a[2].get_value(), ab_sum_inline.get_partial_derivative(&vec![0, 0, 0, 0, 0, 1]).unwrap(), 1.0e-15);
    }

    #[test]
    pub fn  test_linear_combination1_double_d_s()   {
         let a  = vec![-1321008684645961.0 / 268435456.0, -5774608829631843.0 / 268435456.0, -7645843051051357.0 / 8589934592.0, ]
        ;
         let b = vec![
            DerivativeStructure::new(3, 1, 0, -5712344449280879.0 / 2097152.0).unwrap(),
            DerivativeStructure::new(3, 1, 1, -4550117129121957.0 / 2097152.0).unwrap(),
            DerivativeStructure::new(3, 1, 2, 8846951984510141.0 / 131072.0).unwrap(), ]
        ;
         let ab_sum_inline: DerivativeStructure = b[0].linear_combination_f64_3_factors(a[0], &b[0], a[1], &b[1], a[2], &b[2]).unwrap();
         let ab_sum_array: DerivativeStructure = b[0].linear_combination_f64_vec(&a, &b).unwrap();
        assert_within_delta!(ab_sum_inline.get_value(), ab_sum_array.get_value(), 0.0);
        assert_within_delta!(-1.8551294182586248737720779899, ab_sum_inline.get_value(), 1.0e-15);
        assert_within_delta!(a[0], ab_sum_inline.get_partial_derivative(&vec![1, 0, 0]).unwrap(), 1.0e-15);
        assert_within_delta!(a[1], ab_sum_inline.get_partial_derivative(&vec![0, 1, 0]).unwrap(), 1.0e-15);
        assert_within_delta!(a[2], ab_sum_inline.get_partial_derivative(&vec![0, 0, 1]).unwrap(), 1.0e-15);
    }
*/


    #[test]
    pub fn  test_linear_combination2_d_s_d_s()   {
        // we compare accurate versus naive dot product implementations
        // on regular vectors (i.e. not extreme cases like in the previous test)
        let mut random = WellGeneratorFactory::new_well_1024a();
        random.set_seed_i64(&(0xc6af886975069f11u64 as i64));
         {
             let mut i: usize = 0;
            while i < 10000 {

                     let mut u = Vec::new();
                     let mut v = Vec::new();
                    for j in 0..4 {
                        u.push(DerivativeStructure::new(4, 1, j, 1e17 * random.next_double()).unwrap());
                        v.push(DerivativeStructure::new_parameters_order_value(4, 1, 1e17 * random.next_double()).unwrap());
                    }


                     let mut lin: DerivativeStructure = u[0].linear_combination_2_factors(&u[0], &v[0], &u[1], &v[1]).unwrap();
                     let mut reference: f64 = u[0].get_value() * v[0].get_value() + u[1].get_value() * v[1].get_value();
                    assert_within_delta!(reference, lin.get_value(), 1.0e-15 * F64::abs(reference));
                    assert_within_delta!(v[0].get_value(), lin.get_partial_derivative(&vec![1, 0, 0, 0]).unwrap(), 1.0e-15 * F64::abs(v[0].get_value()));
                    assert_within_delta!(v[1].get_value(), lin.get_partial_derivative(&vec![0, 1, 0, 0]).unwrap(), 1.0e-15 * F64::abs(v[1].get_value()));
                    lin = u[0].linear_combination_3_factors(&u[0], &v[0], &u[1],& v[1], &u[2], &v[2]).unwrap();
                    reference = u[0].get_value() * v[0].get_value() + u[1].get_value() * v[1].get_value() + u[2].get_value() * v[2].get_value();
                    assert_within_delta!(reference, lin.get_value(), 1.0e-15 * F64::abs(reference));
                    assert_within_delta!(v[0].get_value(), lin.get_partial_derivative(&vec![1, 0, 0, 0]).unwrap(), 1.0e-15 * F64::abs(v[0].get_value()));
                    assert_within_delta!(v[1].get_value(), lin.get_partial_derivative(&vec![0, 1, 0, 0]).unwrap(), 1.0e-15 * F64::abs(v[1].get_value()));
                    assert_within_delta!(v[2].get_value(), lin.get_partial_derivative(&vec![0, 0, 1, 0]).unwrap(), 1.0e-15 * F64::abs(v[2].get_value()));
                    lin = u[0].linear_combination_4_factors(&u[0], &v[0], &u[1],& v[1], &u[2], &v[2], &u[3], &v[3]).unwrap();
                    reference = u[0].get_value() * v[0].get_value() + u[1].get_value() * v[1].get_value() + u[2].get_value() * v[2].get_value() + u[3].get_value() * v[3].get_value();
                    assert_within_delta!(reference, lin.get_value(), 1.0e-15 * F64::abs(reference));
                    assert_within_delta!(v[0].get_value(), lin.get_partial_derivative(&vec![1, 0, 0, 0]).unwrap(), 1.0e-15 * F64::abs(v[0].get_value()));
                    assert_within_delta!(v[1].get_value(), lin.get_partial_derivative(&vec![0, 1, 0, 0]).unwrap(), 1.0e-15 * F64::abs(v[1].get_value()));
                    assert_within_delta!(v[2].get_value(), lin.get_partial_derivative(&vec![0, 0, 1, 0]).unwrap(), 1.0e-15 * F64::abs(v[2].get_value()));
                    assert_within_delta!(v[3].get_value(), lin.get_partial_derivative(&vec![0, 0, 0, 1]).unwrap(), 1.0e-15 * F64::abs(v[3].get_value()));

                i += 1;
             }
         }

    }

    #[test]
    pub fn  test_linear_combination2_double_d_s()   {
        // we compare accurate versus naive dot product implementations
        // on regular vectors (i.e. not extreme cases like in the previous test)
        let mut random = WellGeneratorFactory::new_well_1024a();
        random.set_seed_i64(&(0xc6af886975069f11u64 as i64));

         {
             let mut i: i32 = 0;
            while i < 10000 {

                    let mut u = Vec::new();
                    let mut v = Vec::new();
                    for j in 0..4 {
                        u.push(1e17 * random.next_double());
                        v.push(DerivativeStructure::new(4, 1, j, 1e17 * random.next_double()).unwrap());
                    }


                    let mut lin: DerivativeStructure = v[0].linear_combination_f64_2_factors(u[0], &v[0], u[1], &v[1]).unwrap();
                    let mut reference: f64 = u[0] * v[0].get_value() + u[1] * v[1].get_value();
                    assert_within_delta!(reference, lin.get_value(), 1.0e-15 * F64::abs(reference));
                    assert_within_delta!(u[0], lin.get_partial_derivative(&vec![1, 0, 0, 0]).unwrap(), 1.0e-15 * F64::abs(v[0].get_value()));
                    assert_within_delta!(u[1], lin.get_partial_derivative(&vec![0, 1, 0, 0]).unwrap(), 1.0e-15 * F64::abs(v[1].get_value()));

                    lin = v[0].linear_combination_f64_3_factors(u[0], &v[0], u[1], &v[1], u[2], &v[2]).unwrap();
                    reference = u[0] * v[0].get_value() + u[1] * v[1].get_value() + u[2] * v[2].get_value();
                    assert_within_delta!(reference, lin.get_value(), 1.0e-15 * F64::abs(reference));
                    assert_within_delta!(u[0], lin.get_partial_derivative(&vec![1, 0, 0, 0]).unwrap(), 1.0e-15 * F64::abs(v[0].get_value()));
                    assert_within_delta!(u[1], lin.get_partial_derivative(&vec![0, 1, 0, 0]).unwrap(), 1.0e-15 * F64::abs(v[1].get_value()));
                    assert_within_delta!(u[2], lin.get_partial_derivative(&vec![0, 0, 1, 0]).unwrap(), 1.0e-15 * F64::abs(v[2].get_value()));

                    lin = v[0].linear_combination_f64_4_factors(u[0], &v[0], u[1], &v[1], u[2], &v[2], u[3], &v[3]).unwrap();
                    reference = u[0] * v[0].get_value() + u[1] * v[1].get_value() + u[2] * v[2].get_value() + u[3] * v[3].get_value();
                    assert_within_delta!(reference, lin.get_value(), 1.0e-15 * F64::abs(reference));
                    assert_within_delta!(u[0], lin.get_partial_derivative(&vec![1, 0, 0, 0]).unwrap(), 1.0e-15 * F64::abs(v[0].get_value()));
                    assert_within_delta!(u[1], lin.get_partial_derivative(&vec![0, 1, 0, 0]).unwrap(), 1.0e-15 * F64::abs(v[1].get_value()));
                    assert_within_delta!(u[2], lin.get_partial_derivative(&vec![0, 0, 1, 0]).unwrap(), 1.0e-15 * F64::abs(v[2].get_value()));
                    assert_within_delta!(u[3], lin.get_partial_derivative(&vec![0, 0, 0, 1]).unwrap(), 1.0e-15 * F64::abs(v[3].get_value()));

                i += 1;
             }
         }

    }

