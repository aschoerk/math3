// Licensed to the Apache Software Foundation (ASF) under one or more
// contributor license agreements.  See the NOTICE file distributed with
// this work for additional information regarding copyright ownership.
// The ASF licenses this file to You under the Apache License, Version 2.0
// (the "License"); you may not use this file except in compliance with
// the License.  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

use base::Field;
use analysis::differentiation::ds_compiler::*;
use std::rc::*;
use std::cell::*;
use java::exc::*;
use std::f64;
use util::fastmath::F64;
use util::rsutils::*;
use util::math_arrays::*;
use std::hash::{Hash, Hasher};

/** Class representing both the value and the differentials of a function.
   <p>This class is the workhorse of the differentiation package.</p>
   <p>This class is an implementation of the extension to Rall's
   numbers described in Dan Kalman's paper <a
   href="http://www1.american.edu/cas/mathstat/People/kalman/pdffiles/mmgautodiff.pdf">Doubly
   Recursive Multivariate Automatic Differentiation</a>, Mathematics Magazine, vol. 75,
   no. 3, June 2002. Rall's numbers are an extension to the real numbers used
   throughout mathematical expressions; they hold the derivative together with the
   value of a function. Dan Kalman's derivative structures hold all partial derivatives
   up to any specified order, with respect to any number of free parameters. Rall's
   numbers therefore can be seen as derivative structures for order one derivative and
   one free parameter, and real numbers can be seen as derivative structures with zero
   order derivative and no free parameters.</p>
   <p>{@link DerivativeStructure} instances can be used directly thanks to
   the arithmetic operators to the mathematical functions provided as
   methods by this class (+, -, *, /, %, sin, cos ...).</p>
   <p>Implementing complex expressions by hand using these classes is
   a tedious and error-prone task but has the advantage of having no limitation
   on the derivation order despite no requiring users to compute the derivatives by
   themselves. Implementing complex expression can also be done by developing computation
   code using standard primitive double values and to use {@link
   UnivariateFunctionDifferentiator differentiators} to create the {@link
   DerivativeStructure}-based instances. This method is simpler but may be limited in
   the accuracy and derivation orders and may be computationally intensive (this is
   typically the case for {@link FiniteDifferencesDifferentiator finite differences
   differentiator}.</p>
   <p>Instances of this class are guaranteed to be immutable.</p>
   @see DSCompiler
   @since 3.1
  */
#[derive(Clone)]
pub struct DerivativeStructure {
    /** Compiler for the current dimensions. */
    pub compiler: DSCompiler,

    /** Combined array holding all values. */
    pub data: Vec<f64>,

    field: Option<Rc<Field<DerivativeStructure>>>,
}

impl PartialEq for DerivativeStructure {
    fn eq(&self, other: &DerivativeStructure) -> bool {
        return (self.get_free_parameters() == other.get_free_parameters()) &&
               (self.get_order() == other.get_order()) &&
               MathArrays::equals(&self.data, &other.data);
    }
}

impl Hash for DerivativeStructure {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.get_free_parameters().hash(state);
        self.get_order().hash(state);
        for f in self.data.clone() {
            double_to_raw_long_bits(&f).hash(state);
        }
    }
}

#[derive(Clone)]
pub struct DerivativStructureField {
    compiler: DSCompiler,
}

impl<'ds> Field<DerivativeStructure> for DerivativStructureField {
    fn get_zero(&self) -> DerivativeStructure {
        return DerivativeStructure::new_parameters_order_value(self.compiler.get_free_parameters(),
                                                               self.compiler.get_order(),
                                                               0.0)
            .unwrap();
    }

    fn get_one(&self) -> DerivativeStructure {
        return DerivativeStructure::new_parameters_order_value(self.compiler.get_free_parameters(),
                                                               self.compiler.get_order(),
                                                               1.0)
            .unwrap();
    }
}

impl DerivativStructureField {
    pub fn new(ds_compiler: &DSCompiler) -> Rc<DerivativStructureField> {
        Rc::new(DerivativStructureField { compiler: ds_compiler.clone() })
    }
}

struct DataTransferObject {
    /** Number of variables.
     * @serial
     */
    variables: usize,

    /** Derivation order.
     * @serial
     */
    order: usize,

    /** Partial derivatives.
     * @serial
     */
    data: Vec<f64>,
}
impl DataTransferObject {
    /** Simple constructor.
     * @param variables number of variables
     * @param order derivation order
     * @param data partial derivatives
     */
    fn new(variables: usize, order: usize, data: &Vec<f64>) -> DataTransferObject {
        DataTransferObject {
            variables: variables,
            order: order,
            data: data.clone(),
        }
    }

    /** Replace the deserialized data transfer object with a {@link DerivativeStructure}.
     * @return replacement {@link DerivativeStructure}
     */
    fn read_resolve(&self) -> Result<DerivativeStructure, Rc<Exception>> {
        return DerivativeStructure::new_parameters_order_values(self.variables,
                                                                self.order,
                                                                &self.data);
    }
}


impl DerivativeStructure {
    pub fn new_compiler(compiler: DSCompiler) -> DerivativeStructure {
        let size = compiler.get_size();
        DerivativeStructure {
            compiler: compiler,
            data: vec![0.0f64; size],
            field: None,
        }
    }

    /** Build an instance with all values and derivatives set to 0.
     * @param parameters number of free parameters
     * @param order derivation order
     * @throws NumberIsTooLargeException if order is too large
     */
    pub fn new_parameters_order(parameters: usize,
                                order: usize)
                                -> Result<DerivativeStructure, Rc<Exception>> {
        match DSCompiler::get_compiler(parameters, order) {
            Some(tmp_compiler) => Ok(DerivativeStructure::new_compiler(tmp_compiler)),
            None => {
                return Err(Exc::new_msg("no compiler",
                                        format!("No Compiler found for {} {}", parameters, order)))
            }  
        }

    }


    /** Build an instance representing a constant value.
     * @param parameters number of free parameters
     * @param order derivation order
     * @param value value of the constant
     * @throws NumberIsTooLargeException if order is too large
     * @see #DerivativeStructure(int, int, int, double)
     */
    pub fn new_parameters_order_value(parameters: usize,
                                      order: usize,
                                      value: f64)
                                      -> Result<DerivativeStructure, Rc<Exception>> {
        match DSCompiler::get_compiler(parameters, order) {
            Some(tmp_compiler) => {
                let mut tmp = DerivativeStructure::new_compiler(tmp_compiler);
                tmp.data[0] = value;
                Ok(tmp)
            }
            None => {
                Err(Exc::new_msg("no compiler",
                                 format!("No Compiler found for {} {}", parameters, order)))
            }  
        }
    }

    /** Build an instance representing a variable.
     * <p>Instances built using this constructor are considered
     * to be the free variables with respect to which differentials
     * are computed. As such, their differential with respect to
     * themselves is +1.</p>
     * @param parameters number of free parameters
     * @param order derivation order
     * @param index index of the variable (from 0 to {@code parameters - 1})
     * @param value value of the variable
     * @exception NumberIsTooLargeException if {@code index >= parameters}.
     * @see #DerivativeStructure(int, int, double)
     */
    pub fn new(parameters: usize,
               order: usize,
               index: usize,
               value: f64)
               -> Result<DerivativeStructure, Rc<Exception>> {
        if index >= parameters {
            return Err(Exc::new_msg("NumberIsTooLargeException",
                                    format!("index: {} parameters: {}", index, parameters)));
        }
        let mut tmp =
            try!(DerivativeStructure::new_parameters_order_value(parameters, order, value));
        if order > 0 {
            match DSCompiler::get_compiler(index, order) {
                Some(tmp_compiler) => tmp.data[tmp_compiler.get_size()] = 1.0,
                None => {
                    return Err(Exc::new_msg("no compiler",
                                            format!("No Compiler found for {} {}", index, order)))
                }  
            };
        };
        Ok(tmp)
    }


    pub fn new_parameters_order_values(parameters: usize,
                                       order: usize,
                                       derivatives: &Vec<f64>)
                                       -> Result<DerivativeStructure, Rc<Exception>> {
        match DSCompiler::get_compiler(parameters, order) {
            Some(tmp_compiler) => {
                let mut tmp = DerivativeStructure::new_compiler(tmp_compiler);
                if derivatives.len() != tmp.data.len() {
                    Err(Exc::new_msg("DimensionMismatchException",
                                     format!("{} {}", derivatives.len(), tmp.data.len())))
                } else {
                    tmp.data = derivatives.clone();
                    Ok(tmp)
                }
            }
            None => {
                Err(Exc::new_msg("no compiler",
                                 format!("No Compiler found for {} {}", parameters, order)))
            } 
        }
    }

    /** Linear combination constructor.
     * The derivative structure built will be a1 * ds1 + a2 * ds2
     * @param a1 first scale factor
     * @param ds1 first base (unscaled) derivative structure
     * @param a2 second scale factor
     * @param ds2 second base (unscaled) derivative structure
     * @exception DimensionMismatchException if number of free parameters or orders are inconsistent
     */
    pub fn new_2d(a1: f64,
                  ds1: &DerivativeStructure,
                  a2: f64,
                  ds2: &DerivativeStructure)
                  -> Result<DerivativeStructure, Rc<Exception>> {
        let mut tmp = DerivativeStructure::new_compiler(ds1.compiler.clone());
        try!(tmp.compiler.check_compatibility(&ds2.compiler));
        tmp.compiler.linear_combination_2d(a1,
                                           &ds1.data,
                                           0,
                                           a2,
                                           &ds2.data,
                                           0,
                                           &RefCell::new(&mut tmp.data),
                                           0);
        return Ok(tmp);
    }

    /** Linear combination constructor.
     * The derivative structure built will be a1 * ds1 + a2 * ds2 + a3 * ds3
     * @param a1 first scale factor
     * @param ds1 first base (unscaled) derivative structure
     * @param a2 second scale factor
     * @param ds2 second base (unscaled) derivative structure
     * @param a3 third scale factor
     * @param ds3 third base (unscaled) derivative structure
     * @exception DimensionMismatchException if number of free parameters or orders are inconsistent
     */
    pub fn new_3d(a1: f64,
                  ds1: &DerivativeStructure,
                  a2: f64,
                  ds2: &DerivativeStructure,
                  a3: f64,
                  ds3: &DerivativeStructure)
                  -> Result<DerivativeStructure, Rc<Exception>> {
        let mut tmp = DerivativeStructure::new_compiler(ds1.compiler.clone());
        try!(tmp.compiler.check_compatibility(&ds2.compiler));
        try!(tmp.compiler.check_compatibility(&ds3.compiler));
        tmp.compiler.linear_combination_3d(a1,
                                           &ds1.data,
                                           0,
                                           a2,
                                           &ds2.data,
                                           0,
                                           a3,
                                           &ds3.data,
                                           0,
                                           &RefCell::new(&mut tmp.data),
                                           0);
        return Ok(tmp);
    }

    /** Linear combination constructor.
     * The derivative structure built will be a1 * ds1 + a2 * ds2 + a3 * ds3 + a4 * ds4
     * @param a1 first scale factor
     * @param ds1 first base (unscaled) derivative structure
     * @param a2 second scale factor
     * @param ds2 second base (unscaled) derivative structure
     * @param a3 third scale factor
     * @param ds3 third base (unscaled) derivative structure
     * @param a4 fourth scale factor
     * @param ds4 fourth base (unscaled) derivative structure
     * @exception DimensionMismatchException if number of free parameters or orders are inconsistent
     */
    pub fn new_4d(a1: f64,
                  ds1: &DerivativeStructure,
                  a2: f64,
                  ds2: &DerivativeStructure,
                  a3: f64,
                  ds3: &DerivativeStructure,
                  a4: f64,
                  ds4: &DerivativeStructure)
                  -> Result<DerivativeStructure, Rc<Exception>> {
        let mut tmp = DerivativeStructure::new_compiler(ds1.compiler.clone());
        try!(tmp.compiler.check_compatibility(&ds2.compiler));
        try!(tmp.compiler.check_compatibility(&ds3.compiler));
        try!(tmp.compiler.check_compatibility(&ds4.compiler));
        tmp.compiler.linear_combination_4d(a1,
                                           &ds1.data,
                                           0,
                                           a2,
                                           &ds2.data,
                                           0,
                                           a3,
                                           &ds3.data,
                                           0,
                                           a4,
                                           &ds4.data,
                                           0,
                                           &RefCell::new(&mut tmp.data),
                                           0);
        return Ok(tmp);
    }

    /** Build an instance from all its derivatives.
     * @param parameters number of free parameters
     * @param order derivation order
     * @param derivatives derivatives sorted according to
     * {@link DSCompiler#getPartialDerivativeIndex(int...)}
     * @exception DimensionMismatchException if derivatives array does not match the
     * {@link DSCompiler#getSize() size} expected by the compiler
     * @throws NumberIsTooLargeException if order is too large
     * @see #getAllDerivatives()
     */
    pub fn new_derivatives(parameters: usize,
                           order: usize,
                           derivatives: &Vec<f64>)
                           -> Result<DerivativeStructure, Rc<Exception>> {
        match DSCompiler::get_compiler(parameters, order) {
            Some(tmp_compiler) => {
                let mut tmp = DerivativeStructure::new_compiler(tmp_compiler);
                if derivatives.len() != tmp.data.len() {
                    Err(Exc::new_msg("DimensionMismatchException",
                                     format!("{} {}", derivatives.len(), tmp.data.len())))
                } else {
                    for i in 0..tmp.data.len() {
                        tmp.data[i] = derivatives[i];
                    }
                    Ok(tmp)
                }
            }
            None => {
                Err(Exc::new_msg("no compiler",
                                 format!("No Compiler found for {} {}", parameters, order)))
            }  
        }
    }

    /** Get the number of free parameters.
     * @return number of free parameters
     */
    pub fn get_free_parameters(&self) -> usize {
        return self.compiler.get_free_parameters();
    }

    /** Get the derivation order.
     * @return derivation order
     */
    pub fn get_order(&self) -> usize {
        return self.compiler.get_order();
    }

    /** Create a constant compatible with instance order and number of parameters.
     * <p>
     * This method is a convenience factory method, it simply calls
     * {@code new DerivativeStructure(getFreeParameters(), getOrder(), c)}
     * </p>
     * @param c value of the constant
     * @return a constant compatible with instance order and number of parameters
     * @see #DerivativeStructure(int, int, double)
     * @since 3.3
     */
    pub fn create_constant(&self, c: f64) -> Result<DerivativeStructure, Rc<Exception>> {
        return DerivativeStructure::new_parameters_order_value(self.get_free_parameters(),
                                                               self.get_order(),
                                                               c);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn get_real(&self) -> f64 {
        return self.data[0];
    }

    /** Get the value part of the derivative structure.
     * @return value part of the derivative structure
     * @see #getPartialDerivative(int...)
     */
    pub fn get_value(&self) -> f64 {
        return self.data[0];
    }


    /** Get a partial derivative.
     * @param orders derivation orders with respect to each variable (if all orders are 0,
     * the value is returned)
     * @return partial derivative
     * @see #getValue()
     * @exception DimensionMismatchException if the numbers of variables does not
     * match the instance
     * @exception NumberIsTooLargeException if sum of derivation orders is larger
     * than the instance limits
     */
    pub fn get_partial_derivative(&self, orders: &Vec<i32>) -> Result<f64, Rc<Exception>> {
        let index = try!(self.compiler.get_partial_derivative_index(orders));
        return Ok(self.data[index as usize]);
    }

    /** Get all partial derivatives.
     * @return a fresh copy of partial derivatives, in an array sorted according to
     * {@link DSCompiler#getPartialDerivativeIndex(int...)}
     */
    pub fn get_all_derivatives(&self) -> Vec<f64> {
        return self.data.clone();
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn add_f64(&self, a: f64) -> Result<DerivativeStructure, Rc<Exception>> {
        let mut ds: DerivativeStructure = self.clone();
        ds.data[0] += a;
        return Ok(ds);
    }

    /** {@inheritDoc}
     * @exception DimensionMismatchException if number of free parameters
     * or orders do not match
     */
    pub fn add(&self, a: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        try!(self.compiler.check_compatibility(&a.compiler));
        let mut ds: DerivativeStructure = self.clone();
        self.compiler.add(&self.data, 0, &a.data, 0, &RefCell::new(&mut ds.data), 0);
        return Ok(ds);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn subtract_f64(&self, a: f64) -> Result<DerivativeStructure, Rc<Exception>> {
        let tmp = try!(self.add_f64(-a));
        return Ok(tmp);
    }

    /** {@inheritDoc}
     * @exception DimensionMismatchException if number of free parameters
     * or orders do not match
     */
    pub fn subtract(&self, a: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        try!(self.compiler.check_compatibility(&a.compiler));
        let mut ds: DerivativeStructure = self.clone();
        self.compiler.subtract(&self.data, 0, &a.data, 0, &RefCell::new(&mut ds.data), 0);
        return Ok(ds);
    }



    pub fn multiply_i32(&self, n: i32) -> Result<DerivativeStructure, Rc<Exception>> {
        return self.multiply_f64(n as f64);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn multiply_f64(&self, a: f64) -> Result<DerivativeStructure, Rc<Exception>> {
        let mut ds: DerivativeStructure = self.clone();
        {
            let mut i: usize = 0;
            while i < ds.data.len() {
                {
                    ds.data[i] *= a;
                }
                i += 1;
            }
        }

        return Ok(ds);
    }

    /** {@inheritDoc}
     * @exception DimensionMismatchException if number of free parameters
     * or orders do not match
     */
    pub fn multiply(&self, a: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        try!(self.compiler.check_compatibility(&a.compiler));

        let mut result: DerivativeStructure = DerivativeStructure::new_compiler(self.compiler
            .clone());
        self.compiler.multiply(&self.data,
                               0,
                               &a.data,
                               0,
                               &RefCell::new(&mut result.data),
                               0);
        return Ok(result);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn divide_f64(&self, a: f64) -> Result<DerivativeStructure, Rc<Exception>> {
        let mut ds: DerivativeStructure = self.clone();
        {
            let mut i: usize = 0;
            while i < ds.data.len() {
                {
                    ds.data[i] /= a;
                }
                i += 1;
            }
        }

        return Ok(ds);
    }

    /** {@inheritDoc}
     * @exception DimensionMismatchException if number of free parameters
     * or orders do not match
     */
    pub fn divide(&self, a: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        try!(self.compiler.check_compatibility(&a.compiler));
        let mut result: DerivativeStructure = DerivativeStructure::new_compiler(self.compiler
            .clone());
        self.compiler.divide(&self.data,
                             0,
                             &a.data,
                             0,
                             &RefCell::new(&mut result.data),
                             0);
        return Ok(result);
    }

    pub fn remainder_f64(&self, a: f64) -> Result<DerivativeStructure, Rc<Exception>> {
        let mut ds: DerivativeStructure = self.clone();
        ds.data[0] = F64::ieee_remainder(ds.data[0], a);
        return Ok(ds);
    }

    /** {@inheritDoc}
     * @exception DimensionMismatchException if number of free parameters
     * or orders do not match
     * @since 3.2
     */
    pub fn remainder(&self, a: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        try!(self.compiler.check_compatibility(&a.compiler));
        let mut result: DerivativeStructure = DerivativeStructure::new_compiler(self.compiler
            .clone());
        self.compiler.remainder(&self.data,
                                0,
                                &a.data,
                                0,
                                &RefCell::new(&mut result.data),
                                0);
        return Ok(result);
    }

    pub fn negate(&self) -> Result<DerivativeStructure, Rc<Exception>> {
        let mut ds: DerivativeStructure = DerivativeStructure::new_compiler(self.compiler.clone());
        {
            let mut i: usize = 0;
            while i < ds.data.len() {
                {
                    ds.data[i] = -self.data[i];
                }
                i += 1;
            }
        }

        return Ok(ds);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn abs(&self) -> Result<DerivativeStructure, Rc<Exception>> {
        if double_to_raw_long_bits(&self.data[0]) & 0x80000000_00000000 != 0 {
            // we use the bits representation to also handle -0.0
            return self.negate();
        } else {
            return Ok(self.clone());
        }
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn ceil(&self) -> Result<DerivativeStructure, Rc<Exception>> {
        return DerivativeStructure::new_parameters_order_value(self.compiler.get_free_parameters(),
                                                               self.compiler.get_order(),
                                                               F64::ceil(self.data[0]));
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn floor(&self) -> Result<DerivativeStructure, Rc<Exception>> {
        return DerivativeStructure::new_parameters_order_value(self.compiler.get_free_parameters(),
                                                               self.compiler.get_order(),
                                                               F64::floor(self.data[0]));
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn rint(&self) -> Result<DerivativeStructure, Rc<Exception>> {
        return DerivativeStructure::new_parameters_order_value(self.compiler.get_free_parameters(),
                                                               self.compiler.get_order(),
                                                               F64::rint(self.data[0]));
    }


    pub fn round(&self) -> i64 {
        return F64::round(self.data[0]);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn signum(&self) -> Result<DerivativeStructure, Rc<Exception>> {
        return DerivativeStructure::new_parameters_order_value(self.compiler.get_free_parameters(),
                                                               self.compiler.get_order(),
                                                               F64::signum(self.data[0]));
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn copy_sign(&self,
                     sign: &DerivativeStructure)
                     -> Result<DerivativeStructure, Rc<Exception>> {
        let m: i64 = double_to_raw_long_bits(&self.data[0]) as i64;
        let s: i64 = double_to_raw_long_bits(&sign.data[0]) as i64;
        if (m >= 0 && s >= 0) || (m < 0 && s < 0) {
            // Sign is currently OK
            return Ok(self.clone());
        }
        // flip sign
        return self.negate();
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn copy_sign_f64(&self, sign: f64) -> Result<DerivativeStructure, Rc<Exception>> {
        let m: i64 = double_to_raw_long_bits(&self.data[0]) as i64;
        let s: i64 = double_to_raw_long_bits(&sign) as i64;
        if (m >= 0 && s >= 0) || (m < 0 && s < 0) {
            // Sign is currently OK
            return Ok(self.clone());
        }
        // flip sign
        return self.negate();
    }

    /**
     * Return the exponent of the instance value, removing the bias.
     * <p>
     * For double numbers of the form 2<sup>x</sup>, the unbiased
     * exponent is exactly x.
     * </p>
     * @return exponent for instance in IEEE754 representation, without bias
     */
    pub fn get_exponent(&self) -> i32 {
        return F64::get_exponent(self.data[0]);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn scalb(&self, n: i32) -> Result<DerivativeStructure, Rc<Exception>> {
        let mut ds: DerivativeStructure = DerivativeStructure::new_compiler(self.compiler.clone());
        {
            let mut i: usize = 0;
            while i < ds.data.len() {
                {
                    ds.data[i] = F64::scalb(self.data[i], n);
                }
                i += 1;
            }
        }

        return Ok(ds);
    }

    /** {@inheritDoc}
     * @exception DimensionMismatchException if number of free parameters
     * or orders do not match
     * @since 3.2
     */
    pub fn hypot(&self, y: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        try!(self.compiler.check_compatibility(&y.compiler));
        if f64::is_infinite(self.data[0]) || f64::is_infinite(y.data[0]) {
            return DerivativeStructure::new_parameters_order_value(self.compiler
                                                                       .get_free_parameters(),
                                                                   self.compiler
                                                                       .get_free_parameters(),
                                                                   f64::INFINITY);
        } else if f64::is_nan(self.data[0]) || f64::is_nan(y.data[0]) {
            return DerivativeStructure::new_parameters_order_value(self.compiler
                                                                       .get_free_parameters(),
                                                                   self.compiler
                                                                       .get_free_parameters(),
                                                                   f64::NAN);
        } else {
            let exp_x: i32 = self.get_exponent();
            let exp_y: i32 = y.get_exponent();
            if exp_x > exp_y + 27 {
                // y is neglectible with respect to x
                return self.abs();
            } else if exp_y > exp_x + 27 {
                // x is neglectible with respect to y
                return y.abs();
            } else {
                // find an intermediate scale to avoid both overflow and underflow
                let middle_exp: i32 = (exp_x + exp_y) / 2;
                // scale parameters without losing precision
                let scaled_x: DerivativeStructure = try!(self.scalb(-middle_exp));
                let scaled_y: DerivativeStructure = try!(y.scalb(-middle_exp));
                // compute scaled hypotenuse
                let term1 = try!(scaled_y.multiply(&scaled_y));
                let term2 = try!(scaled_x.multiply(&scaled_x));
                let term3 = try!(term2.add(&term1));
                let scaled_h: DerivativeStructure = try!(term3.sqrt());
                // remove scaling
                return scaled_h.scalb(middle_exp);
            }
        }
    }

    /**
     * Returns the hypotenuse of a triangle with sides {@code x} and {@code y}
     * - sqrt(<i>x</i><sup>2</sup> +<i>y</i><sup>2</sup>)
     * avoiding intermediate overflow or underflow.
     *
     * <ul>
     * <li> If either argument is infinite, then the result is positive infinity.</li>
     * <li> else, if either argument is NaN then the result is NaN.</li>
     * </ul>
     *
     * @param x a value
     * @param y a value
     * @return sqrt(<i>x</i><sup>2</sup> +<i>y</i><sup>2</sup>)
     * @exception DimensionMismatchException if number of free parameters
     * or orders do not match
     * @since 3.2
     */
    pub fn hypot_static(x: &DerivativeStructure,
                        y: &DerivativeStructure)
                        -> Result<DerivativeStructure, Rc<Exception>> {
        return x.hypot(&y);
    }

    /** Compute composition of the instance by a univariate function.
     * @param f array of value and derivatives of the function at
     * the current point (i.e. [f({@link #getValue()}),
     * f'({@link #getValue()}), f''({@link #getValue()})...]).
     * @return f(this)
     * @exception DimensionMismatchException if the number of derivatives
     * in the array is not equal to {@link #getOrder() order} + 1
     */
    pub fn compose(&self, f: &Vec<f64>) -> Result<DerivativeStructure, Rc<Exception>> {
        if f.len() != self.get_order() + 1 {
            return Err(Exc::new_msg("DimensionMismatchException",
                                    format!("len: {} order: {}", f.len(), self.get_order() + 1)));
        }
        let mut result: DerivativeStructure = DerivativeStructure::new_compiler(self.compiler
            .clone());
        self.compiler.compose(&self.data, 0, f, &RefCell::new(&mut result.data), 0);
        return Ok(result);
    }

    pub fn reciprocal(&self) -> Result<DerivativeStructure, Rc<Exception>> {
        let mut result: DerivativeStructure = DerivativeStructure::new_compiler(self.compiler
            .clone());
        self.compiler.pow_array_f64(&self.data, 0, -1.0, &RefCell::new(&mut result.data), 0);
        return Ok(result);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn sqrt(&self) -> Result<DerivativeStructure, Rc<Exception>> {
        return self.root_n(2);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn cbrt(&self) -> Result<DerivativeStructure, Rc<Exception>> {
        return self.root_n(3);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn root_n(&self, n: i32) -> Result<DerivativeStructure, Rc<Exception>> {
        let mut result: DerivativeStructure = DerivativeStructure::new_compiler(self.compiler
            .clone());
        self.compiler.root_n(&self.data,
                             0,
                             n as usize,
                             &RefCell::new(&mut result.data),
                             0);
        return Ok(result);
    }

    /** Compute a<sup>x</sup> where a is a double and x a {@link DerivativeStructure}
     * @param a number to exponentiate
     * @param x power to apply
     * @return a<sup>x</sup>
     * @since 3.3
     */
    pub fn pow_f64_static(a: f64,
                          x: &DerivativeStructure)
                          -> Result<DerivativeStructure, Rc<Exception>> {
        let mut result: DerivativeStructure = DerivativeStructure::new_compiler(x.compiler.clone());
        x.compiler.pow(a, &x.data, 0, &RefCell::new(&mut result.data), 0);
        return Ok(result);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn pow_f64(&self, p: f64) -> Result<DerivativeStructure, Rc<Exception>> {
        let mut result: DerivativeStructure = DerivativeStructure::new_compiler(self.compiler
            .clone());
        self.compiler.pow_array_f64(&self.data, 0, p, &RefCell::new(&mut result.data), 0);
        return Ok(result);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn pow_i32(&self, n: i32) -> Result<DerivativeStructure, Rc<Exception>> {
        let mut result: DerivativeStructure = DerivativeStructure::new_compiler(self.compiler
            .clone());
        self.compiler.pow_array_i32(&self.data, 0, n, &RefCell::new(&mut result.data), 0);
        return Ok(result);
    }

    /** {@inheritDoc}
     * @exception DimensionMismatchException if number of free parameters
     * or orders do not match
     * @since 3.2
     */
    pub fn pow(&self, e: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        try!(self.compiler.check_compatibility(&e.compiler));
        let mut result: DerivativeStructure = DerivativeStructure::new_compiler(self.compiler
            .clone());
        self.compiler.pow_vec(&self.data,
                              0,
                              &e.data,
                              0,
                              &RefCell::new(&mut result.data),
                              0);
        return Ok(result);
    }



    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn exp(&self) -> Result<DerivativeStructure, Rc<Exception>> {
        let mut result: DerivativeStructure = DerivativeStructure::new_compiler(self.compiler
            .clone());
        self.compiler.exp(&self.data, 0, &RefCell::new(&mut result.data), 0);
        return Ok(result);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn expm1(&self) -> Result<DerivativeStructure, Rc<Exception>> {
        let mut result: DerivativeStructure = DerivativeStructure::new_compiler(self.compiler
            .clone());
        self.compiler.expm1(&self.data, 0, &RefCell::new(&mut result.data), 0);
        return Ok(result);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn log(&self) -> Result<DerivativeStructure, Rc<Exception>> {
        let mut result: DerivativeStructure = DerivativeStructure::new_compiler(self.compiler
            .clone());
        self.compiler.log(&self.data, 0, &RefCell::new(&mut result.data), 0);
        return Ok(result);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn log1p(&self) -> Result<DerivativeStructure, Rc<Exception>> {
        let mut result: DerivativeStructure = DerivativeStructure::new_compiler(self.compiler
            .clone());
        self.compiler.log1p(&self.data, 0, &RefCell::new(&mut result.data), 0);
        return Ok(result);
    }

    /** Base 10 logarithm.
     * @return base 10 logarithm of the instance
     */
    pub fn log10(&self) -> Result<DerivativeStructure, Rc<Exception>> {
        let mut result: DerivativeStructure = DerivativeStructure::new_compiler(self.compiler
            .clone());
        self.compiler.log10(&self.data, 0, &RefCell::new(&mut result.data), 0);
        return Ok(result);
    }
    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn cos(&self) -> Result<DerivativeStructure, Rc<Exception>> {
        let mut result: DerivativeStructure = DerivativeStructure::new_compiler(self.compiler
            .clone());
        self.compiler.cos(&self.data, 0, &RefCell::new(&mut result.data), 0);
        return Ok(result);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn sin(&self) -> Result<DerivativeStructure, Rc<Exception>> {
        let mut result: DerivativeStructure = DerivativeStructure::new_compiler(self.compiler
            .clone());
        self.compiler.sin(&self.data, 0, &RefCell::new(&mut result.data), 0);
        return Ok(result);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn tan(&self) -> Result<DerivativeStructure, Rc<Exception>> {
        let mut result: DerivativeStructure = DerivativeStructure::new_compiler(self.compiler
            .clone());
        self.compiler.tan(&self.data, 0, &RefCell::new(&mut result.data), 0);
        return Ok(result);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn acos(&self) -> Result<DerivativeStructure, Rc<Exception>> {
        let mut result: DerivativeStructure = DerivativeStructure::new_compiler(self.compiler
            .clone());
        self.compiler.acos(&self.data, 0, &RefCell::new(&mut result.data), 0);
        return Ok(result);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn asin(&self) -> Result<DerivativeStructure, Rc<Exception>> {
        let mut result: DerivativeStructure = DerivativeStructure::new_compiler(self.compiler
            .clone());
        self.compiler.asin(&self.data, 0, &RefCell::new(&mut result.data), 0);
        return Ok(result);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn atan(&self) -> Result<DerivativeStructure, Rc<Exception>> {
        let mut result: DerivativeStructure = DerivativeStructure::new_compiler(self.compiler
            .clone());
        self.compiler.atan(&self.data, 0, &RefCell::new(&mut result.data), 0);
        return Ok(result);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn atan2(&self, x: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        try!(self.compiler.check_compatibility(&x.compiler));
        let mut result: DerivativeStructure = DerivativeStructure::new_compiler(self.compiler
            .clone());
        self.compiler.atan2(&self.data,
                            0,
                            &x.data,
                            0,
                            &RefCell::new(&mut result.data),
                            0);
        return Ok(result);
    }

    /** Two arguments arc tangent operation.
     * @param y first argument of the arc tangent
     * @param x second argument of the arc tangent
     * @return atan2(y, x)
     * @exception DimensionMismatchException if number of free parameters
     * or orders do not match
     * @since 3.2
     */
    pub fn atan2_static(y: &DerivativeStructure,
                        x: &DerivativeStructure)
                        -> Result<DerivativeStructure, Rc<Exception>> {
        return y.atan2(&x);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn cosh(&self) -> Result<DerivativeStructure, Rc<Exception>> {
        let mut result: DerivativeStructure = DerivativeStructure::new_compiler(self.compiler
            .clone());
        self.compiler.cosh(&self.data, 0, &RefCell::new(&mut result.data), 0);
        return Ok(result);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn sinh(&self) -> Result<DerivativeStructure, Rc<Exception>> {
        let mut result: DerivativeStructure = DerivativeStructure::new_compiler(self.compiler
            .clone());
        self.compiler.sinh(&self.data, 0, &RefCell::new(&mut result.data), 0);
        return Ok(result);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn tanh(&self) -> Result<DerivativeStructure, Rc<Exception>> {
        let mut result: DerivativeStructure = DerivativeStructure::new_compiler(self.compiler
            .clone());
        self.compiler.tanh(&self.data, 0, &RefCell::new(&mut result.data), 0);
        return Ok(result);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn acosh(&self) -> Result<DerivativeStructure, Rc<Exception>> {
        let mut result: DerivativeStructure = DerivativeStructure::new_compiler(self.compiler
            .clone());
        self.compiler.acosh(&self.data, 0, &RefCell::new(&mut result.data), 0);
        return Ok(result);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn asinh(&self) -> Result<DerivativeStructure, Rc<Exception>> {
        let mut result: DerivativeStructure = DerivativeStructure::new_compiler(self.compiler
            .clone());
        self.compiler.asinh(&self.data, 0, &RefCell::new(&mut result.data), 0);
        return Ok(result);
    }

    /** {@inheritDoc}
     * @since 3.2
     */
    pub fn atanh(&self) -> Result<DerivativeStructure, Rc<Exception>> {
        let mut result: DerivativeStructure = DerivativeStructure::new_compiler(self.compiler
            .clone());
        self.compiler.atanh(&self.data, 0, &RefCell::new(&mut result.data), 0);
        return Ok(result);
    }


    /** Convert radians to degrees, with error of less than 0.5 ULP
     *  @return instance converted into degrees
     */
    pub fn to_degrees(&self) -> Result<DerivativeStructure, Rc<Exception>> {
        let mut ds: DerivativeStructure = DerivativeStructure::new_compiler(self.compiler.clone());
        {
            let mut i: usize = 0;
            while i < ds.data.len() {
                {
                    ds.data[i] = F64::to_degrees(self.data[i]);
                }
                i += 1;
            }
        }

        return Ok(ds);
    }

    /** Convert degrees to radians, with error of less than 0.5 ULP
     *  @return instance converted into radians
     */
    pub fn to_radians(&self) -> Result<DerivativeStructure, Rc<Exception>> {
        let mut ds: DerivativeStructure = DerivativeStructure::new_compiler(self.compiler.clone());
        {
            let mut i: usize = 0;
            while i < ds.data.len() {
                {
                    ds.data[i] = F64::to_radians(self.data[i]);
                }
                i += 1;
            }
        }

        return Ok(ds);
    }

    /** Evaluate Taylor expansion a derivative structure.
     * @param delta parameters offsets (Δx, Δy, ...)
     * @return value of the Taylor expansion at x + Δx, y + Δy, ...
     * @throws MathArithmeticException if factorials becomes too large
     */
    pub fn taylor(&self, delta: &Vec<f64>) -> Result<f64, Rc<Exception>> {
        return self.compiler.taylor(&self.data, 0, delta);
    }

    pub fn get_field(&self) -> Rc<Field<DerivativeStructure>> {

        return DerivativStructureField::new(&self.compiler);
    }


    /** {@inheritDoc}
     * @exception DimensionMismatchException if number of free parameters
     * or orders do not match
     * @since 3.2
     */
    pub fn linear_combination_vec(&self,
                                  a: &Vec<DerivativeStructure>,
                                  b: &Vec<DerivativeStructure>)
                                  -> Result<DerivativeStructure, Rc<Exception>> {
        // compute an accurate value, taking care of cancellations
        let mut a_double = vec![0.0; a.len()];
        {
            let mut i: usize = 0;
            while i < a.len() {
                {
                    a_double[i] = a[i].get_value();
                }
                i += 1;
            }
        }

        let mut b_double = vec![0.0; b.len()];
        {
            let mut i: usize = 0;
            while i < b.len() {
                {
                    b_double[i] = b[i].get_value();
                }
                i += 1;
            }
        }

        let accurate_value: f64 = try!(MathArrays::linear_combination_vec(&a_double, &b_double));
        // compute a simple value, with all partial derivatives
        let mut simple_value: DerivativeStructure = a[0].get_field().get_zero();
        {
            let mut i: usize = 0;
            while i < a.len() {
                {
                    let term = try!(a[i].multiply(&b[i]));
                    simple_value = try!(simple_value.add(&term));
                }
                i += 1;
            }
        }

        // create a result with accurate value and all derivatives (not necessarily as accurate as the value)
        let mut all: Vec<f64> = simple_value.get_all_derivatives();
        all[0] = accurate_value;
        return DerivativeStructure::new_parameters_order_values(simple_value.get_free_parameters(),
                                                                simple_value.get_order(),
                                                                &all);
    }

    /** {@inheritDoc}
     * @exception DimensionMismatchException if number of free parameters
     * or orders do not match
     * @since 3.2
     */
    pub fn linear_combination_f64_vec(&self,
                                      a: &Vec<f64>,
                                      b: &Vec<DerivativeStructure>)
                                      -> Result<DerivativeStructure, Rc<Exception>> {
        // compute an accurate value, taking care of cancellations
        let mut b_double = vec![0.0; b.len()];
        {
            let mut i: usize = 0;
            while i < b.len() {
                {
                    b_double[i] = b[i].get_value();
                }
                i += 1;
            }
        }

        let accurate_value: f64 = try!(MathArrays::linear_combination_vec(a, &b_double));
        // compute a simple value, with all partial derivatives
        let mut simple_value: DerivativeStructure = b[0].get_field().get_zero();
        {
            let mut i: usize = 0;
            while i < a.len() {
                {
                    let term = try!(b[i].multiply_f64(a[i]));
                    simple_value = try!(simple_value.add(&term));
                }
                i += 1;
            }
        }

        // create a result with accurate value and all derivatives (not necessarily as accurate as the value)
        let mut all: Vec<f64> = simple_value.get_all_derivatives();
        all[0] = accurate_value;
        return DerivativeStructure::new_parameters_order_values(simple_value.get_free_parameters(),
                                                                simple_value.get_order(),
                                                                &all);
    }

    /** {@inheritDoc}
     * @exception DimensionMismatchException if number of free parameters
     * or orders do not match
     * @since 3.2
     */
    pub fn linear_combination_2_factors(&self,
                                        a1: &DerivativeStructure,
                                        b1: &DerivativeStructure,
                                        a2: &DerivativeStructure,
                                        b2: &DerivativeStructure)
                                        -> Result<DerivativeStructure, Rc<Exception>> {
        // compute an accurate value, taking care of cancellations
        let accurate_value: f64 = MathArrays::linear_combination_2d(a1.get_value(),
                                                                    b1.get_value(),
                                                                    a2.get_value(),
                                                                    b2.get_value());
        // compute a simple value, with all partial derivatives
        let term1 = try!(a2.multiply(&b2));
        let term2 = try!(a1.multiply(&b1));
        let simple_value: DerivativeStructure = try!(term2.add(&term1));
        // create a result with accurate value and all derivatives (not necessarily as accurate as the value)
        let mut all: Vec<f64> = simple_value.get_all_derivatives();
        all[0] = accurate_value;
        return DerivativeStructure::new_parameters_order_values(self.get_free_parameters(),
                                                                self.get_order(),
                                                                &all);
    }

    /** {@inheritDoc}
     * @exception DimensionMismatchException if number of free parameters
     * or orders do not match
     * @since 3.2
     */
    pub fn linear_combination_f64_2_factors(&self,
                                            a1: f64,
                                            b1: &DerivativeStructure,
                                            a2: f64,
                                            b2: &DerivativeStructure)
                                            -> Result<DerivativeStructure, Rc<Exception>> {
        // compute an accurate value, taking care of cancellations
        let accurate_value: f64 =
            MathArrays::linear_combination_2d(a1, b1.get_value(), a2, b2.get_value());
        // compute a simple value, with all partial derivatives
        let term1 = try!(b2.multiply_f64(a2));
        let term2 = try!(b1.multiply_f64(a1));
        let simple_value: DerivativeStructure = try!(term2.add(&term1));
        // create a result with accurate value and all derivatives (not necessarily as accurate as the value)
        let mut all: Vec<f64> = simple_value.get_all_derivatives();
        all[0] = accurate_value;
        return DerivativeStructure::new_parameters_order_values(self.get_free_parameters(),
                                                                self.get_order(),
                                                                &all);
    }

    /** {@inheritDoc}
     * @exception DimensionMismatchException if number of free parameters
     * or orders do not match
     * @since 3.2
     */
    pub fn linear_combination_3_factors(&self,
                                        a1: &DerivativeStructure,
                                        b1: &DerivativeStructure,
                                        a2: &DerivativeStructure,
                                        b2: &DerivativeStructure,
                                        a3: &DerivativeStructure,
                                        b3: &DerivativeStructure)
                                        -> Result<DerivativeStructure, Rc<Exception>> {
        // compute an accurate value, taking care of cancellations
        let accurate_value: f64 = MathArrays::linear_combination_3d(a1.get_value(),
                                                                    b1.get_value(),
                                                                    a2.get_value(),
                                                                    b2.get_value(),
                                                                    a3.get_value(),
                                                                    b3.get_value());
        // compute a simple value, with all partial derivatives
        let term3 = try!(a3.multiply(&b3));
        let term2 = try!(a2.multiply(&b2));
        let term1 = try!(a1.multiply(&b1));
        let term4 = try!(term1.add(&term2));
        let simple_value: DerivativeStructure = try!(term4.add(&term3));
        // create a result with accurate value and all derivatives (not necessarily as accurate as the value)
        let mut all: Vec<f64> = simple_value.get_all_derivatives();
        all[0] = accurate_value;
        return DerivativeStructure::new_parameters_order_values(self.get_free_parameters(),
                                                                self.get_order(),
                                                                &all);
    }

    /** {@inheritDoc}
     * @exception DimensionMismatchException if number of free parameters
     * or orders do not match
     * @since 3.2
     */
    pub fn linear_combination_f64_3_factors(&self,
                                            a1: f64,
                                            b1: &DerivativeStructure,
                                            a2: f64,
                                            b2: &DerivativeStructure,
                                            a3: f64,
                                            b3: &DerivativeStructure)
                                            -> Result<DerivativeStructure, Rc<Exception>> {
        // compute an accurate value, taking care of cancellations
        let accurate_value: f64 = MathArrays::linear_combination_3d(a1,
                                                                    b1.get_value(),
                                                                    a2,
                                                                    b2.get_value(),
                                                                    a3,
                                                                    b3.get_value());
        // compute a simple value, with all partial derivatives
        let term1 = try!(b1.multiply_f64(a1));
        let term2 = try!(b2.multiply_f64(a2));
        let term3 = try!(b3.multiply_f64(a3));
        let term4 = try!(term1.add(&term2));
        let simple_value: DerivativeStructure = try!(term4.add(&term3));
        // create a result with accurate value and all derivatives (not necessarily as accurate as the value)
        let mut all: Vec<f64> = simple_value.get_all_derivatives();
        all[0] = accurate_value;
        return DerivativeStructure::new_parameters_order_values(self.get_free_parameters(),
                                                                self.get_order(),
                                                                &all);
    }

    /** {@inheritDoc}
     * @exception DimensionMismatchException if number of free parameters
     * or orders do not match
     * @since 3.2
     */
    pub fn linear_combination_4_factors(&self,
                                        a1: &DerivativeStructure,
                                        b1: &DerivativeStructure,
                                        a2: &DerivativeStructure,
                                        b2: &DerivativeStructure,
                                        a3: &DerivativeStructure,
                                        b3: &DerivativeStructure,
                                        a4: &DerivativeStructure,
                                        b4: &DerivativeStructure)
                                        -> Result<DerivativeStructure, Rc<Exception>> {
        // compute an accurate value, taking care of cancellations
        let accurate_value: f64 = MathArrays::linear_combination_4d(a1.get_value(),
                                                                    b1.get_value(),
                                                                    a2.get_value(),
                                                                    b2.get_value(),
                                                                    a3.get_value(),
                                                                    b3.get_value(),
                                                                    a4.get_value(),
                                                                    b4.get_value());
        // compute a simple value, with all partial derivatives
        let term1 = try!(a1.multiply(&b1));
        let term2 = try!(a2.multiply(&b2));
        let term3 = try!(a3.multiply(&b3));
        let term4 = try!(a4.multiply(&b4));
        let term5 = try!(term1.add(&term2));
        let term6 = try!(term5.add(&term3));
        let simple_value: DerivativeStructure = try!(term6.add(&term4));
        // create a result with accurate value and all derivatives (not necessarily as accurate as the value)
        let mut all: Vec<f64> = simple_value.get_all_derivatives();
        all[0] = accurate_value;
        return DerivativeStructure::new_parameters_order_values(self.get_free_parameters(),
                                                                self.get_order(),
                                                                &all);
    }

    /** {@inheritDoc}
     * @exception DimensionMismatchException if number of free parameters
     * or orders do not match
     * @since 3.2
     */
    pub fn linear_combination_f64_4_factors(&self,
                                            a1: f64,
                                            b1: &DerivativeStructure,
                                            a2: f64,
                                            b2: &DerivativeStructure,
                                            a3: f64,
                                            b3: &DerivativeStructure,
                                            a4: f64,
                                            b4: &DerivativeStructure)
                                            -> Result<DerivativeStructure, Rc<Exception>> {
        // compute an accurate value, taking care of cancellations
        let accurate_value: f64 = MathArrays::linear_combination_4d(a1,
                                                                    b1.get_value(),
                                                                    a2,
                                                                    b2.get_value(),
                                                                    a3,
                                                                    b3.get_value(),
                                                                    a4,
                                                                    b4.get_value());
        // compute a simple value, with all partial derivatives
        let term1 = try!(b1.multiply_f64(a1));
        let term2 = try!(b2.multiply_f64(a2));
        let term3 = try!(b3.multiply_f64(a3));
        let term4 = try!(b4.multiply_f64(a4));
        let term5 = try!(term1.add(&term2));
        let term6 = try!(term5.add(&term3));
        let simple_value: DerivativeStructure = try!(term6.add(&term4));
        // create a result with accurate value and all derivatives (not necessarily as accurate as the value)
        let mut all: Vec<f64> = simple_value.get_all_derivatives();
        all[0] = accurate_value;
        return DerivativeStructure::new_parameters_order_values(self.get_free_parameters(),
                                                                self.get_order(),
                                                                &all);
    }

    // Get a hashCode for the derivative structure.
    // @return a hash code value for this object
    // @since 3.2
    //
    // pub fn  hash_code(&self) -> i32  {
    // return 227 + 229 * self.get_free_parameters() + 233 * self.get_order() + 239 * MathUtils::hash(&self.data);
    // }
    //
}
