
// Licensed to the Apache Software Foundation (ASF) under one or more
// contributor license agreements.  See the NOTICE file distributed with
// this work for additional information regarding copyright ownership.
// The ASF licenses this file to You under the Apache License, Version 2.0
// (the "License"); you may not use this file except in compliance with
// the License.  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

use std::cell::*;
use std::rc::*;
use std::f64;
use java::exc::*;
use util::fastmath;
use util::fastmath::I32;
use util::fastmath::F64;
use util::math_arrays::*;
use util::combinatorics_utils::*;

thread_local!(static COMPILERS: RefCell<Vec<Vec<Option<DSCompiler>> >> = RefCell::new(Vec::new()));

/** Class holding "compiled" computation rules for derivative structures.
 * <p>This class implements the computation rules described in Dan Kalman's paper <a
 * href="http://www1.american.edu/cas/mathstat/People/kalman/pdffiles/mmgautodiff.pdf">Doubly
 * Recursive Multivariate Automatic Differentiation</a>, Mathematics Magazine, vol. 75,
 * no. 3, June 2002. However, in order to avoid performances bottlenecks, the recursive
 * rules are "compiled" once in an unfold form. This class does this recursion unrolling
 * and stores the computation rules as simple loops with pre-computed indirection arrays.</p>
 * <p>
 * This class maps all derivative computation into single dimension arrays that hold the
 * value and partial derivatives. The class does not hold these arrays, which remains under
 * the responsibility of the caller. For each combination of number of free parameters and
 * derivation order, only one compiler is necessary, and this compiler will be used to
 * perform computations on all arrays provided to it, which can represent hundreds or
 * thousands of different parameters kept together with all theur partial derivatives.
 * </p>
 * <p>
 * The arrays on which compilers operate contain only the partial derivatives together
 * with the 0<sup>th</sup> derivative, i.e. the value. The partial derivatives are stored in
 * a compiler-specific order, which can be retrieved using methods {@link
 * #getPartialDerivativeIndex(int...) getPartialDerivativeIndex} and {@link
 * #getPartialDerivativeOrders(int)}. The value is guaranteed to be stored as the first element
 * (i.e. the {@link #getPartialDerivativeIndex(int...) getPartialDerivativeIndex} method returns
 * 0 when called with 0 for all derivation orders and {@link #getPartialDerivativeOrders(int)
 * getPartialDerivativeOrders} returns an array filled with 0 when called with 0 as the index).
 * </p>
 * <p>
 * Note that the ordering changes with number of parameters and derivation order. For example
 * given 2 parameters x and y, df/dy is stored at index 2 when derivation order is set to 1 (in
 * this case the array has three elements: f, df/dx and df/dy). If derivation order is set to
 * 2, then df/dy will be stored at index 3 (in this case the array has six elements: f, df/dx,
 * df/dxdx, df/dy, df/dxdy and df/dydy).
 * </p>
 * <p>
 * Given this structure, users can perform some simple operations like adding, subtracting
 * or multiplying constants and negating the elements by themselves, knowing if they want to
 * mutate their array or create a new array. These simple operations are not provided by
 * the compiler. The compiler provides only the more complex operations between several arrays.
 * </p>
 * <p>This class is mainly used as the engine for scalar variable {@link DerivativeStructure}.
 * It can also be used directly to hold several variables in arrays for more complex data
 * structures. User can for example store a vector of n variables depending on three x, y
 * and z free parameters in one array as follows:</p> <pre>
 *   // parameter 0 is x, parameter 1 is y, parameter 2 is z
 *   int parameters = 3;
 *   DSCompiler compiler = DSCompiler.getCompiler(parameters, order);
 *   int size = compiler.getSize();
 *
 *   // pack all elements in a single array
 *   double[] array = new double[n * size];
 *   for (int i = 0; i < n; ++i) {
 *
 *     // we know value is guaranteed to be the first element
 *     array[i * size] = v[i];
 *
 *     // we don't know where first derivatives are stored, so we ask the compiler
 *     array[i * size + compiler.getPartialDerivativeIndex(1, 0, 0) = dvOnDx[i][0];
 *     array[i * size + compiler.getPartialDerivativeIndex(0, 1, 0) = dvOnDy[i][0];
 *     array[i * size + compiler.getPartialDerivativeIndex(0, 0, 1) = dvOnDz[i][0];
 *
 *     // we let all higher order derivatives set to 0
 *
 *   }
 * </pre>
 * <p>Then in another function, user can perform some operations on all elements stored
 * in the single array, such as a simple product of all variables:</p> <pre>
 *   // compute the product of all elements
 *   double[] product = new double[size];
 *   prod[0] = 1.0;
 *   for (int i = 0; i < n; ++i) {
 *     double[] tmp = product.clone();
 *     compiler.multiply(tmp, 0, array, i * size, product, 0);
 *   }
 *
 *   // value
 *   double p = product[0];
 *
 *   // first derivatives
 *   double dPdX = product[compiler.getPartialDerivativeIndex(1, 0, 0)];
 *   double dPdY = product[compiler.getPartialDerivativeIndex(0, 1, 0)];
 *   double dPdZ = product[compiler.getPartialDerivativeIndex(0, 0, 1)];
 *
 *   // cross derivatives (assuming order was at least 2)
 *   double dPdXdX = product[compiler.getPartialDerivativeIndex(2, 0, 0)];
 *   double dPdXdY = product[compiler.getPartialDerivativeIndex(1, 1, 0)];
 *   double dPdXdZ = product[compiler.getPartialDerivativeIndex(1, 0, 1)];
 *   double dPdYdY = product[compiler.getPartialDerivativeIndex(0, 2, 0)];
 *   double dPdYdZ = product[compiler.getPartialDerivativeIndex(0, 1, 1)];
 *   double dPdZdZ = product[compiler.getPartialDerivativeIndex(0, 0, 2)];
 * </pre>
 * @see DerivativeStructure
 * @since 3.1
 */
#[derive(Clone)]
pub struct DSCompiler {
    /** Number of free parameters. */
    pub parameters: i32,

    /** Derivation order. */
    pub order: i32,

    /** Number of partial derivatives (including the single 0 order derivative element). */
    pub sizes: Vec<Vec<i32>>,

    /** Indirection array for partial derivatives. */
    pub derivatives_indirection: Vec<Vec<i32>>,

    /** Indirection array of the lower derivative elements. */
    pub lower_indirection: Vec<i32>,

    /** Indirection arrays for multiplication. */
    pub mult_indirection: Vec<Vec<Vec<i32>>>,

    /** Indirection arrays for function composition. */
    pub comp_indirection: Vec<Vec<Vec<i32>>>,
}

impl DSCompiler {
    /** Private constructor, reserved for the factory method {@link #getCompiler(int, int)}.
     * @param parameters number of free parameters
     * @param order derivation order
     * @param valueCompiler compiler for the value part
     * @param derivativeCompiler compiler for the derivative part
     * @throws NumberIsTooLargeException if order is too large
     */
    fn new(parameters: i32,
           order: i32,
           value_compiler: &Option<DSCompiler>,
           derivative_compiler: &Option<DSCompiler>)
           -> Result<DSCompiler, Rc<Exception>> {
        let lower_indirection = &DSCompiler::compile_lower_indirection(parameters,
                                                                       order,
                                                                       value_compiler,
                                                                       derivative_compiler);
        let derivatives_indirection =
            &DSCompiler::compile_derivatives_indirection(parameters,
                                                         order,
                                                         value_compiler,
                                                         derivative_compiler);
        let sizes = &DSCompiler::compile_sizes(parameters, order, value_compiler);
        Ok(DSCompiler {
            parameters: parameters,
            order: order,
            sizes: sizes.clone(),
            derivatives_indirection: derivatives_indirection.clone(),
            lower_indirection: lower_indirection.clone(),
            mult_indirection: DSCompiler::compile_multiplication_indirection(parameters,
                                                                             order,
                                                                             value_compiler,
                                                                             derivative_compiler,
                                                                             &lower_indirection),
            comp_indirection: DSCompiler::compile_composition_indirection(parameters,
                                                                          order,
                                                                          value_compiler,
                                                                          derivative_compiler,
                                                                          &sizes,
                                                                          &derivatives_indirection)
                .unwrap(),
        })
    }


    /** Get the compiler for number of free parameters and order.
     * @param parameters number of free parameters
     * @param order derivation order
     * @return cached rules set
     * @throws NumberIsTooLargeException if order is too large
     */
    pub fn get_compiler(parameters: usize, order: usize) -> Option<DSCompiler> {
        COMPILERS.with(|compilers_ref| {
            let mut cache: RefMut<Vec<Vec<Option<DSCompiler>>>> = compilers_ref.borrow_mut();
            if cache.len() > parameters && cache[parameters].len() > order {
                if cache[parameters][order].is_some() {
                    return cache[parameters][order].clone();
                };
            }
            let max_parameters: i32 = I32::max(parameters as i32, cache.len() as i32);
            let max_order: i32 = I32::max(order as i32,
                                          if cache.len() == 0 {
                                              0
                                          } else {
                                              cache[0].len() as i32
                                          });
            while (cache.len() as i32) < max_parameters + 1 {
                cache.push(Vec::new());
            }
            for i in 0..cache.len() {
                while (cache[i].len() as i32) < max_order + 1 {
                    cache[i].push(None);
                }
            }
            let mut diag: usize = 0;
            while diag <= parameters + order {
                let mut o: usize = I32::max(0, diag as i32 - parameters as i32) as usize;
                while o <= I32::min(order as i32, diag as i32) as usize {
                    let p: usize = diag as usize - o;
                    if cache[p][o].is_none() {
                        let value_compiler = if p == 0 {
                            None
                        } else {
                            cache[p - 1][o].clone()
                        };
                        let derivative_compiler = if o == 0 {
                            None
                        } else {
                            cache[p][o - 1].clone()
                        };
                        cache[p][o] = Some(DSCompiler::new(p as i32,
                                                           o as i32,
                                                           &value_compiler,
                                                           &derivative_compiler)
                            .unwrap());
                    };
                    o += 1;
                }

                diag += 1;
            }
            cache[parameters][order].clone()
        })
    }



    /** Compile the sizes array.
     * @param parameters number of free parameters
     * @param order derivation order
     * @param valueCompiler compiler for the value part
     * @return sizes array
     */
    fn compile_sizes(parameters: i32,
                     order: i32,
                     value_compiler: &Option<DSCompiler>)
                     -> Vec<Vec<i32>> {
        if parameters == 0 {
            vec![vec![1;order as usize +1]; parameters as usize + 1]
        } else {
            let mut sizes = value_compiler.as_ref().unwrap().sizes.clone();
            for _ in sizes.len()..(parameters + 1) as usize {
                sizes.push(Vec::new());
            }
            for i in 0..sizes.len() {
                for _ in sizes[i].len()..(order + 1) as usize {
                    sizes[i].push(0);
                }
            }
            sizes[parameters as usize][0] = 1;
            {
                let mut i: usize = 0;
                while i < order as usize {
                    {
                        sizes[parameters as usize][i + 1] = sizes[parameters as usize][i] +
                                                            sizes[parameters as usize - 1][i + 1];
                    }
                    i += 1;
                }
            }

            sizes.clone()
        }

    }

    /** Compile the derivatives indirection array.
     * @param parameters number of free parameters
     * @param order derivation order
     * @param valueCompiler compiler for the value part
     * @param derivativeCompiler compiler for the derivative part
     * @return derivatives indirection array
     */
    fn compile_derivatives_indirection(parameters: i32,
                                       order: i32,
                                       value_compiler_o: &Option<DSCompiler>,
                                       derivative_compiler_o: &Option<DSCompiler>)
                                       -> Vec<Vec<i32>> {
        if parameters == 0 || order == 0 {
            vec![vec![0;parameters as usize];1]
        } else {
            let value_compiler = value_compiler_o.as_ref().unwrap();
            let derivative_compiler = derivative_compiler_o.as_ref().unwrap();
            let v_size: usize = value_compiler.derivatives_indirection.len();
            let d_size: usize = derivative_compiler.derivatives_indirection.len();
            let mut derivatives_indirection =
                vec![vec![0;parameters as usize];(v_size as usize) + d_size];
            // set up the indices for the value part
            {
                let mut i: usize = 0;
                while i < v_size {
                    {
                        for j in 0..value_compiler.derivatives_indirection[i].len() {
                            derivatives_indirection[i][j] =
                                value_compiler.derivatives_indirection[i][j];
                        }
                        // copy the first indices, the last one remaining set to 0
                        derivatives_indirection[i][(parameters as usize) - 1] = 0;
                    }
                    i += 1;
                }
            }

            // set up the indices for the derivative part
            {
                let mut i: usize = 0;
                while i < d_size {
                    {
                        for j in 0..derivative_compiler.derivatives_indirection[i].len() {
                            derivatives_indirection[v_size + i][j] =
                                derivative_compiler.derivatives_indirection[i][j];
                        }
                        // increment the derivation order for the last parameter
                        derivatives_indirection[v_size + i][parameters as usize - 1] += 1;
                    }
                    i += 1;
                }
            }

            derivatives_indirection
        }
    }


    /** Compile the lower derivatives indirection array.
     * <p>
     * This indirection array contains the indices of all elements
     * except derivatives for last derivation order.
     * </p>
     * @param parameters number of free parameters
     * @param order derivation order
     * @param valueCompiler compiler for the value part
     * @param derivativeCompiler compiler for the derivative part
     * @return lower derivatives indirection array
     */
    fn compile_lower_indirection(parameters: i32,
                                 order: i32,
                                 value_compiler_o: &Option<DSCompiler>,
                                 derivative_compiler_o: &Option<DSCompiler>)
                                 -> Vec<i32> {
        if parameters == 0 || order <= 1 {
            vec![0i32]
        } else {
            let value_compiler = value_compiler_o.as_ref().unwrap();
            let derivative_compiler = derivative_compiler_o.as_ref().unwrap();
            // this is an implementation of definition 6 in Dan Kalman's paper.
            let v_size: usize = value_compiler.lower_indirection.len();
            let d_size: usize = derivative_compiler.lower_indirection.len();
            let mut lower_indirection = Vec::new();
            for val in value_compiler.lower_indirection.iter() {
                lower_indirection.push(*val);
            }
            {
                assert!(lower_indirection.len() == v_size);
                let mut i: usize = 0;
                while i < d_size {
                    lower_indirection.push(value_compiler.get_size() as i32 +
                                           derivative_compiler.lower_indirection[i]);
                    i += 1;
                }
                assert!(lower_indirection.len() == v_size + d_size);
            }

            lower_indirection
        }
    }

    /** Compile the multiplication indirection array.
     * <p>
     * This indirection array contains the indices of all pairs of elements
     * involved when computing a multiplication. This allows a straightforward
     * loop-based multiplication (see {@link #multiply(double[], int, double[], int, double[], int)}).
     * </p>
     * @param parameters number of free parameters
     * @param order derivation order
     * @param valueCompiler compiler for the value part
     * @param derivativeCompiler compiler for the derivative part
     * @param lowerIndirection lower derivatives indirection array
     * @return multiplication indirection array
     */
    fn compile_multiplication_indirection(parameters: i32,
                                          order: i32,
                                          value_compiler_o: &Option<DSCompiler>,
                                          derivative_compiler_o: &Option<DSCompiler>,
                                          lower_indirection: &Vec<i32>)
                                          -> Vec<Vec<Vec<i32>>> {
        if (parameters == 0) || (order == 0) {
            vec![vec![vec![1, 0, 0]]]
        } else {
            let value_compiler = value_compiler_o.as_ref().unwrap();
            let derivative_compiler = derivative_compiler_o.as_ref().unwrap();
            // this is an implementation of definition 3 in Dan Kalman's paper.
            let v_size: usize = value_compiler.mult_indirection.len();
            let d_size: usize = derivative_compiler.mult_indirection.len();
            let mut mult_indirection = Vec::new();
            for a in value_compiler.mult_indirection.iter() {
                mult_indirection.push(a.clone());
            }

            {
                let mut i: usize = 0;
                while i < d_size {
                    {
                        let d_row = &derivative_compiler.mult_indirection[i];
                        let mut row = Vec::new();
                        {
                            let mut j: usize = 0;
                            while j < d_row.len() {
                                row.push(vec![d_row[j][0], lower_indirection[d_row[j][1] as usize], v_size as i32 + d_row[j][2]]);
                                row.push(vec![d_row[j][0], v_size as i32 + d_row[j][1], lower_indirection[d_row[j][2] as usize]]);
                                j += 1;
                            }
                        }

                        // combine terms with similar derivation orders
                        let mut combined = Vec::new();
                        {
                            let mut j: usize = 0;
                            while j < row.len() {
                                {
                                    let mut term_j = row[j].clone();
                                    if term_j[0] > 0 {
                                        {
                                            let mut k: usize = j + 1;
                                            while k < row.len() {
                                                {
                                                    let term_k = &mut row[k];
                                                    if term_j[1] == term_k[1] &&
                                                       term_j[2] == term_k[2] {
                                                        // combine termJ and termK
                                                        term_j[0] += term_k[0];
                                                        // make sure we will skip termK later on in the outer loop
                                                        term_k[0] = 0;
                                                    }
                                                }
                                                k += 1;
                                            }
                                        }

                                        combined.push(term_j);
                                    }
                                }
                                j += 1;
                            }
                        }

                        mult_indirection.push(combined);
                    }
                    i += 1;
                }
            }

            mult_indirection
        }
    }

    /** Compile the function composition indirection array.
     * <p>
     * This indirection array contains the indices of all sets of elements
     * involved when computing a composition. This allows a straightforward
     * loop-based composition (see {@link #compose(double[], int, double[], double[], int)}).
     * </p>
     * @param parameters number of free parameters
     * @param order derivation order
     * @param valueCompiler compiler for the value part
     * @param derivativeCompiler compiler for the derivative part
     * @param sizes sizes array
     * @param derivatives_indirection derivatives indirection array
     * @return multiplication indirection array
     * @throws NumberIsTooLargeException if order is too large
     */
    fn compile_composition_indirection(parameters: i32,
                                       order: i32,
                                       value_compiler_o: &Option<DSCompiler>,
                                       derivative_compiler_o: &Option<DSCompiler>,
                                       sizes: &Vec<Vec<i32>>,
                                       derivatives_indirection: &Vec<Vec<i32>>)
                                       -> Result<Vec<Vec<Vec<i32>>>, Rc<Exception>> {
        if (parameters == 0) || (order == 0) {
            Ok(vec![vec![vec![1, 0, ]]])

        } else {
            let value_compiler = value_compiler_o.as_ref().unwrap();
            let derivative_compiler = derivative_compiler_o.as_ref().unwrap();
            let v_size: usize = value_compiler.comp_indirection.len();
            let d_size: usize = derivative_compiler.comp_indirection.len();
            let mut comp_indirection = Vec::new();
            for a in value_compiler.comp_indirection.iter() {
                comp_indirection.push(a.clone());
            }

            // underlying one did not handle
            {
                let mut i: usize = 0;
                while i < d_size {
                    {
                        let mut row = Vec::new();
                        for term in derivative_compiler.comp_indirection[i].iter() {
                            // handle term p * f_k(g(x)) * g_l1(x) * g_l2(x) * ... * g_lp(x)
                            // derive the first factor in the term: f_k with respect to new parameter
                            let mut derived_term_f = vec![0;term.len() + 1];
                            // p
                            derived_term_f[0] = term[0];
                            // f_(k+1)
                            derived_term_f[1] = term[1] + 1;
                            let mut orders = vec![0;parameters as usize];
                            orders[parameters as usize - 1] = 1;
                            // g_1
                            derived_term_f[term.len()] =
                                DSCompiler::get_partial_derivative_index_static(parameters,
                                                                                order,
                                                                                &sizes,
                                                                                &orders)
                                    .unwrap();
                            {
                                let mut j: usize = 2;
                                while j < term.len() {
                                    // convert the indices as the mapping for the current order
                                    // is different from the mapping with one less order
                                    derived_term_f[j] = DSCompiler::convert_index(term[j], parameters, &derivative_compiler.derivatives_indirection, parameters, order, &sizes).unwrap();
                                    j += 1;
                                }
                            }

                            let mut rest = derived_term_f.split_off(2);
                            rest.sort();
                            derived_term_f.extend(rest);
                            row.push(derived_term_f);
                            // Arrays.sort(&derived_term_f, 2, derived_term_f.len());
                            // derive the various g_l
                            {
                                let mut l: usize = 2;
                                while l < term.len() {
                                    {
                                        let mut derived_term_g = vec![0; term.len()];
                                        derived_term_g[0] = term[0];
                                        derived_term_g[1] = term[1];
                                        {
                                            let mut j: usize = 2;
                                            while j < term.len() {
                                                {
                                                    // convert the indices as the mapping for the current order
                                                    // is different from the mapping with one less order
                                                    derived_term_g[j] = DSCompiler::convert_index(term[j], parameters, &derivative_compiler.derivatives_indirection, parameters, order, &sizes).unwrap();
                                                    if j == l {
                                                        // derive this term
                                                        // System.arraycopy(derivatives_indirection[derived_term_g[j]], 0, &orders, 0, parameters);
                                                        let mut orders = derivatives_indirection[derived_term_g[j] as usize].clone();
                                                        orders[parameters as usize - 1] += 1;
                                                        derived_term_g[j] = DSCompiler::get_partial_derivative_index_static(parameters, order, &sizes, &orders).unwrap();
                                                    }
                                                }
                                                j += 1;
                                            }
                                        }

                                        let mut tail = derived_term_g.split_off(2);
                                        tail.sort();
                                        derived_term_g.extend(tail);
                                        // Arrays.sort(&derived_term_f, 2, derived_term_f.len());
                                        row.push(derived_term_g);
                                        // Arrays.sort(&derived_term_g, 2, derived_term_g.len());
                                        // row.add(&derived_term_g);
                                    }
                                    l += 1;
                                }
                            }

                        }
                        // combine terms with similar derivation orders
                        let mut combined = Vec::new(); // : List<Vec<i32>> = ArrayList<Vec<i32>>::new(&row.size());
                        {
                            let mut j: usize = 0;
                            while j < row.len() {
                                {
                                    if row[j][0] > 0 {
                                        {
                                            let mut k: usize = j + 1;
                                            while k < row.len() {
                                                {
                                                    let mut equals = row[j].len() == row[k].len();
                                                    {
                                                        let mut l: usize = 1;
                                                        while equals && l < row[j].len() {
                                                            {
                                                                equals &= row[j][l] == row[k][l];
                                                            }
                                                            l += 1;
                                                        }
                                                    }

                                                    if equals {
                                                        // combine termJ and termK
                                                        row[j][0] += row[k][0];
                                                        // make sure we will skip termK later on in the outer loop
                                                        row[k][0] = 0;
                                                    }
                                                }
                                                k += 1;
                                            }
                                        }

                                        combined.push(row[j].clone());
                                    }
                                }
                                j += 1;
                            }
                        }


                        comp_indirection.push(combined);
                        assert!(comp_indirection.len() == v_size + i + 1);
                    }
                    i += 1;
                }
            }

            Ok(comp_indirection)
        }
    }

    /** Get the index of a partial derivative in the array.
     * <p>
     * If all orders are set to 0, then the 0<sup>th</sup> order derivative
     * is returned, which is the value of the function.
     * </p>
     * <p>The indices of derivatives are between 0 and {@link #getSize() getSize()} - 1.
     * Their specific order is fixed for a given compiler, but otherwise not
     * publicly specified. There are however some simple cases which have guaranteed
     * indices:
     * </p>
     * <ul>
     *   <li>the index of 0<sup>th</sup> order derivative is always 0</li>
     *   <li>if there is only 1 {@link #getFreeParameters() free parameter}, then the
     *   derivatives are sorted in increasing derivation order (i.e. f at index 0, df/dp
     *   at index 1, d<sup>2</sup>f/dp<sup>2</sup> at index 2 ...
     *   d<sup>k</sup>f/dp<sup>k</sup> at index k),</li>
     *   <li>if the {@link #getOrder() derivation order} is 1, then the derivatives
     *   are sorted in increasing free parameter order (i.e. f at index 0, df/dx<sub>1</sub>
     *   at index 1, df/dx<sub>2</sub> at index 2 ... df/dx<sub>k</sub> at index k),</li>
     *   <li>all other cases are not publicly specified</li>
     * </ul>
     * <p>
     * This method is the inverse of method {@link #getPartialDerivativeOrders(int)}
     * </p>
     * @param orders derivation orders with respect to each parameter
     * @return index of the partial derivative
     * @exception DimensionMismatchException if the numbers of parameters does not
     * match the instance
     * @exception NumberIsTooLargeException if sum of derivation orders is larger
     * than the instance limits
     * @see #getPartialDerivativeOrders(int)
     */
    pub fn get_partial_derivative_index(&self, orders: &Vec<i32>) -> Result<i32, Rc<Exception>> {
        // safety check
        if orders.len() != self.get_free_parameters() as usize {
            Err(Exc::new("DimensionMismatchException"))
            // throw DimensionMismatchException::new(orders.len(), &self.get_free_parameters());
        } else {
            DSCompiler::get_partial_derivative_index_static(self.parameters,
                                                            self.order,
                                                            &self.sizes,
                                                            orders)
        }
    }

    /** Get the index of a partial derivative in an array.
     * @param parameters number of free parameters
     * @param order derivation order
     * @param sizes sizes array
     * @param orders derivation orders with respect to each parameter
     * (the lenght of this array must match the number of parameters)
     * @return index of the partial derivative
     * @exception NumberIsTooLargeException if sum of derivation orders is larger
     * than the instance limits
     */
    fn get_partial_derivative_index_static(parameters: i32,
                                           order: i32,
                                           sizes: &Vec<Vec<i32>>,
                                           orders: &Vec<i32>)
                                           -> Result<i32, Rc<Exception>> {
        // the value is obtained by diving into the recursive Dan Kalman's structure
        // this is theorem 2 of his paper, with recursion replaced by iteration
        let mut index: usize = 0;
        let mut m: usize = order as usize;
        let mut orders_sum: i32 = 0;
        {
            let mut i: i32 = parameters - 1;
            while i >= 0 {
                {
                    // derivative order for current free parameter
                    let mut derivative_order: i32 = orders[i as usize];
                    // safety check
                    orders_sum += derivative_order;
                    if orders_sum > order {
                        return Err(Exc::new("NumberIsTooLargeException"));
                        // throw NumberIsTooLargeException::new(&orders_sum, &order, true);
                    }
                    while derivative_order > 0 {
                        derivative_order -= 1;
                        // as long as we differentiate according to current free parameter,
                        // we have to skip the value part and dive into the derivative part
                        // so we add the size of the value part to the base index
                        index += sizes[i as usize][m] as usize;
                        m -= 1;
                    }

                }
                i -= 1;
            }
        }

        Ok(index as i32)
    }

    /** Convert an index from one (parameters, order) structure to another.
     * @param index index of a partial derivative in source derivative structure
     * @param srcP number of free parameters in source derivative structure
     * @param srcDerivativesIndirection derivatives indirection array for the source
     * derivative structure
     * @param destP number of free parameters in destination derivative structure
     * @param destO derivation order in destination derivative structure
     * @param destSizes sizes array for the destination derivative structure
     * @return index of the partial derivative with the <em>same</em> characteristics
     * in destination derivative structure
     * @throws NumberIsTooLargeException if order is too large
     */
    fn convert_index(index: i32,
                     src_p: i32,
                     src_derivatives_indirection: &Vec<Vec<i32>>,
                     dest_p: i32,
                     dest_o: i32,
                     dest_sizes: &Vec<Vec<i32>>)
                     -> Result<i32, Rc<Exception>> {
        let mut orders = vec![0; dest_p as usize];
        let num = I32::min(src_p, dest_p);
        for i in 0..num {
            orders[i as usize] = src_derivatives_indirection[index as usize][i as usize];
        }
        return DSCompiler::get_partial_derivative_index_static(dest_p,
                                                               dest_o,
                                                               &dest_sizes,
                                                               &orders);
    }

    /** Get the derivation orders for a specific index in the array.
     * <p>
     * This method is the inverse of {@link #getPartialDerivativeIndex(int...)}.
     * </p>
     * @param index of the partial derivative
     * @return orders derivation orders with respect to each parameter
     * @see #getPartialDerivativeIndex(int...)
     */
    pub fn get_partial_derivative_orders(&self, index: i32) -> Vec<i32> {
        return self.derivatives_indirection[index as usize].clone();
    }

    /** Get the array size required for holding partial derivatives data.
     * <p>
     * This number includes the single 0 order derivative element, which is
     * guaranteed to be stored in the first element of the array.
     * </p>
     * @return array size required for holding partial derivatives data
     */
    pub fn get_size(&self) -> usize {
        return self.sizes[self.parameters as usize][self.order as usize].clone() as usize;
    }

    /** Get the number of free parameters.
     * @return number of free parameters
     */
    pub fn get_free_parameters(&self) -> usize {
        return self.parameters as usize;
    }

    /** Get the derivation order.
     * @return derivation order
     */
    pub fn get_order(&self) -> usize {
        return self.order as usize;
    }

    /** Compute linear combination.
     * The derivative structure built will be a1 * ds1 + a2 * ds2
     * @param a1 first scale factor
     * @param c1 first base (unscaled) component
     * @param offset1 offset of first operand in its array
     * @param a2 second scale factor
     * @param c2 second base (unscaled) component
     * @param offset2 offset of second operand in its array
     * @param result array where result must be stored (it may be
     * one of the input arrays)
     * @param resultOffset offset of the result in its array
     */
    pub fn linear_combination_2d(&self,
                                 a1: f64,
                                 c1: &Vec<f64>,
                                 offset1: usize,
                                 a2: f64,
                                 c2: &Vec<f64>,
                                 offset2: usize,
                                 result_cell: &RefCell<&mut Vec<f64>>,
                                 result_offset: usize) {
        {
            let mut result = result_cell.borrow_mut();
            let mut i: usize = 0;
            while i < self.get_size() {
                {
                    let tmp =
                        MathArrays::linear_combination_2d(a1, c1[offset1 + i], a2, c2[offset2 + i]);
                    result[result_offset + i] = tmp;
                }
                i += 1;
            }
        }

    }

    /** Compute linear combination.
     * The derivative structure built will be a1 * ds1 + a2 * ds2 + a3 * ds3 + a4 * ds4
     * @param a1 first scale factor
     * @param c1 first base (unscaled) component
     * @param offset1 offset of first operand in its array
     * @param a2 second scale factor
     * @param c2 second base (unscaled) component
     * @param offset2 offset of second operand in its array
     * @param a3 third scale factor
     * @param c3 third base (unscaled) component
     * @param offset3 offset of third operand in its array
     * @param result array where result must be stored (it may be
     * one of the input arrays)
     * @param resultOffset offset of the result in its array
     */
    pub fn linear_combination_3d(&self,
                                 a1: f64,
                                 c1: &Vec<f64>,
                                 offset1: usize,
                                 a2: f64,
                                 c2: &Vec<f64>,
                                 offset2: usize,
                                 a3: f64,
                                 c3: &Vec<f64>,
                                 offset3: usize,
                                 result_cell: &RefCell<&mut Vec<f64>>,
                                 result_offset: usize) {
        {
            let mut result = result_cell.borrow_mut();
            let mut i: usize = 0;
            while i < self.get_size() {
                {
                    result[result_offset + i] = MathArrays::linear_combination_3d(a1,
                                                                                  c1[offset1 + i],
                                                                                  a2,
                                                                                  c2[offset2 + i],
                                                                                  a3,
                                                                                  c3[offset3 + i]);
                }
                i += 1;
            }
        }

    }




    /** Compute linear combination.
     * The derivative structure built will be a1 * ds1 + a2 * ds2 + a3 * ds3 + a4 * ds4
     * @param a1 first scale factor
     * @param c1 first base (unscaled) component
     * @param offset1 offset of first operand in its array
     * @param a2 second scale factor
     * @param c2 second base (unscaled) component
     * @param offset2 offset of second operand in its array
     * @param a3 third scale factor
     * @param c3 third base (unscaled) component
     * @param offset3 offset of third operand in its array
     * @param a4 fourth scale factor
     * @param c4 fourth base (unscaled) component
     * @param offset4 offset of fourth operand in its array
     * @param result array where result must be stored (it may be
     * one of the input arrays)
     * @param resultOffset offset of the result in its array
     */
    pub fn linear_combination_4d(&self,
                                 a1: f64,
                                 c1: &Vec<f64>,
                                 offset1: usize,
                                 a2: f64,
                                 c2: &Vec<f64>,
                                 offset2: usize,
                                 a3: f64,
                                 c3: &Vec<f64>,
                                 offset3: usize,
                                 a4: f64,
                                 c4: &Vec<f64>,
                                 offset4: usize,
                                 result_cell: &RefCell<&mut Vec<f64>>,
                                 result_offset: usize) {
        {
            let mut result = result_cell.borrow_mut();
            let mut i: usize = 0;
            while i < self.get_size() {
                {
                    result[result_offset + i] = MathArrays::linear_combination_4d(a1,
                                                                                  c1[offset1 + i],
                                                                                  a2,
                                                                                  c2[offset2 + i],
                                                                                  a3,
                                                                                  c3[offset3 + i],
                                                                                  a4,
                                                                                  c4[offset4 + i]);
                }
                i += 1;
            }
        }

    }



    /** Perform addition of two derivative structures.
     * @param lhs array holding left hand side of addition
     * @param lhsOffset offset of the left hand side in its array
     * @param rhs array right hand side of addition
     * @param rhsOffset offset of the right hand side in its array
     * @param result array where result must be stored (it may be
     * one of the input arrays)
     * @param resultOffset offset of the result in its array
     */
    pub fn add(&self,
               lhs: &Vec<f64>,
               lhs_offset: usize,
               rhs: &Vec<f64>,
               rhs_offset: usize,
               result_cell: &RefCell<&mut Vec<f64>>,
               result_offset: usize) {
        {
            let mut result = result_cell.borrow_mut();
            let mut i: usize = 0;
            while i < self.get_size() {
                {
                    result[result_offset + i] = lhs[lhs_offset + i] + rhs[rhs_offset + i];
                }
                i += 1;
            }
        }

    }

    /** Perform subtraction of two derivative structures.
     * @param lhs array holding left hand side of subtraction
     * @param lhsOffset offset of the left hand side in its array
     * @param rhs array right hand side of subtraction
     * @param rhsOffset offset of the right hand side in its array
     * @param result array where result must be stored (it may be
     * one of the input arrays)
     * @param resultOffset offset of the result in its array
     */
    pub fn subtract(&self,
                    lhs: &Vec<f64>,
                    lhs_offset: usize,
                    rhs: &Vec<f64>,
                    rhs_offset: usize,
                    result_cell: &RefCell<&mut Vec<f64>>,
                    result_offset: usize) {
        {
            let mut result = result_cell.borrow_mut();
            let mut i: usize = 0;
            while i < self.get_size() {
                {
                    result[result_offset + i] = lhs[lhs_offset + i] - rhs[rhs_offset + i];
                }
                i += 1;
            }
        }

    }

    /** Perform multiplication of two derivative structures.
     * @param lhs array holding left hand side of multiplication
     * @param lhsOffset offset of the left hand side in its array
     * @param rhs array right hand side of multiplication
     * @param rhsOffset offset of the right hand side in its array
     * @param result array where result must be stored (for
     * multiplication the result array <em>cannot</em> be one of
     * the input arrays)
     * @param resultOffset offset of the result in its array
     */
    pub fn multiply(&self,
                    lhs: &Vec<f64>,
                    lhs_offset: usize,
                    rhs: &Vec<f64>,
                    rhs_offset: usize,
                    result_cell: &RefCell<&mut Vec<f64>>,
                    result_offset: usize) {
        {
            let mut result = result_cell.borrow_mut();
            let mut i: usize = 0;
            while i < self.mult_indirection.len() {
                {
                    let mapping_i: &Vec<Vec<i32>> = &self.mult_indirection[i];
                    let mut r: f64 = 0.0;
                    {
                        let mut j: usize = 0;
                        while j < mapping_i.len() {
                            {
                                r += mapping_i[j][0] as f64 *
                                     lhs[lhs_offset + mapping_i[j][1] as usize] *
                                     rhs[rhs_offset + mapping_i[j][2] as usize];
                            }
                            j += 1;
                        }
                    }

                    result[result_offset + i] = r;
                }
                i += 1;
            }
        }

    }


    /** Perform division of two derivative structures.
     * @param lhs array holding left hand side of division
     * @param lhsOffset offset of the left hand side in its array
     * @param rhs array right hand side of division
     * @param rhsOffset offset of the right hand side in its array
     * @param result array where result must be stored (for
     * division the result array <em>cannot</em> be one of
     * the input arrays)
     * @param resultOffset offset of the result in its array
     */
    pub fn divide(&self,
                  lhs: &Vec<f64>,
                  lhs_offset: usize,
                  rhs: &Vec<f64>,
                  rhs_offset: usize,
                  result_cell: &RefCell<&mut Vec<f64>>,
                  result_offset: usize) {
        let mut vec = vec![0.0; self.get_size()];
        let reciprocal = RefCell::new(&mut vec);
        self.pow_array_i32(rhs, lhs_offset, -1, &reciprocal, 0);
        self.multiply(&lhs,
                      lhs_offset,
                      &reciprocal.borrow().clone(),
                      0,
                      result_cell,
                      result_offset);
    }

    /** Perform remainder of two derivative structures.
     * @param lhs array holding left hand side of remainder
     * @param lhsOffset offset of the left hand side in its array
     * @param rhs array right hand side of remainder
     * @param rhsOffset offset of the right hand side in its array
     * @param result array where result must be stored (it may be
     * one of the input arrays)
     * @param resultOffset offset of the result in its array
     */
    pub fn remainder(&self,
                     lhs: &Vec<f64>,
                     lhs_offset: usize,
                     rhs: &Vec<f64>,
                     rhs_offset: usize,
                     result_cell: &RefCell<&mut Vec<f64>>,
                     result_offset: usize) {
        // compute k such that lhs % rhs = lhs - k rhs
        let rem: f64 = F64::ieee_remainder(lhs[lhs_offset], rhs[rhs_offset]);
        let k: f64 = F64::rint((lhs[lhs_offset] - rem) / rhs[rhs_offset]);
        let mut result = result_cell.borrow_mut();
        // set up value
        result[result_offset] = rem;
        // set up partial derivatives
        {
            let mut i: usize = 1;
            while i < self.get_size() {
                {
                    result[result_offset + i] = lhs[lhs_offset + i] - k * rhs[rhs_offset + i];
                }
                i += 1;
            }
        }

    }

    /** Compute composition of a derivative structure by a function.
     * @param operand array holding the operand
     * @param operandOffset offset of the operand in its array
     * @param f array of value and derivatives of the function at
     * the current point (i.e. at {@code operand[operandOffset]}).
     * @param result array where result must be stored (for
     * composition the result array <em>cannot</em> be the input
     * array)
     * @param resultOffset offset of the result in its array
     */
    pub fn compose(&self,
                   operand: &Vec<f64>,
                   operand_offset: usize,
                   f: &Vec<f64>,
                   result_cell: &RefCell<&mut Vec<f64>>,
                   result_offset: usize) {
        let mut result = result_cell.borrow_mut();
        {
            let mut i: usize = 0;
            while i < self.comp_indirection.len() {
                {
                    let mapping_i: &Vec<Vec<i32>> = &self.comp_indirection[i];
                    let mut r: f64 = 0.0;
                    {
                        let mut j: usize = 0;
                        while j < mapping_i.len() {
                            {
                                let mapping_i_j: &Vec<i32> = &mapping_i[j];
                                let mut product: f64 = mapping_i_j[0] as f64 *
                                                       f[mapping_i_j[1] as usize];
                                {
                                    let mut k: usize = 2;
                                    while k < mapping_i_j.len() {
                                        {
                                            product *= operand[operand_offset +
                                                               mapping_i_j[k] as usize];
                                        }
                                        k += 1;
                                    }
                                }

                                r += product;
                            }
                            j += 1;
                        }
                    }

                    result[result_offset + i] = r;
                }
                i += 1;
            }
        }

    }

    /** Compute power of a double to a derivative structureV.
     * @param a number to exponentiate
     * @param operand array holding the power
     * @param operandOffset offset of the power in its array
     * @param result array where result must be stored (for
     * power the result array <em>cannot</em> be the input
     * array)
     * @param resultOffset offset of the result in its array
     * @since 3.3
     */
    pub fn pow(&self,
               a: f64,
               operand: &Vec<f64>,
               operand_offset: usize,
               result: &RefCell<&mut Vec<f64>>,
               result_offset: usize) {
        // create the function value and derivatives
        // [a^x, ln(a) a^x, ln(a)^2 a^x,, ln(a)^3 a^x, ... ]
        let mut function = vec![0.0; 1 + self.order as usize];
        if a == 0.0 {
            if operand[operand_offset] == 0.0 {
                function[0] = 1.0;
                let mut infinity: f64 = f64::INFINITY;
                {
                    let mut i: usize = 1;
                    while i < function.len() {
                        {
                            infinity = -infinity;
                            function[i] = infinity;
                        }
                        i += 1;
                    }
                }

            } else if operand[operand_offset] < 0.0 {
                for i in 0..function.len() {
                    function[i] = f64::NAN;
                }
            }
        } else {
            function[0] = F64::pow(a, operand[operand_offset]);
            let ln_a: f64 = F64::log(a);
            {
                let mut i: usize = 1;
                while i < function.len() {
                    {
                        function[i] = ln_a * function[i - 1];
                    }
                    i += 1;
                }
            }

        }
        // apply function composition
        self.compose(&operand, operand_offset, &function, result, result_offset);
    }

    /** Compute power of a derivative structure.
     * @param operand array holding the operand
     * @param operandOffset offset of the operand in its array
     * @param p power to apply
     * @param result array where result must be stored (for
     * power the result array <em>cannot</em> be the input
     * array)
     * @param resultOffset offset of the result in its array
     */
    pub fn pow_array_f64(&self,
                         operand: &Vec<f64>,
                         operand_offset: usize,
                         p: f64,
                         result: &RefCell<&mut Vec<f64>>,
                         result_offset: usize) {
        // create the function value and derivatives
        // [x^p, px^(p-1), p(p-1)x^(p-2), ... ]
        let mut function = vec![0.0; 1 + self.order as usize];
        let mut xk: f64 = F64::pow(operand[operand_offset], p - self.order as f64);
        {
            let mut i: usize = self.order as usize;
            while i > 0 {
                {
                    function[i] = xk;
                    xk *= operand[operand_offset];
                }
                i -= 1;
            }
        }

        function[0] = xk;
        let mut coefficient: f64 = p;
        {
            let mut i: usize = 1;
            while i <= self.order as usize {
                {
                    function[i] *= coefficient;
                    coefficient *= p - i as f64;
                }
                i += 1;
            }
        }

        // apply function composition
        self.compose(&operand, operand_offset, &function, result, result_offset);
    }

    /** Compute integer power of a derivative structure.
     * @param operand array holding the operand
     * @param operandOffset offset of the operand in its array
     * @param n power to apply
     * @param result array where result must be stored (for
     * power the result array <em>cannot</em> be the input
     * array)
     * @param resultOffset offset of the result in its array
     */
    pub fn pow_array_i32(&self,
                         operand: &Vec<f64>,
                         operand_offset: usize,
                         n: i32,
                         result_cell: &RefCell<&mut Vec<f64>>,
                         result_offset: usize) {
        {
            let mut result = result_cell.borrow_mut();
            if n == 0 {
                // special case, x^0 = 1 for all x
                result[result_offset] = 1.0;
                for i in (result_offset + 1)..(result_offset + self.get_size()) {
                    result[i] = 0.0;
                }
                return;
            }
        }
        // create the power function value and derivatives
        // [x^n, nx^(n-1), n(n-1)x^(n-2), ... ]
        let mut function = vec![0.0; 1 + self.order as usize];
        {
            if n > 0 {
                // strictly positive power
                let max_order: i32 = I32::min(self.order, n as i32);
                let mut xk: f64 = F64::pow_i32(operand[operand_offset], n as i32 - max_order);
                {
                    let mut i: usize = max_order as usize;
                    while i > 0 {
                        {
                            function[i] = xk;
                            xk *= operand[operand_offset];
                        }
                        i -= 1;
                    }
                }

                function[0] = xk;
            } else {
                // strictly negative power
                let inv: f64 = 1.0 / operand[operand_offset];
                let mut xk: f64 = F64::pow_i32(inv, -(n as i32));
                {
                    let mut i: usize = 0;
                    while i <= self.order as usize {
                        {
                            function[i] = xk;
                            xk *= inv;
                        }
                        i += 1;
                    }
                }

            }
            let mut coefficient: f64 = n as f64;
            {
                let mut i: usize = 1;
                while i <= self.order as usize {
                    {
                        function[i] *= coefficient;
                        coefficient *= (n as i32 - i as i32) as f64;
                    }
                    i += 1;
                }
            }
        }

        // apply function composition
        self.compose(&operand,
                     operand_offset,
                     &function,
                     result_cell,
                     result_offset);
    }

    /** Compute power of a derivative structure.
     * @param x array holding the base
     * @param xOffset offset of the base in its array
     * @param y array holding the exponent
     * @param yOffset offset of the exponent in its array
     * @param result array where result must be stored (for
     * power the result array <em>cannot</em> be the input
     * array)
     * @param resultOffset offset of the result in its array
     */
    pub fn pow_vec(&self,
                   x: &Vec<f64>,
                   x_offset: usize,
                   y: &Vec<f64>,
                   y_offset: usize,
                   result_cell: &RefCell<&mut Vec<f64>>,
                   result_offset: usize) {
        let mut vec1 = vec![0.0; self.get_size()];
        let log_x = RefCell::new(&mut vec1);
        self.log(&x, x_offset, &log_x, 0);
        let mut vec2 = vec![0.0; self.get_size()];
        let y_log_x = RefCell::new(&mut vec2);
        self.multiply(&log_x.borrow(), 0, &y, y_offset, &y_log_x, 0);
        self.exp(&y_log_x.borrow(), 0, result_cell, result_offset);
    }

    /** Compute exponential of a derivative structure.
     * @param operand array holding the operand
     * @param operandOffset offset of the operand in its array
     * @param result array where result must be stored (for
     * exponential the result array <em>cannot</em> be the input
     * array)
     * @param resultOffset offset of the result in its array
     */
    pub fn exp(&self,
               operand: &Vec<f64>,
               operand_offset: usize,
               result_cell: &RefCell<&mut Vec<f64>>,
               result_offset: usize) {
        // create the function value and derivatives
        let function = vec![F64::exp(operand[operand_offset]); 1 + self.order as usize];

        // apply function composition
        self.compose(operand,
                     operand_offset,
                     &function,
                     &result_cell,
                     result_offset);
    }

    /** Compute exp(x) - 1 of a derivative structure.
     * @param operand array holding the operand
     * @param operandOffset offset of the operand in its array
     * @param result array where result must be stored (for
     * exponential the result array <em>cannot</em> be the input
     * array)
     * @param resultOffset offset of the result in its array
     */
    pub fn expm1(&self,
                 operand: &Vec<f64>,
                 operand_offset: usize,
                 result_cell: &RefCell<&mut Vec<f64>>,
                 result_offset: usize) {
        // create the function value and derivatives
        let mut function = vec![0.0; 1 + self.order as usize];
        function[0] = F64::expm1(operand[operand_offset]);
        let fill = F64::exp(operand[operand_offset]);
        for i in 1..(1 + self.order as usize) {
            function[i] = fill;
        }
        // apply function composition
        self.compose(&operand,
                     operand_offset,
                     &function,
                     result_cell,
                     result_offset);
    }

    /** Compute natural logarithm of a derivative structure.
     * @param operand array holding the operand
     * @param operandOffset offset of the operand in its array
     * @param result array where result must be stored (for
     * logarithm the result array <em>cannot</em> be the input
     * array)
     * @param resultOffset offset of the result in its array
     */
    pub fn log(&self,
               operand: &Vec<f64>,
               operand_offset: usize,
               result_cell: &RefCell<&mut Vec<f64>>,
               result_offset: usize) {
        // create the function value and derivatives
        let mut function = vec![0.0; 1 + self.order as usize];
        function[0] = F64::log(operand[operand_offset]);
        if self.order > 0 {
            let inv: f64 = 1.0 / operand[operand_offset];
            let mut xk: f64 = inv;
            {
                let mut i: i32 = 1;
                while i <= self.order {
                    {
                        function[i as usize] = xk;
                        xk *= -i as f64 * inv;
                    }
                    i += 1;
                }
            }

        }
        // apply function composition
        self.compose(&operand,
                     operand_offset,
                     &function,
                     result_cell,
                     result_offset);
    }

    /** Computes shifted logarithm of a derivative structure.
     * @param operand array holding the operand
     * @param operandOffset offset of the operand in its array
     * @param result array where result must be stored (for
     * shifted logarithm the result array <em>cannot</em> be the input array)
     * @param resultOffset offset of the result in its array
     */
    pub fn log1p(&self,
                 operand: &Vec<f64>,
                 operand_offset: usize,
                 result_cell: &RefCell<&mut Vec<f64>>,
                 result_offset: usize) {
        // create the function value and derivatives
        let mut function = vec![0.0; 1 + self.order as usize];
        function[0] = F64::log1p(operand[operand_offset]);
        if self.order > 0 {
            let inv: f64 = 1.0 / (1.0 + operand[operand_offset]);
            let mut xk: f64 = inv;
            {
                let mut i: i32 = 1;
                while i <= self.order {
                    {
                        function[i as usize] = xk;
                        xk *= -i as f64 * inv;
                    }
                    i += 1;
                }
            }

        }
        // apply function composition
        self.compose(&operand,
                     operand_offset,
                     &function,
                     result_cell,
                     result_offset);
    }

    /** Computes base 10 logarithm of a derivative structure.
     * @param operand array holding the operand
     * @param operandOffset offset of the operand in its array
     * @param result array where result must be stored (for
     * base 10 logarithm the result array <em>cannot</em> be the input array)
     * @param resultOffset offset of the result in its array
     */
    pub fn log10(&self,
                 operand: &Vec<f64>,
                 operand_offset: usize,
                 result_cell: &RefCell<&mut Vec<f64>>,
                 result_offset: usize) {
        // create the function value and derivatives
        let mut function = vec![0.0; 1 + self.order as usize];
        function[0] = F64::log10(operand[operand_offset]);
        if self.order > 0 {
            let inv: f64 = 1.0 / operand[operand_offset];
            let mut xk: f64 = inv / F64::log(10.0);
            {
                let mut i: i32 = 1;
                while i <= self.order {
                    {
                        function[i as usize] = xk;
                        xk *= -i as f64 * inv;
                    }
                    i += 1;
                }
            }

        }
        // apply function composition
        self.compose(&operand,
                     operand_offset,
                     &function,
                     result_cell,
                     result_offset);
    }

    /** Compute n<sup>th</sup> root of a derivative structure.
     * @param operand array holding the operand
     * @param operandOffset offset of the operand in its array
     * @param n order of the root
     * @param result array where result must be stored (for
     * n<sup>th</sup> root the result array <em>cannot</em> be the input
     * array)
     * @param resultOffset offset of the result in its array
     */
    pub fn root_n(&self,
                  operand: &Vec<f64>,
                  operand_offset: usize,
                  n: usize,
                  result_cell: &RefCell<&mut Vec<f64>>,
                  result_offset: usize) {
        // create the function value and derivatives
        // [x^(1/n), (1/n)x^((1/n)-1), (1-n)/n^2x^((1/n)-2), ... ]
        let mut function = vec![0.0; 1 + self.order as usize];
        let mut xk: f64;
        if n == 2 {
            function[0] = F64::sqrt(operand[operand_offset]);
            xk = 0.5 / function[0];
        } else if n == 3 {
            function[0] = F64::cbrt(operand[operand_offset]);
            xk = 1.0 / (3.0 * function[0] * function[0]);
        } else {
            function[0] = F64::pow(operand[operand_offset], 1.0 / n as f64);
            xk = 1.0 / ((n as f64) * F64::pow_i32(function[0], n as i32 - 1));
        }
        let n_reciprocal: f64 = 1.0 / n as f64;
        let x_reciprocal: f64 = 1.0 / operand[operand_offset];
        {
            let mut i: usize = 1;
            while i <= self.order as usize {
                {
                    function[i] = xk;
                    xk *= x_reciprocal * (n_reciprocal - i as f64);
                }
                i += 1;
            }
        }

        // apply function composition
        self.compose(operand,
                     operand_offset,
                     &function,
                     result_cell,
                     result_offset);
    }



    /** Compute cosine of a derivative structure.
     * @param operand array holding the operand
     * @param operandOffset offset of the operand in its array
     * @param result array where result must be stored (for
     * cosine the result array <em>cannot</em> be the input
     * array)
     * @param resultOffset offset of the result in its array
     */
    pub fn cos(&self,
               operand: &Vec<f64>,
               operand_offset: usize,
               result_cell: &RefCell<&mut Vec<f64>>,
               result_offset: usize) {
        // create the function value and derivatives
        let mut function = vec![0.0; 1 + self.order as usize];
        function[0] = F64::cos(operand[operand_offset]);
        if self.order > 0 {
            function[1] = -F64::sin(operand[operand_offset]);
            {
                let mut i: usize = 2;
                while i <= self.order as usize {
                    {
                        function[i] = -function[i - 2];
                    }
                    i += 1;
                }
            }

        }
        // apply function composition
        self.compose(&operand,
                     operand_offset,
                     &function,
                     result_cell,
                     result_offset);
    }

    /** Compute sine of a derivative structure.
     * @param operand array holding the operand
     * @param operandOffset offset of the operand in its array
     * @param result array where result must be stored (for
     * sine the result array <em>cannot</em> be the input
     * array)
     * @param resultOffset offset of the result in its array
     */
    pub fn sin(&self,
               operand: &Vec<f64>,
               operand_offset: usize,
               result_cell: &RefCell<&mut Vec<f64>>,
               result_offset: usize) {
        // create the function value and derivatives
        let mut function = vec![0.0; 1 + self.order as usize];
        function[0] = F64::sin(operand[operand_offset]);
        if self.order > 0 {
            function[1] = F64::cos(operand[operand_offset]);
            {
                let mut i: usize = 2;
                while i <= self.order as usize {
                    {
                        function[i] = -function[i - 2];
                    }
                    i += 1;
                }
            }

        }
        // apply function composition
        self.compose(&operand,
                     operand_offset,
                     &function,
                     result_cell,
                     result_offset);
    }

    /** Compute tangent of a derivative structure.
     * @param operand array holding the operand
     * @param operandOffset offset of the operand in its array
     * @param result array where result must be stored (for
     * tangent the result array <em>cannot</em> be the input
     * array)
     * @param resultOffset offset of the result in its array
     */
    pub fn tan(&self,
               operand: &Vec<f64>,
               operand_offset: usize,
               result_cell: &RefCell<&mut Vec<f64>>,
               result_offset: usize) {
        // create the function value and derivatives
        let mut function = vec![0.0; 1 + self.order as usize];

        let t: f64 = F64::tan(operand[operand_offset]);
        function[0] = t;
        if self.order > 0 {
            // the nth order derivative of tan has the form:
            // dn(tan(x)/dxn = P_n(tan(x))
            // where P_n(t) is a degree n+1 polynomial with same parity as n+1
            // P_0(t) = t, P_1(t) = 1 + t^2, P_2(t) = 2 t (1 + t^2) ...
            // the general recurrence relation for P_n is:
            // P_n(x) = (1+t^2) P_(n-1)'(t)
            // as per polynomial parity, we can store coefficients of both P_(n-1) and P_n in the same array
            let mut p = vec![0.0; self.order as usize + 2];
            p[1] = 1.0;
            let t2: f64 = t * t;
            {
                let mut n: usize = 1;
                while n <= self.order as usize {
                    {
                        // update and evaluate polynomial P_n(t)
                        let mut v: f64 = 0.0;
                        p[n + 1] = n as f64 * p[n];
                        {
                            let mut k: usize = n + 1;
                            loop {
                                {
                                    v = v * t2 + p[k];
                                    if k > 2 {
                                        p[k - 2] = (k as f64 - 1.0) * p[k - 1] +
                                                   (k as f64 - 3.0) * p[k - 3];
                                    } else if k == 2 {
                                        p[0] = p[1];
                                    }
                                }
                                if k < 2 {
                                    break;
                                }
                                k -= 2;
                            }
                        }

                        if (n & 0x1) == 0 {
                            v *= t;
                        }
                        function[n] = v;
                    }
                    n += 1;
                }
            }

        }
        // apply function composition
        self.compose(&operand,
                     operand_offset,
                     &function,
                     result_cell,
                     result_offset);
    }

    /** Compute arc cosine of a derivative structure.
     * @param operand array holding the operand
     * @param operandOffset offset of the operand in its array
     * @param result array where result must be stored (for
     * arc cosine the result array <em>cannot</em> be the input
     * array)
     * @param resultOffset offset of the result in its array
     */
    pub fn acos(&self,
                operand: &Vec<f64>,
                operand_offset: usize,
                result_cell: &RefCell<&mut Vec<f64>>,
                result_offset: usize) {
        // create the function value and derivatives
        let mut function = vec![0.0; 1 + self.order as usize];
        let x: f64 = operand[operand_offset];
        function[0] = F64::acos(x);
        if self.order > 0 {
            // the nth order derivative of acos has the form:
            // dn(acos(x)/dxn = P_n(x) / [1 - x^2]^((2n-1)/2)
            // where P_n(x) is a degree n-1 polynomial with same parity as n-1
            // P_1(x) = -1, P_2(x) = -x, P_3(x) = -2x^2 - 1 ...
            // the general recurrence relation for P_n is:
            // P_n(x) = (1-x^2) P_(n-1)'(x) + (2n-3) x P_(n-1)(x)
            // as per polynomial parity, we can store coefficients of both P_(n-1) and P_n in the same array
            let mut p = vec![0.0; self.order as usize + 2];
            p[0] = -1.0;
            let x2: f64 = x * x;
            let f: f64 = 1.0 / (1.0 - x2);
            let mut coeff: f64 = F64::sqrt(f);
            function[1] = coeff * p[0];
            {
                let mut n: usize = 2;
                while n <= self.order as usize {
                    {
                        // update and evaluate polynomial P_n(x)
                        let mut v: f64 = 0.0;
                        p[n - 1] = (n as f64 - 1.0) * p[n - 2];
                        {
                            let mut k: usize = n - 1;
                            loop {
                                {
                                    v = v * x2 + p[k];
                                    if k > 2 {
                                        p[k - 2] = (k as f64 - 1.0) * p[k - 1] +
                                                   ((2 * n - k) as f64) * p[k - 3];
                                    } else if k == 2 {
                                        p[0] = p[1];
                                    }
                                }
                                if k < 2 {
                                    break;
                                }
                                k -= 2;
                            }
                        }

                        if (n & 0x1) == 0 {
                            v *= x;
                        }
                        coeff *= f;
                        function[n] = coeff * v;
                    }
                    n += 1;
                }
            }

        }
        // apply function composition
        self.compose(&operand,
                     operand_offset,
                     &function,
                     result_cell,
                     result_offset);
    }

    /** Compute arc sine of a derivative structure.
     * @param operand array holding the operand
     * @param operandOffset offset of the operand in its array
     * @param result array where result must be stored (for
     * arc sine the result array <em>cannot</em> be the input
     * array)
     * @param resultOffset offset of the result in its array
     */
    pub fn asin(&self,
                operand: &Vec<f64>,
                operand_offset: usize,
                result: &RefCell<&mut Vec<f64>>,
                result_offset: usize) {
        // create the function value and derivatives
        let mut function = vec![0.0; 1 + self.order as usize];
        let x: f64 = operand[operand_offset];
        function[0] = F64::asin(x);
        if self.order > 0 {
            // the nth order derivative of asin has the form:
            // dn(asin(x)/dxn = P_n(x) / [1 - x^2]^((2n-1)/2)
            // where P_n(x) is a degree n-1 polynomial with same parity as n-1
            // P_1(x) = 1, P_2(x) = x, P_3(x) = 2x^2 + 1 ...
            // the general recurrence relation for P_n is:
            // P_n(x) = (1-x^2) P_(n-1)'(x) + (2n-3) x P_(n-1)(x)
            // as per polynomial parity, we can store coefficients of both P_(n-1) and P_n in the same array
            let mut p = vec![0.0; self.order as usize];
            p[0] = 1.0;
            let x2: f64 = x * x;
            let f: f64 = 1.0 / (1.0 - x2);
            let mut coeff: f64 = F64::sqrt(f);
            function[1] = coeff * p[0];
            {
                let mut n: usize = 2;
                while n <= self.order as usize {
                    {
                        // update and evaluate polynomial P_n(x)
                        let mut v: f64 = 0.0;
                        p[n - 1] = (n - 1) as f64 * p[n - 2];
                        {
                            let mut k: usize = n - 1;
                            loop {
                                {
                                    v = v * x2 + p[k];
                                    if k > 2 {
                                        p[k - 2] = (k as f64 - 1.0) * p[k - 1] +
                                                   (2 * n - k) as f64 * p[k - 3];
                                    } else if k == 2 {
                                        p[0] = p[1];
                                    }
                                }
                                if k < 2 {
                                    break;
                                }
                                k -= 2;
                            }
                        }

                        if (n & 0x1) == 0 {
                            v *= x;
                        }
                        coeff *= f;
                        function[n] = coeff * v;
                    }
                    n += 1;
                }
            }

        }
        // apply function composition
        self.compose(&operand, operand_offset, &function, result, result_offset);
    }

    /** Compute arc tangent of a derivative structure.
     * @param operand array holding the operand
     * @param operandOffset offset of the operand in its array
     * @param result array where result must be stored (for
     * arc tangent the result array <em>cannot</em> be the input
     * array)
     * @param resultOffset offset of the result in its array
     */
    pub fn atan(&self,
                operand: &Vec<f64>,
                operand_offset: usize,
                result: &RefCell<&mut Vec<f64>>,
                result_offset: usize) {
        // create the function value and derivatives
        let mut function = vec![0.0; 1 + self.order as usize];
        let x: f64 = operand[operand_offset];
        function[0] = F64::atan(x);
        if self.order > 0 {
            // the nth order derivative of atan has the form:
            // dn(atan(x)/dxn = Q_n(x) / (1 + x^2)^n
            // where Q_n(x) is a degree n-1 polynomial with same parity as n-1
            // Q_1(x) = 1, Q_2(x) = -2x, Q_3(x) = 6x^2 - 2 ...
            // the general recurrence relation for Q_n is:
            // Q_n(x) = (1+x^2) Q_(n-1)'(x) - 2(n-1) x Q_(n-1)(x)
            // as per polynomial parity, we can store coefficients of both Q_(n-1) and Q_n in the same array
            let mut q = vec![0.0; self.order as usize];
            q[0] = 1.0;
            let x2: f64 = x * x;
            let f: f64 = 1.0 / (1.0 + x2);
            let mut coeff: f64 = f;
            function[1] = coeff * q[0];
            {
                let mut n: usize = 2;
                while n <= self.order as usize {
                    {
                        // update and evaluate polynomial Q_n(x)
                        let mut v: f64 = 0.0;
                        q[n - 1] = -(n as f64) * q[n - 2];
                        {
                            let mut k: usize = n - 1;
                            loop {
                                {
                                    v = v * x2 + q[k];
                                    if k > 2 {
                                        q[k - 2] = (k - 1) as f64 * q[k - 1] +
                                                   (k as i32 - 1 - 2 * n as i32) as f64 * q[k - 3];
                                    } else if k == 2 {
                                        q[0] = q[1];
                                    }
                                }
                                if k < 2 {
                                    break;
                                }
                                k -= 2;
                            }
                        }

                        if (n & 0x1) == 0 {
                            v *= x;
                        }
                        coeff *= f;
                        function[n] = coeff * v;
                    }
                    n += 1;
                }
            }

        }
        // apply function composition
        self.compose(&operand, operand_offset, &function, result, result_offset);
    }


    /** Compute two arguments arc tangent of a derivative structure.
     * @param y array holding the first operand
     * @param yOffset offset of the first operand in its array
     * @param x array holding the second operand
     * @param xOffset offset of the second operand in its array
     * @param result array where result must be stored (for
     * two arguments arc tangent the result array <em>cannot</em>
     * be the input array)
     * @param resultOffset offset of the result in its array
     */
    pub fn atan2(&self,
                 y: &Vec<f64>,
                 y_offset: usize,
                 x: &Vec<f64>,
                 x_offset: usize,
                 result_cell: &RefCell<&mut Vec<f64>>,
                 result_offset: usize) {
        // compute r = sqrt(x^2+y^2)
        let mut vec1 = vec![0.0; self.get_size()];

        // x^2
        self.multiply(x, x_offset, x, x_offset, &RefCell::new(&mut vec1), 0);
        let mut vec2 = vec![0.0; self.get_size()];

        // y^2
        self.multiply(&y, y_offset, &y, y_offset, &RefCell::new(&mut vec2), 0);
        let mut vec3 = vec![0.0; self.get_size()];
        // x^2 + y^2
        self.add(&vec1, 0, &vec2, 0, &RefCell::new(&mut vec3), 0);
        // r = sqrt(x^2 + y^2)
        self.root_n(&vec3, 0, 2, &RefCell::new(&mut vec1), 0);
        let mut result = result_cell.borrow_mut();
        if x[x_offset] >= 0.0 {
            // compute atan2(y, x) = 2 atan(y / (r + x))
            // r + x
            self.add(&vec1, 0, &x, x_offset, &RefCell::new(&mut vec2), 0);
            // y /(r + x)
            self.divide(&y, y_offset, &vec2, 0, &RefCell::new(&mut vec1), 0);
            // atan(y / (r + x))
            self.atan(&vec1, 0, &RefCell::new(&mut vec2), 0);
            {
                let mut i: usize = 0;
                while i < vec2.len() {
                    {
                        // 2 * atan(y / (r + x))
                        result[result_offset + i] = 2.0 * vec2[i];
                    }
                    i += 1;
                }
            }

        } else {
            // compute atan2(y, x) = +/- pi - 2 atan(y / (r - x))
            // r - x
            self.subtract(&vec1, 0, &x, x_offset, &RefCell::new(&mut vec2), 0);
            // y /(r - x)
            self.divide(y, y_offset, &vec2, 0, &RefCell::new(&mut vec1), 0);
            // atan(y / (r - x))
            self.atan(&vec1, 0, &RefCell::new(&mut vec2), 0);
            result[result_offset] = // +/-pi - 2 * atan(y / (r - x))
             (if vec2[0] <= 0.0 { -fastmath::PI } else { fastmath::PI }) - 2.0 * vec2[0];
            {
                let mut i: usize = 1;
                while i < vec2.len() {
                    {
                        // +/-pi - 2 * atan(y / (r - x))
                        result[result_offset + i] = -2.0 * vec2[i];
                    }
                    i += 1;
                }
            }

        }
        // fix value to take special cases (+0/+0, +0/-0, -0/+0, -0/-0, +/-infinity) correctly
        result[result_offset] = F64::atan2(y[y_offset], x[x_offset]);
    }

    /** Compute hyperbolic cosine of a derivative structure.
     * @param operand array holding the operand
     * @param operandOffset offset of the operand in its array
     * @param result array where result must be stored (for
     * hyperbolic cosine the result array <em>cannot</em> be the input
     * array)
     * @param resultOffset offset of the result in its array
     */
    pub fn cosh(&self,
                operand: &Vec<f64>,
                operand_offset: usize,
                result: &RefCell<&mut Vec<f64>>,
                result_offset: usize) {
        // create the function value and derivatives
        let mut function = vec![0.0; 1 + self.order as usize];
        function[0] = F64::cosh(operand[operand_offset]);
        if self.order > 0 {
            function[1] = F64::sinh(operand[operand_offset]);
            {
                let mut i: usize = 2;
                while i <= self.order as usize {
                    {
                        function[i] = function[i - 2];
                    }
                    i += 1;
                }
            }

        }
        // apply function composition
        self.compose(operand, operand_offset, &function, result, result_offset);
    }

    /** Compute hyperbolic sine of a derivative structure.
     * @param operand array holding the operand
     * @param operandOffset offset of the operand in its array
     * @param result array where result must be stored (for
     * hyperbolic sine the result array <em>cannot</em> be the input
     * array)
     * @param resultOffset offset of the result in its array
     */
    pub fn sinh(&self,
                operand: &Vec<f64>,
                operand_offset: usize,
                result: &RefCell<&mut Vec<f64>>,
                result_offset: usize) {
        // create the function value and derivatives
        let mut function = vec![0.0; 1 + self.order as usize];
        function[0] = F64::sinh(operand[operand_offset]);
        if self.order > 0 {
            function[1] = F64::cosh(operand[operand_offset]);
            {
                let mut i: usize = 2;
                while i <= self.order as usize {
                    {
                        function[i] = function[i - 2];
                    }
                    i += 1;
                }
            }

        }
        // apply function composition
        self.compose(&operand, operand_offset, &function, result, result_offset);
    }

    /** Compute hyperbolic tangent of a derivative structure.
     * @param operand array holding the operand
     * @param operandOffset offset of the operand in its array
     * @param result array where result must be stored (for
     * hyperbolic tangent the result array <em>cannot</em> be the input
     * array)
     * @param resultOffset offset of the result in its array
     */
    pub fn tanh(&self,
                operand: &Vec<f64>,
                operand_offset: usize,
                result: &RefCell<&mut Vec<f64>>,
                result_offset: usize) {
        // create the function value and derivatives
        let mut function = vec![0.0; 1 + self.order as usize];
        let t: f64 = F64::tanh(operand[operand_offset]);
        function[0] = t;
        if self.order > 0 {
            // the nth order derivative of tanh has the form:
            // dn(tanh(x)/dxn = P_n(tanh(x))
            // where P_n(t) is a degree n+1 polynomial with same parity as n+1
            // P_0(t) = t, P_1(t) = 1 - t^2, P_2(t) = -2 t (1 - t^2) ...
            // the general recurrence relation for P_n is:
            // P_n(x) = (1-t^2) P_(n-1)'(t)
            // as per polynomial parity, we can store coefficients of both P_(n-1) and P_n in the same array
            let mut p = vec![0.0; self.order as usize + 2];
            p[1] = 1.0;
            let t2: f64 = t * t;
            {
                let mut n: usize = 1;
                while n <= self.order as usize {
                    {
                        // update and evaluate polynomial P_n(t)
                        let mut v: f64 = 0.0;
                        p[n + 1] = -(n as f64) * p[n];
                        {
                            let mut k: usize = n + 1;
                            loop {
                                {
                                    v = v * t2 + p[k];
                                    if k > 2 {
                                        p[k - 2] = (k as f64 - 1.0) * p[k - 1] -
                                                   (k as f64 - 3.0) * p[k - 3];
                                    } else if k == 2 {
                                        p[0] = p[1];
                                    }
                                }
                                if k < 2 {
                                    break;
                                }
                                k -= 2;
                            }
                        }

                        if (n & 0x1) == 0 {
                            v *= t;
                        }
                        function[n] = v;
                    }
                    n += 1;
                }
            }

        }
        // apply function composition
        self.compose(&operand, operand_offset, &function, result, result_offset);
    }

    /** Compute inverse hyperbolic cosine of a derivative structure.
     * @param operand array holding the operand
     * @param operandOffset offset of the operand in its array
     * @param result array where result must be stored (for
     * inverse hyperbolic cosine the result array <em>cannot</em> be the input
     * array)
     * @param resultOffset offset of the result in its array
     */
    pub fn acosh(&self,
                 operand: &Vec<f64>,
                 operand_offset: usize,
                 result: &RefCell<&mut Vec<f64>>,
                 result_offset: usize) {
        // create the function value and derivatives
        let mut function = vec![0.0; 1 + self.order as usize];
        let x: f64 = operand[operand_offset];
        function[0] = F64::acosh(x);
        if self.order > 0 {
            // the nth order derivative of acosh has the form:
            // dn(acosh(x)/dxn = P_n(x) / [x^2 - 1]^((2n-1)/2)
            // where P_n(x) is a degree n-1 polynomial with same parity as n-1
            // P_1(x) = 1, P_2(x) = -x, P_3(x) = 2x^2 + 1 ...
            // the general recurrence relation for P_n is:
            // P_n(x) = (x^2-1) P_(n-1)'(x) - (2n-3) x P_(n-1)(x)
            // as per polynomial parity, we can store coefficients of both P_(n-1) and P_n in the same array
            let mut p = vec![0.0; self.order as usize];
            p[0] = 1.0;
            let x2: f64 = x * x;
            let f: f64 = 1.0 / (x2 - 1.0);
            let mut coeff: f64 = F64::sqrt(f);
            function[1] = coeff * p[0];
            {
                let mut n: usize = 2;
                while n <= self.order as usize {
                    {
                        // update and evaluate polynomial P_n(x)
                        let mut v: f64 = 0.0;
                        p[n - 1] = (1.0 - n as f64) * p[n - 2];
                        {
                            let mut k: usize = n - 1;
                            loop {
                                {
                                    v = v * x2 + p[k];
                                    if k > 2 {
                                        p[k - 2] = (1.0 - k as f64) * p[k - 1] +
                                                   (k as f64 - 2.0 * n as f64) * p[k - 3];
                                    } else if k == 2 {
                                        p[0] = -p[1];
                                    }
                                }
                                if k < 2 {
                                    break;
                                }
                                k -= 2;
                            }
                        }

                        if (n & 0x1) == 0 {
                            v *= x;
                        }
                        coeff *= f;
                        function[n] = coeff * v;
                    }
                    n += 1;
                }
            }

        }
        // apply function composition
        self.compose(&operand, operand_offset, &function, result, result_offset);
    }

    /** Compute inverse hyperbolic sine of a derivative structure.
     * @param operand array holding the operand
     * @param operandOffset offset of the operand in its array
     * @param result array where result must be stored (for
     * inverse hyperbolic sine the result array <em>cannot</em> be the input
     * array)
     * @param resultOffset offset of the result in its array
     */
    pub fn asinh(&self,
                 operand: &Vec<f64>,
                 operand_offset: usize,
                 result: &RefCell<&mut Vec<f64>>,
                 result_offset: usize) {
        // create the function value and derivatives
        let mut function = vec![0.0; 1 + self.order as usize];
        let x: f64 = operand[operand_offset];
        function[0] = F64::asinh(x);
        if self.order > 0 {
            // the nth order derivative of asinh has the form:
            // dn(asinh(x)/dxn = P_n(x) / [x^2 + 1]^((2n-1)/2)
            // where P_n(x) is a degree n-1 polynomial with same parity as n-1
            // P_1(x) = 1, P_2(x) = -x, P_3(x) = 2x^2 - 1 ...
            // the general recurrence relation for P_n is:
            // P_n(x) = (x^2+1) P_(n-1)'(x) - (2n-3) x P_(n-1)(x)
            // as per polynomial parity, we can store coefficients of both P_(n-1) and P_n in the same array
            let mut p = vec![0.0; self.order as usize];
            p[0] = 1.0;
            let x2: f64 = x * x;
            let f: f64 = 1.0 / (1.0 + x2);
            let mut coeff: f64 = F64::sqrt(f);
            function[1] = coeff * p[0];
            {
                let mut n: usize = 2;
                while n <= self.order as usize {
                    {
                        // update and evaluate polynomial P_n(x)
                        let mut v: f64 = 0.0;
                        p[n - 1] = (1.0 - n as f64) * p[n - 2];
                        {
                            let mut k: usize = n - 1;
                            loop {
                                {
                                    v = v * x2 + p[k];
                                    if k > 2 {
                                        p[k - 2] = (k as f64 - 1.0) * p[k - 1] +
                                                   (k as f64 - 2.0 * n as f64) * p[k - 3];
                                    } else if k == 2 {
                                        p[0] = p[1];
                                    }
                                }
                                if k < 2 {
                                    break;
                                }
                                k -= 2;
                            }
                        }

                        if (n & 0x1) == 0 {
                            v *= x;
                        }
                        coeff *= f;
                        function[n] = coeff * v;
                    }
                    n += 1;
                }
            }

        }
        // apply function composition
        self.compose(&operand, operand_offset, &function, result, result_offset);
    }

    /** Compute inverse hyperbolic tangent of a derivative structure.
     * @param operand array holding the operand
     * @param operandOffset offset of the operand in its array
     * @param result array where result must be stored (for
     * inverse hyperbolic tangent the result array <em>cannot</em> be the input
     * array)
     * @param resultOffset offset of the result in its array
     */
    pub fn atanh(&self,
                 operand: &Vec<f64>,
                 operand_offset: usize,
                 result: &RefCell<&mut Vec<f64>>,
                 result_offset: usize) {
        // create the function value and derivatives
        let mut function = vec![0.0; 1 + self.order as usize];
        let x: f64 = operand[operand_offset];
        function[0] = F64::atanh(x);
        if self.order > 0 {
            // the nth order derivative of atanh has the form:
            // dn(atanh(x)/dxn = Q_n(x) / (1 - x^2)^n
            // where Q_n(x) is a degree n-1 polynomial with same parity as n-1
            // Q_1(x) = 1, Q_2(x) = 2x, Q_3(x) = 6x^2 + 2 ...
            // the general recurrence relation for Q_n is:
            // Q_n(x) = (1-x^2) Q_(n-1)'(x) + 2(n-1) x Q_(n-1)(x)
            // as per polynomial parity, we can store coefficients of both Q_(n-1) and Q_n in the same array
            let mut q = vec![0.0; self.order as usize];
            q[0] = 1.0;
            let x2: f64 = x * x;
            let f: f64 = 1.0 / (1.0 - x2);
            let mut coeff: f64 = f;
            function[1] = coeff * q[0];
            {
                let mut n: usize = 2;
                while n <= self.order as usize {
                    {
                        // update and evaluate polynomial Q_n(x)
                        let mut v: f64 = 0.0;
                        q[n - 1] = n as f64 * q[n - 2];
                        {
                            let mut k: usize = n - 1;
                            loop {
                                {
                                    v = v * x2 + q[k];
                                    if k > 2 {
                                        q[k - 2] = (k as f64 - 1.0) * q[k - 1] +
                                                   (2.0 * n as f64 - k as f64 + 1.0) * q[k - 3];
                                    } else if k == 2 {
                                        q[0] = q[1];
                                    }
                                }
                                if k < 2 {
                                    break;
                                }
                                k -= 2;
                            }
                        }

                        if (n & 0x1) == 0 {
                            v *= x;
                        }
                        coeff *= f;
                        function[n] = coeff * v;
                    }
                    n += 1;
                }
            }

        }
        // apply function composition
        self.compose(&operand, operand_offset, &function, result, result_offset);
    }

    /** Evaluate Taylor expansion of a derivative structure.
     * @param ds array holding the derivative structure
     * @param dsOffset offset of the derivative structure in its array
     * @param delta parameters offsets (Δx, Δy, ...)
     * @return value of the Taylor expansion at x + Δx, y + Δy, ...
     * @throws MathArithmeticException if factorials becomes too large
     */
    pub fn taylor(&self,
                  ds: &Vec<f64>,
                  ds_offset: usize,
                  delta: &Vec<f64>)
                  -> Result<f64, Rc<Exception>> {
        let mut value: f64 = 0.0;
        {
            let mut i: usize = self.get_size() - 1;
            loop {
                {
                    let orders: Vec<i32> = self.get_partial_derivative_orders(i as i32);
                    let mut term: f64 = ds[ds_offset + i];
                    {
                        let mut k: usize = 0;
                        while k < orders.len() {
                            {
                                if orders[k] > 0 {
                                    let fac = try!(CombinatoricsUtils::factorial(orders[k]));
                                    let pow = F64::pow_i32(delta[k], orders[k]);
                                    term *= pow / fac as f64;

                                }
                            }
                            k += 1;
                        }
                    }

                    value += term;
                }
                if i == 0 {
                    break;
                }
                i -= 1;
            }
        }

        return Ok(value);
    }

    /** Check rules set compatibility.
     * @param compiler other compiler to check against instance
     * @exception DimensionMismatchException if number of free parameters or orders are inconsistent
     */
    pub fn check_compatibility(&self, compiler: &DSCompiler) -> Result<Void, Rc<Exception>> {
        if self.parameters != compiler.parameters {
            return Err(Exc::new_msg("DimensionMismatchException", format!("parameters: {}, compiler.parameters: {}",self.parameters, compiler.parameters)));
        }
        if self.order != compiler.order {
            return Err(Exc::new_msg("DimensionMismatchException", format!("order: {}, compiler.order: {}",self.order, compiler.order)));
        }
        Ok(Void::new())
    }
}
