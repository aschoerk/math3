
// #[cfg(test)]
// mod tests {
	use analysis::differentiation::ds_compiler::*;
	use util::combinatorics_utils::*;
	use std::collections::HashMap;
	use std::cell::*;
	use java::exc::*;
	use std::fmt::Write;
	
	#[test]
	pub fn test() {
		let c = DSCompiler::get_compiler(10,5).unwrap();
	}
	
	#[test]
    pub fn  test_size()   {
         {
             let mut i: u32 = 0;
            while i < 6 {
                {
                     {
                         let mut j: u32 = 0;
                        while j < 6 {
                            {
                                 let expected: i64 = CombinatoricsUtils::binomial_coefficient(i + j, i).unwrap();
                                assert_eq!(expected as usize, DSCompiler::get_compiler(i as usize, j as usize).unwrap().get_size());
                                assert_eq!(expected as usize, DSCompiler::get_compiler(j as usize, i as usize).unwrap().get_size());
                            }
                            j += 1;
                         }
                     }

                }
                i += 1;
             }
         }

    }
    
    
    
    #[test]
    pub fn  test_indices()   {
         let mut c: DSCompiler = DSCompiler::get_compiler(0, 0).unwrap();
        assert_eq!(&c.get_partial_derivative_orders(0), &Vec::new());
        c = DSCompiler::get_compiler(0, 1).unwrap();
        assert_eq!(&c.get_partial_derivative_orders(0), &Vec::new());
        c = DSCompiler::get_compiler(1, 0).unwrap();
        assert_eq!(&c.get_partial_derivative_orders(0), &vec![0]);
        c = DSCompiler::get_compiler(1, 1).unwrap();
        assert_eq!(&c.get_partial_derivative_orders(0), &vec![0]);
        assert_eq!(&c.get_partial_derivative_orders(1), &vec![1]);
        c = DSCompiler::get_compiler(1, 2).unwrap();
        assert_eq!(&c.get_partial_derivative_orders(0), &vec![0]);
        assert_eq!(&c.get_partial_derivative_orders(1), &vec![1]);
        assert_eq!(&c.get_partial_derivative_orders(2), &vec![2]);
        c = DSCompiler::get_compiler(2, 1).unwrap();
        assert_eq!(&c.get_partial_derivative_orders(0), &vec![0, 0]);
        assert_eq!(&c.get_partial_derivative_orders(1), &vec![1, 0]);
        assert_eq!(&c.get_partial_derivative_orders(2), &vec![0, 1]);
        c = DSCompiler::get_compiler(1, 3).unwrap();
        assert_eq!(&c.get_partial_derivative_orders(0), &vec![0]);
        assert_eq!(&c.get_partial_derivative_orders(1), &vec![1]);
        assert_eq!(&c.get_partial_derivative_orders(2), &vec![2]);
        assert_eq!(&c.get_partial_derivative_orders(3), &vec![3]);
        c = DSCompiler::get_compiler(2, 2).unwrap();
        assert_eq!(&c.get_partial_derivative_orders(0), &vec![0, 0]);
        assert_eq!(&c.get_partial_derivative_orders(1), &vec![1, 0]);
        assert_eq!(&c.get_partial_derivative_orders(2), &vec![2, 0]);
        assert_eq!(&c.get_partial_derivative_orders(3), &vec![0, 1]);
        assert_eq!(&c.get_partial_derivative_orders(4), &vec![1, 1]);
        assert_eq!(&c.get_partial_derivative_orders(5), &vec![0, 2]);
        c = DSCompiler::get_compiler(3, 1).unwrap();
        assert_eq!(&c.get_partial_derivative_orders(0), &vec![0, 0, 0]);
        assert_eq!(&c.get_partial_derivative_orders(1), &vec![1, 0, 0]);
        assert_eq!(&c.get_partial_derivative_orders(2), &vec![0, 1, 0]);
        assert_eq!(&c.get_partial_derivative_orders(3), &vec![0, 0, 1]);
        c = DSCompiler::get_compiler(1, 4).unwrap();
        assert_eq!(&c.get_partial_derivative_orders(0), &vec![0]);
        assert_eq!(&c.get_partial_derivative_orders(1), &vec![1]);
        assert_eq!(&c.get_partial_derivative_orders(2), &vec![2]);
        assert_eq!(&c.get_partial_derivative_orders(3), &vec![3]);
        assert_eq!(&c.get_partial_derivative_orders(4), &vec![4]);
        c = DSCompiler::get_compiler(2, 3).unwrap();
        assert_eq!(&c.get_partial_derivative_orders(0), &vec![0, 0]);
        assert_eq!(&c.get_partial_derivative_orders(1), &vec![1, 0]);
        assert_eq!(&c.get_partial_derivative_orders(2), &vec![2, 0]);
        assert_eq!(&c.get_partial_derivative_orders(3), &vec![3, 0]);
        assert_eq!(&c.get_partial_derivative_orders(4), &vec![0, 1]);
        assert_eq!(&c.get_partial_derivative_orders(5), &vec![1, 1]);
        assert_eq!(&c.get_partial_derivative_orders(6), &vec![2, 1]);
        assert_eq!(&c.get_partial_derivative_orders(7), &vec![0, 2]);
        assert_eq!(&c.get_partial_derivative_orders(8), &vec![1, 2]);
        assert_eq!(&c.get_partial_derivative_orders(9), &vec![0, 3]);
        c = DSCompiler::get_compiler(3, 2).unwrap();
        assert_eq!(&c.get_partial_derivative_orders(0), &vec![0, 0, 0]);
        assert_eq!(&c.get_partial_derivative_orders(1), &vec![1, 0, 0]);
        assert_eq!(&c.get_partial_derivative_orders(2), &vec![2, 0, 0]);
        assert_eq!(&c.get_partial_derivative_orders(3), &vec![0, 1, 0]);
        assert_eq!(&c.get_partial_derivative_orders(4), &vec![1, 1, 0]);
        assert_eq!(&c.get_partial_derivative_orders(5), &vec![0, 2, 0]);
        assert_eq!(&c.get_partial_derivative_orders(6), &vec![0, 0, 1]);
        assert_eq!(&c.get_partial_derivative_orders(7), &vec![1, 0, 1]);
        assert_eq!(&c.get_partial_derivative_orders(8), &vec![0, 1, 1]);
        assert_eq!(&c.get_partial_derivative_orders(9), &vec![0, 0, 2]);
        c = DSCompiler::get_compiler(4, 1).unwrap();
        assert_eq!(&c.get_partial_derivative_orders(0), &vec![0, 0, 0, 0]);
        assert_eq!(&c.get_partial_derivative_orders(1), &vec![1, 0, 0, 0]);
        assert_eq!(&c.get_partial_derivative_orders(2), &vec![0, 1, 0, 0]);
        assert_eq!(&c.get_partial_derivative_orders(3), &vec![0, 0, 1, 0]);
        assert_eq!(&c.get_partial_derivative_orders(4), &vec![0, 0, 0, 1]);
    }

#[test]
    pub fn  test_incompatible_params()   {
        assert!(DSCompiler::get_compiler(3, 2).unwrap().check_compatibility(&DSCompiler::get_compiler(4, 2).unwrap()).is_err());
    }

    #[test]
    pub fn  test_incompatible_order()   {
        assert!(DSCompiler::get_compiler(3, 3).unwrap().check_compatibility(&DSCompiler::get_compiler(3, 2).unwrap()).is_err());
    }
	
	#[test]
    pub fn  test_symmetry()   {
         {
             let mut i: usize = 0;
            while i < 6 {
                {
                     {
                         let mut j: usize = 0;
                        while j < 6 {
                            {
                                 let c: DSCompiler = DSCompiler::get_compiler(i, j).unwrap();
                                 {
                                     let mut k: usize = 0;
                                    while k < c.get_size() {
                                        {
                                            assert_eq!(k as i32, c.get_partial_derivative_index(&c.get_partial_derivative_orders(k as i32)).unwrap());
                                        }
                                        k += 1;
                                     }
                                 }

                            }
                            j += 1;
                         }
                     }

                }
                i += 1;
             }
         }

    }
    
    fn insert_replace(map_cell: &RefCell<HashMap<&'static str, String>>, newkey: &'static str, orgkey: &'static str, orgstr: &str, newstr: &str) {
    	let mut map = map_cell.borrow_mut();
    	let tmp = map.get(orgkey).unwrap().replace(orgstr, newstr);
    	map.insert(newkey, tmp);
    } 
    
     fn  order_to_string(order: i32,  function_name:  &str,  parameter_name:  &str) -> String  {
        if order == 0 {
            return String::from(function_name);
        } else if order == 1 {
            return format!("d{}/d{}", function_name, parameter_name);
        } else {
            return format!("d{}{}/d{}{}", order, function_name, parameter_name, order);
        }
    }
     
     fn  orders_to_string(orders: &Vec<i32>,  function_name: &str,  parameters_names: &Vec<&str>) -> String  {
         let mut sum_orders: i32 = 0;
        for order in orders {
            sum_orders += *order;
        }
        if sum_orders == 0 {
            return String::from(function_name);
        }
         let mut builder = String::new();
        builder.push('d');
        if sum_orders > 1 {
        	write!(&mut builder,"{}", sum_orders); 
        }
        builder.push_str(function_name);
        builder.push('/');
         {
             let mut i: usize = 0;
            while i < orders.len() {
                {
                    if orders[i] > 0 {
                    	write!(&mut builder,"d{}",parameters_names[i]);
                        if orders[i] > 1 {
                    	    write!(&mut builder,"{}",orders[i]);
                        }
                    }
                }
                i += 1;
             }
         }

        return builder;
    }
    
#[test]
    pub fn  test_multiplication_rules()  {
         let mut reference_rules_map = HashMap::<&'static str, String>::new();
        reference_rules_map.insert("(f*g)", String::from("f * g"));
        reference_rules_map.insert("d(f*g)/dx", String::from("f * dg/dx + df/dx * g"));
        reference_rules_map.insert("d2(f*g)/dx2", String::from("f * d2g/dx2 + 2 * df/dx * dg/dx + d2f/dx2 * g"));
        reference_rules_map.insert("d2(f*g)/dxdy", String::from("f * d2g/dxdy + df/dy * dg/dx + df/dx * dg/dy + d2f/dxdy * g"));
        reference_rules_map.insert("d3(f*g)/dx3", String::from("f * d3g/dx3 + 3 * df/dx * d2g/dx2 + 3 * d2f/dx2 * dg/dx + d3f/dx3 * g"));
        reference_rules_map.insert("d3(f*g)/dx2dy", String::from("f * d3g/dx2dy + df/dy * d2g/dx2 + 2 * df/dx * d2g/dxdy + 2 * d2f/dxdy * dg/dx + d2f/dx2 * dg/dy + d3f/dx2dy * g"));
        reference_rules_map.insert("d3(f*g)/dxdy2", String::from("f * d3g/dxdy2 + 2 * df/dy * d2g/dxdy + d2f/dy2 * dg/dx + df/dx * d2g/dy2 + 2 * d2f/dxdy * dg/dy + d3f/dxdy2 * g"));
        reference_rules_map.insert("d3(f*g)/dxdydz", String::from("f * d3g/dxdydz + df/dz * d2g/dxdy + df/dy * d2g/dxdz + d2f/dydz * dg/dx + df/dx * d2g/dydz + d2f/dxdz * dg/dy + d2f/dxdy * dg/dz + d3f/dxdydz * g"));

        let reference_rules = RefCell::new(reference_rules_map);
        insert_replace(&reference_rules, "d(f*g)/dy", "d(f*g)/dx","x", "y");         
        insert_replace(&reference_rules, "d(f*g)/dz", "d(f*g)/dx", "x", "z");
        insert_replace(&reference_rules, "d(f*g)/dt", "d(f*g)/dx", "x", "t");
        
        insert_replace(&reference_rules, "d2(f*g)/dy2", "d2(f*g)/dx2", "x", "y");
        insert_replace(&reference_rules, "d2(f*g)/dz2", "d2(f*g)/dx2", "x", "z");
        insert_replace(&reference_rules, "d2(f*g)/dt2", "d2(f*g)/dx2", "x", "t");
        insert_replace(&reference_rules, "d2(f*g)/dxdz", "d2(f*g)/dxdy", "y", "z");
        insert_replace(&reference_rules, "d2(f*g)/dxdt", "d2(f*g)/dxdy", "y", "t");
        insert_replace(&reference_rules, "d2(f*g)/dydz", "d2(f*g)/dxdz", "x", "y");
        insert_replace(&reference_rules, "d2(f*g)/dydt", "d2(f*g)/dxdt", "x", "y");
        insert_replace(&reference_rules, "d2(f*g)/dzdt", "d2(f*g)/dxdt", "x", "z");
        insert_replace(&reference_rules, "d3(f*g)/dy3", "d3(f*g)/dx3", "x", "y");
        insert_replace(&reference_rules, "d3(f*g)/dz3", "d3(f*g)/dx3", "x", "z");
        insert_replace(&reference_rules, "d3(f*g)/dt3", "d3(f*g)/dx3", "x", "t");
        insert_replace(&reference_rules, "d3(f*g)/dx2dz", "d3(f*g)/dx2dy", "y", "z");
        insert_replace(&reference_rules, "d3(f*g)/dy2dz", "d3(f*g)/dx2dz", "x", "y");
        insert_replace(&reference_rules, "d3(f*g)/dxdz2", "d3(f*g)/dxdy2", "y", "z");
        insert_replace(&reference_rules, "d3(f*g)/dydz2", "d3(f*g)/dxdz2", "x", "y");
        insert_replace(&reference_rules, "d3(f*g)/dx2dt", "d3(f*g)/dx2dz", "z", "t");
        insert_replace(&reference_rules, "d3(f*g)/dy2dt", "d3(f*g)/dx2dt", "x", "y");
        insert_replace(&reference_rules, "d3(f*g)/dz2dt", "d3(f*g)/dx2dt", "x", "z");
        insert_replace(&reference_rules, "d3(f*g)/dxdt2", "d3(f*g)/dxdy2", "y", "t");
        insert_replace(&reference_rules, "d3(f*g)/dydt2", "d3(f*g)/dxdt2", "x", "y");
        insert_replace(&reference_rules, "d3(f*g)/dzdt2", "d3(f*g)/dxdt2", "x", "z");
        insert_replace(&reference_rules, "d3(f*g)/dxdydt", "d3(f*g)/dxdydz", "z", "t");
        insert_replace(&reference_rules, "d3(f*g)/dxdzdt", "d3(f*g)/dxdydt", "y", "z");
        insert_replace(&reference_rules, "d3(f*g)/dydzdt", "d3(f*g)/dxdzdt", "x", "y");
        
         {
             let mut i: usize = 0;
            while i < 5 {
                {
                     {
                         let mut j: usize = 0;
                        while j < 4 {
                            {
                                 let compiler: DSCompiler = DSCompiler::get_compiler(i, j).unwrap();
                                 let mult_indirection: Vec<Vec<Vec<i32>>> = compiler.mult_indirection.clone();
                                 {
                                     let mut k: usize = 0;
                                    while k < mult_indirection.len() {
                                        {
                                             let product: String = orders_to_string(&compiler.get_partial_derivative_orders(k as i32), "(f*g)", &vec!["x", "y", "z", "t"]);
                                             let mut rule = String::new();
                                             
                                            for  term in mult_indirection[k].iter() {
                                                if rule.len() > 0 {
                                                    rule.push_str(" + ");
                                                }
                                                if term[0] > 1 {
                                                	write!(&mut rule,"{} * ",term[0]);
                                                }
                                                rule.push_str(&orders_to_string(&compiler.get_partial_derivative_orders(term[1]), "f", &vec!["x", "y", "z", "t"]));
                                                rule.push_str(" * ");
                                                rule.push_str(&orders_to_string(&compiler.get_partial_derivative_orders(term[2]), "g", &vec!["x", "y", "z", "t"]));
                                            }
                                            let r = reference_rules.borrow();
                                            let str: &str = &product.clone();
                                            assert!(*r.get(str).unwrap() == rule.to_string(), product);
                                        }
                                        k += 1;
                                     }
                                 }

                            }
                            j += 1;
                         }
                     }

                }
                i += 1;
             }
         }

    }
    
     #[test]
    pub fn  test_composition_rules() {
         let mut reference_rules_map = HashMap::<&'static str, String>::new();
        reference_rules_map.insert("(f(g))", String::from("(f(g))"));
        reference_rules_map.insert("d(f(g))/dx", String::from("d(f(g))/dg * dg/dx"));
        reference_rules_map.insert("d2(f(g))/dx2", String::from("d2(f(g))/dg2 * dg/dx * dg/dx + d(f(g))/dg * d2g/dx2"));
        reference_rules_map.insert("d2(f(g))/dxdy", String::from("d2(f(g))/dg2 * dg/dx * dg/dy + d(f(g))/dg * d2g/dxdy"));
        reference_rules_map.insert("d3(f(g))/dx3", String::from("d3(f(g))/dg3 * dg/dx * dg/dx * dg/dx + 3 * d2(f(g))/dg2 * dg/dx * d2g/dx2 + d(f(g))/dg * d3g/dx3"));
        reference_rules_map.insert("d3(f(g))/dxdy2", String::from("d3(f(g))/dg3 * dg/dx * dg/dy * dg/dy + 2 * d2(f(g))/dg2 * dg/dy * d2g/dxdy + d2(f(g))/dg2 * dg/dx * d2g/dy2 + d(f(g))/dg * d3g/dxdy2"));
        reference_rules_map.insert("d3(f(g))/dx2dy", String::from("d3(f(g))/dg3 * dg/dx * dg/dx * dg/dy + 2 * d2(f(g))/dg2 * dg/dx * d2g/dxdy + d2(f(g))/dg2 * d2g/dx2 * dg/dy + d(f(g))/dg * d3g/dx2dy"));
        reference_rules_map.insert("d3(f(g))/dxdydz", String::from("d3(f(g))/dg3 * dg/dx * dg/dy * dg/dz + d2(f(g))/dg2 * dg/dy * d2g/dxdz + d2(f(g))/dg2 * dg/dx * d2g/dydz + d2(f(g))/dg2 * d2g/dxdy * dg/dz + d(f(g))/dg * d3g/dxdydz"));
        reference_rules_map.insert("d4(f(g))/dx4", String::from("d4(f(g))/dg4 * dg/dx * dg/dx * dg/dx * dg/dx + 6 * d3(f(g))/dg3 * dg/dx * dg/dx * d2g/dx2 + 3 * d2(f(g))/dg2 * d2g/dx2 * d2g/dx2 + 4 * d2(f(g))/dg2 * dg/dx * d3g/dx3 + d(f(g))/dg * d4g/dx4"));
        reference_rules_map.insert("d4(f(g))/dx3dy", String::from("d4(f(g))/dg4 * dg/dx * dg/dx * dg/dx * dg/dy + 3 * d3(f(g))/dg3 * dg/dx * dg/dx * d2g/dxdy + 3 * d3(f(g))/dg3 * dg/dx * d2g/dx2 * dg/dy + 3 * d2(f(g))/dg2 * d2g/dx2 * d2g/dxdy + 3 * d2(f(g))/dg2 * dg/dx * d3g/dx2dy + d2(f(g))/dg2 * d3g/dx3 * dg/dy + d(f(g))/dg * d4g/dx3dy"));
        reference_rules_map.insert("d4(f(g))/dxdy3", String::from("d4(f(g))/dg4 * dg/dx * dg/dy * dg/dy * dg/dy + 3 * d3(f(g))/dg3 * dg/dy * dg/dy * d2g/dxdy + 3 * d3(f(g))/dg3 * dg/dx * dg/dy * d2g/dy2 + 3 * d2(f(g))/dg2 * d2g/dxdy * d2g/dy2 + 3 * d2(f(g))/dg2 * dg/dy * d3g/dxdy2 + d2(f(g))/dg2 * dg/dx * d3g/dy3 + d(f(g))/dg * d4g/dxdy3"));
        reference_rules_map.insert("d4(f(g))/dx2dy2", String::from("d4(f(g))/dg4 * dg/dx * dg/dx * dg/dy * dg/dy + 4 * d3(f(g))/dg3 * dg/dx * dg/dy * d2g/dxdy + d3(f(g))/dg3 * dg/dx * dg/dx * d2g/dy2 + 2 * d2(f(g))/dg2 * d2g/dxdy * d2g/dxdy + 2 * d2(f(g))/dg2 * dg/dx * d3g/dxdy2 + d3(f(g))/dg3 * d2g/dx2 * dg/dy * dg/dy + 2 * d2(f(g))/dg2 * dg/dy * d3g/dx2dy + d2(f(g))/dg2 * d2g/dx2 * d2g/dy2 + d(f(g))/dg * d4g/dx2dy2"));
        reference_rules_map.insert("d4(f(g))/dx2dydz", String::from("d4(f(g))/dg4 * dg/dx * dg/dx * dg/dy * dg/dz + 2 * d3(f(g))/dg3 * dg/dx * dg/dy * d2g/dxdz + d3(f(g))/dg3 * dg/dx * dg/dx * d2g/dydz + 2 * d3(f(g))/dg3 * dg/dx * d2g/dxdy * dg/dz + 2 * d2(f(g))/dg2 * d2g/dxdy * d2g/dxdz + 2 * d2(f(g))/dg2 * dg/dx * d3g/dxdydz + d3(f(g))/dg3 * d2g/dx2 * dg/dy * dg/dz + d2(f(g))/dg2 * dg/dy * d3g/dx2dz + d2(f(g))/dg2 * d2g/dx2 * d2g/dydz + d2(f(g))/dg2 * d3g/dx2dy * dg/dz + d(f(g))/dg * d4g/dx2dydz"));
        reference_rules_map.insert("d4(f(g))/dxdy2dz", String::from("d4(f(g))/dg4 * dg/dx * dg/dy * dg/dy * dg/dz + d3(f(g))/dg3 * dg/dy * dg/dy * d2g/dxdz + 2 * d3(f(g))/dg3 * dg/dx * dg/dy * d2g/dydz + 2 * d3(f(g))/dg3 * dg/dy * d2g/dxdy * dg/dz + 2 * d2(f(g))/dg2 * d2g/dxdy * d2g/dydz + 2 * d2(f(g))/dg2 * dg/dy * d3g/dxdydz + d3(f(g))/dg3 * dg/dx * d2g/dy2 * dg/dz + d2(f(g))/dg2 * d2g/dy2 * d2g/dxdz + d2(f(g))/dg2 * dg/dx * d3g/dy2dz + d2(f(g))/dg2 * d3g/dxdy2 * dg/dz + d(f(g))/dg * d4g/dxdy2dz"));
        reference_rules_map.insert("d4(f(g))/dxdydz2", String::from("d4(f(g))/dg4 * dg/dx * dg/dy * dg/dz * dg/dz + 2 * d3(f(g))/dg3 * dg/dy * dg/dz * d2g/dxdz + 2 * d3(f(g))/dg3 * dg/dx * dg/dz * d2g/dydz + d3(f(g))/dg3 * dg/dx * dg/dy * d2g/dz2 + 2 * d2(f(g))/dg2 * d2g/dxdz * d2g/dydz + d2(f(g))/dg2 * dg/dy * d3g/dxdz2 + d2(f(g))/dg2 * dg/dx * d3g/dydz2 + d3(f(g))/dg3 * d2g/dxdy * dg/dz * dg/dz + 2 * d2(f(g))/dg2 * dg/dz * d3g/dxdydz + d2(f(g))/dg2 * d2g/dxdy * d2g/dz2 + d(f(g))/dg * d4g/dxdydz2"));
        reference_rules_map.insert("d4(f(g))/dxdydzdt", String::from("d4(f(g))/dg4 * dg/dx * dg/dy * dg/dz * dg/dt + d3(f(g))/dg3 * dg/dy * dg/dz * d2g/dxdt + d3(f(g))/dg3 * dg/dx * dg/dz * d2g/dydt + d3(f(g))/dg3 * dg/dx * dg/dy * d2g/dzdt + d3(f(g))/dg3 * dg/dy * d2g/dxdz * dg/dt + d2(f(g))/dg2 * d2g/dxdz * d2g/dydt + d2(f(g))/dg2 * dg/dy * d3g/dxdzdt + d3(f(g))/dg3 * dg/dx * d2g/dydz * dg/dt + d2(f(g))/dg2 * d2g/dydz * d2g/dxdt + d2(f(g))/dg2 * dg/dx * d3g/dydzdt + d3(f(g))/dg3 * d2g/dxdy * dg/dz * dg/dt + d2(f(g))/dg2 * dg/dz * d3g/dxdydt + d2(f(g))/dg2 * d2g/dxdy * d2g/dzdt + d2(f(g))/dg2 * d3g/dxdydz * dg/dt + d(f(g))/dg * d4g/dxdydzdt"));
        let reference_rules = RefCell::new(reference_rules_map);
        
        insert_replace(&reference_rules, "d(f(g))/dy", "d(f(g))/dx", "x", "y");
        insert_replace(&reference_rules, "d(f(g))/dz", "d(f(g))/dx", "x", "z");
        insert_replace(&reference_rules, "d(f(g))/dt", "d(f(g))/dx", "x", "t");
        insert_replace(&reference_rules, "d2(f(g))/dy2", "d2(f(g))/dx2", "x", "y");
        insert_replace(&reference_rules, "d2(f(g))/dz2", "d2(f(g))/dx2", "x", "z");
        insert_replace(&reference_rules, "d2(f(g))/dt2", "d2(f(g))/dx2", "x", "t");
        insert_replace(&reference_rules, "d2(f(g))/dxdz", "d2(f(g))/dxdy", "y", "z");
        insert_replace(&reference_rules, "d2(f(g))/dxdt", "d2(f(g))/dxdy", "y", "t");
        insert_replace(&reference_rules, "d2(f(g))/dydz", "d2(f(g))/dxdz", "x", "y");
        insert_replace(&reference_rules, "d2(f(g))/dydt", "d2(f(g))/dxdt", "x", "y");
        insert_replace(&reference_rules, "d2(f(g))/dzdt", "d2(f(g))/dxdt", "x", "z");
        insert_replace(&reference_rules, "d3(f(g))/dy3", "d3(f(g))/dx3", "x", "y");
        insert_replace(&reference_rules, "d3(f(g))/dz3", "d3(f(g))/dx3", "x", "z");
        insert_replace(&reference_rules, "d3(f(g))/dt3", "d3(f(g))/dx3", "x", "t");
        insert_replace(&reference_rules, "d3(f(g))/dxdz2", "d3(f(g))/dxdy2", "y", "z");
        insert_replace(&reference_rules, "d3(f(g))/dxdt2", "d3(f(g))/dxdy2", "y", "t");
        insert_replace(&reference_rules, "d3(f(g))/dydz2", "d3(f(g))/dxdz2", "x", "y");
        insert_replace(&reference_rules, "d3(f(g))/dydt2", "d3(f(g))/dxdt2", "x", "y");
        insert_replace(&reference_rules, "d3(f(g))/dzdt2", "d3(f(g))/dxdt2", "x", "z");
        insert_replace(&reference_rules, "d3(f(g))/dx2dz", "d3(f(g))/dx2dy", "y", "z");
        insert_replace(&reference_rules, "d3(f(g))/dx2dt", "d3(f(g))/dx2dy", "y", "t");
        insert_replace(&reference_rules, "d3(f(g))/dy2dz", "d3(f(g))/dx2dz", "x", "y");
        insert_replace(&reference_rules, "d3(f(g))/dy2dt", "d3(f(g))/dx2dt", "x", "y");
        insert_replace(&reference_rules, "d3(f(g))/dz2dt", "d3(f(g))/dx2dt", "x", "z");
        insert_replace(&reference_rules, "d3(f(g))/dxdydt", "d3(f(g))/dxdydz", "z", "t");
        insert_replace(&reference_rules, "d3(f(g))/dxdzdt", "d3(f(g))/dxdydt", "y", "z");
        insert_replace(&reference_rules, "d3(f(g))/dydzdt", "d3(f(g))/dxdzdt", "x", "y");
        insert_replace(&reference_rules, "d4(f(g))/dy4", "d4(f(g))/dx4", "x", "y");
        insert_replace(&reference_rules, "d4(f(g))/dz4", "d4(f(g))/dx4", "x", "z");
        insert_replace(&reference_rules, "d4(f(g))/dt4", "d4(f(g))/dx4", "x", "t");
        insert_replace(&reference_rules, "d4(f(g))/dx3dz", "d4(f(g))/dx3dy", "y", "z");
        insert_replace(&reference_rules, "d4(f(g))/dx3dt", "d4(f(g))/dx3dy", "y", "t");
        insert_replace(&reference_rules, "d4(f(g))/dxdz3", "d4(f(g))/dxdy3", "y", "z");
        insert_replace(&reference_rules, "d4(f(g))/dxdt3", "d4(f(g))/dxdy3", "y", "t");
        insert_replace(&reference_rules, "d4(f(g))/dy3dz", "d4(f(g))/dx3dz", "x", "y");
        insert_replace(&reference_rules, "d4(f(g))/dy3dt", "d4(f(g))/dx3dt", "x", "y");
        insert_replace(&reference_rules, "d4(f(g))/dydz3", "d4(f(g))/dxdz3", "x", "y");
        insert_replace(&reference_rules, "d4(f(g))/dydt3", "d4(f(g))/dxdt3", "x", "y");
        insert_replace(&reference_rules, "d4(f(g))/dz3dt", "d4(f(g))/dx3dt", "x", "z");
        insert_replace(&reference_rules, "d4(f(g))/dzdt3", "d4(f(g))/dxdt3", "x", "z");
        insert_replace(&reference_rules, "d4(f(g))/dx2dz2", "d4(f(g))/dx2dy2", "y", "z");
        insert_replace(&reference_rules, "d4(f(g))/dx2dt2", "d4(f(g))/dx2dy2", "y", "t");
        insert_replace(&reference_rules, "d4(f(g))/dy2dz2", "d4(f(g))/dx2dz2", "x", "y");
        insert_replace(&reference_rules, "d4(f(g))/dy2dt2", "d4(f(g))/dx2dt2", "x", "y");
        insert_replace(&reference_rules, "d4(f(g))/dz2dt2", "d4(f(g))/dx2dt2", "x", "z");
        insert_replace(&reference_rules, "d4(f(g))/dx2dydt", "d4(f(g))/dx2dydz", "z", "t");
        insert_replace(&reference_rules, "d4(f(g))/dx2dzdt", "d4(f(g))/dx2dydt", "y", "z");
        insert_replace(&reference_rules, "d4(f(g))/dxdy2dt", "d4(f(g))/dxdy2dz", "z", "t");
        insert_replace(&reference_rules, "d4(f(g))/dy2dzdt", "d4(f(g))/dx2dzdt", "x", "y");
        insert_replace(&reference_rules, "d4(f(g))/dxdz2dt", "d4(f(g))/dxdy2dt", "y", "z");
        insert_replace(&reference_rules, "d4(f(g))/dydz2dt", "d4(f(g))/dxdz2dt", "x", "y");
        insert_replace(&reference_rules, "d4(f(g))/dxdydt2", "d4(f(g))/dxdydz2", "z", "t");
        insert_replace(&reference_rules, "d4(f(g))/dxdzdt2", "d4(f(g))/dxdydt2", "y", "z");
        insert_replace(&reference_rules, "d4(f(g))/dydzdt2", "d4(f(g))/dxdzdt2", "x", "y");
        
         {
             let mut i: usize = 0;
            while i < 5 {
                {
                     {
                         let mut j: usize = 0;
                        while j < 5 {
                            {
                            	
                                 let compiler: DSCompiler = DSCompiler::get_compiler(i, j).unwrap();
                                 let comp_indirection: Vec<Vec<Vec<i32>>> = compiler.comp_indirection.clone();
                                 {
                                     let mut k: usize = 0;
                                    while k < comp_indirection.len() {
                                        {  
                                             let product: String = orders_to_string(&compiler.get_partial_derivative_orders(k as i32), "(f(g))", &vec!["x", "y", "z", "t"]);
                                             let mut rule = String::new();
                                            for  term in comp_indirection[k].iter() {
                                                if rule.len() > 0 {
                                                    rule.push_str(" + ");
                                                }
                                                if term[0] > 1 {
                                                    write!(&mut rule,"{} * ",term[0]);
                                                }
                                                rule.push_str(&order_to_string(term[1], "(f(g))", "g"));
                                                 {
                                                     let mut l: usize = 2;
                                                    while l < term.len() {
                                                        {
                                                            rule.push_str(" * ");
                                                            rule.push_str(&orders_to_string(&compiler.get_partial_derivative_orders(term[l]), "g", &vec!["x", "y", "z", "t"]));
                                                        }
                                                        l += 1;
                                                     }
                                                 }

                                            }
                                            let r = reference_rules.borrow();
                                            let str: &str = &product.clone();
                                            assert!(*r.get(str).unwrap() == rule.to_string(), product);
                                            // Assert::assert_equals(&product, &reference_rules.get(&product), &rule.to_string());
                                        }
                                        k += 1;
                                     }
                                 }

                            }
                            j += 1;
                         }
                     }

                }
                i += 1;
             }
         }

    }


	
// }