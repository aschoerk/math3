use analysis::differentiation::derivative_structure::*;
use base::RealFieldElement;
use base::FieldElement;
use util::fastmath;


impl FieldElement<DerivativeStructure> for DerivativeStructure {
    /** '+' operator.
     * @param a right hand side parameter of the operator
     * @return this+a
     */
    fn add(&self, a: &DerivativeStructure) -> DerivativeStructure {
        self.add(a).unwrap()
    }

    /** '-' operator.
     * @param a right hand side parameter of the operator
     * @return this-a
     */
    fn subtract(&self, a: &DerivativeStructure) -> DerivativeStructure {
        self.subtract(a).unwrap()
    }

    fn negate(&self) -> DerivativeStructure {
        self.negate().unwrap()
    }

    fn multiply_i32(&self, n: i32) -> DerivativeStructure {
        self.multiply_i32(n).unwrap()
    }

    fn multiply(&self, a: &DerivativeStructure) -> DerivativeStructure {
        self.multiply(a).unwrap()
    }

    fn divide(&self, a: &DerivativeStructure) -> Result<DerivativeStructure, String> {
        // MathArithmeticsException
        Ok(self.divide(a).unwrap())
    }

    fn reciprocal(&self) -> Result<DerivativeStructure, String> {
        // MathArithmeticsException
        Ok(self.reciprocal().unwrap())
    }
}

impl RealFieldElement<DerivativeStructure> for DerivativeStructure {
    /** Get the real value of the number.
     * @return real value
     */
    fn get_real(&self) -> f64 {
        self.get_real()
    }

    /** '+' operator.
     * @param a right hand side parameter of the operator
     * @return this+a
     */
    fn add_f64(&self, a: f64) -> DerivativeStructure {
        self.add_f64(a).unwrap()
    }

    /** '-' operator.
     * @param a right hand side parameter of the operator
     * @return this-a
     */
    fn subtract_f64(&self, a: f64) -> DerivativeStructure {
        self.subtract_f64(a).unwrap()
    }

    /** '×' operator.
     * @param a right hand side parameter of the operator
     * @return this×a
     */
    fn multiply_f64(&self, a: f64) -> DerivativeStructure {
        self.multiply_f64(a).unwrap()
    }

    /** '÷' operator.
     * @param a right hand side parameter of the operator
     * @return this÷a
     */
    fn divide_f64(&self, a: f64) -> DerivativeStructure {
        self.divide_f64(a).unwrap()
    }

    /** IEEE remainder operator.
     * @param a right hand side parameter of the operator
     * @return this - n × a where n is the closest integer to this/a
     * (the even integer is chosen for n if this/a is halfway between two integers)
     */
    fn remainder_f64(&self, a: f64) -> DerivativeStructure {
        self.remainder_f64(a).unwrap()
    }

    /** IEEE remainder operator.
     * @param a right hand side parameter of the operator
     * @return this - n × a where n is the closest integer to this/a
     * (the even integer is chosen for n if this/a is halfway between two integers)
     * @exception DimensionMismatchException if number of free parameters or orders are inconsistent
     */
    fn remainder(&self, a: &DerivativeStructure) -> Result<DerivativeStructure, String> {
        Ok(self.remainder(a).unwrap())
    }

    /** absolute value.
     * @return abs(this)
     */
    fn abs(&self) -> DerivativeStructure {
        self.abs().unwrap()
    }

    /** Get the smallest whole number larger than instance.
     * @return ceil(this)
     */
    fn ceil(&self) -> DerivativeStructure {
        self.ceil().unwrap()
    }

    /** Get the largest whole number smaller than instance.
     * @return floor(this)
     */
    fn floor(&self) -> DerivativeStructure {
        self.floor().unwrap()
    }

    /** Get the whole number that is the nearest to the instance, or the even one if x is exactly half way between two integers.
     * @return a double number r such that r is an integer r - 0.5 ≤ this ≤ r + 0.5
     */
    fn rint(&self) -> DerivativeStructure {
        self.rint().unwrap()
    }

    /** Get the closest long to instance value.
     * @return closest long to {@link #getReal()}
     */
    fn round(&self) -> i64 {
        fastmath::F64::round(self.get_real())
    }

    /** Compute the signum of the instance.
     * The signum is -1 for negative numbers, +1 for positive numbers and 0 otherwise
     * @return -1.0, -0.0, +0.0, +1.0 or NaN depending on sign of a
     */
    fn signum(&self) -> DerivativeStructure {
        self.signum().unwrap()
    }

    /**
     * Returns the instance with the sign of the argument.
     * A NaN {@code sign} argument is treated as positive.
     *
     * @param sign the sign for the returned value
     * @return the instance with the same sign as the {@code sign} argument
     */
    fn copy_sign(&self, sign: &DerivativeStructure) -> DerivativeStructure {
        self.copy_sign(sign).unwrap()
    }

    /**
     * Returns the instance with the sign of the argument.
     * A NaN {@code sign} argument is treated as positive.
     *
     * @param sign the sign for the returned value
     * @return the instance with the same sign as the {@code sign} argument
     */
    fn copy_sign_f64(&self, sign: f64) -> DerivativeStructure {
        self.copy_sign_f64(sign).unwrap()
    }

    /**
     * Multiply the instance by a power of 2.
     * @param n power of 2
     * @return this × 2<sup>n</sup>
     */
    fn scalb(&self, n: i32) -> DerivativeStructure {
        self.scalb(n).unwrap()
    }

    /**
     * Returns the hypotenuse of a triangle with sides {@code this} and {@code y}
     * - sqrt(<i>this</i><sup>2</sup> +<i>y</i><sup>2</sup>)
     * avoiding intermediate overflow or underflow.
     *
     * <ul>
     * <li> If either argument is infinite, then the result is positive infinity.</li>
     * <li> else, if either argument is NaN then the result is NaN.</li>
     * </ul>
     *
     * @param y a value
     * @return sqrt(<i>this</i><sup>2</sup> +<i>y</i><sup>2</sup>)
     * @exception DimensionMismatchException if number of free parameters or orders are inconsistent
     */
    fn hypot(&self, y: &DerivativeStructure) -> Result<DerivativeStructure, String> {
        Ok(self.hypot(y).unwrap())
    }

    /** {@inheritDoc} */
    fn reciprocal(&self) -> DerivativeStructure {
        self.reciprocal().unwrap()
    }

    /** Square root.
     * @return square root of the instance
     */
    fn sqrt(&self) -> DerivativeStructure {
        self.sqrt().unwrap()
    }

    /** Cubic root.
     * @return cubic root of the instance
     */
    fn cbrt(&self) -> DerivativeStructure {
        self.cbrt().unwrap()
    }

    /** N<sup>th</sup> root.
     * @param n order of the root
     * @return n<sup>th</sup> root of the instance
     */
    fn root_n(&self, n: i32) -> DerivativeStructure {
        self.root_n(n).unwrap()
    }

    /** Power operation.
     * @param p power to apply
     * @return this<sup>p</sup>
     */
    fn pow_f64(&self, p: f64) -> DerivativeStructure {
        self.pow_f64(p).unwrap()
    }

    /** Integer power operation.
     * @param n power to apply
     * @return this<sup>n</sup>
     */
    fn pow_i32(&self, n: i32) -> DerivativeStructure {
        self.pow_i32(n).unwrap()
    }

    /** Power operation.
     * @param e exponent
     * @return this<sup>e</sup>
     * @exception DimensionMismatchException if number of free parameters or orders are inconsistent
     */
    fn pow(&self, e: &DerivativeStructure) -> Result<DerivativeStructure, String> {
        Ok(self.pow(e).unwrap())
    }

    /** Exponential.
     * @return exponential of the instance
     */
    fn exp(&self) -> DerivativeStructure {
        self.exp().unwrap()
    }

    /** Exponential minus 1.
     * @return exponential minus one of the instance
     */
    fn expm1(&self) -> DerivativeStructure {
        self.expm1().unwrap()
    }

    /** Natural logarithm.
     * @return logarithm of the instance
     */
    fn log(&self) -> DerivativeStructure {
        self.log().unwrap()
    }

    /** Shifted natural logarithm.
     * @return logarithm of one plus the instance
     */
    fn log1p(&self) -> DerivativeStructure {
        self.log1p().unwrap()
    }

    //    TODO: add this method in 4.0, as it is not possible to do it in 3.2
    //          due to incompatibility of the return type in the DerivativeStructure class
    //    /** Base 10 logarithm.
    //     * @return base 10 logarithm of the instance
    //     */
    //    DerivativeStructure log10();
    /** Cosine operation.
     * @return cos(this)
     */
    fn cos(&self) -> DerivativeStructure {
        self.cos().unwrap()
    }

    /** Sine operation.
     * @return sin(this)
     */
    fn sin(&self) -> DerivativeStructure {
        self.sin().unwrap()
    }

    /** Tangent operation.
     * @return tan(this)
     */
    fn tan(&self) -> DerivativeStructure {
        self.tan().unwrap()
    }

    /** Arc cosine operation.
     * @return acos(this)
     */
    fn acos(&self) -> DerivativeStructure {
        self.acos().unwrap()
    }

    /** Arc sine operation.
     * @return asin(this)
     */
    fn asin(&self) -> DerivativeStructure {
        self.asin().unwrap()
    }

    /** Arc tangent operation.
     * @return atan(this)
     */
    fn atan(&self) -> DerivativeStructure {
        self.atan().unwrap()
    }

    /** Two arguments arc tangent operation.
     * @param x second argument of the arc tangent
     * @return atan2(this, x)
     * @exception DimensionMismatchException if number of free parameters or orders are inconsistent
     */
    fn atan2(&self, x: &DerivativeStructure) -> Result<DerivativeStructure, String> {
        Ok(self.atan2(x).unwrap())
    }

    /** Hyperbolic cosine operation.
     * @return cosh(this)
     */
    fn cosh(&self) -> DerivativeStructure {
        self.cosh().unwrap()
    }

    /** Hyperbolic sine operation.
     * @return sinh(this)
     */
    fn sinh(&self) -> DerivativeStructure {
        self.sinh().unwrap()
    }

    /** Hyperbolic tangent operation.
     * @return tanh(this)
     */
    fn tanh(&self) -> DerivativeStructure {
        self.tanh().unwrap()
    }

    /** Inverse hyperbolic cosine operation.
     * @return acosh(this)
     */
    fn acosh(&self) -> DerivativeStructure {
        self.acosh().unwrap()
    }

    /** Inverse hyperbolic sine operation.
     * @return asin(this)
     */
    fn asinh(&self) -> DerivativeStructure {
        self.asinh().unwrap()
    }

    /** Inverse hyperbolic  tangent operation.
     * @return atanh(this)
     */
    fn atanh(&self) -> DerivativeStructure {
        self.atanh().unwrap()
    }

    /**
     * Compute a linear combination.
     * @param a Factors.
     * @param b Factors.
     * @return <code>Σ<sub>i</sub> a<sub>i</sub> b<sub>i</sub></code>.
     * @throws DimensionMismatchException if arrays dimensions don't match
     * @since 3.2
     */
    fn linear_combination_vec(&self,
                              a: &Vec<DerivativeStructure>,
                              b: &Vec<DerivativeStructure>)
                              -> Result<DerivativeStructure, String> {
        Ok(self.linear_combination_vec(a, b).unwrap())
    }

    /**
     * Compute a linear combination.
     * @param a Factors.
     * @param b Factors.
     * @return <code>Σ<sub>i</sub> a<sub>i</sub> b<sub>i</sub></code>.
     * @throws DimensionMismatchException if arrays dimensions don't match
     * @since 3.2
     */
    fn linear_combination_f64_vec(&self,
                                  a: &Vec<f64>,
                                  b: &Vec<DerivativeStructure>)
                                  -> Result<DerivativeStructure, String> {
        Ok(self.linear_combination_f64_vec(a, b).unwrap())
    }

    /**
     * Compute a linear combination.
     * @param a1 first factor of the first term
     * @param b1 second factor of the first term
     * @param a2 first factor of the second term
     * @param b2 second factor of the second term
     * @return a<sub>1</sub>×b<sub>1</sub> +
     * a<sub>2</sub>×b<sub>2</sub>
     * @see #linearCombination(Object, Object, Object, Object, Object, Object)
     * @see #linearCombination(Object, Object, Object, Object, Object, Object, Object, Object)
     * @since 3.2
     */
    fn linear_combination_2_factors(&self,
                                    a1: &DerivativeStructure,
                                    b1: &DerivativeStructure,
                                    a2: &DerivativeStructure,
                                    b2: &DerivativeStructure)
                                    -> DerivativeStructure {
        self.linear_combination_2_factors(a1, b1, a2, b2).unwrap()
    }

    /**
     * Compute a linear combination.
     * @param a1 first factor of the first term
     * @param b1 second factor of the first term
     * @param a2 first factor of the second term
     * @param b2 second factor of the second term
     * @return a<sub>1</sub>×b<sub>1</sub> +
     * a<sub>2</sub>×b<sub>2</sub>
     * @see #linearCombination(double, Object, double, Object, double, Object)
     * @see #linearCombination(double, Object, double, Object, double, Object, double, Object)
     * @since 3.2
     */
    fn linear_combination_f64_2_factors(&self,
                                        a1: f64,
                                        b1: &DerivativeStructure,
                                        a2: f64,
                                        b2: &DerivativeStructure)
                                        -> DerivativeStructure {
        self.linear_combination_f64_2_factors(a1, b1, a2, b2).unwrap()
    }

    /**
     * Compute a linear combination.
     * @param a1 first factor of the first term
     * @param b1 second factor of the first term
     * @param a2 first factor of the second term
     * @param b2 second factor of the second term
     * @param a3 first factor of the third term
     * @param b3 second factor of the third term
     * @return a<sub>1</sub>×b<sub>1</sub> +
     * a<sub>2</sub>×b<sub>2</sub> + a<sub>3</sub>×b<sub>3</sub>
     * @see #linearCombination(Object, Object, Object, Object)
     * @see #linearCombination(Object, Object, Object, Object, Object, Object, Object, Object)
     * @since 3.2
     */
    fn linear_combination_3_factors(&self,
                                    a1: &DerivativeStructure,
                                    b1: &DerivativeStructure,
                                    a2: &DerivativeStructure,
                                    b2: &DerivativeStructure,
                                    a3: &DerivativeStructure,
                                    b3: &DerivativeStructure)
                                    -> DerivativeStructure {
        self.linear_combination_3_factors(a1, b1, a2, b2, a3, b3).unwrap()
    }

    /**
     * Compute a linear combination.
     * @param a1 first factor of the first term
     * @param b1 second factor of the first term
     * @param a2 first factor of the second term
     * @param b2 second factor of the second term
     * @param a3 first factor of the third term
     * @param b3 second factor of the third term
     * @return a<sub>1</sub>×b<sub>1</sub> +
     * a<sub>2</sub>×b<sub>2</sub> + a<sub>3</sub>×b<sub>3</sub>
     * @see #linearCombination(double, Object, double, Object)
     * @see #linearCombination(double, Object, double, Object, double, Object, double, Object)
     * @since 3.2
     */
    fn linear_combination_f64_3_factors(&self,
                                        a1: f64,
                                        b1: &DerivativeStructure,
                                        a2: f64,
                                        b2: &DerivativeStructure,
                                        a3: f64,
                                        b3: &DerivativeStructure)
                                        -> DerivativeStructure {
        self.linear_combination_f64_3_factors(a1, b1, a2, b2, a3, b3).unwrap()
    }

    /**
     * Compute a linear combination.
     * @param a1 first factor of the first term
     * @param b1 second factor of the first term
     * @param a2 first factor of the second term
     * @param b2 second factor of the second term
     * @param a3 first factor of the third term
     * @param b3 second factor of the third term
     * @param a4 first factor of the third term
     * @param b4 second factor of the third term
     * @return a<sub>1</sub>×b<sub>1</sub> +
     * a<sub>2</sub>×b<sub>2</sub> + a<sub>3</sub>×b<sub>3</sub> +
     * a<sub>4</sub>×b<sub>4</sub>
     * @see #linearCombination(Object, Object, Object, Object)
     * @see #linearCombination(Object, Object, Object, Object, Object, Object)
     * @since 3.2
     */
    fn linear_combination_4_factors(&self,
                                    a1: &DerivativeStructure,
                                    b1: &DerivativeStructure,
                                    a2: &DerivativeStructure,
                                    b2: &DerivativeStructure,
                                    a3: &DerivativeStructure,
                                    b3: &DerivativeStructure,
                                    a4: &DerivativeStructure,
                                    b4: &DerivativeStructure)
                                    -> DerivativeStructure {
        self.linear_combination_4_factors(a1, b1, a2, b2, a3, b3, a4, b4).unwrap()
    }

    /**
     * Compute a linear combination.
     * @param a1 first factor of the first term
     * @param b1 second factor of the first term
     * @param a2 first factor of the second term
     * @param b2 second factor of the second term
     * @param a3 first factor of the third term
     * @param b3 second factor of the third term
     * @param a4 first factor of the third term
     * @param b4 second factor of the third term
     * @return a<sub>1</sub>×b<sub>1</sub> +
     * a<sub>2</sub>×b<sub>2</sub> + a<sub>3</sub>×b<sub>3</sub> +
     * a<sub>4</sub>×b<sub>4</sub>
     * @see #linearCombination(double, Object, double, Object)
     * @see #linearCombination(double, Object, double, Object, double, Object)
     * @since 3.2
     */
    fn linear_combination_f64_4_factors(&self,
                                        a1: f64,
                                        b1: &DerivativeStructure,
                                        a2: f64,
                                        b2: &DerivativeStructure,
                                        a3: f64,
                                        b3: &DerivativeStructure,
                                        a4: f64,
                                        b4: &DerivativeStructure)
                                        -> DerivativeStructure {
        self.linear_combination_f64_4_factors(a1, b1, a2, b2, a3, b3, a4, b4).unwrap()
    }
}
