use std::rc::*;



/*
trait T1 {
    fn dosomething(&self) -> i32;
}


struct ST1 {

}

impl T1 for ST1 {
    fn dosomething(&self) -> i32 {
        return 10;
    }
}

struct ST1_2 {

}

impl T1 for ST1_2 {
    fn dosomething(&self) -> i32 {
        return 20;
    }
}

trait T2 : T1 {
    fn get_t1(&self) -> &T1;
}

impl T1 for ST2 {
    fn dosomething(&self) -> i32 {
        return 30;
    }	
}

impl T2 for ST2 {
    fn get_t1(&self) -> &Box<T1> {
        &self.t1
    }
}

struct ST2 {
    t1: Box<T1>
}

impl ST2 {
    fn new(the_t1: Box<T1>) -> ST2 {
        ST2 {
            t1: the_t1
        }
    }

}


impl ST1 {
    pub fn new() -> ST1 {
        ST1 {

        }
    }


}

fn derive(f: Box<T1>) -> Box<T2> {
    Box::new(ST2::new(f))
}

#[test]
pub fn test1() {
    let t1 = ST1{ };
    let t2 = derive(Box::new(t1));
    let t3 = derive(Box::new(ST1_2 { }));
    print!("before");
    panic!("ST1: {} ST1_2 {}",t2.get_t1().dosomething(), t3.get_t1().dosomething());
    print!("after");

}
*/
/*

impl ST1 {
    fn new() -> ST1 {
        ST1 {

        }
    }

    fn encapse(&self) -> Rc<T2> {
        Rc::new(ST2::new(Rc::new(*self)))
    }
}

fn derive(f: &T1) -> Rc<T2> {
    Rc::new(ST2::new(Box::new(*f)))
}

trait T2 {
    fn get_t1(&self) -> Rc<T1>;
}

struct ST2 {
    t1: Rc<T1>
}

impl ST2 {
    fn new(the_T1: Rc<T1>) -> ST2 {
        ST2 {
            t1: the_T1
        }
    }

}

impl T2 for ST2 {
    fn get_t1(&self) -> Rc<T1> {
        self.t1.clone()
    }
}

fn test() {
    let t1 = ST1::new();
    let st2 = ST2::new(Rc::new(t1));
    st2.get_t1().dosomething();

}
*/

/*
struct ST2<'a> {
    t1: Rc<T1 + 'a>
}

impl <'a> ST2<'a> {
    fn new(the_T1: Rc<T1 + 'a>) -> ST2 {
        ST2 {
            t1: the_T1
        }
    }

    fn get_t1(&'a self) -> Rc<T1 + 'a> {
        self.t1.clone()
    }
}

*/
