use util::fastmath::F64;
use util::fastmath;
use analysis::traits::*;
use analysis::differentiation::derivative_structure::*;
use std::rc::*;
use java::exc::*;
use analysis::function_utils::to_differentiable_univariate_function;


#[derive(Clone)]
pub struct Abs {
}

impl UnivariateFunction for Abs {
    fn value(&self, x: f64) -> f64 {
        return F64::abs(x);
    }
}



#[derive(Clone)]
pub struct Acos {
}

impl UnivariateFunction for Acos {
    fn value(&self, x: f64) -> f64 {
        return F64::acos(x);
    }
}

impl UnivariateDifferentiableFunction for Acos {
    fn value_ds(&self, t: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        return t.acos();
    }
}


#[derive(Clone)]
pub struct Acosh {
}

impl UnivariateFunction for Acosh {
    fn value(&self, x: f64) -> f64 {
        return F64::acosh(x);
    }
}

impl UnivariateDifferentiableFunction for Acosh {
    fn value_ds(&self, t: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        return t.acosh();
    }
}

impl DifferentiableUnivariateFunction for Acosh {
    fn derivative(&self) -> UnivariateFunctionContainer {
        return to_differentiable_univariate_function(Box::new(self.clone()));
    }
}

impl Acosh {}




#[derive(Clone)]
pub struct Add {
}

impl BivariateFunction for Add {
    fn value(&self, x: f64, y: f64) -> f64 {
        return x + y;
    }
}



#[derive(Clone)]
pub struct Asin {
}

impl UnivariateFunction for Asin {
    fn value(&self, x: f64) -> f64 {
        return F64::asin(x);
    }
}
impl DifferentiableUnivariateFunction for Asin {
    fn derivative(&self) -> UnivariateFunctionContainer {
        return to_differentiable_univariate_function(Box::new(self.clone()));
    }
}
impl UnivariateDifferentiableFunction for Asin {
    fn value_ds(&self, t: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        return t.asin();
    }
}



#[derive(Clone)]
pub struct Asinh {
}

impl UnivariateFunction for Asinh {
    fn value(&self, x: f64) -> f64 {
        return F64::asinh(x);
    }
}

impl DifferentiableUnivariateFunction for Asinh {
    fn derivative(&self) -> UnivariateFunctionContainer {
        return to_differentiable_univariate_function(Box::new(self.clone()));
    }
}

impl UnivariateDifferentiableFunction for Asinh {
    fn value_ds(&self, t: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        return t.asinh();
    }
}



#[derive(Clone)]
pub struct Atan {
}

impl UnivariateFunction for Atan {
    fn value(&self, x: f64) -> f64 {
        return F64::atan(x);
    }
}

impl DifferentiableUnivariateFunction for Atan {
    fn derivative(&self) -> UnivariateFunctionContainer {
        return to_differentiable_univariate_function(Box::new(self.clone()));
    }
}

impl UnivariateDifferentiableFunction for Atan {
    fn value_ds(&self, t: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        return t.atan();
    }
}



#[derive(Clone)]
pub struct Atan2 {
}

impl BivariateFunction for Atan2 {
    fn value(&self, x: f64, y: f64) -> f64 {
        return F64::atan2(x, y);
    }
}



#[derive(Clone)]
pub struct Atanh {
}

impl UnivariateFunction for Atanh {
    fn value(&self, x: f64) -> f64 {
        return F64::atanh(x);
    }
}

impl DifferentiableUnivariateFunction for Atanh {
    fn derivative(&self) -> UnivariateFunctionContainer {
        return to_differentiable_univariate_function(Box::new(self.clone()));
    }
}

impl UnivariateDifferentiableFunction for Atanh {
    fn value_ds(&self, t: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        return t.atanh();
    }
}



#[derive(Clone)]
pub struct Cbrt {
}

impl UnivariateFunction for Cbrt {
    fn value(&self, x: f64) -> f64 {
        return F64::cbrt(x);
    }
}
impl DifferentiableUnivariateFunction for Cbrt {
    fn derivative(&self) -> UnivariateFunctionContainer {
        return to_differentiable_univariate_function(Box::new(self.clone()));
    }
}

impl UnivariateDifferentiableFunction for Cbrt {
    fn value_ds(&self, t: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        return t.cbrt();
    }
}



#[derive(Clone)]
pub struct Ceil {
}

impl UnivariateFunction for Ceil {
    fn value(&self, x: f64) -> f64 {
        return F64::ceil(x);
    }
}



#[derive(Clone)]
pub struct Constant {
    c: f64,
}

impl UnivariateFunction for Constant {
    fn value(&self, x: f64) -> f64 {
        return self.c;
    }
}
impl DifferentiableUnivariateFunction for Constant {
    fn derivative(&self) -> UnivariateFunctionContainer {
        return UnivariateFunctionContainer::new(Box::new(|_| 0.0));
    }
}

impl Constant {
    pub fn new(c: f64) -> Constant {
        Constant { c: c }
    }
}

impl UnivariateDifferentiableFunction for Constant {
    fn value_ds(&self, t: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        return DerivativeStructure::new_parameters_order_value(t.get_free_parameters(),
                                                               t.get_order(),
                                                               self.c);
    }
}



#[derive(Clone)]
pub struct Cos {
}

impl UnivariateFunction for Cos {
    fn value(&self, x: f64) -> f64 {
        return F64::cos(x);
    }
}
impl DifferentiableUnivariateFunction for Cos {
    fn derivative(&self) -> UnivariateFunctionContainer {
        return to_differentiable_univariate_function(Box::new(self.clone()));
    }
}

impl UnivariateDifferentiableFunction for Cos {
    fn value_ds(&self, t: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        return t.cos();
    }
}



#[derive(Clone)]
pub struct Cosh {
}

impl UnivariateFunction for Cosh {
    fn value(&self, x: f64) -> f64 {
        return F64::cosh(x);
    }
}
impl DifferentiableUnivariateFunction for Cosh {
    fn derivative(&self) -> UnivariateFunctionContainer {
        let s = Sinh {};
        return UnivariateFunctionContainer::new(Box::new(move |x: f64| s.value(x)));
    }
}

impl UnivariateDifferentiableFunction for Cosh {
    fn value_ds(&self, t: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        return t.cosh();
    }
}



#[derive(Clone)]
pub struct Divide {
}

impl BivariateFunction for Divide {
    fn value(&self, x: f64, y: f64) -> f64 {
        return x / y;
    }
}



#[derive(Clone)]
pub struct Exp {
}

impl UnivariateFunction for Exp {
    fn value(&self, x: f64) -> f64 {
        return F64::exp(x);
    }
}
impl DifferentiableUnivariateFunction for Exp {
    fn derivative(&self) -> UnivariateFunctionContainer {
        return to_differentiable_univariate_function(Box::new(self.clone()));
    }
}

impl UnivariateDifferentiableFunction for Exp {
    fn value_ds(&self, t: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        return t.exp();
    }
}



#[derive(Clone)]
pub struct Expm1 {
}

impl UnivariateFunction for Expm1 {
    fn value(&self, x: f64) -> f64 {
        return F64::expm1(x);
    }
}
impl DifferentiableUnivariateFunction for Expm1 {
    fn derivative(&self) -> UnivariateFunctionContainer {
        return to_differentiable_univariate_function(Box::new(self.clone()));
    }
}

impl UnivariateDifferentiableFunction for Expm1 {
    fn value_ds(&self, t: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        return t.expm1();
    }
}



#[derive(Clone)]
pub struct Floor {
}

impl UnivariateFunction for Floor {
    fn value(&self, x: f64) -> f64 {
        return F64::floor(x);
    }
}


// pub struct Gaussian {
//
// let mean: f64;
//
// let is: f64;
//
// let i2s2: f64;
//
// let norm: f64;
// }
//
// impl UnivariateDifferentiableFunction for  Gaussian {
//
// pub fn new( norm: f64,  mean: f64,  sigma: f64) -> Gaussian throws NotStrictlyPositiveException {
// if sigma <= 0 {
// throw NotStrictlyPositiveException::new(&sigma);
// }
// let .norm = norm;
// let .mean = mean;
// let .is = 1 / sigma;
// }
//
// pub fn new( mean: f64,  sigma: f64) -> Gaussian throws NotStrictlyPositiveException {
// }
//
// pub fn new() -> Gaussian {
// this(0, 1);
// }
//
// fn  value(&self,  x: f64) -> f64  {
// return self.value(x - self.mean, &self.norm, &self.i2s2);
// }
//
// fn derivative(&self) -> UnivariateFunctionContainer {
// return to_differentiable_univariate_function(Box::new(self.clone()));
// }
//
// #[derive(ParametricUnivariateFunction)]
// pub struct Parametric {
// }
//
// impl Parametric {
//
// fn  value(&self,  x: f64,  param: f64) -> /*  throws NullArgumentException, DimensionMismatchException, NotStrictlyPositiveException */Result<f64>   {
// self.validate_parameters(¶m);
// let diff: f64 = x - param[1];
// return Gaussian.value(&diff, param[0], &self.i2s2);
// }
//
// fn  gradient(&self,  x: f64,  param: f64) -> /*  throws NullArgumentException, DimensionMismatchException, NotStrictlyPositiveException */Result<Vec<f64>>   {
// self.validate_parameters(¶m);
// let norm: f64 = param[0];
// let diff: f64 = x - param[1];
// let sigma: f64 = param[2];
// let n: f64 = Gaussian.value(&diff, 1, &self.i2s2);
// return  : [f64; 3] = [n, m, s, ]
// ;
// }
//
// fn  validate_parameters(&self,  param: &Vec<f64>)   {
// if param == null {
// throw NullArgumentException::new();
// }
// if param.len() != 3 {
// throw DimensionMismatchException::new(param.len(), 3);
// }
// if param[2] <= 0 {
// throw NotStrictlyPositiveException::new(param[2]);
// }
// }
// }
//
//
// fn  value( x_minus_mean: f64,  norm: f64,  i2s2: f64) -> f64  {
// }
//
// fn  value(&self,  t: &DerivativeStructure) -> /*  throws DimensionMismatchException */Result<DerivativeStructure>   {
// let mut f: [f64; t.get_order() + 1] = [0.0; t.get_order() + 1];
// the nth order derivative of the Gaussian has the form:
// dn(g(x)/dxn = (norm / s^n) P_n(u) exp(-u^2/2) with u=(x-m)/s
// where P_n(u) is a degree n polynomial with same parity as n
// P_0(u) = 1, P_1(u) = -u, P_2(u) = u^2 - 1, P_3(u) = -u^3 + 3 u...
// the general recurrence relation for P_n is:
// P_n(u) = P_(n-1)'(u) - u P_(n-1)(u)
// as per polynomial parity, we can store coefficients of both P_(n-1) and P_n in the same array
// let mut p: [f64; f.len()] = [0.0; f.len()];
// p[0] = 1;
// if coeff <= Precision::SAFE_MIN {
// Arrays.fill(&f, 0.0);
// } else {
// f[0] = coeff;
// {
// let mut n: i32 = 1;
// while n < f.len() {
// {
// update and evaluate polynomial P_n(x)
// let v: f64 = 0;
// p[n] = -p[n - 1];
// {
// let mut k: i32 = n;
// while k >= 0 {
// {
// if k > 2 {
// } else if k == 2 {
// p[0] = p[1];
// }
// }
// k -= 2;
// }
// }
//
// if (n & 0x1) == 1 {
// }
// }
// n += 1;
// }
// }
//
// }
// return t.compose(&f);
// }
// }
//
//
//
// #[derive(Clone)]
// pub struct HarmonicOscillator {
//
// let amplitude: f64;
//
// let omega: f64;
//
// let phase: f64;
// }
//
// impl UnivariateDifferentiableFunction for HarmonicOscillator {
//
// pub fn new( amplitude: f64,  omega: f64,  phase: f64) -> HarmonicOscillator {
// let .amplitude = amplitude;
// let .omega = omega;
// let .phase = phase;
// }
//
// fn  value(&self,  x: f64) -> f64  {
// }
//
// fn derivative(&self) -> UnivariateFunctionContainer {
// return to_differentiable_univariate_function(Box::new(self.clone()));
// }
//
// #[derive(ParametricUnivariateFunction)]
// pub struct Parametric {
// }
//
// impl Parametric {
//
// fn  value(&self,  x: f64,  param: f64) -> /*  throws NullArgumentException, DimensionMismatchException */Result<f64>   {
// self.validate_parameters(¶m);
// }
//
// fn  gradient(&self,  x: f64,  param: f64) -> /*  throws NullArgumentException, DimensionMismatchException */Result<Vec<f64>>   {
// self.validate_parameters(¶m);
// let amplitude: f64 = param[0];
// let omega: f64 = param[1];
// let phase: f64 = param[2];
// let a: f64 = HarmonicOscillator.value(&x_times_omega_plus_phase, 1);
// return  : [f64; 3] = [a, w, p, ]
// ;
// }
//
// fn  validate_parameters(&self,  param: &Vec<f64>)   {
// if param == null {
// throw NullArgumentException::new();
// }
// if param.len() != 3 {
// throw DimensionMismatchException::new(param.len(), 3);
// }
// }
// }
//
//
// fn  value( x_times_omega_plus_phase: f64,  amplitude: f64) -> f64  {
// }
//
// fn  value(&self,  t: &DerivativeStructure) -> /*  throws DimensionMismatchException */Result<DerivativeStructure>   {
// let x: f64 = t.get_value();
// let f: [f64; t.get_order() + 1] = [0.0; t.get_order() + 1];
// if f.len() > 1 {
// {
// let mut i: i32 = 2;
// while i < f.len() {
// {
// }
// i += 1;
// }
// }
//
// }
// return t.compose(&f);
// }
// }
//
//

#[derive(Clone,Copy)]
pub struct Identity {
}

impl UnivariateFunction for Identity {
    fn value(&self, x: f64) -> f64 {
        return x;
    }
}
impl DifferentiableUnivariateFunction for Identity {
    fn derivative(&self) -> UnivariateFunctionContainer {
        return UnivariateFunctionContainer::new(Box::new(move |_| 1.0));
    }
}

impl UnivariateDifferentiableFunction for Identity {
    fn value_ds(&self, t: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        return Ok(t.clone());
    }
}



#[derive(Clone)]
pub struct Inverse {
}

impl UnivariateFunction for Inverse {
    fn value(&self, x: f64) -> f64 {
        return 1.0 / x;
    }
}
impl DifferentiableUnivariateFunction for Inverse {
    fn derivative(&self) -> UnivariateFunctionContainer {
        return to_differentiable_univariate_function(Box::new(self.clone()));
    }
}

impl UnivariateDifferentiableFunction for Inverse {
    fn value_ds(&self, t: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        return t.reciprocal();
    }
}



#[derive(Clone)]
pub struct Log {
}

impl UnivariateFunction for Log {
    fn value(&self, x: f64) -> f64 {
        return F64::log(x);
    }
}
impl DifferentiableUnivariateFunction for Log {
    fn derivative(&self) -> UnivariateFunctionContainer {
        return to_differentiable_univariate_function(Box::new(self.clone()));
    }
}

impl UnivariateDifferentiableFunction for Log {
    fn value_ds(&self, t: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        return t.log();
    }
}



#[derive(Clone)]
pub struct Log10 {
}

impl UnivariateFunction for Log10 {
    fn value(&self, x: f64) -> f64 {
        return F64::log10(x);
    }
}
impl DifferentiableUnivariateFunction for Log10 {
    fn derivative(&self) -> UnivariateFunctionContainer {
        return to_differentiable_univariate_function(Box::new(self.clone()));
    }
}

impl UnivariateDifferentiableFunction for Log10 {
    fn value_ds(&self, t: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        return t.log10();
    }
}



#[derive(Clone)]
pub struct Log1p {
}

impl UnivariateFunction for Log1p {
    fn value(&self, x: f64) -> f64 {
        return F64::log1p(x);
    }
}
impl DifferentiableUnivariateFunction for Log1p {
    fn derivative(&self) -> UnivariateFunctionContainer {
        return to_differentiable_univariate_function(Box::new(self.clone()));
    }
}

impl UnivariateDifferentiableFunction for Log1p {
    fn value_ds(&self, t: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        return t.log1p();
    }
}



#[derive(Clone)]
pub struct Logistic {
    a: f64,

    k: f64,

    b: f64,

    one_over_n: f64,

    q: f64,

    m: f64,
}

// impl UnivariateFunction for Logistic {
//
// pub fn new( k: f64,  m: f64,  b: f64,  q: f64,  a: f64,  n: f64) -> Logistic throws NotStrictlyPositiveException {
// if n <= 0 {
// throw NotStrictlyPositiveException::new(&n);
// }
// let .k = k;
// let .m = m;
// let .b = b;
// let .q = q;
// let .a = a;
// one_over_n = 1 / n;
// }
//
// fn  value(&self,  x: f64) -> f64  {
// return self.value(self.m - x, &self.k, &self.b, &self.q, &self.a, &self.one_over_n);
// }
//
// fn derivative(&self) -> UnivariateFunctionContainer {
// return to_differentiable_univariate_function(Box::new(self.clone()));
// }
//
// #[derive(ParametricUnivariateFunction)]
// pub struct Parametric {
// }
//
// impl Parametric {
//
// fn  value(&self,  x: f64,  param: f64) -> /*  throws NullArgumentException, DimensionMismatchException, NotStrictlyPositiveException */Result<f64>   {
// self.validate_parameters(¶m);
// return Logistic.value(param[1] - x, param[0], param[2], param[3], param[4], 1 / param[5]);
// }
//
// fn  gradient(&self,  x: f64,  param: f64) -> /*  throws NullArgumentException, DimensionMismatchException, NotStrictlyPositiveException */Result<Vec<f64>>   {
// self.validate_parameters(¶m);
// let b: f64 = param[2];
// let q: f64 = param[3];
// let m_minus_x: f64 = param[1] - x;
// let one_over_n: f64 = 1 / param[5];
// let q_exp1: f64 = q_exp + 1;
// let factor2: f64 = -factor1 / q_exp1;
// Components of the gradient.
// let gk: f64 = Logistic.value(&m_minus_x, 1, &b, &q, 0, &one_over_n);
// let ga: f64 = Logistic.value(&m_minus_x, 0, &b, &q, 1, &one_over_n);
// return  : [f64; 6] = [gk, gm, gb, gq, ga, gn, ]
// ;
// }
//
// fn  validate_parameters(&self,  param: &Vec<f64>)   {
// if param == null {
// throw NullArgumentException::new();
// }
// if param.len() != 6 {
// throw DimensionMismatchException::new(param.len(), 6);
// }
// if param[5] <= 0 {
// throw NotStrictlyPositiveException::new(param[5]);
// }
// }
// }
//
//
// fn  value( m_minus_x: f64,  k: f64,  b: f64,  q: f64,  a: f64,  one_over_n: f64) -> f64  {
// }
//
// fn  value(&self,  t: &DerivativeStructure) -> Result<DerivativeStructure,Rc<Exception>> {
// return t.negate().add(&self.m).multiply(&self.b).exp().multiply(&self.q).add(1).pow(&self.one_over_n).reciprocal().multiply(self.k - self.a).add(&self.a);
// }
// }
//
//
//
//
// #[derive(Clone)]
// pub struct Logit {
//
// lo: f64;
//
// hi: f64;
// }
//
// impl UnivariateDifferentiableFunction for  Logit {
//
// pub fn new() -> Logit {
// this(0, 1);
// }
//
// pub fn new( lo: f64,  hi: f64) -> Logit {
// let .lo = lo;
// let .hi = hi;
// }
//
// fn  value(&self,  x: f64) -> /*  throws OutOfRangeException */Result<f64>   {
// return self.value(&x, &self.lo, &self.hi);
// }
//
// fn derivative(&self) -> UnivariateFunctionContainer {
// return to_differentiable_univariate_function(Box::new(self.clone()));
// }
//
// #[derive(ParametricUnivariateFunction)]
// pub struct Parametric {
// }
//
// impl Parametric {
//
// fn  value(&self,  x: f64,  param: f64) -> /*  throws NullArgumentException, DimensionMismatchException */Result<f64>   {
// self.validate_parameters(¶m);
// return Logit.value(&x, param[0], param[1]);
// }
//
// fn  gradient(&self,  x: f64,  param: f64) -> /*  throws NullArgumentException, DimensionMismatchException */Result<Vec<f64>>   {
// self.validate_parameters(¶m);
// let lo: f64 = param[0];
// let hi: f64 = param[1];
// return  : [f64; 2] = [1 / (lo - x), 1 / (hi - x), ]
// ;
// }
//
// fn  validate_parameters(&self,  param: &Vec<f64>)   {
// if param == null {
// throw NullArgumentException::new();
// }
// if param.len() != 2 {
// throw DimensionMismatchException::new(param.len(), 2);
// }
// }
// }
//
//
// fn  value( x: f64,  lo: f64,  hi: f64) -> /*  throws OutOfRangeException */Result<f64>   {
// if x < lo || x > hi {
// throw OutOfRangeException::new(&x, &lo, &hi);
// }
// return F64::log((x - lo) / (hi - x));
// }
//
// fn  value(&self,  t: &DerivativeStructure) -> /*  throws OutOfRangeException */Result<DerivativeStructure>   {
// let x: f64 = t.get_value();
// if x < self.lo || x > self.hi {
// throw OutOfRangeException::new(&x, &self.lo, &self.hi);
// }
// let mut f: [f64; t.get_order() + 1] = [0.0; t.get_order() + 1];
// function value
// f[0] = F64::log((x - self.lo) / (self.hi - x));
// if Double.is_infinite(f[0]) {
// if f.len() > 1 {
// f[1] = Double::POSITIVE_INFINITY;
// }
// of the method will transform most infinities into NaN ...
// {
// let mut i: i32 = 2;
// while i < f.len() {
// {
// f[i] = f[i - 2];
// }
// i += 1;
// }
// }
//
// } else {
// function derivatives
// let inv_l: f64 = 1.0 / (x - self.lo);
// let x_l: f64 = inv_l;
// let inv_h: f64 = 1.0 / (self.hi - x);
// let x_h: f64 = inv_h;
// {
// let mut i: i32 = 1;
// while i < f.len() {
// {
// f[i] = x_l + x_h;
// }
// i += 1;
// }
// }
//
// }
// return t.compose(&f);
// }
// }
//
//

#[derive(Clone)]
pub struct Max {
}

impl BivariateFunction for Max {
    fn value(&self, x: f64, y: f64) -> f64 {
        return F64::max(x, y);
    }
}



#[derive(Clone)]
pub struct Min {
}

impl BivariateFunction for Min {
    fn value(&self, x: f64, y: f64) -> f64 {
        return F64::min(x, y);
    }
}



#[derive(Clone)]
pub struct Minus {
}

impl UnivariateFunction for Minus {
    fn value(&self, x: f64) -> f64 {
        return -x;
    }
}
impl DifferentiableUnivariateFunction for Minus {
    fn derivative(&self) -> UnivariateFunctionContainer {
        return UnivariateFunctionContainer::new(Box::new(move |_| -1.0));
    }
}

impl UnivariateDifferentiableFunction for Minus {
    fn value_ds(&self, t: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        return t.negate();
    }
}



#[derive(Clone)]
pub struct Multiply {
}

impl BivariateFunction for Multiply {
    fn value(&self, x: f64, y: f64) -> f64 {
        return x * y;
    }
}



#[derive(Clone)]
pub struct Pow {
}

impl BivariateFunction for Pow {
    fn value(&self, x: f64, y: f64) -> f64 {
        return F64::pow(x, y);
    }
}



#[derive(Clone)]
pub struct Power {
    p: f64,
}

impl UnivariateFunction for Power {
    fn value(&self, x: f64) -> f64 {
        return F64::pow(x, self.p);
    }
}
impl DifferentiableUnivariateFunction for Power {
    fn derivative(&self) -> UnivariateFunctionContainer {
        return to_differentiable_univariate_function(Box::new(self.clone()));
    }
}

impl Power {
    pub fn new(p: f64) -> Power {
        Power { p: p }

    }
}

impl UnivariateDifferentiableFunction for Power {
    fn value_ds(&self, t: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        return t.pow_f64(self.p);
    }
}



#[derive(Clone)]
pub struct Rint {
}

impl UnivariateFunction for Rint {
    fn value(&self, x: f64) -> f64 {
        return F64::rint(x);
    }
}


// pub struct Sigmoid {
//
// let lo: f64;
//
// let hi: f64;
// }
//
// impl UnivariateDifferentiableFunction for Sigmoid {
//
// pub fn new() -> Sigmoid {
// this(0, 1);
// }
//
// pub fn new( lo: f64,  hi: f64) -> Sigmoid {
// let .lo = lo;
// let .hi = hi;
// }
//
// fn derivative(&self) -> UnivariateFunctionContainer {
// return to_differentiable_univariate_function(Box::new(self.clone()));
// }
//
// fn  value(&self,  x: f64) -> f64  {
// return self.value(&x, &self.lo, &self.hi);
// }
//
// #[derive(ParametricUnivariateFunction)]
// pub struct Parametric {
// }
//
// impl Parametric {
//
// fn  value(&self,  x: f64,  param: f64) -> /*  throws NullArgumentException, DimensionMismatchException */Result<f64>   {
// self.validate_parameters(¶m);
// return Sigmoid.value(&x, param[0], param[1]);
// }
//
// fn  gradient(&self,  x: f64,  param: f64) -> /*  throws NullArgumentException, DimensionMismatchException */Result<Vec<f64>>   {
// self.validate_parameters(¶m);
// let inv_exp1: f64 = 1 / (1 + F64::exp(-x));
// return  : [f64; 2] = [1 - inv_exp1, inv_exp1, ]
// ;
// }
//
// fn  validate_parameters(&self,  param: &Vec<f64>)   {
// if param == null {
// throw NullArgumentException::new();
// }
// if param.len() != 2 {
// throw DimensionMismatchException::new(param.len(), 2);
// }
// }
// }
//
//
// fn  value( x: f64,  lo: f64,  hi: f64) -> f64  {
// return lo + (hi - lo) / (1 + F64::exp(-x));
// }
//
// fn  value(&self,  t: &DerivativeStructure) -> /*  throws DimensionMismatchException */Result<DerivativeStructure>   {
// let mut f: [f64; t.get_order() + 1] = [0.0; t.get_order() + 1];
// let exp: f64 = F64::exp(-t.get_value());
// if Double.is_infinite(&exp) {
// special handling near lower boundary, to avoid NaN
// f[0] = self.lo;
// Arrays.fill(&f, 1, f.len(), 0.0);
// } else {
// the nth order derivative of sigmoid has the form:
// dn(sigmoid(x)/dxn = P_n(exp(-x)) / (1+exp(-x))^(n+1)
// where P_n(t) is a degree n polynomial with normalized higher term
// P_0(t) = 1, P_1(t) = t, P_2(t) = t^2 - t, P_3(t) = t^3 - 4 t^2 + t...
// the general recurrence relation for P_n is:
// P_n(x) = n t P_(n-1)(t) - t (1 + t) P_(n-1)'(t)
// let mut p: [f64; f.len()] = [0.0; f.len()];
// let inv: f64 = 1 / (1 + exp);
// let coeff: f64 = self.hi - self.lo;
// {
// let mut n: i32 = 0;
// while n < f.len() {
// {
// update and evaluate polynomial P_n(t)
// let v: f64 = 0;
// p[n] = 1;
// {
// let mut k: i32 = n;
// while k >= 0 {
// {
// if k > 1 {
// } else {
// p[0] = 0;
// }
// }
// k -= 1;
// }
// }
//
// }
// n += 1;
// }
// }
//
// fix function value
// f[0] += self.lo;
// }
// return t.compose(&f);
// }
// }
//
//

#[derive(Clone)]
pub struct Signum {
}

impl UnivariateFunction for Signum {
    fn value(&self, x: f64) -> f64 {
        return F64::signum(x);
    }
}



#[derive(Clone)]
pub struct Sin {
}

impl UnivariateFunction for Sin {
    fn value(&self, x: f64) -> f64 {
        return F64::sin(x);
    }
}
impl DifferentiableUnivariateFunction for Sin {
    fn derivative(&self) -> UnivariateFunctionContainer {
        return UnivariateFunctionContainer::new(Box::new(move |x: f64| Cos {}.value(x)));
    }
}

impl UnivariateDifferentiableFunction for Sin {
    fn value_ds(&self, t: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        return t.sin();
    }
}



const SHORTCUT: f64 = 6.0e-3;

#[derive(Clone)]
pub struct Sinc {
    normalized: bool,
}

impl Sinc {
    pub fn new_non_normalized() -> Sinc {
        Sinc { normalized: false }
    }

    pub fn new(normalized: bool) -> Sinc {
        Sinc { normalized: normalized }
    }
}


impl UnivariateFunction for Sinc {
    fn value(&self, x: f64) -> f64 {
        let scaled_x = if self.normalized { fastmath::PI * x } else { x };
        if F64::abs(scaled_x) <= SHORTCUT {
            // use Taylor series
            let scaled_x2 = scaled_x * scaled_x;
            ((scaled_x2 - 20.0) * scaled_x2 + 120.0) / 120.0
        } else {
            // use definition expression
            return F64::sin(scaled_x) / scaled_x;
        }
    }
}

impl UnivariateDifferentiableFunction for Sinc {
    fn value_ds(&self, t: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        let scaled_x: f64 = (if self.normalized { fastmath::PI } else { 1.0 }) * t.get_value();
        let scaled_x2: f64 = scaled_x * scaled_x;
        let mut f = vec![0.0; t.get_order() + 1];
        if F64::abs(scaled_x) <= SHORTCUT {
            {
                let mut i: usize = 0;
                while i < f.len() {
                    {
                        let k: i32 = i as i32 / 2;
                        if (i & 0x1) == 0 {
                            // even derivation order
                            f[i] = (if (k & 0x1) == 0 { 1.0 } else { -1.0 }) *
                                   (1.0 / (i as f64 + 1.0) -
                                    scaled_x2 *
                                    (1.0 / (2.0 * i as f64 + 6.0) -
                                     scaled_x2 / (24.0 * i as f64 + 120.0)));
                        } else {
                            // odd derivation order
                            f[i] = (if (k & 0x1) == 0 { -scaled_x } else { scaled_x }) *
                                   (1.0 / (i as f64 + 2.0) -
                                    scaled_x2 *
                                    (1.0 / (6.0 * i as f64 + 24.0) -
                                     scaled_x2 / (120.0 * i as f64 + 720.0)));
                        }
                    }
                    i += 1;
                }
            }

        } else {
            let inv: f64 = 1.0 / scaled_x;
            let cos: f64 = F64::cos(scaled_x);
            let sin: f64 = F64::sin(scaled_x);
            f[0] = inv * sin;
            // the nth order derivative of sinc has the form:
            // dn(sinc(x)/dxn = [S_n(x) sin(x) + C_n(x) cos(x)] / x^(n+1)
            // where S_n(x) is an even polynomial with degree n-1 or n (depending on parity)
            // and C_n(x) is an odd polynomial with degree n-1 or n (depending on parity)
            // S_0(x) = 1, S_1(x) = -1, S_2(x) = -x^2 + 2, S_3(x) = 3x^2 - 6...
            // C_0(x) = 0, C_1(x) = x, C_2(x) = -2x, C_3(x) = -x^3 + 6x...
            // the general recurrence relations for S_n and C_n are:
            // S_n(x) = x S_(n-1)'(x) - n S_(n-1)(x) - x C_(n-1)(x)
            // C_n(x) = x C_(n-1)'(x) - n C_(n-1)(x) + x S_(n-1)(x)
            // as per polynomials parity, we can store both S_n and C_n in the same array
            let mut sc = vec![0.0; f.len()];
            sc[0] = 1.0;
            let mut coeff: f64 = inv;
            {
                let mut n: usize = 1;
                while n < f.len() {
                    {
                        let mut s: f64 = 0.0;
                        let mut c: f64 = 0.0;
                        // update and evaluate polynomials S_n(x) and C_n(x)
                        let k_start: usize;
                        if (n & 0x1) == 0 {
                            // even derivation order, S_n is degree n and C_n is degree n-1
                            sc[n] = 0.0;
                            k_start = n;
                        } else {
                            // odd derivation order, S_n is degree n-1 and C_n is degree n
                            sc[n] = sc[n - 1];
                            c = sc[n];
                            k_start = n - 1;
                        }
                        // in this loop, k is always even
                        {
                            let mut k: usize = k_start;
                            while k > 1 {
                                {
                                    // sine part
                                    sc[k] = (k - n) as f64 * sc[k] - sc[k - 1];
                                    s = s * scaled_x2 + sc[k];
                                    // cosine part
                                    sc[k - 1] = (k - 1 - n) as f64 * sc[k - 1] + sc[k - 2];
                                    c = c * scaled_x2 + sc[k - 1];
                                }
                                if k <= 3 {
                                    break;
                                }
                                k -= 2;
                            }
                        }

                        sc[0] *= -(n as f64) as f64;
                        s = s * scaled_x2 + sc[0];
                        coeff *= inv;
                        f[n] = coeff * (s * sin + c * scaled_x * cos);
                    }
                    n += 1;
                }
            }

        }
        if self.normalized {
            let mut scale: f64 = fastmath::PI;
            {
                let mut i: usize = 1;
                while i < f.len() {
                    {
                        f[i] *= scale;
                        scale *= fastmath::PI;
                    }
                    i += 1;
                }
            }

        }
        return t.compose(&f);
    }
}


#[derive(Clone)]
pub struct Sinh {
}

impl UnivariateFunction for Sinh {
    fn value(&self, x: f64) -> f64 {
        return F64::sinh(x);
    }
}
impl DifferentiableUnivariateFunction for Sinh {
    fn derivative(&self) -> UnivariateFunctionContainer {
        return UnivariateFunctionContainer::new(Box::new(move |x: f64| Cosh {}.value(x)));
    }
}

impl UnivariateDifferentiableFunction for Sinh {
    fn value_ds(&self, t: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        return t.sinh();
    }
}



#[derive(Clone)]
pub struct Sqrt {
}

impl UnivariateFunction for Sqrt {
    fn value(&self, x: f64) -> f64 {
        return F64::sqrt(x);
    }
}
impl DifferentiableUnivariateFunction for Sqrt {
    fn derivative(&self) -> UnivariateFunctionContainer {
        return to_differentiable_univariate_function(Box::new(self.clone()));
    }
}

impl UnivariateDifferentiableFunction for Sqrt {
    fn value_ds(&self, t: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        return t.sqrt();
    }
}



#[derive(Clone)]
pub struct StepFunction {
    abscissa: Vec<f64>,

    ordinate: Vec<f64>,
}

// impl UnivariateFunction for StepFunction {
//
// pub fn new( x: &Vec<f64>,  y: &Vec<f64>) -> StepFunction  {
// if x.len() == 0 || y.len() == 0 {
// throw NoDataException::new();
// }
// if y.len() != x.len() {
// throw DimensionMismatchException::new(y.len(), x.len());
// }
// MathArrays.check_order(&x);
// abscissa = MathArrays.copy_of(&x);
// ordinate = MathArrays.copy_of(&y);
// }
//
// fn  value(&self,  x: f64) -> f64  {
// let index: i32 = Arrays.binary_search(&self.abscissa, &x);
// let mut fx: f64 = 0;
// if index < -1 {
// "x" is between "abscissa[-index-2]" and "abscissa[-index-1]".
// fx = self.ordinate[-index - 2];
// } else if index >= 0 {
// "x" is exactly "abscissa[index]".
// fx = self.ordinate[index];
// } else {
// Otherwise, "x" is smaller than the first value in "abscissa"
// (hence the returned value should be "ordinate[0]").
// fx = self.ordinate[0];
// }
// return fx;
// }
// }
//


#[derive(Clone)]
pub struct Subtract {
}

impl BivariateFunction for Subtract {
    fn value(&self, x: f64, y: f64) -> f64 {
        return x - y;
    }
}



#[derive(Clone)]
pub struct Tan {
}

impl UnivariateFunction for Tan {
    fn value(&self, x: f64) -> f64 {
        return F64::tan(x);
    }
}
impl DifferentiableUnivariateFunction for Tan {
    fn derivative(&self) -> UnivariateFunctionContainer {
        return to_differentiable_univariate_function(Box::new(self.clone()));
    }
}

impl UnivariateDifferentiableFunction for Tan {
    fn value_ds(&self, t: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        return t.tan();
    }
}



#[derive(Clone)]
pub struct Tanh {
}

impl UnivariateFunction for Tanh {
    fn value(&self, x: f64) -> f64 {
        return F64::tanh(x);
    }
}
impl DifferentiableUnivariateFunction for Tanh {
    fn derivative(&self) -> UnivariateFunctionContainer {
        return to_differentiable_univariate_function(Box::new(self.clone()));
    }
}

impl UnivariateDifferentiableFunction for Tanh {
    fn value_ds(&self, t: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        return t.tanh();
    }
}



#[derive(Clone)]
pub struct Ulp {
}

impl UnivariateFunction for Ulp {
    fn value(&self, x: f64) -> f64 {
        return F64::ulp(x);
    }
}
