pub mod traits;

// pub mod traittest;
pub mod function_utils;
pub mod functions;
// pub mod function_utils_test;
pub mod differentiation;
pub mod polynomials;
pub mod interpolation;
