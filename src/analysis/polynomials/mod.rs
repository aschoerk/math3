pub mod polynomial_function;
pub mod polynomial_function_test;
pub mod polynomial_function_newton_form;
pub mod polynomial_function_newton_form_test;
pub mod polynomial_function_lagrange_form;
pub mod polynomial_function_lagrange_form_test;
pub mod polynomial_spline_function;
pub mod polynomial_spline_function_test;
