// Licensed to the Apache Software Foundation (ASF) under one or more
// contributor license agreements.  See the NOTICE file distributed with
// this work for additional information regarding copyright ownership.
// The ASF licenses this file to You under the Apache License, Version 2.0
// (the "License"); you may not use this file except in compliance with
// the License.  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#![allow(unused_imports)]
use analysis::polynomials::polynomial_function_lagrange_form::*;
use assert;
use analysis::traits::*;

/**
 * Test case for Lagrange form of polynomial function.
 * <p>
 * We use n+1 points to interpolate a polynomial of degree n. This should
 * give us the exact same polynomial as result. Thus we can use a very
 * small tolerance to account only for round-off errors.
 *
 */
/**
 * Test of polynomial for the linear function.
 */
#[test]
pub fn test_linear_function() {
    let mut p: PolynomialFunctionLagrangeForm;
    let mut z: f64;
    let mut expected: f64;
    let mut result: f64;
    let tolerance: f64 = 1E-12;
    // p(x) = 1.5x - 4
    let x = vec![0.0, 3.0, ];
    let y = vec![-4.0, 0.5, ];
    p = PolynomialFunctionLagrangeForm::new(&x, &y).unwrap();
    z = 2.0;
    expected = -1.0;
    result = p.value(z);
    assert_within_delta!(expected, result, tolerance);
    z = 4.5;
    expected = 2.75;
    result = p.value(z);
    assert_within_delta!(expected, result, tolerance);
    z = 6.0;
    expected = 5.0;
    result = p.value(z);
    assert_within_delta!(expected, result, tolerance);
    assert_eq!(1, p.degree());
    let c = p.get_coefficients();
    assert_eq!(2, c.len());
    assert_within_delta!(-4.0, c[0], tolerance);
    assert_within_delta!(1.5, c[1], tolerance);
}


/**
 * Test of polynomial for the quadratic function.
 */
#[test]
pub fn test_quadratic_function() {
    let mut p: PolynomialFunctionLagrangeForm;
    let mut z: f64;
    let mut expected: f64;
    let mut result: f64;
    let tolerance: f64 = 1E-12;
    // p(x) = 2x^2 + 5x - 3 = (2x - 1)(x + 3)
    let x = vec![0.0, -1.0, 0.5, ];
    let y = vec![-3.0, -6.0, 0.0, ];
    p = PolynomialFunctionLagrangeForm::new(&x, &y).unwrap();
    z = 1.0;
    expected = 4.0;
    result = p.value(z);
    assert_within_delta!(expected, result, tolerance);
    z = 2.5;
    expected = 22.0;
    result = p.value(z);
    assert_within_delta!(expected, result, tolerance);
    z = -2.0;
    expected = -5.0;
    result = p.value(z);
    assert_within_delta!(expected, result, tolerance);
    assert_eq!(2, p.degree());
    let c = p.get_coefficients();
    assert_eq!(3, c.len());
    assert_within_delta!(-3.0, c[0], tolerance);
    assert_within_delta!(5.0, c[1], tolerance);
    assert_within_delta!(2.0, c[2], tolerance);
}

/**
 * Test of polynomial for the quintic function.
 */
#[test]
pub fn test_quintic_function() {
    let mut p: PolynomialFunctionLagrangeForm;
    let mut z: f64;
    let mut expected: f64;
    let mut result: f64;
    let tolerance: f64 = 1E-12;
    // p(x) = x^5 - x^4 - 7x^3 + x^2 + 6x = x(x^2 - 1)(x + 2)(x - 3)
    let x = vec![1.0, -1.0, 2.0, 3.0, -3.0, 0.5, ];
    let y = vec![0.0, 0.0, -24.0, 0.0, -144.0, 2.34375, ];
    p = PolynomialFunctionLagrangeForm::new(&x, &y).unwrap();
    z = 0.0;
    expected = 0.0;
    result = p.value(z);
    assert_within_delta!(expected, result, tolerance);
    z = -2.0;
    expected = 0.0;
    result = p.value(z);
    assert_within_delta!(expected, result, tolerance);
    z = 4.0;
    expected = 360.0;
    result = p.value(z);
    assert_within_delta!(expected, result, tolerance);
    assert_eq!(5, p.degree());
    let c = p.get_coefficients();
    assert_eq!(6, c.len());
    assert_within_delta!(0.0, c[0], tolerance);
    assert_within_delta!(6.0, c[1], tolerance);
    assert_within_delta!(1.0, c[2], tolerance);
    assert_within_delta!(-7.0, c[3], tolerance);
    assert_within_delta!(-1.0, c[4], tolerance);
    assert_within_delta!(1.0, c[5], tolerance);
}

/**
 * Test of parameters for the polynomial.
 */
#[test]
pub fn test_parameters() {
    // bad input array length
    let x = vec![1.0, ];
    let y = vec![2.0, ];
    assert!(PolynomialFunctionLagrangeForm::new(&x, &y).is_err());
    // mismatch input arrays
    let x = vec![1.0, 2.0, 3.0, 4.0, ];
    let y = vec![0.0, -4.0, -24.0, ];
    assert!(PolynomialFunctionLagrangeForm::new(&x, &y).is_err());
}
