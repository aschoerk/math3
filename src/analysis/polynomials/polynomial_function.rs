
// Licensed to the Apache Software Foundation (ASF) under one or more
// contributor license agreements.  See the NOTICE file distributed with
// this work for additional information regarding copyright ownership.
// The ASF licenses this file to You under the Apache License, Version 2.0
// (the "License"); you may not use this file except in compliance with
// the License.  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
use std::rc::*;
use java::exc::*;
use analysis::differentiation::derivative_structure::*;
use util::fastmath::I32;
use util::fastmath::F64;
use analysis::traits::*;


/**
 * Immutable representation of a real polynomial function with real coefficients.
 * <p>
 * <a href="http://mathworld.wolfram.com/HornersMethod.html">Horner's Method</a>
 * is used to evaluate the function.</p>
 *
 */
// #[derive(UnivariateDifferentiableFunction, DifferentiableUnivariateFunction, Serializable)]
#[derive(Clone)]
pub struct PolynomialFunction {
    /**
     * The coefficients of the polynomial, ordered by degree -- i.e.,
     * coefficients[0] is the constant term and coefficients[n] is the
     * coefficient of x^n where n is the degree of the polynomial.
     */
    coefficients: Vec<f64>,
}

impl UnivariateFunction for PolynomialFunction {
    /**
     * Compute the value of the function for the given argument.
     * <p>
     *  The value returned is </p><p>
     *  {@code coefficients[n] * x^n + ... + coefficients[1] * x  + coefficients[0]}
     * </p>
     *
     * @param x Argument for which the function value should be computed.
     * @return the value of the polynomial at the given point.
     * @see UnivariateFunction#value(double)
     */
    fn value(&self, x: f64) -> f64 {
        return PolynomialFunction::evaluate(&self.coefficients, x).unwrap();
    }
}

impl DifferentiableUnivariateFunction for PolynomialFunction {
    /**
     * Returns the derivative as a {@link UnivariateFunction}.
     *
     * @return the derivative function.
     */
    fn derivative(&self) -> UnivariateFunctionContainer {
        let derivative = self.polynomial_derivative().unwrap();
        UnivariateFunctionContainer { f: Box::new(move |x| derivative.value(x)) }
    }
}

impl UnivariateDifferentiableFunction for PolynomialFunction {
    /** {@inheritDoc}
     * @since 3.1
     * @throws NoDataException if {@code coefficients} is empty.
     * @throws NullArgumentException if {@code coefficients} is {@code null}.
     */
    fn value_ds(&self, t: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        let n: usize = self.coefficients.len();
        if n == 0 {
            return Err(Exc::new_msg("NoDataException",
                                    format!("empty polynomials coefficients array")));
        }
        let mut result: DerivativeStructure =
            try!(DerivativeStructure::new_parameters_order_value(t.get_free_parameters(),
                                                                 t.get_order(),
                                                                 self.coefficients[n - 1]));
        {
            let mut j: i32 = n as i32 - 2;
            while j >= 0 {
                {
                    result = result.multiply(&t)
                        .unwrap()
                        .add_f64(self.coefficients[j as usize])
                        .unwrap();
                }
                j -= 1;
            }
        }

        return Ok(result);
    }
}

impl PolynomialFunction {
    /**
     * Construct a polynomial with the given coefficients.  The first element
     * of the coefficients array is the constant term.  Higher degree
     * coefficients follow in sequence.  The degree of the resulting polynomial
     * is the index of the last non-null element of the array, or 0 if all elements
     * are null.
     * <p>
     * The constructor makes a copy of the input array and assigns the copy to
     * the coefficients property.</p>
     *
     * @param c Polynomial coefficients.
     * @throws NullArgumentException if {@code c} is {@code null}.
     * @throws NoDataException if {@code c} is empty.
     */
    pub fn new(c: &Vec<f64>) -> Result<PolynomialFunction, Rc<Exception>> {
        let mut n: usize = c.len();
        if n == 0 {
            return Err(Exc::new_msg("NoDataException",
                                    format!("empty polynomials coefficients array")));
        }
        while (n > 1) && (c[n - 1] == 0.0) {
            n -= 1;
        }

        let mut coefficients = Vec::new();
        for i in 0..n {
            coefficients.push(c[i]);
        }
        Ok(PolynomialFunction { coefficients: coefficients })
    }



    /**
     * Returns the degree of the polynomial.
     *
     * @return the degree of the polynomial.
     */
    pub fn degree(&self) -> i32 {
        return (self.coefficients.len() - 1) as i32;
    }

    /**
     * Returns a copy of the coefficients array.
     * <p>
     * Changes made to the returned copy will not affect the coefficients of
     * the polynomial.</p>
     *
     * @return a fresh copy of the coefficients array.
     */
    pub fn get_coefficients(&self) -> Vec<f64> {
        return self.coefficients.clone();
    }

    /**
     * Uses Horner's Method to evaluate the polynomial with the given coefficients at
     * the argument.
     *
     * @param coefficients Coefficients of the polynomial to evaluate.
     * @param argument Input value.
     * @return the value of the polynomial.
     * @throws NoDataException if {@code coefficients} is empty.
     * @throws NullArgumentException if {@code coefficients} is {@code null}.
     */
    pub fn evaluate(coefficients: &Vec<f64>, argument: f64) -> Result<f64, Rc<Exception>> {
        let n: usize = coefficients.len();
        if n == 0 {
            return Err(Exc::new_msg("NoDataException",
                                    format!("empty polynomials coefficients array")));
        }
        let mut result: f64 = coefficients[n - 1];
        {
            let mut j: i32 = n as i32 - 2;
            while j >= 0 {
                {
                    result = argument * result + coefficients[j as usize];
                }
                j -= 1;
            }
        }

        return Ok(result);
    }


    /**
     * Add a polynomial to the instance.
     *
     * @param p Polynomial to add.
     * @return a new polynomial which is the sum of the instance and {@code p}.
     */
    pub fn add(&self, p: &PolynomialFunction) -> PolynomialFunction {
        // identify the lowest degree polynomial
        let low_length: i32 = I32::min(self.coefficients.len() as i32, p.coefficients.len() as i32);
        let high_length: i32 = I32::max(self.coefficients.len() as i32,
                                        p.coefficients.len() as i32);
        // build the coefficients array
        let mut new_coefficients = vec![0.0; high_length as usize];
        {
            let mut i: usize = 0;
            while i < low_length as usize {
                {
                    new_coefficients[i] = self.coefficients[i] + p.coefficients[i];
                }
                i += 1;
            }
        }

        let src = if self.coefficients.len() < p.coefficients.len() {
            &p.coefficients
        } else {
            &self.coefficients
        };
        for i in low_length..high_length {
            new_coefficients[i as usize] = src[i as usize];
        }

        return PolynomialFunction::new(&new_coefficients).unwrap();
    }

    /**
     * Subtract a polynomial from the instance.
     *
     * @param p Polynomial to subtract.
     * @return a new polynomial which is the instance minus {@code p}.
     */
    pub fn subtract(&self, p: &PolynomialFunction) -> PolynomialFunction {
        // identify the lowest degree polynomial
        let low_length: i32 = I32::min(self.coefficients.len() as i32, p.coefficients.len() as i32);
        let high_length: i32 = I32::max(self.coefficients.len() as i32,
                                        p.coefficients.len() as i32);
        // build the coefficients array
        let mut new_coefficients = vec![0.0; high_length as usize];
        {
            let mut i: usize = 0;
            while i < low_length as usize {
                {
                    new_coefficients[i] = self.coefficients[i] - p.coefficients[i];
                }
                i += 1;
            }
        }

        if self.coefficients.len() < p.coefficients.len() {
            {
                let mut i: usize = low_length as usize;
                while i < high_length as usize {
                    {
                        new_coefficients[i] = -p.coefficients[i];
                    }
                    i += 1;
                }
            }

        } else {
            for i in low_length..high_length {
                new_coefficients[i as usize] = self.coefficients[i as usize];
            }
        }
        return PolynomialFunction::new(&new_coefficients).unwrap();
    }

    /**
     * Negate the instance.
     *
     * @return a new polynomial with all coefficients negated
     */
    pub fn negate(&self) -> PolynomialFunction {
        let mut new_coefficients = vec![0.0; self.coefficients.len()];
        {
            let mut i: usize = 0;
            while i < self.coefficients.len() {
                {
                    new_coefficients[i] = -self.coefficients[i];
                }
                i += 1;
            }
        }

        return PolynomialFunction::new(&new_coefficients).unwrap();
    }

    /**
     * Multiply the instance by a polynomial.
     *
     * @param p Polynomial to multiply by.
     * @return a new polynomial equal to this times {@code p}
     */
    pub fn multiply(&self, p: &PolynomialFunction) -> PolynomialFunction {
        let mut new_coefficients = vec![0.0; self.coefficients.len() + p.coefficients.len() - 1];
        {
            let mut i: usize = 0;
            while i < new_coefficients.len() {
                {
                    new_coefficients[i] = 0.0;
                    {
                        let mut j: i32 = I32::max(0, i as i32 + 1 - p.coefficients.len() as i32);
                        while j < I32::min(self.coefficients.len() as i32, i as i32 + 1) {
                            {
                                new_coefficients[i] += self.coefficients[j as usize] *
                                                       p.coefficients[i - j as usize];
                            }
                            j += 1;
                        }
                    }

                }
                i += 1;
            }
        }

        return PolynomialFunction::new(&new_coefficients).unwrap();
    }

    /**
     * Returns the coefficients of the derivative of the polynomial with the given coefficients.
     *
     * @param coefficients Coefficients of the polynomial to differentiate.
     * @return the coefficients of the derivative or {@code null} if coefficients has length 1.
     * @throws NoDataException if {@code coefficients} is empty.
     * @throws NullArgumentException if {@code coefficients} is {@code null}.
     */
    pub fn differentiate(coefficients: &Vec<f64>) -> Result<Vec<f64>, Rc<Exception>> {
        let n: usize = coefficients.len();
        if n == 0 {
            return Err(Exc::new_msg("NoDataException",
                                    format!("empty polynomials coefficients array")));
        }
        if n == 1 {
            return Ok(vec![0.0]);
        }
        let mut result = vec![0.0; n - 1];
        {
            let mut i: usize = n - 1;
            while i > 0 {
                {
                    result[i - 1] = i as f64 * coefficients[i];
                }
                i -= 1;
            }
        }

        return Ok(result);
    }

    /**
     * Returns the derivative as a {@link PolynomialFunction}.
     *
     * @return the derivative polynomial.
     */
    pub fn polynomial_derivative(&self) -> Result<PolynomialFunction, Rc<Exception>> {
        return PolynomialFunction::new(&try!(PolynomialFunction::differentiate(&self.coefficients)));
    }



    /**
     * Returns a string representation of the polynomial.
     *
     * <p>The representation is user oriented. Terms are displayed lowest
     * degrees first. The multiplications signs, coefficients equals to
     * one and null terms are not displayed (except if the polynomial is 0,
     * in which case the 0 constant term is displayed). Addition of terms
     * with negative coefficients are replaced by subtraction of terms
     * with positive coefficients except for the first displayed term
     * (i.e. we display <code>-3</code> for a constant negative polynomial,
     * but <code>1 - 3 x + x^2</code> if the negative coefficient is not
     * the first one displayed).</p>
     *
     * @return a string representation of the polynomial.
     */
    pub fn to_string(&self) -> String {
        let mut s: String = String::new();
        if self.coefficients[0] == 0.0 {
            if self.coefficients.len() == 1 {
                return "0".to_string();
            }
        } else {
            s.push_str(&PolynomialFunction::coefficient_to_string(self.coefficients[0]));
        }
        {
            let mut i: usize = 1;
            while i < self.coefficients.len() {
                {
                    if self.coefficients[i] != 0.0 {
                        if s.len() > 0 {
                            if self.coefficients[i] < 0.0 {
                                s.push_str(" - ");
                            } else {
                                s.push_str(" + ");
                            }
                        } else {
                            if self.coefficients[i] < 0.0 {
                                s.push_str("-");
                            }
                        }
                        let abs_ai: f64 = F64::abs(self.coefficients[i]);
                        if (abs_ai - 1.0) != 0.0 {
                            s.push_str(&PolynomialFunction::coefficient_to_string(abs_ai));
                            s.push(' ');
                        }
                        s.push('x');
                        if i > 1 {
                            s.push('^');
                            s.push_str(&i.to_string());
                        }
                    }
                }
                i += 1;
            }
        }

        return s.to_string();
    }

    /**
     * Creates a string representing a coefficient, removing ".0" endings.
     *
     * @param coeff Coefficient.
     * @return a string representation of {@code coeff}.
     */
    fn coefficient_to_string(coeff: f64) -> String {
        let c: String = coeff.to_string();
        if c.ends_with(".0") {
            return c[..c.len() - 2].to_string();
        } else {
            return c;
        }
    }

    // pub fn  hash_code(&self) -> i32  {
    // let prime: i32 = 31;
    // let mut result: i32 = 1;
    // result = prime * result + Arrays::hash_code(self.coefficients);
    // return result;
    // }
    //
    // pub fn  equals(&self,  obj: &Object) -> bool  {
    // if self == obj {
    // return true;
    // }
    // if !(obj instanceof PolynomialFunction) {
    // return false;
    // }
    // let other: PolynomialFunction = obj as PolynomialFunction;
    // if !Arrays::equals(self.coefficients, other.coefficients) {
    // return false;
    // }
    // return true;
    // }
    //
    //
    // Dedicated parametric polynomial class.
    //
    // @since 3.0
    // /
    // #[derive(ParametricUnivariateFunction)]
    // pub struct Parametric {
    // }
    //
    // impl Parametric {
    //
    // {@inheritDoc} */
    // pub fn  gradient(&self,  x: f64,  parameters: f64) -> Vec<f64>  {
    // let mut gradient: [f64; parameters.len()] = [0.0; parameters.len()];
    // let mut xn: f64 = 1.0;
    // {
    // let mut i: i32 = 0;
    // while i < parameters.len() {
    // {
    // gradient[i] = xn;
    // xn *= x;
    // }
    // i += 1;
    // }
    // }
    //
    // return gradient;
    // }
    //
    // {@inheritDoc} */
    // pub fn  value(&self,  x: f64,  parameters: f64) -> /*  throws NoDataException */Result<f64, Rc<Exception>>   {
    // return Ok(PolynomialFunction::evaluate(parameters, x));
    // }
    // }
    //
}
