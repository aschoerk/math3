
// Licensed to the Apache Software Foundation (ASF) under one or more
// contributor license agreements.  See the NOTICE file distributed with
// this work for additional information regarding copyright ownership.
// The ASF licenses this file to You under the Apache License, Version 2.0
// (the "License"); you may not use this file except in compliance with
// the License.  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
/**
 * Represents a polynomial spline function.
 * <p>
 * A <strong>polynomial spline function</strong> consists of a set of
 * <i>interpolating polynomials</i> and an ascending array of domain
 * <i>knot points</i>, determining the intervals over which the spline function
 * is defined by the constituent polynomials.  The polynomials are assumed to
 * have been computed to match the values of another function at the knot
 * points.  The value consistency constraints are not currently enforced by
 * <code>PolynomialSplineFunction</code> itself, but are assumed to hold among
 * the polynomials and knot points passed to the constructor.</p>
 * <p>
 * N.B.:  The polynomials in the <code>polynomials</code> property must be
 * centered on the knot points to compute the spline function values.
 * See below.</p>
 * <p>
 * The domain of the polynomial spline function is
 * <code>[smallest knot, largest knot]</code>.  Attempts to evaluate the
 * function at values outside of this range generate IllegalArgumentExceptions.
 * </p>
 * <p>
 * The value of the polynomial spline function for an argument <code>x</code>
 * is computed as follows:
 * <ol>
 * <li>The knot array is searched to find the segment to which <code>x</code>
 * belongs.  If <code>x</code> is less than the smallest knot point or greater
 * than the largest one, an <code>IllegalArgumentException</code>
 * is thrown.</li>
 * <li> Let <code>j</code> be the index of the largest knot point that is less
 * than or equal to <code>x</code>.  The value returned is
 * {@code polynomials[j](x - knot[j])}</li></ol>
 *
 */
use std::rc::*;
use java::exc::*;
use std::cmp::Ordering;
use util::math_arrays::*;
use analysis::traits::*;
use analysis::polynomials::polynomial_function::*;
use analysis::differentiation::derivative_structure::*;

pub struct PolynomialSplineFunction {
    /**
     * Spline segment interval delimiters (knots).
     * Size is n + 1 for n segments.
     */
    knots: Vec<f64>,

    /**
     * The polynomial functions that make up the spline.  The first element
     * determines the value of the spline over the first subinterval, the
     * second over the second, etc.   Spline function values are determined by
     * evaluating these functions at {@code (x - knot[i])} where i is the
     * knot segment to which x belongs.
     */
    polynomials: Vec<PolynomialFunction>,

    /**
     * Number of spline segments. It is equal to the number of polynomials and
     * to the number of partition points - 1.
     */
    n: usize,
}

impl UnivariateFunction for PolynomialSplineFunction {
    /**
     * Compute the value for the function.
     * See {@link PolynomialSplineFunction} for details on the algorithm for
     * computing the value of the function.
     *
     * @param v Point for which the function value should be computed.
     * @return the value.
     * @throws OutOfRangeException if {@code v} is outside of the domain of the
     * spline function (smaller than the smallest knot point or larger than the
     * largest knot point).
     */
    fn value(&self, v: f64) -> f64 {
        if v.is_nan() || v < self.knots[0] || v > self.knots[self.n] {
            panic!("UnivariateFunction#value throws: {}, v:{} first knot: {} last knot: {}", "OutOfRangeException", v, self.knots[0], self.knots[self.n]);
            // throw OutOfRangeException::new(v, self.knots[0], self.knots[self.n]);
        }
        let index = match self.knots.binary_search_by(|probe| match probe.partial_cmp(&v) { 
            Some(res) => res, 
            None => Ordering::Greater,
        }) {
            Ok(index_found) => {
                if index_found >= self.polynomials.len() {
                    index_found - 1
                } else {
                    index_found
                }
            }
            Err(index_to_be_placed) => index_to_be_placed - 1,
        };

        return self.polynomials[index].value(v - self.knots[index]);
    }
}

impl UnivariateDifferentiableFunction for PolynomialSplineFunction {
    /** {@inheritDoc}
     * @since 3.1
     */
    fn value_ds(&self, t: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        let t0: f64 = t.get_value();
        if t0.is_nan() || t0 < self.knots[0] || t0 > self.knots[self.n] {
            return Err(Exc::new_msg("OutOfRangeException", format!("t0 {}, not between {} and {}", t0, self.knots[0], self.knots[self.n])));
        }
        let index = match self.knots.binary_search_by(|probe| match probe.partial_cmp(&t0) { 
            Some(res) => res, 
            None => Ordering::Greater,
        }) {
            Ok(index_found) => {
                if index_found >= self.polynomials.len() {
                    index_found - 1
                } else {
                    index_found
                }
            }
            Err(index_to_be_placed) => index_to_be_placed - 1,
        };

        let tmp = try!(t.subtract_f64(self.knots[index]));
        let res = try!(self.polynomials[index].value_ds(&tmp));

        return Ok(res);
    }
}

impl PolynomialSplineFunction {
    /**
     * Construct a polynomial spline function with the given segment delimiters
     * and interpolating polynomials.
     * The constructor copies both arrays and assigns the copies to the knots
     * and polynomials properties, respectively.
     *
     * @param knots Spline segment interval delimiters.
     * @param polynomials Polynomial functions that make up the spline.
     * @throws NullArgumentException if either of the input arrays is {@code null}.
     * @throws NumberIsTooSmallException if knots has length less than 2.
     * @throws DimensionMismatchException if {@code polynomials.length != knots.length - 1}.
     * @throws NonMonotonicSequenceException if the {@code knots} array is not strictly increasing.
     *
     */
    pub fn new(knots: &Vec<f64>,
               polynomials: &Vec<PolynomialFunction>)
               -> Result<PolynomialSplineFunction, Rc<Exception>> {
        if knots.len() < 2 {
            return Err(Exc::new_msg("NumberIsTooSmallException",format!("spline partition must have at least {0} points, got {1}", 2, knots.len())));
        }
        if knots.len() - 1 != polynomials.len() {
            return Err(Exc::new_msg("DimensionMismatchException",format!("array sizes should have difference 1 ({0} != {1} + 1)",polynomials.len(), knots.len())));
        }
        try!(MathArrays::check_order_inc_strict(knots));
        Ok(PolynomialSplineFunction {
            n: knots.len() - 1,
            knots: knots.clone(),
            polynomials: polynomials.clone(),
        })
    }



    /**
     * Get the derivative of the polynomial spline function.
     *
     * @return the derivative function.
     */
    pub fn derivative(&self) -> Result<UnivariateFunctionContainer, Rc<Exception>> {
        let derivative = try!(self.polynomial_spline_derivative());
        Ok(UnivariateFunctionContainer::new(Box::new(move |x: f64| {
            return derivative.value(x);
        })))

    }

    /**
     * Get the derivative of the polynomial spline function.
     *
     * @return the derivative function.
     */
    pub fn polynomial_spline_derivative(&self) -> Result<PolynomialSplineFunction, Rc<Exception>> {
        let mut derivative_polynomials = Vec::new();
        {
            for i in 0..self.n {
                let derivative = try!(self.polynomials[i].polynomial_derivative());
                derivative_polynomials.push(derivative);
            }
        }

        return PolynomialSplineFunction::new(&self.knots, &derivative_polynomials);
    }



    /**
     * Get the number of spline segments.
     * It is also the number of polynomials and the number of knot points - 1.
     *
     * @return the number of spline segments.
     */
    pub fn get_n(&self) -> usize {
        return self.n;
    }

    /**
     * Get a copy of the interpolating polynomials array.
     * It returns a fresh copy of the array. Changes made to the copy will
     * not affect the polynomials property.
     *
     * @return the interpolating polynomials.
     */
    pub fn get_polynomials(&self) -> Vec<PolynomialFunction> {
        return self.polynomials.clone();
    }

    /**
     * Get an array copy of the knot points.
     * It returns a fresh copy of the array. Changes made to the copy
     * will not affect the knots property.
     *
     * @return the knot points.
     */
    pub fn get_knots(&self) -> Vec<f64> {
        return self.knots.clone();
    }

    /**
     * Indicates whether a point is within the interpolation range.
     *
     * @param x Point.
     * @return {@code true} if {@code x} is a valid point.
     */
    pub fn is_valid_point(&self, x: f64) -> bool {
        if x < self.knots[0] || x > self.knots[self.n] {
            return false;
        } else {
            return true;
        }
    }
}
