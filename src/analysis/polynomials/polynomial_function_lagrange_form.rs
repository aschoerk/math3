
// Licensed to the Apache Software Foundation (ASF) under one or more
// contributor license agreements.  See the NOTICE file distributed with
// this work for additional information regarding copyright ownership.
// The ASF licenses this file to You under the Apache License, Version 2.0
// (the "License"); you may not use this file except in compliance with
// the License.  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
/**
 * Implements the representation of a real polynomial function in
 * <a href="http://mathworld.wolfram.com/LagrangeInterpolatingPolynomial.html">
 * Lagrange Form</a>. For reference, see <b>Introduction to Numerical
 * Analysis</b>, ISBN 038795452X, chapter 2.
 * <p>
 * The approximated function should be smooth enough for Lagrange polynomial
 * to work well. Otherwise, consider using splines instead.</p>
 *
 * @since 1.2
 */
use util::math_arrays::*;
use util::fastmath::F64;
use std::rc::*;
use std::f64;
use java::exc::*;
use analysis::traits::*;


pub struct PolynomialFunctionLagrangeForm {
    /**
     * The coefficients of the polynomial, ordered by degree -- i.e.
     * coefficients[0] is the constant term and coefficients[n] is the
     * coefficient of x^n where n is the degree of the polynomial.
     */
    pub coefficients: Vec<f64>,

    /**
     * Interpolating points (abscissas).
     */
    pub x: Vec<f64>,

    /**
     * Function values at interpolating points.
     */
    pub y: Vec<f64>,

    /**
     * Whether the polynomial coefficients are available.
     */
    pub coefficients_computed: bool,
}


impl UnivariateFunction for PolynomialFunctionLagrangeForm {
    /**
     * Calculate the function value at the given point.
     *
     * @param z Point at which the function value is to be computed.
     * @return the function value.
     * @throws DimensionMismatchException if {@code x} and {@code y} have
     * different lengths.
     * @throws org.apache.commons.math3.exception.NonMonotonicSequenceException
     * if {@code x} is not sorted in strictly increasing order.
     * @throws NumberIsTooSmallException if the size of {@code x} is less
     * than 2.
     */
    fn value(&self, z: f64) -> f64 {
        return PolynomialFunctionLagrangeForm::evaluate_internal(&self.x, &self.y, z);
    }
}

impl PolynomialFunctionLagrangeForm {
    /**
     * Construct a Lagrange polynomial with the given abscissas and function
     * values. The order of interpolating points are not important.
     * <p>
     * The constructor makes copy of the input arrays and assigns them.</p>
     *
     * @param x interpolating points
     * @param y function values at interpolating points
     * @throws DimensionMismatchException if the array lengths are different.
     * @throws NumberIsTooSmallException if the number of points is less than 2.
     * @throws NonMonotonicSequenceException
     * if two abscissae have the same value.
     */
    pub fn new(x_p: &Vec<f64>,
               y_p: &Vec<f64>)
               -> Result<PolynomialFunctionLagrangeForm, Rc<Exception>> {
        let mut this = PolynomialFunctionLagrangeForm {
            coefficients: Vec::new(),
            x: x_p.clone(),
            y: y_p.clone(),
            coefficients_computed: false,
        };
        let ok = try!(PolynomialFunctionLagrangeForm::verify_interpolation_array(&this.x,
                                                                                 &this.y,
                                                                                 false));
        if !ok {
            try!(MathArrays::sort_in_place(&mut this.x, &mut vec![&mut this.y]));
            // Second check in case some abscissa is duplicated.
            try!(PolynomialFunctionLagrangeForm::verify_interpolation_array(&this.x,
                                                                            &this.y,
                                                                            true));
        }
        Ok(this)
    }



    /**
     * Returns the degree of the polynomial.
     *
     * @return the degree of the polynomial
     */
    pub fn degree(&self) -> usize {
        return self.x.len() - 1;
    }

    /**
     * Returns a copy of the interpolating points array.
     * <p>
     * Changes made to the returned copy will not affect the polynomial.</p>
     *
     * @return a fresh copy of the interpolating points array
     */
    pub fn get_interpolating_points(&self) -> Vec<f64> {
        return self.x.clone();
    }

    /**
     * Returns a copy of the interpolating values array.
     * <p>
     * Changes made to the returned copy will not affect the polynomial.</p>
     *
     * @return a fresh copy of the interpolating values array
     */
    pub fn get_interpolating_values(&self) -> Vec<f64> {
        return self.y.clone();
    }

    /**
     * Returns a copy of the coefficients array.
     * <p>
     * Changes made to the returned copy will not affect the polynomial.</p>
     * <p>
     * Note that coefficients computation can be ill-conditioned. Use with caution
     * and only when it is necessary.</p>
     *
     * @return a fresh copy of the coefficients array
     */
    pub fn get_coefficients(&mut self) -> Vec<f64> {
        if !self.coefficients_computed {
            self.compute_coefficients();
        }
        return self.coefficients.clone();
    }

    /**
     * Evaluate the Lagrange polynomial using
     * <a href="http://mathworld.wolfram.com/NevillesAlgorithm.html">
     * Neville's Algorithm</a>. It takes O(n^2) time.
     *
     * @param x Interpolating points array.
     * @param y Interpolating values array.
     * @param z Point at which the function value is to be computed.
     * @return the function value.
     * @throws DimensionMismatchException if {@code x} and {@code y} have
     * different lengths.
     * @throws NonMonotonicSequenceException
     * if {@code x} is not sorted in strictly increasing order.
     * @throws NumberIsTooSmallException if the size of {@code x} is less
     * than 2.
     */
    pub fn evaluate(x: &Vec<f64>, y: &Vec<f64>, z: f64) -> Result<f64, Rc<Exception>> {
        if try!(PolynomialFunctionLagrangeForm::verify_interpolation_array(&x, &y, false)) {
            return Ok(PolynomialFunctionLagrangeForm::evaluate_internal(&x, &y, z));
        }
        // Array is not sorted.
        let mut x_new = x.clone();
        let mut y_new = y.clone();
        try!(MathArrays::sort_in_place(&mut x_new, &mut vec![&mut y_new]));
        // Second check in case some abscissa is duplicated.
        try!(PolynomialFunctionLagrangeForm::verify_interpolation_array(&x_new, &y_new, true));
        return Ok(PolynomialFunctionLagrangeForm::evaluate_internal(&x_new, &y_new, z));
    }

    /**
     * Evaluate the Lagrange polynomial using
     * <a href="http://mathworld.wolfram.com/NevillesAlgorithm.html">
     * Neville's Algorithm</a>. It takes O(n^2) time.
     *
     * @param x Interpolating points array.
     * @param y Interpolating values array.
     * @param z Point at which the function value is to be computed.
     * @return the function value.
     * @throws DimensionMismatchException if {@code x} and {@code y} have
     * different lengths.
     * @throws org.apache.commons.math3.exception.NonMonotonicSequenceException
     * if {@code x} is not sorted in strictly increasing order.
     * @throws NumberIsTooSmallException if the size of {@code x} is less
     * than 2.
     */
    fn evaluate_internal(x: &Vec<f64>, y: &Vec<f64>, z: f64) -> f64 {
        let mut nearest: usize = 0;
        let n: usize = x.len();
        let mut c = Vec::new();
        let mut d = Vec::new();
        let mut min_dist: f64 = f64::INFINITY;
        {
            let mut i: usize = 0;
            while i < n {
                {
                    // initialize the difference arrays
                    c.push(y[i]);
                    d.push(y[i]);
                    // find out the abscissa closest to z
                    let dist: f64 = F64::abs(z - x[i]);
                    if dist < min_dist {
                        nearest = i;
                        min_dist = dist;
                    }
                }
                i += 1;
            }
        }

        // initial approximation to the function value at z
        let mut value: f64 = y[nearest];
        {
            let mut i: usize = 1;
            while i < n {
                {
                    {
                        let mut j: usize = 0;
                        while j < n - i {
                            {
                                let tc: f64 = x[j] - z;
                                let td: f64 = x[i + j] - z;
                                let divider: f64 = x[j] - x[i + j];
                                // update the difference arrays
                                let w: f64 = (c[j + 1] - d[j]) / divider;
                                c[j] = tc * w;
                                d[j] = td * w;
                            }
                            j += 1;
                        }
                    }

                    // sum up the difference terms to get the final value
                    if (nearest as f64) < 0.5 * (n - i + 1) as f64 {
                        // fork down
                        value += c[nearest];
                    } else {
                        nearest -= 1;
                        // fork up
                        value += d[nearest];
                    }
                }
                i += 1;
            }
        }

        return value;
    }

    /**
     * Calculate the coefficients of Lagrange polynomial from the
     * interpolation data. It takes O(n^2) time.
     * Note that this computation can be ill-conditioned: Use with caution
     * and only when it is necessary.
     */
    pub fn compute_coefficients(&mut self) {
        let n: usize = self.degree() + 1;
        self.coefficients = Vec::new();
        {
            let mut i: usize = 0;
            while i < n {
                {
                    self.coefficients.push(0.0);
                }
                i += 1;
            }
        }

        // c[] are the coefficients of P(x) = (x-x[0])(x-x[1])...(x-x[n-1])
        let mut c = vec![0.0; n + 1];
        c[0] = 1.0;
        {
            let mut i: usize = 0;
            while i < n {
                {
                    {
                        let mut j: usize = i;
                        while j > 0 {
                            {
                                c[j] = c[j - 1] - c[j] * self.x[i];
                            }
                            j -= 1;
                        }
                    }

                    c[0] *= -self.x[i];
                    c[i + 1] = 1.0;
                }
                i += 1;
            }
        }

        let mut tc = vec![0.0; n];
        {
            let mut i: usize = 0;
            while i < n {
                {
                    // d = (x[i]-x[0])...(x[i]-x[i-1])(x[i]-x[i+1])...(x[i]-x[n-1])
                    let mut d: f64 = 1.0;
                    {
                        let mut j: usize = 0;
                        while j < n {
                            {
                                if i != j {
                                    d *= self.x[i] - self.x[j];
                                }
                            }
                            j += 1;
                        }
                    }

                    let t: f64 = self.y[i] / d;
                    // Lagrange polynomial is the sum of n terms, each of which is a
                    // polynomial of degree n-1. tc[] are the coefficients of the i-th
                    // numerator Pi(x) = (x-x[0])...(x-x[i-1])(x-x[i+1])...(x-x[n-1]).
                    // actually c[n] = 1
                    tc[n - 1] = c[n];
                    self.coefficients[n - 1] += t * tc[n - 1];
                    {
                        let mut j: usize = n - 2;
                        loop {
                            {
                                tc[j] = c[j + 1] + tc[j + 1] * self.x[i];
                                self.coefficients[j] += t * tc[j];
                            }
                            if j == 0 {
                                break;
                            }
                            j -= 1;
                        }
                    }

                }
                i += 1;
            }
        }

        self.coefficients_computed = true;
    }


    /**
     * Check that the interpolation arrays are valid.
     * The arrays features checked by this method are that both arrays have the
     * same length and this length is at least 2.
     *
     * @param x Interpolating points array.
     * @param y Interpolating values array.
     * @param abort Whether to throw an exception if {@code x} is not sorted.
     * @throws DimensionMismatchException if the array lengths are different.
     * @throws NumberIsTooSmallException if the number of points is less than 2.
     * @throws org.apache.commons.math3.exception.NonMonotonicSequenceException
     * if {@code x} is not sorted in strictly increasing order and {@code abort}
     * is {@code true}.
     * @return {@code false} if the {@code x} is not sorted in increasing order,
     * {@code true} otherwise.
     * @see #evaluate(double[], double[], double)
     * @see #computeCoefficients()
     */
    pub fn verify_interpolation_array(x: &Vec<f64>,
                                      y: &Vec<f64>,
                                      abort: bool)
                                      -> Result<bool, Rc<Exception>> {
        if x.len() != y.len() {
            return Err(Exc::new_msg("DimensionMismatchException",
                                    format!("{} {}", x.len(), y.len())));
        }
        if x.len() < 2 {
            return Err(Exc::new_msg("NumberIsTooSmallException",
                                    format!("{0} points are required, got only {1}", 2, x.len())));
        }
        let res = try!(MathArrays::check_order_abort(x, OrderDirection::INCREASING, true, abort));
        Ok(res)
    }
}
