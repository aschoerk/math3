// Licensed to the Apache Software Foundation (ASF) under one or more
// contributor license agreements.  See the NOTICE file distributed with
// this work for additional information regarding copyright ownership.
// The ASF licenses this file to You under the Apache License, Version 2.0
// (the "License"); you may not use this file except in compliance with
// the License.  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#![allow(unused_imports)]
use analysis::polynomials::polynomial_function::*;
use assert;
use analysis::traits::*;
use util::fastmath;
/**
* Tests the PolynomialFunction implementation of a UnivariateFunction.
*
*/
/** Error tolerance for tests */
const TOLERANCE: f64 = 1e-12;


pub fn check_polynomial(p: &PolynomialFunction, reference: &str) {
    assert_eq!(reference, p.to_string());
}


fn check_null_polynomial(p: &PolynomialFunction) {
    for coefficient in p.get_coefficients() {
        assert_within_delta!(0.0, coefficient, 1e-15);
    }
}


/**
 * tests the value of a constant polynomial.
 *
 * <p>value of this is 2.5 everywhere.</p>
 */
#[test]
pub fn test_constants() {
    let c = vec![2.5, ];
    let f: PolynomialFunction = PolynomialFunction::new(&c).unwrap();
    // verify that we are equal to c[0] at several (nonsymmetric) places
    assert_within_delta!(f.value(0.0), c[0], TOLERANCE);
    assert_within_delta!(f.value(-1.0), c[0], TOLERANCE);
    assert_within_delta!(f.value(-123.5), c[0], TOLERANCE);
    assert_within_delta!(f.value(3.0), c[0], TOLERANCE);
    assert_within_delta!(f.value(456.89), c[0], TOLERANCE);
    assert_eq!(f.degree(), 0);
    assert_within_delta!(f.derivative().value(0.0), 0.0, TOLERANCE);
    assert_within_delta!(f.polynomial_derivative().unwrap().derivative().value(0.0), 0.0, TOLERANCE);
}


/**
 * tests the value of a linear polynomial.
 *
 * <p>This will test the function f(x) = 3*x - 1.5</p>
 * <p>This will have the values
 *  <tt>f(0) = -1.5, f(-1) = -4.5, f(-2.5) = -9,
 *      f(0.5) = 0, f(1.5) = 3</tt> and {@code f(3) = 7.5}
 * </p>
 */
#[test]
pub fn test_linear() {
    let c = vec![-1.5, 3.0, ];
    let f: PolynomialFunction = PolynomialFunction::new(&c).unwrap();
    // verify that we are equal to c[0] when x=0
    assert_within_delta!(f.value(0.0), c[0], TOLERANCE);
    // now check a few other places
    assert_within_delta!(-4.5, f.value(-1.0), TOLERANCE);
    assert_within_delta!(-9.0, f.value(-2.5), TOLERANCE);
    assert_within_delta!(0.0, f.value(0.5), TOLERANCE);
    assert_within_delta!(3.0, f.value(1.5), TOLERANCE);
    assert_within_delta!(7.5, f.value(3.0), TOLERANCE);
    assert_eq!(f.degree(), 1);
    assert_within_delta!(f.polynomial_derivative().unwrap().derivative().value(0.0), 0.0, TOLERANCE);
}


/**
 * Tests a second order polynomial.
 * <p> This will test the function f(x) = 2x^2 - 3x -2 = (2x+1)(x-2)</p>
 */
#[test]
pub fn test_quadratic() {
    let c = vec![-2.0, -3.0, 2.0, ];
    let f: PolynomialFunction = PolynomialFunction::new(&c).unwrap();
    // verify that we are equal to c[0] when x=0
    assert_within_delta!(f.value(0.0), c[0], TOLERANCE);
    // now check a few other places
    assert_within_delta!(0.0, f.value(-0.5), TOLERANCE);
    assert_within_delta!(0.0, f.value(2.0), TOLERANCE);
    assert_within_delta!(-2.0, f.value(1.5), TOLERANCE);
    assert_within_delta!(7.0, f.value(-1.5), TOLERANCE);
    assert_within_delta!(265.5312, f.value(12.34), TOLERANCE);
}


/**
 * This will test the quintic function
 *   f(x) = x^2(x-5)(x+3)(x-1) = x^5 - 3x^4 -13x^3 + 15x^2</p>
 */
#[test]
pub fn test_quintic() {
    let c = vec![0.0, 0.0, 15.0, -13.0, -3.0, 1.0, ];
    let f: PolynomialFunction = PolynomialFunction::new(&c).unwrap();
    // verify that we are equal to c[0] when x=0
    assert_within_delta!(f.value(0.0), c[0], TOLERANCE);
    // now check a few other places
    assert_within_delta!(0.0, f.value(5.0), TOLERANCE);
    assert_within_delta!(0.0, f.value(1.0), TOLERANCE);
    assert_within_delta!(0.0, f.value(-3.0), TOLERANCE);
    assert_within_delta!(54.84375, f.value(-1.5), TOLERANCE);
    assert_within_delta!(-8.06637, f.value(1.3), TOLERANCE);
    assert_eq!(f.degree(), 5);
}


/**
 * tests the firstDerivative function by comparison
 *
 * <p>This will test the functions
 * {@code f(x) = x^3 - 2x^2 + 6x + 3, g(x) = 3x^2 - 4x + 6}
 * and {@code h(x) = 6x - 4}
 */
#[test]
pub fn testfirst_derivative_comparison() {
    let f_coeff = vec![3.0, 6.0, -2.0, 1.0, ];
    let g_coeff = vec![6.0, -4.0, 3.0, ];
    let h_coeff = vec![-4.0, 6.0, ];
    let f: PolynomialFunction = PolynomialFunction::new(&f_coeff).unwrap();
    let g: PolynomialFunction = PolynomialFunction::new(&g_coeff).unwrap();
    let h: PolynomialFunction = PolynomialFunction::new(&h_coeff).unwrap();
    // compare f' = g
    assert_within_delta!(f.derivative().value(0.0), g.value(0.0), TOLERANCE);
    assert_within_delta!(f.derivative().value(1.0), g.value(1.0), TOLERANCE);
    assert_within_delta!(f.derivative().value(100.0), g.value(100.0), TOLERANCE);
    assert_within_delta!(f.derivative().value(4.1), g.value(4.1), TOLERANCE);
    assert_within_delta!(f.derivative().value(-3.25), g.value(-3.25), TOLERANCE);
    // compare g' = h
    assert_within_delta!(g.derivative().value(fastmath::PI), h.value(fastmath::PI), TOLERANCE);
    assert_within_delta!(g.derivative().value(fastmath::E), h.value(fastmath::E), TOLERANCE);
}


#[test]
pub fn test_string() {
    let p: PolynomialFunction = PolynomialFunction::new(&vec![-5.0, 3.0, 1.0, ]).unwrap();
    check_polynomial(&p, "-5 + 3 x + x^2");
    check_polynomial(&PolynomialFunction::new(&vec![0.0, -2.0, 3.0, ]).unwrap(),
                     "-2 x + 3 x^2");
    check_polynomial(&PolynomialFunction::new(&vec![1.0, -2.0, 3.0, ]).unwrap(),
                     "1 - 2 x + 3 x^2");
    check_polynomial(&PolynomialFunction::new(&vec![0.0, 2.0, 3.0, ]).unwrap(),
                     "2 x + 3 x^2");
    check_polynomial(&PolynomialFunction::new(&vec![1.0, 2.0, 3.0, ]).unwrap(),
                     "1 + 2 x + 3 x^2");
    check_polynomial(&PolynomialFunction::new(&vec![1.0, 0.0, 3.0, ]).unwrap(),
                     "1 + 3 x^2");
    check_polynomial(&PolynomialFunction::new(&vec![0.0, ]).unwrap(), "0");
}


#[test]
pub fn test_addition() {
    let mut p1: PolynomialFunction = PolynomialFunction::new(&vec![-2.0, 1.0, ]).unwrap();
    let mut p2: PolynomialFunction = PolynomialFunction::new(&vec![2.0, -1.0, 0.0, ]).unwrap();
    check_null_polynomial(&p1.add(&p2));
    p2 = p1.add(&p1);
    check_polynomial(&p2, "-4 + 2 x");
    p1 = PolynomialFunction::new(&vec![1.0, -4.0, 2.0, ]).unwrap();
    p2 = PolynomialFunction::new(&vec![-1.0, 3.0, -2.0, ]).unwrap();
    p1 = p1.add(&p2);
    assert_eq!(1, p1.degree());
    check_polynomial(&p1, "-x");
}


#[test]
pub fn test_subtraction() {
    let mut p1: PolynomialFunction = PolynomialFunction::new(&vec![-2.0, 1.0, ]).unwrap();
    check_null_polynomial(&p1.subtract(&p1));
    let mut p2: PolynomialFunction = PolynomialFunction::new(&vec![-2.0, 6.0, ]).unwrap();
    p2 = p2.subtract(&p1);
    check_polynomial(&p2, "5 x");
    p1 = PolynomialFunction::new(&vec![1.0, -4.0, 2.0, ]).unwrap();
    p2 = PolynomialFunction::new(&vec![-1.0, 3.0, 2.0, ]).unwrap();
    p1 = p1.subtract(&p2);
    assert_eq!(1, p1.degree());
    check_polynomial(&p1, "2 - 7 x");
}

#[test]
pub fn test_multiplication() {
    let mut p1: PolynomialFunction = PolynomialFunction::new(&vec![-3.0, 2.0, ]).unwrap();
    let mut p2: PolynomialFunction = PolynomialFunction::new(&vec![3.0, 2.0, 1.0, ]).unwrap();
    check_polynomial(&p1.multiply(&p2), "-9 + x^2 + 2 x^3");
    p1 = PolynomialFunction::new(&vec![0.0, 1.0, ]).unwrap();
    p2 = p1.clone();
    {
        let mut i: i32 = 2;
        while i < 10 {
            {
                p2 = p2.multiply(&p1);
                check_polynomial(&p2, &format!("x^{}", i));
            }
            i += 1;
        }
    }
}

// #[test]
// pub fn  test_serial()   {
// let p2: PolynomialFunction = PolynomialFunction::new( vec![3.0, 2.0, 1.0, ]
// ).unwrap();
// assert_within_delta!(p2, &TestUtils::serialize_and_recover(&p2));
// }
//

/**
 * tests the firstDerivative function by comparison
 *
 * <p>This will test the functions
 * {@code f(x) = x^3 - 2x^2 + 6x + 3, g(x) = 3x^2 - 4x + 6}
 * and {@code h(x) = 6x - 4}
 */
#[test]
pub fn test_math341() {
    let f_coeff = vec![3.0, 6.0, -2.0, 1.0, ];
    let g_coeff = vec![6.0, -4.0, 3.0, ];
    let h_coeff = vec![-4.0, 6.0, ];
    let f: PolynomialFunction = PolynomialFunction::new(&f_coeff).unwrap();
    let g: PolynomialFunction = PolynomialFunction::new(&g_coeff).unwrap();
    let h: PolynomialFunction = PolynomialFunction::new(&h_coeff).unwrap();
    // compare f' = g
    assert_within_delta!(f.derivative().value(0.0), g.value(0.0), TOLERANCE);
    assert_within_delta!(f.derivative().value(1.0), g.value(1.0), TOLERANCE);
    assert_within_delta!(f.derivative().value(100.0), g.value(100.0), TOLERANCE);
    assert_within_delta!(f.derivative().value(4.1), g.value(4.1), TOLERANCE);
    assert_within_delta!(f.derivative().value(-3.25), g.value(-3.25), TOLERANCE);
    // compare g' = h
    assert_within_delta!(g.derivative().value(fastmath::PI), h.value(fastmath::PI), TOLERANCE);
    assert_within_delta!(g.derivative().value(fastmath::E), h.value(fastmath::E), TOLERANCE);
}
