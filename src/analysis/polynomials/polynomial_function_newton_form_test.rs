// Licensed to the Apache Software Foundation (ASF) under one or more
// contributor license agreements.  See the NOTICE file distributed with
// this work for additional information regarding copyright ownership.
// The ASF licenses this file to You under the Apache License, Version 2.0
// (the "License"); you may not use this file except in compliance with
// the License.  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#![allow(unused_imports)]
use analysis::polynomials::polynomial_function_newton_form::*;
use assert;
use analysis::traits::*;
use analysis::differentiation::derivative_structure::*;

/**
 * Test case for Newton form of polynomial function.
 * <p>
 * The small tolerance number is used only to account for round-off errors.
 *
 */
/**
 * Test of polynomial for the linear function.
 */
#[test]
pub fn test_linear_function() {
    let mut p: PolynomialFunctionNewtonForm;
    let mut z: f64;
    let mut expected: f64;
    let mut result: f64;
    let tolerance: f64 = 1E-12;
    // p(x) = 1.5x - 4 = 2 + 1.5(x-4)
    let a = vec![2.0, 1.5, ];
    let c = vec![4.0, ];
    p = PolynomialFunctionNewtonForm::new(&a, &c).unwrap();
    z = 2.0;
    expected = -1.0;
    result = p.value(z);
    assert_within_delta!(expected, result, tolerance);
    z = 4.5;
    expected = 2.75;
    result = p.value(z);
    assert_within_delta!(expected, result, tolerance);
    z = 6.0;
    expected = 5.0;
    result = p.value(z);
    assert_within_delta!(expected, result, tolerance);
    assert_eq!(1, p.degree());
    let coefficients = p.get_coefficients();
    assert_eq!(2, coefficients.len());
    assert_within_delta!(-4.0, coefficients[0], tolerance);
    assert_within_delta!(1.5, coefficients[1], tolerance);
}

/**
 * Test of polynomial for the quadratic function.
 */
#[test]
pub fn test_quadratic_function() {
    let mut p: PolynomialFunctionNewtonForm;
    let mut z: f64;
    let mut expected: f64;
    let mut result: f64;
    let tolerance: f64 = 1E-12;
    // p(x) = 2x^2 + 5x - 3 = 4 + 3(x-1) + 2(x-1)(x+2)
    let a = vec![4.0, 3.0, 2.0, ];
    let c = vec![1.0, -2.0, ];
    p = PolynomialFunctionNewtonForm::new(&a, &c).unwrap();
    z = 1.0;
    expected = 4.0;
    result = p.value(z);
    assert_within_delta!(expected, result, tolerance);
    z = 2.5;
    expected = 22.0;
    result = p.value(z);
    assert_within_delta!(expected, result, tolerance);
    z = -2.0;
    expected = -5.0;
    result = p.value(z);
    assert_within_delta!(expected, result, tolerance);
    assert_eq!(2, p.degree());
    let coefficients = p.get_coefficients();
    assert_eq!(3, coefficients.len());
    assert_within_delta!(-3.0, coefficients[0], tolerance);
    assert_within_delta!(5.0, coefficients[1], tolerance);
    assert_within_delta!(2.0, coefficients[2], tolerance);
}

/**
 * Test of polynomial for the quintic function.
 */
#[test]
pub fn test_quintic_function() {
    let mut p: PolynomialFunctionNewtonForm;
    let mut z: f64;
    let mut expected: f64;
    let mut result: f64;
    let tolerance: f64 = 1E-12;
    // p(x) = x^5 - x^4 - 7x^3 + x^2 + 6x
    //      = 6x - 6x^2 -6x^2(x-1) + x^2(x-1)(x+1) + x^2(x-1)(x+1)(x-2)
    let a = vec![0.0, 6.0, -6.0, -6.0, 1.0, 1.0, ];
    let c = vec![0.0, 0.0, 1.0, -1.0, 2.0, ];
    p = PolynomialFunctionNewtonForm::new(&a, &c).unwrap();
    z = 0.0;
    expected = 0.0;
    result = p.value(z);
    assert_within_delta!(expected, result, tolerance);
    z = -2.0;
    expected = 0.0;
    result = p.value(z);
    assert_within_delta!(expected, result, tolerance);
    z = 4.0;
    expected = 360.0;
    result = p.value(z);
    assert_within_delta!(expected, result, tolerance);
    assert_eq!(5, p.degree());
    let coefficients = p.get_coefficients();
    assert_eq!(6, coefficients.len());
    assert_within_delta!(0.0, coefficients[0], tolerance);
    assert_within_delta!(6.0, coefficients[1], tolerance);
    assert_within_delta!(1.0, coefficients[2], tolerance);
    assert_within_delta!(-7.0, coefficients[3], tolerance);
    assert_within_delta!(-1.0, coefficients[4], tolerance);
    assert_within_delta!(1.0, coefficients[5], tolerance);
}

/**
 * Test for derivatives.
 */
#[test]
pub fn test_derivative() {
    // x^3 = 0 * [1] + 1 * [x] + 3 * [x(x-1)] + 1 * [x(x-1)(x-2)]
    let p: PolynomialFunctionNewtonForm =
        PolynomialFunctionNewtonForm::new(&vec![0.0, 1.0, 3.0, 1.0, ], &vec![0.0, 1.0, 2.0, ])
            .unwrap();
    let eps: f64 = 2.0e-14;
    {
        let mut t: f64 = 0.0;
        while t < 10.0 {
            {
                let x: DerivativeStructure = DerivativeStructure::new(1, 4, 0, t).unwrap();
                let y: DerivativeStructure = p.value_ds(&x).unwrap();
                assert_within_delta!(t * t * t, y.get_value(), eps * t * t * t);
                assert_within_delta!(3.0 * t * t, y.get_partial_derivative(&vec![1]).unwrap(), eps * 3.0 * t * t);
                assert_within_delta!(6.0 * t, y.get_partial_derivative(&vec![2]).unwrap(), eps * 6.0 * t);
                assert_within_delta!(6.0, y.get_partial_derivative(&vec![3]).unwrap(), eps * 6.0);
                assert_within_delta!(0.0, y.get_partial_derivative(&vec![4]).unwrap(), eps);
            }
            t += 0.1;
        }
    }
}

/**
 * Test of parameters for the polynomial.
 */
#[test]
pub fn test_parameters() {
    // bad input array length
    let a = vec![1.0, ];
    let c = vec![2.0, ];
    assert!(PolynomialFunctionNewtonForm::new(&a, &c).is_err());

    // mismatch input arrays
    let a = vec![1.0, 2.0, 3.0, 4.0, ];
    let c = vec![4.0, 3.0, 2.0, 1.0, ];
    assert!(PolynomialFunctionNewtonForm::new(&a, &c).is_err());
}
