
// Licensed to the Apache Software Foundation (ASF) under one or more
// contributor license agreements.  See the NOTICE file distributed with
// this work for additional information regarding copyright ownership.
// The ASF licenses this file to You under the Apache License, Version 2.0
// (the "License"); you may not use this file except in compliance with
// the License.  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#![allow(unused_imports)]
use analysis::polynomials::polynomial_function::*;
use analysis::polynomials::polynomial_spline_function::*;
use assert;
use analysis::traits::*;
use std::rc::*;
use java::exc::*;
/**
 * Tests the PolynomialSplineFunction implementation.
 *
 */
/** Error tolerance for tests */
const TOLERANCE: f64 = 1.0e-12;

/**
     * Quadratic polynomials used in tests:
     *
     * x^2 + x            [-1, 0)
     * x^2 + x + 2        [0, 1)
     * x^2 + x + 4        [1, 2)
     *
     * Defined so that evaluation using PolynomialSplineFunction evaluation
     * algorithm agrees at knot point boundaries.
     */
fn polynomials() -> Vec<PolynomialFunction> {
    vec![
    	PolynomialFunction::new(&vec![0.0, 1.0, 1.0, ]).unwrap(), 
    	PolynomialFunction::new(&vec![2.0, 1.0, 1.0, ]).unwrap(), 
    	PolynomialFunction::new(&vec![4.0, 1.0, 1.0, ]).unwrap(), ]
}



/** Knot points  */
fn knots() -> Vec<f64> {
    vec![-1.0, 0.0, 1.0, 2.0, ]
}


/** Derivative of test polynomials -- 2x + 1  */
fn dp() -> PolynomialFunction {
    PolynomialFunction::new(&vec![1.0, 2.0, ]).unwrap()
}




#[test]
pub fn test_constructor() {
    let spline: PolynomialSplineFunction = PolynomialSplineFunction::new(&knots(), &polynomials())
        .unwrap();
    assert!(knots() == spline.get_knots());
    assert_within_delta!(1.0, spline.get_polynomials()[0].get_coefficients()[2], 0.0);
    assert_eq!(3, spline.get_n());

    // too few knots
    assert!(PolynomialSplineFunction::new(&vec![0.0, ], &polynomials()).is_err());

    // too many knots
    assert!(PolynomialSplineFunction::new(&vec![0.0, 1.0, 2.0, 3.0, 4.0, ], &polynomials()).is_err());

    // knots not increasing
    assert!(PolynomialSplineFunction::new(&vec![0.0, 1.0, 3.0, 2.0, ], &polynomials()).is_err());


}

/**
     *  Do linear search to find largest knot point less than or equal to x.
     *  Implementation does binary search.
     */
pub fn find_knot(knots: &Vec<f64>, x: f64) -> Result<usize, Rc<Exception>> {
    if x < knots[0] || x >= knots[knots.len() - 1] {
        return Err(Exc::new_msg("OutOfRangeException",format!("{} not between {} and {}",x, knots[0], knots[knots.len() - 1])));
    }
    {
        let mut i: usize = 0;
        while i < knots.len() {
            {
                if knots[i] > x {
                    return Ok(i - 1);
                }
            }
            i += 1;
        }
    }

    return Err(Exc::new("MathIllegalStateException"));
}

#[test]
pub fn test_values() {
    let spline = PolynomialSplineFunction::new(&knots(), &polynomials()).unwrap();
    let d_spline = spline.derivative().unwrap();
    // interior points -- spline value at x should equal p(x - knot)
    // where knot is the largest knot point less than or equal to x and p
    // is the polynomial defined over the knot segment to which x belongs.
    //
    let mut x: f64 = -1.0;
    let mut index: usize;
    {
        let mut i: usize = 0;
        while i < 10 {
            {
                x += 0.25;
                index = find_knot(&knots(), x).unwrap();
                assert_within_delta!(polynomials()[index].value(x - knots()[index]), spline.value(x), TOLERANCE,"spline function evaluation failed for x={}", x);
                assert_within_delta!(dp().value(x - knots()[index]), d_spline.value(x), TOLERANCE,"spline derivative evaluation failed for x={}", x);
            }
            i += 1;
        }
    }

    // knot points -- centering should zero arguments
    {
        let mut i: usize = 0;
        while i < 3 {
            {
                assert_within_delta!(polynomials()[i].value(0.0), spline.value(knots()[i]), TOLERANCE,"spline function evaluation failed for knot={}", knots()[i]);
                assert_within_delta!(dp().value(0.0), d_spline.value(knots()[i]), TOLERANCE,"spline function evaluation failed for knot={}", knots()[i]);
            }
            i += 1;
        }
    }

}

#[test]
#[should_panic]
pub fn expect_panic() {
    let spline: PolynomialSplineFunction = PolynomialSplineFunction::new(&knots(), &polynomials())
        .unwrap();
    spline.value(-1.5);
}

#[test]
#[should_panic]
pub fn expect_panic2() {
    let spline: PolynomialSplineFunction = PolynomialSplineFunction::new(&knots(), &polynomials())
        .unwrap();
    spline.value(2.5);
}


#[test]
pub fn test_is_valid_point() {
    let spline: PolynomialSplineFunction = PolynomialSplineFunction::new(&knots(), &polynomials())
        .unwrap();
    let x_min: f64 = knots()[0];
    let x_max: f64 = knots()[knots().len() - 1];
    let mut x: f64;
    x = x_min;
    assert!(spline.is_valid_point(x));
    // Ensure that no exception is thrown.
    spline.value(x);
    x = x_max;
    assert!(spline.is_valid_point(x));
    // Ensure that no exception is thrown.
    spline.value(x);
    let x_range: f64 = x_max - x_min;
    x = x_min + x_range / 3.4;
    assert!(spline.is_valid_point(x));
    // Ensure that no exception is thrown.
    spline.value(x);
    let small: f64 = 1e-8;
    x = x_min - small;
    assert!(!spline.is_valid_point(x));
    // Ensure that an exception would have been thrown.

}

#[test]
#[should_panic]
pub fn expect_panic3() {
    let spline: PolynomialSplineFunction = PolynomialSplineFunction::new(&knots(), &polynomials())
        .unwrap();
    let small: f64 = 1e-8;
    let x_min: f64 = knots()[0];
    let x = x_min - small;
    spline.value(x);
}
