// Licensed to the Apache Software Foundation (ASF) under one or more
// contributor license agreements.  See the NOTICE file distributed with
// this work for additional information regarding copyright ownership.
// The ASF licenses this file to You under the Apache License, Version 2.0
// (the "License"); you may not use this file except in compliance with
// the License.  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
/**
 * Implements the representation of a real polynomial function in
 * Newton Form. For reference, see <b>Elementary Numerical Analysis</b>,
 * ISBN 0070124477, chapter 2.
 * <p>
 * The formula of polynomial in Newton form is
 *     p(x) = a[0] + a[1](x-c[0]) + a[2](x-c[0])(x-c[1]) + ... +
 *            a[n](x-c[0])(x-c[1])...(x-c[n-1])
 * Note that the length of a[] is one more than the length of c[]</p>
 *
 * @since 1.2
 */
use std::rc::*;
use std::f64;
use java::exc::*;
use analysis::traits::*;
use analysis::differentiation::derivative_structure::*;

pub struct PolynomialFunctionNewtonForm {
    /**
     * The coefficients of the polynomial, ordered by degree -- i.e.
     * coefficients[0] is the constant term and coefficients[n] is the
     * coefficient of x^n where n is the degree of the polynomial.
     */
    pub coefficients: Vec<f64>,

    /**
     * Centers of the Newton polynomial.
     */
    pub c: Vec<f64>,

    /**
     * When all c[i] = 0, a[] becomes normal polynomial coefficients,
     * i.e. a[i] = coefficients[i].
     */
    pub a: Vec<f64>,

    /**
     * Whether the polynomial coefficients are available.
     */
    pub coefficients_computed: bool,
}

impl UnivariateFunction for PolynomialFunctionNewtonForm {
    /**
* Calculate the function value at the given point.
*
* @param z Point at which the function value is to be computed.
* @return the function value.
*/
    fn value(&self, z: f64) -> f64 {
        return PolynomialFunctionNewtonForm::evaluate(&self.a, &self.c, z).unwrap();
    }
}


impl UnivariateDifferentiableFunction for PolynomialFunctionNewtonForm {
    /**
    * {@inheritDoc}
    * @since 3.1
    */
    fn value_ds(&self, t: &DerivativeStructure) -> Result<DerivativeStructure, Rc<Exception>> {
        try!(PolynomialFunctionNewtonForm::verify_input_array(&self.a, &self.c));
        let n: usize = self.c.len();
        let mut value: DerivativeStructure =
            DerivativeStructure::new_parameters_order_value(t.get_free_parameters(),
                                                            t.get_order(),
                                                            self.a[n])
                .unwrap();
        {
            let mut i: usize = n - 1;
            loop {
                {
                    let tmp = try!(t.subtract_f64(self.c[i]));
                    let tmp2 = try!(tmp.multiply(&value));
                    value = try!(tmp2.add_f64(self.a[i]));
                }
                if i == 0 {
                    break;
                }
                i -= 1;
            }
        }

        return Ok(value);
    }
}

impl PolynomialFunctionNewtonForm {
    /**
     * Construct a Newton polynomial with the given a[] and c[]. The order of
     * centers are important in that if c[] shuffle, then values of a[] would
     * completely change, not just a permutation of old a[].
     * <p>
     * The constructor makes copy of the input arrays and assigns them.</p>
     *
     * @param a Coefficients in Newton form formula.
     * @param c Centers.
     * @throws NullArgumentException if any argument is {@code null}.
     * @throws NoDataException if any array has zero length.
     * @throws DimensionMismatchException if the size difference between
     * {@code a} and {@code c} is not equal to 1.
     */
    pub fn new(a: &Vec<f64>, c: &Vec<f64>) -> Result<PolynomialFunctionNewtonForm, Rc<Exception>> {
        try!(PolynomialFunctionNewtonForm::verify_input_array(a, c));
        Ok(PolynomialFunctionNewtonForm {
            coefficients: Vec::new(),
            c: c.clone(),
            a: a.clone(),
            coefficients_computed: false,
        })
    }


    /**
     * Returns the degree of the polynomial.
     *
     * @return the degree of the polynomial
     */
    pub fn degree(&self) -> usize {
        return self.c.len();
    }

    /**
     * Returns a copy of coefficients in Newton form formula.
     * <p>
     * Changes made to the returned copy will not affect the polynomial.</p>
     *
     * @return a fresh copy of coefficients in Newton form formula
     */
    pub fn get_newton_coefficients(&self) -> Vec<f64> {
        return self.a.clone();
    }

    /**
     * Returns a copy of the centers array.
     * <p>
     * Changes made to the returned copy will not affect the polynomial.</p>
     *
     * @return a fresh copy of the centers array.
     */
    pub fn get_centers(&self) -> Vec<f64> {
        return self.c.clone();
    }

    /**
     * Returns a copy of the coefficients array.
     * <p>
     * Changes made to the returned copy will not affect the polynomial.</p>
     *
     * @return a fresh copy of the coefficients array.
     */
    pub fn get_coefficients(&mut self) -> Vec<f64> {
        if !self.coefficients_computed {
            self.compute_coefficients();
        }
        return self.coefficients.clone();
    }

    /**
     * Evaluate the Newton polynomial using nested multiplication. It is
     * also called <a href="http://mathworld.wolfram.com/HornersRule.html">
     * Horner's Rule</a> and takes O(N) time.
     *
     * @param a Coefficients in Newton form formula.
     * @param c Centers.
     * @param z Point at which the function value is to be computed.
     * @return the function value.
     * @throws NullArgumentException if any argument is {@code null}.
     * @throws NoDataException if any array has zero length.
     * @throws DimensionMismatchException if the size difference between
     * {@code a} and {@code c} is not equal to 1.
     */
    pub fn evaluate(a: &Vec<f64>, c: &Vec<f64>, z: f64) -> Result<f64, Rc<Exception>> {
        try!(PolynomialFunctionNewtonForm::verify_input_array(a, c));
        let n: usize = c.len();
        let mut value: f64 = a[n];
        {
            let mut i: usize = n - 1;
            loop {
                {
                    value = a[i] + (z - c[i]) * value;
                }
                if i == 0 {
                    break;
                }
                i -= 1;
            }
        }

        return Ok(value);
    }

    /**
     * Calculate the normal polynomial coefficients given the Newton form.
     * It also uses nested multiplication but takes O(N^2) time.
     */
    pub fn compute_coefficients(&mut self) {
        let n: usize = self.degree();
        self.coefficients = Vec::new();
        {
            let mut i: usize = 0;
            while i <= n {
                {
                    self.coefficients.push(0.0);
                }
                i += 1;
            }
        }

        self.coefficients[0] = self.a[n];
        {
            let mut i: usize = n - 1;
            loop {
                {
                    {
                        let mut j: usize = n - i;
                        while j > 0 {
                            {
                                self.coefficients[j] = self.coefficients[j - 1] -
                                                       self.c[i] * self.coefficients[j];
                            }
                            j -= 1;
                        }
                    }

                    self.coefficients[0] = self.a[i] - self.c[i] * self.coefficients[0];
                }
                if i == 0 {
                    break;
                }
                i -= 1;
            }
        }

        self.coefficients_computed = true;
    }

    /**
     * Verifies that the input arrays are valid.
     * <p>
     * The centers must be distinct for interpolation purposes, but not
     * for general use. Thus it is not verified here.</p>
     *
     * @param a the coefficients in Newton form formula
     * @param c the centers
     * @throws NullArgumentException if any argument is {@code null}.
     * @throws NoDataException if any array has zero length.
     * @throws DimensionMismatchException if the size difference between
     * {@code a} and {@code c} is not equal to 1.
     * @see org.apache.commons.math3.analysis.interpolation.DividedDifferenceInterpolator#computeDividedDifference(double[],
     * double[])
     */
    pub fn verify_input_array(a: &Vec<f64>, c: &Vec<f64>) -> Result<Void, Rc<Exception>> {
        if a.len() == 0 || c.len() == 0 {
            return Err(Exc::new_msg("NoDataException",
                                    format!("empty polynomials coefficients array")));
        }
        if a.len() != c.len() + 1 {
            return Err(Exc::new_msg("DimensionMismatchException",
                                    format!("array sizes should have difference 1 ({0} != {1} \
                                             + 1)",
                                            a.len(),
                                            c.len())));
        }
        Ok(Void::new())
    }
}
