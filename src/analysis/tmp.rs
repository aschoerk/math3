                
                   /*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * Test for class {@link DerivativeStructure}.
 */
pub struct DerivativeStructureTest {
    super: ExtendedFieldElementAbstractTest<DerivativeStructure>;
}

impl DerivativeStructureTest {


    

   


   
    

   

   

   
    
    #[test]
    pub fn  test_compose_mismatched_dimensions()   {
        DerivativeStructure::new(1, 3, 0, 1.2).unwrap().compose(: [f64; 3] = [0.0; 3]).unwrap();
    }

    #[test]
    pub fn  test_compose()   {
         let epsilon  = vec![1.0e-20, 5.0e-14, 2.0e-13, 3.0e-13, 2.0e-13, 1.0e-20, ]
        ;
         let poly: PolynomialFunction = PolynomialFunction::new(  = vec![1.0, 2.0, 3.0, 4.0, 5.0, 6.0, ]
        );
         {
             let mut max_order: usize = 0;
            while max_order < 6 {
                {
                     let mut p: [Option<PolynomialFunction>; max_order + 1] = [None; max_order + 1];
                    p[0] = poly;
                     {
                         let mut i: i32 = 1;
                        while i <= max_order {
                            {
                                p[i] = p[i - 1].polynomial_derivative();
                            }
                            i += 1;
                         }
                     }

                     {
                         let mut x: f64 = 0.1;
                        while x < 1.2 {
                            {
                                 let ds_x: DerivativeStructure = DerivativeStructure::new(1, max_order, 0, x).unwrap();
                                 let ds_y1: DerivativeStructure = ds_x.get_field().get_zero();
                                 {
                                     let mut i: i32 = poly.degree();
                                    while i >= 0 {
                                        {
                                            ds_y1 = ds_y1.multiply(&ds_x).add(poly.get_coefficients()[i]);
                                        }
                                        i -= 1;
                                     }
                                 }

                                 let mut f: [f64; max_order + 1.0] = [0.0; max_order + 1.0];
                                 {
                                     let mut i: i32 = 0;
                                    while i < f.len() {
                                        {
                                            f[i] = p[i].value(x);
                                        }
                                        i += 1;
                                     }
                                 }

                                 let ds_y2: DerivativeStructure = ds_x.compose(&f);
                                 let zero: DerivativeStructure = ds_y1.subtract(&ds_y2);
                                 {
                                     let mut n: usize = 0;
                                    while n <= max_order {
                                        {
                                            assert_within_delta!(0.0, zero.get_partial_derivative(&vec![n as i32]).unwrap(), epsilon[n]);
                                        }
                                        n += 1;
                                     }
                                 }

                            }
                            x += 0.001;
                         }
                     }

                }
                max_order += 1;
             }
         }

    }

    #[test]
    pub fn  test_field()   {
         {
             let mut max_order: usize = 1;
            while max_order < 5 {
                {
                     let x: DerivativeStructure = DerivativeStructure::new(3, max_order, 0, 1.0).unwrap();
                    check_f0_f1(&x.get_field().get_zero(), 0.0, 0.0, 0.0, 0.0);
                    check_f0_f1(&x.get_field().get_one(), 1.0, 0.0, 0.0, 0.0);
                    assert_within_delta!(max_order, x.get_field().get_zero().get_order());
                    assert_within_delta!(3.0, x.get_field().get_zero().get_free_parameters());
                    assert_within_delta!(DerivativeStructure.class, x.get_field().get_runtime_class());
                }
                max_order += 1;
             }
         }

    }

    #[test]
    pub fn  test_one_parameter_constructor()   {
         let x: f64 = 1.2;
         let cos: f64 = F64::cos(x);
         let sin: f64 = F64::sin(x);
         let y_ref: DerivativeStructure = DerivativeStructure::new(1, 4, 0, x).unwrap().cos().unwrap();
        let tryResult1 = 0;
        'try1: loop {
        {
            DerivativeStructure::new(1, 4, 0, 0.0).unwrap();
            Assert::fail("an exception should have been thrown");
        }
        break 'try1
        }
        match tryResult1 {
             catch ( dme: &DimensionMismatchException) {
            } catch ( e: &Exception) {
                Assert::fail(format!("wrong exceptionc caught {}", e.get_class().get_name()));
            }  0 => break
        }

         let derivatives  = vec![cos, -sin, -cos, sin, cos, ]
        ;
         let y: DerivativeStructure = DerivativeStructure::new(1.0, 4.0, &derivatives).unwrap();
        check_equals(&y_ref, &y, 1.0e-15);
        TestUtils::assert_equals(&derivatives, &y.get_all_derivatives(), 1.0e-15);
    }

    #[test]
    pub fn  test_one_order_constructor()   {
         let x: f64 = 1.2;
         let y: f64 = 2.4;
         let z: f64 = 12.5;
         let x_ref: DerivativeStructure = DerivativeStructure::new(3, 1, 0, x).unwrap();
         let y_ref: DerivativeStructure = DerivativeStructure::new(3, 1, 1, y).unwrap();
         let z_ref: DerivativeStructure = DerivativeStructure::new(3, 1, 2, z).unwrap();
        let tryResult1 = 0;
        'try1: loop {
        {
            DerivativeStructure::new(3.0, 1.0, x + y - z, 1.0, 1.0).unwrap();
            Assert::fail("an exception should have been thrown");
        }
        break 'try1
        }
        match tryResult1 {
             catch ( dme: &DimensionMismatchException) {
            } catch ( e: &Exception) {
                Assert::fail(format!("wrong exceptionc caught {}", e.get_class().get_name()));
            }  0 => break
        }

         let derivatives  = vec![x + y - z, 1.0, 1.0, -1.0, ]
        ;
         let t: DerivativeStructure = DerivativeStructure::new(3.0, 1.0, &derivatives).unwrap();
        check_equals(&x_ref.add(&y_ref.subtract(&z_ref)), &t, 1.0e-15);
        TestUtils::assert_equals(&derivatives, &x_ref.add(&y_ref.subtract(&z_ref)).get_all_derivatives(), 1.0e-15);
    }

    #[test]
    pub fn  test_linear_combination1_d_s_d_s()   {
         let a : vec![DerivativeStructure; 3] = vec![DerivativeStructure::new(6, 1, 0, -1321008684645961.0 / 268435456.0).unwrap(), DerivativeStructure::new(6, 1, 1, -5774608829631843.0 / 268435456.0).unwrap(), DerivativeStructure::new(6, 1, 2, -7645843051051357.0 / 8589934592.0).unwrap(), ]
        ;
         let b : vec![DerivativeStructure; 3] = vec![DerivativeStructure::new(6, 1, 3, -5712344449280879.0 / 2097152.0).unwrap(), DerivativeStructure::new(6, 1, 4, -4550117129121957.0 / 2097152.0).unwrap(), DerivativeStructure::new(6, 1, 5, 8846951984510141.0 / 131072.0).unwrap(), ]
        ;
         let ab_sum_inline: DerivativeStructure = a[0].linear_combination(a[0], b[0], a[1], b[1], a[2], b[2]);
         let ab_sum_array: DerivativeStructure = a[0].linear_combination(&a, &b);
        assert_within_delta!(ab_sum_inline.get_value(), ab_sum_array.get_value(), 0);
        assert_within_delta!(-1.8551294182586248737720779899, ab_sum_inline.get_value(), 1.0e-15);
        assert_within_delta!(b[0].get_value(), ab_sum_inline.get_partial_derivative(&vec![1, 0, 0, 0, 0, 0]).unwrap(), 1.0e-15);
        assert_within_delta!(b[1].get_value(), ab_sum_inline.get_partial_derivative(&vec![0, 1, 0, 0, 0, 0]).unwrap(), 1.0e-15);
        assert_within_delta!(b[2].get_value(), ab_sum_inline.get_partial_derivative(&vec![0, 0, 1, 0, 0, 0]).unwrap(), 1.0e-15);
        assert_within_delta!(a[0].get_value(), ab_sum_inline.get_partial_derivative(&vec![0, 0, 0, 1, 0, 0]).unwrap(), 1.0e-15);
        assert_within_delta!(a[1].get_value(), ab_sum_inline.get_partial_derivative(&vec![0, 0, 0, 0, 1, 0]).unwrap(), 1.0e-15);
        assert_within_delta!(a[2].get_value(), ab_sum_inline.get_partial_derivative(&vec![0, 0, 0, 0, 0, 1]).unwrap(), 1.0e-15);
    }

    #[test]
    pub fn  test_linear_combination1_double_d_s()   {
         let a  = vec![-1321008684645961.0 / 268435456.0, -5774608829631843.0 / 268435456.0, -7645843051051357.0 / 8589934592.0, ]
        ;
         let b : vec![DerivativeStructure; 3] = vec![DerivativeStructure::new(3, 1, 0, -5712344449280879.0 / 2097152.0).unwrap(), DerivativeStructure::new(3, 1, 1, -4550117129121957.0 / 2097152.0).unwrap(), DerivativeStructure::new(3, 1, 2, 8846951984510141.0 / 131072.0).unwrap(), ]
        ;
         let ab_sum_inline: DerivativeStructure = b[0].linear_combination(a[0], b[0], a[1], b[1], a[2], b[2]);
         let ab_sum_array: DerivativeStructure = b[0].linear_combination(&a, &b);
        assert_within_delta!(ab_sum_inline.get_value(), ab_sum_array.get_value(), 0);
        assert_within_delta!(-1.8551294182586248737720779899, ab_sum_inline.get_value(), 1.0e-15);
        assert_within_delta!(a[0], ab_sum_inline.get_partial_derivative(&vec![1, 0, 0]).unwrap(), 1.0e-15);
        assert_within_delta!(a[1], ab_sum_inline.get_partial_derivative(&vec![0, 1, 0]).unwrap(), 1.0e-15);
        assert_within_delta!(a[2], ab_sum_inline.get_partial_derivative(&vec![0, 0, 1]).unwrap(), 1.0e-15);
    }

    #[test]
    pub fn  test_linear_combination2_d_s_d_s()   {
        // we compare accurate versus naive dot product implementations
        // on regular vectors (i.e. not extreme cases like in the previous test)
         let random: Well1024a = Well1024a::new(0xc6af886975069f11);
         {
             let mut i: i32 = 0;
            while i < 10000 {
                {
                     let mut u: [Option<DerivativeStructure>; 4] = [None; 4];
                     let mut v: [Option<DerivativeStructure>; 4] = [None; 4];
                     {
                         let mut j: i32 = 0;
                        while j < u.len() {
                            {
                                u[j] = DerivativeStructure::new(u.len(), 1.0, j, 1e17 * random.next_double()).unwrap();
                                v[j] = DerivativeStructure::new(u.len(), 1.0, 1e17 * random.next_double()).unwrap();
                            }
                            j += 1;
                         }
                     }

                     let mut lin: DerivativeStructure = u[0].linear_combination(u[0], v[0], u[1], v[1]);
                     let mut ref: f64 = u[0].get_value() * v[0].get_value() + u[1].get_value() * v[1].get_value();
                    assert_within_delta!(ref, lin.get_value(), 1.0e-15 * F64::abs(ref));
                    assert_within_delta!(v[0].get_value(), lin.get_partial_derivative(&vec![1, 0, 0, 0]).unwrap(), 1.0e-15 * F64::abs(&v[0].get_value()));
                    assert_within_delta!(v[1].get_value(), lin.get_partial_derivative(&vec![0, 1, 0, 0]).unwrap(), 1.0e-15 * F64::abs(&v[1].get_value()));
                    lin = u[0].linear_combination(u[0], v[0], u[1], v[1], u[2], v[2]);
                    ref = u[0].get_value() * v[0].get_value() + u[1].get_value() * v[1].get_value() + u[2].get_value() * v[2].get_value();
                    assert_within_delta!(ref, lin.get_value(), 1.0e-15 * F64::abs(ref));
                    assert_within_delta!(v[0].get_value(), lin.get_partial_derivative(&vec![1, 0, 0, 0]).unwrap(), 1.0e-15 * F64::abs(&v[0].get_value()));
                    assert_within_delta!(v[1].get_value(), lin.get_partial_derivative(&vec![0, 1, 0, 0]).unwrap(), 1.0e-15 * F64::abs(&v[1].get_value()));
                    assert_within_delta!(v[2].get_value(), lin.get_partial_derivative(&vec![0, 0, 1, 0]).unwrap(), 1.0e-15 * F64::abs(&v[2].get_value()));
                    lin = u[0].linear_combination(u[0], v[0], u[1], v[1], u[2], v[2], u[3], v[3]);
                    ref = u[0].get_value() * v[0].get_value() + u[1].get_value() * v[1].get_value() + u[2].get_value() * v[2].get_value() + u[3].get_value() * v[3].get_value();
                    assert_within_delta!(ref, lin.get_value(), 1.0e-15 * F64::abs(ref));
                    assert_within_delta!(v[0].get_value(), lin.get_partial_derivative(&vec![1, 0, 0, 0]).unwrap(), 1.0e-15 * F64::abs(&v[0].get_value()));
                    assert_within_delta!(v[1].get_value(), lin.get_partial_derivative(&vec![0, 1, 0, 0]).unwrap(), 1.0e-15 * F64::abs(&v[1].get_value()));
                    assert_within_delta!(v[2].get_value(), lin.get_partial_derivative(&vec![0, 0, 1, 0]).unwrap(), 1.0e-15 * F64::abs(&v[2].get_value()));
                    assert_within_delta!(v[3].get_value(), lin.get_partial_derivative(&vec![0, 0, 0, 1]).unwrap(), 1.0e-15 * F64::abs(&v[3].get_value()));
                }
                i += 1;
             }
         }

    }

    #[test]
    pub fn  test_linear_combination2_double_d_s()   {
        // we compare accurate versus naive dot product implementations
        // on regular vectors (i.e. not extreme cases like in the previous test)
         let random: Well1024a = Well1024a::new(0xc6af886975069f11);
         {
             let mut i: i32 = 0;
            while i < 10000 {
                {
                     let mut u: [f64; 4.0] = [0.0; 4.0];
                     let mut v: [Option<DerivativeStructure>; 4] = [None; 4];
                     {
                         let mut j: i32 = 0;
                        while j < u.len() {
                            {
                                u[j] = 1e17 * random.next_double();
                                v[j] = DerivativeStructure::new(u.len(), 1.0, j, 1e17 * random.next_double()).unwrap();
                            }
                            j += 1;
                         }
                     }

                     let mut lin: DerivativeStructure = v[0].linear_combination(u[0], v[0], u[1], v[1]);
                     let mut ref: f64 = u[0] * v[0].get_value() + u[1] * v[1].get_value();
                    assert_within_delta!(ref, lin.get_value(), 1.0e-15 * F64::abs(ref));
                    assert_within_delta!(u[0], lin.get_partial_derivative(&vec![1, 0, 0, 0]).unwrap(), 1.0e-15 * F64::abs(&v[0].get_value()));
                    assert_within_delta!(u[1], lin.get_partial_derivative(&vec![0, 1, 0, 0]).unwrap(), 1.0e-15 * F64::abs(&v[1].get_value()));
                    lin = v[0].linear_combination(u[0], v[0], u[1], v[1], u[2], v[2]);
                    ref = u[0] * v[0].get_value() + u[1] * v[1].get_value() + u[2] * v[2].get_value();
                    assert_within_delta!(ref, lin.get_value(), 1.0e-15 * F64::abs(ref));
                    assert_within_delta!(u[0], lin.get_partial_derivative(&vec![1, 0, 0, 0]).unwrap(), 1.0e-15 * F64::abs(&v[0].get_value()));
                    assert_within_delta!(u[1], lin.get_partial_derivative(&vec![0, 1, 0, 0]).unwrap(), 1.0e-15 * F64::abs(&v[1].get_value()));
                    assert_within_delta!(u[2], lin.get_partial_derivative(&vec![0, 0, 1, 0]).unwrap(), 1.0e-15 * F64::abs(&v[2].get_value()));
                    lin = v[0].linear_combination(u[0], v[0], u[1], v[1], u[2], v[2], u[3], v[3]);
                    ref = u[0] * v[0].get_value() + u[1] * v[1].get_value() + u[2] * v[2].get_value() + u[3] * v[3].get_value();
                    assert_within_delta!(ref, lin.get_value(), 1.0e-15 * F64::abs(ref));
                    assert_within_delta!(u[0], lin.get_partial_derivative(&vec![1, 0, 0, 0]).unwrap(), 1.0e-15 * F64::abs(&v[0].get_value()));
                    assert_within_delta!(u[1], lin.get_partial_derivative(&vec![0, 1, 0, 0]).unwrap(), 1.0e-15 * F64::abs(&v[1].get_value()));
                    assert_within_delta!(u[2], lin.get_partial_derivative(&vec![0, 0, 1, 0]).unwrap(), 1.0e-15 * F64::abs(&v[2].get_value()));
                    assert_within_delta!(u[3], lin.get_partial_derivative(&vec![0, 0, 0, 1]).unwrap(), 1.0e-15 * F64::abs(&v[3].get_value()));
                }
                i += 1;
             }
         }

    }

    #[test]
    pub fn  test_serialization()   {
         let a: DerivativeStructure = DerivativeStructure::new(3, 2, 0, 1.3).unwrap();
         let b: DerivativeStructure = TestUtils::serialize_and_recover(&a) as DerivativeStructure;
        assert_within_delta!(a.get_free_parameters(), b.get_free_parameters());
        assert_within_delta!(a.get_order(), b.get_order());
        check_equals(&a, &b, 1.0e-15);
    }

    
    
}


                
                