                
                   /*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * Utilities for manipulating function objects.
 *
 * @since 3.0
 */
pub struct FunctionUtils {
}

impl FunctionUtils {

    /**
     * Class only contains static methods.
     */
    fn new() -> FunctionUtils {
    }

   
   
    /**
     * Composes functions.
     * <p>
     * The functions in the argument list are composed sequentially, in the
     * given order.  For example, compose(f1,f2,f3) acts like f1(f2(f3(x))).</p>
     *
     * @param f List of functions.
     * @return the composite function.
     * @deprecated as of 3.1 replaced by {@link #compose(UnivariateDifferentiableFunction...)}
     */
    pub fn  compose( f: &DifferentiableUnivariateFunction) -> DifferentiableUnivariateFunction  {
        return DifferentiableUnivariateFunction::new() {

            pub fn  value(&self,  x: f64) -> f64  {
                 let mut r: f64 = x;
                 {
                     let mut i: i32 = f.len() - 1;
                    while i >= 0 {
                        {
                            r = f[i].value(r);
                        }
                        i -= 1;
                     }
                 }

                return Ok(r);
            }

            pub fn  derivative(&self) -> UnivariateFunction  {
                return UnivariateFunction::new() {

                    pub fn  value(&self,  x: f64) -> f64  {
                         let mut p: f64 = 1;
                         let mut r: f64 = x;
                         {
                             let mut i: i32 = f.len() - 1;
                            while i >= 0 {
                                {
                                    p *= f[i].derivative().value(r);
                                    r = f[i].value(r);
                                }
                                i -= 1;
                             }
                         }

                        return Ok(p);
                    }
                };
            }
        };
    }

   
   
    /**
     * Adds functions.
     *
     * @param f List of functions.
     * @return a function that computes the sum of the functions.
     * @deprecated as of 3.1 replaced by {@link #add(UnivariateDifferentiableFunction...)}
     */
    pub fn  add( f: &DifferentiableUnivariateFunction) -> DifferentiableUnivariateFunction  {
        return DifferentiableUnivariateFunction::new() {

            pub fn  value(&self,  x: f64) -> f64  {
                 let mut r: f64 = f[0].value(x);
                 {
                     let mut i: i32 = 1;
                    while i < f.len() {
                        {
                            r += f[i].value(x);
                        }
                        i += 1;
                     }
                 }

                return Ok(r);
            }

            pub fn  derivative(&self) -> UnivariateFunction  {
                return UnivariateFunction::new() {

                    pub fn  value(&self,  x: f64) -> f64  {
                         let mut r: f64 = f[0].derivative().value(x);
                         {
                             let mut i: i32 = 1;
                            while i < f.len() {
                                {
                                    r += f[i].derivative().value(x);
                                }
                                i += 1;
                             }
                         }

                        return Ok(r);
                    }
                };
            }
        };
    }

   
    /**
     * Multiplies functions.
     *
     * @param f List of functions.
     * @return a function that computes the product of the functions.
     * @deprecated as of 3.1 replaced by {@link #multiply(UnivariateDifferentiableFunction...)}
     */
    pub fn  multiply( f: &DifferentiableUnivariateFunction) -> DifferentiableUnivariateFunction  {
        return DifferentiableUnivariateFunction::new() {

            pub fn  value(&self,  x: f64) -> f64  {
                 let mut r: f64 = f[0].value(x);
                 {
                     let mut i: i32 = 1;
                    while i < f.len() {
                        {
                            r *= f[i].value(x);
                        }
                        i += 1;
                     }
                 }

                return Ok(r);
            }

            pub fn  derivative(&self) -> UnivariateFunction  {
                return UnivariateFunction::new() {

                    pub fn  value(&self,  x: f64) -> f64  {
                         let mut sum: f64 = 0;
                         {
                             let mut i: i32 = 0;
                            while i < f.len() {
                                {
                                     let mut prod: f64 = f[i].derivative().value(x);
                                     {
                                         let mut j: i32 = 0;
                                        while j < f.len() {
                                            {
                                                if i != j {
                                                    prod *= f[j].value(x);
                                                }
                                            }
                                            j += 1;
                                         }
                                     }

                                    sum += prod;
                                }
                                i += 1;
                             }
                         }

                        return Ok(sum);
                    }
                };
            }
        };
    }



    
    /**
     * Convert a {@link DifferentiableUnivariateFunction} into a {@link UnivariateDifferentiableFunction}.
     * <p>
     * Note that the converted function is able to handle {@link DerivativeStructure} up to order one.
     * If the function is called with higher order, a {@link NumberIsTooLargeException} is thrown.
     * </p>
     * @param f function to convert
     * @return converted function
     * @deprecated this conversion method is temporary in version 3.1, as the {@link
     * DifferentiableUnivariateFunction} interface itself is deprecated
     */
    pub fn  to_univariate_differential( f: &DifferentiableUnivariateFunction) -> UnivariateDifferentiableFunction  {
        return UnivariateDifferentiableFunction::new() {

            pub fn  value(&self,  x: f64) -> f64  {
                return Ok(f.value(x));
            }

            /** {@inheritDoc}
             * @exception NumberIsTooLargeException if derivation order is greater than 1
             */
            pub fn  value(&self,  t: &DerivativeStructure) -> /*  throws NumberIsTooLargeException */Result<DerivativeStructure, Rc<Exception>>   {
                match t.get_order() {
                      0 => 
                         {
                            return Ok(DerivativeStructure::new(&t.get_free_parameters(), 0, &f.value(&t.get_value())));
                        }
                      1 => 
                         {
                            {
                                 let parameters: i32 = t.get_free_parameters();
                                 let mut derivatives: [f64; parameters + 1.0] = [0.0; parameters + 1.0];
                                derivatives[0] = f.value(&t.get_value());
                                 let f_prime: f64 = f.derivative().value(&t.get_value());
                                 let mut orders: [i32; parameters] = [0; parameters];
                                 {
                                     let mut i: i32 = 0;
                                    while i < parameters {
                                        {
                                            orders[i] = 1;
                                            derivatives[i + 1] = f_prime * t.get_partial_derivative(&orders);
                                            orders[i] = 0;
                                        }
                                        i += 1;
                                     }
                                 }

                                return Ok(DerivativeStructure::new(parameters, 1.0, &derivatives));
                            }
                        }
                    _ => 
                         {
                            throw NumberIsTooLargeException::new(&t.get_order(), 1, true);
                        }
                }
            }
        };
    }

    /**
     * Convert a {@link MultivariateDifferentiableFunction} into a {@link DifferentiableMultivariateFunction}.
     *
     * @param f function to convert
     * @return converted function
     * @deprecated this conversion method is temporary in version 3.1, as the {@link
     * DifferentiableMultivariateFunction} interface itself is deprecated
     */
    pub fn  to_differentiable_multivariate_function( f: &MultivariateDifferentiableFunction) -> DifferentiableMultivariateFunction  {
        return DifferentiableMultivariateFunction::new() {

            pub fn  value(&self,  x: &Vec<f64>) -> f64  {
                return Ok(f.value(&x));
            }

            pub fn  partial_derivative(&self,  k: i32) -> MultivariateFunction  {
                return MultivariateFunction::new() {

                    pub fn  value(&self,  x: &Vec<f64>) -> f64  {
                         let n: i32 = x.len();
                        // delegate computation to underlying function
                         let ds_x: [Option<DerivativeStructure>; n] = [None; n];
                         {
                             let mut i: i32 = 0;
                            while i < n {
                                {
                                    if i == k {
                                        ds_x[i] = DerivativeStructure::new(1.0, 1.0, 0.0, x[i]);
                                    } else {
                                        ds_x[i] = DerivativeStructure::new(1.0, 1.0, x[i]);
                                    }
                                }
                                i += 1;
                             }
                         }

                         let y: DerivativeStructure = f.value(&ds_x);
                        // extract partial derivative
                        return Ok(y.get_partial_derivative(1));
                    }
                };
            }

            pub fn  gradient(&self) -> MultivariateVectorFunction  {
                return MultivariateVectorFunction::new() {

                    pub fn  value(&self,  x: &Vec<f64>) -> Vec<f64>  {
                         let n: i32 = x.len();
                        // delegate computation to underlying function
                         let ds_x: [Option<DerivativeStructure>; n] = [None; n];
                         {
                             let mut i: i32 = 0;
                            while i < n {
                                {
                                    ds_x[i] = DerivativeStructure::new(n, 1.0, i, x[i]);
                                }
                                i += 1;
                             }
                         }

                         let y: DerivativeStructure = f.value(&ds_x);
                        // extract gradient
                         let mut gradient: [f64; n] = [0.0; n];
                         let mut orders: [i32; n] = [0; n];
                         {
                             let mut i: i32 = 0;
                            while i < n {
                                {
                                    orders[i] = 1;
                                    gradient[i] = y.get_partial_derivative(&orders);
                                    orders[i] = 0;
                                }
                                i += 1;
                             }
                         }

                        return Ok(gradient);
                    }
                };
            }
        };
    }

    /**
     * Convert a {@link DifferentiableMultivariateFunction} into a {@link MultivariateDifferentiableFunction}.
     * <p>
     * Note that the converted function is able to handle {@link DerivativeStructure} elements
     * that all have the same number of free parameters and order, and with order at most 1.
     * If the function is called with inconsistent numbers of free parameters or higher order, a
     * {@link DimensionMismatchException} or a {@link NumberIsTooLargeException} will be thrown.
     * </p>
     * @param f function to convert
     * @return converted function
     * @deprecated this conversion method is temporary in version 3.1, as the {@link
     * DifferentiableMultivariateFunction} interface itself is deprecated
     */
    pub fn  to_multivariate_differentiable_function( f: &DifferentiableMultivariateFunction) -> MultivariateDifferentiableFunction  {
        return MultivariateDifferentiableFunction::new() {

            pub fn  value(&self,  x: &Vec<f64>) -> f64  {
                return Ok(f.value(&x));
            }

            /** {@inheritDoc}
             * @exception NumberIsTooLargeException if derivation order is higher than 1
             * @exception DimensionMismatchException if numbers of free parameters are inconsistent
             */
            pub fn  value(&self,  t: &Vec<DerivativeStructure>) -> /*  throws DimensionMismatchException, NumberIsTooLargeException */Result<DerivativeStructure, Rc<Exception>>   {
                // check parameters and orders limits
                 let parameters: i32 = t[0].get_free_parameters();
                 let order: i32 = t[0].get_order();
                 let n: i32 = t.len();
                if order > 1 {
                    throw NumberIsTooLargeException::new(order, 1, true);
                }
                // check all elements in the array are consistent
                 {
                     let mut i: i32 = 0;
                    while i < n {
                        {
                            if t[i].get_free_parameters() != parameters {
                                throw DimensionMismatchException::new(&t[i].get_free_parameters(), parameters);
                            }
                            if t[i].get_order() != order {
                                throw DimensionMismatchException::new(&t[i].get_order(), order);
                            }
                        }
                        i += 1;
                     }
                 }

                // delegate computation to underlying function
                 let mut point: [f64; n] = [0.0; n];
                 {
                     let mut i: i32 = 0;
                    while i < n {
                        {
                            point[i] = t[i].get_value();
                        }
                        i += 1;
                     }
                 }

                 let value: f64 = f.value(&point);
                 let gradient: Vec<f64> = f.gradient().value(&point);
                // merge value and gradient into one DerivativeStructure
                 let mut derivatives: [f64; parameters + 1.0] = [0.0; parameters + 1.0];
                derivatives[0] = value;
                 let mut orders: [i32; parameters] = [0; parameters];
                 {
                     let mut i: i32 = 0;
                    while i < parameters {
                        {
                            orders[i] = 1;
                             {
                                 let mut j: i32 = 0;
                                while j < n {
                                    {
                                        derivatives[i + 1] += gradient[j] * t[j].get_partial_derivative(&orders);
                                    }
                                    j += 1;
                                 }
                             }

                            orders[i] = 0;
                        }
                        i += 1;
                     }
                 }

                return Ok(DerivativeStructure::new(parameters, order, &derivatives));
            }
        };
    }

    /**
     * Convert a {@link MultivariateDifferentiableVectorFunction} into a {@link DifferentiableMultivariateVectorFunction}.
     *
     * @param f function to convert
     * @return converted function
     * @deprecated this conversion method is temporary in version 3.1, as the {@link
     * DifferentiableMultivariateVectorFunction} interface itself is deprecated
     */
    pub fn  to_differentiable_multivariate_vector_function( f: &MultivariateDifferentiableVectorFunction) -> DifferentiableMultivariateVectorFunction  {
        return DifferentiableMultivariateVectorFunction::new() {

            pub fn  value(&self,  x: &Vec<f64>) -> Vec<f64>  {
                return Ok(f.value(&x));
            }

            pub fn  jacobian(&self) -> MultivariateMatrixFunction  {
                return MultivariateMatrixFunction::new() {

                    pub fn  value(&self,  x: &Vec<f64>) -> Vec<Vec<f64>>  {
                         let n: i32 = x.len();
                        // delegate computation to underlying function
                         let ds_x: [Option<DerivativeStructure>; n] = [None; n];
                         {
                             let mut i: i32 = 0;
                            while i < n {
                                {
                                    ds_x[i] = DerivativeStructure::new(n, 1.0, i, x[i]);
                                }
                                i += 1;
                             }
                         }

                         let y: Vec<DerivativeStructure> = f.value(&ds_x);
                        // extract Jacobian
                         let mut jacobian: [[f64; n]; y.len()] = [[0.0; n]; y.len()];
                         let mut orders: [i32; n] = [0; n];
                         {
                             let mut i: i32 = 0;
                            while i < y.len() {
                                {
                                     {
                                         let mut j: i32 = 0;
                                        while j < n {
                                            {
                                                orders[j] = 1;
                                                jacobian[i][j] = y[i].get_partial_derivative(&orders);
                                                orders[j] = 0;
                                            }
                                            j += 1;
                                         }
                                     }

                                }
                                i += 1;
                             }
                         }

                        return Ok(jacobian);
                    }
                };
            }
        };
    }

    /**
     * Convert a {@link DifferentiableMultivariateVectorFunction} into a {@link MultivariateDifferentiableVectorFunction}.
     * <p>
     * Note that the converted function is able to handle {@link DerivativeStructure} elements
     * that all have the same number of free parameters and order, and with order at most 1.
     * If the function is called with inconsistent numbers of free parameters or higher order, a
     * {@link DimensionMismatchException} or a {@link NumberIsTooLargeException} will be thrown.
     * </p>
     * @param f function to convert
     * @return converted function
     * @deprecated this conversion method is temporary in version 3.1, as the {@link
     * DifferentiableMultivariateFunction} interface itself is deprecated
     */
    pub fn  to_multivariate_differentiable_vector_function( f: &DifferentiableMultivariateVectorFunction) -> MultivariateDifferentiableVectorFunction  {
        return MultivariateDifferentiableVectorFunction::new() {

            pub fn  value(&self,  x: &Vec<f64>) -> Vec<f64>  {
                return Ok(f.value(&x));
            }

            pub fn  value(&self,  t: &Vec<DerivativeStructure>) -> /*  throws DimensionMismatchException, NumberIsTooLargeException */Result<Vec<DerivativeStructure>, Rc<Exception>>   {
                // check parameters and orders limits
                 let parameters: i32 = t[0].get_free_parameters();
                 let order: i32 = t[0].get_order();
                 let n: i32 = t.len();
                if order > 1 {
                    throw NumberIsTooLargeException::new(order, 1, true);
                }
                // check all elements in the array are consistent
                 {
                     let mut i: i32 = 0;
                    while i < n {
                        {
                            if t[i].get_free_parameters() != parameters {
                                throw DimensionMismatchException::new(&t[i].get_free_parameters(), parameters);
                            }
                            if t[i].get_order() != order {
                                throw DimensionMismatchException::new(&t[i].get_order(), order);
                            }
                        }
                        i += 1;
                     }
                 }

                // delegate computation to underlying function
                 let mut point: [f64; n] = [0.0; n];
                 {
                     let mut i: i32 = 0;
                    while i < n {
                        {
                            point[i] = t[i].get_value();
                        }
                        i += 1;
                     }
                 }

                 let value: Vec<f64> = f.value(&point);
                 let jacobian: Vec<Vec<f64>> = f.jacobian().value(&point);
                // merge value and Jacobian into a DerivativeStructure array
                 let mut merged: [Option<DerivativeStructure>; value.len()] = [None; value.len()];
                 {
                     let mut k: i32 = 0;
                    while k < merged.len() {
                        {
                             let mut derivatives: [f64; parameters + 1.0] = [0.0; parameters + 1.0];
                            derivatives[0] = value[k];
                             let mut orders: [i32; parameters] = [0; parameters];
                             {
                                 let mut i: i32 = 0;
                                while i < parameters {
                                    {
                                        orders[i] = 1;
                                         {
                                             let mut j: i32 = 0;
                                            while j < n {
                                                {
                                                    derivatives[i + 1] += jacobian[k][j] * t[j].get_partial_derivative(&orders);
                                                }
                                                j += 1;
                                             }
                                         }

                                        orders[i] = 0;
                                    }
                                    i += 1;
                                 }
                             }

                            merged[k] = DerivativeStructure::new(parameters, order, &derivatives);
                        }
                        k += 1;
                     }
                 }

                return Ok(merged);
            }
        };
    }
}


                
                