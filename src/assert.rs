use std::ops::Sub;
use std::cmp::Ordering;


pub struct Assert {
	
}

impl Assert {
    pub fn differs_within_delta<T: PartialOrd + Sub<Output = T>>(expected: T,
                                                                 actual: T,
                                                                 delta: T)
                                                                 -> bool {
        let cmp_res = expected.partial_cmp(&actual);
        match cmp_res {
            Some(Ordering::Equal) => true,
            Some(Ordering::Less) => delta >= (actual - expected),
            Some(Ordering::Greater) => delta >= (expected - actual),
            _ => false,
        }
    }
    pub fn differs<T: PartialOrd + Sub<Output = T>>(expected: T, actual: T) -> bool {
        let cmp_res = expected.partial_cmp(&actual);
        match cmp_res {
            Some(Ordering::Equal) => true,
            _ => false,
        }
    }

    pub fn assert_equals(comment: String, term1: bool, term2: bool) {
        if term1 != term2 {
            panic!(comment.clone());
        }
    }
}



#[macro_export]
macro_rules! assert_within_delta {
	($a:expr, $b:expr, $d:expr) => ({
			let (a, b, d) = (&$a, &$b, &$d);
            if !assert::Assert::differs_within_delta(*a, *b, *d) {
				panic!("{} differs from {} by more than {}", *a, *b, *d)
            }			
	});
	($a:expr, $b:expr, $d:expr, $($arg:tt)+) => ({
		let (a, b, d) = (&$a, &$b, &$d);
        if !assert::Assert::differs_within_delta(*a, *b, *d) {
            panic!($($arg)+)
        }
	});
	($a:expr, $b:expr) => (
		let (a, b) = (&$a, &$b);
        if !assert::Assert::differs(*a, *b) {
            panic!("{} differs from {}", *a, *b)
        }
    );
}
