                
                   /*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org::apache::commons::math3::complex;

use java::util::List;
use org::apache::commons::math3::TestUtils;
use org::apache::commons::math3::exception::NullArgumentException;
use org::apache::commons::math3::util::FastMath;
use org::junit::Assert;
use org::junit::Test;

/**
 */
pub struct ComplexTest {

     let inf: f64 = f64::INFINITY;

     let neginf: f64 = f64::NEG_INFINITY;

     let nan: f64 = f64::NAN;

     let pi: f64 = f64::consts::PI;

     let one_inf: Complex = Complex::new(1, &f64::INFINITY);

     let one_neg_inf: Complex = Complex::new(1, &f64::NEG_INFINITY);

     let inf_one: Complex = Complex::new(&f64::INFINITY, 1);

     let inf_zero: Complex = Complex::new(&f64::INFINITY, 0);

     let inf_na_n: Complex = Complex::new(&f64::INFINITY, &self.nan);

     let inf_neg_inf: Complex = Complex::new(&f64::INFINITY, &f64::NEG_INFINITY);

     let inf_inf: Complex = Complex::new(&f64::INFINITY, &f64::INFINITY);

     let neg_inf_inf: Complex = Complex::new(&f64::NEG_INFINITY, &f64::INFINITY);

     let neg_inf_zero: Complex = Complex::new(&f64::NEG_INFINITY, 0);

     let neg_inf_one: Complex = Complex::new(&f64::NEG_INFINITY, 1);

     let neg_inf_na_n: Complex = Complex::new(&f64::NEG_INFINITY, &self.nan);

     let neg_inf_neg_inf: Complex = Complex::new(&f64::NEG_INFINITY, &f64::NEG_INFINITY);

     let one_na_n: Complex = Complex::new(1, &self.nan);

     let zero_inf: Complex = Complex::new(0, &f64::INFINITY);

     let zero_na_n: Complex = Complex::new(0, &self.nan);

     let nan_inf: Complex = Complex::new(&self.nan, &f64::INFINITY);

     let nan_neg_inf: Complex = Complex::new(&self.nan, &f64::NEG_INFINITY);

     let nan_zero: Complex = Complex::new(&self.nan, 0);
}

impl ComplexTest {




    

   

   
    #[test]
    pub fn  test_equals_null()  {
         let x: Complex = Complex::new(3.0, 4.0);
        assert!(!&x.equals(null));
    }

    #[test]
    pub fn  test_floating_point_equals_precondition1()  {
        Complex::equals(Complex::new(3.0, 4.0), null, 3);
    }

    #[test]
    pub fn  test_floating_point_equals_precondition2()  {
        Complex::equals(null, Complex::new(3.0, 4.0), 3);
    }

    #[test]
    pub fn  test_equals_class()  {
         let x: Complex = Complex::new(3.0, 4.0);
        assert!(!&x.equals(self));
    }


    #[test]
    pub fn  test_polar2_complex(&self)   {
        TestUtils.assert_equals(Complex::ONE, &Complex::polar2_complex(1, 0), 10e-12);
        TestUtils.assert_equals(Complex::ZERO, &Complex::polar2_complex(0, 1), 10e-12);
        TestUtils.assert_equals(Complex::ZERO, &Complex::polar2_complex(0, -1), 10e-12);
        TestUtils.assert_equals(Complex::I, &Complex::polar2_complex(1, pi / 2), 10e-12);
        TestUtils.assert_equals(&Complex::I.negate(), &Complex::polar2_complex(1, -pi / 2), 10e-12);
         let mut r: f64 = 0;
         {
             let mut i: i32 = 0;
            while i < 5 {
                {
                    r += i;
                     let mut theta: f64 = 0;
                     {
                         let mut j: i32 = 0;
                        while j < 20 {
                            {
                                theta += pi / 6;
                                TestUtils.assert_equals(&self.alt_polar(&r, &theta), &Complex::polar2_complex(&r, &theta), 10e-12);
                            }
                            j += 1 !!!check!!! post increment;
                         }
                     }

                    theta = -2 * pi;
                     {
                         let mut j: i32 = 0;
                        while j < 20 {
                            {
                                theta -= pi / 6;
                                TestUtils.assert_equals(&self.alt_polar(&r, &theta), &Complex::polar2_complex(&r, &theta), 10e-12);
                            }
                            j += 1 !!!check!!! post increment;
                         }
                     }

                }
                i += 1 !!!check!!! post increment;
             }
         }

    }

    pub fn  alt_polar(r: f64,  theta: f64) -> Complex  {
        return Complex::I.multiply(Complex::new(theta, 0.0)).exp().multiply(Complex::new(r, 0.0));
    }

   

    #[test]
    pub fn  test_convert_to_complex()   {
         let real : [f64; 7] = [neg_inf, -123.45, 0, 1, 234.56, pi, inf, ]
        ;
         let complex: Complex[] = Complex::convert_to_complex(&real);
         {
             let mut i: i32 = 0;
            while i < real.length {
                {
                    Assert.assert_equals(real[i], &complex[i].get_real(), 0d.0);
                }
                i += 1 !!!check!!! post increment;
             }
         }

    }
   
   


  
    


   

   


    #[test]
    pub fn  test_serial()  {
         let z: Complex = Complex::new(3.0, 4.0);
        assert_within_delta!(&z, &TestUtils::serialize_and_recover(&z));
         let ncmplx: Complex = TestUtils::serialize_and_recover(&ONE_NAN) as Complex;
        assert_within_delta!(&NAN_ZERO, &ncmplx);
       assert!(ncmplx.is_nan());
         let infcmplx: Complex = TestUtils::serialize_and_recover(&INF_INF) as Complex;
        assert_within_delta!(&INF_INF, &infcmplx);
       assert!(infcmplx.is_infinite());
         let tz: TestComplex = TestComplex::new(3.0, 4.0);
        assert_within_delta!(&tz, &TestUtils::serialize_and_recover(&tz));
         let ntcmplx: TestComplex = TestUtils::serialize_and_recover(TestComplex::new(&ONE_NAN)) as TestComplex;
        assert_within_delta!(&NAN_ZERO, &ntcmplx);
       assert!(ntcmplx.is_nan());
         let inftcmplx: TestComplex = TestUtils::serialize_and_recover(TestComplex::new(&INF_INF)) as TestComplex;
        assert_within_delta!(&INF_INF, &inftcmplx);
       assert!(inftcmplx.is_infinite());
    }

    /**
     * Class to test extending Complex
     */

    /**
         * Serialization identifier.
         */
     let serial_version_u_i_d: i64 = 3268726724160389237;
    pub struct TestComplex {
        super: Complex;
    }
    
    impl TestComplex {

        pub fn new( real: f64,  imaginary: f64) -> TestComplex {
            super(&real, &imaginary);
        }

        pub fn new( other: &Complex) -> TestComplex {
            this(&other.get_real(), &other.get_imaginary());
        }

        pub fn  create_complex(&self,  real: f64,  imaginary: f64) -> TestComplex  {
            return TestComplex::new(&real, &imaginary);
        }
    }

}


                
                