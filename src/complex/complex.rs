#![allow(dead_code)]

use std::f64;
use util::precision;
use util::fastmath::*;
pub use std::fmt;
use std::hash::{Hash, SipHasher, Hasher};
use util::rsutils;


pub struct Complex {
    pub real: f64,

    pub imaginary: f64,

    pub is_nan: bool,

    pub is_infinite: bool,
}

pub const I: Complex = Complex {
    real: 0.0,
    imaginary: 1.0,
    is_nan: false,
    is_infinite: false,
};

/** A complex number representing "NaN + NaNi" */
pub const NAN: Complex = Complex {
    real: f64::NAN,
    imaginary: f64::NAN,
    is_nan: true,
    is_infinite: false,
};

/** A complex number representing "+INF + INFi" */
pub const INF: Complex = Complex {
    real: f64::INFINITY,
    imaginary: f64::INFINITY,
    is_nan: false,
    is_infinite: true,
};

/** A complex number representing "1.0 + 0.0i" */
pub const ONE: Complex = Complex {
    real: 1.0,
    imaginary: 0.0,
    is_nan: false,
    is_infinite: false,
};

/** A complex number representing "0.0 + 0.0i" */
pub const ZERO: Complex = Complex {
    real: 0.0,
    imaginary: 0.0,
    is_nan: false,
    is_infinite: false,
};

impl PartialEq for Complex {
    fn eq(&self, other: &Complex) -> bool {
        if other.is_nan {
            self.is_nan
        } else {
            self.real == other.real && self.imaginary == other.imaginary
        }
    }
}

impl Hash for Complex {
    fn hash<H: Hasher>(&self, state: &mut H) {
        if self.is_nan {
            7.hash(state);
        } else {
            rsutils::double_to_raw_long_bits(&self.imaginary.abs()).hash(state);
            rsutils::double_to_raw_long_bits(&self.real.abs()).hash(state);
        }
    }
}

impl fmt::Debug for Complex {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Complex: {}", self.to_string())
    }
}

impl Complex {
    pub fn new(real: f64, imaginary: f64) -> Complex {
        let is_nan = f64::is_nan(real) || f64::is_nan(imaginary);
        Complex {
            real: real,
            imaginary: imaginary,
            is_nan: is_nan,
            is_infinite: f64::is_infinite(real) || f64::is_infinite(imaginary),
        }
    }

    pub fn new_real(real: f64) -> Complex {
        Complex::new(real, 0.0)
    }

    /**
     * Access the real part.
     *
     * @return the real part.
     */
    pub fn get_real(&self) -> f64 {
        self.real
    }

    /**
     * Access the imaginary part.
     *
     * @return the imaginary part.
     */
    pub fn get_imaginary(&self) -> f64 {
        self.imaginary
    }

    /**
     * Checks whether either or both parts of this complex number is
     * {@code NaN}.
     *
     * @return true if either or both parts of this complex number is
     * {@code NaN}; false otherwise.
     */
    pub fn is_nan(&self) -> bool {
        self.is_nan
    }

    /**
     * Checks whether either the real or imaginary part of this complex number
     * takes an infinite value (either {@code f64::POSITIVE_INFINITY} or
     * {@code f64::NEGATIVE_INFINITY}) and neither part
     * is {@code NaN}.
     *
     * @return true if one or both parts of this complex number are infinite
     * and neither part is {@code NaN}.
     */
    pub fn is_infinite(&self) -> bool {
        self.is_infinite
    }


    /**
     * Return the absolute value of this complex number.
     * Returns {@code NaN} if either real or imaginary part is {@code NaN}
     * and {@code f64::POSITIVE_INFINITY} if neither part is {@code NaN},
     * but at least one part is infinite.
     *
     * @return the absolute value.
     */
    pub fn abs(&self) -> f64 {
        match self {
            &Complex { real: _, imaginary: _, is_nan: true, is_infinite: _ } => f64::NAN,
            &Complex { real: _, imaginary: _, is_nan: _, is_infinite: true } => f64::INFINITY,
            _ => {
                if f64::abs(self.real) < f64::abs(self.imaginary) {
                    if self.imaginary == 0.0 {
                        f64::abs(self.real)
                    } else {
                        let q = self.real / self.imaginary;
                        f64::abs(self.imaginary) * f64::sqrt(1.0 + q * q)
                    }
                } else {
                    if self.real == 0.0 {
                        f64::abs(self.imaginary)
                    } else {
                        let q = self.imaginary / self.real;
                        f64::abs(self.real) * f64::sqrt(1.0 + q * q)
                    }
                }
            }
        }
    }

    /**
     * Returns a {@code Complex} whose value is
     * {@code (this + addend)}.
     * Uses the definitional formula
     * <p>
     *   {@code (a + bi) + (c + di) = (a+c) + (b+d)i}
     * </p>
     * If either {@code this} or {@code addend} has a {@code NaN} value in
     * either part, {@link #NaN} is returned; otherwise {@code Infinite}
     * and {@code NaN} values are returned in the parts of the result
     * according to the rules for {@link java.lang.Double} arithmetic.
     *
     * @param  addend Value to be added to this {@code Complex}.
     * @return {@code this + addend}.
     * @throws NullArgumentException if {@code addend} is {@code null}.
     */
    pub fn add(&self, addend: &Complex) -> Complex {
        if self.is_nan || addend.is_nan {
            NAN
        } else {
            Complex::new(self.real + addend.get_real(),
                         self.imaginary + addend.get_imaginary())
        }
    }

    /**
     * Returns a {@code Complex} whose value is {@code (this + addend)},
     * with {@code addend} interpreted as a real number.
     *
     * @param addend Value to be added to this {@code Complex}.
     * @return {@code this + addend}.
     * @see #add(Complex)
     */
    pub fn add_f64(&self, addend: f64) -> Complex {
        if self.is_nan || f64::is_nan(addend) {
            NAN
        } else {
            Complex::new(self.real + addend, self.imaginary)
        }
    }

    /**
     * Returns the conjugate of this complex number.
     * The conjugate of {@code a + bi} is {@code a - bi}.
     * <p>
     * {@link #NaN} is returned if either the real or imaginary
     * part of this Complex number equals {@code f64::NaN}.
     * </p><p>
     * If the imaginary part is infinite, and the real part is not
     * {@code NaN}, the returned value has infinite imaginary part
     * of the opposite sign, e.g. the conjugate of
     * {@code 1 + POSITIVE_INFINITY i} is {@code 1 - NEGATIVE_INFINITY i}.
     * </p>
     * @return the conjugate of this Complex object.
     */
    pub fn conjugate(&self) -> Complex {
        if self.is_nan {
            NAN
        } else {
            Complex::new(self.real, -self.imaginary)
        }
    }

    /**
     * Returns a {@code Complex} whose value is
     * {@code (this / divisor)}.
     * Implements the definitional formula
     * <pre>
     *  <code>
     *    a + bi          ac + bd + (bc - ad)i
     *    ----------- = -------------------------
     *    c + di         c<sup>2</sup> + d<sup>2</sup>
     *  </code>
     * </pre>
     * but uses
     * <a href="http://doi.acm.org/10.1145/1039813.1039814">
     * prescaling of operands</a> to limit the effects of overflows and
     * underflows in the computation.
     * <p>
     * {@code Infinite} and {@code NaN} values are handled according to the
     * following rules, applied in the order presented:
     * <ul>
     *  <li>If either {@code this} or {@code divisor} has a {@code NaN} value
     *   in either part, {@link #NaN} is returned.
     *  </li>
     *  <li>If {@code divisor} equals {@link #ZERO}, {@link #NaN} is returned.
     *  </li>
     *  <li>If {@code this} and {@code divisor} are both infinite,
     *   {@link #NaN} is returned.
     *  </li>
     *  <li>If {@code this} is finite (i.e., has no {@code Infinite} or
     *   {@code NaN} parts) and {@code divisor} is infinite (one or both parts
     *   infinite), {@link #ZERO} is returned.
     *  </li>
     *  <li>If {@code this} is infinite and {@code divisor} is finite,
     *   {@code NaN} values are returned in the parts of the result if the
     *   {@link java.lang.Double} rules applied to the definitional formula
     *   force {@code NaN} results.
     *  </li>
     * </ul>
     *
     * @param divisor Value by which this {@code Complex} is to be divided.
     * @return {@code this / divisor}.
     * @throws NullArgumentException if {@code divisor} is {@code null}.
     */
    pub fn divide(&self, divisor: &Complex) -> Complex {
        if self.is_nan || divisor.is_nan {
            return NAN;
        }

        let c = divisor.get_real();
        let d = divisor.get_imaginary();
        if c == 0.0 && d == 0.0 {
            return NAN;
        }

        if divisor.is_infinite() && !self.is_infinite() {
            return ZERO;
        }

        if f64::abs(c) < f64::abs(d) {
            let q = c / d;
            let denominator = c * q + d;
            Complex::new((self.real * q + self.imaginary) / denominator,
                         (self.imaginary * q - self.real) / denominator)
        } else {
            let q = d / c;
            let denominator = d * q + c;
            Complex::new((self.imaginary * q + self.real) / denominator,
                         (self.imaginary - self.real * q) / denominator)
        }
    }

    /**
     * Returns a {@code Complex} whose value is {@code (this / divisor)},
     * with {@code divisor} interpreted as a real number.
     *
     * @param  divisor Value by which this {@code Complex} is to be divided.
     * @return {@code this / divisor}.
     * @see #divide(Complex)
     */
    pub fn divide_f64(&self, divisor: f64) -> Complex {
        if self.is_nan || f64::is_nan(divisor) {
            return NAN;
        }
        if divisor == 0.0 {
            return NAN;
        }
        if f64::is_infinite(divisor) {
            if !self.is_infinite { ZERO } else { NAN }
        } else {
            Complex::new(self.real / divisor, self.imaginary / divisor)
        }
    }

    /** {@inheritDoc} */
    pub fn reciprocal(&self) -> Complex {
        if self.is_nan {
            return NAN;
        }

        if self.real == 0.0 && self.imaginary == 0.0 {
            return INF;
        }

        if self.is_infinite {
            return ZERO;
        }

        if f64::abs(self.real) < f64::abs(self.imaginary) {
            let q = self.real / self.imaginary;
            let scale = 1.0 / (self.real * q + self.imaginary);
            Complex::new(scale * q, -scale)
        } else {
            let q = self.imaginary / self.real;
            let scale = 1. / (self.imaginary * q + self.real);
            Complex::new(scale, -scale * q)
        }
    }

    /**
     * Test for equality with another object.
     * If both the real and imaginary parts of two complex numbers
     * are exactly the same, and neither is {@code f64::NaN}, the two
     * Complex objects are considered to be equal.
     * The behavior is the same as for JDK's {@link Double#equals(Object)
     * Double}:
     * <ul>
     *  <li>All {@code NaN} values are considered to be equal,
     *   i.e, if either (or both) real and imaginary parts of the complex
     *   number are equal to {@code f64::NaN}, the complex number is equal
     *   to {@code NaN}.
     *  </li>
     *  <li>
     *   Instances constructed with different representations of zero (i.e.
     *   either "0" or "-0") are <em>not</em> considered to be equal.
     *  </li>
     * </ul>
     *
     * @param other Object to test for equality with this instance.
     * @return {@code true} if the objects are equal, {@code false} if object
     * is {@code null}, not an instance of {@code Complex}, or not equal to
     * this instance.
     */
    pub fn equals(&self, c: &Complex) -> bool {
        if c.is_nan() {
            return self.is_nan;
        } else {
            return self.real == c.real && self.imaginary == c.imaginary;
        }
    }

    /**
     * Test for the floating-point equality between Complex objects.
     * It returns {@code true} if both arguments are equal or within the
     * range of allowed error (inclusive).
     *
     * @param x First value (cannot be {@code null}).
     * @param y Second value (cannot be {@code null}).
     * @param maxUlps {@code (maxUlps - 1)} is the number of floating point
     * values between the real (resp. imaginary) parts of {@code x} and
     * {@code y}.
     * @return {@code true} if there are fewer than {@code maxUlps} floating
     * point values between the real (resp. imaginary) parts of {@code x}
     * and {@code y}.
     *
     * @see Precision#equals(double,double,int)
     * @since 3.3
     */
    pub fn equals_ulps(x: &Complex, y: &Complex, max_ulps: u32) -> bool {
        return precision::F64::equals_i32(x.real, y.real, max_ulps) &&
               precision::F64::equals_i32(x.imaginary, y.imaginary, max_ulps);
    }

    /**
     * Returns {@code true} iff the values are equal as defined by
     * {@link #equals(Complex,Complex,int) equals(x, y, 1)}.
     *
     * @param x First value (cannot be {@code null}).
     * @param y Second value (cannot be {@code null}).
     * @return {@code true} if the values are equal.
     *
     * @since 3.3
     */
    pub fn equals_static(x: &Complex, y: &Complex) -> bool {
        return Complex::equals_ulps(&x, &y, 1);
    }


    /**
     * Returns {@code true} if, both for the real part and for the imaginary
     * part, there is no double value strictly between the arguments or the
     * difference between them is within the range of allowed error
     * (inclusive).  Returns {@code false} if either of the arguments is NaN.
     *
     * @param x First value (cannot be {@code null}).
     * @param y Second value (cannot be {@code null}).
     * @param eps Amount of allowed absolute error.
     * @return {@code true} if the values are two adjacent floating point
     * numbers or they are within range of each other.
     *
     * @see Precision#equals(double,double,double)
     * @since 3.3
     */
    pub fn equals_eps(x: &Complex, y: &Complex, eps: f64) -> bool {
        return precision::F64::equals_eps(x.real, y.real, eps) &&
               precision::F64::equals_eps(x.imaginary, y.imaginary, eps);
    }


    /**
     * Returns {@code true} if, both for the real part and for the imaginary
     * part, there is no double value strictly between the arguments or the
     * relative difference between them is smaller or equal to the given
     * tolerance. Returns {@code false} if either of the arguments is NaN.
     *
     * @param x First value (cannot be {@code null}).
     * @param y Second value (cannot be {@code null}).
     * @param eps Amount of allowed relative error.
     * @return {@code true} if the values are two adjacent floating point
     * numbers or they are within range of each other.
     *
     * @see Precision#equalsWithRelativeTolerance(double,double,double)
     * @since 3.3
     */
    pub fn equals_with_relative_tolerance(x: &Complex, y: &Complex, eps: f64) -> bool {
        return precision::F64::equals_with_relative_tolerance(x.real, y.real, eps) &&
               precision::F64::equals_with_relative_tolerance(x.imaginary, y.imaginary, eps);
    }



    /**
     * Returns a {@code Complex} whose value is {@code this * factor}.
     * Implements preliminary checks for {@code NaN} and infinity followed by
     * the definitional formula:
     * <p>
     *   {@code (a + bi)(c + di) = (ac - bd) + (ad + bc)i}
     * </p>
     * Returns {@link #NaN} if either {@code this} or {@code factor} has one or
     * more {@code NaN} parts.
     * <p>
     * Returns {@link #INF} if neither {@code this} nor {@code factor} has one
     * or more {@code NaN} parts and if either {@code this} or {@code factor}
     * has one or more infinite parts (same result is returned regardless of
     * the sign of the components).
     * </p><p>
     * Returns finite values in components of the result per the definitional
     * formula in all remaining cases.</p>
     *
     * @param  factor value to be multiplied by this {@code Complex}.
     * @return {@code this * factor}.
     * @throws NullArgumentException if {@code factor} is {@code null}.
     */
    pub fn multiply(&self, factor: &Complex) -> Complex {
        if self.is_nan || factor.is_nan {
            return NAN;
        }
        if f64::is_infinite(self.real) || f64::is_infinite(self.imaginary) ||
           f64::is_infinite(factor.real) || f64::is_infinite(factor.imaginary) {
            // we don't use isInfinite() to avoid testing for NaN again
            return INF;
        }
        return Complex::new(self.real * factor.real - self.imaginary * factor.imaginary,
                            self.real * factor.imaginary + self.imaginary * factor.real);
    }

    /**
     * Returns a {@code Complex} whose value is {@code this * factor}, with {@code factor}
     * interpreted as a integer number.
     *
     * @param  factor value to be multiplied by this {@code Complex}.
     * @return {@code this * factor}.
     * @see #multiply(Complex)
     */
    pub fn multiply_i32(&self, factor: i32) -> Complex {
        if self.is_nan {
            return NAN;
        }
        if f64::is_infinite(self.real) || f64::is_infinite(self.imaginary) {
            return INF;
        }
        return Complex::new(self.real * factor as f64, self.imaginary * factor as f64);
    }

    /**
     * Returns a {@code Complex} whose value is {@code this * factor}, with {@code factor}
     * interpreted as a real number.
     *
     * @param  factor value to be multiplied by this {@code Complex}.
     * @return {@code this * factor}.
     * @see #multiply(Complex)
     */
    pub fn multiply_f64(&self, factor: f64) -> Complex {
        if self.is_nan || f64::is_nan(factor) {
            return NAN;
        }
        if f64::is_infinite(self.real) || f64::is_infinite(self.imaginary) ||
           f64::is_infinite(factor) {
            // we don't use isInfinite() to avoid testing for NaN again
            return INF;
        }
        return Complex::new(self.real * factor, self.imaginary * factor);
    }

    /**
     * Returns a {@code Complex} whose value is {@code (-this)}.
     * Returns {@code NaN} if either real or imaginary
     * part of this Complex number is {@code f64::NaN}.
     *
     * @return {@code -this}.
     */
    pub fn negate(&self) -> Complex {
        if self.is_nan {
            return NAN;
        }
        return Complex::new(-self.real, -self.imaginary);
    }



    /**
     * Returns a {@code Complex} whose value is
     * {@code (this - subtrahend)}.
     * Uses the definitional formula
     * <p>
     *  {@code (a + bi) - (c + di) = (a-c) + (b-d)i}
     * </p>
     * If either {@code this} or {@code subtrahend} has a {@code NaN]} value in either part,
     * {@link #NaN} is returned; otherwise infinite and {@code NaN} values are
     * returned in the parts of the result according to the rules for
     * {@link java.lang.Double} arithmetic.
     *
     * @param  subtrahend value to be subtracted from this {@code Complex}.
     * @return {@code this - subtrahend}.
     * @throws NullArgumentException if {@code subtrahend} is {@code null}.
     */
    pub fn subtract(&self, subtrahend: &Complex) -> Complex {
        if self.is_nan || subtrahend.is_nan {
            return NAN;
        }
        return Complex::new(self.real - subtrahend.get_real(),
                            self.imaginary - subtrahend.get_imaginary());
    }


    /**
     * Returns a {@code Complex} whose value is
     * {@code (this - subtrahend)}.
     *
     * @param  subtrahend value to be subtracted from this {@code Complex}.
     * @return {@code this - subtrahend}.
     * @see #subtract(Complex)
     */
    pub fn subtract_f64(&self, subtrahend: f64) -> Complex {
        if self.is_nan || f64::is_nan(subtrahend) {
            return NAN;
        }
        return Complex::new(self.real - subtrahend, self.imaginary);
    }

    /**
     * Compute the
     * <a href="http://mathworld.wolfram.com/SquareRoot.html" TARGET="_top">
     * square root</a> of this complex number.
     * Implements the following algorithm to compute {@code sqrt(a + bi)}:
     * <ol><li>Let {@code t = sqrt((|a| + |a + bi|) / 2)}</li>
     * <li><pre>if {@code  a ≥ 0} return {@code t + (b/2t)i}
     *  else return {@code |b|/2t + sign(b)t i }</pre></li>
     * </ol>
     * where <ul>
     * <li>{@code |a| = }{@link FastMath#abs}(a)</li>
     * <li>{@code |a + bi| = }{@link Complex#abs}(a + bi)</li>
     * <li>{@code sign(b) =  }{@link FastMath#copySign(double,double) copySign(1d, b)}
     * </ul>
     * <p>
     * Returns {@link Complex#NaN} if either real or imaginary part of the
     * input argument is {@code NaN}.
     * </p>
     * Infinite values in real or imaginary parts of the input may result in
     * infinite or NaN values returned in parts of the result.
     * <pre>
     *  Examples:
     *  <code>
     *   sqrt(1 ± INFINITY i) = INFINITY + NaN i
     *   sqrt(INFINITY + i) = INFINITY + 0i
     *   sqrt(-INFINITY + i) = 0 + INFINITY i
     *   sqrt(INFINITY ± INFINITY i) = INFINITY + NaN i
     *   sqrt(-INFINITY ± INFINITY i) = NaN ± INFINITY i
     *  </code>
     * </pre>
     *
     * @return the square root of {@code this}.
     * @since 1.2
     */
    pub fn sqrt(&self) -> Complex {
        if self.is_nan {
            return NAN;
        }
        if self.real == 0.0 && self.imaginary == 0.0 {
            return Complex::new(0.0, 0.0);
        }
        let t: f64 = F64::sqrt((F64::abs(self.real) + self.abs()) / 2.0);
        if self.real >= 0.0 {
            return Complex::new(t, self.imaginary / (2.0 * t));
        } else {
            return Complex::new(F64::abs(self.imaginary) / (2.0 * t),
                                F64::copy_sign(1.0, self.imaginary) * t);
        }
    }



    /**
     * Compute the
     * <a href="http://mathworld.wolfram.com/SquareRoot.html" TARGET="_top">
     * square root</a> of <code>1 - this<sup>2</sup></code> for this complex
     * number.
     * Computes the result directly as
     * {@code sqrt(ONE.subtract(z.multiply(z)))}.
     * <p>
     * Returns {@link Complex#NaN} if either real or imaginary part of the
     * input argument is {@code NaN}.
     * </p>
     * Infinite values in real or imaginary parts of the input may result in
     * infinite or NaN values returned in parts of the result.
     *
     * @return the square root of <code>1 - this<sup>2</sup></code>.
     * @since 1.2
     */
    pub fn sqrt1z(&self) -> Complex {
        return Complex::new(1.0, 0.0).subtract(&self.multiply(self)).sqrt();
    }

    /**
     * Compute the
     * <a href="http://mathworld.wolfram.com/NaturalLogarithm.html" TARGET="_top">
     * natural logarithm</a> of this complex number.
     * Implements the formula:
     * <pre>
     *  <code>
     *   log(a + bi) = ln(|a + bi|) + arg(a + bi)i
     *  </code>
     * </pre>
     * where ln on the right hand side is {@link FastMath#log},
     * {@code |a + bi|} is the modulus, {@link Complex#abs},  and
     * {@code arg(a + bi) = }{@link FastMath#atan2}(b, a).
     * <p>
     * Returns {@link Complex#NaN} if either real or imaginary part of the
     * input argument is {@code NaN}.
     * </p>
     * Infinite (or critical) values in real or imaginary parts of the input may
     * result in infinite or NaN values returned in parts of the result.
     * <pre>
     *  Examples:
     *  <code>
     *   log(1 ± INFINITY i) = INFINITY ± (π/2)i
     *   log(INFINITY + i) = INFINITY + 0i
     *   log(-INFINITY + i) = INFINITY + πi
     *   log(INFINITY ± INFINITY i) = INFINITY ± (π/4)i
     *   log(-INFINITY ± INFINITY i) = INFINITY ± (3π/4)i
     *   log(0 + 0i) = -INFINITY + 0i
     *  </code>
     * </pre>
     *
     * @return the value <code>ln   this</code>, the natural logarithm
     * of {@code this}.
     * @since 1.2
     */
    pub fn log(&self) -> Complex {
        if self.is_nan {
            return NAN;
        }
        return Complex::new(F64::log(self.abs()), F64::atan2(self.imaginary, self.real));
    }



    /**
     * Compute the
     * <a href="http://mathworld.wolfram.com/InverseCosine.html" TARGET="_top">
     * inverse cosine</a> of this complex number.
     * Implements the formula:
     * <p>
     *  {@code acos(z) = -i (log(z + i (sqrt(1 - z<sup>2</sup>))))}
     * </p>
     * Returns {@link Complex#NaN} if either real or imaginary part of the
     * input argument is {@code NaN} or infinite.
     *
     * @return the inverse cosine of this complex number.
     * @since 1.2
     */
    pub fn acos(&self) -> Complex {
        if self.is_nan {
            return NAN;
        }
        return self.add(&self.sqrt1z().multiply(&I)).log().multiply(&I.negate());
    }

    /**
     * Compute the
     * <a href="http://mathworld.wolfram.com/InverseSine.html" TARGET="_top">
     * inverse sine</a> of this complex number.
     * Implements the formula:
     * <p>
     *  {@code asin(z) = -i (log(sqrt(1 - z<sup>2</sup>) + iz))}
     * </p><p>
     * Returns {@link Complex#NaN} if either real or imaginary part of the
     * input argument is {@code NaN} or infinite.</p>
     *
     * @return the inverse sine of this complex number.
     * @since 1.2
     */
    pub fn asin(&self) -> Complex {
        if self.is_nan {
            return NAN;
        }
        return self.sqrt1z().add(&self.multiply(&I)).log().multiply(&I.negate());
    }

    /**
     * Compute the
     * <a href="http://mathworld.wolfram.com/InverseTangent.html" TARGET="_top">
     * inverse tangent</a> of this complex number.
     * Implements the formula:
     * <p>
     * {@code atan(z) = (i/2) log((i + z)/(i - z))}
     * </p><p>
     * Returns {@link Complex#NaN} if either real or imaginary part of the
     * input argument is {@code NaN} or infinite.</p>
     *
     * @return the inverse tangent of this complex number
     * @since 1.2
     */
    pub fn atan(&self) -> Complex {
        if self.is_nan {
            return NAN;
        }
        return self.add(&I)
            .divide(&I.subtract(self))
            .log()
            .multiply(&I.divide(&Complex::new(2.0, 0.0)));
    }



    /**
     * Compute the
     * <a href="http://mathworld.wolfram.com/Cosine.html" TARGET="_top">
     * cosine</a> of this complex number.
     * Implements the formula:
     * <p>
     *  {@code cos(a + bi) = cos(a)cosh(b) - sin(a)sinh(b)i}
     * </p><p>
     * where the (real) functions on the right-hand side are
     * {@link FastMath#sin}, {@link FastMath#cos},
     * {@link FastMath#cosh} and {@link FastMath#sinh}.
     * </p><p>
     * Returns {@link Complex#NaN} if either real or imaginary part of the
     * input argument is {@code NaN}.
     * </p><p>
     * Infinite values in real or imaginary parts of the input may result in
     * infinite or NaN values returned in parts of the result.</p>
     * <pre>
     *  Examples:
     *  <code>
     *   cos(1 ± INFINITY i) = 1 \u2213 INFINITY i
     *   cos(±INFINITY + i) = NaN + NaN i
     *   cos(±INFINITY ± INFINITY i) = NaN + NaN i
     *  </code>
     * </pre>
     *
     * @return the cosine of this complex number.
     * @since 1.2
     */
    pub fn cos(&self) -> Complex {
        if self.is_nan {
            return NAN;
        }
        return Complex::new(F64::cos(self.real) * F64::cosh(self.imaginary),
                            -F64::sin(self.real) * F64::sinh(self.imaginary));
    }

    /**
     * Compute the
     * <a href="http://mathworld.wolfram.com/HyperbolicCosine.html" TARGET="_top">
     * hyperbolic cosine</a> of this complex number.
     * Implements the formula:
     * <pre>
     *  <code>
     *   cosh(a + bi) = cosh(a)cos(b) + sinh(a)sin(b)i
     *  </code>
     * </pre>
     * where the (real) functions on the right-hand side are
     * {@link FastMath#sin}, {@link FastMath#cos},
     * {@link FastMath#cosh} and {@link FastMath#sinh}.
     * <p>
     * Returns {@link Complex#NaN} if either real or imaginary part of the
     * input argument is {@code NaN}.
     * </p>
     * Infinite values in real or imaginary parts of the input may result in
     * infinite or NaN values returned in parts of the result.
     * <pre>
     *  Examples:
     *  <code>
     *   cosh(1 ± INFINITY i) = NaN + NaN i
     *   cosh(±INFINITY + i) = INFINITY ± INFINITY i
     *   cosh(±INFINITY ± INFINITY i) = NaN + NaN i
     *  </code>
     * </pre>
     *
     * @return the hyperbolic cosine of this complex number.
     * @since 1.2
     */
    pub fn cosh(&self) -> Complex {
        if self.is_nan {
            return NAN;
        }
        return Complex::new(F64::cosh(self.real) * F64::cos(self.imaginary),
                            F64::sinh(self.real) * F64::sin(self.imaginary));
    }

    /**
     * Compute the
     * <a href="http://mathworld.wolfram.com/ExponentialFunction.html" TARGET="_top">
     * exponential function</a> of this complex number.
     * Implements the formula:
     * <pre>
     *  <code>
     *   exp(a + bi) = exp(a)cos(b) + exp(a)sin(b)i
     *  </code>
     * </pre>
     * where the (real) functions on the right-hand side are
     * {@link FastMath#exp}, {@link FastMath#cos}, and
     * {@link FastMath#sin}.
     * <p>
     * Returns {@link Complex#NaN} if either real or imaginary part of the
     * input argument is {@code NaN}.
     * </p>
     * Infinite values in real or imaginary parts of the input may result in
     * infinite or NaN values returned in parts of the result.
     * <pre>
     *  Examples:
     *  <code>
     *   exp(1 ± INFINITY i) = NaN + NaN i
     *   exp(INFINITY + i) = INFINITY + INFINITY i
     *   exp(-INFINITY + i) = 0 + 0i
     *   exp(±INFINITY ± INFINITY i) = NaN + NaN i
     *  </code>
     * </pre>
     *
     * @return <code><i>e</i><sup>this</sup></code>.
     * @since 1.2
     */
    pub fn exp(&self) -> Complex {
        if self.is_nan {
            return NAN;
        }
        let exp_real: f64 = F64::exp(self.real);
        return Complex::new(exp_real * F64::cos(self.imaginary),
                            exp_real * F64::sin(self.imaginary));
    }


    /**
     * Returns of value of this complex number raised to the power of {@code x}.
     * Implements the formula:
     * <pre>
     *  <code>
     *   y<sup>x</sup> = exp(x·log(y))
     *  </code>
     * </pre>
     * where {@code exp} and {@code log} are {@link #exp} and
     * {@link #log}, respectively.
     * <p>
     * Returns {@link Complex#NaN} if either real or imaginary part of the
     * input argument is {@code NaN} or infinite, or if {@code y}
     * equals {@link Complex#ZERO}.</p>
     *
     * @param  x exponent to which this {@code Complex} is to be raised.
     * @return <code> this<sup>x</sup></code>.
     * @throws NullArgumentException if x is {@code null}.
     * @since 1.2
     */
    pub fn pow(&self, x: &Complex) -> Complex {
        return self.log().multiply(x).exp();
    }

    /**
     * Returns of value of this complex number raised to the power of {@code x}.
     *
     * @param  x exponent to which this {@code Complex} is to be raised.
     * @return <code>this<sup>x</sup></code>.
     * @see #pow(Complex)
     */
    pub fn pow_f64(&self, x: f64) -> Complex {
        return self.log().multiply_f64(x).exp();
    }

    /**
     * Compute the
     * <a href="http://mathworld.wolfram.com/Sine.html" TARGET="_top">
     * sine</a>
     * of this complex number.
     * Implements the formula:
     * <pre>
     *  <code>
     *   sin(a + bi) = sin(a)cosh(b) - cos(a)sinh(b)i
     *  </code>
     * </pre>
     * where the (real) functions on the right-hand side are
     * {@link FastMath#sin}, {@link FastMath#cos},
     * {@link FastMath#cosh} and {@link FastMath#sinh}.
     * <p>
     * Returns {@link Complex#NaN} if either real or imaginary part of the
     * input argument is {@code NaN}.
     * </p><p>
     * Infinite values in real or imaginary parts of the input may result in
     * infinite or {@code NaN} values returned in parts of the result.
     * <pre>
     *  Examples:
     *  <code>
     *   sin(1 ± INFINITY i) = 1 ± INFINITY i
     *   sin(±INFINITY + i) = NaN + NaN i
     *   sin(±INFINITY ± INFINITY i) = NaN + NaN i
     *  </code>
     * </pre>
     *
     * @return the sine of this complex number.
     * @since 1.2
     */
    pub fn sin(&self) -> Complex {
        if self.is_nan {
            return NAN;
        }
        return Complex::new(F64::sin(self.real) * F64::cosh(self.imaginary),
                            F64::cos(self.real) * F64::sinh(self.imaginary));
    }

    /**
     * Compute the
     * <a href="http://mathworld.wolfram.com/HyperbolicSine.html" TARGET="_top">
     * hyperbolic sine</a> of this complex number.
     * Implements the formula:
     * <pre>
     *  <code>
     *   sinh(a + bi) = sinh(a)cos(b)) + cosh(a)sin(b)i
     *  </code>
     * </pre>
     * where the (real) functions on the right-hand side are
     * {@link FastMath#sin}, {@link FastMath#cos},
     * {@link FastMath#cosh} and {@link FastMath#sinh}.
     * <p>
     * Returns {@link Complex#NaN} if either real or imaginary part of the
     * input argument is {@code NaN}.
     * </p><p>
     * Infinite values in real or imaginary parts of the input may result in
     * infinite or NaN values returned in parts of the result.
     * <pre>
     *  Examples:
     *  <code>
     *   sinh(1 ± INFINITY i) = NaN + NaN i
     *   sinh(±INFINITY + i) = ± INFINITY + INFINITY i
     *   sinh(±INFINITY ± INFINITY i) = NaN + NaN i
     *  </code>
     * </pre>
     *
     * @return the hyperbolic sine of {@code this}.
     * @since 1.2
     */
    pub fn sinh(&self) -> Complex {
        if self.is_nan {
            return NAN;
        }
        return Complex::new(F64::sinh(self.real) * F64::cos(self.imaginary),
                            F64::cosh(self.real) * F64::sin(self.imaginary));
    }



    /**
     * Compute the
     * <a href="http://mathworld.wolfram.com/Tangent.html" TARGET="_top">
     * tangent</a> of this complex number.
     * Implements the formula:
     * <pre>
     *  <code>
     *   tan(a + bi) = sin(2a)/(cos(2a)+cosh(2b)) + [sinh(2b)/(cos(2a)+cosh(2b))]i
     *  </code>
     * </pre>
     * where the (real) functions on the right-hand side are
     * {@link FastMath#sin}, {@link FastMath#cos}, {@link FastMath#cosh} and
     * {@link FastMath#sinh}.
     * <p>
     * Returns {@link Complex#NaN} if either real or imaginary part of the
     * input argument is {@code NaN}.
     * </p>
     * Infinite (or critical) values in real or imaginary parts of the input may
     * result in infinite or NaN values returned in parts of the result.
     * <pre>
     *  Examples:
     *  <code>
     *   tan(a ± INFINITY i) = 0 ± i
     *   tan(±INFINITY + bi) = NaN + NaN i
     *   tan(±INFINITY ± INFINITY i) = NaN + NaN i
     *   tan(±π/2 + 0 i) = ±INFINITY + NaN i
     *  </code>
     * </pre>
     *
     * @return the tangent of {@code this}.
     * @since 1.2
     */
    pub fn tan(&self) -> Complex {
        if self.is_nan || f64::is_infinite(self.real) {
            return NAN;
        }
        if self.imaginary > 20.0 {
            return Complex::new(0.0, 1.0);
        }
        if self.imaginary < -20.0 {
            return Complex::new(0.0, -1.0);
        }
        let real2: f64 = 2.0 * self.real;
        let imaginary2: f64 = 2.0 * self.imaginary;
        let d: f64 = F64::cos(real2) + F64::cosh(imaginary2);
        return Complex::new(F64::sin(real2) / d, F64::sinh(imaginary2) / d);
    }

    /**
     * Compute the
     * <a href="http://mathworld.wolfram.com/HyperbolicTangent.html" TARGET="_top">
     * hyperbolic tangent</a> of this complex number.
     * Implements the formula:
     * <pre>
     *  <code>
     *   tan(a + bi) = sinh(2a)/(cosh(2a)+cos(2b)) + [sin(2b)/(cosh(2a)+cos(2b))]i
     *  </code>
     * </pre>
     * where the (real) functions on the right-hand side are
     * {@link FastMath#sin}, {@link FastMath#cos}, {@link FastMath#cosh} and
     * {@link FastMath#sinh}.
     * <p>
     * Returns {@link Complex#NaN} if either real or imaginary part of the
     * input argument is {@code NaN}.
     * </p>
     * Infinite values in real or imaginary parts of the input may result in
     * infinite or NaN values returned in parts of the result.
     * <pre>
     *  Examples:
     *  <code>
     *   tanh(a ± INFINITY i) = NaN + NaN i
     *   tanh(±INFINITY + bi) = ±1 + 0 i
     *   tanh(±INFINITY ± INFINITY i) = NaN + NaN i
     *   tanh(0 + (π/2)i) = NaN + INFINITY i
     *  </code>
     * </pre>
     *
     * @return the hyperbolic tangent of {@code this}.
     * @since 1.2
     */
    pub fn tanh(&self) -> Complex {
        if self.is_nan || f64::is_infinite(self.imaginary) {
            return NAN;
        }
        if self.real > 20.0 {
            return Complex::new(1.0, 0.0);
        }
        if self.real < -20.0 {
            return Complex::new(-1.0, 0.0);
        }
        let real2: f64 = 2.0 * self.real;
        let imaginary2: f64 = 2.0 * self.imaginary;
        let d: f64 = F64::cosh(real2) + F64::cos(imaginary2);
        return Complex::new(F64::sinh(real2) / d, F64::sin(imaginary2) / d);
    }



    /**
     * Compute the argument of this complex number.
     * The argument is the angle phi between the positive real axis and
     * the point representing this number in the complex plane.
     * The value returned is between -PI (not inclusive)
     * and PI (inclusive), with negative values returned for numbers with
     * negative imaginary parts.
     * <p>
     * If either real or imaginary part (or both) is NaN, NaN is returned.
     * Infinite parts are handled as {@code Math.atan2} handles them,
     * essentially treating finite parts as zero in the presence of an
     * infinite coordinate and returning a multiple of pi/4 depending on
     * the signs of the infinite parts.
     * See the javadoc for {@code Math.atan2} for full details.
     *
     * @return the argument of {@code this}.
     */
    pub fn get_argument(&self) -> f64 {
        return F64::atan2(self.get_imaginary(), self.get_real());
    }

    /**
     * Computes the n-th roots of this complex number.
     * The nth roots are defined by the formula:
     * <pre>
     *  <code>
     *   z<sub>k</sub> = abs<sup>1/n</sup> (cos(phi + 2πk/n) + i (sin(phi + 2πk/n))
     *  </code>
     * </pre>
     * for <i>{@code k=0, 1, ..., n-1}</i>, where {@code abs} and {@code phi}
     * are respectively the {@link #abs() modulus} and
     * {@link #getArgument() argument} of this complex number.
     * <p>
     * If one or both parts of this complex number is NaN, a list with just
     * one element, {@link #NaN} is returned.
     * if neither part is NaN, but at least one part is infinite, the result
     * is a one-element list containing {@link #INF}.
     *
     * @param n Degree of root.
     * @return a List of all {@code n}-th roots of {@code this}.
     * @throws NotPositiveException if {@code n <= 0}.
     * @since 2.0
     */
    pub fn nth_root(&self, n: u32) -> Result<Vec<Complex>, &'static str> {
        if n <= 0 {
            return Err("CANNOT_COMPUTE_NTH_ROOT_FOR_NEGATIVE_N {}");
        }
        let mut result = Vec::new();
        if self.is_nan {
            result.push(NAN);
            return Ok(result);
        }
        if self.is_infinite() {
            result.push(INF);
            return Ok(result);
        }
        // nth root of abs -- faster / more accurate to use a solver here?
        let nth_root_of_abs: f64 = F64::pow(self.abs(), 1.0 / n as f64);
        // Compute nth roots of complex number with k = 0, 1, ... n-1
        let nth_phi: f64 = self.get_argument() / n as f64;
        let slice: f64 = 2.0 * f64::consts::PI / n as f64;
        let mut inner_part: f64 = nth_phi;
        {
            let mut k: i32 = 0;
            while k < n as i32 {
                {
                    // inner part
                    let real_part: f64 = nth_root_of_abs * F64::cos(inner_part);
                    let imaginary_part: f64 = nth_root_of_abs * F64::sin(inner_part);
                    result.push(Complex::new(real_part, imaginary_part));
                    inner_part += slice;
                }
                k += 1;
            }
        }

        return Ok(result);
    }

    /**
     * Create a complex number given the real and imaginary parts.
     *
     * @param realPart Real part.
     * @param imaginaryPart Imaginary part.
     * @return a Complex instance.
     */
    pub fn value_of(real_part: f64, imaginary_part: f64) -> Complex {
        if f64::is_nan(real_part) || f64::is_nan(imaginary_part) {
            return NAN;
        }
        return Complex::new(real_part, imaginary_part);
    }

    /**
     * Create a complex number given only the real part.
     *
     * @param realPart Real part.
     * @return a Complex instance.
     */
    pub fn value_of_real(real_part: f64) -> Complex {
        if f64::is_nan(real_part) {
            return NAN;
        }
        return Complex::new(real_part, 0.0);
    }

    /**
     * Resolve the transient fields in a deserialized Complex Object.
     * Subclasses will need to override {@link #createComplex} to
     * deserialize properly.
     *
     * @return A Complex instance with all fields resolved.
     * @since 2.0
    pub fn  read_resolve(&self) -> Object  {
        return Complex::new(self.real, self.imaginary);
    }
     */
    /** {@inheritDoc} */
    pub fn to_string(&self) -> String {
        format!("({}, {})", self.real, self.imaginary)
    }

    pub fn hash_code(&self) -> u64 {
        let mut hasher = SipHasher::new();
        self.hash(&mut hasher);
        let res = hasher.finish();
        res
    }

    /**
     * Creates a complex number from the given polar representation.
     * <p>
     * The value returned is <code>r·e<sup>i·theta</sup></code>,
     * computed as <code>r·cos(theta) + r·sin(theta)i</code></p>
     * <p>
     * If either <code>r</code> or <code>theta</code> is NaN, or
     * <code>theta</code> is infinite, {@link Complex#NaN} is returned.</p>
     * <p>
     * If <code>r</code> is infinite and <code>theta</code> is finite,
     * infinite or NaN values may be returned in parts of the result, following
     * the rules for double arithmetic.<pre>
     * Examples:
     * <code>
     * polar2Complex(INFINITY, π/4) = INFINITY + INFINITY i
     * polar2Complex(INFINITY, 0) = INFINITY + NaN i
     * polar2Complex(INFINITY, -π/4) = INFINITY - INFINITY i
     * polar2Complex(INFINITY, 5π/4) = -INFINITY - INFINITY i </code></pre></p>
     *
     * @param r the modulus of the complex number to create
     * @param theta  the argument of the complex number to create
     * @return <code>r·e<sup>i·theta</sup></code>
     * @throws MathIllegalArgumentException if {@code r} is negative.
     * @since 1.1
     */
    pub fn polar2_complex(r: f64, theta: f64) -> Result<Complex, &'static str> {
        if r < 0.0 {
            Err("MathIllegalArgumentException")
        } else {
            Ok(Complex::new(r * F64::cos(theta), r * F64::sin(theta)))
        }
    }

    /**
     * Convert an array of primitive doubles to an array of {@code Complex} objects.
     *
     * @param real Array of numbers to be converted to their {@code Complex}
     * equivalent.
     * @return an array of {@code Complex} objects.
     *
     * @since 3.1
     */
    pub fn convert_to_complex(real: &Vec<f64>) -> Vec<Complex> {
        let mut res = Vec::new();
        {
            let mut i: usize = 0;
            while i < real.len() {
                {
                    res.push(Complex::new(real[i], 0.0));
                }
                i += 1;
            }
        }

        return res;
    }
}
