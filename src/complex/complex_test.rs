#[cfg(test)]
mod tests {
	use hamcrest::*;	
	use complex::complex;
	use complex::complex::*;
	use assert;	
	use std::f64;
	use util::fastmath::F64;
	use util::test_utils::TestUtils;
	
	const F64_INF: f64 = f64::INFINITY;
	const F64_NEG_INF: f64 = f64::NEG_INFINITY;
	const ONE_INF: Complex = Complex {real: 1.0, imaginary: f64::INFINITY, is_nan: false, is_infinite: true};
	const ONE_NEG_INF: Complex = Complex {real: 1.0, imaginary: f64::NEG_INFINITY, is_nan: false, is_infinite: true};
	const INF_ONE: Complex = Complex {real: f64::INFINITY, imaginary: 1.0, is_nan: false, is_infinite: true};
	const INF_ZERO: Complex = Complex {real: f64::INFINITY , imaginary: 0.0, is_nan: false, is_infinite: true};
	const INF_NAN: Complex = Complex {real: f64::INFINITY, imaginary: f64::NAN, is_nan: true, is_infinite: true};
	const INF_NEG_INF: Complex = Complex {real: f64::INFINITY, imaginary: f64::NEG_INFINITY, is_nan: false, is_infinite: true};
	const INF_INF: Complex = Complex {real: f64::INFINITY, imaginary: f64::INFINITY, is_nan: false, is_infinite: true};
	const NEG_INF_INF: Complex = Complex {real: f64::NEG_INFINITY, imaginary: f64::INFINITY, is_nan: false, is_infinite: true};
	const NEG_INF_ZERO: Complex = Complex {real: f64::NEG_INFINITY, imaginary: 0.0, is_nan: false, is_infinite: true};
	const NEG_INF_ONE: Complex = Complex {real: f64::NEG_INFINITY, imaginary: 1.0, is_nan: false, is_infinite: true};
	const NEG_INF_NAN: Complex = Complex {real: f64::NEG_INFINITY, imaginary: f64::NAN, is_nan: true, is_infinite: true};
	const NEG_INF_NEG_INF: Complex = Complex {real: f64::NEG_INFINITY, imaginary: f64::NEG_INFINITY, is_nan: false, is_infinite: true};
	const ONE_NAN: Complex = Complex {real: 1.0, imaginary: f64::NAN, is_nan: true, is_infinite: false};
	const ZERO_INF: Complex = Complex {real: 0.0, imaginary: f64::INFINITY, is_nan: false, is_infinite: true};
	const ZERO_NAN: Complex = Complex {real: 0.0, imaginary: f64::NAN, is_nan: true, is_infinite: false};
	const NAN_INF: Complex = Complex {real: f64::NAN, imaginary: f64::INFINITY, is_nan: true, is_infinite: true};
	const NAN_NEG_INF: Complex = Complex {real: f64::NAN, imaginary: f64::NEG_INFINITY, is_nan: true, is_infinite: true};
	const NAN_ZERO: Complex = Complex {real: f64::NAN, imaginary: 0.0, is_nan: true, is_infinite: false};

    
    
	#[test]
	fn nan() {
		let c: Complex = Complex::new(f64::NAN, f64::NAN);
		assert!(c.is_nan());
	}
	
	#[test]
	fn constructor_test() {
		let c: Complex = Complex::new(3.0, 4.0);
		assert_that(c.get_real(),is(close_to(3.0,0.00001)));
		assert_that(c.get_imaginary(),is(close_to(4.0,0.00001)));
		
	}
	
	#[test]
	fn test_constructor_nan() {
        let z: Complex = Complex::new(3.0, f64::NAN);
        assert!(z.is_nan());

        let z: Complex = Complex::new(f64::NAN, 4.0);
        assert!(z.is_nan());
 
        let z: Complex = Complex::new(3.0, 4.0);
        assert!(!z.is_nan());
    }
	
	#[test]
	fn test_abs() {
		let z: Complex = Complex::new(3.0,4.0);
		assert_that(z.abs(), is(close_to(5.0,0.0001)))
	}
	
	#[test]
    fn test_abs_nan() {
        assert!(f64::is_nan(complex::NAN.abs()));
        let z: Complex = Complex::new(f64::INFINITY, f64::NAN);
        assert!(f64::is_nan(z.abs()));
    }

    #[test]
    fn test_abs_infinite() {
        let z: Complex = Complex::new(f64::INFINITY, 0.0);
        assert_that(f64::INFINITY,is(close_to( z.abs(), 0.0)));
        let z: Complex = Complex::new(0.0, f64::NEG_INFINITY);
        assert_that(f64::INFINITY,is(close_to( z.abs(), 0.0)));
        let z: Complex = Complex::new(f64::INFINITY, f64::NEG_INFINITY);
        assert_that(f64::INFINITY,is(close_to( z.abs(), 0.0)));
    }

    #[test]
    fn test_add() {
        let x: Complex = Complex::new(3.0, 4.0);
        let y: Complex = Complex::new(5.0, 6.0);
        let z: Complex = x.add(&y);
        assert_that(8.0, is(close_to(z.get_real(), 1.0e-5)));
        assert_that(10.0, is(close_to(z.get_imaginary(), 1.0e-5)));
    }

    #[test]
    fn testadd_nan() {
        let x: Complex = Complex::new(3.0, 4.0);
        let z: Complex = x.add(&complex::NAN);
        assert!(z.is_nan());
        let z = Complex::new(1.0, f64::NAN);
        let w: Complex = x.add(&z);
        assert!(w.is_nan());
    }

    #[test]
    fn test_add_inf() {
        let x = Complex::new(1.0, 1.0);
        let z = Complex::new(f64::INFINITY, 0.0);
        let w: Complex = x.add(&z);
        assert!(w.get_imaginary() ==  1.0);
        assert!(f64::INFINITY == w.get_real());

        let x = Complex::new(f64::NEG_INFINITY, 0.0);
        assert!(f64::is_nan(x.add(&z).get_real()));
    }


    #[test]
    fn test_scalar_add() {
        let x = Complex::new(3.0, 4.0);
        let y_double: f64 = 2.0;
        let y_complex = Complex::new_real(y_double);
        assert!(x.add(&y_complex) == x.add_f64(y_double));
    }

    #[test]
    fn test_scalar_add_nan() {
        let x = Complex::new(3.0, 4.0);
        let y_double: f64 = f64::NAN;
        let y_complex = Complex::new_real(y_double);
        assert!(x.add(&y_complex) == x.add_f64(y_double));
    }

    #[test]
    fn test_scalar_add_inf() {
        let x = Complex::new(1.0, 1.0);
        let y_double: f64 = f64::INFINITY;

        let y_complex = Complex::new_real(y_double);
        assert!(x.add(&y_complex) == x.add_f64(y_double));

        let x = Complex::new(f64::INFINITY, 0.0);
        assert!(x.add(&y_complex) == x.add_f64(y_double));
    }


    #[test]
    fn test_conjugate() {
        let x = Complex::new(3.0, 4.0);
        let z: Complex = x.conjugate();
        assert_that(3.0, is(close_to(z.get_real(), 1.0e-5)));
        assert_that(-4.0, is(close_to(z.get_imaginary(), 1.0e-5)));
    }

    #[test]
    fn test_conjugate_nan() {
        let z = complex::NAN.conjugate();
        assert!(z.is_nan());
    }


    #[test]
    fn test_conjugate_infiinite() {
        let z = Complex::new(0.0, f64::INFINITY);
        assert_that(f64::INFINITY, is(close_to(z.conjugate().get_imaginary(), 0.0)));
        let z = Complex::new(0.0, f64::INFINITY);
        assert_that(f64::INFINITY, is(close_to(z.conjugate().get_imaginary(), 0.0)));
    }

    #[test]
    fn test_divide() {
        let x = Complex::new(3.0, 4.0);
        let y = Complex::new(5.0, 6.0);
        let z: Complex = x.divide(&y);
        assert_that(39.0 / 61.0, is(close_to(z.get_real(), 1.0e-5)));
        assert_that(2.0 / 61.0, is(close_to(z.get_imaginary(), 1.0e-5)));
    }

    #[test]
    fn test_divide_real() {
        let x = Complex::new(2f64, 3f64);
        let y = Complex::new(2f64, 0f64);
        assert!(Complex::new(1f64, 1.5f64) == x.divide(&y));

    }


    #[test]
    fn test_divide_imaginary() {
        let x = Complex::new(2f64, 3f64);
        let y = Complex::new(0f64, 2f64);
        assert!(Complex::new(1.5f64	, -1f64) == x.divide(&y));
    }

    #[test]
    fn test_divide_inf() {
        let x = Complex::new(3.0, 4.0);
        let w = Complex::new(f64::NEG_INFINITY, f64::INFINITY);
        assert!(x.divide(&w) == complex::ZERO);

        let z = w.divide(&x);
        assert!(f64::is_nan(z.get_real()));
        assert_that(f64::INFINITY, is(close_to(z.get_imaginary(), 0.0)));

        let w = Complex::new(f64::INFINITY, f64::INFINITY);
        let z = w.divide(&x);
        assert!(f64::is_nan(z.get_imaginary()));
        assert_that(f64::INFINITY, is(close_to(z.get_real(), 0.0)));

        let w = Complex::new(1.0, f64::INFINITY);
        let z = w.divide(&w);
        assert!(f64::is_nan(z.get_real()));
        assert!(f64::is_nan(z.get_imaginary()));
    }

    #[test]
    fn test_divide_zero() {
        let x = Complex::new(3.0, 4.0);
        let z: Complex = x.divide(&complex::ZERO);
        // assert_that(z, Complex.INF); // See MATH-657
        assert!(z == complex::NAN);
    }

    #[test]
    fn test_divide_zero_zero() {
        let x = Complex::new(0.0, 0.0);
        let z: Complex = x.divide(&complex::ZERO);
        assert!(z == complex::NAN);
    }

    #[test]
    fn test_divide_nan() {
        let x = Complex::new(3.0, 4.0);
        let z: Complex = x.divide(&complex::NAN);
        assert!(z.is_nan());
    }

    #[test]
    fn test_divide_nan_inf() {
       let z: Complex = ONE_INF.divide(&complex::ONE);
       assert!(f64::is_nan(z.get_real()));
       assert_that(f64::INFINITY,is(close_to( z.get_imaginary(), 0.0)));

       let z = NEG_INF_NEG_INF.divide(&ONE_NAN);
       assert!(f64::is_nan(z.get_real()));
       assert!(f64::is_nan(z.get_imaginary()));

       let z = NEG_INF_INF.divide(&complex::ONE);
       assert!(f64::is_nan(z.get_real()));
       assert!(f64::is_nan(z.get_imaginary()));
    }

    #[test]
    fn test_scalar_divide() {
        let x = Complex::new(3.0, 4.0);
        let y_double: f64 = 2.0;
        let y_complex = Complex::new_real(y_double);
        assert!(x.divide(&y_complex) == x.divide_f64(y_double));
    }

    #[test]
    fn test_scalar_divide_nan() {
        let x = Complex::new(3.0, 4.0);
        let y_double: f64 = f64::NAN;
        let y_complex = Complex::new_real(y_double);
        assert!(x.divide(&y_complex) == x.divide_f64(y_double));
    }

    #[test]
    fn test_scalar_divide_inf() {
        let x = Complex::new(1.0,1.0);
        let y_double: f64 = f64::INFINITY;
        let y_complex = Complex::new_real(y_double);
        assert!(x.divide(&y_complex) == x.divide_f64(y_double));

        let y_double = f64::NEG_INFINITY;
        let y_complex = Complex::new_real(y_double);
        assert!(x.divide(&y_complex) == x.divide_f64(y_double));

        let x = Complex::new(1.0, f64::NEG_INFINITY);
        assert!(x.divide(&y_complex) == x.divide_f64(y_double));
    }

    #[test]
    fn test_scalar_divide_zero() {
        let x = Complex::new(1.0,1.0);
        assert!(x.divide(&complex::ZERO) == x.divide_f64(0.0));
    }
    
    use std::mem;
    
    fn ulp(f: &f64)-> f64 {
    	if f64::is_infinite(*f) {
    		f64::INFINITY
    	} else {
    	  	let au64: u64 = unsafe { mem::transmute(*f) };
    	  	println!("au64     {:?}",au64);
    	  	println!("au64 ^ 1 {:?}",au64 ^ 1u64);
    	  	let af64: f64 = unsafe { mem::transmute(au64 ^ 1u64) };
    	  	println!("af64     {:?}",af64);
    	  	println!("abs(f-af64)     {:?}",f64::abs(f - af64));
    		f64::abs(f - af64)
    	}
    }

    #[test]
    fn test_reciprocal() {
        let z = Complex::new(5.0, 6.0);
        let act = z.reciprocal();
        let exp_re = 5.0 / 61.0;
        let exp_im = -6.0 / 61.0;
        assert_within_delta!(exp_re, act.get_real(), ulp(&exp_re));
        assert_within_delta!(exp_im, act.get_imaginary(), 2.0 * ulp(&exp_im));
        // assert::Assert::differs_within_delta(expRe, act.get_real(), ulp(&expRe));
        // assert::Assert::differs_within_delta(expIm, act.get_imaginary(), ulp(&expIm));
    }
    
    #[test]
    pub fn  test_reciprocal_real()   {
         let z: Complex = Complex::new(-2.0, 0.0);
       assert_eq!(&Complex::new(-0.5, 0.0), &z.reciprocal());
    }

    #[test]
    pub fn  test_reciprocal_imaginary()  {
         let z: Complex = Complex::new(0.0, -2.0);
       assert_eq!(&Complex::new(0.0, 0.5), &z.reciprocal());
    }

    #[test]
    pub fn  test_reciprocal_inf()  {
         let mut z: Complex = Complex::new(F64_NEG_INF, F64_INF);
       assert!(z.reciprocal().equals(&complex::ZERO));
        z = Complex::new(1.0, F64_INF).reciprocal();
       assert!(z.equals(&complex::ZERO));
    }
    
    #[test]
    pub fn  test_reciprocal_zero()  {
        assert_eq!(&complex::ZERO.reciprocal(), &complex::INF);
    }
    
    #[test]
    pub fn  test_reciprocal_nan()  {
       assert!(complex::NAN.reciprocal().is_nan());
    }

    #[test]
    pub fn  test_multiply()  {
         let x: Complex = Complex::new(3.0, 4.0);
         let y: Complex = Complex::new(5.0, 6.0);
         let z: Complex = x.multiply(&y);
        assert_within_delta!(-9.0, z.get_real(), 1.0e-5);
        assert_within_delta!(38.0, z.get_imaginary(), 1.0e-5);
    }

    #[test]
    pub fn  test_multiply_nan()  {
         let x: Complex = Complex::new(3.0, 4.0);
         let mut z: Complex = x.multiply(&complex::NAN);
        assert_eq!(&complex::NAN, &z);
        z = complex::NAN.multiply_i32(5);
        assert_eq!(&complex::NAN, &z);
    }
    
	#[test]
    pub fn  test_multiply_inf_inf()  {
        // Assert.assertTrue(infInf.multiply(infInf).isNaN()); // MATH-620
       assert!(&INF_INF.multiply(&INF_INF).is_infinite());
    }

    #[test]
    pub fn  test_multiply_nan_inf()  {
         let mut z: Complex = Complex::new(1.0, 1.0);
         let mut w: Complex = z.multiply(&INF_ONE);
        assert_within_delta!(w.get_real(), f64::INFINITY, 0.0);
        assert_within_delta!(w.get_imaginary(), f64::INFINITY, 0.0);
        // [MATH-164]
       assert!(&Complex::new(1.0, 0.0).multiply(&INF_INF).equals(&complex::INF));
       assert!(&Complex::new(-1.0, 0.0).multiply(&INF_INF).equals(&complex::INF));
       assert!(&Complex::new(1.0, 0.0).multiply(&NEG_INF_ZERO).equals(&complex::INF));
        w = ONE_INF.multiply(&ONE_NEG_INF);
        assert_within_delta!(w.get_real(), f64::INFINITY, 0.0);
        assert_within_delta!(w.get_imaginary(), f64::INFINITY, 0.0);
        w = NEG_INF_NEG_INF.multiply(&ONE_NAN);
       assert!(f64::is_nan(w.get_real()));
       assert!(f64::is_nan(w.get_imaginary()));
        z = Complex::new(1.0, f64::NEG_INFINITY);
        assert_eq!(&complex::INF, &z.multiply(&z));
    }

    #[test]
    pub fn  test_scalar_multiply()  {
         let x: Complex = Complex::new(3.0, 4.0);
         let y_double: f64 = 2.0;
         let y_complex: Complex = Complex::new(y_double, 0.0);
        assert_eq!(&x.multiply(&y_complex), &x.multiply_f64(y_double));
         let z_int: i32 = -5;
         let z_complex: Complex = Complex::new(z_int as f64, 0.0);
        assert_eq!(&x.multiply(&z_complex), &x.multiply_i32(z_int));
    }
    
     #[test]
    pub fn  test_scalar_multiply_nan()  {
         let x: Complex = Complex::new(3.0, 4.0);
         let y_double: f64 = f64::NAN;
         let y_complex: Complex = Complex::new(y_double, 0.0);
        assert_eq!(&x.multiply(&y_complex), &x.multiply_f64(y_double));
    }

    #[test]
    pub fn  test_scalar_multiply_inf()  {
         let x: Complex = Complex::new(1.0, 1.0);
         let mut y_double: f64 = f64::INFINITY;
         let mut y_complex: Complex = Complex::new(y_double, 0.0);
        assert_eq!(&x.multiply(&y_complex), &x.multiply_f64(y_double));
        y_double = f64::NEG_INFINITY;
        y_complex = Complex::new(y_double, 0.0);
        assert_eq!(&x.multiply(&y_complex), &x.multiply_f64(y_double));
    }

    #[test]
    pub fn  test_negate()  {
         let x: Complex = Complex::new(3.0, 4.0);
         let z: Complex = x.negate();
        assert_within_delta!(-3.0, z.get_real(), 1.0e-5);
        assert_within_delta!(-4.0, z.get_imaginary(), 1.0e-5);
    }

    #[test]
    pub fn  test_negate_na_n()  {
         let z: Complex = complex::NAN.negate();
       assert!(&z.is_nan());
    }


 #[test]
    pub fn  test_subtract()  {
         let x: Complex = Complex::new(3.0, 4.0);
         let y: Complex = Complex::new(5.0, 6.0);
         let z: Complex = x.subtract(&y);
        assert_within_delta!(-2.0, z.get_real(), 1.0e-5);
        assert_within_delta!(-2.0, z.get_imaginary(), 1.0e-5);
    }

    #[test]
    pub fn  test_subtract_nan()  {
         let x: Complex = Complex::new(3.0, 4.0);
         let mut z: Complex = x.subtract(&complex::NAN);
        assert_eq!(&complex::NAN, &z);
        z = Complex::new(1.0, f64::NAN);
         let w: Complex = x.subtract(&z);
        assert_eq!(&complex::NAN, &w);
    }

    #[test]
    pub fn  test_subtract_inf()  {
         let mut x: Complex = Complex::new(1.0, 1.0);
         let z: Complex = Complex::new(f64::NEG_INFINITY, 0.0);
         let w: Complex = x.subtract(&z);
        assert_within_delta!(w.get_imaginary(), 1.0, 0.0);
        assert_within_delta!(f64::INFINITY, w.get_real(), 0.0);
        x = Complex::new(f64::NEG_INFINITY, 0.0);
       assert!(f64::is_nan(x.subtract(&z).get_real()));
    }

    #[test]
    pub fn  test_scalar_subtract()  {
         let x: Complex = Complex::new(3.0, 4.0);
         let y_double: f64 = 2.0;
         let y_complex: Complex = Complex::new(y_double,0.0);
        assert_eq!(&x.subtract(&y_complex), &x.subtract_f64(y_double));
    }

    #[test]
    pub fn  test_scalar_subtract_na_n()  {
         let x: Complex = Complex::new(3.0, 4.0);
         let y_double: f64 = f64::NAN;
         let y_complex: Complex = Complex::new(y_double,0.0);
        assert_eq!(&x.subtract(&y_complex), &x.subtract_f64(y_double));
    }

    #[test]
    pub fn  test_scalar_subtract_inf()  {
         let mut x: Complex = Complex::new(1.0, 1.0);
         let y_double: f64 = f64::INFINITY;
         let y_complex: Complex = Complex::new(y_double,0.0);
        assert_eq!(&x.subtract(&y_complex), &x.subtract_f64(y_double));
        x = Complex::new(f64::NEG_INFINITY, 0.0);
        assert_eq!(&x.subtract(&y_complex), &x.subtract_f64(y_double));
    }


     #[test]
    pub fn  test_floating_point_equals()  {
         let mut re: f64 = -3.21;
         let mut im: f64 = 456789e10;
         let x: Complex = Complex::new(re, im);
         let mut y: Complex = Complex::new(re, im);
       assert!(x.equals(&y));
       assert!(Complex::equals_static(&x, &y));
         let max_ulps: i32 = 5;
         {
             let mut i: i32 = 0;
            while i < max_ulps {
                {
                    re = F64::next_up(re);
                    im = F64::next_up(im);
                }
                i += 1;
             }
         }

        y = Complex::new(re, im);
        assert!(Complex::equals_ulps(&x, &y, max_ulps));
        re = F64::next_up(re);
        im = F64::next_up(im);
        y = Complex::new(re, im);
        assert!(!Complex::equals_ulps(&x, &y, max_ulps));
    }

    #[test]
    pub fn  test_floating_point_equals_na_n()  {
         let mut c: Complex = Complex::new(f64::NAN, 1.0);
        assert!(!&Complex::equals_static(&c, &c));
        c = Complex::new(1.0, f64::NAN);
        assert!(!&Complex::equals_static(&c, &c));
    }
   

    #[test]
    pub fn  test_floating_point_equals_with_allowed_delta()  {
         let re: f64 = 153.0000;
         let im: f64 = 152.9375;
         let tol1: f64 = 0.0625;
         let x: Complex = Complex::new(re, im);
         let y: Complex = Complex::new(re + tol1, im + tol1);
       assert!(Complex::equals_eps(&x, &y, tol1));
         let tol2: f64 = 0.0624;
        assert!(!Complex::equals_eps(&x, &y, tol2));
    }


    #[test]
    pub fn  test_floating_point_equals_with_allowed_delta_nan()  {
         let x: Complex = Complex::new(0.0, f64::NAN);
         let y: Complex = Complex::new(f64::NAN, 0.0);
        assert!(!Complex::equals_eps(&x, &complex::ZERO, 0.1));
        assert!(!Complex::equals_eps(&x, &x, 0.1));
        assert!(!Complex::equals_eps(&x, &y, 0.1));
    }

    #[test]
    pub fn  test_floating_point_equals_with_relative_tolerance()  {
         let tol: f64 = 1e-4;
         let re: f64 = 1.0;
         let im: f64 = 1e10;
         let f: f64 = 1.0 + tol;
         let x: Complex = Complex::new(re, im);
         let y: Complex = Complex::new(re * f, im * f);
       assert!(Complex::equals_with_relative_tolerance(&x, &y, tol));
    }

    #[test]
    pub fn  test_floating_point_equals_with_relative_tolerance_na_n()  {
         let x: Complex = Complex::new(0.0, f64::NAN);
         let y: Complex = Complex::new(f64::NAN, 0.0);
        assert!(!Complex::equals_with_relative_tolerance(&x, &complex::ZERO, 0.1));
        assert!(!Complex::equals_with_relative_tolerance(&x, &x, 0.1));
        assert!(!Complex::equals_with_relative_tolerance(&x, &y, 0.1));
    }

 #[test]
    pub fn  test_equals_same()  {
         let x: Complex = Complex::new(3.0, 4.0);
       assert!(x.equals(&x));
    }

   
    #[test]
    pub fn  test_equals_true()  {
         let x: Complex = Complex::new(3.0, 4.0);
         let y: Complex = Complex::new(3.0, 4.0);
       assert!(x.equals(&y));
    }

    #[test]
    pub fn  test_equals_real_difference()  {
         let x: Complex = Complex::new(0.0, 0.0);
         let y: Complex = Complex::new(0.0 + f64::MIN, 0.0);
        assert!(!x.equals(&y));
    }

    #[test]
    pub fn  test_equals_imaginary_difference()  {
         let x: Complex = Complex::new(0.0, 0.0);
         let y: Complex = Complex::new(0.0, 0.0 + f64::MIN);
        assert!(!&x.equals(&y));
    }

    #[test]
    pub fn  test_equals_nan()  {
         let real_nan: Complex = Complex::new(f64::NAN, 0.0);
         let imaginary_nan: Complex = Complex::new(0.0, f64::NAN);
         let complex_nan: Complex = complex::NAN;
       assert!(real_nan.equals(&imaginary_nan));
       assert!(imaginary_nan.equals(&complex_nan));
       assert!(real_nan.equals(&complex_nan));
    }
    
     #[test]
    pub fn  test_acos()  {
         let z: Complex = Complex::new(3.0, 4.0);
         let expected: Complex = Complex::new(0.936812, -2.30551);
        TestUtils::assert_equals_complex(&expected, &z.acos(), 1.0e-5);
        TestUtils::assert_equals_complex(&Complex::new(F64::acos(0.0), 0.0), &complex::ZERO.acos(), 1.0e-12);
    }

	#[test]
    pub fn  test_acos_inf()  {
        assert!(&ONE_INF.acos().is_nan());
        assert!(&ONE_NEG_INF.acos().is_nan());
        assert!(&INF_ONE.acos().is_nan());
        assert!(&NEG_INF_ONE.acos().is_nan());
        assert!(&INF_INF.acos().is_nan());
        assert!(&INF_NEG_INF.acos().is_nan());
        assert!(&NEG_INF_INF.acos().is_nan());
        assert!(&NEG_INF_NEG_INF.acos().is_nan());
    }

	#[test]
    pub fn  test_acos_na_n()  {
       assert!(&complex::NAN.acos().is_nan());
    }

    #[test]
    pub fn  test_asin()  {
         let z: Complex = Complex::new(3.0, 4.0);
         let expected: Complex = Complex::new(0.633984, 2.30551);
        TestUtils::assert_equals_complex(&expected, &z.asin(), 1.0e-5);
    }

    #[test]
    pub fn  test_asin_nan()  {
       assert!(complex::NAN.asin().is_nan());
    }

    #[test]
    pub fn  test_asin_inf()  {
        assert!(ONE_INF.asin().is_nan());
        assert!(ONE_NEG_INF.asin().is_nan());
        assert!(INF_ONE.asin().is_nan());
        assert!(NEG_INF_ONE.asin().is_nan());
        assert!(INF_INF.asin().is_nan());
        assert!(INF_NEG_INF.asin().is_nan());
        assert!(NEG_INF_INF.asin().is_nan());
        assert!(NEG_INF_NEG_INF.asin().is_nan());
    }
    
    
    #[test]
    pub fn  test_atan()  {
         let z: Complex = Complex::new(3.0, 4.0);
         let expected: Complex = Complex::new(1.44831, 0.158997);
        TestUtils::assert_equals_complex(&expected, &z.atan(), 1.0e-5);
    }

    #[test]
    pub fn  test_atan_inf()  {
        assert!(ONE_INF.atan().is_nan());
        assert!(ONE_NEG_INF.atan().is_nan());
        assert!(INF_ONE.atan().is_nan());
        assert!(NEG_INF_ONE.atan().is_nan());
        assert!(INF_INF.atan().is_nan());
        assert!(INF_NEG_INF.atan().is_nan());
        assert!(NEG_INF_INF.atan().is_nan());
        assert!(NEG_INF_NEG_INF.atan().is_nan());
    }

    #[test]
    pub fn  test_atan_i()  {
       assert!(complex::I.atan().is_nan());
    }

    #[test]
    pub fn  test_atan_nan()  {
       assert!(complex::NAN.atan().is_nan());
    }

    #[test]
    pub fn  test_cos()  {
         let z: Complex = Complex::new(3.0, 4.0);
         let expected: Complex = Complex::new(-27.03495, -3.851153);
        TestUtils::assert_equals_complex(&expected, &z.cos(), 1.0e-5);
    }

    #[test]
    pub fn  test_cos_nan()  {
       assert!(complex::NAN.cos().is_nan());
    }

    #[test]
    pub fn  test_cos_inf()  {
        TestUtils::assert_same_complex(&INF_NEG_INF, &ONE_INF.cos());
        TestUtils::assert_same_complex(&INF_INF, &ONE_NEG_INF.cos());
        assert!(INF_ONE.cos().is_nan());
        assert!(NEG_INF_ONE.cos().is_nan());
        assert!(INF_INF.cos().is_nan());
        assert!(INF_NEG_INF.cos().is_nan());
        assert!(NEG_INF_INF.cos().is_nan());
        assert!(NEG_INF_NEG_INF.cos().is_nan());
    }

    #[test]
    pub fn  test_cosh()  {
         let z: Complex = Complex::new(3.0, 4.0);
         let expected: Complex = Complex::new(-6.58066, -7.58155);
        TestUtils::assert_equals_complex(&expected, &z.cosh(), 1.0e-5);
    }

    #[test]
    pub fn  test_cosh_nan()  {
       assert!(complex::NAN.cosh().is_nan());
    }

    #[test]
    pub fn  test_cosh_inf()  {
        assert!(ONE_INF.cosh().is_nan());
        assert!(ONE_NEG_INF.cosh().is_nan());
        TestUtils::assert_same_complex(&INF_INF, &INF_ONE.cosh());
        TestUtils::assert_same_complex(&INF_NEG_INF, &NEG_INF_ONE.cosh());
        assert!(INF_INF.cosh().is_nan());
        assert!(INF_NEG_INF.cosh().is_nan());
        assert!(NEG_INF_INF.cosh().is_nan());
        assert!(NEG_INF_NEG_INF.cosh().is_nan());
    }

    #[test]
    pub fn  test_exp()  {
         let z: Complex = Complex::new(3.0, 4.0);
         let expected: Complex = Complex::new(-13.12878, -15.20078);
        TestUtils::assert_equals_complex(&expected, &z.exp(), 1.0e-5);
        TestUtils::assert_equals_complex(&complex::ONE, &complex::ZERO.exp(), 10e-12);
         let i_pi: Complex = complex::I.multiply(&Complex::new(f64::consts::PI, 0.0));
        TestUtils::assert_equals_complex(&complex::ONE.negate(), &i_pi.exp(), 10e-12);
    }

    #[test]
    pub fn  test_exp_nan()  {
       assert!(complex::NAN.exp().is_nan());
    }

    #[test]
    pub fn  test_exp_inf1()  {
        assert!(ONE_INF.exp().is_nan());
    }

    #[test]
    pub fn  test_exp_inf2()  {
        assert!(ONE_NEG_INF.exp().is_nan());
    }

    #[test]
    pub fn  test_exp_inf3()  {
        TestUtils::assert_same_complex(&INF_INF, &INF_ONE.exp());
    }

    #[test]
    pub fn  test_exp_inf4()  {
         let exp: Complex = NEG_INF_ONE.exp();
        TestUtils::assert_same_complex(&complex::ZERO, &exp);
    }

    #[test]
    pub fn  test_exp_inf5()  {
        assert!(INF_INF.exp().is_nan());
    }

    #[test]
    pub fn  test_exp_inf6()  {
        assert!(INF_NEG_INF.exp().is_nan());
    }

    #[test]
    pub fn  test_exp_inf7()  {
        assert!(NEG_INF_INF.exp().is_nan());
    }

    #[test]
    pub fn  test_exp_inf8()  {
        assert!(NEG_INF_NEG_INF.exp().is_nan());
    }

    #[test]
    pub fn  test_log()  {
         let z: Complex = Complex::new(3.0, 4.0);
         let expected: Complex = Complex::new(1.60944, 0.927295);
        TestUtils::assert_equals_complex(&expected, &z.log(), 1.0e-5);
    }

    #[test]
    pub fn  test_log_nan()  {
       assert!(complex::NAN.log().is_nan());
    }

#[test]
    pub fn  test_log_inf()  {
        TestUtils::assert_equals_complex(&Complex::new(f64::INFINITY, f64::consts::PI / 2.0), &ONE_INF.log(), 10e-12);
        TestUtils::assert_equals_complex(&Complex::new(f64::INFINITY, -f64::consts::PI / 2.0), &ONE_NEG_INF.log(), 10e-12);
        TestUtils::assert_equals_complex(&INF_ZERO, &INF_ONE.log(), 10e-12);
        TestUtils::assert_equals_complex(&Complex::new(f64::INFINITY, f64::consts::PI), &NEG_INF_ONE.log(), 10e-12);
        TestUtils::assert_equals_complex(&Complex::new(f64::INFINITY, f64::consts::PI / 4.0), &INF_INF.log(), 10e-12);
        TestUtils::assert_equals_complex(&Complex::new(f64::INFINITY, -f64::consts::PI / 4.0), &INF_NEG_INF.log(), 10e-12);
        TestUtils::assert_equals_complex(&Complex::new(f64::INFINITY, 3.0 * f64::consts::PI / 4.0), &NEG_INF_INF.log(), 10e-12);
        TestUtils::assert_equals_complex(&Complex::new(f64::INFINITY, -3.0 * f64::consts::PI / 4.0), &NEG_INF_NEG_INF.log(), 10e-12);
    }

    #[test]
    pub fn  test_log_zero()  {
        TestUtils::assert_same_complex(&NEG_INF_ZERO, &complex::ZERO.log());
    }

 #[test]
    pub fn  test_pow()  {
         let x: Complex = Complex::new(3.0, 4.0);
         let y: Complex = Complex::new(5.0, 6.0);
         let expected: Complex = Complex::new(-1.860893, 11.83677);
        TestUtils::assert_equals_complex(&expected, &x.pow(&y), 1.0e-5);
    }

    #[test]
    pub fn  test_pow_na_n_base()  {
         let x: Complex = Complex::new(3.0, 4.0);
       assert!(complex::NAN.pow(&x).is_nan());
    }

    #[test]
    pub fn  test_pow_na_n_exponent()  {
         let x: Complex = Complex::new(3.0, 4.0);
       assert!(x.pow(&complex::NAN).is_nan());
    }

    #[test]
    pub fn  test_pow_inf()  {
        assert!(complex::ONE.pow(&ONE_INF).is_nan());
        assert!(complex::ONE.pow(&ONE_NEG_INF).is_nan());
        assert!(complex::ONE.pow(&INF_ONE).is_nan());
        assert!(complex::ONE.pow(&INF_INF).is_nan());
        assert!(complex::ONE.pow(&INF_NEG_INF).is_nan());
        assert!(complex::ONE.pow(&NEG_INF_INF).is_nan());
        assert!(complex::ONE.pow(&NEG_INF_NEG_INF).is_nan());
        assert!(INF_ONE.pow(&complex::ONE).is_nan());
        assert!(NEG_INF_ONE.pow(&complex::ONE).is_nan());
        assert!(INF_INF.pow(&complex::ONE).is_nan());
        assert!(INF_NEG_INF.pow(&complex::ONE).is_nan());
        assert!(NEG_INF_INF.pow(&complex::ONE).is_nan());
        assert!(NEG_INF_NEG_INF.pow(&complex::ONE).is_nan());
        assert!(NEG_INF_NEG_INF.pow(&INF_NEG_INF).is_nan());
        assert!(NEG_INF_NEG_INF.pow(&NEG_INF_NEG_INF).is_nan());
        assert!(NEG_INF_NEG_INF.pow(&INF_INF).is_nan());
        assert!(INF_INF.pow(&INF_NEG_INF).is_nan());
        assert!(INF_INF.pow(&NEG_INF_NEG_INF).is_nan());
        assert!(INF_INF.pow(&INF_INF).is_nan());
        assert!(INF_NEG_INF.pow(&INF_NEG_INF).is_nan());
        assert!(INF_NEG_INF.pow(&NEG_INF_NEG_INF).is_nan());
        assert!(INF_NEG_INF.pow(&INF_INF).is_nan());
    }

    #[test]
    pub fn  test_pow_zero()  {
        assert!(complex::ZERO.pow(&complex::ONE).is_nan());
        assert!(complex::ZERO.pow(&complex::ZERO).is_nan());
        assert!(complex::ZERO.pow(&complex::I).is_nan());
        TestUtils::assert_equals_complex(&complex::ONE, &complex::ONE.pow(&complex::ZERO), 10e-12);
        TestUtils::assert_equals_complex(&complex::ONE, &complex::I.pow(&complex::ZERO), 10e-12);
        TestUtils::assert_equals_complex(&complex::ONE, &Complex::new(-1.0, 3.0).pow(&complex::ZERO), 10e-12);
    }

    #[test]
    pub fn  test_scalar_pow()  {
         let x: Complex = Complex::new(3.0, 4.0);
         let y_double: f64 = 5.0;
         let y_complex: Complex = Complex::new(y_double,0.0);
        assert_eq!(&x.pow(&y_complex), &x.pow_f64(y_double));
    }

    #[test]
    pub fn  test_scalar_pow_na_n_base()  {
         let x: Complex = complex::NAN;
         let y_double: f64 = 5.0;
         let y_complex: Complex = Complex::new(y_double, 0.0);
        assert_eq!(&x.pow(&y_complex), &x.pow_f64(y_double));
    }

    #[test]
    pub fn  test_scalar_pow_na_n_exponent()  {
         let x: Complex = Complex::new(3.0, 4.0);
         let y_double: f64 = f64::NAN;
         let y_complex: Complex = Complex::new(y_double, 0.0);
        assert_eq!(&x.pow(&y_complex), &x.pow_f64(y_double));
    }

    #[test]
    pub fn  test_scalar_pow_inf()  {
        assert!(complex::ONE.pow_f64(f64::INFINITY).is_nan());
        assert!(complex::ONE.pow_f64(f64::NEG_INFINITY).is_nan());
        assert!(INF_ONE.pow_f64(1.0).is_nan());
        assert!(NEG_INF_ONE.pow_f64(1.0).is_nan());
        assert!(INF_INF.pow_f64(1.0).is_nan());
        assert!(INF_NEG_INF.pow_f64(1.0).is_nan());
        assert!(NEG_INF_INF.pow_f64(10.0).is_nan());
        assert!(NEG_INF_NEG_INF.pow_f64(1.0).is_nan());
        assert!(NEG_INF_NEG_INF.pow_f64(f64::INFINITY).is_nan());
        assert!(NEG_INF_NEG_INF.pow_f64(f64::INFINITY).is_nan());
        assert!(INF_INF.pow_f64(f64::INFINITY).is_nan());
        assert!(INF_INF.pow_f64(f64::NEG_INFINITY).is_nan());
        assert!(INF_NEG_INF.pow_f64(f64::NEG_INFINITY).is_nan());
        assert!(INF_NEG_INF.pow_f64(f64::INFINITY).is_nan());
    }
    
      #[test]
    pub fn  test_scalar_pow_zero()  {
        assert!(complex::ZERO.pow_f64(1.0).is_nan());
        assert!(complex::ZERO.pow_f64(0.0).is_nan());
        TestUtils::assert_equals_complex(&complex::ONE, &complex::ONE.pow_f64(0.0), 10e-12);
        TestUtils::assert_equals_complex(&complex::ONE, &complex::I.pow_f64(0.0), 10e-12);
        TestUtils::assert_equals_complex(&complex::ONE, &Complex::new(-1.0, 3.0).pow_f64(0.0), 10e-12);
    }

   
    #[test]
    pub fn  test_sin()  {
         let z: Complex = Complex::new(3.0, 4.0);
         let expected: Complex = Complex::new(3.853738, -27.01681);
        TestUtils::assert_equals_complex(&expected, &z.sin(), 1.0e-5);
    }

    #[test]
    pub fn  test_sin_inf()  {
        TestUtils::assert_same_complex(&INF_INF, &ONE_INF.sin());
        TestUtils::assert_same_complex(&INF_NEG_INF, &ONE_NEG_INF.sin());
        assert!(INF_ONE.sin().is_nan());
        assert!(NEG_INF_ONE.sin().is_nan());
        assert!(INF_INF.sin().is_nan());
        assert!(INF_NEG_INF.sin().is_nan());
        assert!(NEG_INF_INF.sin().is_nan());
        assert!(NEG_INF_NEG_INF.sin().is_nan());
    }

    #[test]
    pub fn  test_sin_nan()  {
       assert!(complex::NAN.sin().is_nan());
    }

    #[test]
    pub fn  test_sinh()  {
         let z: Complex = Complex::new(3.0, 4.0);
         let expected: Complex = Complex::new(-6.54812, -7.61923);
        TestUtils::assert_equals_complex(&expected, &z.sinh(), 1.0e-5);
    }

    #[test]
    pub fn  test_sinh_nan()  {
       assert!(complex::NAN.sinh().is_nan());
    }
  

    #[test]
    pub fn  test_sinh_inf()  {
        assert!(ONE_INF.sinh().is_nan());
        assert!(ONE_NEG_INF.sinh().is_nan());
        TestUtils::assert_same_complex(&INF_INF, &INF_ONE.sinh());
        TestUtils::assert_same_complex(&NEG_INF_INF, &NEG_INF_ONE.sinh());
        assert!(INF_INF.sinh().is_nan());
        assert!(INF_NEG_INF.sinh().is_nan());
        assert!(NEG_INF_INF.sinh().is_nan());
        assert!(NEG_INF_NEG_INF.sinh().is_nan());
    }

    #[test]
    pub fn  test_sqrt_real_positive()  {
         let z: Complex = Complex::new(3.0, 4.0);
         let expected: Complex = Complex::new(2.0, 1.0);
        TestUtils::assert_equals_complex(&expected, &z.sqrt(), 1.0e-5);
    }

    #[test]
    pub fn  test_sqrt_real_zero()  {
         let z: Complex = Complex::new(0.0, 4.0);
         let expected: Complex = Complex::new(1.41421, 1.41421);
        TestUtils::assert_equals_complex(&expected, &z.sqrt(), 1.0e-5);
    }

    #[test]
    pub fn  test_sqrt_real_negative()  {
         let z: Complex = Complex::new(-3.0, 4.0);
         let expected: Complex = Complex::new(1.0, 2.0);
        TestUtils::assert_equals_complex(&expected, &z.sqrt(), 1.0e-5);
    }

    #[test]
    pub fn  test_sqrt_imaginary_zero()  {
         let z: Complex = Complex::new(-3.0, 0.0);
         let expected: Complex = Complex::new(0.0, 1.73205);
        TestUtils::assert_equals_complex(&expected, &z.sqrt(), 1.0e-5);
    }

    #[test]
    pub fn  test_sqrt_imaginary_negative()  {
         let z: Complex = Complex::new(-3.0, -4.0);
         let expected: Complex = Complex::new(1.0, -2.0);
        TestUtils::assert_equals_complex(&expected, &z.sqrt(), 1.0e-5);
    }

      #[test]
    pub fn  test_sqrt_nan()  {
       assert!(complex::NAN.sqrt().is_nan());
    }

    #[test]
    pub fn  test_sqrt_inf()  {
        TestUtils::assert_same_complex(&INF_NAN, &ONE_INF.sqrt());
        TestUtils::assert_same_complex(&INF_NAN, &ONE_NEG_INF.sqrt());
        TestUtils::assert_same_complex(&INF_ZERO, &INF_ONE.sqrt());
        TestUtils::assert_same_complex(&ZERO_INF, &NEG_INF_ONE.sqrt());
        TestUtils::assert_same_complex(&INF_NAN, &INF_INF.sqrt());
        TestUtils::assert_same_complex(&INF_NAN, &INF_NEG_INF.sqrt());
        TestUtils::assert_same_complex(&NAN_INF, &NEG_INF_INF.sqrt());
        TestUtils::assert_same_complex(&NAN_NEG_INF, &NEG_INF_NEG_INF.sqrt());
    }

    #[test]
    pub fn  test_sqrt1z()  {
         let z: Complex = Complex::new(3.0, 4.0);
         let expected: Complex = Complex::new(4.08033, -2.94094);
        TestUtils::assert_equals_complex(&expected, &z.sqrt1z(), 1.0e-5);
    }

    #[test]
    pub fn  test_sqrt1z_na_n()  {
       assert!(complex::NAN.sqrt1z().is_nan());
    }

    #[test]
    pub fn  test_tan()  {
         let z: Complex = Complex::new(3.0, 4.0);
         let mut expected: Complex = Complex::new(-0.000187346, 0.999356);
        TestUtils::assert_equals_complex(&expected, &z.tan(), 1.0e-5);
        /* Check that no overflow occurs (MATH-722) */
         let mut actual: Complex = Complex::new(3.0, 1E10).tan();
        expected = Complex::new(0.0, 1.0);
        TestUtils::assert_equals_complex(&expected, &actual, 1.0e-5);
        actual = Complex::new(3.0, -1E10).tan();
        expected = Complex::new(0.0, -1.0);
        TestUtils::assert_equals_complex(&expected, &actual, 1.0e-5);
    }

    #[test]
    pub fn  test_tan_nan()  {
       assert!(complex::NAN.tan().is_nan());
    }

    #[test]
    pub fn  test_tan_inf()  {
        TestUtils::assert_same_complex(&Complex::value_of(0.0, 1.0), &ONE_INF.tan());
        TestUtils::assert_same_complex(&Complex::value_of(0.0, -1.0), &ONE_NEG_INF.tan());
        assert!(INF_ONE.tan().is_nan());
        assert!(NEG_INF_ONE.tan().is_nan());
        assert!(INF_INF.tan().is_nan());
        assert!(INF_NEG_INF.tan().is_nan());
        assert!(NEG_INF_INF.tan().is_nan());
        assert!(NEG_INF_NEG_INF.tan().is_nan());
    }

    #[test]
    pub fn  test_tan_critical()  {
        TestUtils::assert_same_complex(&INF_NAN, &Complex::new(f64::consts::PI / 2.0, 0.0).tan());
        TestUtils::assert_same_complex(&NEG_INF_NAN, &Complex::new(-f64::consts::PI / 2.0, 0.0).tan());
    }

    #[test]
    pub fn  test_tanh()  {
         let z: Complex = Complex::new(3.0, 4.0);
         let mut expected: Complex = Complex::new(1.00071, 0.00490826);
        TestUtils::assert_equals_complex(&expected, &z.tanh(), 1.0e-5);
        /* Check that no overflow occurs (MATH-722) */
         let mut actual: Complex = Complex::new(1E10, 3.0).tanh();
        expected = Complex::new(1.0, 0.0);
        TestUtils::assert_equals_complex(&expected, &actual, 1.0e-5);
        actual = Complex::new(-1E10, 3.0).tanh();
        expected = Complex::new(-1.0, 0.0);
        TestUtils::assert_equals_complex(&expected, &actual, 1.0e-5);
    }

 #[test]
    pub fn  test_tanh_nan()  {
       assert!(complex::NAN.tanh().is_nan());
    }

    #[test]
    pub fn  test_tanh_inf()  {
        assert!(ONE_INF.tanh().is_nan());
        assert!(ONE_NEG_INF.tanh().is_nan());
        TestUtils::assert_same_complex(&Complex::value_of(1.0, 0.0), &INF_ONE.tanh());
        TestUtils::assert_same_complex(&Complex::value_of(-1.0, 0.0), &NEG_INF_ONE.tanh());
        assert!(INF_INF.tanh().is_nan());
        assert!(INF_NEG_INF.tanh().is_nan());
        assert!(NEG_INF_INF.tanh().is_nan());
        assert!(NEG_INF_NEG_INF.tanh().is_nan());
    }

    #[test]
    pub fn  test_tanh_critical()  {
        TestUtils::assert_same_complex(&NAN_INF, &Complex::new(0.0, f64::consts::PI / 2.0).tanh());
    }

    /** test issue MATH-221 */
    #[test]
    pub fn  test_math221()  {
       assert!(Complex::equals(&Complex::new(0.0, -1.0), &Complex::new(0.0, 1.0).multiply(&Complex::new(-1.0, 0.0))));
    }

     #[test]
    pub fn  test_hash_code()  {
         let mut x: Complex = Complex::new(0.0, 0.0);
         let mut y: Complex = Complex::new(0.0, 0.0 + f64::MIN);
        assert!(x.hash_code() != y.hash_code());
        y = Complex::new(0.0 + f64::MIN, 0.0);
        assert!(x.hash_code() != y.hash_code());
         let real_na_n: Complex = Complex::new(f64::NAN, 0.0);
         let imaginary_na_n: Complex = Complex::new(0.0, f64::NAN);
        assert_eq!(real_na_n.hash_code(), imaginary_na_n.hash_code());
        assert_eq!(imaginary_na_n.hash_code(), complex::NAN.hash_code());
        // MATH-1118
        // "equals" and "hash_codeCode" must be compatible: if two objects have
        // different hash_code codes, "equals" must return false.
         let msg = "'equals' not compatible with 'hash_codeCode'";
        x = Complex::new(0.0, 0.0);
        y = Complex::new(0.0, -0.0);
       assert!(x.hash_code() == y.hash_code());
        assert!(x.equals(&y), msg);
        
        x = Complex::new(0.0, 0.0);
        y = Complex::new(-0.0, 0.0);
       assert!(x.hash_code() == y.hash_code());
        assert!(x.equals(&y), msg);
    }

       #[test]
       pub fn  test_sqrt_polar()  {
         let mut r: f64 = 1.0;
         {
             let mut i: i32 = 0;
            while i < 5 {
                {
                    r += i as f64;
                     let mut theta: f64 = 0.0;
                     {
                         let mut j: i32 = 0;
                        while j < 11 {
                            {
                                theta += f64::consts::PI / 12.0;
                                 let z: Complex = Complex::polar2_complex(r, theta).unwrap();
                                 let sqrtz: Complex = Complex::polar2_complex(f64::sqrt(r), theta / 2.0).unwrap();
                                TestUtils::assert_equals_complex(&sqrtz, &z.sqrt(), 10e-12);
                            }
                            j += 1;
                         }
                     }

                }
                i += 1;
             }
         }
    }

    /**
     * Test: computing <b>third roots</b> of z.
     * <pre>
     * <code>
     * <b>z = -2 + 2 * i</b>
     *   => z_0 =  1      +          i
     *   => z_1 = -1.3660 + 0.3660 * i
     *   => z_2 =  0.3660 - 1.3660 * i
     * </code>
     * </pre>
     */
    #[test]
    pub fn  test_nth_root_normal_third_root()  {
        // The complex number we want to compute all third-roots for.
         let z: Complex = Complex::new(-2.0, 2.0);
        // The List holding all third roots
         let third_roots_of_z = z.nth_root(3).unwrap();
        // Returned Collection must not be empty!
        assert_within_delta!(3, third_roots_of_z.len());
        // test z_0
        assert_within_delta!(1.0, third_roots_of_z[0].get_real(), 1.0e-5);
        assert_within_delta!(1.0, third_roots_of_z[0].get_imaginary(), 1.0e-5);
        // test z_1
        assert_within_delta!(-1.3660254037844386, third_roots_of_z[1].get_real(), 1.0e-5);
        assert_within_delta!(0.36602540378443843, third_roots_of_z[1].get_imaginary(), 1.0e-5);
        // test z_2
        assert_within_delta!(0.366025403784439, third_roots_of_z[2].get_real(), 1.0e-5);
        assert_within_delta!(-1.3660254037844384, third_roots_of_z[2].get_imaginary(), 1.0e-5);
    }
    
     /**
     * Test: computing <b>fourth roots</b> of z.
     * <pre>
     * <code>
     * <b>z = 5 - 2 * i</b>
     *   => z_0 =  1.5164 - 0.1446 * i
     *   => z_1 =  0.1446 + 1.5164 * i
     *   => z_2 = -1.5164 + 0.1446 * i
     *   => z_3 = -1.5164 - 0.1446 * i
     * </code>
     * </pre>
     */
    #[test]
    pub fn  test_nth_root_normal_fourth_root()  {
        // The complex number we want to compute all third-roots for.
         let z: Complex = Complex::new(5.0, -2.0);
        // The List holding all fourth roots
         let fourth_roots_of_z = z.nth_root(4).unwrap();
         // Returned Collection must not be empty!
        assert_within_delta!(4, fourth_roots_of_z.len());
        // test z_0
        assert_within_delta!(1.5164629308487783, fourth_roots_of_z[0].get_real(), 1.0e-5);
        assert_within_delta!(-0.14469266210702247, fourth_roots_of_z[0].get_imaginary(), 1.0e-5);
        // test z_1
        assert_within_delta!(0.14469266210702256, fourth_roots_of_z[1].get_real(), 1.0e-5);
        assert_within_delta!(1.5164629308487783, fourth_roots_of_z[1].get_imaginary(), 1.0e-5);
        // test z_2
        assert_within_delta!(-1.5164629308487783, fourth_roots_of_z[2].get_real(), 1.0e-5);
        assert_within_delta!(0.14469266210702267, fourth_roots_of_z[2].get_imaginary(), 1.0e-5);
        // test z_3
        assert_within_delta!(-0.14469266210702275, fourth_roots_of_z[3].get_real(), 1.0e-5);
        assert_within_delta!(-1.5164629308487783, fourth_roots_of_z[3].get_imaginary(), 1.0e-5);
    }
    
    

    /**
     * Test: computing <b>third roots</b> of z.
     * <pre>
     * <code>
     * <b>z = 8</b>
     *   => z_0 =  2
     *   => z_1 = -1 + 1.73205 * i
     *   => z_2 = -1 - 1.73205 * i
     * </code>
     * </pre>
     */
    #[test]
    pub fn  test_nth_root_cornercase_third_root_imaginary_part_empty()  {
        // The number 8 has three third roots. One we all already know is the number 2.
        // But there are two more complex roots.
         let z: Complex = Complex::new(8.0, 0.0);
        // The List holding all third roots
            let third_roots_of_z = z.nth_root(3).unwrap();
        // Returned Collection must not be empty!
        assert_within_delta!(3, third_roots_of_z.len());
        // test z_0
        assert_within_delta!(2.0, third_roots_of_z[0].get_real(), 1.0e-5);
        assert_within_delta!(0.0, third_roots_of_z[0].get_imaginary(), 1.0e-5);
        // test z_1
        assert_within_delta!(-1.0, third_roots_of_z[1].get_real(), 1.0e-5);
        assert_within_delta!(1.7320508075688774, third_roots_of_z[1].get_imaginary(), 1.0e-5);
        // test z_2
        assert_within_delta!(-1.0, third_roots_of_z[2].get_real(), 1.0e-5);
        assert_within_delta!(-1.732050807568877, third_roots_of_z[2].get_imaginary(), 1.0e-5);
    }
    
    

    /**
     * Test: computing <b>third roots</b> of z with real part 0.
     * <pre>
     * <code>
     * <b>z = 2 * i</b>
     *   => z_0 =  1.0911 + 0.6299 * i
     *   => z_1 = -1.0911 + 0.6299 * i
     *   => z_2 = -2.3144 - 1.2599 * i
     * </code>
     * </pre>
     */
    #[test]
    pub fn  test_nth_root_cornercase_third_root_real_part_zero()  {
        // complex number with only imaginary part
         let z: Complex = Complex::new(0.0, 2.0);
        // The List holding all third roots
         let third_roots_of_z = z.nth_root(3).unwrap();
        // Returned Collection must not be empty!
        assert_within_delta!(3, third_roots_of_z.len());
        // test z_0
        assert_within_delta!(1.0911236359717216, third_roots_of_z[0].get_real(), 1.0e-5);
        assert_within_delta!(0.6299605249474365, third_roots_of_z[0].get_imaginary(), 1.0e-5);
        // test z_1
        assert_within_delta!(-1.0911236359717216, third_roots_of_z[1].get_real(), 1.0e-5);
        assert_within_delta!(0.6299605249474365, third_roots_of_z[1].get_imaginary(), 1.0e-5);
        // test z_2
        assert_within_delta!(-2.3144374213981936E-16, third_roots_of_z[2].get_real(), 1.0e-5);
        assert_within_delta!(-1.2599210498948732, third_roots_of_z[2].get_imaginary(), 1.0e-5);
    }
    
    

    /**
     * Test cornercases with NAN and Infinity.
     */
    #[test]
    pub fn  test_nth_root_cornercase_nan_inf()  {
        // NAN + finite -> NAN
         let mut roots = ONE_NAN.nth_root(3).unwrap();
        assert_eq!(1usize, roots.len());
        assert_eq!(complex::NAN, roots[0]);
        roots = NAN_ZERO.nth_root(3).unwrap();
        assert_eq!(1usize, roots.len());
        assert_eq!(complex::NAN, roots[0]);
        // NAN + infinite -> NAN
        roots = NAN_INF.nth_root(3).unwrap();
        assert_eq!(1usize, roots.len());
        assert_eq!(complex::NAN, roots[0]);
        // finite + infinite -> Inf
        roots = ONE_INF.nth_root(3).unwrap();
        assert_eq!(1usize, roots.len());
        assert_eq!(complex::INF, roots[0]);
        // infinite + infinite -> Inf
        roots = NEG_INF_INF.nth_root(3).unwrap();
        assert_eq!(1usize, roots.len());
        assert_eq!(complex::INF, roots[0]);
    }
    
 /**
     * Test standard values
     */
    #[test]
    pub fn  test_get_argument()  {
         let mut z: Complex = Complex::new(1.0, 0.0);
        assert_within_delta!(0.0, z.get_argument(), 1.0e-12);
        z = Complex::new(1.0, 1.0);
        assert_within_delta!(f64::consts::PI / 4.0, z.get_argument(), 1.0e-12);
        z = Complex::new(0.0, 1.0);
        assert_within_delta!(f64::consts::PI / 2.0, z.get_argument(), 1.0e-12);
        z = Complex::new(-1.0, 1.0);
        assert_within_delta!(3.0 * f64::consts::PI / 4.0, z.get_argument(), 1.0e-12);
        z = Complex::new(-1.0, 0.0);
        assert_within_delta!(f64::consts::PI, z.get_argument(), 1.0e-12);
        z = Complex::new(-1.0, -1.0);
        assert_within_delta!(-3.0 * f64::consts::PI / 4.0, z.get_argument(), 1.0e-12);
        z = Complex::new(0.0, -1.0);
        assert_within_delta!(-f64::consts::PI / 2.0, z.get_argument(), 1.0e-12);
        z = Complex::new(1.0, -1.0);
        assert_within_delta!(-f64::consts::PI / 4.0, z.get_argument(), 1.0e-12);
    }

    /**
     * Verify atan2-style handling of infinite parts
     */
    #[test]
    pub fn  test_get_argument_inf()  {
        assert_within_delta!(f64::consts::PI / 4.0, INF_INF.get_argument(), 1.0e-12);
        assert_within_delta!(f64::consts::PI / 2.0, ONE_INF.get_argument(), 1.0e-12);
        assert_within_delta!(0.0, INF_ONE.get_argument(), 1.0e-12);
        assert_within_delta!(f64::consts::PI / 2.0, ZERO_INF.get_argument(), 1.0e-12);
        assert_within_delta!(0.0, INF_ZERO.get_argument(), 1.0e-12);
        assert_within_delta!(f64::consts::PI, NEG_INF_ONE.get_argument(), 1.0e-12);
        assert_within_delta!(-3.0 * f64::consts::PI / 4.0, NEG_INF_NEG_INF.get_argument(), 1.0e-12);
        assert_within_delta!(-f64::consts::PI / 2.0, ONE_NEG_INF.get_argument(), 1.0e-12);
    }    


    /**
     * Verify that either part NAN results in NAN
     */
    #[test]
    pub fn  test_get_argument_na_n()  {
       assert!(f64::is_nan(NAN_ZERO.get_argument()));
       assert!(f64::is_nan(ZERO_NAN.get_argument()));
       assert!(f64::is_nan(complex::NAN.get_argument()));
    }
    
     #[test]
     #[allow(unused_variables)]
    pub fn  test_polar2_complex_illegal_modulus()   {
        assert!(match Complex::polar2_complex(-1.0, 0.0) { Err(e) => true, _ => false });
    }

    #[test]
    pub fn  test_polar2_complex_na_n()   {
        TestUtils::assert_same_complex(&complex::NAN, &Complex::polar2_complex(f64::NAN, 1.0).unwrap());
        TestUtils::assert_same_complex(&complex::NAN, &Complex::polar2_complex(1.0, f64::NAN).unwrap());
        TestUtils::assert_same_complex(&complex::NAN, &Complex::polar2_complex(f64::NAN, f64::NAN).unwrap());
    }

    #[test]
    pub fn  test_polar2_complex_inf()   {
        TestUtils::assert_same_complex(&complex::NAN, &Complex::polar2_complex(1.0, f64::INFINITY).unwrap());
        TestUtils::assert_same_complex(&complex::NAN, &Complex::polar2_complex(1.0, f64::NEG_INFINITY).unwrap());
        TestUtils::assert_same_complex(&complex::NAN, &Complex::polar2_complex(f64::INFINITY, f64::INFINITY).unwrap());
        TestUtils::assert_same_complex(&complex::NAN, &Complex::polar2_complex(f64::INFINITY, f64::NEG_INFINITY).unwrap());
        TestUtils::assert_same_complex(&INF_INF, &Complex::polar2_complex(f64::INFINITY, f64::consts::PI / 4.0).unwrap());
        TestUtils::assert_same_complex(&INF_NAN, &Complex::polar2_complex(f64::INFINITY, 0.0).unwrap());
        TestUtils::assert_same_complex(&INF_NEG_INF, &Complex::polar2_complex(f64::INFINITY, -f64::consts::PI / 4.0).unwrap());
        TestUtils::assert_same_complex(&NEG_INF_INF, &Complex::polar2_complex(f64::INFINITY, 3.0 * f64::consts::PI / 4.0).unwrap());
        TestUtils::assert_same_complex(&NEG_INF_NEG_INF, &Complex::polar2_complex(f64::INFINITY, 5.0 * f64::consts::PI / 4.0).unwrap());
    }

	 #[test]
    pub fn  test_convert_to_complex()   {
         let real = vec![f64::NEG_INFINITY, -123.45, 0.0, 1.0, 234.56, f64::consts::PI, f64::INFINITY, ];
        
         let complex = Complex::convert_to_complex(&real);
         {
             let mut i: usize = 0;
            while i < real.len() {
                {
                    assert_within_delta!(real[i], complex[i].get_real(), 0.0);
                }
                i += 1;
             }
         }

    }
   


}


