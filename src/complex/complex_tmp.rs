                
                   /*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org::apache::commons::math3::complex;

use java::io::Serializable;
use java::util::ArrayList;
use java::util::List;
use org::apache::commons::math3::FieldElement;
use org::apache::commons::math3::exception::NotPositiveException;
use org::apache::commons::math3::exception::NullArgumentException;
use org::apache::commons::math3::exception::util::LocalizedFormats;
use org::apache::commons::math3::util::FastMath;
use org::apache::commons::math3::util::MathUtils;
use org::apache::commons::math3::util::Precision;

/**
 * Representation of a Complex number, i.e. a number which has both a
 * real and imaginary part.
 * <p>
 * Implementations of arithmetic operations handle {@code NaN} and
 * infinite values according to the rules for {@link java.lang.Double}, i.e.
 * {@link #equals} is an equivalence relation for all instances that have
 * a {@code NaN} in either real or imaginary part, e.g. the following are
 * considered equal:
 * <ul>
 *  <li>{@code 1 + NaNi}</li>
 *  <li>{@code NaN + i}</li>
 *  <li>{@code NaN + NaNi}</li>
 * </ul><p>
 * Note that this contradicts the IEEE-754 standard for floating
 * point numbers (according to which the test {@code x == x} must fail if
 * {@code x} is {@code NaN}). The method
 * {@link org.apache.commons.math3.util.Precision#equals(double,double,int)
 * equals for primitive double} in {@link org.apache.commons.math3.util.Precision}
 * conforms with IEEE-754 while this class conforms with the standard behavior
 * for Java object types.</p>
 *
 */

/** The square root of -1. A number representing "0.0 + 1.0i" */
 const I: Complex = Complex::new(0.0, 1.0);

// CHECKSTYLE: stop ConstantName
/** A complex number representing "NaN + NaNi" */
 const NAN: Complex = Complex::new(Double::NaN, Double::NaN);

// CHECKSTYLE: resume ConstantName
/** A complex number representing "+INF + INFi" */
 const INF: Complex = Complex::new(Double::POSITIVE_INFINITY, Double::POSITIVE_INFINITY);

/** A complex number representing "1.0 + 0.0i" */
 const ONE: Complex = Complex::new(1.0, 0.0);

/** A complex number representing "0.0 + 0.0i" */
 const ZERO: Complex = Complex::new(0.0, 0.0);

/** Serializable version identifier */
 let serial_version_u_i_d: i64 = -6195664516687396620;
#[derive(FieldElement<Complex>, Serializable)]
pub struct Complex {

    /** The imaginary part. */
     let imaginary: f64;

    /** The real part. */
     let real: f64;

    /** Record whether this complex number is equal to NaN. */
     let is_nan: bool;

    /** Record whether this complex number is infinite. */
     let is_infinite: bool;
}

impl Complex {

    /**
     * Create a complex number given only the real part.
     *
     * @param real Real part.
     */
    pub fn new( real: f64) -> Complex {
        this(&real, 0.0);
    }

    /**
     * Create a complex number given the real and imaginary parts.
     *
     * @param real Real part.
     * @param imaginary Imaginary part.
     */
    pub fn new( real: f64,  imaginary: f64) -> Complex {
        let .real = real;
        let .imaginary = imaginary;
        is_nan = f64::is_nan(&real) || f64::is_nan(&imaginary);
        is_infinite = !is_nan && (f64::is_infinite(&real) || f64::is_infinite(&imaginary));
    }



    /**
     * Test for the floating-point equality between Complex objects.
     * It returns {@code true} if both arguments are equal or within the
     * range of allowed error (inclusive).
     *
     * @param x First value (cannot be {@code null}).
     * @param y Second value (cannot be {@code null}).
     * @param maxUlps {@code (maxUlps - 1)} is the number of floating point
     * values between the real (resp. imaginary) parts of {@code x} and
     * {@code y}.
     * @return {@code true} if there are fewer than {@code maxUlps} floating
     * point values between the real (resp. imaginary) parts of {@code x}
     * and {@code y}.
     *
     * @see Precision#equals(double,double,int)
     * @since 3.3
     */
    pub fn  equals( x: &Complex,  y: &Complex,  max_ulps: i32) -> bool  {
        return Precision.equals(x.real, y.real, &max_ulps) && Precision.equals(x.imaginary, y.imaginary, &max_ulps);
    }



    /**
     * Get a hashCode for the complex number.
     * Any {@code f64::NaN} value in real or imaginary part produces
     * the same hash code {@code 7}.
     *
     * @return a hash code value for this object.
     */
    pub fn  hash_code(&self) -> i32  {
        if self.is_nan {
            return 7;
        }
        return 37 * (17 * MathUtils.hash(&self.imaginary) + MathUtils.hash(&self.real));
    }



    

    /**
     * Create a complex number given the real and imaginary parts.
     *
     * @param realPart Real part.
     * @param imaginaryPart Imaginary part.
     * @return a new complex number instance.
     * @since 1.2
     * @see #valueOf(double, double)
     */
    pub fn  create_complex(&self,  real_part: f64,  imaginary_part: f64) -> Complex  {
        return Complex::new(&real_part, &imaginary_part);
    }

}


                
                