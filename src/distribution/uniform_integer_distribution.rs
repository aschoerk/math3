
// Licensed to the Apache Software Foundation (ASF) under one or more
// contributor license agreements.  See the NOTICE file distributed with
// this work for additional information regarding copyright ownership.
// The ASF licenses this file to You under the Apache License, Version 2.0
// (the "License"); you may not use this file except in compliance with
// the License.  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
use std::rc::*;
use std::cell::*;
use java::exc::*;
use distribution::base::*;
use distribution::integer_distribution::*;
use random::well_generator::*;
use random::bits_stream_generator::*;
/**
 * Implementation of the uniform integer distribution.
 *
 * @see <a href="http://en.wikipedia.org/wiki/Uniform_distribution_(discrete)"
 * >Uniform distribution (discrete), at Wikipedia</a>
 *
 * @since 3.0
 */


pub struct UniformIntegerDistribution {
    /** Lower bound (inclusive) of this distribution. */
    lower: i32,

    /** Upper bound (inclusive) of this distribution. */
    upper: i32,

    random: Rc<RefCell<BitsStreamGenerator>>,
}

impl RandomGeneratorMgr for UniformIntegerDistribution {
    fn get_random_generator(&mut self) -> Rc<RefCell<BitsStreamGenerator>> {
        return self.random.clone();
    }
}

impl UniformIntegerDistribution {
    /**
     * Creates a new uniform integer distribution using the given lower and
     * upper bounds (both inclusive).
     * <p>
     * <b>Note:</b> this constructor will implicitly create an instance of
     * {@link Well19937c} as random generator to be used for sampling only (see
     * {@link #sample()} and {@link #sample(int)}). In case no sampling is
     * needed for the created distribution, it is advised to pass {@code null}
     * as random generator via the appropriate constructors to avoid the
     * additional initialisation overhead.
     *
     * @param lower Lower bound (inclusive) of this distribution.
     * @param upper Upper bound (inclusive) of this distribution.
     * @throws NumberIsTooLargeException if {@code lower >= upper}.
     */
    pub fn new(lower: i32, upper: i32) -> Result<UniformIntegerDistribution, Rc<Exception>> {
        return UniformIntegerDistribution::new_gen(Rc::new(RefCell::new(WellGeneratorFactory::new_well_19937c())), lower, upper);
    }

    /**
     * Creates a new uniform integer distribution using the given lower and
     * upper bounds (both inclusive).
     *
     * @param rng Random number generator.
     * @param lower Lower bound (inclusive) of this distribution.
     * @param upper Upper bound (inclusive) of this distribution.
     * @throws NumberIsTooLargeException if {@code lower > upper}.
     * @since 3.1
     */
    pub fn new_gen(rng: Rc<RefCell<BitsStreamGenerator>>,
                   lower: i32,
                   upper: i32)
                   -> Result<UniformIntegerDistribution, Rc<Exception>> {
        if lower > upper {
            return Err(Exc::new_msg("NumberIsTooLargeException",
                                    format!("lower bound ({0}) must be strictly less than \
                                             upper bound ({1})",
                                            lower,
                                            upper)));
        }
        Ok(UniformIntegerDistribution {
            lower: lower,
            upper: upper,
            random: rng.clone(),
        })

    }
}

impl IntegerDistribution for UniformIntegerDistribution {
    /** {@inheritDoc} */
    fn probability(&self, x: i32) -> f64 {
        if x < self.lower || x > self.upper {
            return 0.0;
        }
        return 1.0 / (self.upper - self.lower + 1) as f64;
    }

    /** {@inheritDoc} */
    fn cumulative_probability(&self, x: i32) -> f64 {
        if x < self.lower {
            return 0.0;
        }
        if x > self.upper {
            return 1.0;
        }
        return (x - self.lower + 1) as f64 / (self.upper - self.lower + 1) as f64;
    }

    /**
     * {@inheritDoc}
     *
     * For lower bound {@code lower} and upper bound {@code upper}, the mean is
     * {@code 0.5 * (lower + upper)}.
     */
    fn get_numerical_mean(&self) -> f64 {
        return 0.5 * (self.lower + self.upper) as f64;
    }

    /**
     * {@inheritDoc}
     *
     * For lower bound {@code lower} and upper bound {@code upper}, and
     * {@code n = upper - lower + 1}, the variance is {@code (n^2 - 1) / 12}.
     */
    fn get_numerical_variance(&self) -> f64 {
        let n: i32 = self.upper - self.lower + 1;
        return (n * n - 1) as f64 / 12.0;
    }

    /**
     * {@inheritDoc}
     *
     * The lower bound of the support is equal to the lower bound parameter
     * of the distribution.
     *
     * @return lower bound of the support
     */
    fn get_support_lower_bound(&self) -> i32 {
        return self.lower;
    }

    /**
     * {@inheritDoc}
     *
     * The upper bound of the support is equal to the upper bound parameter
     * of the distribution.
     *
     * @return upper bound of the support
     */
    fn get_support_upper_bound(&self) -> i32 {
        return self.upper;
    }

    /**
     * {@inheritDoc}
     *
     * The support of this distribution is connected.
     *
     * @return {@code true}
     */
    fn is_support_connected(&self) -> bool {
        return true;
    }

    /** {@inheritDoc} */
    fn sample(&mut self) -> Result<i32, Rc<Exception>> {
        let max: i32 = (self.upper - self.lower) + 1;
        let gen = self.get_random_generator();
        let mut mut_gen = gen.borrow_mut();
        if max <= 0 {
            // we use a simple rejection method.
            loop {
                let r: i32 = mut_gen.next_int();
                if r >= self.lower && r <= self.upper {
                    return Ok(r);
                }
            }
        } else {
            let tmp = mut_gen.next_int_i32(max).unwrap();
            // We can shift the range and directly generate a positive int.
            return Ok(self.lower + tmp);
        }
    }
}
