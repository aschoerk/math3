
// Licensed to the Apache Software Foundation (ASF) under one or more
// contributor license agreements.  See the NOTICE file distributed with
// this work for additional information regarding copyright ownership.
// The ASF licenses this file to You under the Apache License, Version 2.0
// (the "License"); you may not use this file except in compliance with
// the License.  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
use std::rc::*;
use java::exc::*;
use util::fastmath::F64;
use std::i32;
use std::f64;
use distribution::base::*;


/**
 * Base class for integer-valued discrete distributions.  Default
 * implementations are provided for some of the methods that do not vary
 * from distribution to distribution.
 *
 */
pub trait IntegerDistribution: RandomGeneratorMgr {
    /**
     * For a random variable {@code X} whose values are distributed according
     * to this distribution, this method returns {@code P(X = x)}. In other
     * words, this method represents the probability mass function (PMF)
     * for the distribution.
     *
     * @param x the point at which the PMF is evaluated
     * @return the value of the probability mass function at {@code x}
     */
    fn probability(&self, x: i32) -> f64;

    /**
     * Computes the quantile function of this distribution.
     * For a random variable {@code X} distributed according to this distribution,
     * the returned value is
     * <ul>
     * <li><code>inf{x in Z | P(X<=x) >= p}</code> for {@code 0 < p <= 1},</li>
     * <li><code>inf{x in Z | P(X<=x) > 0}</code> for {@code p = 0}.</li>
     * </ul>
     * If the result exceeds the range of the data type {@code int},
     * then {@code Integer.MIN_VALUE} or {@code Integer.MAX_VALUE} is returned.
     *
     * @param p the cumulative probability
     * @return the smallest {@code p}-quantile of this distribution
     * (largest 0-quantile for {@code p = 0})
     * @throws OutOfRangeException if {@code p < 0} or {@code p > 1}
     *
     * For a random variable {@code X} whose values are distributed according
     * to this distribution, this method returns {@code P(X <= x)}.  In other
     * words, this method represents the (cumulative) distribution function
     * (CDF) for this distribution.
     *
     * @param x the point at which the CDF is evaluated
     * @return the probability that a random variable with this
     * distribution takes a value less than or equal to {@code x}
     */
    fn cumulative_probability(&self, x: i32) -> f64;


    /**
     * For a random variable {@code X} whose values are distributed according
     * to this distribution, this method returns {@code P(x0 < X <= x1)}.
     *
     * @param x0 the exclusive lower bound
     * @param x1 the inclusive upper bound
     * @return the probability that a random variable with this distribution
     * will take a value between {@code x0} and {@code x1},
     * excluding the lower and including the upper endpoint
     * @throws NumberIsTooLargeException if {@code x0 > x1}
     *
     *
     * The default implementation uses the identity
     * <p>{@code P(x0 < X <= x1) = P(X <= x1) - P(X <= x0)}</p>
     */
    fn cumulative_probability_bounded(&self, x0: i32, x1: i32) -> Result<f64, Rc<Exception>> {
        if x1 < x0 {
            return Err(Exc::new_msg("NumberIsTooLargeException",
                                    format!("lower endpoint ({0}) must be less than or equal \
                                             to upper endpoint ({1})",
                                            x0,
                                            x1)));
        }
        return Ok(self.cumulative_probability(x1) - self.cumulative_probability(x0));
    }

    /**
     * {@inheritDoc}
     *
     * The default implementation returns
     * <ul>
     * <li>{@link #getSupportLowerBound()} for {@code p = 0},</li>
     * <li>{@link #getSupportUpperBound()} for {@code p = 1}, and</li>
     * <li>{@link #solveInverseCumulativeProbability(double, int, int)} for
     *     {@code 0 < p < 1}.</li>
     * </ul>
     */
    fn inverse_cumulative_probability(&self, p: f64) -> Result<i32, Rc<Exception>> {
        if p < 0.0 || p > 1.0 {
            return Err(Exc::new_msg("OutOfRangeException",
                                    format!("{} not between {} and {}", p, 0, 1)));
        }
        let mut lower: i32 = self.get_support_lower_bound();
        if p == 0.0 {
            return Ok(lower);
        }
        if lower == i32::MIN {
            let checked_cumulative_probability = try!(self.checked_cumulative_probability(lower));
            if checked_cumulative_probability >= p {
                return Ok(lower);
            }
        } else {
            // this ensures cumulativeProbability(lower) < p, which
            lower -= 1;
            // is important for the solving step
        }
        let mut upper: i32 = self.get_support_upper_bound();
        if p == 1.0 {
            return Ok(upper);
        }
        // use the one-sided Chebyshev inequality to narrow the bracket
        // cf. AbstractRealDistribution.inverseCumulativeProbability(double)
        let mu: f64 = self.get_numerical_mean();
        let sigma: f64 = F64::sqrt(self.get_numerical_variance());
        let chebyshev_applies: bool =
            !(f64::is_infinite(mu) || f64::is_nan(mu) || f64::is_infinite(sigma) ||
              f64::is_nan(sigma) || sigma == 0.0);
        if chebyshev_applies {
            let mut k: f64 = F64::sqrt((1.0 - p) / p);
            let mut tmp: f64 = mu - k * sigma;
            if tmp > lower as f64 {
                lower = (F64::ceil(tmp) as i32) - 1;
            }
            k = 1.0 / k;
            tmp = mu + k * sigma;
            if tmp < upper as f64 {
                upper = (F64::ceil(tmp) as i32) - 1;
            }
        }
        return self.solve_inverse_cumulative_probability(p, lower, upper);
    }

    /**
     * This is a utility function used by {@link
     * #inverseCumulativeProbability(double)}. It assumes {@code 0 < p < 1} and
     * that the inverse cumulative probability lies in the bracket {@code
     * (lower, upper]}. The implementation does simple bisection to find the
     * smallest {@code p}-quantile <code>inf{x in Z | P(X<=x) >= p}</code>.
     *
     * @param p the cumulative probability
     * @param lower a value satisfying {@code cumulativeProbability(lower) < p}
     * @param upper a value satisfying {@code p <= cumulativeProbability(upper)}
     * @return the smallest {@code p}-quantile of this distribution
     */
    fn solve_inverse_cumulative_probability(&self,
                                            p: f64,
                                            lower_p: i32,
                                            upper_p: i32)
                                            -> Result<i32, Rc<Exception>> {
        let mut upper = upper_p;
        let mut lower = lower_p;
        while lower + 1 < upper {
            let mut xm: i32 = (lower + upper) / 2;
            if xm < lower || xm > upper {
                // Overflow.
                // There will never be an overflow in both calculation methods
                // for xm at the same time
                //
                xm = lower + (upper - lower) / 2;
            }
            let pm: f64 = try!(self.checked_cumulative_probability(xm));
            if pm >= p {
                upper = xm;
            } else {
                lower = xm;
            }
        }
        return Ok(upper);
    }

    /**
     * Use this method to get the numerical value of the mean of this
     * distribution.
     *
     * @return the mean or {@code Double.NaN} if it is not defined
     */
    fn get_numerical_mean(&self) -> f64;

    /**
     * Use this method to get the numerical value of the variance of this
     * distribution.
     *
     * @return the variance (possibly {@code Double.POSITIVE_INFINITY} or
     * {@code Double.NaN} if it is not defined)
     */
    fn get_numerical_variance(&self) -> f64;

    /**
     * Access the lower bound of the support. This method must return the same
     * value as {@code inverseCumulativeProbability(0)}. In other words, this
     * method must return
     * <p><code>inf {x in Z | P(X <= x) > 0}</code>.</p>
     *
     * @return lower bound of the support ({@code Integer.MIN_VALUE}
     * for negative infinity)
     */
    fn get_support_lower_bound(&self) -> i32;

    /**
     * Access the upper bound of the support. This method must return the same
     * value as {@code inverseCumulativeProbability(1)}. In other words, this
     * method must return
     * <p><code>inf {x in R | P(X <= x) = 1}</code>.</p>
     *
     * @return upper bound of the support ({@code Integer.MAX_VALUE}
     * for positive infinity)
     */
    fn get_support_upper_bound(&self) -> i32;

    /**
     * Use this method to get information about whether the support is
     * connected, i.e. whether all integers between the lower and upper bound of
     * the support are included in the support.
     *
     * @return whether the support is connected or not
     */
    fn is_support_connected(&self) -> bool;

    /**
     * Reseed the random generator used to generate samples.
     *
     * @param seed the new seed
     * @since 3.0
     */
    fn reseed_random_generator(&mut self, seed: i64) {
        let gen = self.get_random_generator();
        gen.borrow_mut().set_seed_i64(&seed);
    }

    /**
     * Generate a random value sampled from this distribution.
     *
     * @return a random value
     * @since 3.0
     *
     *
     * The default implementation uses the
     * <a href="http://en.wikipedia.org/wiki/Inverse_transform_sampling">
     * inversion method</a>.
     */
    fn sample(&mut self) -> Result<i32, Rc<Exception>> {
        let gen = self.get_random_generator();
        let dbl = gen.borrow_mut().next_double();
        return self.inverse_cumulative_probability(dbl);
    }

    /**
     * Generate a random sample from the distribution.
     *
     * @param sampleSize the number of random values to generate
     * @return an array representing the random sample
     * @throws org.apache.commons.math3.exception.NotStrictlyPositiveException
     * if {@code sampleSize} is not positive
     * @since 3.0
     *
     * The default implementation generates the sample by calling
     * {@link #sample()} in a loop.
     */
    fn sample_size(&mut self, sample_size: usize) -> Result<Vec<i32>, Rc<Exception>> {
        if sample_size <= 0 {
            return Err(Exc::new_msg("NotStrictlyPositiveException",
                                    format!("invalid number of samples: {}", sample_size)));
        }
        let mut out = Vec::new();
        {
            let mut i: usize = 0;
            while i < sample_size {
                {
                    let sample = try!(self.sample());
                    out.push(sample);
                }
                i += 1;
            }
        }

        return Ok(out);
    }

    /**
     * Computes the cumulative probability function and checks for {@code NaN}
     * values returned. Throws {@code MathInternalError} if the value is
     * {@code NaN}. Rethrows any exception encountered evaluating the cumulative
     * probability function. Throws {@code MathInternalError} if the cumulative
     * probability function returns {@code NaN}.
     *
     * @param argument input value
     * @return the cumulative probability
     * @throws MathInternalError if the cumulative probability is {@code NaN}
     */
    fn checked_cumulative_probability(&self, argument: i32) -> Result<f64, Rc<Exception>> {
        let result = self.cumulative_probability(argument);
        if f64::is_nan(result) {
            return Err(Exc::new_msg("MathInternalError",
                                    format!("Discrete cumulative probability function returned \
                                             NaN for argument {0}",
                                            argument)));
        }
        return Ok(result);
    }

    /**
     * For a random variable {@code X} whose values are distributed according to
     * this distribution, this method returns {@code log(P(X = x))}, where
     * {@code log} is the natural logarithm. In other words, this method
     * represents the logarithm of the probability mass function (PMF) for the
     * distribution. Note that due to the floating point precision and
     * under/overflow issues, this method will for some distributions be more
     * precise and faster than computing the logarithm of
     * {@link #probability(int)}.
     * <p>
     * The default implementation simply computes the logarithm of {@code probability(x)}.</p>
     *
     * @param x the point at which the PMF is evaluated
     * @return the logarithm of the value of the probability mass function at {@code x}
     */
    fn log_probability(&self, x: i32) -> f64 {
        return F64::log(self.probability(x));
    }
}
