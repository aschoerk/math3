                
                   /*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#[cfg(test)]
mod tests {
/**
 * Test cases for UniformIntegerDistribution. See class javadoc for
 * {@link IntegerDistributionAbstractTest} for further details.
 */
pub struct UniformIntegerDistributionTest {
    super: IntegerDistributionAbstractTest;
}

impl UniformIntegerDistributionTest {

    // --- Override tolerance -------------------------------------------------
    pub fn  set_up(&self)   {
        super.set_up();
        set_tolerance(1e-9);
    }

    //--- Implementations for abstract methods --------------------------------
    /** Creates the default discrete distribution instance to use in tests. */
    pub fn  make_distribution(&self) -> IntegerDistribution  {
        return UniformIntegerDistribution::new(-3, 5);
    }

    /** Creates the default probability density test input values. */
    pub fn  make_density_test_points(&self) -> Vec<i32>  {
        return  : vec![i32; 11] = vec![-4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, ]
        ;
    }

    /** Creates the default probability density test expected values. */
    pub fn  make_density_test_values(&self) -> Vec<f64>  {
         let d: f64 = 1.0 / (5 - -3 + 1);
        return  : vec![f64; 11] = vec![0, d, d, d, d, d, d, d, d, d, 0, ]
        ;
    }

    /** Creates the default cumulative probability density test input values. */
    pub fn  make_cumulative_test_points(&self) -> Vec<i32>  {
        return self.make_density_test_points();
    }

    /** Creates the default cumulative probability density test expected values. */
    pub fn  make_cumulative_test_values(&self) -> Vec<f64>  {
        return  : vec![f64; 11] = vec![0, 1 / 9.0, 2 / 9.0, 3 / 9.0, 4 / 9.0, 5 / 9.0, 6 / 9.0, 7 / 9.0, 8 / 9.0, 1, 1, ]
        ;
    }

    /** Creates the default inverse cumulative probability test input values */
    pub fn  make_inverse_cumulative_test_points(&self) -> Vec<f64>  {
        return  : vec![f64; 14] = vec![0, 0.001, 0.010, 0.025, 0.050, 0.100, 0.200, 0.5, 0.999, 0.990, 0.975, 0.950, 0.900, 1, ]
        ;
    }

    /** Creates the default inverse cumulative probability density test expected values */
    pub fn  make_inverse_cumulative_test_values(&self) -> Vec<i32>  {
        return  : vec![i32; 14] = vec![-3, -3, -3, -3, -3, -3, -2, 1, 5, 5, 5, 5, 5, 5, ]
        ;
    }

    //--- Additional test cases -----------------------------------------------
    /** Test mean/variance. */
    #[test]
    pub fn  test_moments(&self)   {
         let mut dist: UniformIntegerDistribution;
        dist = UniformIntegerDistribution::new(0, 5);
        Assert::assert_equals(&dist.get_numerical_mean(), 2.5, 0);
        Assert::assert_equals(&dist.get_numerical_variance(), 35 / 12.0, 0);
        dist = UniformIntegerDistribution::new(0, 1);
        Assert::assert_equals(&dist.get_numerical_mean(), 0.5, 0);
        Assert::assert_equals(&dist.get_numerical_variance(), 3 / 12.0, 0);
    }

    // MATH-1141
    #[test]
    pub fn  test_precondition_upper_bound_inclusive(&self)   {
        let tryResult1 = 0;
        'try1 loop {
        {
            UniformIntegerDistribution::new(1, 0);
        }
        break 'try1
        }
        match tryResult1 {
             catch ( e: &NumberIsTooLargeException) {
            }  0 => break
        }

        // Degenerate case is allowed.
        UniformIntegerDistribution::new(0, 0);
    }
}


                
}