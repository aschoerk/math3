                
                   /*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * Abstract base class for {@link IntegerDistribution} tests.
 * <p>
 * To create a concrete test class for an integer distribution implementation,
 *  implement makeDistribution() to return a distribution instance to use in
 *  tests and each of the test data generation methods below.  In each case, the
 *  test points and test values arrays returned represent parallel arrays of
 *  inputs and expected values for the distribution returned by makeDistribution().
 *  <p>
 *  makeDensityTestPoints() -- arguments used to test probability density calculation
 *  makeDensityTestValues() -- expected probability densities
 *  makeCumulativeTestPoints() -- arguments used to test cumulative probabilities
 *  makeCumulativeTestValues() -- expected cumulative probabilites
 *  makeInverseCumulativeTestPoints() -- arguments used to test inverse cdf evaluation
 *  makeInverseCumulativeTestValues() -- expected inverse cdf values
 * <p>
 *  To implement additional test cases with different distribution instances and test data,
 *  use the setXxx methods for the instance data in test cases and call the verifyXxx methods
 *  to verify results.
 *
 */

use distribution::integer_distribution::*;                   

use std::rc::*;

pub struct IntegerDistributionTest {

    //-------------------- Private test instance data -------------------------
    /** Discrete distribution instance used to perform tests */
    distribution: Rc<IntegerDistribution>,

    /** Tolerance used in comparing expected and returned values */
     tolerance: f64, //  = 1E-12,

    /** Arguments used to test probability density calculations */
     density_test_points: Vec<i32>,

    /** Values used to test probability density calculations */
     density_test_values: Vec<f64>,

    /** Values used to test logarithmic probability density calculations */
     log_density_test_values: Vec<f64>,

    /** Arguments used to test cumulative probability density calculations */
     cumulative_test_points: Vec<i32>,

    /** Values used to test cumulative probability density calculations */
     cumulative_test_values: Vec<f64>,

    /** Arguments used to test inverse cumulative probability density calculations */
     inverse_cumulative_test_points: Vec<f64>,

    /** Values used to test inverse cumulative probability density calculations */
     inverse_cumulative_test_values: Vec<i32>,
}

#[cfg(test)]
pub mod tests {
	use std::rc::*;
	use std::i32;
	use util::fastmath::F64;
	use distribution::integer_distribution::*;
	use distribution::integer_distribution_test::*;
	use distribution::uniform_integer_distribution::*;
	
	use assert;


pub trait IntegerDistributionTestDataFactory {
	    //-------------------- Abstract methods -----------------------------------
    /** Creates the default discrete distribution instance to use in tests. */
    fn  make_distribution(&self) -> Rc<IntegerDistribution> ;

    /** Creates the default probability density test input values */
    fn  make_density_test_points(&self) -> Vec<i32> ;

    /** Creates the default probability density test expected values */
    fn  make_density_test_values(&self) -> Vec<f64> ;
    
    /** Creates the default logarithmic probability density test expected values.
     *
     * The default implementation simply computes the logarithm of all the values in
     * {@link #makeDensityTestValues()}.
     *
     * @return double[] the default logarithmic probability density test expected values.
     */
    fn  make_log_density_test_values(&self) -> Vec<f64>  {
         let density_test_values: Vec<f64> = self.make_density_test_values();
         let mut log_density_test_values = Vec::new();
         {
             let mut i: usize = 0;
            while i < density_test_values.len() {
                {
                    log_density_test_values.push(F64::log(density_test_values[i]));
                }
                i += 1;
             }
         }

        return log_density_test_values;
    }

    /** Creates the default cumulative probability density test input values */
    fn  make_cumulative_test_points(&self) -> Vec<i32> ;

    /** Creates the default cumulative probability density test expected values */
    fn  make_cumulative_test_values(&self) -> Vec<f64> ;

    /** Creates the default inverse cumulative probability test input values */
    fn  make_inverse_cumulative_test_points(&self) -> Vec<f64> ;

    /** Creates the default inverse cumulative probability density test expected values */
    fn  make_inverse_cumulative_test_values(&self) -> Vec<i32> ;
    
    fn get_tolerance(&self) -> f64 {
    	1E-12
    }

}

pub struct UniformIntegerDistributionTestData {
	
}

impl UniformIntegerDistributionTestData {
	pub fn new() -> UniformIntegerDistributionTestData {
		UniformIntegerDistributionTestData {
			
		}
	}
}

impl IntegerDistributionTestDataFactory for UniformIntegerDistributionTestData {
	//--- Implementations for abstract methods --------------------------------
    /** Creates the default discrete distribution instance to use in tests. */
    fn  make_distribution(&self) -> Rc<IntegerDistribution>  {
        return Rc::new(UniformIntegerDistribution::new(-3, 5).unwrap());
    }

    /** Creates the default probability density test input values. */
    fn  make_density_test_points(&self) -> Vec<i32>  {
        return  vec![-4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, ]
        ;
    }

    /** Creates the default probability density test expected values. */
    fn  make_density_test_values(&self) -> Vec<f64>  {
         let d: f64 = 1.0 / (5 - -3 + 1) as f64;
        return  vec![0.0, d, d, d, d, d, d, d, d, d, 0.0, ]
        ;
    }

    /** Creates the default cumulative probability density test input values. */
    fn  make_cumulative_test_points(&self) -> Vec<i32>  {
        return self.make_density_test_points();
    }

    /** Creates the default cumulative probability density test expected values. */
    fn  make_cumulative_test_values(&self) -> Vec<f64>  {
        return  vec![0.0, 1.0 / 9.0, 2.0 / 9.0, 3.0 / 9.0, 4.0 / 9.0, 5.0 / 9.0, 6.0 / 9.0, 7.0 / 9.0, 8.0 / 9.0, 1.0, 1.0, ]
        ;
    }

    /** Creates the default inverse cumulative probability test input values */
    fn  make_inverse_cumulative_test_points(&self) -> Vec<f64>  {
        return   vec![0.0, 0.001, 0.010, 0.025, 0.050, 0.100, 0.200, 0.5, 0.999, 0.990, 0.975, 0.950, 0.900, 1.0, ]
        ;
    }

    /** Creates the default inverse cumulative probability density test expected values */
    fn  make_inverse_cumulative_test_values(&self) -> Vec<i32>  {
        return  vec![-3, -3, -3, -3, -3, -3, -2, 1, 5, 5, 5, 5, 5, 5, ]
        ;
    }

    fn get_tolerance(&self) -> f64 {
    	1E-9
    }
        


}

pub fn  create_integer_distribution_test_data<T: IntegerDistributionTestDataFactory>(factory: T) -> IntegerDistributionTest {
	IntegerDistributionTest {
		distribution: factory.make_distribution(),
        density_test_points: factory.make_density_test_points(),
        density_test_values: factory.make_density_test_values(),
        log_density_test_values: factory.make_log_density_test_values(),
        cumulative_test_points: factory.make_cumulative_test_points(),
        cumulative_test_values: factory.make_cumulative_test_values(),
        inverse_cumulative_test_points: factory.make_inverse_cumulative_test_points(),
        inverse_cumulative_test_values: factory.make_inverse_cumulative_test_values(), 
        tolerance: factory.get_tolerance()
	}
}


impl IntegerDistributionTest {

    //-------------------- Setup / tear down ----------------------------------

  
    //-------------------- Verification methods -------------------------------
    /**
     * Verifies that probability density calculations match expected values
     * using current test instance data
     */
    pub fn  verify_densities(&self)   {
         {
             let mut i: usize = 0;
            while i < self.density_test_points.len() {
                {
                	let a: f64 = self.density_test_values[i];
                	let b: f64 = self.distribution.probability(self.density_test_points[i]);
                	let c: f64 = self.tolerance;
                	assert_within_delta!(a,b,c, "Incorrect density value returned for {}",self.density_test_points[i])
                    // Assert::assert_equals(, self.density_test_values[i], &self.distribution.probability(self.density_test_points[i]), &self.get_tolerance());
                }
                i += 1;
             }
         }

    }

    /**
     * Verifies that logarithmic probability density calculations match expected values
     * using current test instance data.
     */
    pub fn  verify_log_densities(&self)   {
         {
             let mut i: usize = 0;
            while i < self.density_test_points.len() {
                {
                    // FIXME: when logProbability methods are added to IntegerDistribution in 4.0, remove cast below
                    assert_within_delta!( self.log_density_test_values[i], self.distribution.log_probability(self.density_test_points[i]), self.tolerance,
                    "Incorrect log density value returned for {}",self.density_test_points[i]);
                }
                i += 1;
             }
         }

    }

    /**
     * Verifies that cumulative probability density calculations match expected values
     * using current test instance data
     */
    pub fn  verify_cumulative_probabilities(&self)   {
         {
             let mut i: usize = 0;
            while i < self.cumulative_test_points.len() {
                {
                    assert_within_delta!(
                    	self.cumulative_test_values[i], self.distribution.cumulative_probability(self.cumulative_test_points[i]), self.tolerance,
                    	"Incorrect cumulative probability value returned for {}",self.cumulative_test_points[i]);
                }
                i += 1;
             }
         }

    }

    /**
     * Verifies that inverse cumulative probability density calculations match expected values
     * using current test instance data
     */
    pub fn  verify_inverse_cumulative_probabilities(&self)   {
         {
             let mut i: usize = 0;
            while i < self.inverse_cumulative_test_points.len() {
                {
                    assert_within_delta!(self.inverse_cumulative_test_values[i] as f64, self.distribution.inverse_cumulative_probability(self.inverse_cumulative_test_points[i]).unwrap() as f64,0.0f64,
                    	"Incorrect inverse cumulative probability value returned for {}",self.inverse_cumulative_test_points[i]);
                }
                i += 1;
             }
         }

    }


    /**
     * Verifies that illegal arguments are correctly handled
     */    
    pub fn  test_illegal_arguments(&self)   {
        assert!(self.distribution.cumulative_probability_bounded(1, 0).is_err(),
            "Expecting MathIllegalArgumentException for bad cumulativeProbability interval");
        assert!(self.distribution.inverse_cumulative_probability(-1.0).is_err(),
             	"Expecting MathIllegalArgumentException for p = -1");
        assert!(self.distribution.inverse_cumulative_probability(2.0).is_err(),
            "Expecting MathIllegalArgumentException for p = 2");
    }

    /*
    /**
     * Test sampling
     */    
    pub fn  verify_sampling(&self)   {
         let density_points: Vec<i32> = self.make_density_test_points();
         let density_values: Vec<f64> = self.make_density_test_values();
         let sample_size: i32 = 1000;
         let length: i32 = TestUtils::eliminate_zero_mass_points(density_points, density_values);
         let distribution: AbstractIntegerDistribution = self.make_distribution() as AbstractIntegerDistribution;
         let expected_counts: [f64; len()] = [0.0; len()];
         let observed_counts: [i64; len()] = [0; len()];
         {
             let mut i: usize = 0;
            while i < len() {
                {
                    expected_counts[i] = sample_size * density_values[i];
                }
                i += 1;
             }
         }

        // Use fixed seed
        distribution.reseed_random_generator(1000);
         let sample: Vec<i32> = distribution.sample(sample_size);
         {
             let mut i: usize = 0;
            while i < sample_size {
                {
                     {
                         let mut j: i32 = 0;
                        while j < len() {
                            {
                                if sample[i] == density_points[j] {
                                    observed_counts[j] += 1;
                                }
                            }
                            j += 1;
                         }
                     }

                }
                i += 1;
             }
         }

        TestUtils::assert_chi_square_accept(density_points, expected_counts, observed_counts, .001);
    }
    */

    //------------------ Getters / Setters for test instance data -----------
    /**
     * @return Returns the cumulativeTestPoints.
     */
    pub fn  get_cumulative_test_points(&self) -> Vec<i32>  {
        return self.cumulative_test_points.clone();
    }

    /**
     * @param cumulativeTestPoints The cumulativeTestPoints to set.
     */
    pub fn  set_cumulative_test_points(&mut self,  cumulative_test_points: &Vec<i32>)   {
        self.cumulative_test_points = cumulative_test_points.clone();
    }

    /**
     * @return Returns the cumulativeTestValues.
     */
    pub fn  get_cumulative_test_values(&self) -> Vec<f64>  {
        return self.cumulative_test_values.clone();
    }

    /**
     * @param cumulativeTestValues The cumulativeTestValues to set.
     */
    pub fn  set_cumulative_test_values(&mut self,  cumulative_test_values: &Vec<f64>)   {
        self.cumulative_test_values = cumulative_test_values.clone();
    }

    /**
     * @return Returns the densityTestPoints.
     */
    pub fn  get_density_test_points(&self) -> Vec<i32>  {
        return self.density_test_points.clone();
    }

    /**
     * @param densityTestPoints The densityTestPoints to set.
     */
    pub fn  set_density_test_points(&mut self,  density_test_points: &Vec<i32>)   {
        self.density_test_points = density_test_points.clone();
    }

    /**
     * @return Returns the densityTestValues.
     */
    pub fn  get_density_test_values(&self) -> Vec<f64>  {
        return self.density_test_values.clone();
    }

    /**
     * @param densityTestValues The densityTestValues to set.
     */
    pub fn  set_density_test_values(&mut self,  density_test_values: &Vec<f64>)   {
        self.density_test_values = density_test_values.clone();
    }

    /**
     * @return Returns the distribution. 
     */
    pub fn  get_distribution(&self) -> Rc<IntegerDistribution>  {
        return self.distribution.clone();
    }

    /**
     * @param distribution The distribution to set.
     */
    pub fn  set_distribution(&mut self,  distribution: &Rc<IntegerDistribution>)   {
        self.distribution = distribution.clone();
    }

    /**
     * @return Returns the inverseCumulativeTestPoints.
     */
    pub fn  get_inverse_cumulative_test_points(&self) -> Vec<f64>  {
        return self.inverse_cumulative_test_points.clone();
    }

    /**
     * @param inverseCumulativeTestPoints The inverseCumulativeTestPoints to set.
     */
    pub fn  set_inverse_cumulative_test_points(&mut self,  inverse_cumulative_test_points: &Vec<f64>)   {
        self.inverse_cumulative_test_points = inverse_cumulative_test_points.clone();
    }

    /**
     * @return Returns the inverseCumulativeTestValues.
     */
    pub fn  get_inverse_cumulative_test_values(&self) -> Vec<i32>  {
        return self.inverse_cumulative_test_values.clone();
    }

    /**
     * @param inverseCumulativeTestValues The inverseCumulativeTestValues to set.
     */
    pub fn  set_inverse_cumulative_test_values(&mut self,  inverse_cumulative_test_values: &Vec<i32>)   {
        self.inverse_cumulative_test_values = inverse_cumulative_test_values.clone();
    }

    /**
     * @return Returns the tolerance.
     */
    pub fn  get_tolerance(&self) -> f64  {
        return self.tolerance;
    }

    /**
     * @param tolerance The tolerance to set.
     */
    pub fn  set_tolerance(&mut self,  tolerance: f64)   {
        self.tolerance = tolerance;
    }
    
}

	fn gen_uniform_dist_test_data() -> IntegerDistributionTest {
			let td = UniformIntegerDistributionTestData::new();
			create_integer_distribution_test_data(td)
	}
	
	#[test]
	pub fn test() {
		gen_uniform_dist_test_data().verify_densities();		
	}
	
	
    //------------------------ Default test cases -----------------------------
    /**
     * Verifies that probability density calculations match expected values
     * using default test instance data
     */
    #[test]
    pub fn  test_densities()   {
        gen_uniform_dist_test_data().verify_densities();
    }

    /**
     * Verifies that logarithmic probability density calculations match expected values
     * using default test instance data
     */
    #[test]
    pub fn  test_log_densities()   {
        gen_uniform_dist_test_data().verify_log_densities();
    }

    /**
     * Verifies that cumulative probability density calculations match expected values
     * using default test instance data
     */
    #[test]
    pub fn  test_cumulative_probabilities()   {
        gen_uniform_dist_test_data().verify_cumulative_probabilities();
    }

    /**
     * Verifies that inverse cumulative probability density calculations match expected values
     * using default test instance data
     */
    #[test]
    pub fn  test_inverse_cumulative_probabilities()   {
        gen_uniform_dist_test_data().verify_inverse_cumulative_probabilities();
    }

    #[test]
    pub fn  test_consistency_at_support_bounds()   {
    	let test_data = gen_uniform_dist_test_data();
         let lower: i32 = test_data.distribution.get_support_lower_bound();
        assert_within_delta!(0.0, test_data.distribution.cumulative_probability(lower - 1), 0.0,"Cumulative probability mmust be 0 below support lower bound.");
        assert_within_delta!( test_data.distribution.probability(lower), test_data.distribution.cumulative_probability(lower), test_data.get_tolerance(),"Cumulative probability of support lower bound must be equal to probability mass at this point.");
        assert_within_delta!(lower, test_data.distribution.inverse_cumulative_probability(0.0).unwrap(),0,"Inverse cumulative probability of 0 must be equal to support lower bound.");
         let upper: i32 = test_data.distribution.get_support_upper_bound();
        if upper != i32::MAX {
            assert_within_delta!(1.0, test_data.distribution.cumulative_probability(upper), 0.0,"Cumulative probability of support upper bound must be equal to 1.");
        }

        assert_within_delta!(upper as f64, test_data.distribution.inverse_cumulative_probability(1.0).unwrap() as f64, 0.0 as f64,
        	"Inverse cumulative probability of 1 must be equal to support upper bound.");
    }
    
    #[test]
    fn  test_illegal_arguments()   {
    	gen_uniform_dist_test_data().test_illegal_arguments();
    }

	
}


                
                