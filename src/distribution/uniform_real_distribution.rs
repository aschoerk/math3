                
                   /*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use super::base::*;
use util::fastmath::F64;
                   use std::f64;
                   use java::exc::*;
                   use std::rc::*;
                   use analysis::traits::*;

/** Default accuracy. */
 const SOLVER_DEFAULT_ABSOLUTE_ACCURACY: f64 = 1e-6;


/**
* Base class for probability distributions on the reals.
* Default implementations are provided for some of the methods
* that do not vary from distribution to distribution.
*
* @since 3.0
*/

pub trait RealDistribution : RandomGeneratorMgr {


    /**
    * For a random variable {@code X} whose values are distributed according
    * to this distribution, this method returns {@code P(X <= x)}. In other
    * words, this method represents the (cumulative) distribution function
    * (CDF) for this distribution.
    *
    * @param x the point at which the CDF is evaluated
    * @return the probability that a random variable with this
    * distribution takes a value less than or equal to {@code x}
    */
    fn  cumulative_probability(&self,  x: f64) -> f64 ;

                   /**
     * For a random variable {@code X} whose values are distributed according
     * to this distribution, this method returns {@code P(x0 < X <= x1)}.
     *
     * @param x0 the exclusive lower bound
     * @param x1 the inclusive upper bound
     * @return the probability that a random variable with this distribution
     * takes a value between {@code x0} and {@code x1},
     * excluding the lower and including the upper endpoint
     * @throws NumberIsTooLargeException if {@code x0 > x1}
     *
     * @deprecated As of 3.1. In 4.0, this method will be renamed
     * {@code probability(double x0, double x1)}.
     *
     * The default implementation uses the identity
     * <p>{@code P(x0 < X <= x1) = P(X <= x1) - P(X <= x0)}</p>
     *
     * @deprecated As of 3.1 (to be removed in 4.0). Please use
     * {@link #probability(double,double)} instead.
     */
    fn  cumulative_probability_range(&self,  x0: f64,  x1: f64) -> /*  throws NumberIsTooLargeException */Result<f64, Rc<Exception>>   {
        return Ok(self.probability(x0, x1));
    }

    /**
     * For a random variable {@code X} whose values are distributed according
     * to this distribution, this method returns {@code P(x0 < X <= x1)}.
     *
     * @param x0 Lower bound (excluded).
     * @param x1 Upper bound (included).
     * @return the probability that a random variable with this distribution
     * takes a value between {@code x0} and {@code x1}, excluding the lower
     * and including the upper endpoint.
     * @throws NumberIsTooLargeException if {@code x0 > x1}.
     *
     * The default implementation uses the identity
     * {@code P(x0 < X <= x1) = P(X <= x1) - P(X <= x0)}
     *
     * @since 3.1
     */
    fn  probability_range(&self,  x0: f64,  x1: f64) -> f64  {
        if x0 > x1 {
                   panic!("lower endpoint ({0}) must be less than or equal to upper endpoint ({1})", x0, x1);
        }
        return self.cumulative_probability(x1) - self.cumulative_probability(x0);
    }

    /**
      * Computes the quantile function of this distribution. For a random
     * variable {@code X} distributed according to this distribution, the
     * returned value is
     * <ul>
     * <li><code>inf{x in R | P(X<=x) >= p}</code> for {@code 0 < p <= 1},</li>
     * <li><code>inf{x in R | P(X<=x) > 0}</code> for {@code p = 0}.</li>
     * </ul>
     *
     * @param p the cumulative probability
     * @return the smallest {@code p}-quantile of this distribution
     * (largest 0-quantile for {@code p = 0})
     * @throws OutOfRangeException if {@code p < 0} or {@code p > 1}
     *
     * The default implementation returns
     * <ul>
     * <li>{@link #getSupportLowerBound()} for {@code p = 0},</li>
     * <li>{@link #getSupportUpperBound()} for {@code p = 1}.</li>
     * </ul>
     */
    fn  inverse_cumulative_probability(&self,  p: f64) -> /*  throws OutOfRangeException */Result<f64, Rc<Exception>>   {
        /*
         * IMPLEMENTATION NOTES
         * --------------------
         * Where applicable, use is made of the one-sided Chebyshev inequality
         * to bracket the root. This inequality states that
         * P(X - mu >= k * sig) <= 1 / (1 + k^2),
         * mu: mean, sig: standard deviation. Equivalently
         * 1 - P(X < mu + k * sig) <= 1 / (1 + k^2),
         * F(mu + k * sig) >= k^2 / (1 + k^2).
         *
         * For k = sqrt(p / (1 - p)), we find
         * F(mu + k * sig) >= p,
         * and (mu + k * sig) is an upper-bound for the root.
         *
         * Then, introducing Y = -X, mean(Y) = -mu, sd(Y) = sig, and
         * P(Y >= -mu + k * sig) <= 1 / (1 + k^2),
         * P(-X >= -mu + k * sig) <= 1 / (1 + k^2),
         * P(X <= mu - k * sig) <= 1 / (1 + k^2),
         * F(mu - k * sig) <= 1 / (1 + k^2).
         *
         * For k = sqrt((1 - p) / p), we find
         * F(mu - k * sig) <= p,
         * and (mu - k * sig) is a lower-bound for the root.
         *
         * In cases where the Chebyshev inequality does not apply, geometric
         * progressions 1, 2, 4, ... and -1, -2, -4, ... are used to bracket
         * the root.
         */
        if p < 0.0 || p > 1.0 {
            return Err(Exc::new_msg("OutOfRangeException", format!("{0} out of [{1}, {2}] range",p, 0.0, 1.0)));
        }
         let lower_bound: f64 = self.get_support_lower_bound();
        if p == 0.0 {
            return Ok(lower_bound);
        }
         let upper_bound: f64 = self.get_support_upper_bound();
        if p == 1.0 {
            return Ok(upper_bound);
        }
         let mu: f64 = self.get_numerical_mean();
         let sig: f64 = F64::sqrt(&self.get_numerical_variance());
         let chebyshev_applies: bool;
        chebyshev_applies = !(f64::is_infinite(mu) || f64::is_nan(mu) || f64::is_infinite(sig) || f64::is_nan(sig));
        if lower_bound == f64::NEG_INFINITY {
            if chebyshev_applies {
                lower_bound = mu - sig * F64::sqrt((1.0 - p) / p);
            } else {
                lower_bound = -1.0;
                while self.cumulative_probability(lower_bound) >= p {
                    lower_bound *= 2.0;
                }
            }
        }
        if upper_bound == f64::INFINITY {
            if chebyshev_applies {
                upper_bound = mu + sig * F64::sqrt(p / (1.0 - p));
            } else {
                upper_bound = 1.0;
                while self.cumulative_probability(upper_bound) < p {
                    upper_bound *= 2.0;
                }
            }
        }
         let to_solve = UnivariateFunctionContainer::new(Box::new(move |x: f64| self.cumulative_probability(x) - p));
         let x: f64 = UnivariateSolverUtils::solve(&to_solve, lower_bound, upper_bound, &self.get_solver_absolute_accuracy());
        if !self.is_support_connected() {
            /* Test for plateau. */
             let dx: f64 = self.get_solver_absolute_accuracy();
            if x - dx >= self.get_support_lower_bound() {
                 let px: f64 = self.cumulative_probability(x);
                if self.cumulative_probability(x - dx) == px {
                    upper_bound = x;
                    while upper_bound - lower_bound > dx {
                         let mid_point: f64 = 0.5 * (lower_bound + upper_bound);
                        if self.cumulative_probability(mid_point) < px {
                            lower_bound = mid_point;
                        } else {
                            upper_bound = mid_point;
                        }
                    }
                    return upper_bound;
                }
            }
        }
        return x;
    }

    /**
     * Returns the solver absolute accuracy for inverse cumulative computation.
     * You can override this method in order to use a Brent solver with an
     * absolute accuracy different from the default.
     *
     * @return the maximum absolute error in inverse cumulative probability estimates
     */
    fn  get_solver_absolute_accuracy(&self) -> f64  {
        return self.solver_absolute_accuracy;
    }

    /**
     * Reseed the random generator used to generate samples.
     *
     * @param seed the new seed
     */fn  reseed_random_generator(&self,  seed: i64)   {
        let gen = self.get_random_generator();
        let mut tmp = gen.borrow_mut();
        tmp.set_seed_i64(&seed);
        tmp.re_seed(seed);
    }

    /**
     * Generate a random value sampled from this distribution.
     *
     * @return a random value.
     *
     * The default implementation uses the
     * <a href="http://en.wikipedia.org/wiki/Inverse_transform_sampling">
     * inversion method.
     * </a>
     */
    fn  sample(&self) -> f64  {
        return self.inverse_cumulative_probability(&self.random.next_double());
    }

    /**
     * Generate a random sample from the distribution.
     *
     * @param sampleSize the number of random values to generate
     * @return an array representing the random sample
     * @throws org.apache.commons.math3.exception.NotStrictlyPositiveException
     * if {@code sampleSize} is not positive
     *
     * The default implementation generates the sample by calling
     * {@link #sample()} in a loop.
     */
    fn  samples(&self,  sample_size: i32) -> Vec<f64>  {
        if sample_size <= 0 {
            panic!("number of samples ({0})", sample_size);
        }
         let mut out: [f64; sample_size] = [0.0; sample_size];
         {
             let mut i: i32 = 0;
            while i < sample_size {
                {
                    out[i] = self.sample();
                }
                i += 1;
             }
         }

        return out;
    }

    /**
     * For a random variable {@code X} whose values are distributed according
     * to this distribution, this method returns {@code P(X = x)}. In other
     * words, this method represents the probability mass function (PMF)
     * for the distribution.
     *
     * @param x the point at which the PMF is evaluated
     * @return the value of the probability mass function at point {@code x}
     *
     * @return zero.
     * @since 3.1
     */
    fn  probability(&self,  x: f64) -> f64  {
        return 0.0;
    }

    /**
     * Returns the natural logarithm of the probability density function (PDF) of this distribution
     * evaluated at the specified point {@code x}. In general, the PDF is the derivative of the
     * {@link #cumulativeProbability(double) CDF}. If the derivative does not exist at {@code x},
     * then an appropriate replacement should be returned, e.g. {@code Double.POSITIVE_INFINITY},
     * {@code Double.NaN}, or the limit inferior or limit superior of the difference quotient. Note
     * that due to the floating point precision and under/overflow issues, this method will for some
     * distributions be more precise and faster than computing the logarithm of
     * {@link #density(double)}. The default implementation simply computes the logarithm of
     * {@code density(x)}.
     *
     * @param x the point at which the PDF is evaluated
     * @return the logarithm of the value of the probability density function at point {@code x}
     */
    fn  log_density(&self,  x: f64) -> f64  {
        return F64::log(&self.density(x));
    }

    /**
    * Returns the probability density function (PDF) of this distribution
    * evaluated at the specified point {@code x}. In general, the PDF is
    * the derivative of the {@link #cumulativeProbability(double) CDF}.
    * If the derivative does not exist at {@code x}, then an appropriate
    * replacement should be returned, e.g. {@code Double.POSITIVE_INFINITY},
    * {@code Double.NaN}, or  the limit inferior or limit superior of the
    * difference quotient.
    *
    * @param x the point at which the PDF is evaluated
    * @return the value of the probability density function at point {@code x}
    */
    fn  density(&self,  x: f64) -> f64 ;

    /**
    * Use this method to get the numerical value of the mean of this
    * distribution.
    *
    * @return the mean or {@code Double.NaN} if it is not defined
    */
    fn  get_numerical_mean(&self) -> f64 ;

    /**
     * Use this method to get the numerical value of the variance of this
     * distribution.
     *
     * @return the variance (possibly {@code Double.POSITIVE_INFINITY} as
     * for certain cases in {@link TDistribution}) or {@code Double.NaN} if it
     * is not defined
     */
    fn  get_numerical_variance(&self) -> f64 ;

    /**
     * Access the lower bound of the support. This method must return the same
     * value as {@code inverseCumulativeProbability(0)}. In other words, this
     * method must return
     * <p><code>inf {x in R | P(X <= x) > 0}</code>.</p>
     *
     * @return lower bound of the support (might be
     * {@code Double.NEGATIVE_INFINITY})
     */
    fn  get_support_lower_bound(&self) -> f64 ;

    /**
     * Access the upper bound of the support. This method must return the same
     * value as {@code inverseCumulativeProbability(1)}. In other words, this
     * method must return
     * <p><code>inf {x in R | P(X <= x) = 1}</code>.</p>
     *
     * @return upper bound of the support (might be
     * {@code Double.POSITIVE_INFINITY})
     */
    fn  get_support_upper_bound(&self) -> f64 ;

    /**
     * Whether or not the lower bound of support is in the domain of the density
     * function.  Returns true iff {@code getSupporLowerBound()} is finite and
     * {@code density(getSupportLowerBound())} returns a non-NaN, non-infinite
     * value.
     *
     * @return true if the lower bound of support is finite and the density
     * function returns a non-NaN, non-infinite value there
     * @deprecated to be removed in 4.0
     */
    fn  is_support_lower_bound_inclusive(&self) -> bool ;

    /**
     * Whether or not the upper bound of support is in the domain of the density
     * function.  Returns true iff {@code getSupportUpperBound()} is finite and
     * {@code density(getSupportUpperBound())} returns a non-NaN, non-infinite
     * value.
     *
     * @return true if the upper bound of support is finite and the density
     * function returns a non-NaN, non-infinite value there
     * @deprecated to be removed in 4.0
     */
    fn  is_support_upper_bound_inclusive(&self) -> bool ;

    /**
     * Use this method to get information about whether the support is connected,
     * i.e. whether all values between the lower and upper bound of the support
     * are included in the support.
     *
     * @return whether the support is connected or not
     */
    fn  is_support_connected(&self) -> bool ;

}


                
                