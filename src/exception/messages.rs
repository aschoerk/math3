const ARGUMENT_OUTSIDE_DOMAIN: &'static str = "Argument {0} outside domain [{1} ; {2}]";
const    ARRAY_SIZE_EXCEEDS_MAX_VARIABLES: &'static str = "array size cannot be greater than {0}";
const    ARRAY_SIZES_SHOULD_HAVE_DIFFERENCE_1: &'static str = "array sizes should have difference 1  = {0} != {1} + 1)";
const    ARRAY_SUMS_TO_ZERO: &'static str = "array sums to zero";
const    ASSYMETRIC_EIGEN_NOT_SUPPORTED: &'static str = "eigen decomposition of assymetric matrices not supported yet";
const    AT_LEAST_ONE_COLUMN: &'static str = "matrix must have at least one column";
const    AT_LEAST_ONE_ROW: &'static str = "matrix must have at least one row";
const    BANDWIDTH: &'static str = "bandwidth  = {0})";
const    BESSEL_FUNCTION_BAD_ARGUMENT: &'static str = "Bessel function of order {0} cannot be computed for x = {1}";
const    BESSEL_FUNCTION_FAILED_CONVERGENCE: &'static str = "Bessel function of order {0} failed to converge for x = {1}";
const    BINOMIAL_INVALID_PARAMETERS_ORDER: &'static str = "must have n >= k for binomial coefficient  = n, k; got k = {0}, n = {1}";
const    BINOMIAL_NEGATIVE_PARAMETER: &'static str = "must have n >= 0 for binomial coefficient  = n, k; got n = {0}";
const    CANNOT_CLEAR_STATISTIC_CONSTRUCTED_FROM_EXTERNAL_MOMENTS: &'static str = "statistics constructed from external moments cannot be cleared";
const    CANNOT_COMPUTE_0TH_ROOT_OF_UNITY: &'static str = "cannot compute 0-th root of unity, indefinite result";
const    CANNOT_COMPUTE_BETA_DENSITY_AT_0_FOR_SOME_ALPHA: &'static str = "cannot compute beta density at 0 when alpha = {0,number}";
const    CANNOT_COMPUTE_BETA_DENSITY_AT_1_FOR_SOME_BETA: &'static str = "cannot compute beta density at 1 when beta = %.3g";
const    CANNOT_COMPUTE_NTH_ROOT_FOR_NEGATIVE_N: &'static str = "cannot compute nth root for null or negative n: {0}";
const    CANNOT_DISCARD_NEGATIVE_NUMBER_OF_ELEMENTS: &'static str = "cannot discard a negative number of elements  = {0})";
const    CANNOT_FORMAT_INSTANCE_AS_3D_VECTOR: &'static str = "cannot format a {0} instance as a 3D vector";
const    CANNOT_FORMAT_INSTANCE_AS_COMPLEX: &'static str = "cannot format a {0} instance as a complex number";
const    CANNOT_FORMAT_INSTANCE_AS_REAL_VECTOR: &'static str = "cannot format a {0} instance as a real vector";
const    CANNOT_FORMAT_OBJECT_TO_FRACTION: &'static str = "cannot format given object as a fraction number";
const    CANNOT_INCREMENT_STATISTIC_CONSTRUCTED_FROM_EXTERNAL_MOMENTS: &'static str = "statistics constructed from external moments cannot be incremented";
const    CANNOT_NORMALIZE_A_ZERO_NORM_VECTOR: &'static str = "cannot normalize a zero norm vector";
const    CANNOT_RETRIEVE_AT_NEGATIVE_INDEX: &'static str = "elements cannot be retrieved from a negative array index {0}";
const    CANNOT_SET_AT_NEGATIVE_INDEX: &'static str = "cannot set an element at a negative index {0}";
const    CANNOT_SUBSTITUTE_ELEMENT_FROM_EMPTY_ARRAY: &'static str = "cannot substitute an element from an empty array";
const    CANNOT_TRANSFORM_TO_DOUBLE: &'static str = "Conversion Exception in Transformation: {0}";
const    CARDAN_ANGLES_SINGULARITY: &'static str = "Cardan angles singularity";
const    CLASS_DOESNT_IMPLEMENT_COMPARABLE: &'static str = "class  = {0}) does not implement Comparable";
const    CLOSE_VERTICES: &'static str = "too close vertices near point  = {0}, {1}, {2})";
const    CLOSEST_ORTHOGONAL_MATRIX_HAS_NEGATIVE_DETERMINANT: &'static str = "the closest orthogonal matrix has a negative determinant {0}";
const    COLUMN_INDEX_OUT_OF_RANGE: &'static str = "column index {0} out of allowed range [{1}, {2}]";
const    COLUMN_INDEX: &'static str = "column index  = {0})"; /* keep */
const    CONSTRAINT: &'static str = "constraint"; /* keep */
const    CONTINUED_FRACTION_INFINITY_DIVERGENCE: &'static str = "Continued fraction convergents diverged to +/- infinity for value {0}";
const    CONTINUED_FRACTION_NAN_DIVERGENCE: &'static str = "Continued fraction diverged to NaN for value {0}";
const    CONTRACTION_CRITERIA_SMALLER_THAN_EXPANSION_FACTOR: &'static str = "contraction criteria  = {0}) smaller than the expansion factor  = {1}).  This would lead to a never ending loop of expansion and contraction as a newly expanded internal storage array would immediately satisfy the criteria for contraction.";
const    CONTRACTION_CRITERIA_SMALLER_THAN_ONE: &'static str = "contraction criteria smaller than one  = {0}).  This would lead to a never ending loop of expansion and contraction as an internal storage array length equal to the number of elements would satisfy the contraction criteria.";
const    CONVERGENCE_FAILED: &'static str = "convergence failed"; /* keep */
const    CROSSING_BOUNDARY_LOOPS: &'static str = "some outline boundary loops cross each other";
const    CROSSOVER_RATE: &'static str = "crossover rate  = {0})";
const    CUMULATIVE_PROBABILITY_RETURNED_NAN: &'static str = "Cumulative probability function returned NaN for argument {0} p = {1}";
const    DIFFERENT_ROWS_LENGTHS: &'static str = "some rows have length {0} while others have length {1}";
const    DIFFERENT_ORIG_AND_PERMUTED_DATA: &'static str = "original and permuted data must contain the same elements";
const    DIGEST_NOT_INITIALIZED: &'static str = "digest not initialized";
const    DIMENSIONS_MISMATCH_2x2: &'static str = "got {0}x{1} but expected {2}x{3}"; /* keep */
const    DIMENSIONS_MISMATCH_SIMPLE: &'static str = "{0} != {1}"; /* keep */
const    DIMENSIONS_MISMATCH: &'static str = "dimensions mismatch"; /* keep */
const    DISCRETE_CUMULATIVE_PROBABILITY_RETURNED_NAN: &'static str = "Discrete cumulative probability function returned NaN for argument {0}";
const    DISTRIBUTION_NOT_LOADED: &'static str = "distribution not loaded";
const    DUPLICATED_ABSCISSA_DIVISION_BY_ZERO: &'static str = "duplicated abscissa {0} causes division by zero";
const    EDGE_CONNECTED_TO_ONE_FACET: &'static str = "edge joining points  = {0}, {1}, {2}) and  = {3}, {4}, {5}) is connected to one facet only";
const    ELITISM_RATE: &'static str = "elitism rate  = {0})";
const    EMPTY_CLUSTER_IN_K_MEANS: &'static str = "empty cluster in k-means";
const    EMPTY_INTERPOLATION_SAMPLE: &'static str = "sample for interpolation is empty";
const    EMPTY_POLYNOMIALS_COEFFICIENTS_ARRAY: &'static str = "empty polynomials coefficients array"; /* keep */
const    EMPTY_SELECTED_COLUMN_INDEX_ARRAY: &'static str = "empty selected column index array";
const    EMPTY_SELECTED_ROW_INDEX_ARRAY: &'static str = "empty selected row index array";
const    EMPTY_STRING_FOR_IMAGINARY_CHARACTER: &'static str = "empty string for imaginary character";
const    ENDPOINTS_NOT_AN_INTERVAL: &'static str = "endpoints do not specify an interval: [{0}, {1}]";
const    EQUAL_VERTICES_IN_SIMPLEX: &'static str = "equal vertices {0} and {1} in simplex configuration";
const    EULER_ANGLES_SINGULARITY: &'static str = "Euler angles singularity";
const    EVALUATION: &'static str = "evaluation"; /* keep */
const    EXPANSION_FACTOR_SMALLER_THAN_ONE: &'static str = "expansion factor smaller than one  = {0})";
const    FACET_ORIENTATION_MISMATCH: &'static str = "facets orientation mismatch around edge joining points  = {0}, {1}, {2}) and  = {3}, {4}, {5})";
const    FACTORIAL_NEGATIVE_PARAMETER: &'static str = "must have n >= 0 for n!, got n = {0}";
const    FAILED_BRACKETING: &'static str = "number of iterations={4}, maximum iterations={5}, initial={6}, lower bound={7}, upper bound={8}, final a value={0}, final b value={1}, f = a)={2}, f = b)={3}";
const    FAILED_FRACTION_CONVERSION: &'static str = "Unable to convert {0} to fraction after {1} iterations";
const    FIRST_COLUMNS_NOT_INITIALIZED_YET: &'static str = "first {0} columns are not initialized yet";
const    FIRST_ELEMENT_NOT_ZERO: &'static str = "first element is not 0: {0}";
const    FIRST_ROWS_NOT_INITIALIZED_YET: &'static str = "first {0} rows are not initialized yet";
const    FRACTION_CONVERSION_OVERFLOW: &'static str = "Overflow trying to convert {0} to fraction  = {1}/{2})";
const    FUNCTION_NOT_DIFFERENTIABLE: &'static str = "function is not differentiable";
const    FUNCTION_NOT_POLYNOMIAL: &'static str = "function is not polynomial";
const    GCD_OVERFLOW_32_BITS: &'static str = "overflow: gcd = {0}, {1}) is 2^31";
const    GCD_OVERFLOW_64_BITS: &'static str = "overflow: gcd = {0}, {1}) is 2^63";
const    HOLE_BETWEEN_MODELS_TIME_RANGES: &'static str = "{0} wide hole between models time ranges";
const    ILL_CONDITIONED_OPERATOR: &'static str = "condition number {1} is too high ";
const    INCONSISTENT_STATE_AT_2_PI_WRAPPING: &'static str = "inconsistent state at 2\u03c0 wrapping";
const    INDEX_LARGER_THAN_MAX: &'static str = "the index specified: {0} is larger than the current maximal index {1}";
const    INDEX_NOT_POSITIVE: &'static str = "index  = {0}) is not positive";
const    INDEX_OUT_OF_RANGE: &'static str = "index {0} out of allowed range [{1}, {2}]";
const    INDEX: &'static str = "index  = {0})"; /* keep */
const    NOT_FINITE_NUMBER: &'static str = "{0} is not a finite number"; /* keep */
const    INFINITE_BOUND: &'static str = "interval bounds must be finite";
const    ARRAY_ELEMENT: &'static str = "value {0} at index {1}"; /* keep */
const    INFINITE_ARRAY_ELEMENT: &'static str = "Array contains an infinite element, {0} at index {1}";
const    INFINITE_VALUE_CONVERSION: &'static str = "cannot convert infinite value";
const    INITIAL_CAPACITY_NOT_POSITIVE: &'static str = "initial capacity  = {0}) is not positive";
const    INITIAL_COLUMN_AFTER_FINAL_COLUMN: &'static str = "initial column {1} after final column {0}";
const    INITIAL_ROW_AFTER_FINAL_ROW: &'static str = "initial row {1} after final row {0}";
const    @Deprecated
const    INPUT_DATA_FROM_UNSUPPORTED_DATASOURCE: &'static str = "input data comes from unsupported datasource: {0}, supported sources: {1}, {2}";
const    INSTANCES_NOT_COMPARABLE_TO_EXISTING_VALUES: &'static str = "instance of class {0} not comparable to existing values";
const    INSUFFICIENT_DATA: &'static str = "insufficient data";
const    INSUFFICIENT_DATA_FOR_T_STATISTIC: &'static str = "insufficient data for t statistic, needs at least 2, got {0}";
const    INSUFFICIENT_DIMENSION: &'static str = "insufficient dimension {0}, must be at least {1}";
const    DIMENSION: &'static str = "dimension  = {0})"; /* keep */
const    INSUFFICIENT_OBSERVED_POINTS_IN_SAMPLE: &'static str = "sample contains {0} observed points, at least {1} are required";
const    INSUFFICIENT_ROWS_AND_COLUMNS: &'static str = "insufficient data: only {0} rows and {1} columns.";
const    INTEGRATION_METHOD_NEEDS_AT_LEAST_TWO_PREVIOUS_POINTS: &'static str = "multistep method needs at least {0} previous steps, got {1}";
const    INTERNAL_ERROR: &'static str = "internal error, please fill a bug report at {0}";
const    INVALID_BINARY_DIGIT: &'static str = "invalid binary digit: {0}";
const    INVALID_BINARY_CHROMOSOME: &'static str = "binary mutation works on BinaryChromosome only";
const    INVALID_BRACKETING_PARAMETERS: &'static str = "invalid bracketing parameters:  lower bound={0},  initial={1}, upper bound={2}";
const    INVALID_FIXED_LENGTH_CHROMOSOME: &'static str = "one-point crossover only works with fixed-length chromosomes";
const    INVALID_IMPLEMENTATION: &'static str = "required functionality is missing in {0}";
const    INVALID_INTERVAL_INITIAL_VALUE_PARAMETERS: &'static str = "invalid interval, initial value parameters:  lower={0}, initial={1}, upper={2}";
const    INVALID_ITERATIONS_LIMITS: &'static str = "invalid iteration limits: min={0}, max={1}";
const    INVALID_MAX_ITERATIONS: &'static str = "bad value for maximum iterations number: {0}";
const    NOT_ENOUGH_DATA_REGRESSION: &'static str = "the number of observations is not sufficient to conduct regression";
const    INVALID_REGRESSION_ARRAY: &'static str = "input data array length = {0} does not match the number of observations = {1} and the number of regressors = {2}";
const    INVALID_REGRESSION_OBSERVATION: &'static str = "length of regressor array = {0} does not match the number of variables = {1} in the model";
const    INVALID_ROUNDING_METHOD: &'static str = "invalid rounding method {0}, valid methods: {1}  = {2}; {3}  = {4}; {5}  = {6}; {7}  = {8}; {9}  = {10}; {11}  = {12}; {13}  = {14}; {15}  = {16})";
const    ITERATOR_EXHAUSTED: &'static str = "iterator exhausted";
const    ITERATIONS: &'static str = "iterations"; /* keep */
const    LCM_OVERFLOW_32_BITS: &'static str = "overflow: lcm = {0}, {1}) is 2^31";
const    LCM_OVERFLOW_64_BITS: &'static str = "overflow: lcm = {0}, {1}) is 2^63";
const    LIST_OF_CHROMOSOMES_BIGGER_THAN_POPULATION_SIZE: &'static str = "list of chromosomes bigger than maxPopulationSize";
const    LOESS_EXPECTS_AT_LEAST_ONE_POINT: &'static str = "Loess expects at least 1 point";
const    LOWER_BOUND_NOT_BELOW_UPPER_BOUND: &'static str = "lower bound  = {0}) must be strictly less than upper bound  = {1})"; /* keep */
const    LOWER_ENDPOINT_ABOVE_UPPER_ENDPOINT: &'static str = "lower endpoint  = {0}) must be less than or equal to upper endpoint  = {1})";
const    MAP_MODIFIED_WHILE_ITERATING: &'static str = "map has been modified while iterating";
const    MULTISTEP_STARTER_STOPPED_EARLY: &'static str = "multistep integrator starter stopped early, maybe too large step size";
const    EVALUATIONS: &'static str = "evaluations"; /* keep */
const    MAX_COUNT_EXCEEDED: &'static str = "maximal count  = {0}) exceeded"; /* keep */
const    MAX_ITERATIONS_EXCEEDED: &'static str = "maximal number of iterations  = {0}) exceeded";
const    MINIMAL_STEPSIZE_REACHED_DURING_INTEGRATION: &'static str = "minimal step size  = {1,number,0.00E00}) reached, integration needs {0,number,0.00E00}";
const    MISMATCHED_LOESS_ABSCISSA_ORDINATE_ARRAYS: &'static str = "Loess expects the abscissa and ordinate arrays to be of the same size, but got {0} abscissae and {1} ordinatae";
const    MUTATION_RATE: &'static str = "mutation rate  = {0})";
const    NAN_ELEMENT_AT_INDEX: &'static str = "element {0} is NaN";
const    NAN_VALUE_CONVERSION: &'static str = "cannot convert NaN value";
const    NEGATIVE_BRIGHTNESS_EXPONENT: &'static str = "brightness exponent should be positive or null, but got {0}";
const    NEGATIVE_COMPLEX_MODULE: &'static str = "negative complex module {0}";
const    NEGATIVE_ELEMENT_AT_2D_INDEX: &'static str = "element  = {0}, {1}) is negative: {2}";
const    NEGATIVE_ELEMENT_AT_INDEX: &'static str = "element {0} is negative: {1}";
const    NEGATIVE_NUMBER_OF_SUCCESSES: &'static str = "number of successes must be non-negative  = {0})";
const    NUMBER_OF_SUCCESSES: &'static str = "number of successes  = {0})"; /* keep */
const    NEGATIVE_NUMBER_OF_TRIALS: &'static str = "number of trials must be non-negative  = {0})";
const    NUMBER_OF_INTERPOLATION_POINTS: &'static str = "number of interpolation points  = {0})"; /* keep */
const    NUMBER_OF_TRIALS: &'static str = "number of trials  = {0})";
const    NOT_CONVEX: &'static str = "vertices do not form a convex hull in CCW winding";
const    NOT_CONVEX_HYPERPLANES: &'static str = "hyperplanes do not define a convex region";
const    ROBUSTNESS_ITERATIONS: &'static str = "number of robustness iterations  = {0})";
const    START_POSITION: &'static str = "start position  = {0})"; /* keep */
const    NON_CONVERGENT_CONTINUED_FRACTION: &'static str = "Continued fraction convergents failed to converge  = in less than {0} iterations) for value {1}";
const    NON_INVERTIBLE_TRANSFORM: &'static str = "non-invertible affine transform collapses some lines into single points";
const    NON_POSITIVE_MICROSPHERE_ELEMENTS: &'static str = "number of microsphere elements must be positive, but got {0}";
const    NON_POSITIVE_POLYNOMIAL_DEGREE: &'static str = "polynomial degree must be positive: degree={0}";
const    NON_REAL_FINITE_ABSCISSA: &'static str = "all abscissae must be finite real numbers, but {0}-th is {1}";
const    NON_REAL_FINITE_ORDINATE: &'static str = "all ordinatae must be finite real numbers, but {0}-th is {1}";
const    NON_REAL_FINITE_WEIGHT: &'static str = "all weights must be finite real numbers, but {0}-th is {1}";
const    NON_SQUARE_MATRIX: &'static str = "non square  = {0}x{1}) matrix";
const    NORM: &'static str = "Norm  = {0})"; /* keep */
const    NORMALIZE_INFINITE: &'static str = "Cannot normalize to an infinite value";
const    NORMALIZE_NAN: &'static str = "Cannot normalize to NaN";
const    NOT_ADDITION_COMPATIBLE_MATRICES: &'static str = "{0}x{1} and {2}x{3} matrices are not addition compatible";
const    NOT_DECREASING_NUMBER_OF_POINTS: &'static str = "points {0} and {1} are not decreasing  = {2} < {3})";
const    NOT_DECREASING_SEQUENCE: &'static str = "points {3} and {2} are not decreasing  = {1} < {0})"; /* keep */
const    NOT_ENOUGH_DATA_FOR_NUMBER_OF_PREDICTORS: &'static str = "not enough data  = {0} rows) for this many predictors  = {1} predictors)";
const    NOT_ENOUGH_POINTS_IN_SPLINE_PARTITION: &'static str = "spline partition must have at least {0} points, got {1}";
const    NOT_INCREASING_NUMBER_OF_POINTS: &'static str = "points {0} and {1} are not increasing  = {2} > {3})";
const    NOT_INCREASING_SEQUENCE: &'static str = "points {3} and {2} are not increasing  = {1} > {0})"; /* keep */
const    NOT_MULTIPLICATION_COMPATIBLE_MATRICES: &'static str = "{0}x{1} and {2}x{3} matrices are not multiplication compatible";
const    NOT_POSITIVE_DEFINITE_MATRIX: &'static str = "not positive definite matrix"; /* keep */
const    NON_POSITIVE_DEFINITE_MATRIX: &'static str = "not positive definite matrix: diagonal element at  = {1},{1}) is smaller than {2}  = {0})";
const    NON_POSITIVE_DEFINITE_OPERATOR: &'static str = "non positive definite linear operator"; /* keep */
const    NON_SELF_ADJOINT_OPERATOR: &'static str = "non self-adjoint linear operator"; /* keep */
const    NON_SQUARE_OPERATOR: &'static str = "non square  = {0}x{1}) linear operator"; /* keep */
const    DEGREES_OF_FREEDOM: &'static str = "degrees of freedom  = {0})"; /* keep */
const    NOT_POSITIVE_DEGREES_OF_FREEDOM: &'static str = "degrees of freedom must be positive  = {0})";
const    NOT_POSITIVE_ELEMENT_AT_INDEX: &'static str = "element {0} is not positive: {1}";
const    NOT_POSITIVE_EXPONENT: &'static str = "invalid exponent {0}  = must be positive)";
const    NUMBER_OF_ELEMENTS_SHOULD_BE_POSITIVE: &'static str = "number of elements should be positive  = {0})";
const    BASE: &'static str = "base  = {0})"; /* keep */
const    EXPONENT: &'static str = "exponent  = {0})"; /* keep */
const    NOT_POSITIVE_LENGTH: &'static str = "length must be positive  = {0})";
const    LENGTH: &'static str = "length  = {0})"; /* keep */
const    NOT_POSITIVE_MEAN: &'static str = "mean must be positive  = {0})";
const    MEAN: &'static str = "mean  = {0})"; /* keep */
const    NOT_POSITIVE_NUMBER_OF_SAMPLES: &'static str = "number of sample is not positive: {0}";
const    NUMBER_OF_SAMPLES: &'static str = "number of samples  = {0})"; /* keep */
const    NOT_POSITIVE_PERMUTATION: &'static str = "permutation k  = {0}) must be positive";
const    PERMUTATION_SIZE: &'static str = "permutation size  = {0}"; /* keep */
const    NOT_POSITIVE_POISSON_MEAN: &'static str = "the Poisson mean must be positive  = {0})";
const    NOT_POSITIVE_POPULATION_SIZE: &'static str = "population size must be positive  = {0})";
const    POPULATION_SIZE: &'static str = "population size  = {0})"; /* keep */
const    NOT_POSITIVE_ROW_DIMENSION: &'static str = "invalid row dimension: {0}  = must be positive)";
const    NOT_POSITIVE_SAMPLE_SIZE: &'static str = "sample size must be positive  = {0})";
const    NOT_POSITIVE_SCALE: &'static str = "scale must be positive  = {0})";
const    SCALE: &'static str = "scale  = {0})"; /* keep */
const    NOT_POSITIVE_SHAPE: &'static str = "shape must be positive  = {0})";
const    SHAPE: &'static str = "shape  = {0})"; /* keep */
const    NOT_POSITIVE_STANDARD_DEVIATION: &'static str = "standard deviation must be positive  = {0})";
const    STANDARD_DEVIATION: &'static str = "standard deviation  = {0})"; /* keep */
const    NOT_POSITIVE_UPPER_BOUND: &'static str = "upper bound must be positive  = {0})";
const    NOT_POSITIVE_WINDOW_SIZE: &'static str = "window size must be positive  = {0})";
const    NOT_POWER_OF_TWO: &'static str = "{0} is not a power of 2";
const    NOT_POWER_OF_TWO_CONSIDER_PADDING: &'static str = "{0} is not a power of 2, consider padding for fix";
const    NOT_POWER_OF_TWO_PLUS_ONE: &'static str = "{0} is not a power of 2 plus one";
const    NOT_STRICTLY_DECREASING_NUMBER_OF_POINTS: &'static str = "points {0} and {1} are not strictly decreasing  = {2} <= {3})";
const    NOT_STRICTLY_DECREASING_SEQUENCE: &'static str = "points {3} and {2} are not strictly decreasing  = {1} <= {0})"; /* keep */
const    NOT_STRICTLY_INCREASING_KNOT_VALUES: &'static str = "knot values must be strictly increasing";
const    NOT_STRICTLY_INCREASING_NUMBER_OF_POINTS: &'static str = "points {0} and {1} are not strictly increasing  = {2} >= {3})";
const    NOT_STRICTLY_INCREASING_SEQUENCE: &'static str = "points {3} and {2} are not strictly increasing  = {1} >= {0})"; /* keep */
const    NOT_SUBTRACTION_COMPATIBLE_MATRICES: &'static str = "{0}x{1} and {2}x{3} matrices are not subtraction compatible";
const    NOT_SUPPORTED_IN_DIMENSION_N: &'static str = "method not supported in dimension {0}";
const    NOT_SYMMETRIC_MATRIX: &'static str = "not symmetric matrix";
const    NON_SYMMETRIC_MATRIX: &'static str = "non symmetric matrix: the difference between entries at  = {0},{1}) and  = {1},{0}) is larger than {2}"; /* keep */
const    NO_BIN_SELECTED: &'static str = "no bin selected";
const    NO_CONVERGENCE_WITH_ANY_START_POINT: &'static str = "none of the {0} start points lead to convergence"; /* keep */
const    NO_DATA: &'static str = "no data"; /* keep */
const    NO_DEGREES_OF_FREEDOM: &'static str = "no degrees of freedom  = {0} measurements, {1} parameters)";
const    NO_DENSITY_FOR_THIS_DISTRIBUTION: &'static str = "This distribution does not have a density function implemented";
const    NO_FEASIBLE_SOLUTION: &'static str = "no feasible solution";
const    NO_OPTIMUM_COMPUTED_YET: &'static str = "no optimum computed yet"; /* keep */
const    NO_REGRESSORS: &'static str = "Regression model must include at least one regressor";
const    NO_RESULT_AVAILABLE: &'static str = "no result available";
const    NO_SUCH_MATRIX_ENTRY: &'static str = "no entry at indices  = {0}, {1}) in a {2}x{3} matrix";
const    NAN_NOT_ALLOWED: &'static str = "NaN is not allowed";
const    NULL_NOT_ALLOWED: &'static str = "null is not allowed"; /* keep */
const    ARRAY_ZERO_LENGTH_OR_NULL_NOT_ALLOWED: &'static str = "a null or zero length array not allowed";
const    COVARIANCE_MATRIX: &'static str = "covariance matrix"; /* keep */
const    DENOMINATOR: &'static str = "denominator"; /* keep */
const    DENOMINATOR_FORMAT: &'static str = "denominator format"; /* keep */
const    FRACTION: &'static str = "fraction"; /* keep */
const    FUNCTION: &'static str = "function"; /* keep */
const    IMAGINARY_FORMAT: &'static str = "imaginary format"; /* keep */
const    INPUT_ARRAY: &'static str = "input array"; /* keep */
const    NUMERATOR: &'static str = "numerator"; /* keep */
const    NUMERATOR_FORMAT: &'static str = "numerator format"; /* keep */
const    OBJECT_TRANSFORMATION: &'static str = "conversion exception in transformation"; /* keep */
const    REAL_FORMAT: &'static str = "real format"; /* keep */
const    WHOLE_FORMAT: &'static str = "whole format"; /* keep */
const    NUMBER_TOO_LARGE: &'static str = "{0} is larger than the maximum  = {1})"; /* keep */
const    NUMBER_TOO_SMALL: &'static str = "{0} is smaller than the minimum  = {1})"; /* keep */
const    NUMBER_TOO_LARGE_BOUND_EXCLUDED: &'static str = "{0} is larger than, or equal to, the maximum  = {1})"; /* keep */
const    NUMBER_TOO_SMALL_BOUND_EXCLUDED: &'static str = "{0} is smaller than, or equal to, the minimum  = {1})"; /* keep */
const    NUMBER_OF_SUCCESS_LARGER_THAN_POPULATION_SIZE: &'static str = "number of successes  = {0}) must be less than or equal to population size  = {1})";
const    NUMERATOR_OVERFLOW_AFTER_MULTIPLY: &'static str = "overflow, numerator too large after multiply: {0}";
const    N_POINTS_GAUSS_LEGENDRE_INTEGRATOR_NOT_SUPPORTED: &'static str = "{0} points Legendre-Gauss integrator not supported, number of points must be in the {1}-{2} range";
const    OBSERVED_COUNTS_ALL_ZERO: &'static str = "observed counts are all 0 in observed array {0}";
const    OBSERVED_COUNTS_BOTTH_ZERO_FOR_ENTRY: &'static str = "observed counts are both zero for entry {0}";
const    BOBYQA_BOUND_DIFFERENCE_CONDITION: &'static str = "the difference between the upper and lower bound must be larger than twice the initial trust region radius  = {0})";
const    OUT_OF_BOUNDS_QUANTILE_VALUE: &'static str = "out of bounds quantile value: {0}, must be in  = 0, 100]";
const    OUT_OF_BOUNDS_CONFIDENCE_LEVEL: &'static str = "out of bounds confidence level {0}, must be between {1} and {2}";
const    OUT_OF_BOUND_SIGNIFICANCE_LEVEL: &'static str = "out of bounds significance level {0}, must be between {1} and {2}";
const    SIGNIFICANCE_LEVEL: &'static str = "significance level  = {0})"; /* keep */
const    OUT_OF_ORDER_ABSCISSA_ARRAY: &'static str = "the abscissae array must be sorted in a strictly increasing order, but the {0}-th element is {1} whereas {2}-th is {3}";
const    OUT_OF_PLANE: &'static str = "point  = {0}, {1}, {2}) is out of plane";
const    OUT_OF_RANGE_ROOT_OF_UNITY_INDEX: &'static str = "out of range root of unity index {0}  = must be in [{1};{2}])";
const    OUT_OF_RANGE: &'static str = "out of range"; /* keep */
const    OUT_OF_RANGE_SIMPLE: &'static str = "{0} out of [{1}, {2}] range"; /* keep */
const    OUT_OF_RANGE_LEFT: &'static str = "{0} out of  = {1}, {2}] range";
const    OUT_OF_RANGE_RIGHT: &'static str = "{0} out of [{1}, {2}) range";
const    OUTLINE_BOUNDARY_LOOP_OPEN: &'static str = "an outline boundary loop is open";
const    OVERFLOW: &'static str = "overflow"; /* keep */
const    OVERFLOW_IN_FRACTION: &'static str = "overflow in fraction {0}/{1}, cannot negate";
const    OVERFLOW_IN_ADDITION: &'static str = "overflow in addition: {0} + {1}";
const    OVERFLOW_IN_SUBTRACTION: &'static str = "overflow in subtraction: {0} - {1}";
const    OVERFLOW_IN_MULTIPLICATION: &'static str = "overflow in multiplication: {0} * {1}";
const    PERCENTILE_IMPLEMENTATION_CANNOT_ACCESS_METHOD: &'static str = "cannot access {0} method in percentile implementation {1}";
const    PERCENTILE_IMPLEMENTATION_UNSUPPORTED_METHOD: &'static str = "percentile implementation {0} does not support {1}";
const    PERMUTATION_EXCEEDS_N: &'static str = "permutation size  = {0}) exceeds permuation domain  = {1})"; /* keep */
const    POLYNOMIAL: &'static str = "polynomial"; /* keep */
const    POLYNOMIAL_INTERPOLANTS_MISMATCH_SEGMENTS: &'static str = "number of polynomial interpolants must match the number of segments  = {0} != {1} - 1)";
const    POPULATION_LIMIT_NOT_POSITIVE: &'static str = "population limit has to be positive";
const    POWER_NEGATIVE_PARAMETERS: &'static str = "cannot raise an integral value to a negative power  = {0}^{1})";
const    PROPAGATION_DIRECTION_MISMATCH: &'static str = "propagation direction mismatch";
const    RANDOMKEY_MUTATION_WRONG_CLASS: &'static str = "RandomKeyMutation works only with RandomKeys, not {0}";
const    ROOTS_OF_UNITY_NOT_COMPUTED_YET: &'static str = "roots of unity have not been computed yet";
const    ROTATION_MATRIX_DIMENSIONS: &'static str = "a {0}x{1} matrix cannot be a rotation matrix";
const    ROW_INDEX_OUT_OF_RANGE: &'static str = "row index {0} out of allowed range [{1}, {2}]";
const    ROW_INDEX: &'static str = "row index  = {0})"; /* keep */
const    SAME_SIGN_AT_ENDPOINTS: &'static str = "function values at endpoints do not have different signs, endpoints: [{0}, {1}], values: [{2}, {3}]";
const    SAMPLE_SIZE_EXCEEDS_COLLECTION_SIZE: &'static str = "sample size  = {0}) exceeds collection size  = {1})"; /* keep */
const    SAMPLE_SIZE_LARGER_THAN_POPULATION_SIZE: &'static str = "sample size  = {0}) must be less than or equal to population size  = {1})";
const    SIMPLEX_NEED_ONE_POINT: &'static str = "simplex must contain at least one point";
const    SIMPLE_MESSAGE: &'static str = "{0}";
const    SINGULAR_MATRIX: &'static str = "matrix is singular"; /* keep */
const    SINGULAR_OPERATOR: &'static str = "operator is singular";
const    SUBARRAY_ENDS_AFTER_ARRAY_END: &'static str = "subarray ends after array end";
const    TOO_LARGE_CUTOFF_SINGULAR_VALUE: &'static str = "cutoff singular value is {0}, should be at most {1}";
const    TOO_LARGE_TOURNAMENT_ARITY: &'static str = "tournament arity  = {0}) cannot be bigger than population size  = {1})";
const    TOO_MANY_ELEMENTS_TO_DISCARD_FROM_ARRAY: &'static str = "cannot discard {0} elements from a {1} elements array";
const    TOO_MANY_REGRESSORS: &'static str = "too many regressors  = {0}) specified, only {1} in the model";
const    TOO_SMALL_COST_RELATIVE_TOLERANCE: &'static str = "cost relative tolerance is too small  = {0}; no further reduction in the sum of squares is possible";
const    TOO_SMALL_INTEGRATION_INTERVAL: &'static str = "too small integration interval: length = {0}";
const    TOO_SMALL_ORTHOGONALITY_TOLERANCE: &'static str = "orthogonality tolerance is too small  = {0}; solution is orthogonal to the jacobian";
const    TOO_SMALL_PARAMETERS_RELATIVE_TOLERANCE: &'static str = "parameters relative tolerance is too small  = {0}; no further improvement in the approximate solution is possible";
const    TRUST_REGION_STEP_FAILED: &'static str = "trust region step has failed to reduce Q";
const    TWO_OR_MORE_CATEGORIES_REQUIRED: &'static str = "two or more categories required, got {0}";
const    TWO_OR_MORE_VALUES_IN_CATEGORY_REQUIRED: &'static str = "two or more values required in each category, one has {0}";
const    UNABLE_TO_BRACKET_OPTIMUM_IN_LINE_SEARCH: &'static str = "unable to bracket optimum in line search";
const    UNABLE_TO_COMPUTE_COVARIANCE_SINGULAR_PROBLEM: &'static str = "unable to compute covariances: singular problem";
const    UNABLE_TO_FIRST_GUESS_HARMONIC_COEFFICIENTS: &'static str = "unable to first guess the harmonic coefficients";
const    UNABLE_TO_ORTHOGONOLIZE_MATRIX: &'static str = "unable to orthogonalize matrix in {0} iterations";
const    UNABLE_TO_PERFORM_QR_DECOMPOSITION_ON_JACOBIAN: &'static str = "unable to perform Q.R decomposition on the {0}x{1} jacobian matrix";
const    UNABLE_TO_SOLVE_SINGULAR_PROBLEM: &'static str = "unable to solve: singular problem";
const    UNBOUNDED_SOLUTION: &'static str = "unbounded solution";
const    UNKNOWN_MODE: &'static str = "unknown mode {0}, known modes: {1}  = {2}; {3}  = {4}; {5}  = {6}; {7}  = {8}; {9}  = {10}) and {11}  = {12})";
const    UNKNOWN_PARAMETER: &'static str = "unknown parameter {0}";
const    UNMATCHED_ODE_IN_EXPANDED_SET: &'static str = "ode does not match the main ode set in the extended set";
const    CANNOT_PARSE_AS_TYPE: &'static str = "string \"{0}\" unparseable  = from position {1}) as an object of type {2}"; /* keep */
const    CANNOT_PARSE: &'static str = "string \"{0}\" unparseable  = from position {1})"; /* keep */
const    UNPARSEABLE_3D_VECTOR: &'static str = "unparseable 3D vector: \"{0}\"";
const    UNPARSEABLE_COMPLEX_NUMBER: &'static str = "unparseable complex number: \"{0}\"";
const    UNPARSEABLE_REAL_VECTOR: &'static str = "unparseable real vector: \"{0}\"";
const    UNSUPPORTED_EXPANSION_MODE: &'static str = "unsupported expansion mode {0}, supported modes are {1}  = {2}) and {3}  = {4})";
const    UNSUPPORTED_OPERATION: &'static str = "unsupported operation"; /* keep */
const    ARITHMETIC_EXCEPTION: &'static str = "arithmetic exception"; /* keep */
const    ILLEGAL_STATE: &'static str = "illegal state"; /* keep */
const    USER_EXCEPTION: &'static str = "exception generated in user code"; /* keep */
const    URL_CONTAINS_NO_DATA: &'static str = "URL {0} contains no data";
const    VALUES_ADDED_BEFORE_CONFIGURING_STATISTIC: &'static str = "{0} values have been added before statistic is configured";
const    VECTOR_LENGTH_MISMATCH: &'static str = "vector length mismatch: got {0} but expected {1}";
const    VECTOR_MUST_HAVE_AT_LEAST_ONE_ELEMENT: &'static str = "vector must have at least one element";
const    WEIGHT_AT_LEAST_ONE_NON_ZERO: &'static str = "weigth array must contain at least one non-zero value";
const    WRONG_BLOCK_LENGTH: &'static str = "wrong array shape  = block length = {0}, expected {1})";
const    WRONG_NUMBER_OF_POINTS: &'static str = "{0} points are required, got only {1}";
const    NUMBER_OF_POINTS: &'static str = "number of points  = {0})"; /* keep */
const    ZERO_DENOMINATOR: &'static str = "denominator must be different from 0"; /* keep */
const    ZERO_DENOMINATOR_IN_FRACTION: &'static str = "zero denominator in fraction {0}/{1}";
const    ZERO_FRACTION_TO_DIVIDE_BY: &'static str = "the fraction to divide by must not be zero: {0}/{1}";
const    ZERO_NORM: &'static str = "zero norm";
const    ZERO_NORM_FOR_ROTATION_AXIS: &'static str = "zero norm for rotation axis";
const    ZERO_NORM_FOR_ROTATION_DEFINING_VECTOR: &'static str = "zero norm for rotation defining vector";
const    ZERO_NOT_ALLOWED: &'static str = "zero not allowed here";
